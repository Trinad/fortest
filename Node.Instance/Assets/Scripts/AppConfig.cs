﻿using System;
using System.Xml.Serialization;
using Assets.Scripts.Utils.Logger;

namespace Assets.Scripts
{
	public class AppConfig : ILogConfig
	{
		public bool EnableUlink { get; set; } = true;
		public bool bDebugLogger { get; set; } = true;
		public ErrorLevel MinLogLevel { get; set; } = ErrorLevel.debug;
		public string Version = "";

		public SosConfig Sos { get; set; } = new SosConfig();
		public p2pLobbyConnectConfig p2pLobbyConnect = new p2pLobbyConnectConfig();
		public p2pSelfConnectConfig p2pSelfConnect = new p2pSelfConnectConfig();
        public AnalyticConnect analyticConnect = new AnalyticConnect();
        public int MaxConnections = 100;
		public int Interval = 200; //sync time
		public string PasswordForP2P = String.Empty;
		public uLinkStatsConfig uLinkStats = new uLinkStatsConfig();
		public NLogConfig nlog { get; set; } = new NLogConfig();
		public int ShiftPortForConnectFromLobby = 2000;
		public string level_url { get; set; }
#warning сделать потом чтобы с лобби строка эта приходила. так наверное будет правильнее чем дублировать в разных конфигах
        public string SqlConnectString;

        public int DefaultMapGearScore = 0;
		public string DefaultLevelName = "zone01";
		public int PingTimeSec = 20;
		public int StopEmptyServerAfterTimeSec = 300; //у тракториста
		public bool EnableExternalLogs = false;
		public string ConnectionStringExternalLogs = @"Server=192.168.1.19;Connect Timeout=30;user=sa;password=qwe123#;Database=iPixelLogDB";

#if DEBUG
        public bool ConsoleLog { get; set; } = true;
#else
		public bool ConsoleLog { get; set; } = false;
#endif

		public static AppConfig Instance { get; private set; }

		public AppConfig()
		{
			Instance = this;
            ILogger.Instance.Send("AppConfig Init");
		}

		public class p2pLobbyConnectConfig
		{
			[XmlAttribute]
			public string host = string.Empty;

			[XmlAttribute]
			public int port;

			[XmlAttribute]
			public string pwd = string.Empty;

			[XmlAttribute]
			public int interval;

			[XmlAttribute]
			public int maxConnections;

			[XmlAttribute]
			public string type = string.Empty;
		}

		public class AnalyticConnect
		{
			[XmlAttribute]
			public string host = "127.0.0.1";

			[XmlAttribute]
			public int port = 64128;

			[XmlAttribute]
			public bool enable = false;

			private Uri uri = null;

			public Uri GetUri()
			{
				if (uri == null)
				{
					ILogger.Instance.Send($"AnalyticsSender PW analytic host, host {host} port = {port}");
					UriBuilder builder = new UriBuilder("http", host, port);
					uri = builder.Uri;
				}
				return uri;
			}
		}

        public class p2pSelfConnectConfig
		{
			[XmlAttribute]
			public int shiftport;

			[XmlAttribute]
			public string pwd = string.Empty;

			[XmlAttribute]
			public int maxConnections;

			[XmlAttribute]
			public string type = string.Empty;
		}

		public class uLinkStatsConfig
		{
			[XmlAttribute]
			public bool enable;

			[XmlAttribute]
			public int interval;
		}
	}
}
