﻿using System;
using System.Collections.Generic;
using System.Threading;
using Assets.Scripts.InstanceServer;
using Zenject;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.Utils;
using UtilsLib;
using Fragoria.Common.Utils.Threads;
using Node.Instance.Assets.Scripts;
using UtilsLib.Logic;
using UtilsLib.Logic.ServerStart;

namespace Assets.Scripts
{
	/// <summary>
	/// Класс, который производит старт инстанс сервера
	/// </summary>
	[System.Reflection.Obfuscation(Exclude = true, ApplyToMembers = false)]
	public class ServerStartGameManager : IUpdateGameManager
	{
		public int FPS_MAX { get; } = 50;

		private DIListManagers managers;
		private AppConfig m_appConfig;
		private InstanceCommandLineOptions m_commandLineOptions;
        private TimeProviderGameManager timeManager;
        private PathBuilder pathBuilder;

		public static string ticket = string.Empty;
		public static string levelName = string.Empty;

		[Inject]
		public void Construct(PathBuilder _pathBuilder, ILogger logger, InstanceGameManager instanceGameManager, AppConfig config,
			InstanceCommandLineOptions commandLineOptions, DIListManagers _managers)
		{
            pathBuilder = _pathBuilder;
			m_appConfig = config;
			m_commandLineOptions = commandLineOptions;
			managers = _managers;

			instanceGameManager.Quit += Quit;
        }


		public void Start()
		{
			ILogger.Instance.Send($"ServerStartGameManager.Start args {m_commandLineOptions}", ErrorLevel.info);

			ticket = m_commandLineOptions.Ticket;
			levelName = m_commandLineOptions.LevelName;

			ILogger.Instance.Send("ticket = " + ticket + " zone = " + levelName);

			FRRandom.Init();

			if (string.IsNullOrEmpty(ticket))
			{
				levelName = m_appConfig.DefaultLevelName;
				ticket = Guid.NewGuid().ToString("N") + "_" + levelName;
                ILogger.Instance.Send("ServerStartGameManager.Start", $"levelName: {levelName} ticket: {ticket}", ErrorLevel.error);
            }

			ILogger.Instance.InitLoggerParam(new Dictionary<string, string>
			{
				{ "AliasId", "emptyAliasId" }, //TODO_Deg ннадо?
				{ "GameSessionId", "emptyGameSession" }, //TODO_Deg ннадо?
				{ "TicketId", ticket },
				{ "MapName", levelName }
			});

            string url = pathBuilder.GetZoneUrl(Map.Edge.GetJsonName());
            Map.LoadJsonByUrl(url);
            ILogger.Instance.Send("ServerStartGameManager.Start Map.LoadJson success levelName: " + levelName, ErrorLevel.debug);

            timeManager = new TimeProviderGameManager();
            TimeProvider.Instance = timeManager;

			Time.Init(TimeProvider.Instance, FPS_MAX);

			managers.Start();

            ILogger.Instance.Send($"uLink.Network.status = {uLink.Network.Instance.status}");

			if (uLink.Network.Instance.status != uLink.NetworkStatus.Connected)
			{
				ILogger.Instance.Send(" StartServer Quit() by network status is not connected ticket = " + ticket, ErrorLevel.warning);
				Quit();
			}
		}

		public void Update()
		{
			timeManager.Update();
			Time.RealTimeUpdate();

			while (Time.FixedTimeCheck())
			{
				CoroutineRunner.Instance.RunCoroutines();

				managers.Update();
				managers.LateUpdate();
			}
		}

		public bool bQuit { get; private set; }

		public void Quit()
		{
			ILogger.Instance.Send("Quit() ticket = " + ticket, ErrorLevel.debug);
			bQuit = true;
		}

		public void Stop()
		{
			ILogger.Instance.Send("ServerStartGameManager.Stop(): stopping managers", string.Join(";", managers.GetManagers<BaseGameManager>()), ErrorLevel.debug);
			managers.UnloadAll();

			FRRandom.Stop();
			ILogger.Instance.Shutdown();
		}

		public override string ToString()
		{
			return "Instance";
		}
	}
}
