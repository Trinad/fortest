﻿using System.Collections.Generic;
using System.Numerics;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Utils.Common;
using UtilsLib.Logic.Enums;
using Assets.Scripts.Utils;
using Zenject;

namespace Assets.Scripts.InstanceServer.Abilities.Magician
{
	public class FireWall : Aura
	{
		public float wallLenght;
		public Vector2 rightEnd;
		public Vector2 leftEnd;
		public Vector2 lineOrientation;
		public List<MapEffect> effectList;

	    private INotifyController notifyController;
        [Inject]
	    public void Construct(INotifyController notifyController)
	    {
	        this.notifyController = notifyController;
	    }

        public override void Init()
		{
            TeamRules = false;
            effectList = new List<MapEffect>();

			int count = (int) (wallLenght / Helper.VoxelSize);
			float d = wallLenght / count; 

			ILogger.Instance.Send(string.Format("FireWall wallLenght={0} count={1} d={2}", wallLenght, count, d));

            bool existSaveZona = false;
            for (int i = 0; i <= count; i++)
			{
				var point = owner.map.UltraCorrectYPosition((rightEnd + lineOrientation * i * d).Vector2ToVector3(), false);
                if (!owner.map.IsWaterSurface(point))
                {
                    CoroutineRunner.Instance.Timer(0.2f / count * i, () =>
                    {
                        effectList.Add(owner.map.CreateFx(point, EffectType.fire_wall));
                        owner.map.AddPassability((int)(point.X / Helper.VoxelSize),(int)(point.Z / Helper.VoxelSize), 8);
                    });
                }

                //if (owner.map.IsSafeZone(point))
                //{
                //    existSaveZona = true;
                //    continue;
                //}

                //Sos.Debug("point " + point);
			}

            if (existSaveZona && owner is PersonBehaviour)
            {
                notifyController.SendShowNotificationTehNameResponse(((PersonBehaviour)owner).owner, NotifyState.CommonNotify, ServerConstants.Instance.WallInSaveZoneMessage);
            }

            base.Init();
		}

		public override void OnHit(MapTargetObject target)
		{
			if (target.IsDead)
				return;

            if (!owner.map.IsWaterSurface(target.position))
            {
                var statContainer = target.GetStatContainer();
			    if (statContainer != null && statContainer.GetStatValue(Stats.FavorOfGods) > 0)
				    return;

			    var targetpos = target.position.Vector3ToVector2();

	            if (target is MonsterBehaviour monster && monster.bMoveable)
		        {
		            Vector3 newPos;
		            if (GeometryAlgorithm.VectorMultMagnitude(rightEnd - targetpos, leftEnd - targetpos) > 0)
		                newPos = target.position -
		                         GeometryAlgorithm.RotateMinus90(lineOrientation).Vector2ToVector3()
		                             .normalized()*1;
		            else
		                newPos = target.position +
		                         GeometryAlgorithm.RotateMinus90(lineOrientation).Vector2ToVector3()
		                             .normalized()*1;

		            newPos = owner.map.UltraCorrectXYZPosition(target.position, newPos);
		            newPos = owner.map.UltraCorrectYPosition(newPos);
		            monster.PathClear();
		            monster.position = newPos;
		            monster.TrySendStartMoveResponse();
		        }


	            if (target is PersonBehaviour person && !person.bCharging && !person.bRapidRetreat)
	            {
		            Vector3 newPos;
		            if (GeometryAlgorithm.VectorMultMagnitude(person.Speed.Vector3ToVector2(), lineOrientation) > 0)
			            newPos = target.position - GeometryAlgorithm.RotateMinus90(lineOrientation)
				                     .Vector2ToVector3().normalized() * 1;
		            else
			            newPos = target.position + GeometryAlgorithm.RotateMinus90(lineOrientation)
				                     .Vector2ToVector3().normalized() * 1;

		            Vector3 direction = (newPos - target.position).normalized();
		            float knockbackTime = 0.35f;
		            person.SetSpeedOverrideFunction(knockbackTime, AbilityType.FireWall, (normalizedTime) =>
		            {
			            float smoothSpeed = GeometryAlgorithm.Lerp(1f, 0f, normalizedTime);
			            return direction * (smoothSpeed * 10f);
		            });
	            }

	            if (!owner.IsTargetAlly(target))
                {
                    base.OnHit(target);
                }
            }
        }
        
        public override void Explosion()
		{
			base.Explosion();

            foreach (var effect in effectList)
            {
                owner.map.AddPassability((int)(effect.position.X / Helper.VoxelSize), (int)(effect.position.Z / Helper.VoxelSize), -8);
                owner.map.RemoveFx(effect);
            }
		}
	}
}
