﻿using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.InstanceServer.Enums;
using System;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using Assets.Scripts.InstanceServer.Analytic;

namespace Assets.Scripts.InstanceServer
{
    public class GlobalEvents
    {
        public event Action<Map> MapCreate;
        public event Action<InstancePerson, ResourceType, int, ItemSourceType> WalletResourceChange;
        public event Action<EventXmlConfig> EventXmlConfigChange;
        public event Action<TechworksXmlConfig> TechworksXmlConfigChange;
        public event Action<LobbyLogin, InventoryItemData> GiveSkin;
        public event Action<LobbyLogin, Building> GiveBuilding;
        public event Action<LobbyLogin, Building> RemoveBuilding;
        public event Action<LobbyLogin, BuildingKit> GiveBuildingKit;
        public event Action<LobbyLogin, string> NotificationAdd;
        public event Action<InstancePerson, Entries.XmlData.AbilityData.AbilityXmlData> PersonEquippedAbility;
        public event Action<InstancePerson, AnlyticItemInfo> PersonGotItem;
        public event Action<string> EventStarted;
        public event Action<string> EventEnded;
        public event Action<InstancePerson> PersonGotEventReward;
        public event Action<InstancePerson, float, string> PersonDonat;
        public event Action<InstancePerson> PlayerConnected;
        public event Action<InstancePerson> PlayerDisconnected;
		public event Action<InstancePerson, ArenaRewardChestType> OpenChest;
		public event Action<InstancePerson> ArenaBotCreate;
		public event Action<InstancePerson, ArenaMatchType, bool, bool> ArenaGameFinish;
		public event Action<InstancePerson> TakeProgressLineReward;
        public event Action<InstancePerson> LoginEmeraldsActualised;


        public ScenarioStringCollection ScenarioString = new ScenarioStringCollection(null);

        //TODO: maximpr подумать как прокинуть эту зависимость через DI в формулы
        public static GlobalEvents Instance;

		

		public GlobalEvents()
        {
            Instance = this;
        }

        public void OnPlayerConnected(InstancePerson person)
        {
            try
            {
                PlayerConnected?.Invoke(person);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"OnPersonConnected ex={ex}", ErrorLevel.exception);
            }
        }

		public void OnPlayerDisconnected(InstancePerson person)
		{
			try
			{
				PlayerDisconnected?.Invoke(person);
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send($"OnPersonConnected ex={ex}", ErrorLevel.exception);
			}
		}

        public void OnMapCreate(Map map)
        {
	        MapCreate?.Invoke(map);
        }

        public void OnWalletResourceChange(InstancePerson person, ResourceType type, int count, ItemSourceType source)
        {
            WalletResourceChange?.Invoke(person, type, count, source);
        }

        public void OnEventXmlConfigChange(EventXmlConfig config)
        {
            EventXmlConfigChange?.Invoke(config);
        }

        public void OnTechworksXmlConfigChange(TechworksXmlConfig config)
        {
            TechworksXmlConfigChange?.Invoke(config);
        }

		public void OnGiveSkin(LobbyLogin lobbyLogin, InventoryItemData skin)
		{
			GiveSkin?.Invoke(lobbyLogin, skin);
		}

        internal void OnGiveBuilding(LobbyLogin lobbyLogin, Building building)
        {
            GiveBuilding?.Invoke(lobbyLogin, building);
        }

        internal void OnRemoveBuilding(LobbyLogin lobbyLogin, Building building)
        {
            RemoveBuilding?.Invoke(lobbyLogin, building);
        }

		internal void OnGiveBuildingKit(LobbyLogin lobbyLogin, BuildingKit buildingKit)
		{
			GiveBuildingKit?.Invoke(lobbyLogin, buildingKit);
		}

		public void OnNotificationAdd(LobbyLogin lobbyLogin, string notification)
		{
			NotificationAdd?.Invoke(lobbyLogin, notification);
		}

        internal void OnPersonEquippedAbility(InstancePerson person, Entries.XmlData.AbilityData.AbilityXmlData abilityXmlData)
        {
            try
            {
                PersonEquippedAbility?.Invoke(person, abilityXmlData);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"PersonEquippedAbility ex={ex}", ErrorLevel.exception);
            }
        }

		internal void OnPersonGotItem(InstancePerson person, AnlyticItemInfo anlyticItemInfo)
        {
            try
            {
                PersonGotItem?.Invoke(person, anlyticItemInfo);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"PersonGotItem ex={ex}", ErrorLevel.exception);
            }
        }

        internal void OnEventStarted(string eventName)
        {
            try
            {
                EventStarted?.Invoke(eventName);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"OnEventStarted ex={ex}", ErrorLevel.exception);
            }
        }

        internal void OnEventEnded(string eventName)
        {
            try
            {
                EventEnded?.Invoke(eventName);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"OnEventEnded ex={ex}", ErrorLevel.exception);
            }
        }

        internal void OnPersonGotEventReward(InstancePerson person)
        {
            try
            {
                PersonGotEventReward?.Invoke(person);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"OnPersonGotEvetReward ex={ex}", ErrorLevel.exception);
            }
        }

        internal void OnPersonDonat(InstancePerson person, float price, string name)
        {
            try
            {
                PersonDonat?.Invoke(person, price, name);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"OnPersonEventDonater ex={ex}", ErrorLevel.exception);
            }
        }

		internal void OnTakeProgressLineReward(InstancePerson person)
		{
			try
			{
				TakeProgressLineReward?.Invoke(person);
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send($"OnTakeProgressLineReward ex={ex.ToString()}", ErrorLevel.exception);
			}
		}

		internal void OnOpenChest(InstancePerson person, ArenaRewardChestType chestType)
		{
			try
			{
				OpenChest?.Invoke(person, chestType);
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send($"OnOpenChest ex={ex.ToString()}", ErrorLevel.exception);
			}
		}

		public void OnArenaBotCreate(InstancePerson instancePerson)
		{
			ArenaBotCreate?.Invoke(instancePerson);
		}

		internal void OnArenaGameFinish(InstancePerson instancePerson, ArenaMatchType matchType, bool bWin, bool bLeave)
		{
			try
			{
				ArenaGameFinish?.Invoke(instancePerson, matchType, bWin, bLeave);
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send($"OnArenaGameFinish ex={ex.ToString()}", ErrorLevel.exception);
			}
		}
        internal void OnLoginEmeraldsActualised(InstancePerson instancePerson)
        {
            try
            {
                LoginEmeraldsActualised?.Invoke(instancePerson);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"OnPersonEventDonater ex={ex.ToString()}", ErrorLevel.exception);
            }
        }

		public event Action<LobbyLogin> LoginRename;
		public void OnLoginRename(LobbyLogin login)
		{
			LoginRename?.Invoke(login);
		}

		public event Action<LobbyLogin> LoginAvatar;
		public void OnLoginAvatar(LobbyLogin login)
		{
			LoginAvatar?.Invoke(login);
		}
    }
}
