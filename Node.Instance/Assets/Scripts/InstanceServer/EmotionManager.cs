﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InstanceServer.Analytic;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using uLink;
using UtilsLib;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer
{
    public class EmotionManager : BaseGameManager
    {
		public Action<InstancePerson> SendRemoveSmileFrameResponse;

		ServerConstants serverConstants;
        ShopManager shopManager;

        public List<InstancePerson> UsingEmotionsPerson { get; private set; }
        public EmotionSettings EmotionSettings { get; private set; }

        public EmotionManager(ServerConstants serverConstants, EmotionSettings emotionSettings, ShopManager shopManager)
        {
            this.EmotionSettings = emotionSettings;
            
            this. shopManager = shopManager;
            this.serverConstants = serverConstants;
            
            UsingEmotionsPerson = new List<InstancePerson>();
        }

        public void CreatEmotionPerson(InstancePerson instancePerson, List<Emotion> availableEmotions,
            List<Emotion> availableEmotionsForBuy)
        {
            instancePerson.CreatEmotionPerson(new EmotionPerson());
            Load(instancePerson, availableEmotions, availableEmotionsForBuy);
            RefreshEmotions(instancePerson);
        }

        public void Load(InstancePerson instancePerson, List<Emotion> availableEmotions, List<Emotion> forBuy)
        {
            instancePerson.EmotionPerson.Load(availableEmotions, forBuy);

            if (!availableEmotions.Exists(e => e.Group == EmotionGroup.Free))
            {
                AddEmotionStarterPack(instancePerson.EmotionPerson, EmotionGroup.Free);
            }

            if (!availableEmotions.Exists(e => e.Group == EmotionGroup.Misc))
            {
                AddEmotionStarterPack(instancePerson.EmotionPerson, EmotionGroup.Misc);
            }

            if (forBuy.Count == 0)
            {
                instancePerson.EmotionPerson.SetLastRefreshTime(TimeProvider.UTCNow); //timeProvider.TimeUTCNow
                RefreshEmotions(instancePerson);
            }
        }

		public void RefreshEmotions(InstancePerson instancePerson)
        {
            if (TimeProvider.UTCNow >= instancePerson.EmotionPerson.RefreshEmotionsAfter)
            {
                var newEmotionsForBuy = new List<Emotion>();

                for (int i = 0; i < serverConstants.CountForBuy; i++)
                {
                    var newRandomEmotion = GetRandomEmotion(instancePerson, instancePerson.EmotionPerson.AvailableEmotions, newEmotionsForBuy);
                    if (newRandomEmotion != null)
                        newEmotionsForBuy.Add(newRandomEmotion);
                    else
                    {
                        ILogger.Instance.Send("RefreshEmotions - new emotions are ended", ErrorLevel.warning);
                    }
                }

                instancePerson.EmotionPerson.UpdateEmotionsForBuy(newEmotionsForBuy);
                instancePerson.EmotionPerson.SetLastRefreshTime(TimeProvider.UTCNow.AddHours(serverConstants.RefreshTimeHours));// = timeProvider.TimeUTCNow.AddHours(RefreshTimeHours);
            }
        }
        Emotion GetRandomEmotion(InstancePerson instancePerson, List<Emotion> ignorePersonEmotions, List<Emotion> ignorePersonEmotionsForBuy)
        {
            var availableNewEmoticons = GetAllEmotionsForBuyByGroup(instancePerson, EmotionGroup.Pay);
            Emotion newEmotion = null;
            var count = availableNewEmoticons.Count;
            int index = FRRandom.Next(count);
            for (int j = 0; j < count; j++)
            {
                var newElementIndex = (index + j) % count;
                newEmotion = availableNewEmoticons[newElementIndex];

                if (!ignorePersonEmotions.Exists(e => e.Id == newEmotion.Id) && !ignorePersonEmotionsForBuy.Exists(e => e.Id == newEmotion.Id))
                {
                    return newEmotion;
                }
            }

            return null;
        }

        public override void Update()
        {
            CheckUseEmotionDuration();
            RemoveUsedEmotions();
        }

        private void CheckUseEmotionDuration()
        {
            foreach (var instancePerson in UsingEmotionsPerson)
            {
                if (instancePerson.EmotionPerson != null && instancePerson.EmotionPerson.InUse != null && TimeProvider.UTCNow > instancePerson.EmotionPerson.UseDuration)//DateTime.UtcNow
                {
					SendRemoveSmileFrameResponse?.Invoke(instancePerson);
                    instancePerson.EmotionPerson.ResetInUse();
                }
            }
        }
        private void RemoveUsedEmotions()
        {
            UsingEmotionsPerson.RemoveAll(instancePerson => instancePerson.EmotionPerson.InUse == null || instancePerson.player.Equals(NetworkPlayer.unassigned));
        }

		/// <summary>
		/// получаем эмотиконов доступных для покупки определенной группы
		/// </summary>
		/// <param name="instancePerson"></param>
		/// <param name="emotionGroup"></param>
		/// <returns></returns>
		public List<Emotion> GetAllEmotionsForBuyByGroup(InstancePerson instancePerson, EmotionGroup emotionGroup)
        {
            var byGroup = GetAllEmotionsForBuy(instancePerson).FindAll(e => e.Group == emotionGroup);
            return byGroup.Where(emotion => !instancePerson.EmotionPerson.AvailableEmotions.Exists(e => e.Id == emotion.Id)).ToList();
        }

        /// <summary>
        /// получаем эмотиконов доступных для покупки
        /// </summary>
        /// <param name="instancePerson"></param>
        /// <returns></returns>
        public List<Emotion> GetAllEmotionsForBuy(InstancePerson instancePerson)
        {
			try
			{
				var emotionShopItems = shopManager.GetShopItems(instancePerson.login, 6);
				if (emotionShopItems == null)
					return new List<Emotion>();
				var result = new List<Emotion>();
				foreach (var emotionXmlData in EmotionSettings.EmotionList)
				{
					var productData = emotionShopItems.Find(e => e.BaseId == emotionXmlData.Id);
					if (productData == null)// || instancePerson.EmotionPerson.AvailableEmotions.Exists(e => e.Id == emotionXmlData.Id))
						continue;

					var newData = new Emotion(emotionXmlData.Id, emotionXmlData.Group, productData);

					result.Add(newData);
				}
				return result;
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send("GetAllEmotionsForBuy " + ex, ErrorLevel.exception);
				return new List<Emotion>();
			}
        }

		/// <summary>
		/// Используем эмотикон (появляется на serverConstants.TimeInUseSec секунд над персом)
		/// </summary>
		/// <param name="instancePerson"></param>
		/// <param name="emotionId"></param>
		public bool UseEmotion(InstancePerson instancePerson, int emotionId)
        {
			var emotionPerson = instancePerson.EmotionPerson;
			var duration = TimeProvider.UTCNow.AddSeconds(serverConstants.TimeInUseSec);

			var emotion = emotionPerson.AvailableEmotions.Find(e => e.Id == emotionId);

			if(emotion == null)
			{
				ILogger.Instance.Send("UseEmotion emotion == null",
					string.Format("emotionId: {0}, personId: {1}", emotionId, instancePerson.personId), ErrorLevel.error);
				return false;
			}

			emotionPerson.UpdateUseEmotion(emotion, duration);

			var useEmotionPerson = UsingEmotionsPerson.Find(p => p.personId == instancePerson.personId);
            if (useEmotionPerson != null)
            {
                UsingEmotionsPerson.Remove(instancePerson);
            }

            UsingEmotionsPerson.Add(instancePerson);

			return true;
        }

        /// <summary>
        /// получаем список список эмотиконов игрока для отправки на клиент
        /// </summary>
        /// <returns></returns>
        public List<EmotionData> GetRPCAvailableEmotions(InstancePerson instancePerson)
        {
            return instancePerson.EmotionPerson.AvailableEmotions.Where(e => e.Group != EmotionGroup.Misc).Select(availableEmotion => (EmotionData)availableEmotion).ToList();
        }

        /// <summary>
        /// получаем список только купленных
        /// </summary>
        /// <returns></returns>
        public List<Emotion> GetBoughtAvailableEmotions(InstancePerson instancePerson)
        {
            return instancePerson.EmotionPerson.AvailableEmotions.Where(availableEmotion => availableEmotion.Group == EmotionGroup.Pay).ToList();
        }

        /// <summary>
        /// получаем список доступных для покупки смайлов (их 3 задаются рандомно) в формат для передачи на клиент
        /// </summary>
        /// <param name="instancePerson"></param>
        /// <returns></returns>
        public List<EmotionData> GetRpcEmotionsForBuy(InstancePerson instancePerson)
        {
            var selected = instancePerson.EmotionPerson.AvailableEmotionsForBuy.Select(e => (EmotionData)e);
            var toList = selected.ToList();

            return toList;
        }
        
        public void AddEmotionStarterPack(EmotionPerson emotionPerson, EmotionGroup emotionGroup)
        {
            if(emotionGroup != EmotionGroup.Pay)
                emotionPerson.AvailableEmotions.RemoveAll(s => s.Group == emotionGroup);

            foreach (var emotionXmlData in EmotionSettings.EmotionList)
            {
                if (emotionXmlData.Group == emotionGroup)
                {
                    var newEmotion = new Emotion(emotionXmlData.Id, emotionXmlData.Group, emotionXmlData.SlotId);
                    emotionPerson.AvailableEmotions.Add(newEmotion);
                }
            }
        }

        public void AddNewEmotion(InstancePerson instancePerson, int id)
        {
            var newEmotion = instancePerson.EmotionPerson.AvailableEmotionsForBuy.Find(e => e.Id == id);
            if (newEmotion != null && !instancePerson.EmotionPerson.AvailableEmotions.Exists(e => e.Id == id))
            {
                newEmotion.UpdateData(-1, true);
                instancePerson.EmotionPerson.AvailableEmotions.Add(newEmotion);
                UpdateEmotionsForBuy(instancePerson, newEmotion);
            }
            else
            {
                ILogger.Instance.Send("AddNewEmotion newEmotion = null id: " + id, ErrorLevel.error);
            }
        }

        void UpdateEmotionsForBuy(InstancePerson instancePerson, Emotion newEmotion)
        {
            instancePerson.EmotionPerson.AvailableEmotionsForBuy.Remove(newEmotion);

            var newForBuy = GetRandomEmotion(instancePerson, instancePerson.EmotionPerson.AvailableEmotions, instancePerson.EmotionPerson.AvailableEmotionsForBuy);
            if (newForBuy != null)
            {
                newForBuy.SetTime(TimeProvider.UTCNow.AddSeconds(serverConstants.RestoreTimeSec));
                instancePerson.EmotionPerson.AvailableEmotionsForBuy.Add(newForBuy);
            }
        }

		public void CheatDeleteAllEmitions(InstancePerson instancePerson)
		{
			instancePerson.EmotionPerson.AvailableEmotions.Clear();
			AddEmotionStarterPack(instancePerson.EmotionPerson, EmotionGroup.Free);
			AddEmotionStarterPack(instancePerson.EmotionPerson, EmotionGroup.Misc);
		}
    }
}
