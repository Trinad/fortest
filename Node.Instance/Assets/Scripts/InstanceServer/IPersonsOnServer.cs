﻿using System.Collections.Generic;

namespace Assets.Scripts.InstanceServer
{
	public interface IPersonsOnServer
	{
		void AddReplacePerson(InstancePerson person);
		InstancePerson GetPerson(uLink.NetworkPlayer networkPlayer);
		InstancePerson getPersonByLoginId(int loginId);
		InstancePerson GetPersonByPersonId(int personId);
		IEnumerable<InstancePerson> GetAllPersons();
		void ClearPersons();
	}
}
