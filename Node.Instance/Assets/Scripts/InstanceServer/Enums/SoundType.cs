﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum SoundType
	{
		Hit,
		Voice,
		Pain,
		Destruction,
	}

}
