﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum CharacterType : sbyte
	{
		None = -1,
		Person = 0,
		NPC = 1,
		Mob = 2,
		Res = 3,
		Wall = 4,
	}
}
