﻿namespace Assets.Scripts.InstanceServer.Enums
{
    public enum TutorialEvents
    {
        //----------------------Это тестовые события, их моэно удалить
        OpenSkillsMenuClick,
        OnTalentClick,
        OnTalentsTabClick,
        //----------------------
        PressAttackButton,
        PressAbilityButton,

        UserConfirmTutorial,
        UserDeclineTutorial,

        OnNextTutorialStepClick,
        OnOpenLocationForm,

        //Step 2
        OnScreenTouchedCharacterControlTutorialStep,
        PlayerStupidAndDontMove,
        PlayerMovesMoreThanTwoSeconds,

        //Для 4го шага обучения
        AllDead,

        // Для 6 шага
        CloseSetupCameraForm,

        //Для 7го шага
        CharacterDead,

        //Для 8го шага обучения
        OnRewardFormShowed,
        OnRewardChesthowed,
        OnRewardChestClick,
        UserDontClickArenaResultButtonLongTime,
        OnOpenArenaResultClick,

        //Step9
        OnUserNameAcceptedByServer,

        //Step11
        OnArenaButtonClick,

        //Step12
        OnArenaSelectModeButtonClick,
        OnArenaStartFightButtonClick,

        //Step13
        //OnRewardFormShowed, переиспользуется из шага 8
        //OnOpenArenaResultClick, переиспользуется из шага 8
        OnOpenArenaResultForm,
        OnGoToLobbyButtonClick,

        BreakTutorial,
        MoveJoystickInProgress,
        DeathFormResurrectResponse,

        OnAllRewardsOpened,
        OnRewardTitleTextSetted,

        OnWaitPlayerMessageShowed,
        OnStartMatchingMessageShowed,

        // Step 15
        ShowReward,
        HideReward,

        OnOptionsMenuOpen,

        //События для показа подсказки, подсвечивающей кнопку выбора режима боя
        OnPersonalSpaceFormShow,
        OnPersonalSpaceFormHide,
        OnSelectMapFormShow,
        OnSelectMapFormHide,
        OnGoToBattleButtonClick,
        OnBackButtonClick,
        OnBuildingsPanelShow,
        OnBuildingsPanelHide,
        OnAnyFormHide,
        OnAnyFormShow
    };
}
