﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum SpecialActionSectionType
	{
		NewbieSection = 0, //Набор новичка
		RareEquipment = 1, //Редкая экипировка
		PotionSection = 2, //Лучшие зелья
		EvolutionSection = 3, //Развитие персонажа
		UpgradeEquipment = 4, //Улучшение экипировки
		DungeonConquerer = 5, //Покорителю подземелий
	}
}
