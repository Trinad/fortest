﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum TutorialFrameType
	{
		None = -1,
		Arrow = 0,
		Smiley = 1,
	}
}
