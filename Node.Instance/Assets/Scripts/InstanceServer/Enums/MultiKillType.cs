﻿namespace Assets.Scripts.InstanceServer.Enums
{
    public enum MultiKillType
    {
        DoubleKill = 2,
        TripleKill = 3,
        MassKill = 4,
        PentaKill = 5,
        MegaKill = 10,
    }
}