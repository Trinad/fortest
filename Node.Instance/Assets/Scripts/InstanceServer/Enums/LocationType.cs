﻿namespace Assets.Scripts.InstanceServer.Enums
{
    public enum LocationType
    {
        None,
        Tutorial,
        Camp,
        Location,
        Dungeon,
        Fort,
        EnemyFort,
        AllyFort,
        Arena,
        CTF,
        BattlegroundWorldBossArena,
        GuildRoom,
        ArenaCoopWaves,
        TeamDM,
        TeamCTF
    }
}
