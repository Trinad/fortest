﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.InstanceServer.Enums
{
    [Flags]
    public enum TargetIcon
    {
        Aim = 1,
        Lock = 2,
        Quest = 4,
        Timer = 8,
    }
}
