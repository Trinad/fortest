﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum UpgradeItemMode
	{
		Normal,
		Magic,
		Multiple,
	}
}
