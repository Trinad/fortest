﻿namespace Assets.Scripts.InstanceServer.Enums
{
    public enum BuyItemResponseType : byte
    {
        ShopItem = 0,
        SpecialAction = 1,
    }
}