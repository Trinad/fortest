﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum MoveType : byte
	{
		Walk = 0,
		Swim = 1,
		Fly = 2,
	}
}
