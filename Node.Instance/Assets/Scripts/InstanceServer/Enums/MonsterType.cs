﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.InstanceServer.Enums
{
    public enum MonsterType : sbyte
    {
        None = -1,
        Simple = 0,
        Champion = 1,
        Boss = 2,
        Animal = 3,
        Golem = 4,
		Tower = 5,
		Trap = 6,
		Gate = 7,
        NotTargeted = 8,
        SafeZone = 9
    }
}
