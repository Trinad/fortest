﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum FriendFormSectionType
	{
		Players = 0,
		Friends = 1,
		Guild = 2,
	}
}
