﻿namespace Assets.Scripts.InstanceServer.Enums
{
    public enum TextFrameType : byte
    {
        Damage,
        Healing,
        Crit,
        Miss,
        Potion,
        Ability,
        AbilityInterruption,
        Immunity,
        SafeZone,
        Loot,
        Buff,

        StunImmunity,
        BleedingImmunity,
        SlowdownImmunity,
        SilenceImmunity,
        BurningImmunity,
        CurseImmunity,
        PhysicalWeaknessImmunity,
        MagicalWeaknessImmunity,
        PoisonImmunity,
        FrozenImmunity,

        StunResist,
        BleedingResist,
        SlowdownResist,
        SilenceResist,
        BurningResist,
        CurseResist,
        PhysicalWeaknessResist,
        MagicalWeaknessResist,
        PoisonResist,
        FrozenResist,
    }
}
