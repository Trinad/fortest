﻿namespace Assets.Scripts.InstanceServer.Enums
{
    //следить чтобы номера совпадали с items.xml
    public class ItemsHelper
    {
        //None = -1,

        //RandomPotion = 1,

        const string WarriorSword = "WarriorSword";//9,
                                                   //      WarriorShield = 10,
                                                   //      MagicianRight = 11,
                                                   //      MagicianLeft = 12,
                                                   //      ArcherBow = 13,
                                                   //      ArcherArrow = 14,

        //      WarriorArmor = 15,
        //      MagicianArmor = 16,
        //      ArcherArmor = 17,

        //WarriorBelt = 20,
        //MagicianBelt = 21,
        //ArcherBelt = 22,
        //GunnerBelt = 23,

        //      public const string WarriorAmulet = 24,
        //      public const string WarriorRing = 25,
        //      public const string WarriorArtifact = 26,
        //      public const string SparkCube = 27,

        //      public const string Gold3 = 42,
        //      public const string Gold4 = 43,

        //      public const string WarriorBoots = 66,
        //      public const string MagicianBoots = 67,
        //      public const string ArcherBoots = 68,

        //      public const string WarriorBracers = 69,
        //      public const string MagicianBracers = 70,
        //      public const string ArcherBracers = 71,

        //      public const string WarriorHelmet = 72,
        //      public const string MagicianHelmet = 73,
        //      public const string ArcherHelmet = 74,

        //      public const string WarriorShoulders = 75,
        //      public const string MagicianShoulders = 76,
        //      public const string ArcherShoulders = 77,
        //      public const string ArcherAmulet = 78,
        //      public const string ArcherRing = 79,
        //      public const string ArcherArtifact = 80,

        //      public const string MagicianAmulet = 81,
        //      public const string MagicianRing = 82,
        //      public const string MagicianArtifact = 83,

        //      public const string WarriorTutorialSword = 84,
        //      public const string WarriorTutorialShield = 85,
        //      public const string WarriorTutorialArmor = 86,
        //      public const string WarriorTutorialHelmet = 87,
        //      public const string WarriorTutorialShoulders = 88,
        //      public const string WarriorTutorialBracers = 89,
        //      public const string WarriorTutorialBoots = 90,
        //      public const string WarriorTutorialAmulet = 91,
        //      public const string WarriorTutorialRing = 92,
        //      public const string WarriorTutorialArtifact = 93,

        //      public const string GunnerGun = 97,
        //      public const string GunnerGrenade = 98,
        //      public const string GunnerArmor = 99,
        //      public const string GunnerAmulet = 100,
        //      public const string GunnerRing = 101,
        //      public const string GunnerArtifact = 102,
        //      public const string GunnerBoots = 103,
        //      public const string GunnerBracers = 104,
        //      public const string GunnerHelmet = 105,
        //      public const string GunnerShoulders = 106,

        public const string HealingPotion1 = "107";
        public const string HealingPotion2 = "108";
        public const string HealingPotion3 = "109";
        public const string HealingPotion4 = "110";
        public const string HealingPotion5 = "111";
        public const string ManaPotion1 = "112";
        public const string ManaPotion2 = "113";
        public const string ManaPotion3 = "114";
        public const string ManaPotion4 = "115";
        public const string ManaPotion5 = "116";
        public const string RejuvenationPotion1 = "117";
        public const string RejuvenationPotion2 = "118";
        public const string RejuvenationPotion3 = "119";
        public const string RejuvenationPotion4 = "120";
        public const string RejuvenationPotion5 = "121";
        public const string PanaceaPotion = "122";
        public const string AntidotePotion = "123";
        public const string HolyWater = "124";
        public const string ConcentrationPotion = "125";
        public const string AwakeningPotion = "126";
        public const string BerserkPotion = "127";
        public const string RagePotion = "128";
        public const string MarksmanshipPotion = "129";
        public const string EagleEyePotion = "130";
        public const string AccuracyPotion = "131";
        public const string BurningBloodPotion = "132";
        public const string StoneSkinPotion = "133";
        public const string MagicTrickPotion = "134";
        public const string MeditationPotion = "135";
        public const string GodSparkPotion = "136";
        public const string MaxSPPotion = "137";
        public const string MaxHPPotion = "138";
        public const string SummoningPotion = "139";
        public const string TeleportPotion = "140";
        public const string WarriorSkin = "141";
        public const string MagicianSkin = "142";
        public const string ArcherSkin = "143";
        public const string GunnerSkin = "144";
    }
}
