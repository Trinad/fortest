﻿namespace Assets.Scripts.InstanceServer.Enums
{
	//Названия объектов на локации
	//Локализация group="21"
	public enum MapObjectTitle : sbyte
	{
		None = -1,
		Portal = 0, //Портал. Вперёд к приключениям!
		LightHouse = 1, //Портал. В свой форт
		NpcTrader = 2, //Торговая лавка
		NpcQuest = 3,//Распорядитель заданий
		Crafthouse = 4, //Кузня
		Warehouse = 5, //Склад
		Crystal = 6, //Кристалл
		Altar = 7, //Алтарь
		Laboratory = 8, //Лаборатория
		MapBuildPlace = 9, //Защита. Оборонительные сооружения
		ReturnPortal = 10, //Портал. Возвращение в лагерь
		StatueBlessing = 11, //Статуя Благословения
		GuildMaster = 12,//Гильдмастер
		Obelisk = 13, //Обелиск славы
		GuildPortal = 14, //Портал. В гильдзамок
		WorldBoss = 15, //Козёл
		Battleground = 16, //Батлграунд
		Scroll = 17, //Свиток
        NpcTraderPotion = 18, // Торговец зельями
        PortalOfAvenger = 19, //Портал мстителя
        MoodStatue = 20, //Статуя настроения
        ArenaOfValor = 21, //Арена доблести
        EvolutionStatue = 22, //Статуя повышения уровня
        ChangeNameBook = 23, // Алтарь для переименования
    }
}
