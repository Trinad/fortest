﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum MinimapItemType
	{
		None = -1,
		Dungeon = 0,
		Quest = 1,
		ForFuture = 2,
		FamousSpot = 3,
		Portal = 4,
		PersonalLocFree = 5,
		PersonalLocEnemy = 5, // 6 Убрали, тк Арсений не смог вписать в стилистику
		PersonalLocAlly = 5, // 7
		PersonalLocPlayer = 5, // 8
		Trader = 9,
        BuffEmpty = 10, // пустая подставка
        HealthBuff = 11, // баф лечения
        Person = 12,
        GuildMember = 13,
        GreenTeamMember = GuildMember,
        RedTeamMember = 15,
        BlueTeamMember = 14,
        PlayerWithFlag = 16,
        PlayerWithBlueFlag = 17,
        Team1Flag = 18,
        Team2Flag = 19,

	    ConcentrationBuff = 20, // зелье концентрации
        BurningBloodBuff = 21, // кровавой ярости
	    RageBuff = 22, //баф ярости
        MarksmanshipAndAccuracyBuff = 23, // снайперской точности и точности
        StoneSkinBuff = 24, // каменной кожи
        MagicTrickBuff = 25, // магической завесы

		AvengersPortal = 26, // портал мстителей
		Crafthouse = 27, // кузница
		Altar = 28, // алтарь
		Crystal = 29, // кристал
		Warehouse = 30, // склад
		Gate = 31, // ворота
		Laboratory = 32, // лаборатория
		PortalOwner = 33, // портал. в лагерь
		PortalEnemy = 34, // портал нападающих

        RenamePerson = 35, //переименование персонажа
        BlessingStatue = 36, // статуя благословления
        ObeliskOfFame = 37, // обелиск славы
        Arena = 38, // арена
        MagicMask = 40, // статуя смайлов

        KeyKeeperYellow = 44,
        KeyKeeperGreen = 45,
        KeyKeeperBlue = 46,

        KeyGateGreen = 47,
        KeyGateYellow = 48,
        KeyGateBlue = 49,

	    SafeZone = 50,

		ChestGreen = 51,
		ChestYellow = 52,
		ChestBlue = 53,

		LevelCapStatue = 54,
        Guildmaster = 55, // распорядитель гильдий
        SafeWithEmeralds = 56, //сейф-копилка (изумрудик)

        Monster = 57,
		WorldBoss = 58,
		BattleGround = 59,
		Corpse = 60, // труп персонажа
		PanaceaBuff = 61,

        SpeedUp = 69, //ускорение

        BrawlTower  = 70,// - синяя башня
        RedTower = 71,// - красная башня
        BrawlTurret = 72,// - синяя турель
        RedTurret = 73,// - красная турель
    }
}
