﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum MonsterState
	{
		BackToPride,//бег назад в прайд
		FindTarget, MoveToPoint,// бродит, ничем не занят
		MoveToTarget,//бег к цели
		Retreat,//отступление от цели
		StayAndHit,
		MoveToBuff,
		LavaEscape,
	}
}
