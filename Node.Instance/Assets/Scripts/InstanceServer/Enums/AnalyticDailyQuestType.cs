﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum AnalyticDailyQuestType : sbyte
	{
		dq_items,
		dq_resources,
		dq_pve,
		dq_pvp,
		dq_modes,
		dq_classes
	}
}
