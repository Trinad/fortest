﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum EdgeLocationState
	{
		Disable,
		Lock,
		Unlock,
		Hidden,
		Buoy,
		Ship,
		Dungeon,
		Arena,
	}
}
