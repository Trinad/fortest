﻿namespace Assets.Scripts.InstanceServer.Enums
{
	//Актуальные места на локации
	public enum ActualPlaceType : short
	{
		None = -1,

		//Для города
		NpcQuest = 0, //Распорядитель заданий
		NpcTrader = 1, //Торговая лавка. Продажа редких товаров
		Portal = 2, //Портал. Вперёд к приключениям!
		LightHouse = 3, //Портал. В свой форт

		//Для своего форта
		Warehouse = 4, //Склад. Хранение вещей
		Altar = 5, //Алтарь. Особенности персонажа
		Laboratory = 6, //Лаборатория. Усиление своего форта
		Crystal = 7, //Кристалл. Навыки
		Crafthouse = 8, //Кузня. Улучшение экипировки
		MapBuildPlace = 9, //Защита. Оборонительные сооружения
		ReturnPortal = 10, //Портал. Возвращение в лагерь

		StatueBlessing = 11, //Статуя Благословения. Позволяет передать Искры в свой форт
		Guildmaster = 12, //Распорядитель гильдий
		RenamePerson = 13, //Переименовать персонажа
		Obelisk = 14, //Обелиск славы
		WorldBoss = 15, //Подземелье Гигантов
		BattleGround = 16, //Обелиск грядущих сражений
		Scroll = 17, //Свиток
		GuildPortal = 18, //Портал. В гильдзамок
		PotionTrader = 19, //Торговец зальями
        MagicMask = 20,
		Arena = 21, // Арена доблести
		LevelCapStatue = 22,
		ResourcesTrader = 23,

        //Нету на клиенте
        MapLoot = 1011, //Лут
		ToPoint = 1012, //К точке, нужно указать Pos
		Monster = 1013, //К монстру, нужно указать Data
		MapObject = 1014, //К объекту, нужно указать Data
		GameMode = 1015, //К активному объекту, нужно указать Data
		TraderById = 1016, //К торговцу, нужно указать ид торговца в Data
		DefendBuilding = 1017, //К защитным постройкам
	}

	//Иконки для актуальных мест
	public enum ActualPlaceIcon : sbyte
	{
		None = -1,
		NpcQuest = 0, //Распорядитель заданий
		NpcTrader = 1, //Торговая лавка. Продажа редких товаров
		Portal = 2, //Портал. Вперёд к приключениям!
		LightHouse = 3, //Портал. В свой форт
		Warehouse = 4, //Склад. Хранение вещей
		Altar = 5, //Алтарь. Особенности персонажа
		Laboratory = 6, //Лаборатория. Усиление своего форта
		Crystal = 7, //Кристалл. Навыки
		Crafthouse = 8, //Кузня. Улучшение экипировки
		MapBuildPlace = 9, //Защита. Оборонительные сооружения
		ReturnPortal = 10, //Портал. Возвращение в лагерь
		StatueBlessing = 11, //Статуя Благословения. Позволяет передать Искры в свой форт
		Guildmaster = 12, //Распорядитель гильдий
		RenamePerson = 13, //Переименовать персонажа
		Obelisk = 14, //Обелиск славы
		WorldBoss = 15, //Подземелье Гигантов
		BattleGround = 16, //Батлграунд
		Scroll = 17, //Свиток
		GuildPortal = 18, //Портал. В гильдзамок
		Arena = 21, // Арена доблести
        LevelCapStatue = 22,
        Dangeon = 23,
    }

	//Локализация group="50"
	public enum ActualPlaceTitle : sbyte
	{
		None = -1,
		NpcQuest = 0, //Распорядитель заданий
		NpcTrader = 1, //Торговая лавка. Продажа редких товаров
		Portal = 2, //Портал. Вперёд к приключениям!
		LightHouse = 3, //Портал. В свой форт
		Warehouse = 4, //Склад. Хранение вещей
		Altar = 5, //Алтарь. Особенности персонажа
		Laboratory = 6, //Лаборатория. Усиление своего форта
		Crystal = 7, //Кристалл. Навыки
		Crafthouse = 8, //Кузня. Улучшение экипировки
		MapBuildPlace = 9, //Защита. Оборонительные сооружения
		ReturnPortal = 10, //Портал. Возвращение в лагерь
		StatueBlessing = 11, //Статуя Благословения. Позволяет передать Искры в свой форт
		Guildmaster = 12, //Распорядитель гильдий
		Obelisk = 13, //Обелиск славы
		GuildPortal = 14, //Портал. В гильдзамок
		WorldBoss = 15, //Подземелье Гигантов
		BattleGround = 16, //Батлграунд
		Scroll = 17, //Свиток
		PotionTrader = 18, //Торговец зальями
		Arena = 21, // Арена доблести
        LevelCapStatue = 22,
        Rename = 23, // Смена имени персонажа
		RobotPromouter = 24,
		ResourcesTrader = 25,
	}
}
