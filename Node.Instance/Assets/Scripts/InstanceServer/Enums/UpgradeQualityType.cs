﻿namespace Assets.Scripts.InstanceServer.Enums
{
	internal enum UpgradeQualityType
	{
		None,
		Damage,
		Protection,
		Other
	}
}
