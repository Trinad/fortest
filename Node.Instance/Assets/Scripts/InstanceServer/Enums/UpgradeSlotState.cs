﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum UpgradeSlotState
	{
		LOCK,
		NONBUYED,
		EMPTY,
		INPROGRESS,
		COMPLITED
	}
}
