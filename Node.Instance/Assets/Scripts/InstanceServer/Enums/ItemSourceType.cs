﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum ItemSourceType
	{
		None = 0,
		AnyChest = 1,
        BoughtForEmeralds = 2,
        DailyReward = 3,
        MatchReward = 4,
        AccountReward = 5,
        PurchaseShop = 6,
        Event = 7,
        Exchange = 8,
        ItemSell = 9,
        LevelUp = 10,
        Unknown = 11,
        QuestReward = 12,
	    Cheat = 13,
    }

    public enum ExpandedItemSourceType
    {
        None = 0,
        GoldChest = 4,
        SilverTimedChest = 5,
        EventChest = 6,
        QuestChest = 7,
        ProgressLine = 8,
    }
}
