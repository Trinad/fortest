﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.InstanceServer.Enums
{
	public enum DeathReasonType
	{
		None,
		Water,
		Lava,
		Monster,
		Player,
		Building, // любое строение, пока что турель или башня
		Acid,
		Stone,
		Tornado,
		Turret,
		Tower,
		Trap,
		Plant,
		Pit,
		Boss,
	}
}
