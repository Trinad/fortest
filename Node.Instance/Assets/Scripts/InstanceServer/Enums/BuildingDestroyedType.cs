﻿namespace Assets.Scripts.InstanceServer.Enums
{
	//Статус здания
	public enum BuildingDestroyedType
	{
		Normal, //Норм
		Damaged, //Повреждено
		Destroyed, //Уничтожено
		Reparing, //Чинится
	}
}
