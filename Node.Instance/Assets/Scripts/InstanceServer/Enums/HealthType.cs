﻿namespace Assets.Scripts.InstanceServer.Enums
{
    public enum HealthType : byte
    {
        Normal,
        BasedOnHits
    }
}
