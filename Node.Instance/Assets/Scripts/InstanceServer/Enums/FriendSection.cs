﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum FriendSection
	{
		Players = 0,
		Guild = 1,
		Friends = 2,
		Gifts = 3,
		Count
	}
}
