﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum LocalizationLevelName
	{
		None = -1,
		Location1 = 0,
		Location2 = 1,
		Location3 = 2,
		Location4 = 3,
		Location5 = 4,
		Location6 = 5,
		Location7 = 6,
		Location8 = 7,
		Location9 = 8,
		Location10 = 9,
		Location11 = 10,
		Location12 = 11,
		Location13 = 12,
		Location14 = 13,
		Location15 = 14,

		TempCamp = 50,
		FortOwner = 51,
		Tutor = 52,
		Dundeon = 52,
		FortEnemy = 54,
	}
}
