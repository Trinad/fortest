﻿namespace Assets.Scripts.InstanceServer.Enums
{
    public enum Elements : byte
    {
        Neutral = 0,
        Water = 1,//Blue = 1,
        Fire = 2,//Red = 2,
        Nature = 3,//Green = 3,
        Light = 4,//Amber = 4,
        Dark = 5,//Purple = 5,
        Electric = 6,
        Poison = 7,
        Count = 8,

        FromWeapon = 255,
    }
}
