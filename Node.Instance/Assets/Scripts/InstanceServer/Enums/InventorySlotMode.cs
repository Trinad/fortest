﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum InventorySlotMode
	{
		NONE = 0,
		EQUIPMENT = 1,
		BACKPACK = 2,
		WAREHOUSE = 3,
		SHOP = 4,
		UPGRADE = 5,
		SPARKS = 6,
		SHOP_CENTER = 7,
		VIP = 8,
	}
}
