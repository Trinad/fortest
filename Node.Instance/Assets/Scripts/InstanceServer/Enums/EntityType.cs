﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum EntityType : byte
	{
		Entity,
		Object,
		Effect,
		Loot,
		Equip,
	}
}
