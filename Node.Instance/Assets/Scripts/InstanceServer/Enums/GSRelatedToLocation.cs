﻿namespace Assets.Scripts.InstanceServer.Enums
{
    public enum GSRelatedToLocation
    {
        Below = 0,
        Equal = 1,
        More = 2
    }
}
