﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum SpecialActionBannerType
	{
		//NewbieSection
		StarterPack = 0, //Комплект первооткрывателя
		BigStarterPack = 1, //Большой комплект первооткрывателя
		StarterPotionPack = 2, //Стартовый набор зелий
		BigStarterPotionPack = 3, //Большой Стартовый комплект зелий

		//RareEquipment
		ExplorerPack = 4, //Комплект исследователя
		SubjugatorPack = 5, //Комплект покорителя
		ConquerorPack = 6, //Комплект завоевателя

		//PotionSection
		PotionPack = 7, //Комплекты зелий

		//EvolutionSection
		EvolutionPack = 8, //Набор развития персонажа

		//UpgradeEquipment
		UpgradeEquipmentPack = 9, //Набор улучшения экипировки

		//DungeonConquerer
		DungeonSubjugatorPack = 10, //Набор покорителя подземелий
	}
}
