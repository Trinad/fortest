﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum DailyQuestEventType : sbyte
	{
		Finished,
		Skipped,
	}
}
