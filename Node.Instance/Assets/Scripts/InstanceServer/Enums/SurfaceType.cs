﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum SurfaceType
	{
		Grass, //трава
		WaterLow, //вода мелкая
		Dirt, //грязь
		Stone, //камень
		Lava, // лава
		DeadZone, //мертвая зона
		WaterMedium, //вода до пояса
		WaterHigh, //вода выше головы
		PitTrap, //ловушка с шипами
		Acid, //Кислота
		Shrub, //Опасный куст
	}
}
