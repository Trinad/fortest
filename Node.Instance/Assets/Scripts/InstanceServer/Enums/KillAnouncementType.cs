﻿namespace Assets.Scripts.InstanceServer.Enums
{
    internal enum KillAnouncementType
    {
        FirstBlood,
        Kill,
        Suicide,
		KillPvP
	}
}
