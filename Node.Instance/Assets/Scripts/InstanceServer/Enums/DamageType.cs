﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum DamageType
	{
		Melee = 0,
        Range = 1,
        Dot = 2,
        Area = 3,
	}
}
