﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum ArenaRewardChestType
	{
		None = -1,
		Wood,
		Bronze,
		Silver,
		Gold,
		Unique,
		Stone,
		Guban,
		Dragon,
		Ancient,
		Energy,
	}
}
