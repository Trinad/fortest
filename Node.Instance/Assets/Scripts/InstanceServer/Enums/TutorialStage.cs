﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum TutorialStage
	{
		Start,

		//Начало тутора
		AskAboutTutorial = 1,
		CharacterControlTutorialStep = 2,
		AttackTutorialStep = 3,
		AOEAbilityTutorialStep = 4,
		FightDialogTutorialStep = 5,
		TeamDeathMatchTutorialStep = 6,
		ArenaRewardTutorialStep = 7,
		SetAccountNameTutorialStep = 8,
		CreatePersonTutorialStep = 9,
		LobbyMenuTutorialStep = 10,
		StartArenaTutorialStep = 11,
		FinishArenaTutorialStep = 12,
		CompleteBasicTutorialStep = 13,
		RewardTutorialStep = 14,
        ShowArenaButtonTutorialStep = 15,

        Finish,
	}
}
