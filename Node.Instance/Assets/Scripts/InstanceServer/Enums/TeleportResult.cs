﻿namespace Assets.Scripts.InstanceServer.Enums
{
	public enum TeleportResult
	{
		StartActivate,
		EnterNow,
		Break
	}
}
