﻿namespace Assets.Scripts.InstanceServer.Enums
{
    public enum HealthTypeVisual : byte
    {
        NormalPlayer, NormalMonster, BasedOnHitsWithTimer
    }
}
