﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.Networking;
using System;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries;
using UtilsLib.Logic.Enums;
using UtilsLib.BatiyScript;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.InstanceServer.Entries.Login;
using System.Linq;
using Assets.Scripts.InstanceServer.Extensions;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class DeathmatchSoloStatistic : StatisticBase
    {
        public FormulaX PlaceKoeff;
        
        public FormulaX LeagueKoefficient;
        public FormulaX WinChance;
        public FormulaX RatingChange;
        public FormulaX RatingChangeIfLeave;

        public override ArenaMatchType GetArenaMatchType()
        {
            return ArenaMatchType.Deathmatch;
        }

        public override void Start(Map sceneMap)
        {
            base.Start(sceneMap);

            map.VisitLocation += PersonVisitLocation;

            map.PersonDie += PersonDie;
			map.MonsterDie += MonsterDie;
        }

        protected void PersonDie(InstancePerson target, MapTargetObject attacker)
        {
            MapTargetDie(target.syn, attacker);
        }

        protected void MonsterDie(MonsterBehaviour target, MapTargetObject attacker)
        {
            MapTargetDie(target, attacker);
        }

        public override void SyncEloRating(InstancePerson person, PlayerTeam team, bool bStopped)
        {
            if (person == null)
                return;

            ArenaPlayerInfo api_person = getPlayerInfo(person);
            if (api_person == null || api_person.Disconnected)
                return;

            int addRating = (bStopped ? getPersonRating(api_person) : getLeavePersonRating(api_person));
            person.StatContainer.SetStatValue(Stats.EloRatingTeam, Math.Max(0, api_person.startRating + addRating));
        }

        public override int GetPersonRating(ArenaPlayerInfo api_person)
        {
            return getPersonRating(api_person);
        }

        private int getPersonPlace(int id)
        {
            ArenaPlayerInfo api = (getPlayerInfo(id));
            if (api == null)
                return -1;

            return api.Place;
        }


        public int GetMaxScore()
        {
            return persons.Count > 0 ? persons.Values.Max(p => p.FullScore) : 0;
        }

        private double PlaceKoefficient(ArenaPlayerInfo person)
        {
            if (persons.Count == 0)
                return 0.0;

            int place = getPersonPlace(person.PersonId);
            if (place <= 0)
                return 0.0;

            int totalCount = persons.Count;

            if (totalCount < place)
                return 0.0;

            return PlaceKoeff.Calc(totalCount == 1 ? 2 : totalCount, place);// TODO fil разобраться почему totalCount = 1
        }

        private double winchance(ArenaPlayerInfo person)
        {
            return WinChance.Calc(person.startRating, averageEnemiesRating(person));
        }

        private int getPersonRating(ArenaPlayerInfo person)
        {
            double placeKoefficient = PlaceKoefficient(person);
            double winChance = winchance(person);
            int koeff = LeagueKoefficient.IntCalc(person.startRating, person.gameCount);

            return (int)RatingChange.Calc(koeff, placeKoefficient, winChance);
        }

        private int getLeavePersonRating(ArenaPlayerInfo person)
        {
            double placeKoefficient = PlaceKoefficient(person);
            double winChance = winchance(person);
            int koeff = LeagueKoefficient.IntCalc(person.startRating, person.gameCount);

            return (int)RatingChangeIfLeave.Calc(koeff, winChance);
        }

        protected void MapTargetDie(MapTargetObject target, MapTargetObject attacker)
        {
            int targetId = target.GetTargetId();
            AddPersonDeath(targetId);

            bool bSuicide = AddSuicide(target, attacker);

            int attackerId = attacker.GetTargetId();

            if (!bSuicide && attackerId != 0)
                AddSeries(targetId, attackerId);
			
            if (!bSuicide && attackerId != 0)
            {
                int score = AddKillScore.IntCalc(series[attackerId]);
                RegisterKillScore(attacker, target, score);
            }

			OnSendArenaStatistics(StatisticSendMode.PartialUpdate);
		}

        public override List<ArenaPlayerInfo> GetPlayerList()
        {
            List<ArenaPlayerInfo> lst = new List<ArenaPlayerInfo>();
            foreach (var kvp in persons)
            {
                if (kvp.Value.isPerson)
                {
                    var pers = map.persons.Find(x => x.GetTargetId() == kvp.Key);
                    kvp.Value.Disconnected = pers != null ? !pers.bOnLine : true;
                }
                lst.Add(kvp.Value);
            }


            lst.Sort((a,b) => 
            {
                if (a.Disconnected != b.Disconnected)
                    return a.Disconnected ? 1 : -1;
                if (a.FullScore != b.FullScore)
                    return b.FullScore - a.FullScore;
                if (a.Kills != b.Kills)
                    return b.Kills - a.Kills;
                if (a.Deaths != b.Deaths)
                    return a.Deaths - b.Deaths;
                if (a.Assists != b.Assists)
                    return b.Assists - a.Assists;
                return string.Compare(a.Name, b.Name);
            }
            );

            for (int i = 0; i < lst.Count; i++)
                lst[i].Place = i + 1;

            return lst;
        }

        internal void UpdateEntityId(InstancePerson p)
        {
            PersonBehaviour pb = p.syn;
            if (pb == null)
                return;

            var info = getPlayerInfo(p);
            if (info != null)
                info.EntityId = pb.Id;
        }

        private void PersonVisitLocation(InstancePerson p)
		{
            WinCounter winCounter = p.login.WinCounterCollection.Get(p.personType);
            var gameStats = winCounter.GetArenaGameStats(ArenaMatchType.Deathmatch);
            int gameCount = gameStats.GameCount;
            AddArenaPlayerInfo(p.syn, gameCount);
            ServerRpcListener.Instance.SendRpc(p.ClientRpc.MultiKillDurationResponse, SeriesMaxTimeSec);
        }

        public override GameResult GetGameResult()
        {

            return GameResult.None;
        }

        public override string ToString()
        {
            return "DeathmatchSoloStatistic";
        }
    }
}
