﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Actions;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.InstanceServer.Extensions;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Networking.ClientDataRPC.Arenas;
using Assets.Scripts.Utils;
using UtilsLib.BatiyScript;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.InstanceServer.Addin
{
    public abstract class StatisticBase : Scenario
    {
        [XmlAttribute]
        public int MaxSeries;

        [XmlAttribute]
        public int SeriesMaxTimeSec;

        public FormulaX AddKillScore;
        public FormulaX AddAssistScore;
        public FormulaX AddSuicideScore;
        public FormulaX EndMatchScoreCount;

		[XmlIgnore]
		public AssistsMeter PlayerAssists;

        protected Dictionary<int, int> series = new Dictionary<int, int>();
        protected Dictionary<int, float> kills = new Dictionary<int, float>();
        protected Dictionary<int, ArenaPlayerInfo> persons = new Dictionary<int, ArenaPlayerInfo>();

        public event Action<StatisticSendMode> SendArenaStatisticsHandler;
        public event Action<MapTargetObject, PersonBehaviour> OnPersonAssists;

        public IEnumerable<PersonBehaviour> GetAllRegisterPersonBehaviours()
        {
            return map.persons.Where(x => getPlayerInfo(x) != null);
        }

		public IEnumerable<MapTargetObject> GetAllRegisterMapTargetObject()
		{
			return map.GetAllLiveTarget().Where(x => getPlayerInfo(x) != null);
		}

		public override void Start(Map sceneMap)
		{
			map = sceneMap;

			map.PersonDie += (person, target) => MapTargetDie(person.syn, target);
			map.MonsterDie += MapTargetDie;
            map.OnOutDamage += OnOutDamage;

            map.PersonHeal += PersonHeal;

            PlayerAssists = new AssistsMeter(map, AddAssistScore,
				attacker => attacker.characterType == CharacterType.Person,
				target => target.characterType == CharacterType.Person
			);

			PlayerAssists.OnAddAssistDamage += (attackerId, damage) =>
			{
                if (persons.TryGetValue(attackerId, out var api_attacker))
                {
                    api_attacker.DamageDealed += damage;
                }
			};

			PlayerAssists.OnAssist += (target, assistId, score) =>
			{
				if (persons.TryGetValue(assistId, out var api_assist))
				{
					api_assist.AddAssist(score);
					PersonBehaviour assistant = map.persons.Find(x => x.Id == api_assist.EntityId);
					if (assistant != null)
					{
						ServerRpcListener.Instance.SendShowCommonNotificationResponse(assistant.owner, ServerConstants.Instance.ArenaDeathmatchAssist, score);
						OnPersonAssists?.Invoke(target, assistant);
					}
				}
			};
		}

        private void OnOutDamage(MapTargetObject attacker, MapTargetObject target, int damage)
        {
            ArenaPlayerInfo api = getPlayerInfo(target);
            if (api == null)
                return;
            api.DamageReseaved += damage;
        }


        private void MapTargetDie(MapTargetObject target, MapTargetObject attacker)
		{
			PlayerAssists.CalculateAssistsScore(target);
		}

        #region EventHandlers

        protected void AddPersonDeath(int targetId)
        {
            ArenaPlayerInfo api_target = getPlayerInfo(targetId);

            if (api_target != null)
            {
                api_target.AddDeath();
            }
        }

        protected bool AddSuicide(MapTargetObject target, MapTargetObject attacker)
        {
            if (attacker == null || attacker == target)
            {
                ArenaPlayerInfo api_target = getPlayerInfo(target);

                //Sos.Debug("самоубийство");
                //2 очка отнимается за самоубийство или смерть от механик карты (упал в пропасть, сгорел в лаве);

                //Пока эта механика отключена
               // if (api_target != null)
               // {
             //       api_target.score += AddSuicideScore.IntCalc();
             //   }

                return true;
            }

            return false;
        }

        protected void AddSeries(int targetId, int attackerId)
        {
            if (!series.ContainsKey(attackerId))
                series[attackerId] = 0;

            if (!kills.ContainsKey(attackerId))
                kills[attackerId] = float.MinValue;
            // Серия прерывается после смерти
            series[targetId] = 0;

            //Серия засчитывается, если между убийствами прошло не более 2ух секунд.
            float lastKillTime = kills[attackerId];
            if (Time.time < lastKillTime + SeriesMaxTimeSec)
            {
                series[attackerId] += 1;
                //Sos.Debug("серия +1 "+ series[attackerId]);
            }
            else
                series[attackerId] = 1;

            kills[attackerId] = Time.time;
        }

        protected void RegisterKillScore(MapTargetObject attacker, MapTargetObject target, int scores)
        {
            ArenaPlayerInfo api_killer = getPlayerInfo(attacker);
            if (api_killer == null)
                return;
            api_killer.AddKill(scores);
        }

        //TODO_maximpr что означает это событие. оно ничего не сообщает
        protected void OnSendArenaStatistics(StatisticSendMode mode)
        {
            try
            {
                SendArenaStatisticsHandler?.Invoke(mode);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send(ex.ToString(), ErrorLevel.error);
            }
        }

        #endregion

        public int redTeamPointsInRound { get; protected set; }
        public int blueTeamPointsInRound { get; protected set; }
        public int redTeamPointsInMatch { get; protected set; }
        public int blueTeamPointsInMatch { get; protected set; }

        protected List<int> redScores = new List<int>();
        protected List<int> blueScores = new List<int>();

        public GameResultForTeam GetGameResultForTeam(PlayerTeam team)
        {
            if (team == PlayerTeam.Team2 || team == PlayerTeam.Team1)
            {
                GameResult gameResult = GetGameResult();

                if (gameResult == GameResult.Draw)
                    return GameResultForTeam.Draw;

                if (team == PlayerTeam.Team2)
                    return gameResult == GameResult.RedWin ? GameResultForTeam.Win : GameResultForTeam.Lose;

                return gameResult == GameResult.RedWin ? GameResultForTeam.Lose : GameResultForTeam.Win;
            }

            return GameResultForTeam.NoMatter;
        }

        public virtual GameResult GetGameResult()
        {
            if (redTeamPointsInMatch == blueTeamPointsInMatch)
                return GameResult.Draw;

            if (redTeamPointsInMatch > blueTeamPointsInMatch)
                return GameResult.RedWin;
            else
                return GameResult.BlueWin;
        }

        private int GetGameScore(PlayerTeam team)
        {
            return EndMatchScoreCount.IntCalc((int)GetGameResultForTeam(team));
        }

		internal void AddWinScore()
		{
			foreach (var personStat in persons.Values)
			{
				if (personStat.Disconnected)
					continue;

				PlayerTeam team = personStat.playerTeam;
				var gameTeamScore = GetGameScore(team);
                
				personStat.AddTeamScore(GetGameResultForTeam(team) == GameResultForTeam.Win, gameTeamScore);
			}

			GetPlayerList(); //Чтоб место пересчитать
		}

        public abstract List<ArenaPlayerInfo> GetPlayerList();

		public virtual List<ArenaResultPlayerInfoBase> GetPlayerListResult()
		{
			throw new NotImplementedException();
		}


		public virtual List<ArenaPlayerInfoBase> GetPlayerListLight()
		{
			throw new NotImplementedException();
		}
		public abstract ArenaMatchType GetArenaMatchType();

        public ArenaPlayerInfo getPlayerInfo(int targetId)
        {
            return persons.TryGetValue(targetId, out var result) ? result : null;
        }
        public ArenaPlayerInfo getPlayerInfo(MapTargetObject target)
        {
            return persons.TryGetValue(target.GetTargetId(), out var result) ? result : null;
        }
        public ArenaPlayerInfo getPlayerInfo(InstancePerson person)
        {
            return persons.TryGetValue(person.GetTargetId(), out var result) ? result : null;
        }

		public virtual ArenaResultPlayerInfoBase getPlayerInfoNew(InstancePerson person)
		{
			throw new NotImplementedException();
		}

		public void GetEloRatingInfo(InstancePerson person, out int addRating, out int startElo)
        {
            startElo = 0;
            addRating = 0;
            if (person == null)
                return;

            ArenaPlayerInfo api_person = getPlayerInfo(person);
            if (api_person == null)
                return;

            addRating = GetPersonRating(api_person);
            startElo = api_person.startRating;

            if ((startElo + addRating) < 0)
                addRating = -startElo;
        }
        public abstract int GetPersonRating(ArenaPlayerInfo person);

        protected double averageEnemiesRating(ArenaPlayerInfo person)
        {
            double total = 0.0;
            if (persons.Count == 0)
                return 0;

            var enemies = persons.Values.Where(x => x.playerTeam != person.playerTeam || (person.playerTeam == PlayerTeam.None && x.PersonId != person.PersonId)).ToList();
            if (enemies.Count == 0)
                return 0;

            foreach (var api in enemies)
                total += api.startRating;
            return total / enemies.Count;
        }

        public abstract void SyncEloRating(InstancePerson person, PlayerTeam team, bool bStopped);

		public void AddArenaPlayerInfo(MapTargetObject target, int gameCount)
		{
			int startRating = 0;
			var container = target.GetStatContainer();
			startRating = (int)container?.GetStatValue(Stats.EloRatingTeam);

			AddArenaPlayerInfo(target, gameCount, startRating);
		}

		public void AddArenaPlayerInfo(MapTargetObject target, int gameCount, int startRating)
		{
			int personId = target.GetTargetId();
			if (!persons.ContainsKey(personId))
			{
				ArenaPlayerInfo arenaPlayerInfo = new ArenaPlayerInfo
				{
					Name = target.NickName,
					PersonId = personId,
					EntityId = target.Id,
					startRating = startRating,
					playerTeam = target.TeamColor,
					gameCount = gameCount,
					isPerson = personId > 0,
					PersonType = target.personType,
					GearScore = target.HeroPower,

				};
				arenaPlayerInfo.AvatarName = target.GetAvatar();

				PersonBehaviour person = target as PersonBehaviour;
				if (person != null)
					arenaPlayerInfo.Level = person.owner.Level;
				else
					arenaPlayerInfo.Level = target.Level;

				persons[personId] = arenaPlayerInfo;
			}

			if (!series.ContainsKey(personId))
				series[personId] = 0;

			if (!kills.ContainsKey(personId))
				kills[personId] = float.MinValue;

			OnSendArenaStatistics(StatisticSendMode.FullUpdate);
		}


        public void CalcMVP(bool teamMode)
        {
            ArenaPlayerInfo Team1MVP = null;
            ArenaPlayerInfo Team2MVP = null;
            foreach (var item in persons)
            {
                if ((item.Value.playerTeam == PlayerTeam.Team1 || !teamMode) && (Team1MVP == null || Team1MVP.DamageDealed < item.Value.DamageDealed))
                    Team1MVP = item.Value;

                if (teamMode)
                    if (item.Value.playerTeam == PlayerTeam.Team2 && (Team2MVP == null || Team2MVP.DamageDealed < item.Value.DamageDealed))
                        Team2MVP = item.Value;
            }

            if (Team1MVP != null)
                Team1MVP.IsMVP = true;

            if (Team2MVP != null)
                Team2MVP.IsMVP = true;
        }

        private void PersonHeal(MapTargetObject target, MapTargetObject healer, int heal)
        {
            if ((healer is PersonBehaviour healerPerson) && healerPerson.owner == null)
                return;

            ArenaPlayerInfo api_healer = getPlayerInfo(healer);

            if (api_healer != null && healer != target)
            {
                api_healer.AddHeal(heal);
            }
        }
    }
}
