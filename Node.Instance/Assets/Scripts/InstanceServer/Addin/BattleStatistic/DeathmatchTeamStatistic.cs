﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.Networking;
using System;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries;
using UtilsLib.Logic.Enums;
using UtilsLib.BatiyScript;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Extensions;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class DeathmatchTeamStatistic : StatisticBase
    {
        public FormulaX LeagueKoefficient;
        public FormulaX WinChance;
        public FormulaX RatingChange;
        public FormulaX RatingChangeIfLeave;

        public int TeamKillScore = 1;

        public override void Start(Map sceneMap)
        {
            base.Start(sceneMap);

            map.VisitLocation += PersonVisitLocation;

            map.PersonDie += PersonDie;
            map.MonsterDie += Map_MonsterDie;
        }
        
        private void PersonDie(InstancePerson target, MapTargetObject attacker)
        {
            MapTargetDie(target.syn, attacker);
        }

        private void Map_MonsterDie(MonsterBehaviour target, MapTargetObject attacker)
        {
            MapTargetDie(target, attacker);
        }

        internal int GetMaxTeamPoints()
        {
            return Math.Max(redTeamPointsInMatch, blueTeamPointsInMatch);
        }


        internal void FillArenaInfo(TeamDeathmatchData amd, bool fullInfo)
        {
            amd.RoundRedScore = redTeamPointsInRound;
            amd.RoundBlueScore = blueTeamPointsInRound;
            amd.MatchRedScore = redTeamPointsInMatch;
            amd.MatchBlueScore = blueTeamPointsInMatch;
            amd.FullInfo = fullInfo;
            if (fullInfo)
            {
                amd.RedScores = redScores;
                amd.BlueScores = blueScores;
            }
        }

        private void AddTeamPoints(PlayerTeam team, int points)
        {
            if (team == PlayerTeam.Team2)
                redTeamPointsInRound += points;
            else
                blueTeamPointsInRound += points;
        }

        public override ArenaMatchType GetArenaMatchType()
        {
            return ArenaMatchType.TeamDeathmatch;
        }
        
        public override void SyncEloRating(InstancePerson person, PlayerTeam team, bool bStopped)
        {
            if (person == null)
                return;

            ArenaPlayerInfo api_person = getPlayerInfo(person);
            if (api_person == null || api_person.Disconnected)
                return;

            int addRating = (bStopped ? getPersonRating(api_person, team) : getLeavePersonRating(api_person, team));
            person.StatContainer.SetStatValue(Stats.EloRatingTeam, Math.Max(0, api_person.startRating + addRating));
        }

        public override int GetPersonRating(ArenaPlayerInfo api_person)
        {
            return getPersonRating(api_person, api_person.playerTeam);
        }

        private int getPersonRating(ArenaPlayerInfo person, PlayerTeam team)
        {
            double placeKoefficient = (int)GetGameResultForTeam(team);
            double winChance = WinChance.Calc(person.startRating, averageEnemiesRating(person));
            int koeff = LeagueKoefficient.IntCalc(person.startRating, person.gameCount);
            
            return (int)RatingChange.Calc(koeff, placeKoefficient, winChance);
        }

        private int getLeavePersonRating(ArenaPlayerInfo person, PlayerTeam team)
        {
            double winChance = 0;
            int koeff = LeagueKoefficient.IntCalc(person.startRating, person.gameCount);
            
            return (int)RatingChangeIfLeave.Calc(koeff, winChance);
        }

        private void MapTargetDie(MapTargetObject target, MapTargetObject attacker)
        {
            int targetId = target.GetTargetId();

			AddPersonDeath(targetId);

            bool bSuicide = AddSuicide(target, attacker);

            int attackerId = attacker.GetTargetId();

            if (!bSuicide && attackerId != 0)
                AddSeries(targetId, attackerId);

            if (!bSuicide && attackerId != 0)
            {
                int score = AddKillScore.IntCalc(series[attackerId]);
                RegisterKillScore(attacker, target, score);
            }

            if (!bSuicide)
                AddTeamPoints(attacker.TeamColor, TeamKillScore);

            OnSendArenaStatistics(StatisticSendMode.PartialUpdate);
		}

        public override List<ArenaPlayerInfo> GetPlayerList()
        {
            List<ArenaPlayerInfo> lst = new List<ArenaPlayerInfo>();
            foreach (var kvp in persons)
            {
                if (kvp.Value.isPerson)
                {
                    var pers = map.persons.Find(x => x.GetTargetId() == kvp.Key);
                    kvp.Value.Disconnected = pers != null ? !pers.bOnLine : true;
                }
                lst.Add(kvp.Value);
            }

            lst.Sort((a, b) =>
                {
                    if (a.Disconnected != b.Disconnected)
                        return a.Disconnected ? 1 : -1;
                    if (a.FullScore != b.FullScore)
                        return b.FullScore - a.FullScore;
                    if (a.Kills != b.Kills)
                        return b.Kills - a.Kills;
                    if (a.PersonId != b.PersonId)
                        return b.PersonId - a.PersonId;
                    return string.Compare(a.Name, b.Name);
                }
            );

            for (int i = 0; i < lst.Count; i++)
                lst[i].Place = i + 1;

            return lst;
        }

        internal void UpdateEntityId(InstancePerson p)
        {
            PersonBehaviour pb = p.syn;
            if (pb == null)
                return;

            var info = getPlayerInfo(p);
            if (info != null)
                info.EntityId = pb.Id;
        }

        private void PersonVisitLocation(InstancePerson p)
		{
            WinCounter winCounter = p.login.WinCounterCollection.Get(p.personType);
            var gameStats = winCounter.GetArenaGameStats(ArenaMatchType.TeamDeathmatch);
            int gameCount = gameStats.GameCount;
            AddArenaPlayerInfo(p.syn, gameCount);
            ServerRpcListener.Instance.SendRpc(p.ClientRpc.MultiKillDurationResponse, SeriesMaxTimeSec);
        }

        internal void EndRound()
        {
            redTeamPointsInMatch += (redTeamPointsInRound >= blueTeamPointsInRound) ? 1 : 0;
            blueTeamPointsInMatch += (blueTeamPointsInRound >= redTeamPointsInRound) ? 1 : 0;
            //тут запомнить статистику...
            redScores.Add(redTeamPointsInRound);
            blueScores.Add(blueTeamPointsInRound);
        }

        public override string ToString()
        {
            return "DeathmatchTeamStatistic";
        }
    }
}
