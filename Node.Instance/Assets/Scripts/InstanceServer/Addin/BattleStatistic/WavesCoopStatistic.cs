﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.InstanceServer.Extensions;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Networking.ClientDataRPC.Arenas;
using Assets.Scripts.Utils;
using UtilsLib.BatiyScript;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class WavesCoopStatistic : StatisticBase
    {
        public FormulaX PersonalWaveScore;
        public FormulaX PlaceKoeff;
        public FormulaX LeagueKoefficient;

        [XmlIgnore]
        AssistsMeter MobAssists;

        public override ArenaMatchType GetArenaMatchType()
        {
            return ArenaMatchType.Waves;
        }

		public override ArenaResultPlayerInfoBase getPlayerInfoNew(InstancePerson person)
		{
			persons.TryGetValue(person.GetTargetId(), out var result);
			if (result == null)
				return null;

			var info = new WavesResultPlayerInfo(result);
			return info;
		}

		public override void Start(Map sceneMap)
        {
			base.Start(sceneMap);

            MobAssists = new AssistsMeter(map, AddAssistScore,
                attacker => attacker.characterType == CharacterType.Person,
                target => target.characterType == CharacterType.Mob
            );
            MobAssists.OnAddAssistDamage += (attackerId, damage) =>
            {
                if (persons.TryGetValue(attackerId, out var api_attacker))
                {
                    api_attacker.DamageDealed += damage;
                }
            };

            MobAssists.OnAssist += (target, assistId, score) =>
            {
                if (persons.TryGetValue(assistId, out var api_assist))
                {
                    api_assist.AddAssist(0);
                }
            };

            map.VisitLocation += PersonVisitLocation;
        }

        private int deathNumber = 0;

        internal void OnPersonDie(InstancePerson person)
        {
            var p = getPlayerInfo(person);
            if (p != null && !p.bDead)
            {
                p.bDead = true;
                deathNumber++;
                p.deathNumber = deathNumber;
            }
        }

        internal void OnMonsterDie(MonsterBehaviour monster, MapTargetObject attacker, int waveId)
        {
            if (monster.characterType == Enums.CharacterType.Person)
            {
                var m = getPlayerInfo(monster);
                if (m != null)
                {
                    m.bDead = true;
                    deathNumber++;
                    m.deathNumber = deathNumber;
                }
            }
            else
            {
                int targetId = monster.GetTargetId();
                int attackerId = attacker.GetTargetId();
                if (attackerId == 0)
                    return;

                AddSeries(targetId, attackerId);

                int score = AddKillScore.IntCalc((int)monster.Data.MonsterType, waveId);
                RegisterKillScore(attacker, monster, score);
                MobAssists.CalculateAssistsScore(monster);
            }

        }

        internal void OnWaveNextWaveStart(int waveId)
        {
            foreach (var record in persons)
            {
                if (!record.Value.bDead)
                {
                    record.Value.AddWaveScore((sbyte)waveId, PersonalWaveScore.IntCalc(waveId));
                }
            }
        }
        private void PersonVisitLocation(InstancePerson p)
        {
            WinCounter winCounter = p.login.WinCounterCollection.Get(p.personType);
            var gameStats = winCounter.GetArenaGameStats(ArenaMatchType.Waves);
            int gameCount = gameStats.GameCount;

			var startRating = (int)p.rating_elo_coop;
			AddArenaPlayerInfo(p.syn, gameCount, startRating);
        }

        public override List<ArenaPlayerInfo> GetPlayerList()//Непонял, что тут, но помоему что-то лишнее смерть это конец волн
        {
            List<ArenaPlayerInfo> lst = new List<ArenaPlayerInfo>();
            foreach (var kvp in persons)
                lst.Add(kvp.Value);

            lst.Sort((a, b) =>
            {
                if (a.FullScore != b.FullScore)
                    return b.FullScore - a.FullScore;
                if (a.Kills != b.Kills)
                    return b.Kills - a.Kills;
                if (a.Deaths != b.Deaths)
                    return a.Deaths - b.Deaths;
                if (a.Assists != b.Assists)
                    return b.Assists - a.Assists;
                return string.Compare(a.Name, b.Name);
            }
            );

            for (int i = 0; i < lst.Count; i++)
                lst[i].Place = i + 1;


            return lst;
        }

		public override List<ArenaResultPlayerInfoBase> GetPlayerListResult()//Непонял, что тут, но помоему что-то лишнее смерть это конец волн
		{
			var lst = new List<WavesResultPlayerInfo>();
			foreach (var kvp in persons)
				lst.Add(new WavesResultPlayerInfo(kvp.Value));

			lst.Sort((a, b) =>
			{
				if (a.FullScore() != b.FullScore())
					return b.FullScore() - a.FullScore();
				if (a.Kill != b.Kill)
					return b.Kill - a.Kill;
				if (a.Assist != b.Assist)
					return b.Assist - a.Assist;
				return string.Compare(a.Name, b.Name);
			}
			);

			for (int i = 0; i < lst.Count; i++)
				lst[i].Place = i + 1;


			return lst.OfType<ArenaResultPlayerInfoBase>().ToList();
		}

		public override List<ArenaPlayerInfoBase> GetPlayerListLight()//Непонял, что тут, но помоему что-то лишнее смерть это конец волн
		{
			var lst = new List<WavesPlayerInfo>();
			foreach (var kvp in persons)
				lst.Add(new WavesPlayerInfo(kvp.Value));

			lst.Sort((a, b) =>
			{
				if (a.Score != b.Score)
					return b.Score - a.Score;
				return string.Compare(a.Name, b.Name);
			}
			);

			for (int i = 0; i < lst.Count; i++)
				lst[i].Place = i + 1;

			return lst.OfType<ArenaPlayerInfoBase>().ToList();
		}

		public override void SyncEloRating(InstancePerson person,PlayerTeam pt, bool bStopped)
        {
            //throw new NotImplementedException();
        }
        public override int GetPersonRating(ArenaPlayerInfo api_person)
        {
            return 0;
        }

        public double getPersonEffectPart(InstancePerson person)
        {
            int summ = 0;
            int self = 0;

            foreach (var p in persons)
            {
                if (p.Value.isPerson)
                {
                    if (p.Key == person.GetTargetId())
                        self += p.Value.DamageDealed + p.Value.HealAmount;

                    summ += p.Value.DamageDealed + p.Value.HealAmount;
                }
            }

            if (summ == 0)
                return 0;

            return (double)self / (double)summ;
        }
    }
}
