﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using System.Collections.Generic;
using System;
using System.Xml.Serialization;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class MinimapSpecialIcons : MinimapIconsScenario
    {
        [XmlAttribute]
        public MinimapItemType Icon;

        [XmlAttribute]
        public string ActiveData;

        public override Dictionary<MapTargetObject, MinimapItemType> MinimapResult()
        {
            Dictionary<MapTargetObject, MinimapItemType> minimapDatas = new Dictionary<MapTargetObject, MinimapItemType>();

            foreach (var m in map.monsters)
            {
                if (!m.IsDead && m.ActiveData == ActiveData)
                    minimapDatas.Add(m, Icon);
            }

            return minimapDatas;
        }
    }
}
