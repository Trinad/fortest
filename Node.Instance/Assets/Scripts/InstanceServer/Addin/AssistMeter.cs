﻿using System;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Extensions;
using UtilsLib.BatiyScript;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class AssistsMeter
    {
        private FormulaX addAssistScore;
		private Func<MapTargetObject, bool> IsAttaker;
		private Func<MapTargetObject, bool> IsTarget;

		public event Action<int, int> OnAddAssistDamage;
		public event Action<MapTargetObject, int, int> OnAssist;

        public AssistsMeter(Map map, FormulaX addAssistScore, Func<MapTargetObject, bool> IsAttaker, Func<MapTargetObject, bool> IsTarget)
        {
			map.monsters.OnAddAction += Register;
			map.persons.OnAddAction += Register;
			map.OnOutDamage += AddAssistDamage;

			this.addAssistScore = addAssistScore;
			this.IsAttaker = IsAttaker;
			this.IsTarget = IsTarget;

			foreach (var monster in map.monsters)
				Register(monster);
			foreach (var person in map.persons)
				Register(person);
        }

        protected Dictionary<int, Dictionary<int, int>> assists = new Dictionary<int, Dictionary<int, int>>();
        protected Dictionary<int, AssistInfo> persons = new Dictionary<int, AssistInfo>();

        public AssistInfo GetAssistInfo(int personId)
        {
            persons.TryGetValue(personId, out var person);
            return person;
        }

		public int GetAssistDamage(MapTargetObject target, MapTargetObject attacker)
		{
			return assists.TryGetValue(target.GetTargetId(), out var assist) && assist.TryGetValue(attacker.GetTargetId(), out var damage) ? damage : 0;
		}

		private void Register(MapTargetObject target)
		{
			int targetId = target.GetTargetId();

			if (IsAttaker(target))
			{
				if (!persons.ContainsKey(targetId))
					persons[targetId] = new AssistInfo();
				else
					ILogger.Instance.Send($"AssistsMeter.RegisterAttacker(): Dublicate register {targetId}", ErrorLevel.warning);
			}

			if (IsTarget(target))
			{
				if (!assists.ContainsKey(targetId))
					assists[targetId] = new Dictionary<int, int>();
				else
					ILogger.Instance.Send($"AssistsMeter.RegisterTarget(): Dublicate register {targetId}", ErrorLevel.warning);
			}
		}

        private void AddAssistDamage(MapTargetObject attacker, MapTargetObject target, int damage)
        {
            if (attacker == null)
                return;

            //это не ранение а смертельный урон, не надо награждать за помощь в таком случае
            if (attacker.IsDead)
                return;

            if (!IsTarget(target))
                return;

            int attackerId = attacker.GetTargetId();
            int targetId = target.GetTargetId();

            if (assists.TryGetValue(targetId, out var attackerList))
            {
                attackerList.TryGetValue(attackerId, out int totalDamage);

                //Sos.Debug("засчитали помощь");
                attackerList[attackerId] = totalDamage + damage;

                OnAddAssistDamage?.Invoke(attackerId, damage);
            }
            else
                ILogger.Instance.Send("out of key", " there is no asist info for = " + targetId, ErrorLevel.error);
        }

        public void CalculateAssistsScore(MapTargetObject target)
        {
            if (!IsTarget(target))
                return;

            int targetId = target.GetTargetId();
            Dictionary<int, int> attackerList = assists[targetId];

            var totalDamag = attackerList.Sum(a => a.Value);
            if (totalDamag > 0)
            {
                foreach (var pair in attackerList)
                {
                    double damagePercent = (double)pair.Value / totalDamag;

                    AssistInfo api_assist = GetAssistInfo(pair.Key);
                    if (api_assist != null)
                    {
                        int score = addAssistScore.IntCalc(damagePercent, target.GetStatContainer());
                        if (score > 0)
                        {
                            api_assist.Assist++;
                            api_assist.Score += score;
                            OnAssist?.Invoke(target, pair.Key, score);
                        }
                    }
                }
            }

            attackerList.Clear();
        }
    }


    public class AssistInfo
    {
        public int Score;
        public int Assist;

        public void Clear()
        {
            Score = 0;
            Assist = 0;
        }
    }
}
