﻿using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Assets.Scripts.Networking;
using Fragoria.Common.Utils;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class ShowBrawlNotify : IAction
    {
        [XmlAttribute]
        public string Text; //ключ локализации

        [XmlAttribute]
        public string IsRed; //в какой команде находится игрок

        [XmlAttribute]
        public bool OnlyPerson;// отсылка игроку или всем на карту

        private INotifyController notifyController;

        [XmlInject]
        public void Construct(INotifyController notifyController)
        {
            this.notifyController = notifyController;
        }
        public override bool Execute(PersonBehaviour person)
        {
            if (OnlyPerson)
                notifyController.SendShowNotificationTehNameResponse(person.owner, NotifyState.BrawlAnnounce, Text, IsRed);
            else
                notifyController.SendShowNotificationTehNameResponse(person.map, NotifyState.BrawlAnnounce, Text, IsRed);

            return true;
        }

        public override bool Execute(ContextIAction context)
        {
            if (OnlyPerson)
                notifyController.SendShowNotificationTehNameResponse(context.actorOwner, NotifyState.BrawlAnnounce, Text, IsRed);
            else
                notifyController.SendShowNotificationTehNameResponse(context.map, NotifyState.BrawlAnnounce, Text, IsRed);
            return true;
        }
    }
}