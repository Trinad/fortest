﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UtilsLib;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class MonsterXmlGenerators
    {
        [XmlArrayItem("MonsterXmlGroup", typeof(MonsterXmlGroup))]
        public List<MonsterXmlGroup> MonstersXmlGroups;

        [XmlAttribute]
        public int ActiveGenerators = 1;

		public void getMonsters(int waveId, out MonsterGroupData simpleMonsterData, out MonsterGroupData championMonsterData)
		{
			var group = MonstersXmlGroups.Find(x => x.waveId == waveId);

			simpleMonsterData = group.Datas.FindAll(m => m.Type == "simple").Random();
			championMonsterData = group.Datas.FindAll(m => m.Type == "champion").Random();
		}
	}

    public class MonsterXmlGroup
    {
        [XmlAttribute]
        public int waveId;
        public List<MonsterGroupData> Datas;
    }

    public class MonsterGroupData
    {
        [XmlAttribute] public string Name;
        [XmlAttribute] public string Type;
        [XmlAttribute] public int Count = 0;
    }
}
