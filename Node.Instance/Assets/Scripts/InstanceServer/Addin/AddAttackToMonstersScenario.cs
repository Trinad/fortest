﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.ActiveObjects;
using UtilsLib.Logic.Enums;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Actions;
using System.Numerics;
using UtilsLib.Logic;
using Assets.Scripts.Utils.Logger;
using Assets.Scripts.Utils.Common;
using Assets.Scripts.InstanceServer.Algorithms;
using UtilsLib;
using Assets.Scripts.InstanceServer.Entries.XmlData.AttackDatas;
using UtilsLib.DegSerializers;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class AddAttackToMonstersScenario : Scenario
    {
        [XmlArrayItem(typeof(CustomTargetRetreat))]
        public List<MonsterBehaviourPart> Attacks;

        public override void Start(Map sceneMap)
        {
            map = sceneMap;
            map.monsters.OnAddAction += OnMonsterAdd;
        }

        public void OnMonsterAdd(MonsterBehaviour monsterBehaviour)
        {
            foreach (var attack in Attacks)
            {
                var copy = attack.Copy();
                copy.monster = monsterBehaviour;
                monsterBehaviour.attacks.Add(copy);
                copy.Init();
            }            
        }

        public override string ToString()
        {
            return "AddAttackToMonstersScenario";
        }
    }
}
