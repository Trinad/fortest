﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.ActiveObjects;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using UtilsLib.DegSerializers;

namespace Assets.Scripts.InstanceServer.Addin
{
	public class SetupInteractions : Scenario
	{
		[XmlElement(typeof(Interaction_MagicMask))]
		[XmlElement(typeof(Interaction_Rename))]
		[XmlElement(typeof(Interaction_Arena))]
		[XmlElement(typeof(Interaction_Obelisk))]
		[XmlElement(typeof(Interaction_StatueBlessing))]
		[XmlElement(typeof(Interaction_Battleground))]
		[XmlElement(typeof(Interaction_Scroll))]
		[XmlElement(typeof(Interaction_WorldBoss))]
		[XmlElement(typeof(Interaction_CollectionChest_PvE))]
		[XmlElement(typeof(Interaction_CollectionChest_PvP))]
		[XmlElement(typeof(Interaction_Guildmaster))]
		[XmlElement("Interaction_NPC", typeof(MapNPC))]
		[XmlElement(typeof(Interaction_Teleport))]
		[XmlElement(typeof(Interaction_Flag))]
		[XmlElement(typeof(Interaction_BattlegroundRespawnBase))]
		public List<MapActiveObject> MapActiveObjects;

		public override void Start(Map sceneMap)
		{
			map = sceneMap;

			MapActiveObjects.RemoveAll(x => !x.fCondition.Calc(map));
			MapActiveObjects.RemoveAll(x => string.IsNullOrEmpty(x.ObjectName) && string.IsNullOrEmpty(x.ActiveData));

			foreach (var activeObject in MapActiveObjects)
			{
				var objectName = activeObject.ObjectName;
				var activeData = activeObject.ActiveData;

				foreach (var mapObject in map.objects)
					if ((objectName == null || mapObject.obj.name == objectName) && (activeData == null || mapObject.ActiveData == activeData))
						SetActivate(mapObject, activeObject, null);

				foreach (var npc in map.npcs)
					if ((objectName == null || npc.TargetName == objectName) && (activeData == null || npc.ActiveData == activeData))
						SetActivate(npc, activeObject, null);
			}

			map.CreateActiveObject += (target, owner) =>
			{
				foreach (var activeObject in MapActiveObjects)
				{
					var objectName = activeObject.ObjectName;
					var activeData = activeObject.ActiveData;

					if (target is MapObject mapObject)
						if ((objectName == null || mapObject.obj.name == objectName) && (activeData == null || mapObject.ActiveData == activeData))
							SetActivate(mapObject, activeObject, owner);

					if (target is NPCBehaviour npc)
						if ((objectName == null || npc.TargetName == objectName) && (activeData == null || npc.ActiveData == activeData))
							SetActivate(npc, activeObject, owner);
				}

				//ILogger.Instance.Send($"Not Found Interaction for {ActiveType}",error);
			};
		}

		private void SetActivate(MapTargetObject target, MapActiveObject activeObject, PersonBehaviour person)
		{
			var activeMapObject = activeObject.Copy();
			activeMapObject.target = target;

			if (person == null)
			{
				map.ActiveMapObjectAdd(activeMapObject);
			}
			else
			{
				var activeMapObjects = person.owner.GetActiveMapObjects();
				activeMapObject.map = map;
				activeMapObjects.Add(activeMapObject);
				activeMapObject.Init();
			}
		}
	}
}
