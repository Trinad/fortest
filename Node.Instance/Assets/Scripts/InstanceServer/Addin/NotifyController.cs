﻿using System.Linq;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using uLink;
using UtilsLib.Logic.Enums;
using Zenject;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class NotifyController : BaseInstanceRpcListener, INotifyController
    {
        public NotifyController(NetworkP2P networkP2P, uLink.Network network, INetworkView view) : base(networkP2P, network, view)
        {
        }

        [Inject]
        public void Construct(GameXmlData _gameXmlData)
        {
        }

        public void SendShowNotificationTehNameResponse(InstancePerson instancePerson, NotifyState notifyState, string notifyName, params object[] notifyParam)
        {
            if (notifyName == null)
                return; //TODO_Deg у некоторых квестов/тасков нет этого

            int notifyId = 0; //TODO_Deg удалить потом
            var sParams = notifyParam.Select(x => x.ToString()).ToList();
            SendRpc(instancePerson.ClientRpc.ShowNotificationResponse, notifyState, notifyId, notifyName, sParams);
        }

        public void SendShowNotificationTehNameResponse(Map map, NotifyState notifyState, string notifyName, params object[] notifyParam)
        {
            int notifyId = 0; //TODO_Deg удалить потом
            var sParams = notifyParam.Select(x => x.ToString()).ToList();
            SendRpc(map.ClientRpc.ShowNotificationResponse, notifyState, notifyId, notifyName, sParams);
        }

        public void SendShowNotificationResponse(InstancePerson instancePerson, NotifyState notifyState, params object[] notifyParam)
        {
            int notifyId = 0; //TODO_Deg удалить потом
            var notifyName = string.Empty;
            var sParams = notifyParam.Select(x => x.ToString()).ToList();
            SendRpc(instancePerson.ClientRpc.ShowNotificationResponse, notifyState, notifyId, notifyName, sParams);
        }
    }
}
