﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.Utils.Common;
using UtilsLib;
using Assets.Scripts.Utils;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class Champions : Scenario
    {
        [XmlAttribute]
        public List<string> LocationName;

        [XmlAttribute]
        public int Chance;

        [XmlAttribute]
        public int MaxCount;

	    [XmlElement("LocationMonsterData")]
		public List<LocationMonsterData> BossDatas;

        private int BossCount;

		private readonly List<int> champions = new List<int>();

		public override void Start(Map sceneMap)
        {
            if (!LocationName.Contains(ServerStartGameManager.levelName))
                return;

            map = sceneMap;
            Spawn(BossDatas.Random());

            map.MonsterDie += OnMonsterDie;
        }

        private void Spawn(LocationMonsterData data)
        {
            CoroutineRunner.Instance.RunNextFrame(() =>
            {
                var locationMonster = Map.Edge.LocationMonsters.Random(x => !x.IsWaterMonster);
	            int level = locationMonster.Level;

                CreateMonsterData createMonsterData = new CreateMonsterData()
                {
                    monsterName = data.MonsterName,
                    lootName = data.LootName,
                    level = level,
                    startPoint = locationMonster.AreaName,
                    replaceTechnicalName = data.ReplaceTechnicalName,
                };
                //var monster = map.CreateMonster(data.MonsterName, data.LootName, level, locationMonster.AreaName, data.ReplaceTechnicalName); //false
                var monster = map.CreateMonster(createMonsterData, false); //false
                map.AddMonsterOnMap(monster);
                monster.ResurrectTimeSec = -1;

				champions.Add(monster.Id);

				monster.CreateMobGenerator(Helper.GetGeneratorFor(),
					() => ServerRpcListener.Instance.SendCreateEntity(monster)
				);
            });
            BossCount++;
        }

        private void OnMonsterDie(MonsterBehaviour monster, MapTargetObject attacker)
        {
			if (champions.Contains(monster.Id))
			{
                BossCount--;
				champions.Remove(monster.Id);
			}

            if (BossCount < MaxCount && FRRandom.CheckPercentChance(Chance))
            {
				Spawn(BossDatas.Random());
            }
        }

        public override string ToString()
        {
            return "Champions";
        }
    }
}
