﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Abilities.Magician;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.HitActions;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Utils;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class SurfaceDamage : IAction
    {
		[XmlArrayItem(typeof(SendCreateFxResponseHitAction))]
        [XmlArrayItem(typeof(Chance))]
        [XmlArrayItem(typeof(AddInfluence))]
        [XmlArrayItem(typeof(KillTarget))]
        [XmlArrayItem(typeof(InfluenceChance))]
        [XmlArrayItem(typeof(Downfall))]
        public List<HitActionBase> HitActionList;

        [XmlAttribute]
        public SurfaceType SurfaceType;

		[XmlAttribute]
		public DeathReasonType DeathReason = DeathReasonType.None;

		[XmlAttribute]
		public bool IsFly;

		public override bool Execute(PersonBehaviour person)
        {
            throw new NotImplementedException();
        }

        public override bool Execute(ContextIAction context)
        {
            var lava = new LavaBehaviour();
            lava.map = context.map;
            lava.HitActionList = HitActionList;
            lava.SurfaceType = SurfaceType;
            lava.IsFly = IsFly;
            lava.Init();
	        lava.LavaTargetObject.DeathReason = DeathReason;

			return true;
        }
    }
}
