﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries;
using UtilsLib.Logic.Enums;
using System.Linq;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Actions;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Entries.Influences;
using System.Numerics;
using Assets.Scripts.InstanceServer.Games;
using Fragoria.Common.Utils;
using UtilsLib;
using UtilsLib.Logic;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class BotGenerator
    {
        protected int botNextAttackTimeSec = 5;

		[XmlAttribute]
		public bool CheckTeam = true;

        //заполняется из xml
        [XmlElement("CreateMonster", typeof(CreateMonster))]
        public List<CreateMonster> Bots;

		[XmlIgnore]
		public NickNameGeneratorManager nickNameGeneratorManager;

		[XmlInject]
		public void Init(NickNameGeneratorManager _nickNameGeneratorManager)
		{
			nickNameGeneratorManager = _nickNameGeneratorManager;
		}

		[XmlIgnore]
		private bool[] EnableTypes;

        private PersonType GetPersonsTypeInTeams(Map map, PlayerTeam team, List<PlayerTeamData> playerTeams)
        {
			if (EnableTypes == null)
			{
				EnableTypes = new bool[(int)PersonType.Count];
				foreach (var bot in Bots)
					EnableTypes[(int) bot.GetPersonType()] = true;
			}

            int[] Types = new int[(int)PersonType.Count];
            foreach (var alive in map.GetAllLiveTarget())
                if ((!CheckTeam || alive.TeamColor == team) && alive.personType != PersonType.None)
                    Types[(int)alive.personType]++;

			foreach (var playerData in playerTeams)
			{
				if (map.persons.Exists(x => x.owner.personId == playerData.PersonId))
					continue; //Уже учли

				if (!CheckTeam || playerData.Team == team)
					Types[(int)playerData.PersonType]++;
			}

			for (int i = 0; i < (int)PersonType.Count; i++)
				if (!EnableTypes[i])
					Types[i] = int.MaxValue;

			var min = Types.Min();
			return (PersonType)Types.Select((v,i) => new KeyValuePair<int, int>(i,v)).Random(x => x.Value == min).Key;
		}

        public List<MonsterBehaviour> CreateBots(Map map, int level, int startCount, int endCount, int botRating,
			List<PlayerTeamData> playerTeams)
        {
			var teamDistributor = map.Scenarios.OfType<ITeamDistributor>().First();

            var result = new List<MonsterBehaviour>();

            for (int i = startCount; i < endCount; i++)
            {
                var playerTeam = teamDistributor.GetNextTeamId(null);
                var personType = GetPersonsTypeInTeams(map, playerTeam, playerTeams);
				ILogger.Instance.Send($"CreateBots gameId={map.GameId} playerTeam={playerTeam} personType={personType}");
                var creater = Bots.Find(b => b.GetPersonType() == personType);
                string areaName = teamDistributor.GetStartPointByTeam(playerTeam);
                var pos = teamDistributor.GetAreaPos(areaName, i);
				var rating = Math.Max(0, botRating + FRRandom.Next(-200, 200));
				var monster = CreateStupidBot(map, creater, i, level, playerTeam, areaName, pos, rating);
                result.Add(monster);
            }

            return result;
        }

        protected MonsterBehaviour CreateStupidBot(Map map, CreateMonster creater, int id, int level, PlayerTeam team, string areaName, Vector3 pos, int botRating)
        {
            var monster = creater.GetMonster(map, pos, level);
            creater.AddMonsterOnMap(monster);
			monster.NickName = nickNameGeneratorManager.GenerateUnique("bot", map);
			//monster.NickName = NicknameAlgorithm.GetRandomName(monster.personType != PersonType.Gunner,
			//    id * FRRandom.Next(100, 999) * 16 + FRRandom.Next(8, 16));
            monster.AreaName = areaName;
            monster.TeamColor = team;
            monster.ResurrectTimeSec = -1; //Время задается в ResurrectTimer
            monster.nextAttack = TimeProvider.UTCNow.AddSeconds(botNextAttackTimeSec);

            var statContainer = monster.GetStatContainer();
            //statContainer.AddInfluence(InfluenceXmlDataList.Instance.GetInfluence(monster, InfluenceHelper.Freeze, 10000, 1));
			statContainer.AddStat(Stats.EloRatingTeam, botRating);

			return monster;
        }
    }
}
