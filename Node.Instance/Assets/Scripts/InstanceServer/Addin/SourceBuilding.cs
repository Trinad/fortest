﻿using System.Collections.Generic;
using System.Xml.Serialization;
using UtilsLib;
using UtilsLib.Logic;
using System;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Login.DataRPC;
using Assets.Scripts.Networking;
using uLink;
using Fragoria.Common.Utils;
using Assets.Scripts.Utils.Logger;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic.Enums;
using System.Linq;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.Utils.Common;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class SourceBuilding : Scenario
    {
        [XmlAttribute]
        public string BuildingName;

        [XmlAttribute]
        public string LoginScenarioString;

        private GameXmlData gameXmlData;

        public SourceBuilding()
        {
            CanCopy = false;
        }

        [XmlInject]
        public void Construct(GameXmlData gameXmlData, GlobalEvents globalEvents)
        {
            this.gameXmlData = gameXmlData;

			globalEvents.GiveBuilding += ChangeBuilding;
			globalEvents.RemoveBuilding += ChangeBuilding;
        }

		private void ChangeBuilding(LobbyLogin login, Building building)
        {
			if (building.Name == BuildingName)
				SendGetBonusSourceResponse(login);
        }

        public override void Start(Map sceneMap)
        {
	        sceneMap.GetLobbyInfoRequest += (person, loginInfo) => SendGetBonusSourceResponse(person.login);
        }

        private void SendGetBonusSourceResponse(LobbyLogin login)
        {
            List<BonusSourceData> sourceBuildings = new List<BonusSourceData>();

            var building = login.Buildings.FirstOrDefault(b => b.Name == BuildingName && b.State!= BuildingState.None);
            if (building == null)
			{
				ServerRpcListener.Instance.SendRpc(login.ClientRpc.GetBonusSourceResponse, sourceBuildings);
				return;
			}

	        var sFinishTime = login.scenarioStrings.Get(LoginScenarioString);
            if (string.IsNullOrEmpty(sFinishTime))
            {
				ServerRpcListener.Instance.SendRpc(login.ClientRpc.GetBonusSourceResponse, sourceBuildings);
				return;
            }

	        var finishTime = ScenarioStringHelper.GetDateTimeFromEpochTime(sFinishTime);
			if (finishTime < TimeProvider.UTCNow)
			{
				login.scenarioStrings.Remove(LoginScenarioString);
				ServerRpcListener.Instance.SendRpc(login.ClientRpc.GetBonusSourceResponse, sourceBuildings);
				return;
			}

            var buildingData = gameXmlData.PlayerCityData.Buildings.Find(x => x.Name == BuildingName);
            var sourceBuilding = new BonusSourceData()
            {
                Icon = buildingData.Icon,
                TechnicalName = buildingData.Desc,
                LocParams = new Dictionary<string, string>(),
                TimeSec = 2,
            };
            sourceBuilding.TimeSec += (int) (finishTime - TimeProvider.UTCNow).TotalSeconds;

            sourceBuildings.Add(sourceBuilding);
            ServerRpcListener.Instance.SendRpc(login.ClientRpc.GetBonusSourceResponse, sourceBuildings);
        }
    }

    public class BonusSourceData
    {
        public string Icon;
        public int TimeSec;
        public string TechnicalName;
        public Dictionary<string, string> LocParams;

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            var si = (BonusSourceData)value;
            stream.Write<string>(si.Icon);
            stream.Write<int>(si.TimeSec);
            stream.Write<string>(si.TechnicalName);
            stream.Write<Dictionary<string, string>>(si.LocParams);
        }
    }
}
