﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.Networking;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Addin
{
	public class SpecialEventScenarioData
	{
		[XmlAttribute]
		public string Name = string.Empty;
		[XmlAttribute]
		public string QuestName = string.Empty;

        public string Text = string.Empty;
        public string TopColor = string.Empty;
        public string BottomColor = string.Empty;

        public string FormBackgroundImage = string.Empty;
        public string SpecialEventBackgroundImage = string.Empty;
        public string SpecialEventOverlayImage = string.Empty;
        public string ProgressBarImage = string.Empty;
        public string ProgressBarGradientImage = string.Empty;
        public string ProgressBarHeadImage = string.Empty;
        public string CollectedItemImage = string.Empty;
        public string CollectedItemsBgImage = string.Empty;
        public string LobbyIconImage = string.Empty;
        public string ShowItemsButtonImage = string.Empty;

		[XmlAttribute]
		public List<string> DecorImages = new List<string>();

		public List<BuffString> GetDecorImages()
		{
			return DecorImages.Select(d => (BuffString)d).ToList();
		}


		public string ShowItemsButtonText = string.Empty;
        public string FirstStepHint = string.Empty; // ключи локализации для первой подсказки
        public string SecondStepHint = string.Empty;  // ключи локализации для второй подсказки
        public string ThirdStepHint = string.Empty;  // ключи локализации для третьей подсказки

        public string BackButtonColor = string.Empty; // цвет заголовка, нижний градиент (строка вида ffd2001e в формате ARGB)
        public string ProgressNextColor = string.Empty; // цвет текста прогресса, которого еще не достигли (строка вида ffd2001e в формате ARGB)
        public string ProgressCurrentColor = string.Empty;//  цвет текста прогресса, которого еще достигли сейчас (строка вида ffd2001e в формате ARGB)
        public string ProgressPrevColor = string.Empty; //  цвет текста прогресса, который уже прошли (строка вида ffd2001e в формате ARGB)

        [XmlAttribute]
        public ResourceType ResourceType = ResourceType.Token;
		[XmlAttribute]
		public string TokenIcon = string.Empty;
        public string ResourceIcon = string.Empty;
		public string CompensationHint = string.Empty;
		public string NotEnoughTokenHint = string.Empty;
    }
}
