﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.Networking;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.Utils;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.BatiyScript;
using System.Linq;
using Assets.Scripts.InstanceServer.Extensions;
using UtilsLib.Logic;
using Assets.Scripts.Networking.ClientDataRPC;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class ResurrectTimer : Scenario
    {
		[XmlAttribute]
		public bool ResurrectAtLocation = true;

        public FormulaX TimeSec = new FormulaX("elapsedSec deathCount p", "8");
        private float StartSec;

        public override void Start(Map sceneMap)
        {
            map = sceneMap;
            map.SetPlayerDeathInfoResponseDelegate(SendPlayerDeathInfoResponseOnArena);
            map.PersonDie += OnPersonDie;
            map.MonsterDie += OnMonsterDie;

            StartSec = Time.time;
        }

        //здесь умирают боты, и боты это персы :( 
        private void OnMonsterDie(MonsterBehaviour monster, MapTargetObject attacker)
        {
            //TODO Добавить корректный CharacterType, который будет только для бота. 
            //TODO Поправить айдишники для монстров и персов, чтоб никогда не перескались
            if (monster.characterType == CharacterType.Person)
			{
				monster.ResurrectTimeSec = GetRessurectSec(monster);
				monster.SetResurrectTime(monster.ResurrectTimeSec);
			}
        }

        private int GetDeathCount(MapTargetObject target)
        {
            var statistics = map.Scenarios.OfType<StatisticBase>().FirstOrDefault();
			if (statistics == null)
				return 0;

			return statistics.getPlayerInfo(target)?.Deaths ?? 0;
        }

        private int GetRessurectSec(MapTargetObject target)
        {
			var elapsedSec = Time.time - StartSec;
			var deathCount = GetDeathCount(target);
			return (int)TimeSec.Calc(elapsedSec, deathCount, target.GetStatContainer());
        }

		private void OnPersonDie(InstancePerson person, MapTargetObject attacker)
		{
			int ressurectSec = GetRessurectSec(person.syn);
			person.SetResurrectTime(ressurectSec);
			person.ResurrectCost = -1;
		}

        private void SendPlayerDeathInfoResponseOnArena(Map map, InstancePerson person, MapTargetObject attacker)
        {
			var playerDeathData = new PlayerDeathData(person, attacker, person.ResurrectSec);

			person.SetPlayerDeathData(playerDeathData);
			ServerRpcListener.Instance.PlayerDeathInfoResponse(person.syn, playerDeathData);
			

			CoroutineRunner.Instance.Timer(person.ResurrectSec, () =>
            {
                if (person.syn == null)
                    return;

                person.syn.ResurrectNow();
                ILogger.Instance.Send($"SendPlayerDeathInfoResponseOnArena()... ResurrectRequest TimeProvider.UTCNow: {TimeProvider.UTCNow.ToShortTimeString()}");
            });
        }
    }
}
