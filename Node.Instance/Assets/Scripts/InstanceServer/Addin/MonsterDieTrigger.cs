﻿using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Actions;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Conditions;
using System.Collections.Generic;
using System.Xml.Serialization;
using UtilsLib;
using UtilsLib.Logic;
using System;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.Utils.Logger;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Batyi.TinyServer;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class MonsterDieTrigger : IfThenScenario
    {
		[XmlAttribute("OnlyNames")]
		public List<string> names = new List<string>();

        public override void Start(Map sceneMap)
        {
            map = sceneMap;
            map.MonsterDie += Check;
        }

        private void Check(MonsterBehaviour monster, MapTargetObject attacker)
        {
            if (names != null && names.Count != 0 && !names.Contains(monster.Data.Name))
                return;

            var context = new ContextIAction(map);
            context.actor = attacker as PersonBehaviour;
            context.target = monster;
            BaseCheck(context);
        }

        public override string ToString()
        {
            return "MonsterDieTrigger";
        }
    }
}
