﻿using System;
using System.Collections.Generic;
using System.Linq;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.InstanceServer.Games;
using UtilsLib.BatiyScript;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Networking;
using Assets.Scripts.InstanceServer.Analytic;
using Assets.Scripts.Networking.ClientDataRPC.Arenas;

namespace Assets.Scripts.InstanceServer.Addin
{
    public enum GameResultForTeam
    {
        Lose = -1,
        Draw = 0,
        Win = 1,
        NoMatter = 2,
    }

    public enum GameResult
    {
        None = 0,
        RedWin = 1,
        BlueWin = 2,
        Draw = 3,
    }

    public enum ArenaRewardSlotType
    {
        Free = 0,
        VIP = 1,
        VIP_diffLevel = 2,
        GoldenShield = 3,
        ArenaActivity = 4,
        Advertising = 5
    }

    public class ArenaReward : Scenario
    {
        public FormulaX ExpReward;
        public FormulaX GloryReward;

		public List<PlaceReward> Rewards;

        protected StatisticBase statistic;

        public event Action<InstancePerson, int, int, int> OnBattleSendReward;

        public class PlaceReward
		{
			[XmlAttribute]
			public int Place = Int32.MaxValue;

            [XmlAttribute]
            public int WaveId = Int32.MinValue; //для волн

            [XmlAttribute]
			public string Reward;
			[XmlAttribute]
			public ArenaRewardChestType ChestType;
		}

		public override void Start(Map sceneMap)
		{
            map = sceneMap;
            statistic = map.Scenarios.OfType<StatisticBase>().First();
        }

		public virtual MatchBase.PersonArenaReward GetReward(InstancePerson instancePerson, ArenaPlayerInfo playerInfo, bool AddExp, GameResultForTeam gameResult)
		{
			PersonExperienceData personExperienceData = new PersonExperienceData();
			if (playerInfo == null)
			{
				ILogger.Instance.Send($"GetReward() playerInfo was null, loginId = {instancePerson.loginId}, personId = {instancePerson.syn.Id}, nickname = {instancePerson.syn.NickName}", ErrorLevel.warning);
				return new MatchBase.PersonArenaReward
				{
					ChestType = ArenaRewardChestType.Wood,
					rewardList = new List<ArenaRewardData>(),
					personExperienceData = personExperienceData
				};
			}

			//вызывается до изменения количества игр.. 
			if (AddExp)
			{
				int expReward = ExpReward?.IntCalc(instancePerson.StatContainer, playerInfo.gameCount, gameResult == GameResultForTeam.Win ? 1 : 0, playerInfo.FullScore) ?? 0;
				int gloryReward = GloryReward?.IntCalc(instancePerson.StatContainer, playerInfo.gameCount, gameResult == GameResultForTeam.Win ? 1 : 0, playerInfo.FullScore) ?? 0;

				if (expReward > 0 || gloryReward > 0)
				{
					statistic.GetEloRatingInfo(instancePerson, out int dif, out int elo);

					BattleSendReward(instancePerson, expReward, gloryReward, dif);

					personExperienceData = ApplyArenaEndPlayerParams(instancePerson, expReward, gloryReward, elo, dif);
				}

				ILogger.Instance.Send($"GetReward() loginId = {instancePerson.loginId}, personId = {instancePerson.syn.Id}, nickname = {instancePerson.syn.NickName}, expReward = {expReward}, gloryReward = {gloryReward}", ErrorLevel.info);
			}

			var rewardList = new List<ArenaRewardData>();
			var reward = Rewards.FirstOrDefault(x => playerInfo.Place <= x.Place);
			if (reward != null)
			{
				var lootlist = LootLists.Instance.GetLoots(reward.Reward);
				foreach (var item in lootlist.GetItemList(instancePerson.syn, instancePerson))
					rewardList.Add(new ArenaRewardData { SlotId = rewardList.Count, ItemData = item });
			}

			return new MatchBase.PersonArenaReward
			{
				ChestType = reward?.ChestType ?? ArenaRewardChestType.Wood,
				rewardList = rewardList,
				personExperienceData = personExperienceData
			};
		}

        protected void BattleSendReward(InstancePerson instancePerson, int expReward, int gloryReward, int dif)
        {
            try
            {
                OnBattleSendReward?.Invoke(instancePerson, expReward, gloryReward, dif);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"OnBattleSendReward exception:{ex}", ErrorLevel.exception);
            }
        }

        public static PersonExperienceData ApplyArenaEndPlayerParams(InstancePerson instancePerson, int exp, int glory, int oldElo, int difElo)
        {
            int prevLevel = instancePerson.Level;
            instancePerson.AddExperience(ref exp);

            int oldGlory = instancePerson.login.GloryPoint;
            instancePerson.login.AddGloryPoint(glory);

            int goldAdd = 0;
            int emeraldsAdd = 0;
            bool newLevel = prevLevel != instancePerson.Level;

            PersonExperienceData personExperienceData = new PersonExperienceData()
            {
                NewLevel = newLevel,
                Exp = exp,
                Glory = glory,
                OldElo = oldElo,
                DifElo = difElo,
                Level = instancePerson.Level,
                OldGlory = oldGlory
            };

            
            if (newLevel)
            {
                goldAdd = CommonFormulas.Instance.AddGoldForLevel.IntCalc(instancePerson.Level);
                emeraldsAdd = CommonFormulas.Instance.AddEmeraldsForLevel.IntCalc(instancePerson.Level);

                personExperienceData.GoldAdd = goldAdd;
                personExperienceData.EmeraldsAdd = emeraldsAdd;
                var analyticInfo = new AnalyticCurrencyInfo()
                {
                    needSendInWallet = true,
                    sendGain = true,
                    Amount = goldAdd,
                    Currency = ResourceType.Gold,
                    USDCost = 0f,
                    Source = SourceGain.LevelUp,
                    SourceType = instancePerson.Level.ToString(),
                    PersonLevel = instancePerson.Level,
					TestPurchase = instancePerson.login.settings.testPurchase
				};
				//LoginEvent? instancePerson.login.GetMaxPersonLevel().ToString() : .ToString();

				instancePerson.TryAddGold(goldAdd, analyticInfo);
                InstanceGameManager.Instance.ShopManager.EmeraldCheat(instancePerson, emeraldsAdd);

                analyticInfo.Amount = emeraldsAdd;
                analyticInfo.Currency = ResourceType.Emerald;

                AnalyticsManager.Instance.SendData(instancePerson, analyticInfo);
            }

            return personExperienceData;
        }
    }
}
