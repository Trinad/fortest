﻿using Assets.Scripts.InstanceServer.Games;
using Assets.Scripts.Networking.ClientDataRPC;
using System;
using System.Collections.Generic;
using System.Text;
using UtilsLib.Logic.Enums;
using Assets.Scripts.InstanceServer.Enums;
using System.Linq;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Newtonsoft.Json;
using Assets.Scripts.Networking.ClientDataRPC.Arenas;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class ArenaRewardWaves : ArenaReward
    {
		public override MatchBase.PersonArenaReward GetReward(InstancePerson instancePerson, ArenaPlayerInfo playerInfo, bool AddExp, GameResultForTeam gameResult)
		{
            PersonExperienceData personExperienceData = new PersonExperienceData();
            if (playerInfo == null)
            {
                ILogger.Instance.Send($"GetReward() playerInfo was null, loginId = {instancePerson.loginId}, personId = {instancePerson.syn.Id}, nickname = {instancePerson.syn.NickName}", ErrorLevel.warning);
                return new MatchBase.PersonArenaReward
                {
                    ChestType = ArenaRewardChestType.Wood,
                    rewardList = new List<ArenaRewardData>(),
                    personExperienceData = personExperienceData
                };
            }

			//вызывается до изменения количества игр.. 
			if (AddExp)
            {
                int expReward = ExpReward?.IntCalc(instancePerson.StatContainer, playerInfo.gameCount, gameResult == GameResultForTeam.Win ? 1 : 0, playerInfo.FullScore) ?? 0;
                int gloryReward = GloryReward?.IntCalc(instancePerson.StatContainer, playerInfo.gameCount, gameResult == GameResultForTeam.Win ? 1 : 0, playerInfo.FullScore) ?? 0;

                if (expReward > 0 || gloryReward > 0)
                {
                    statistic.GetEloRatingInfo(instancePerson, out int dif, out int elo);

                    BattleSendReward(instancePerson, expReward, gloryReward, dif);

                    personExperienceData = ApplyArenaEndPlayerParams(instancePerson, expReward, gloryReward, elo, dif);
                }

                ILogger.Instance.Send($"GetReward() loginId = {instancePerson.loginId}, personId = {instancePerson.syn.Id}, nickname = {instancePerson.syn.NickName}, expReward = {expReward}, gloryReward = {gloryReward}", ErrorLevel.info);
            }

            var rewardList = new List<ArenaRewardData>();
            var reward = Rewards.FindAll(x => playerInfo.LastComplitedWaveId == x.WaveId).FirstOrDefault(x => playerInfo.Place <= x.Place);            
            if (reward != null)
            {
                var lootlist = LootLists.Instance.GetLoots(reward.Reward);
                foreach (var item in lootlist.GetItemList(instancePerson.syn, instancePerson))
                    rewardList.Add(new ArenaRewardData { SlotId = rewardList.Count, ItemData = item });
			}
			else
			{
				ILogger.Instance.Send($"ArenaRewardWaves GetReward() reward = null {JsonConvert.SerializeObject(playerInfo, Formatting.Indented)}, ", ErrorLevel.error);
			}

            return new MatchBase.PersonArenaReward
            {
                ChestType = reward?.ChestType ?? ArenaRewardChestType.Wood,
                rewardList = rewardList,
                personExperienceData = personExperienceData
            };
        }
    }
}
