﻿using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Conditions;
using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Games;
using Node.Instance.Assets.Scripts.InstanceServer.Addin;
using UtilsLib.DegSerializers;
using Assets.Scripts.InstanceServer.Addin.AnalyticsScenarios;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class ScenariosXML
    {
        [XmlElement(typeof(DungeonScenario))]
        [XmlElement(typeof(Champions))]
        [XmlElement(typeof(ChampionsCurse))]
        //[XmlElement(typeof(Keywardens))]
        [XmlElement(typeof(QuestsAllDoneTrigger))]
        [XmlElement(typeof(QuestDoneTrigger))]
        [XmlElement(typeof(MonsterDieTrigger))]
        [XmlElement(typeof(StartLocationTrigger))]
        [XmlElement(typeof(DailyQuestsScenario))]
        [XmlElement(typeof(NewPersonTrigger))]
        [XmlElement(typeof(TimeOneSecTrigger))]
        [XmlElement(typeof(VisitLocationTrigger))]
        [XmlElement(typeof(StartLocationPeriodTrigger))]
        [XmlElement(typeof(MyTrigger))]
        [XmlElement(typeof(MyPeriodTrigger))]
        [XmlElement(typeof(Protection))]
        [XmlElement(typeof(StaticMonster))]
        [XmlElement(typeof(LogMapActivObject))]
        [XmlElement(typeof(DeathmatchSolo))]
        [XmlElement(typeof(DeathmatchSoloStatistic))]
        [XmlElement(typeof(DeathmatchTeam))]
        [XmlElement(typeof(DeathmatchTeamStatistic))]
        [XmlElement(typeof(CTF))]
        [XmlElement(typeof(CTFStatistic))]
        [XmlElement(typeof(WavesCoop))]
        [XmlElement(typeof(WavesCoopStatistic))]
        [XmlElement(typeof(GoldenShield))]
        [XmlElement(typeof(LobbyDailyReward))]
        [XmlElement(typeof(SetupInteractions))]
        [XmlElement(typeof(ArenaReward))]
        [XmlElement(typeof(ArenaRewardWaves))]
        [XmlElement(typeof(AddPersonBehaviourParts))]
        [XmlElement(typeof(ResurrectTimer))]
        [XmlElement(typeof(TimerChestSilver))]
        [XmlElement(typeof(TimerChestGold))]
        [XmlElement(typeof(ItemUpgrader))]
        [XmlElement(typeof(KillAnnounce))]
        [XmlElement(typeof(KillAnnounceOld))]
        [XmlElement(typeof(BrawlArena))]
        [XmlElement(typeof(BrawlStatistic))]
        [XmlElement(typeof(MinimapTeamColorIcons))]
        [XmlElement(typeof(MinimapSpecialIcons))]
        [XmlElement(typeof(BattleLogAggregator))]
        [XmlElement(typeof(InitRaiting))]
        [XmlElement(typeof(DamageLogger))]
        [XmlElement(typeof(KilSeriesLogger))]
        [XmlElement(typeof(GoldStatistic))]
		[XmlElement(typeof(SpecialEventSchedule))]
		[XmlElement(typeof(ProgressLineScenario))]
		[XmlElement(typeof(NotificationScenario))]
		[XmlElement(typeof(CreditRatingScenario))]
        [XmlElement(typeof(RetentionSender))]
        [XmlElement(typeof(LoyalUserSender))]
        [XmlElement(typeof(RenameController))]
        [XmlElement(typeof(SourceBuilding))]
        [XmlElement(typeof(PerksController))]
        [XmlElement(typeof(PerkPointScenario))]
        [XmlElement(typeof(AFKScenario))]
        [XmlElement(typeof(AnalyticMatchingScenario))]
        [XmlElement(typeof(AnalyticMatchBattleScenario))]
		[XmlElement(typeof(TeamDistributor_OneTeams))]
        [XmlElement(typeof(TeamDistributor_TwoTeams))]
        [XmlElement(typeof(TeamDistributor_DifferentTeams))]
        [XmlElement(typeof(AnalyticPlayerBattleScenario))]
        [XmlElement(typeof(MatchesPlayedScenario))]
        [XmlElement(typeof(AnalyticCharacterCustomization))]
        [XmlElement(typeof(AnalyticsAccountProgress))]
        [XmlElement(typeof(TechworksScenario))]
        [XmlElement(typeof(AnalyticEventUser))]
        [XmlElement(typeof(AnalyticQuestComplete))]
        [XmlElement(typeof(AnalyticLoginEntered))]
        [XmlElement(typeof(TornadoScenario))]
        [XmlElement(typeof(AddAttackToMonstersScenario))]
		[XmlElement(typeof(RateApp))]
		[XmlElement(typeof(AdvertisingRevard))]
		[XmlElement(typeof(PathfinderDeep))]
		[XmlElement(typeof(DeathNotResurrect))]
		[XmlElement(typeof(DeathAnnonce))]
		[XmlElement(typeof(ChangeBotName))]
		[XmlElement(typeof(InfluenceSetter))]
		public List<Scenario> Scenarios;
    }

    public abstract class Scenario: ICanCopy
    {
        public bool CanCopy = true;

        protected Map map;

        public abstract void Start(Map sceneMap);

        public virtual void Update()
        {
        }

        public virtual void Init(Map sceneMap)
        {
        }
    }

    public abstract class IfThenScenario: Scenario
    {
        public List<FormulaCondition> If;

        public IActionList Then;

        public override void Init(Map sceneMap)
        {
            map = sceneMap;
        }

        protected void BaseCheck(InstancePerson person)
        {
            foreach (var condition in If)
            {
                if (!condition.Check(map, person))
                    return;
            }

            map.Run(Then.Actions, person);
        }

        protected void BaseCheck(ContextIAction context)
        {
            foreach (var condition in If)
            {
                if (!condition.Check(map, context.actor, context.target))
                    return;
            }

            map.Run(Then.Actions, context);
        }
    }
}
