﻿using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Addin
{
    public interface INotifyController
    {
        void SendShowNotificationTehNameResponse(InstancePerson instancePerson, NotifyState notifyState, string notifyName, params object[] notifyParam);
        void SendShowNotificationTehNameResponse(Map map, NotifyState notifyState, string notifyName, params object[] notifyParam);
        void SendShowNotificationResponse(InstancePerson instancePerson, NotifyState notifyState, params object[] notifyParam);
    }
}