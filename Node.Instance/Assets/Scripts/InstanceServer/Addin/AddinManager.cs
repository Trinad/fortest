﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilsLib.Logic;
using System.IO;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using System.Collections;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.Utils.Logger;
using UtilsLib.DegSerializers;
using UtilsLib.NetworkingData;
using Zenject;
using Fragoria.Common.Utils;

namespace Assets.Scripts.InstanceServer.Addin
{
    public sealed class AddinManager
    {
		private readonly List<Scenario> scenarios;
        private readonly List<AddinLocation> AddinLocations;
        private readonly string m_addinPath;
        private readonly Func<Type, object> creator;

		public Action<List<Scenario>> OnLoaded;// НЕ СТАВИТЬ ПОСЛЕ Inject

		public AddinManager(PathBuilder pathBuilder, GameXmlData gameXmlData, Func<Type, object> creator)
	    {
            AddinLocations = gameXmlData.AddinLocations;
		    scenarios = new List<Scenario>();
		    m_addinPath = pathBuilder.AddinPath;
            this.creator = creator;
        }

        public List<string> GetAddinNames(string levelName)
        {
            var addinNames = new List<string>();
            foreach (var addin in AddinLocations)
            {
                if (addin.Name.Contains(levelName) && addin.listAddins != null)
                    addinNames.AddRange(addin.listAddins);
            }
            return addinNames;
        }

        public void Load(string levelName)
        {
            var addinNames = GetAddinNames(levelName);
            Load(addinNames);
			OnLoaded?.Invoke(scenarios);

			if (creator != null)
            {
                DIInjector.Inject(scenarios, creator);
            }
            else
            {
                DIInjector.Inject(scenarios, type => null);
            }
        }

        public void Load(List<string> fileNames)
        {
            ILogger.Instance.Send("Added AddinManager {Load}");
            if (fileNames == null)
                return;

            scenarios.Clear();
            foreach (var name in fileNames)
            {
                var fullPath = Path.Combine(m_addinPath, name);
                ScenariosXML locationScenarios = fullPath.LoadFromFileXml<ScenariosXML>();
                scenarios.AddRange(locationScenarios.Scenarios);
            }
        }

		public List<Scenario> GetScenariosClons()
        {
            var curMapScenarios = new List<Scenario>();

            foreach (var scenario in scenarios)
            {
                if (scenario.CanCopy)
                    curMapScenarios.Add(scenario.Copy());
                else
                    curMapScenarios.Add(scenario);
            }

            return curMapScenarios;
        }
    }
}
