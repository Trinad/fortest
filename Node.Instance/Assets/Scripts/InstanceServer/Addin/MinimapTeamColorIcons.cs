﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using System.Collections.Generic;
using UtilsLib.Logic.Enums;
using System;
using System.Xml.Serialization;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class MinimapTeamColorIcons : MinimapIconsScenario
    {
        [XmlAttribute]
        public MinimapItemType RedTeamIcon = MinimapItemType.Person;

        [XmlAttribute]
        public MinimapItemType BlueTeamIcon = MinimapItemType.Person;

        [XmlAttribute]
        public MinimapItemType NoneTeamIcon = MinimapItemType.Person;

        [XmlAttribute]
        public bool IncludeMonsters = false;

        public  MinimapItemType GetMinimapType(MapTargetObject target)
        {
            if (target.GetStatContainer().GetStatValue(Stats.RedFlag) > 0)
                return MinimapItemType.PlayerWithFlag;

            if (target.GetStatContainer().GetStatValue(Stats.BlueFlag) > 0)
                return MinimapItemType.PlayerWithFlag;

            switch (target.TeamColor)
            {
                case PlayerTeam.Team2: return RedTeamIcon;
                case PlayerTeam.Team1: return BlueTeamIcon;
                default: return NoneTeamIcon;
            }            
        }

        public override Dictionary<MapTargetObject, MinimapItemType> MinimapResult()
        {
            Dictionary<MapTargetObject, MinimapItemType> minimapDatas = new Dictionary<MapTargetObject, MinimapItemType>();

            //живые персонажи
            foreach (var p in map.persons)
            {
                if (p.IsDead)
                    continue;

                minimapDatas.Add(p, GetMinimapType(p));
            }

            //боты
            foreach (var m in map.monsters)
            {
                if (!m.IsLiveObject || m.IsDead)
                    continue;

                if (m.characterType == CharacterType.Person)
                {
                    minimapDatas.Add(m, GetMinimapType(m));
                }
                else
                {
                    if (IncludeMonsters)
                        minimapDatas.Add(m, GetMinimapType(m));
                }
            }

            return minimapDatas;
        }
    }
}
