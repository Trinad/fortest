﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Analytic;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Assets.Scripts.InstanceServer.Entries.XmlData.Trade;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Batyi.TinyServer;
using Fragoria.Common.Utils;
using uLink;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class SpecialEventSchedule : Scenario //этот сценарий по задумке изменяет globalEvents.ScenarioString и еще много чего
    {
		[XmlElement]
		public List<SpecialEventScenarioData> events;

        public SpecialEventSchedule()
        {
            CanCopy = false;
        }

        private GlobalEvents globalEvents;
        private GameXmlData gameXmlData;
        private PersonsOnServer personsOnServer;
        private ILoginController loginController;
        private AnalyticsManager analyticsManager;
		private MatchmakingController matchmakingController;

        private List<InventoryItemData> itemDataList = new List<InventoryItemData>(); 

        [XmlInject]
        public void Construct(GlobalEvents _globalEvents, INetworkView view, GameXmlData _gameXmlData, PersonsOnServer _personsOnServer,
			ILoginController _loginController, AnalyticsManager _analyticsManager, MatchmakingController _matchmakingController)
        {
            personsOnServer = _personsOnServer;
            gameXmlData = _gameXmlData;
            globalEvents = _globalEvents;
            loginController = _loginController;
			analyticsManager = _analyticsManager;
			matchmakingController = _matchmakingController;

            view.RegisterListener(this);

            globalEvents.EventXmlConfigChange += GlobalScenarioStringUpdate;
            GlobalScenarioStringUpdate(EventXmlConfig.Instance);
        }

		public override void Start(Map sceneMap)
        {
			//sceneMap.PlayerConnected += (person, reconnect, changePerson) => UpdateEventForPerson(person);
        }

		public void UpdateEventForPerson(InstancePerson person)
		{
			var specialEvent = GetCurrentSpecialEvent();

			//Выдадим квест-термометр, если его нет
			if (specialEvent != null)
			{
				var questId = specialEvent.QuestName;
				if (!person.HaveQuest(questId))
				{
					person.AddNewQuest(questId);
					person.RemoveResource(specialEvent.ResourceType);
				}
			}
		}

		private void SpecialEventCompensation(InstancePerson person)
		{
			var specialEvent = GetEndingSpecialEvent(person);
			if (specialEvent == null)
				return;

			var quest = person.ActiveQuest.FirstOrDefault(x => x.Name == specialEvent.QuestName);
			if (quest == null)
				return;

			//заберем награду
			foreach (var reward in quest.Rewards)
				TakeReward(person, quest, reward.rewardId, false);

			//Заберем квест-термометр
			person.ActiveQuest.Remove(quest);
			quest.Dispose();

			//Конвертация
			int count = person.CountResource(specialEvent.ResourceType);
			if (count > 0)
			{
                var analyticInfo = new AnalyticCurrencyInfo()
                {
                    needSendInWallet = true,
                    sendGain = false,
                    Amount = count,
                    Destination = SinkType.Exchange.ToString(),
                    DestinationType = AnalyticСurrency.gold.ToString(),
                    Currency = specialEvent.ResourceType,
					CurrencyEvent = specialEvent.Name,
                    Source = SourceGain.Exchange,
                    SourceType = AnalyticСurrency.other.ToString(),
                    USDCost = 0f,
                    PersonLevel = person.login.GetMaxPersonLevel(),
					TestPurchase = person.login.settings.testPurchase,
					transactionId = AnalyticsManager.GetSHA256(DateTime.UtcNow.ToString() + person.loginId.ToString())
				};

				//Заберем валюту
				person.RemoveResource(specialEvent.ResourceType, count, analyticInfo);

                analyticInfo.sendGain = true;
                analyticInfo.Currency = ResourceType.Gold;
                analyticInfo.Amount = count;

                //Выдать золото
                person.TryAddGold(count, analyticInfo);
            }
		}

        public void GlobalScenarioStringUpdate(EventXmlConfig config)
        {
            foreach (var dataKey in config.KeyDatas)
            {
                foreach (var data in dataKey.Datas)
                {
                    if (data.StartDate < TimeProvider.UTCNow && data.EndDate > TimeProvider.UTCNow && data.fEnabled.Calc())
                    {
                        if (globalEvents.ScenarioString.Get(dataKey.Key) != data.Value)
                        {
                            globalEvents.OnEventEnded(globalEvents.ScenarioString.Get(dataKey.Key));
                            globalEvents.ScenarioString.Add(dataKey.Key, data.Value);
                            globalEvents.ScenarioString.Add(dataKey.Key + "_endTime", data.EndDate.ToString());
                            //переслать клиентам команду про форму евентов и всю инфу о предметах
                            StartSpecialEvent();
                        }
                        break;
                    }
                }
            }
        }

        private float NextRecalc;

        public override void Update()
        {
            if (NextRecalc > Time.time)
                return;

            NextRecalc = Time.time + 1;

            GlobalScenarioStringUpdate(EventXmlConfig.Instance);
        }

		//Текущий эвент
        private SpecialEventScenarioData GetCurrentSpecialEvent()
		{
            string currentEvent = globalEvents.ScenarioString.Get("event");
            return events.FirstOrDefault(x=>x.Name == currentEvent);
		}

		//Завершенный эвент
		private SpecialEventScenarioData GetEndingSpecialEvent(InstancePerson person)
		{
			string currentEvent = globalEvents.ScenarioString.Get("event");
			foreach (var specialEvent in events)
				if (specialEvent.Name != currentEvent && person.ActiveQuest.Exists(x => x.Name == specialEvent.QuestName))
					return specialEvent;

			return null;
		}

		private void StartSpecialEvent()
        {
            var specialEvent = GetCurrentSpecialEvent();
            if (specialEvent != null)
            {
                var token = gameXmlData.ItemDataRepository.GetPrototypesByType(ItemType.Resource).FirstOrDefault(x => x.Resource == ResourceType.Token);
				if (token != null)
					token.Icon = specialEvent.TokenIcon;

                itemDataList = new List<InventoryItemData>();
                var lootlist = gameXmlData.LootListsXml.GetLoots("trader_in_inventary");

                foreach (var item in lootlist.GetItemList(null, null))
                    if (item.GetTradeInfoData().BuyCostType == specialEvent.ResourceType)
                        itemDataList.Add(item);

                foreach (var building in gameXmlData.PlayerCityData.Buildings)
                {
                    var tradeInfoData = building.TradeInfos.GetCurrentTradeOffer(TimeProvider.UTCNow);
                    if (tradeInfoData.BuyCostType == specialEvent.ResourceType)
                    {
                        var item = gameXmlData.ItemDataRepository.GetCopyInventoryItemData(building.Name, 1);
                        itemDataList.Add(item);
                    }
                }

                foreach (var buildingKit in gameXmlData.PlayerCityData.BuildingKits)
                {
                    var tradeInfoData = buildingKit.TradeInfos.GetCurrentTradeOffer(TimeProvider.UTCNow);
                    if (tradeInfoData.BuyCostType == specialEvent.ResourceType)
                    {
                        var item = gameXmlData.ItemDataRepository.GetCopyInventoryItemData(buildingKit.Name, 1);
                        itemDataList.Add(item);
                    }
                }

                globalEvents.OnEventStarted(specialEvent.Name);
            }

			if (Map.Edge.Type == LocationType.Camp)
			{
				foreach (var person in personsOnServer.GetAllPersons())
					if (person.bOnLine)
					{
						SpecialEventDataRequest(person);
						loginController.SendGetLobbyInfoResponse(person);
						loginController.SendGetSelectMapResponse(person);
						matchmakingController.GetAllPlayModeDatasRequest(person);
						loginController.GetLobbyBuildAllDataRequest(person);
					}
			}
			else
			{
				foreach (var person in personsOnServer.GetAllPersons())
					if (person.bOnLine)
					{
						loginController.SendGetSelectMapResponse(person);
						matchmakingController.GetAllPlayModeDatasRequest(person);
					}
			}
        }

		#region RPCs

        [RPC]
		public void SpecialEventDataRequest(InstancePerson person)
		{
			//Если есть завершенный эвент
			var endingSpecialEvent = GetEndingSpecialEvent(person);
			if (endingSpecialEvent != null)
			{
				var count = person.CountResource(endingSpecialEvent.ResourceType);

				//Если есть компенсация или игрок не в лобби, то показываем прошлый эвент
				if (count > 0 || Map.Edge.Type != LocationType.Camp)
				{
					var endingquestId = endingSpecialEvent.QuestName;
					var endingQuest = person.ActiveQuest.FirstOrDefault(x => x.Name == endingquestId);
					if (endingQuest == null)
						return;

					//Инфа о компенсации
					var endingData = new SpecialEventData(person, endingSpecialEvent, itemDataList, 0, endingQuest);
					endingData.CompensationCurrencyCount = count;
					ServerRpcListener.Instance.SendRpc(person.ClientRpc.SpecialEventDataResponse, endingData);
					return;
				}

				//Компенсации нету и игрок в лобби, то автоматически завершаем прошлый эвент
				SpecialEventCompensation(person);
			}

			var specialEvent = GetCurrentSpecialEvent();
            if (specialEvent == null)
            {
                ServerRpcListener.Instance.SendRpc(person.ClientRpc.SpecialEventDataResponse, new SpecialEventData());
                return;
            }

			UpdateEventForPerson(person);

			var questId = specialEvent.QuestName;
			var quest = person.ActiveQuest.FirstOrDefault(x => x.Name == questId);
			if (quest == null)
				return;

            string enddateString = globalEvents.ScenarioString.Get("event_endTime");
            DateTime enddate = DateTime.Parse(enddateString);

            int duration = (int)(enddate-TimeProvider.UTCNow).TotalSeconds + 2; //TODO: maximpr две секунды нужны чтобы наверняка
			var data = new SpecialEventData(person, specialEvent, itemDataList, duration, quest);

			ServerRpcListener.Instance.SendRpc(person.ClientRpc.SpecialEventDataResponse, data);
		}

		[RPC]
		public void SpecialEventCompensationRequest(InstancePerson person)
		{
			SpecialEventCompensation(person);
			SpecialEventDataRequest(person);
		}

		[RPC]
		public void TakeSpecialEventRewardRequest(InstancePerson currentPerson, int personId, int iReward)
		{
            var person = currentPerson.login.InstancePersons.FirstOrDefault(x => x.personId == personId);
            if (person == null)
            {
                ILogger.Instance.Send("TakeSpecialEventRewardRequest person == null", ErrorLevel.error);
                return;
            }

			var specialEvent = GetCurrentSpecialEvent();
			if (specialEvent == null)
				return;

			var quest = person.ActiveQuest.FirstOrDefault(x => x.Name == specialEvent.QuestName);
			if (quest == null)
				return;

			if (!TakeReward(person, quest, iReward.ToString(), true))
				return;

			ServerRpcListener.Instance.SendRpc(person.ClientRpc.TakeSpecialEventRewardResponse, iReward);
		}

		private bool TakeReward(InstancePerson person, QuestItemData quest, string rewardId, bool showChest)
		{
			var reward = quest.Rewards.FirstOrDefault(x => x.rewardId == rewardId);
			if (reward == null)
				return false;

			if (quest.TakeRewardList.Contains(reward.rewardId))
				return false; //Уже забрали

			if (reward.Count > quest.TasksDone)
				return false; //Еще не заслужили

			quest.TakeRewardList.Add(reward.rewardId);

			var itemList = reward.GetItemList(null, person);

			var source = ItemSourceType.None;

            var analyticInfo = new AnalyticCurrencyInfo
            {
                needSendInWallet = true,
                sendGain = true,
                USDCost = 0f,
                Source = SourceGain.Event,
                SourceType = quest.Name,
                PersonLevel = person.login.GetMaxPersonLevel(),
				TestPurchase = person.login.settings.testPurchase
			};

            if (reward.ChestType != ArenaRewardChestType.None)
			{
				source = ItemSourceType.AnyChest;
                analyticInfo.Source = SourceGain.Chest;
                analyticInfo.SourceType = ExpandedItemSourceType.EventChest.ToString();

				if (showChest)
					ServerRpcListener.Instance.SendRpc(person.ClientRpc.LobbyChestRewardResponse, itemList);
			}

            if (itemList.Count > 0)
                globalEvents.OnPersonGotEventReward(person);

            foreach (var item in itemList)
            {
                analyticInfo.Amount = item.Count;
                analyticInfo.Currency = item.Resource;
                person.AddItem(item, Source: source, analyticInfo:analyticInfo);
            }

			return true;
        }

		#endregion
	}
}
