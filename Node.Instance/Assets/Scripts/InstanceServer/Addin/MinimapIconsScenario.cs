﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries;
using UtilsLib.Logic.Enums;
using Assets.Scripts.Networking.ClientDataRPC;
using System;
using System.Xml.Serialization;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Addin
{
    public abstract class MinimapIconsScenario : Scenario
    {
        [XmlAttribute]
        public float PeriodUpdate = 0.5f;

        public override void Start(Map sceneMap)
        {
            map = sceneMap;
            map.MinimapManager.MinimapResultByMapType += MinimapResultByMapType;
        }

        private void MinimapResultByMapType(InstancePerson person, List<MinimapData> list)
        {
            foreach (var nowPair in currentCollection)
            {
                if (nowPair.Key != person.syn)
                    list.Add(new MinimapData(nowPair.Key, nowPair.Value));
            }
        }

        public abstract Dictionary<MapTargetObject, MinimapItemType> MinimapResult();
       

        private Dictionary<MapTargetObject, MinimapItemType> currentCollection = new Dictionary<MapTargetObject, MinimapItemType>();
        private float nextUpdateTime;
        public override void Update()
        {
            if (Time.time < nextUpdateTime)
                return;

            nextUpdateTime = Time.time + PeriodUpdate;

            Dictionary<MapTargetObject, MinimapItemType> nowCollection = MinimapResult();
            //добавим новые
            foreach (var nowPair in nowCollection)
            {
                if (!currentCollection.ContainsKey(nowPair.Key))
                    map.MinimapManager.SendAddObjectToAll(nowPair.Key, nowPair.Value);
            }

            //удалим исчезнувшие
            foreach (var nowPair in currentCollection)
            {
                if (!nowCollection.ContainsKey(nowPair.Key))
                    map.MinimapManager.SendRemoveObjectToAll(nowPair.Key, nowPair.Value);
            }

            //перешлем изменившиеся
            foreach (var nowPair in nowCollection)
            {
                if (currentCollection.ContainsKey(nowPair.Key) && currentCollection[nowPair.Key] != nowPair.Value)
                {
                    map.MinimapManager.SendRemoveObjectToAll(nowPair.Key, currentCollection[nowPair.Key]);
                    map.MinimapManager.SendAddObjectToAll(nowPair.Key, nowPair.Value);
                }
            }

            currentCollection = nowCollection;
        }
    }
}
