﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Actions;
using System;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.Utils;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial
{
	public abstract class IAction
	{
        public virtual bool Execute(PersonBehaviour person)
        {
            return true;
        }

        public virtual bool Execute(ContextIAction context)
        {
            return Execute(context.actor);
        }
    }

    public class IActionList : IAction
    {
        [XmlElement(typeof(RunMyTrigger))]
        [XmlElement(typeof(RunMyPeriodTrigger))]
        [XmlElement(typeof(StopMyPeriodTrigger))]
        [XmlElement(typeof(ShowMessage))]
        [XmlElement(typeof(SetScenarioString))]
        [XmlElement(typeof(SendAnalitycVisit))]
        [XmlElement(typeof(RemoveScenarioString))]
        [XmlElement(typeof(RandomRun))]
        [XmlElement(typeof(SetMapVariableDouble))]
        [XmlElement(typeof(SetMapVariableString))]
        [XmlElement(typeof(WaitKillPerson))]
        [XmlElement(typeof(WaitPersonHealth))]
        [XmlElement(typeof(ClearTutorial))]
        [XmlElement(typeof(SetTutorial))]
        [XmlElement(typeof(SetMiniTutorial))]
        [XmlElement(typeof(WaitDialog))]
        [XmlElement(typeof(WaitTime))]
		[XmlElement(typeof(WaitTeleport))]
        [XmlElement(typeof(KillPerson))]
        [XmlElement(typeof(MovePerson))]
        [XmlElement(typeof(SendLikeApp))]
        [XmlElement(typeof(AddItem))]
        [XmlElement(typeof(EquipItem))]
        [XmlElement(typeof(UnEquipItem))]
        [XmlElement(typeof(CheckItem))]
		[XmlElement(typeof(Check))]
		[XmlElement(typeof(WaitCheck))]
        [XmlElement(typeof(EquipQuest))]
        [XmlElement(typeof(UpdateQuest))]
        [XmlElement(typeof(WaitQuest))]
        [XmlElement(typeof(WaitQuestCompleted))]
        [XmlElement(typeof(CompliteQuest))]
        [XmlElement(typeof(WaitRewardQuest))]
        [XmlElement(typeof(RemoveQuest))]
		[XmlElement(typeof(IfDungeonQuestChange))]
        [XmlElement(typeof(LearnAbility))]
        [XmlElement(typeof(CreateNPC))]
        [XmlElement(typeof(CreateRobotPromouter))]
        [XmlElement(typeof(MoveNPC))]
        [XmlElement(typeof(AnimationNPC))]
        [XmlElement(typeof(AddEntityBehaviour))]
        [XmlElement(typeof(TutorialGizmoShowBag))]
        [XmlElement(typeof(DeleteNPC))]
        [XmlElement(typeof(ExplorationGameMode))]
        [XmlElement(typeof(InteractionMapObjectAdd))]
        [XmlElement(typeof(InteractionMapObjectRemove))]
        [XmlElement(typeof(InteractionMapObjectChange))]
        [XmlElement(typeof(InteractionMapObjectDisabled))]
        [XmlElement(typeof(MapObjectSettings))]
        [XmlElement(typeof(SetMapObjectLoot))]
        [XmlElement(typeof(CreateMapObject))]
		[XmlElement(typeof(CreateStopwall))]
        [XmlElement(typeof(RemoveStopwall))]
        [XmlElement(typeof(ArrowToMapObjectShow))]
        [XmlElement(typeof(ArrowToMapObjectHide))]
        [XmlElement(typeof(EffectToMapObjectShow))]
        [XmlElement(typeof(EffectToMapObjectHide))]
        [XmlElement(typeof(ShiningPathShow))]
        [XmlElement(typeof(ShiningPathHide))]
        [XmlElement(typeof(BuffStandSetting))]
        [XmlElement(typeof(WaitDestroy))]
        [XmlElement(typeof(CreateMinibossMonster))]
        [XmlElement(typeof(CreateMonster))]
		[XmlElement(typeof(MoveMonster))]
        [XmlElement(typeof(MonsterAggro))]
		[XmlElement(typeof(MonsterAggroInRadius))]
		[XmlElement(typeof(MonsterInfluenceAdd))]
		[XmlElement(typeof(MonsterInfluenceRemove))]
		[XmlElement(typeof(AnimationMonster))]
        [XmlElement(typeof(WaitMonsterDie))]
        [XmlElement(typeof(WaitMonsterGetDamage))]
		[XmlElement(typeof(AddLoot))]
        [XmlElement(typeof(WaitMapObjectDestroy))]
        [XmlElement(typeof(TutorialApproachToGolemResponse))]
        [XmlElement(typeof(TutorialCameraTarget))]
		[XmlElement(typeof(TutorialStateFinish))]
		[XmlElement(typeof(TutorialReward))]
        [XmlElement(typeof(PersonSettings))]
        [XmlElement(typeof(LoadLevel))]
        [XmlElement(typeof(SurfaceDamage))]
		[XmlElement(typeof(SendAnalytic))]
        [XmlElement(typeof(SendAnalyticArena))]
        [XmlElement(typeof(RandomRunChance))]
        [XmlElement(typeof(SetMonstersLevel))]
        [XmlElement(typeof(RandomTeleport))]
        [XmlElement(typeof(SendCreateFxResponseAction))]
        [XmlElement(typeof(AddEnergy))]
        [XmlElement(typeof(AddHealth))]
        [XmlElement(typeof(ApplyInfluence))]
        [XmlElement(typeof(ApplyBuffInfluence))]
        [XmlElement(typeof(StatAdd))]
        [XmlElement(typeof(StatusInflRemove))]
        [XmlElement(typeof(SendTeamDeathmatchDataResponse))]
        [XmlElement(typeof(SendGetRewardResponse))]
        [XmlElement(typeof(SendGetLobbyBuildAllDataResponse))]
        [XmlElement(typeof(TutorialExpBorder))]
        [XmlElement(typeof(TutorialEvent))]
        [XmlElement(typeof(WaitAskAboutTutorial))]
        [XmlElement(typeof(TutorialAnalyticsStep))]
        [XmlElement(typeof(ShowNotify))]
        [XmlElement(typeof(ShowBrawlNotify))]
        [XmlElement(typeof(ApplyInfluenceInstancePerson))]
        [XmlElement(typeof(AdapterIAction))]
		[XmlElement(typeof(ActionsOnTeam))]
		public List<IAction> Actions;
    }
}
