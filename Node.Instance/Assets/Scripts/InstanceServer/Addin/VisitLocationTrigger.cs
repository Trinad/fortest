﻿using System.Collections.Generic;
using System.Xml.Serialization;
using UtilsLib;
using UtilsLib.Logic;
using System;
using Assets.Scripts.InstanceServer.Entries;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class VisitLocationTrigger : IfThenScenario
    {
        [XmlAttribute]
        public string LevelName;

        public override void Start(Map sceneMap)
        {
            if (!string.IsNullOrEmpty(LevelName) && LevelName != ServerStartGameManager.levelName)
                return;
            map = sceneMap;
            map.VisitLocation += Check;
        }

        private void Check(InstancePerson person)
        {
            BaseCheck(person);
        }

        public override string ToString()
        {
            return "VisitLocationTrigger";
        }
    }
}
