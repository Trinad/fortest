﻿using System.Collections.Generic;
using System.Numerics;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Utils.Common;
using Assets.Scripts.Utils.JsonBlockClasses;
using Node.Instance.Assets.Scripts.Utils;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Addin
{
    public class LavaBehaviour : MonoBehaviour
    {
        public List<HitActionBase> HitActionList;
        public Map map;
        public SurfaceType SurfaceType { get; set; }
		public bool IsFly;

        float timer;
        public override void Update()
        {
            if (Time.time > timer)
            {
                foreach (var person in map.persons)
                {
                    if (person.IsFly != IsFly)
                        continue;

                    var surface = map.GetSurface(Helper.GetCell(person.position.X), Helper.GetCell(person.position.Z));
                    if (surface == SurfaceType && !person.IsDead)
                        OnHit(person);
                }

                foreach (var monster in map.monsters)
                {
                    var surface = map.GetSurface(Helper.GetCell(monster.position.X), Helper.GetCell(monster.position.Z));
                    if (surface == SurfaceType && !monster.IsDead)
                        OnHit(monster);
                }

                timer = Time.time + 0.2f;
            }
        }

        public MapObject LavaTargetObject; 
        public virtual void OnHit(MapTargetObject target)
        {
            if (HitActionList == null || HitActionList.Count == 0)
                return;

            foreach (var action in HitActionList)
                action.Execute(1, target, LavaTargetObject, null);

            //ILogger.Instance.Send("LavaBehaviour OnHit: " + target.name);
        }

        public void Init()
        {
            LavaTargetObject = new MapObject();
            LavaTargetObject.SetStatContainer(new SurfaceStatContainer());
            
            var obj = new MapObjectInfo
            {
                name = "Lava",
                id = MapTargetObject.objId++
            };
            LavaTargetObject.Init(obj, Vector3.Zero, map);
        }
    }
}
