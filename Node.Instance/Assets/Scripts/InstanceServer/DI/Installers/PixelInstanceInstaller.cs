﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Groups;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils.Db;
using Assets.Scripts.Utils.Logger;
using Fragoria.Common.Utils;
using Node.Instance.Assets.Scripts;
using uLink;
using UtilsLib.Logic;
using Zenject;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.InstanceServer.DI.Installers
{
	public class PixelInstanceInstaller : Installer
	{
		private InstanceCommandLineOptions m_options;

		public PixelInstanceInstaller(InstanceCommandLineOptions options)
		{
			m_options = options ?? throw new ArgumentNullException(nameof(options));
		}

		public override void InstallBindings()
		{
			Formulas.Init();

			// регистрируем зависимости
			InstallULink();
			InstallConfigsAndSettings();
			InstallManagers();
			InstallGroups();
			InstallTutorial();
			InstallNPC();
            InstallArena();
		    InstallCheats();
			InstallControllers();
		    InstallCheatsPanel();

            Container.Bind<DIListManagers>().AsSingle();
            //TODO_maximpr ServerStartGameManager создается не тут и NonLazy() похоже не имеет значения
            //у нас LoginController вызывает старт всего сервера
            //LoginController -> ServerRpcListener -> ServerStartGameManager
            Container.BindInterfacesAndSelfTo<ServerStartGameManager>()
				.AsSingle()
				.NonLazy();

			var xmlData = Container.TryResolve(typeof(GameXmlData));
			DIInjector.Inject(xmlData, type => Container.Resolve(type));
		}

		private void InstallULink()
		{
			const int networkViewId = 1;
			var network =  uLink.Network.Instance;
			var p2p = new NetworkP2P(network);
			var view = new uLink.NetworkView(network);
			view.RegisterListener(this);
			view.stateSynchronization = NetworkStateSynchronization.Off;
			view.SetManualViewID(networkViewId);
			view.OnEnable();

			network.statsCalculationInterval = 1.0f;

			Container.Bind<uLink.Network>().FromInstance(network).AsSingle();
			Container.Bind<uLink.NetworkP2P>().FromInstance(p2p).AsSingle();
			Container.Bind<uLink.INetworkView>().FromInstance(view).AsSingle();

			Container.Bind<uLinkManager>().AsSingle();
			Container.Bind<RegisterBitStreamCodecs>().AsSingle().NonLazy();
		}

        private void InstallCheatsPanel()
        {
            Container.Bind<CheatPanelManager>().AsSingle();
            Container.Bind<IDbConverter<CheatPanelData>>().To<CheatPanelDbConverter>().AsSingle();
        }

        private void InstallConfigsAndSettings()
		{
			Container.Bind<InstanceCommandLineOptions>().FromInstance(m_options).AsSingle();

			var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			var pathBuilder = new PathBuilder(Path.Combine(path, "Assets/StreamingAssets"));
			Container.Bind<PathBuilder>().FromInstance(pathBuilder).AsSingle();

			var appConfig = pathBuilder.InstanceXml.LoadFromFileXml<AppConfig>();
            appConfig.nlog.ConfigurationFileName = "Assets/StreamingAssets/cfg/NLog.xml";
			Container.Bind<AppConfig>().FromInstance(appConfig).AsSingle();

			var logger = new AsyncLogger(appConfig, new LobbyInstanceLogger(appConfig));
			logger.Init();
			logger.AddTag("srvType", "Instance");
			ILogger.Instance = logger;
			Container.Bind<ILogger>().FromInstance(logger).AsSingle();

			InstallLoadAllXmlsInParallel();

            Container.Bind(typeof(InstanceSqlAdapter))
                .FromResolveGetter<AppConfig, InstanceSqlAdapter>(config => new InstanceSqlAdapter(config.SqlConnectString, "Instance", logStateInfo: false))
            .AsSingle();

            Container.Bind<Func<Type, object>>().FromInstance((type) => Container.Resolve(type)).AsSingle();

            Container.Bind<BitStreamStringList>().AsSingle().NonLazy();

            ILogger.Instance.Send($"AppConfig: appConfig {appConfig.analyticConnect.port} appConfig {appConfig.analyticConnect.host}",ErrorLevel.info);
        }

		private void InstallLoadAllXmlsInParallel()
		{
			var measureXmlLoadTime = Stopwatch.StartNew();

			var pathBuilder = Container.Resolve<PathBuilder>();

            var gameXmlData = GameXmlData.LoadAllXmlsInParallel(pathBuilder, out List<object> objects);
            

            measureXmlLoadTime.Stop();
			ILogger.Instance.Send($"InstallLoadAllXmlsInParallel(): tasks complete in {measureXmlLoadTime.ElapsedMilliseconds} ms", ErrorLevel.info);

		    Container.Bind<GameXmlData>().FromInstance(gameXmlData).AsSingle();
			Container.BindInstances(objects.ToArray());
		}

		private void InstallManagers()
		{
			Container.BindInterfacesAndSelfTo<PersonsOnServer>().AsSingle();
			Container.BindInterfacesAndSelfTo<InstanceGameManager>().AsSingle();

            Container.Bind<GlobalEvents>().AsSingle();
            Container.Bind<ShopManager>().AsSingle();
            Container.Bind<EmotionManager>().AsSingle();
            Container.Bind<AddinManager>().AsSingle();
			Container.Bind<QuestCollection>().AsSingle();
            Container.Bind<SpecialActionManager>().AsSingle();
			Container.Bind<AnalyticsManager>().AsSingle().NonLazy();
			Container.Bind<ESport>().AsSingle();
            Container.Bind<PromoCodeManage>().AsSingle();
            Container.Bind<DungeonManager>().AsSingle();
			Container.Bind<NickNameGeneratorManager>().AsSingle();
			Container.Bind<EventManager>().AsSingle();
			Container.Bind<SQLManager>().AsSingle();
		}

		private void InstallGroups()
		{
            Container.BindInterfacesAndSelfTo<MapFactory>().AsSingle();
            Container.BindInterfacesAndSelfTo<InstancePersonFactory>().AsSingle();
            Container.BindInterfacesAndSelfTo<MonsterBehaviourFactory>().AsSingle();
            Container.BindInterfacesAndSelfTo<GroupPersonDataFactory>().AsSingle();
			Container.BindInterfacesAndSelfTo<GroupFactory>().AsSingle();
			

			Container.Bind<GroupManager>().AsSingle();
		}

		private void InstallTutorial()
		{
			Container.Bind<TutorialAlgorithm>().AsSingle();
		}

		private void InstallNPC()
		{
			Container.Bind<NPCTradeAlgorithm>().AsSingle();
		}

        private void InstallArena()
        {
            Container.Bind<ArenaAlgorithm>().AsSingle();
        }

	    private void InstallCheats()
	    {
	        Container.Bind<Cheats>().AsSingle();
        }

		private void InstallControllers()
		{
            InstallController<ShopController>();
            InstallController<ShopSender>();

            InstallController<ChatController>();
            InstallController<EmotionController>();

            InstallController<SpecialActionController>();

            InstallController<InstanceGameController>();
            InstallController<InstanceGameSender>();

			InstallController<MatchmakingController>();
			InstallController<EdgesMapController>();
			InstallController<LoginController>();
            InstallController<LobbyController>();
			InstallController<GroupController>();
			InstallController<GuildController>();
			InstallController<FriendController>();
			InstallController<MailNotifier>();
            InstallController<NPCTradeController>();
			InstallController<ServerRpcListener>();

            InstallController<AnalyticsSender>();
            //Container.Bind<IAnalyticsSender>().To<AnalyticsSender>();

		    InstallController<CheatPanelController>();
            InstallController<PromoCodeController>();
            InstallController<PromoCodeSender>();

		    InstallController<NotifyController>();
            InstallController<ServerRpcSender>();
            InstallController<DataHashController>();
        }

		private void InstallController<T>()
		{
			Container.BindInterfacesAndSelfTo<T>().AsSingle().NonLazy();
		}
	}
}
