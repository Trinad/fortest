﻿using Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using Assets.Scripts.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.Utils.Common;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries.ActiveObjects;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.InstanceServer.Entries.XmlData.AttackDatas;
using Assets.Scripts.Utils;
using LobbyInstanceLib.Networking;
using UtilsLib;
using UtilsLib.NetworkingData;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Actions;
using Assets.Scripts.Utils.JsonBlockClasses;
using Assets.Scripts.Utils.Logger;
using System.Globalization;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Games;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Assets.Scripts.Networking.ClientDataRPC;
using Newtonsoft.Json;
using Node.Instance.Assets.Scripts.InstanceServer.Enums;
using UtilsLib.ExternalLogs.Classes;
using Assets.Scripts.InstanceServer.Analytic;

namespace Assets.Scripts.InstanceServer
{
    public class Cheats
    {
        public static Regex PatternCheat = new Regex("/( )?(?<message>.+)");
        private static Regex argsParser = new Regex(@"(?<name>\w+)(?<args> .*)?");
        private static char[] separators = new char[] { ' ' };
        static Dictionary<string, Action<InstancePerson, string[]>> cheatDic = new Dictionary<string, Action<InstancePerson, string[]>>();
        static List<KeyValuePair<string, string>> cheatsDescr = new List<KeyValuePair<string, string>>();

        private ILoginController loginController;
		IEmotionController emotionController;
		ISpecialActionController specialActionController;
        ShopManager shopManager;
        NPCTradeAlgorithm npcTradeAlgorithm;
		NickNameGeneratorManager nickNameGeneratorManager;
        PathBuilder pathBuilder;
		private PersonsOnServer personsOnServer;
		private GuildController guildController;

        public static Action<string, InstancePerson> SendCheatChatMessage = delegate { };

        public Cheats(PathBuilder pathBuilder, IEmotionController emotionController,
			ISpecialActionController specialActionController, ShopManager shopManager, ILoginController loginController,
			NPCTradeAlgorithm npcTradeAlgorithm, NickNameGeneratorManager nickNameGeneratorManager,
			PersonsOnServer personsOnServer, GuildController guildController)
        {
            this.pathBuilder = pathBuilder;
            this.loginController = loginController;
            this.emotionController = emotionController;
            this.specialActionController = specialActionController;
            this.shopManager = shopManager;
            this.npcTradeAlgorithm = npcTradeAlgorithm;
            this.nickNameGeneratorManager = nickNameGeneratorManager;
			this.personsOnServer = personsOnServer;
			this.guildController = guildController;

			//cheatDic = new Dictionary<string, Action<InstancePerson, string[]>>();
			RegisterCheats();
            cheatsDescr.Sort((x, y) => string.Compare(x.Key, y.Key));
        }

        public static void Clear()
        {
            cheatDic.Clear();
            cheatsDescr.Clear();
        }

        public bool TryRunCheat(string InputMessage, InstancePerson person)
        {
            var m = PatternCheat.Match(InputMessage);
            if (m.Success)
            {
                string CheatmMessage = m.Groups["message"].Value;
                var m2 = argsParser.Match(CheatmMessage);
                var name = m2.Groups["name"].Value;
                name = name.ToLower();
                return RunCheat(CheatmMessage, person);
            }
            return false;
        }

        public bool TryRunCheatForAll(string InputMessage, InstancePerson person)
        {
            if (InputMessage.ToLower().StartsWith("/forall"))
            {
                InputMessage = InputMessage.Substring(7);
                foreach (var item in person.syn.map.persons)
                {
                    if (!TryRunCheat(InputMessage, item.owner))
                        return false;
                }
                return true;
            }
            return false;
        }

        public bool TryExecuteChatMessageAsCheat(InstancePerson instancePerson, string message, int room)
        {
            if (message.StartsWith("/")) //Проверяем сообщение на наличие читов и пробуем выполнить
            {
                if (!RemoteXmlConfig.Instance.IsUserCanApplyCheats(instancePerson.loginId))
                    return true;

                SendCheatChatMessage("run cheat: " + message, instancePerson);

                if (TryRunCheatForAll(message, instancePerson))
                {
                    return true;
                }

                if (TryRunCheat(message, instancePerson))
                {
                    return true;
                }

                SendCheatChatMessage("cheat: " + message + " fail!!", instancePerson);
                SendCheatChatMessage("for help write /help", instancePerson);
                return true;
            }
            return false;
        }

        public bool RunCheat(string message, InstancePerson p)
        {
            try
            {
                var m = argsParser.Match(message);
                var name = m.Groups["name"].Value;
                name = name.ToLower();
                var argsStr = m.Groups["args"].Value;
                string[] args = argsStr.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                Action<InstancePerson, string[]> action;
                if (cheatDic.TryGetValue(args.Length + name, out action))
                {
                    action(p, args);
                    return true;
                }
                else
                    ILogger.Instance.Send("не найден обработчик для чита " + message);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send(ex.ToString(), ErrorLevel.exception);
                ILogger.Instance.Send(new System.Diagnostics.StackTrace().ToString(), ErrorLevel.exception);
                ILogger.Instance.Send("при выполнении чита " + message + "что-то пошло не так", ErrorLevel.exception);
            }
            return false;
        }



        private void RegisterCheats()
        {
            RegisterCheat("PlayVideoClip", p =>
            {
                var obj = p.syn.map.npcs.Random();
                if (obj != null)
                {
                    var dir = new Vector3(1, -1, -1).normalized();
                    var pos = obj.position + new Vector3(0, 0.5f, 0) - 3 * dir;
                    ServerRpcListener.Instance.SendRpc(p.ClientRpc.PlayVideoClipResponse, pos, dir, 3f);
                }
            }, "Заменяет миникарту на миниклип со случайным NPC. Сделано для открытия ворот в данже");

            RegisterCheat("unlock", p => p.EnableEdgeLocationId = int.MaxValue, "разлочить все локации");
            RegisterCheat("unlock", (p, lockId) => p.EnableEdgeLocationId = lockId, "разлочить локации до определенной");
            RegisterCheat("LootItem", (p) => GetLootItem(p), "Делает 1000 предметов и пишет сколько из них выпало какого класса. Для теста");
            RegisterCheat("CompletedQuests", (p) => p.login.QuestsCompleted(), "Текущий квест считается выполненным и выдается следующий");
            RegisterCheat("ClearQuests", (p) => p.QuestsClear(x => true), "Удаляет все квесты, в том числе и дейли");
            RegisterCheat("GetQuest", (p, id) => p.AddNewQuest(id), "Выдаёт квест по номеру. Актуальный номер у Дементьева Саши, либо взять с вики на свой страх и риск");
			RegisterCheat("printQuests", (p) =>
			{
				var sb = new StringBuilder();
				sb.AppendLine();
				foreach (var quest in p.ActiveQuest)
					sb.AppendLine($"{quest.Type} {quest.Name} {quest.TasksDone}/{quest.TasksTotal}");

				SendCheatChatMessage.Invoke(sb.ToString(), p);
			}, "Выводит список квестов в чат");
            RegisterCheat<string, int>("GetItem", (p, name, count) =>
            {
                var item = p.gameXmlData.ItemDataRepository.GetCopyInventoryItemData(name, count);

				AnalyticCurrencyInfo analyticInfo = null;
				if (name == "Token")
				{
                    analyticInfo = new AnalyticCurrencyInfo()
					{
						Amount = count,
						Source = SourceGain.Cheating,
						SourceType = "GetItem",
						Destination = SinkType.Exchange.ToString(),
						DestinationType = "Token",
						sendGain = true,
						Currency = ResourceType.Token,
						PersonLevel = p.login.GetMaxPersonLevel(),
						needSendInWallet = true,
						TestPurchase = p.login.settings.testPurchase
					};
				}

				p.AddItem(item, analyticInfo: analyticInfo);

				
                //loginController.SendGetLobbyInfoResponse(p);
                //p.AddChangedItems(item);
            }
            , "(Id, count)getitem 5 999 - даст 999 штук предметов под номером 5");
            //RegisterCheat("GetItem", (p, baseId, count, level) => ItemGenerator.Instance.GetTestItem(p, baseId, count, level), "(Id, count, level) getitem 5 999 1 - даст 999 штук предметов зеленой рарности под номером 5. Рарность - 0..4");
            RegisterCheat("DropItems", (p) => p.DropItems(), "Очищает ваш мешок и куклу");
            RegisterCheat("GoldAdd", (p, count) => {
                AnalyticCurrencyInfo analyticInfo = new AnalyticCurrencyInfo()
                {
                    Amount = count,
                    Source = SourceGain.Cheating,
                    SourceType = "GoldAdd",
					sendGain = true,
                    Currency = ResourceType.Gold,
                    PersonLevel = p.login.GetMaxPersonLevel(),
                    needSendInWallet = true,
					TestPurchase = p.login.settings.testPurchase
				};
                p.TryAddGold(count, analyticInfo: analyticInfo); }, "Добавляет золото на персонажа. Работает как в + так и в -");
            RegisterCheat("Emerald",(p, count) => 
			{
				var level = p.login.GetMaxPersonLevel();
				var testPurchase = p.login.settings.testPurchase;
				Action<AccountChangeParams> action = (@params) => 
				{					
					AnalyticCurrencyInfo analyticInfo = new AnalyticCurrencyInfo()
					{
						Amount = -@params.summ, //TODO fil приходит минус надо разбираться
						Source = SourceGain.Cheating,
						SourceType = "Emerald",
						sendGain = true,
						Currency = ResourceType.Emerald,
						PersonLevel = level,
						needSendInWallet = true,
						TestPurchase = testPurchase
					};

				AnalyticsManager.Instance.SendData(p, analyticInfo);
				};

				shopManager.EmeraldCheat(p, count, action);
			}, "Добавляет изюм на персонажа. Работает как в + так и в -");
            RegisterCheat("EmeraldBuy", (p, count) => shopManager.EmeraldBuyCheat(p, count));

            RegisterCheat("delbablo", (p) =>
            {
                var emeraldCount = -(p.Emeralds - 1);
                shopManager.EmeraldCheat(p, emeraldCount);

                var goldCount = -(p.Gold - 1);
                AnalyticCurrencyInfo analyticInfo = new AnalyticCurrencyInfo()
                {
                    Amount = goldCount,
                    Source = SourceGain.Cheating,
                    SourceType = SourceGain.Cheating.ToString(),
                    sendGain = true,
                    Currency = ResourceType.Gold,
                    PersonLevel = p.login.GetMaxPersonLevel(),
					TestPurchase = p.login.settings.testPurchase,
					needSendInWallet = true

                };
                p.TryAddGold(goldCount, analyticInfo);
            });
            
            RegisterCheat<float, float>("pos", (p, x, z) => {
                var position = Helper.UltraCorrectYPosition(p.syn.map, new Vector3(x, 0, z));
                p.syn.ForseSetPosition(position);
                }, "поставить себя в выбранные координаты");
            RegisterCheat<float, float>("targetpos", (p, x, z) => {
                var target = p.syn.Target;
                target.position = Helper.UltraCorrectYPosition(p.syn.map, new Vector3(x, 0, z));
                p.syn.map.RegisterForTransformSend(p, ChangeTransformReliability.ReliableForced, target);
            }, "поставить цель в выбранные координаты");

            RegisterCheat("GetStat", (p, stat) => SendCheatChatMessage.Invoke(p.StatContainer.GetStatValue(stat).ToString(), p), "Добавляет стат по названию из чита stats");
            RegisterCheat("Stats", PrintStats, "Показывает текущие статы персонажа");
            
            //RegisterCheat("LostReward", (p, timeSec) => m_mailManager.SendLostRewardMailCheat(3, p.loginId, timeSec));
            //RegisterCheat("LostReward", (p) => m_mailManager.SendLostRewardMailCheat(3, p.loginId, 3 * 60));
			RegisterCheat("level", (p, level) =>
			{
				level = Math.Min(Math.Max(1, level), p.gameXmlData.ExpData.MaxLevel);
                var exp = p.StatContainer.GetStatValue(Stats.Exp);
                var addexp = (int)(CommonFormulas.Instance.LevelToExp.Calc(level) - 1 - exp);
                p.AddExperience(addexp);
				if (Map.Edge.Type == LocationType.Camp)
					loginController.SendGetLobbyInfoResponse(p);
			});

			RegisterCheat("leveln", (p, level) =>
			{
				foreach (var item in p.TradeItems)
				{
					if (item.Level <= level)
						item.IsNew = false;
				}

				p.potions.ForEachItem((item) =>
				{
					if(item!=null)
					if (item.Level <= level)
						item.IsNew = false;
				});

				foreach (var item in p.items)
				{
					if (item.Level <= level)
						item.IsNew = false;
				}

				
				ServerConstants.PotionUnlock[] potionLevelUnlock = new ServerConstants.PotionUnlock[p.gameXmlData.ServerConstantsXml.PotionLevelUnlock.Count];  //костыль чтобы чит не вмешиваясь никуда не слал нотификации о разблокировке новых слотов для потов
				p.gameXmlData.ServerConstantsXml.PotionLevelUnlock.CopyTo(potionLevelUnlock);																	//очищаю список с нифой для отправки нотификаций при разблокировке новых слотов
				p.gameXmlData.ServerConstantsXml.PotionLevelUnlock.Clear();																						//

				level = Math.Min(Math.Max(1, level), p.gameXmlData.ExpData.MaxLevel);
				var exp = p.StatContainer.GetStatValue(Stats.Exp);
				var addexp = (int)(CommonFormulas.Instance.LevelToExp.Calc(level) - 1 - exp);
				p.AddExperience(addexp);

				p.gameXmlData.ServerConstantsXml.PotionLevelUnlock = new List<ServerConstants.PotionUnlock>(potionLevelUnlock);//возвращаю исходные данные в PotionLevelUnlock что бы нотификации вновь работали если понадобится

				if (Map.Edge.Type == LocationType.Camp)
					loginController.SendGetLobbyInfoResponse(p);
			});

			RegisterCheat("getlevel", (p) =>
            {
                int currentRequimentExp = (int)p.StatContainer.GetStatValue("ExpTopBorder");
                SendCheatChatMessage.Invoke($"current level: {p.Level}/{CommonFormulas.Instance.PersonMaxLevel.Calc()}, exp: {p.StatContainer.GetStatValue(Stats.Exp)}/{currentRequimentExp}", p);
            }, "Получаем левел");

            RegisterCheat("full", (p) =>
            {
                Full(p, 30);
            }, "Заполняет инвентарь шмотками до 30 уровня всех рарностей, экипирует 30 уровень. Выдает 1 млн изюма и 10 млн золота.");
            RegisterCheat("full", (p, level) =>
            {
                Full(p, level);
            }, "(level) Тоже что и full но уровень задается параметром");

            RegisterCheat("hp", (p, percent) => p.syn.SetHealth(p.syn, (int)(p.syn.MaxHealth * (double)percent / 100)), "Восстановить здоровье в %. hp 100 восстановит полностью");
            RegisterCheat("sp", (p, percent) => p.syn.SetEnergy(p.syn.MaxEnergy * percent / 100), "Восстановить манну в %. sp 100 восстановит полностью");
            RegisterCheat("st", (p, percent) => p.syn.SetStamina(p.syn.MaxStamina * percent / 100), "Восстановить стамину в %. sp 100 восстановит полностью");
			RegisterCheat("chest", (InstancePerson p, int color) =>
			{
				var edgeMap = p.gameXmlData.EdgesMapXml.FirstOrDefault(x => x.location == Map.Edge);
				if (edgeMap != null)
					p.ScenarioStrings.Remove("chest_" + edgeMap.LocationId);

				int time = 24 * 60 * 60;
				var infl = new StatMultiInfluence(p.StatContainer, null, time, (Stats)((ushort)Stats.KeyBlue + color), PlayerStatus.None, 1);
                infl.AddInner((Stats)((ushort)Stats.KeyBlue + color));
                infl.FxId = (int)EffectType.dungeonkey_blue + color;
				infl.DeathRemove = true;
				infl.NeedSave = false;
				p.StatContainer.AddInfluence(infl);

				var actives = new List<string> {"ChestBlue", "ChestGreen", "ChestYellow"};
				var objInfo = new MapObjectInfo
				{
					name = "chest_time",
					bundleName = "location/common/chest_time",
					id = MapTargetObject.objId++,
					ActiveData = actives[color]
				};

				var pos = p.syn.position + p.syn.direction.Vector2ToVector3() * ServerConstants.Instance.ActiveMapObjectsNearRadius * 1.1f;
				var mapObject = p.syn.map.CreateMapObject(objInfo, pos);
				mapObject.view = uLinkHelper.CreateView(mapObject);
				ServerRpcListener.Instance.SendCreateEntity(mapObject);

			}, "Призвать сундук и выдать ключ от него");
            // повредить ближайшего моба
            RegisterCheat("Kill", (p, count) =>
            {
		        const float killRadius = 1.2f;
                var mapTargetObj = p.syn.map.GetNearestTarget(p.syn, killRadius, false);
                if (mapTargetObj != null)
                {
                    mapTargetObj.SetHealth(p.syn, count);
                }
                
            }, "(count)Повредить ближаешего моба на count hp. Если ближайших несколько не работает");

            // убить всех мобов ботов кроме башень
            RegisterCheat("KillAll", (p) =>
            {
                p.syn.map.monsters.ForEach((behaviour =>
                {
                    bool isBrawlTower = !string.IsNullOrEmpty(behaviour.ActiveData) &&
                                        (behaviour.ActiveData.Contains("tower") ||
                                         behaviour.ActiveData.Contains("turret"));
                    if (!isBrawlTower)
                    {
                        var oldHealth = behaviour.Health;
                        behaviour.SetHealth(p.syn, 0);
						if(behaviour.IsDead)
							p.syn.OnKill(behaviour, oldHealth);
                    }
                }));
            }, "Убивает всех мобов ботов на локации");

            // убить все башни
            RegisterCheat("KillAllT", (p) =>
            {

                for (var i = 0; i < p.syn.map.monsters.Count; i++)
                {
                    var behaviour = p.syn.map.monsters[i];
                    bool isBrawlTower = !string.IsNullOrEmpty(behaviour.ActiveData) &&
                                        (behaviour.ActiveData.Contains("tower") ||
                                         behaviour.ActiveData.Contains("turret"));
                    if (isBrawlTower)
                    {
                        var oldHealth = behaviour.Health;
                        behaviour.SetHealth(p.syn, 0);
						if (behaviour.IsDead)
							p.syn.OnKill(behaviour, oldHealth);
                    }
                }
            }, "Убивает все башни");

            RegisterCheat("KillT", (p, text) =>
            {

                for (var i = 0; i < p.syn.map.monsters.Count; i++)
                {
                    var behaviour = p.syn.map.monsters[i];
                    bool isBrawlTower = !string.IsNullOrEmpty(behaviour.ActiveData) &&
                                        (behaviour.ActiveData.Contains(text));
                    if (isBrawlTower)
                    {
                        var oldHealth = behaviour.Health;
                        behaviour.SetHealth(p.syn, 0);
						if (behaviour.IsDead)
							p.syn.OnKill(behaviour, oldHealth);
                    }
                }
            }, "Убивает сто напишешь и это есть в активДате");

            // убить всех мобов Mob = 2, ботов Person = 0,
            RegisterCheat("KillAll", (p, type) =>
            {
                p.syn.map.monsters.ForEach((behaviour =>
                {
                    if (behaviour.Data.CharacterType == (CharacterType)type)
                    {
						var oldHealth = behaviour.Health;
						behaviour.SetHealth(p.syn, 0);
						if (behaviour.IsDead)
							p.syn.OnKill(behaviour, oldHealth);
                    }
                }));
            }, "Убивает выборочно мобов на локации");
            
            // повредить всех мобов
            RegisterCheat("DamAll", (p, count) =>
            {
                p.syn.map.monsters.ForEach((behaviour =>
                {
                    behaviour.SetHealth(p.syn, count);
                }));
            }, "(count)Повредить всех мобов на локации на count hp");

            RegisterCheat("tutorial", (p, state) =>
            {
                p.bRunTutorial = true;
                p.TutorialState = state;
                p.ResetTutorialState();
            }, "(state) - переход к стэйту тутора. номера стэйтов в вики");
            RegisterCheat("tutorialAdd", (p, state) =>
            {
                p.bRunTutorial = true;
                p.TutorialStateAdd = state;
                p.ResetTutorialState();
            }, "Чит только дял BigD");
            RegisterCheat("skip", p =>
            {
                if (p.bRunTutorial)
                    p.ScenarioStrings.Add("SkipTutorial", "true");
            }, "Пропускаем туториал");
            RegisterCheat("clearScenarioParams", (p) => p.ScenarioStrings.scenarioParams.Clear(), "Удаляет текстовые сценарные переменные персонажа. Чит только для МаксаП");
            RegisterCheat("params", (p) => SendCheatChatMessage.Invoke(p.PersonalParam, p), "Выводит в чат текущие параметры персонажа. Те что для входа в чужой форт");
            RegisterCheat("callkit", (p, name, kit) => CreateMonster(p, name, 1, null, kit),
                "Призыв моба с именованным поведений на локацию по имени");
            RegisterCheat("call", (p, name) => CreateMonster(p, name, 1, null), 
                "Призыв моба на локацию по имени");
            RegisterCheat<string, int>("call", (p, name, level) => CreateMonster(p, name, level, null),
            "(name, level) Призыв моба на локацию по имени и уровню");
            RegisterCheat<string, int, string>("call", (p, name, level, lootName) => CreateMonster(p, name, level, lootName),
            "(name, level, lootName) Призыв моба на локацию по имени и уровню. К монстру привязывается лут с именем lootName");
            RegisterCheat("callpatrol", (p, name, dist) =>
            {
				var mob = CreateMonster(p, name, 1, null);
                if (mob == null)
                    return;

                var pos = p.syn.position;
                var pos1 = pos + Helper.Vector3forward.normalized() * 1;
                var pos2 = pos + Helper.Vector3forward.normalized() * Math.Max(1, dist);
                var action = new PatrolPoints
                {
                    bMainControl = true,
                    monster = mob,
                    points = new List<Vector3>
                    {
                        p.syn.map.GetNearTransparentWithMakePath(pos.X, pos.Z, pos1.X, pos1.Z),
                        p.syn.map.GetNearTransparentWithMakePath(pos.X, pos.Z, pos2.X, pos2.Z)
                    }
                };

                mob.attacks.Add(action);
                action.Init();
                mob.SetCurrentAttack(action);
            }, "Патруль. Заставляет ходить монстра с именем name по дорожке длиной sdist");
			RegisterCheat("callmany", (p) =>
			{
				var cm = new CreateMonster();
				cm.Construct(p.gameXmlData);
				cm.Name = "target"; //"tutorial_training_dummy";
				cm.Level = "1";
				cm.Kit = null;

				for(int x = -5; x <=5; x++)
					for (int z = -5; z <= 5; z++)
					{
						var pos = p.syn.position + Helper.Vector3forward.normalized() + new Vector3(x, 0, z) * Helper.VoxelSize * 4;
						var m = cm.GetMonster(p.syn.map, pos);
						cm.AddMonsterOnMap(m);
					}
			},
			"Призыв кучу мобов на локацию");

            RegisterCheat("burning", (p) =>
            {
                Bburning(p, 100, 0.35f, 1.5f);
            }, "(число эффектов = 100, плотность эффектов = 0.35, скорость распространения огня = 0.9)тестировать эффект поджигания");

            RegisterCheat<int, float, float>("burning", (p, count, density, speed) =>
            {
                Bburning(p, count, density, speed);
            }, "(число эффектов, плотность эффектов, скорость распространения огня) тестировать эффект поджигания");


            RegisterCheat("StayMonster", (p, name) =>
            {
				var mob = CreateMonster(p, name, 1, null);
                if (mob == null)
                    return;

                var action = new CommonEmpty()
                {
                    bMainControl = true,
                    monster = mob
                };

                mob.attacks.Add(action);
                action.Init();
                mob.SetCurrentAttack(action);
            }, "Создаёт монстра, который стоит и ничего не делает. полезно для отладки анимаций");

            RegisterCheat("AnimatorTrigger", (p, animatorParameterName) =>
            {
                MonsterBehaviour monster = p.syn.Target as MonsterBehaviour;
                if (monster != null)
                {
                    monster.AddTriggerAnimatorParameterForSend(animatorParameterName);
                }
            }, "Отсылает заданный параметр аниматора для выделенной цели.");

            RegisterCheat("AnimatorBool", (p, animatorParameterName, parameterValue) =>
            {
                MonsterBehaviour monster = p.syn.Target as MonsterBehaviour;
                if (monster != null)
                {
                    monster.AddBoolAnimatorParameterForSend(animatorParameterName, parameterValue);
                }
            }, "Отсылает заданный параметр аниматора для выделенной цели.");

            RegisterCheat("getelo", (p) => SendCheatChatMessage.Invoke("EloRating team: " + (int)p.StatContainer.GetStatValue(Stats.EloRatingTeam) + " coop: " + p.rating_elo_coop, p), "Выводит в чат эло рейтинг персонажа");
            RegisterCheat("setelo", (person, rating_elo_team, rating_elo_coop) =>
            {
                //person.rating_elo_solo = rating_elo_solo;
                person.StatContainer.SetStatValue(Stats.EloRatingTeam, rating_elo_team);
                person.rating_elo_coop = rating_elo_coop;
                ServerRpcListener.Instance.SendToRatingServer(person, RatingRequestType.None, ArenaMatchType.None, false, 1, person.syn?.map.LevelParam);
            },"Установить ELO рейтинги персонажа");

            RegisterCheat("addelo", (InstancePerson person) =>
            {
                //person.rating_elo_solo += 200;
                person.StatContainer.AddStat(Stats.EloRatingTeam, 200);
                person.rating_elo_coop += 200;
                ServerRpcListener.Instance.SendToRatingServer(person, RatingRequestType.None, ArenaMatchType.None, false, 1, person.syn?.map.LevelParam);
            }, "Установить ELO рейтинги персонажа");

			RegisterCheat("endwave", (person, sec) =>
			{
				WavesCoop wc = person.syn.map.Scenarios.FirstOrDefault(x => x is WavesCoop) as WavesCoop;
				if (wc != null)
					wc.endWaveByTime_Cheat(sec);
			}, "Заканчиваем текущую в волну по времени");

			RegisterCheat("endwave", (person) =>
			{
				WavesCoop wc = person.syn.map.Scenarios.FirstOrDefault(x => x is WavesCoop) as WavesCoop;
				if (wc != null)
					wc.endWaveByTime_Cheat(3);
			}, "Заканчиваем текущую в волну");

			RegisterCheat("waveId", (person, waveId, isKillAll) =>
            {
                WavesCoop wc = person.syn.map.Scenarios.FirstOrDefault(x => x is WavesCoop) as WavesCoop;
                if (wc != null)
                    wc.winWaves_Cheat(person, waveId, isKillAll);
            }, "перейти на новую волну");


            RegisterCheat("getbotlevel", (InstancePerson person) =>
            {
                float avgLevel = 0;
                int count = 0;
                foreach (var monster in person.syn.map.monsters)
                {

                    if (monster?.characterType == CharacterType.Person)
                    {
                        avgLevel += monster.Level;
                        count++;
                    }
                }
                if (count > 0)
                    SendCheatChatMessage.Invoke($"средний уровень ботов: {avgLevel / count}", person);
                else
                    SendCheatChatMessage.Invoke($"Боты на локации не найдены", person);
            }, "Показывает средний уровень ботов на локации");

            RegisterCheat("botsinfo", (InstancePerson person) =>
            {
                float avgLevel = 0;
                int count = 0;
                foreach (var monster in person.syn.map.monsters)
                {
                    if (monster.characterType == CharacterType.Person)
                    {
                        avgLevel += monster.Level;
                        count++;
                        SendCheatChatMessage.Invoke($"{monster.NickName}, команда {monster.TeamColor}, уровень {monster.Level}", person);

                        foreach (PersonAbility ability in monster.attacks.OfType<PersonAbility>())
                        {
                            SendCheatChatMessage.Invoke($"умение {ability.Type}", person);
                        }
                    }
                }
                if (count > 0)
                    SendCheatChatMessage.Invoke($"средний уровень ботов: {avgLevel / count}", person);
                else
                    SendCheatChatMessage.Invoke($"Боты на локации не найдены", person);
            }, "Показывает средний уровень ботов на локации");

            RegisterCheat("gettypes", (person) =>
            {
                int[] allPersonTypes = new int[(int)PersonType.Count];
                foreach (var monster in person.syn.map.monsters)
                {
                    var personType = monster.personType;
                    if(personType != PersonType.None && personType != PersonType.Count)
                        allPersonTypes[(int)personType]++;
                }

                foreach (var monster in person.syn.map.persons)
                {
                    var personType = monster.personType;
                    if (personType != PersonType.None && personType != PersonType.Count)
                        allPersonTypes[(int)personType]++;
                }

                var stringBuilder = new StringBuilder();
                for (int i = 0; i < allPersonTypes.Length; i++)
                {
                    var pCount = allPersonTypes[i];
                    stringBuilder.Append($" personType: {(PersonType)i}, pCount: {pCount} \n");
                }

                SendCheatChatMessage.Invoke($"количество всех типов на карте: \n {stringBuilder.ToString()}", person);
            });

            RegisterCheat("callobj", (p, objName) => CallMapObject(p, objName));
            RegisterCheat("loot", (InstancePerson person, int rType, int isPerson, int removeTime) =>
            {
                if (rType > Enum.GetNames(typeof(ResourceType)).Length - 1)
                    return;
                ResourceType resType = (ResourceType)rType;
                for (int i = 0; i < 10; i++)
                {
                    var resouce = new Resource { Type = resType, };
                    var getPos = person.syn.map.GetLootPos(person.syn.position);
                    MapLoot loot = null;
                    if (isPerson > 0)
                    {
                        loot = new MapLoot(resouce, person.syn, person.syn, person.syn.position, getPos, removeTime);
                    }
                    else
                    {
                        loot = new MapLoot(resouce, person.syn, null, person.syn.position, getPos, removeTime);
                    }

                    //ILogger.Instance.Send(string.Format("Cheat: resouce: {0}, getPos: {1}, loot: {2}",resouce.LootName, getPos, loot));

                    person.syn.map.loots.Add(loot);
                }
            }, "выкидывает лут по номеру rType (wiki), который виден всем (0) или тебе (1), на время removeTime");
            RegisterCheat("fx", (person, fxId) =>
            {
                ServerRpcListener.Instance.SendCreateFxResponse(person.syn, (EffectType)fxId, 0);
            }, "(fxId) Создает эффект над головой персонажа по номеру fxId");

            RegisterCheat<int, float>("fx", (person, fxId, time) =>
            {
                int effectInstanceId = ServerRpcListener.Instance.SendCreateFxResponse(person.syn, (EffectType)fxId, 0);
                CoroutineRunner.Instance.Timer(time, () =>
                {
                    ServerRpcListener.Instance.DestroyFxResponse(person.syn.map, effectInstanceId, Vector3.Zero, true);
                });
            }, "(fxId time) Создает эффект над головой персонажа по номеру fxId на время time");

            RegisterCheat("fxobj", (person, fxId, objName) => AddMapObjectFx(person, fxId, objName), "(fxId, objName) Создает эффект по номеру fxId, над объектом по его имени objName");
            RegisterCheat("fx", (person, fxId, time, count) =>
            {
                for (int i = 0; i < count; i++)
                {
                    var MapObjectExId = new MapEffect(person.syn.map)
                    {
                        fxId = (EffectType)fxId,
                        position = person.syn.position + new Vector3(i / 2f, 0, 0),
                    };
                    ServerRpcListener.Instance.SendCreateFxResponse(MapObjectExId);

                    int effectInstanceId = MapObjectExId.id;
                    CoroutineRunner.Instance.Timer(time, () =>
                    {
                        ServerRpcListener.Instance.DestroyFxResponse(person.syn.map, effectInstanceId, Vector3.Zero, true);
                    });
                }
            }, "(fxId, time, count) Создает count штук эффектов, немного смещенных относительно персонажа, по номеру fxId  и разрушает их через заданное время");

            RegisterCheat<int>("teamcolor", (person, teamIndex) =>
            {
                person.syn.TeamColor = (PlayerTeam)teamIndex;
                person.syn.SendRpc(person.ClientRpc.SetTeamColor, teamIndex);
            }, "(teamcolor) Изменяет команду текущего персонажа");

            RegisterCheat("fxmassive", (person, fxId, time, count, destroyTime) =>
            {
                for (int i = 0; i < count; i++)
                {
                    CoroutineRunner.Instance.Timer(i * time, () =>
                    {
                        Vector3 effectPosition = person.syn.position + new Vector3(FRRandom.Next(8f) - 4f, 0, FRRandom.Next(8f) - 4f);
                        effectPosition = Helper.UltraCorrectYPosition(person.syn.map, effectPosition);
                        var MapObjectExId = new MapEffect(person.syn.map)
                        {
                            fxId = (EffectType)fxId,
                            position = effectPosition,
                        };
                        ServerRpcListener.Instance.SendCreateFxResponse(MapObjectExId);

                        CoroutineRunner.Instance.Timer(destroyTime, () =>
                        {
                            ServerRpcListener.Instance.DestroyFxResponse(MapObjectExId);
                        });
                    });
                }
            }, "(fxId, time, count, destroyTime) Создает count штук эффектов, рандомно вокруг персонажа, по номеру fxId через интервал времени time и разрушает их через заданное время destroyTime");

            RegisterCheat("fxtarget", (InstancePerson person, int fxId, float time) =>
            {
                MonsterBehaviour monster = person.syn.Target as MonsterBehaviour;
                if (monster != null)
                {
                    monster.direction = GeometryAlgorithm.Vector3ToVector2(person.syn.position - monster.position).normalized();
                    int effectInstanceId = MapObject.objId++;
                    ServerRpcListener.Instance.CreateFxResponse_SearchingMissile(person.syn.map, effectInstanceId, monster.Id, (EffectType)fxId, 10, monster.position, person.syn.position, person.syn.Id);

                    CoroutineRunner.Instance.Timer(time, () =>
                    {
                        ServerRpcListener.Instance.DestroyFxResponse(person.syn.map, effectInstanceId, Vector3.Zero, true);
                    });
                }
            }, "(fxId, time) Создает эффект по номеру fxId и разрушает через заданное время");

            RegisterCheat("smile", (person, smileId) =>
            {
                ServerRpcListener.Instance.TutorialAddFrameResponse(person, person.syn, TutorialFrameType.Smiley, smileId);
            }, "Над персонажем появляется иконка эмоции по номеру");

            RegisterCheat("name", (p) => ShowMonsterName = !ShowMonsterName, "К именам мобов добавляется их серверное имя, хп, дамаг и уровень. Для работы мобы должны пересоздаться. Перезайти клиентом например");
            RegisterCheat("loginconflict", p => p.loginConflict = true, "Пишешь и выходишь в лобби. Видишь форму конфликта аккаунтов фб и гостевого. Там будет твой акк и случайный, гостевой или фб неважно");
            RegisterCheat<float, string>("statadd", (p, count, name) => p.StatContainer.AddStat((Stats)Enum.Parse(typeof(Stats), name), count), "(count , name) Повысить стат по его имени на количество count");
            RegisterCheat("infl", (p, id, timeSec, value) =>
            {
                var infl = InfluenceXmlDataList.Instance.GetInfluence(p.syn, id, timeSec, value);
                bool successApply = p.StatContainer.AddInfluence(infl);
                if (!successApply)
                    InfluenceXmlDataList.Instance.BackPool(infl);
            }, "(id, timeSec, value) Повесить инфлюэнс на перса по номеру, на время в секундах, с тиком value");
            RegisterCheat("infltarget", (p, id, timeSec, value) =>
            {
                var infl = InfluenceXmlDataList.Instance.GetInfluence(p.syn, id, timeSec, value);
                if (p.syn.TargetLockObject != null)
                {
                    p.syn.TargetLockObject.GetStatContainer().AddInfluence(infl);
                }
            }, "(id, timeSec, value) Повесить инфлюэнс на перса по номеру, на время в секундах, с тиком value");
            RegisterCheat("removeinfl", (p) => p.StatContainer.RemoveInfluences(), "Снимает все инфлюэнсы с перса");
            RegisterCheat("elixir", (p, baseItemId) => InstanceGameManager.Instance.gameXmlData.Elixirs.Find(x => x.BaseId == baseItemId).Use(p), "Использовать элексир с номером таким-то (wiki)");

            RegisterCheat("info", (p) => Info(p, p.syn.TargetLockObject), "Выводит инфу о монстре, взятым в таргет, в чат");
			RegisterCheat<string>("buildbuy", (p, buildingName) =>
			{
				var buildingXmlData = p.gameXmlData.PlayerCityData.Buildings.Find(x => x.Name == buildingName);
				if (buildingXmlData == null)
				{
					SendCheatChatMessage.Invoke($"buildingName='{buildingName}' not found", p);
					return;
				}

				var loginBuilding = p.login.Buildings.Find(x => x.Name == buildingXmlData.Name);
				if (loginBuilding != null && loginBuilding.State == BuildingState.None)
					p.login.AddBuilding(buildingXmlData);
			}, "Купить здание для лобби");
			RegisterCheat<string>("buildremove", (p, buildingName) =>
			{
				var buildingXmlData = p.gameXmlData.PlayerCityData.Buildings.Find(x => x.Name == buildingName);
				if (buildingXmlData == null)
				{
					SendCheatChatMessage.Invoke($"buildingName='{buildingName}' not found", p);
					return;
				}

				var loginBuilding = p.login.Buildings.Find(x => x.Name == buildingXmlData.Name);
				if (loginBuilding != null && loginBuilding.State != BuildingState.None)
					p.login.RemoveBuilding(buildingXmlData);
			}, "Продать здание для лобби");
			//RegisterCheat("buildset", (p, buildId) => loginController.SetBuildingRequest(p, buildId), "Выбрать здание для лобби");
			RegisterCheat("buildget", (p) =>
			{
				foreach (var build in p.login.Buildings)
					SendCheatChatMessage.Invoke($"{build.Name} {build.State}", p);
			}, "Показать здания в лобби");
            RegisterCheat("p", (p) => ServerPathFinderAlgorithm.SetTargetForMiniMap(p.syn.map, p.syn.position.X, p.syn.position.Z, 2, true), "Ставить в текущиее координаты персонажа область, которую монсты будут огибать");

            RegisterCheat("hmap", (p) => SendCheatChatMessage.Invoke(ServerPathFinderAlgorithm.DebugPrintHMap(p.syn.map, Helper.GetCell(p.syn.position.X), Helper.GetCell(p.syn.position.Z), 13), p), "Выводит небольшой кусок карты высот в чат");
            RegisterCheat("omap", (p) => SendCheatChatMessage.Invoke(ServerPathFinderAlgorithm.DebugPrintObjectMap(p.syn.map, Helper.GetCell(p.syn.position.X), Helper.GetCell(p.syn.position.Z), 13), p), "Выводит небольшой кусок карты с кустами и деревьями");
            RegisterCheat("tmap", (p) => SendCheatChatMessage.Invoke(ServerPathFinderAlgorithm.DebugPrintTransparentMap(p.syn.map, Helper.GetCell(p.syn.position.X), Helper.GetCell(p.syn.position.Z), 13), p), "Выводит небольшой кусок карты с непроходимыми клетками");
            RegisterCheat("pmap", (p) => SendCheatChatMessage.Invoke(ServerPathFinderAlgorithm.DebugPrintPassabilityMap(p.syn.map, Helper.GetCell(p.syn.position.X), Helper.GetCell(p.syn.position.Z), 13), p), "Выводит небольшой кусок карты с труднопроходимыми клетками");
            RegisterCheat("cell", (p) => SendCheatChatMessage.Invoke("x=" + Helper.GetCell(p.syn.position.X) + " z=" + Helper.GetCell(p.syn.position.Z), p), "Выводит в чат текущие координаты персонажа");

            RegisterCheat("worldboss", SendWorldBossFx, "Призыв worlboss'а. Механика BG, сейчас не работает");


            RegisterCheat("endmatch", (p, sec) =>
            {
                p.syn.map.CheatEndMatch(sec);
            }, "Закончить матч. Механика для BattleGround");

            /*--------------------------------------------------------------------------------------------------------------------*/

            RegisterCheat("endarena", (p) =>
            {
                p.syn.map.CheatEndMatch(10);

                //var dmSolo = p.syn.map.Scenarios.Find(s => s is DeathmatchSolo) as DeathmatchSolo;
                //if (dmSolo != null)
                //{
                //    dmSolo.EndMatch();
                //}
            }, "Закончить раунд. Механика для Arena");

            RegisterCheat("endarena", (p, sec) =>
            {
                p.syn.map.CheatEndMatch(sec);

                //var dmSolo = p.syn.map.Scenarios.Find(s => s is DeathmatchSolo) as DeathmatchSolo;
                //if (dmSolo != null)
                //{
                //    dmSolo.EndMatch();
                //}
            }, "Закончить раунд. Механика для Arena");

            RegisterCheat("setArenaButtonId", (p, buttonId) => loginController.ArenaMatchTypeSelectRequest(p, buttonId), "Присвоть значение последней выбранной арены");
            /*--------------------------------------------------------------------------------------------------------------------*/

            RegisterCheat("runtrigger", (p, name) => p.syn.map.MyTriggerDic[name].Check(p), "Управление ботами в лагере. Статья в вики camp_bot");
            RegisterCheat("runPeriodTrigger", (p, name) => p.syn.map.MyPeriodTriggerDic[name].Check(p), "Управление ботами в лагере. Статья в вики camp_bot");
            RegisterCheat("stopPeriodTrigger", (p, name) => p.syn.map.MyPeriodTriggerDic[name].bStart = false, "Управление ботами в лагере. Статья в вики camp_bot");

            RegisterCheat("guildm", (p) =>
            {
                var x = (p.syn.position + Helper.Vector3forward.normalized()).X;
                var z = (p.syn.position + Helper.Vector3forward.normalized()).Z;
                string startPos = $"{x};{z}";

                NPCInfo npcInfo = new NPCInfo()
                {
                    Name = "GuildMaster",
                    ActiveData = "GuildMaster",
                    StartPoint = startPos,
                };
                
                p.syn.map.SetVariableBool("guildm", true);
                var npc = p.syn.map.CreateNPC(npcInfo);
                ServerRpcListener.Instance.SendCreateEntity(npc);
            }, "Создает гилдмастера");

			RegisterCheat("clanreset7day", (p) =>
			{
				p.login.ClanLeave = DateTime.MinValue;
				p.login.ClanEnter = DateTime.MinValue;
				guildController.SendClanResetCooldown(p);
				guildController.SendGuildDataResponse(p);
			}, "Кланы. Сбрасывает кулдауны");


            RegisterCheat("resetlikeapp", (p) => {
                p.ScenarioStrings.Add("likeappTimeLabel1", "0");
                p.ScenarioStrings.Add("likeappTimeLabel2", "0");
                p.ScenarioStrings.Add("likeappTimeLabel3", "0");
                p.ScenarioStrings.Add("likeapp", "true");
            }, "Форма голосования за приложение. Заставляет забыть о том, когда ты убивал боссов");

            RegisterCheat("likeapp", (p) => ServerRpcListener.Instance.SendLikeApp(p), "Форма голосования за приложение. Открывает форму голосования без мороки с монстрами");

            RegisterCheat("statue", (InstancePerson p, float TimeStart, float TimePerCube, float FxSpeed, int CubeCount) =>
            {
                Interaction_StatueBlessing.TimeStart = Math.Max(0, TimeStart);
                Interaction_StatueBlessing.TimePerCube = Math.Max(0.1f, TimePerCube);
                Interaction_StatueBlessing.FxSpeed = Math.Max(0.1f, FxSpeed);
                Interaction_StatueBlessing.CubeCount = Math.Max(1, CubeCount);
            }, "(TimeStart, TimePerCube, FxSpeed, CubeCount) Регулировать эффект передачи кубов у Статуи Благославления");
            RegisterCheat("setString", (p, key, str) => {
                p.ScenarioStrings.Add(key, str);
                SendCheatScenarioStrings(p);
            }, "(key, str)Меняет значения ключей для выбранного персонажа из чита /strings . key - тот самый ключ . str - значение ");
            RegisterCheat("setStringLogin", (p, key, str) => {
                p.login.scenarioStrings.Add(key, str);
                SendCheatScenarioStrings(p);
            }, "(key, str)Меняет значения ключей для логина из чита /strings . key - тот самый ключ . str - значение ");
            RegisterCheat("removeStringLogin", (p, key) => {
                p.login.scenarioStrings.Remove(key);
                SendCheatScenarioStrings(p);
            }, "(key) удаляет ключ для логина из чита /strings . key - тот самый ключ ");
            RegisterCheat("removeString", (p, key) => {
                p.ScenarioStrings.Remove(key);
                SendCheatScenarioStrings(p);
            }, "(key) удаляет ключ для персонажа из чита /strings . key - тот самый ключ ");


            RegisterCheat("Strings", (p) => SendCheatScenarioStrings(p), "Выводит список текстовых сценарных переменных персонажа. (консультации у МаксимаП)");
            RegisterCheat("SetMapVariableBool", (p, key, val) => {
                p.syn.map.SetVariableBool(key, val);
                p.syn.map.SendCheatScenarioStrings(p);
            }, "(key, str)Меняет значения ключей из чита /MapVariablesBool . key - тот самый ключ . val - булевское значение ");

            RegisterCheat("MapVariablesBool", (p) => p.syn.map.SendCheatScenarioStrings(p), "Выводит список булевских сценарных переменных карты. (консультации у МаксимаП)");
            RegisterCheat("help", (p) => SendCheatDescription(p), "Показать список читов");

            RegisterCheat("StatusChance", (p) => ShowStatusChance = !ShowStatusChance, "включает/выключает логирование шансов навешивания инфлуенсов");
            RegisterCheat("CastSensorLog", (p) => ShowCastSensor = !ShowCastSensor, "включает/выключает логирование срабатывания каст сенсора");

            RegisterCheat("SendAdminGift", (p) =>
            {
                ServerRpcListener.Instance.SendAdministrationGiftResponse(p);
            }, "Отослать подарок от администрации.");

            RegisterCheat("resetCooldowns", (p) =>
            {
                p.syn.ResetCooldown();
                ServerRpcListener.Instance.SendRpc(p.ClientRpc.ShowAbilityButtonProgressResponse, (byte)1, 0f.ToFloatUnsignedPacked());
                ServerRpcListener.Instance.SendRpc(p.ClientRpc.ShowAbilityButtonProgressResponse, (byte)2, 0f.ToFloatUnsignedPacked());
                ServerRpcListener.Instance.SendRpc(p.ClientRpc.ShowAbilityButtonProgressResponse, (byte)3, 0f.ToFloatUnsignedPacked());
            }, "Откатывает кулдауны");
            
            RegisterCheat("AbilityState", (p) =>
            {
                foreach (var ability in p.activeAbility)
                {
                    if (ability.SlotId < 0)
                        continue;
                    ServerRpcListener.Instance.SendRpc(p.ClientRpc.AbilitiesStateCheatOnlyResponse, ability.SlotId, ability.GetButtonIsActive());
                }
            }, "Показывает состояние умений на сервере");

            RegisterCheat("showSafeZone", (p) =>
            {
                Map map = p.syn.map;
                for (int i = 0; i < map.heightX; i++)
                    for (int j = 0; j < map.heightZ; j++)
                    {
                        if (map.IsSafeZone(i, j))
                        {
                            Vector3 pos = Helper.UltraCorrectYPosition(map, new Vector3(i * Helper.VoxelSize, 0, j * Helper.VoxelSize));
                            int effectInstanceId =
                            ServerRpcListener.Instance.SendCreateFxResponseInPos(p.syn, EffectType.cocon_target, pos,
                                0);
                            CoroutineRunner.Instance.Timer(100, () =>
                            {
                                ServerRpcListener.Instance.DestroyFxResponse(p.syn.map, effectInstanceId, Vector3.Zero,
                                    true);
                            });
                        }
                    }
            }, "Показывает клетки сейфзоны");

            RegisterCheat("emotion", (p) => CheatSendEmotionCheat(p));
            RegisterCheat("emotionBuy", (p, id) => CheatSendEmotionBuy(p, id));
            RegisterCheat("emotionRef", (p) => CheatEmotionRefresh(p));
            RegisterCheat("emotionDel", (p) => CheatDeleteAll(p));

            //RegisterCheat("analytic", (p, name, param) =>
            //{
            //    AnalyticsManager.Instance.DevToDevEventResponse(p, name, new Dictionary<string, string>() { { "marker", param } });
            //    AnalyticsManager.Instance.AppsFlyerEventResponse(p, name, new Dictionary<string, string>() { { "marker", param } });
            //});

            RegisterCheat("lk", (p) => LoginKick(p));

			RegisterCheat("disconnect", (p, t) => CoroutineRunner.Instance.Timer(t, () => ServerRpcListener.Instance.Disconnect(p)));

            RegisterCheat("AnalyticReset", (p) => p.Analitics.Clear());

            RegisterCheat("ShowSpawnPoint", (p) =>
            {
                MonsterBehaviour monster = p.syn.Target as MonsterBehaviour;
                if (monster != null)
                {
                    int effectInstanceId = ServerRpcListener.Instance.SendCreateFxResponseInPos(p.syn, EffectType.cocon_target, monster.PridePosition, 0);
                    Sos.Warning("ShowSpawnPoint surface " + monster.map.GetSurface(monster.PridePosition));
                    CoroutineRunner.Instance.Timer(10, () =>
                    {
                        ServerRpcListener.Instance.DestroyFxResponse(p.syn.map, effectInstanceId, Vector3.Zero,
                            true);
                    });
                    // monster.AddBoolAnimatorParameterForSend(animatorParameterName, parameterValue);
                }
            }, "показывает точку респа моба");
            RegisterCheat("ShowSpawnPoints", (p) =>
            {
                var list = p.syn.map.GetGoodSpawnPoints(Helper.GetCell(p.syn.position.X), Helper.GetCell(p.syn.position.Z), 20, ServerPathFinderAlgorithm.IsGoodSpawnPointForCommonMonster);
                if (list == null)
                    return;
                Sos.Error("ShowSpawnPoint count " + list.Count);
                foreach (var item in list)
                {

                    int effectInstanceId = ServerRpcListener.Instance.SendCreateFxResponseInPos(p.syn, EffectType.cocon_target, Helper.UltraCorrectYPosition(p.syn.map, new Vector3((item.x + 0.5f) * Helper.VoxelSize, 0, (item.y + 0.5f) * Helper.VoxelSize)), 0);
                    Sos.Warning("ShowSpawnPoint surface " + p.syn.map.GetSurface(item.x, item.y));
                    CoroutineRunner.Instance.Timer(10, () =>
                    {
                        ServerRpcListener.Instance.DestroyFxResponse(p.syn.map, effectInstanceId, Vector3.Zero,
                            true);
                    });
                }
                // monster.AddBoolAnimatorParameterForSend(animatorParameterName, parameterValue);
            }, "показывает возможные точки респа вокруг персонажа");

            RegisterCheat("exp", (p, exp) =>
            {
                p.AddExperience(exp);
				if (Map.Edge.Type == LocationType.Camp)
					loginController.SendGetLobbyInfoResponse(p);
            }, "добавить опыта");

            RegisterCheat("collection", (p, collection) => p.Collection.Set(collection), "установить коллекцию, для открытия следующей локации");

            RegisterCheat("addbots", (InstancePerson p, int count, int level, int rarity, string type) =>
            {
                var names = new List<String>();

                if (type.Contains('w'))
                    names.Add("dm_bot_warrior");
                if (type.Contains('m'))
                    names.Add("dm_bot_wizard");
                if (type.Contains('a'))
                    names.Add("dm_bot_archer");
                if (type.Contains('g'))
                    names.Add("dm_bot_gunner");

                for (int t = 0; t < count; t++)
                {
                        var creater = new CreateMonster() { Name = names.Random() };
                        creater.Construct(p.gameXmlData);
                        creater.Level = level.ToString();

                        //var areaName = GetStartPoinName(null);
                        var pos = p.syn.position + new Vector3(FRRandom.Next(2.2f) - 0.1f, 0, FRRandom.Next(2.2f) - 0.1f);

                        var monster = creater.GetMonster(p.syn.map, pos);
                        creater.AddMonsterOnMap(monster);
                        monster.NickName = nickNameGeneratorManager.GenerateUnique("bot", p.syn.map);
                        monster.AreaName = "";
                        monster.nextAttack = TimeProvider.UTCNow.AddSeconds(5);
                }
            }, "сгенерить ботов (count, level,rarity,\"wmag\")");

			RegisterCheat("rpcout", p =>
			{
				ILogger.Instance.Send($"Seconds passed: {(DateTime.UtcNow - uLink.NetworkView.OutgoingRPCLastReset).TotalSeconds}");
				uLink.NetworkView.OutgoingRPCLastReset = DateTime.UtcNow;

				lock (uLink.NetworkView.OutgoingRPCsCount)
				{
					foreach (var kvp in uLink.NetworkView.OutgoingRPCsCount.OrderByDescending(pair => pair.Value))
					{
						ILogger.Instance.Send($"{kvp.Key}: {kvp.Value}");
					}
					uLink.NetworkView.OutgoingRPCsCount.Clear();
				}
			}, "log outgoing RPC stats");

            RegisterCheat("arenareward", (p) =>
            {
                var dmSolo = p.syn.map.Scenarios.Find(s => s is DeathmatchSolo) as DeathmatchSolo;
                if (dmSolo != null)
                    ServerRpcListener.Instance.SendArenaRewardFromCheat(p, dmSolo.CreateMatchReward_Cheat(p));
            }, "посмотреть форму ревардов на арене. без возможности получить");

			RegisterCheat("cancel_queue", p =>
			{
				ServerRpcListener.Instance.SendCancelInstanceQueue(p);
			}, "удалить себя из очередей на вход в другой инстанс");
            //RegisterCheat("setkeyr", (p, radius) =>
            //{
            //    var keywardens = p.syn.map.Scenarios.Cast<Keywardens>().FirstOrDefault(s => s.LocationName.Contains(p.syn.map.LevelName));
            //    if(keywardens != null)
            //        keywardens.RadiusForGetKey = radius;
            //}, "установить радиюс для получения ключа ассистом");
			RegisterCheat("minimap", (p, iconId) =>
			{
				var minimapData = new MinimapData((MinimapItemType)iconId, p.syn.position.Vector3ToVector2());
				p.syn.map.MinimapManager.SendAddObject(p, minimapData);
			}, "посмотреть иконку на миникарте");

            RegisterCheat("pfDeep", (p) => p.syn.map.CurrentPathFinder = new PathFinderDeep(p.syn.map), "Устанавливает PathFinderDeep (поиск по реперным точкам) как основной алгоритм поиска пути");
            RegisterCheat("pfAStar", (p) => p.syn.map.CurrentPathFinder = new PathFinderAStar(), "Устанавливает PathFinderAStar (самый обычный поиск по клеточкам) как основной алгоритм поиска пути");
            RegisterCheat("pfDirect", (p) => p.syn.map.CurrentPathFinder = new PathFinderDirectLine(), "Устанавливает PathFinderDirectLine (нет никакого поиска. путь - прямая до целевой точки) как основной алгоритм поиска пути");
	        RegisterCheat("emptyName", p => p.login.loginName = string.Empty);
            RegisterCheat("delpers", (p) => {
                var login = p.login;
                login.RemovePerson(p);
                login.ResetSlots(); });

			RegisterCheat("delall", (p) => { DelAll(p); });
			RegisterCheat("delall", (p, isDev) => { DelAll(p, isDev); });

            RegisterCheat<string>("mbox", (p, str) => ServerRpcListener.Instance.SendRpc(p.syn.map.ClientRpc.ShowDebugMessageBox, str), "(str) показывает месседж бокс с текстом str");
            RegisterCheat<string, float>("mbox", (p, str, delaySec) => CoroutineRunner.Instance.Timer(delaySec, ()=>
                                            ServerRpcListener.Instance.SendRpc(p.syn.map.ClientRpc.ShowDebugMessageBox, str)),
            "(str, delaySec) показывает месседж бокс с текстом str через delaySec секунд");

            RegisterCheat("getarenastat", (p) =>
            {
                var message = JsonConvert.SerializeObject(p.login.WinCounterCollection.GetSaveData()).ToString();
                SendCheatChatMessage.Invoke(message, p);
            }, "получить информацию о количестве боев/побед в режимах");

            RegisterCheat<int, int>("lobbyArenaWin", (p, itype, count) =>
            {
                ArenaMatchType type = (ArenaMatchType)itype;
                for (int i = 0; i < count; i++)
                    p.OnArenaGameFinish(type, true);

            }, "(type, count) выиграть count боев в режиме type");

            RegisterCheat("getoffers", (p) => specialActionController.SpecialActionRequest(p), "обновить акции");
            RegisterCheat("endoffer", (p, timeSec) => specialActionController.SpecialActionRequestCheat(p, timeSec), "ставим время на завершение акции");
            RegisterCheat("cooloffer", (p, timeSec) => OfferEndCooldown(p, timeSec), "завершаем кулдаун акции");
            RegisterCheat("deloffers", (p) => specialActionController.CheatDelOffers(p), "удалить все акции и все на выдачу");
            RegisterCheat("getoffersid", (p) => SendAvalableOffersId(p), "получит активные у логина акции");
            RegisterCheat("getalloffersid", (p) => SendAllLoginOffersId(p), "получит все что есть у логинаакции");
            RegisterCheat("getofferbyid", (p, id) => GetOfferById(p, id), "обновить насильно один офер без условий + те что были");
            RegisterCheat("getofferone", (p, id) => specialActionController.CheatDelAllGetOfferById(p, id, false), "получить только один без условий");
            RegisterCheat("gettrueoffer", (p, id) => specialActionController.CheatDelAllGetOfferById(p, id, true), "получить только один с условиями");
            RegisterCheat("setauthreg", (p, days) => SetAuthReg(p, days), "сдвинуть дату регистрации назад  в днях");            
            RegisterCheat("getauthreg", (p) => SendCheatChatMessage.Invoke("authRegTime: " + p.login.authRegTime, p), "получить дату регистрации");
            RegisterCheat("saveauthreg", (p, days) => SaveAuthReg(p, days), "сдвинуть дату регистрации назад и сохранить в бд, в днях");

            RegisterCheat("setofferstime", (p, days) => SetOffersBoughtTime(p, days), "сдвинуть дату покупок офферов");
            RegisterCheat("getofferssum", (p, days) => GetOffersSumm(p, days), "показать сумму за последние Х дней");

            RegisterCheat("getoffersinfo", (p, days) => GetOffersInfo(p, days), "показать список купленных оферов");
            RegisterCheat("getoffersinfo", (p) => GetOffersInfo(p), "показать список купленных оферов");

            RegisterCheat("personsid", (p) => SendPeronsId(p), "получить все ид персов");
            RegisterCheat("buyoffer", (p, id) => 
			{
				specialActionController.BuySpecialOffer(p, id);
			}, "купить оффер по ид");
            RegisterCheat("addoffer", (p, id) => specialActionController.SpecialActionBuyRequest(p, id), "выдать на перса");
            RegisterCheat("ratingp", (p) => ServerRpcListener.Instance.SendRatingApplay(p, "ping"), "пинг рейтига");
            RegisterCheat("ratingremove", (p) => ServerRpcListener.Instance.SendRemoveRating(p), "обнулить рейтинги всех своих персонажей");
            RegisterCheat("ratinginit", (p) =>
            {
                ServerRpcListener.Instance.SendLoginDataToRatingServer(p);
                foreach (var pers in p.login.InstancePersons)
                {
                    ServerRpcListener.Instance.SendToRatingServer(pers, RatingRequestType.Init, ArenaMatchType.None, false, 300, "ping");
                }
            }, "проинитить рейтинги свтоих персонажей");
			RegisterCheat<string, int>("event", (p, name, duration) =>
			{
				var @event = EventXmlConfig.Instance.KeyDatas.FirstOrDefault(x => x.Key == "event");
				if (@event == null)
					return;

				foreach (var eventData in @event.Datas)
				{
					if (eventData.Value == "default")
						continue;

					if (eventData.Value.ToLower() == name.ToLower())
					{
						eventData.StartDate = TimeProvider.UTCNow;
						eventData.EndDate = TimeProvider.UTCNow.AddSeconds(duration);
					}
					else
					{
						eventData.StartDate = TimeProvider.UTCNow.AddSeconds(-300);
						eventData.EndDate = TimeProvider.UTCNow.AddSeconds(-100);
					}
				}

				var decodedata = EventXmlConfig.Instance.SaveToXml();
				var xml = ZipUtils.GetBase64ZipXorString(decodedata, string.Empty);
				ServerRpcListener.Instance.SendCheatXmlChange(xml, "eventconfig.xml");
			});

            RegisterCheat<int,int,bool>("LearnAbility", (p, skillId, slotId, ignorLevel) => {
                LearnAbility learnAbility = new LearnAbility();
                learnAbility.IgnoreUnlockLevel = ignorLevel;
                learnAbility.SkillId = (AbilityType)skillId;
                learnAbility.SlotId = slotId;
                learnAbility.SendToClient = true;
                learnAbility.Execute(p);
            }, "выставить абилку персонажу");

            RegisterCheat<int, int>("AddPassiveAbility", (person, abilityId, abilityLvl) =>
                {
                    person.ResetPassiveSkills();
                    var ability = person.passiveAbilityStorage.PassiveAbilities.FirstOrDefault(a => a.Id == abilityId);
                    if (ability == null)
                    {
                        SendCheatChatMessage.Invoke($"Не смог найти абилку у класса {person.personType} абилити ид  = {abilityId}", person);
                        return;
                    }

                    for (int i = 0; i < abilityLvl && ability.MaxLevel > i; i++)
                        ability.LevelUp();

                    SendCheatChatMessage.Invoke($"Добавил на персонажа {person.personName} пассивку {ability.Description}", person);
                }, "Добавить пассивную способность на персонажа"
            );
			RegisterCheat("InstInfo", person => ServerRpcListener.Instance.SendServerInstanceInfo(person), "Информация о инстансах, картах и персах");
			RegisterCheat("LobbyRating", (person, lobbyId, enterCount, sendCount) => ServerRpcListener.Instance.SendCheatLobbyRatingSet(lobbyId, enterCount, sendCount), "Поправить рейтинг лобби");
			RegisterCheat("block", person =>
			{
				InstanceGameManager.Instance.blockedLogins.Add(person.loginId);
				LoginKick(person);
			}, "Забанить себя на этом инстансе");
            RegisterCheat("deleteLogin", person =>
            {
	            guildController.RemoveLogin(person.loginId);
				LoginKick(person);
				loginController.DeleteLogin(person);
				ServerRpcListener.Instance.SendRemoveRating(person, isPersons: false);
				guildController.RemoveLogin(person.loginId);
            });

			RegisterCheat("botitems", person =>
			{
				if(person.syn != null)
					BotInfo(person);
			});

			RegisterCheat("inflall", (p, id, timeSec, value) =>
			{
				AddAllInfl(p, id, timeSec, value);
			}, "(id, timeSec, value) Повесить на всех монстров/ботов по id");

			RegisterCheat("removeinflall", (p, id) => RemoveInflAll(p, id), "Снять со всех монстров/ботов по id");

			RegisterCheat("donate", (p, iframeUrl) =>
			{
				CoroutineRunner.Instance.Timer(2, () => ServerRpcListener.Instance.SendRpc(p.ClientRpc.DonateShowResponse, iframeUrl));
			}, "Показать фрейм доната");
		}

		void DelAll(InstancePerson p, int isDev = 0)
		{
			var login = p.login;
			if (isDev == 1)// для никиты
				login.settings = new GameSettings();

			login.QuestsClear(x => true);
			login.Wallet[ResourceType.Token] = 0;
			login.CreditRating = 100;
			login.AddGloryPoint(-login.GloryPoint);
			login.scenarioStrings.Clear();

			login.Notifications.Clear();
			login.ArenaMatchDataStatuses.Clear();
			login.MapItemDataStatuses.Clear();
			login.BuildingSetStatuses.Clear();
			login.BuildingCategoryStatuses.Clear();
			login.BuildingSubCategoryStatuses.Clear();

			login.Skins.Clear();
			login.Buildings.Clear();
			login.BuildingKits.Clear();

			//Костыль, клиент не убирает бонус +25 за маникен
			foreach (var building in p.gameXmlData.PlayerCityData.Buildings)
				GlobalEvents.Instance.OnRemoveBuilding(login, building);

			while (login.InstancePersons.Count > 0)
			{
				var person = login.InstancePersons[0];
				login.RemovePerson(person, false);
			}

			login.ResetSlots();
			login.tutorRun = true;
			login.Tutorials.Clear();
			login.SetAvatar(string.Empty);

			login.ClanLeave = DateTime.MinValue;
			login.ClanEnter = DateTime.MinValue;
			guildController.RemoveLogin(login.loginId);

			InstanceGameManager.Instance.CreateDefaultPerson(p);

			login.InitPersons(); //Дефолтные здания выдать

			ServerRpcListener.Instance.SendRemoveRating(p);

			LoginKick(p);
		}

		void AddAllInfl(InstancePerson instancePerson, int id, int timeSec, int value)
		{
			var map = instancePerson.syn?.map;
			if (map == null)
			{
				SendCheatChatMessage.Invoke("AddAllInfl: not working in lobby", instancePerson);
				return;
			}

			foreach (var m in map.monsters)
			{
				var statContainer = m.GetStatContainer();
				if (statContainer == null)
					continue;

				var infl = InfluenceXmlDataList.Instance.GetInfluence(instancePerson.syn, id, timeSec, value);
				bool successApply = statContainer.AddInfluence(infl);
				if (!successApply)
					InfluenceXmlDataList.Instance.BackPool(infl);
			}
		}

		void RemoveInflAll(InstancePerson instancePerson, int id)
		{
			var map = instancePerson.syn?.map;
			if (map == null)
			{
				SendCheatChatMessage.Invoke("RemoveInflAll: not working in lobby", instancePerson);
				return;
			}

			foreach(var m in map.monsters)
			{
				var statContainer = m.GetStatContainer();
				if (statContainer == null)
					continue;

				statContainer.RemoveInfluence(id);
			}

			//map.monsters.Select(m => m.GetStatContainer()).ToList().ForEach(c => c.RemoveAllInfluences());
		}


		private void GetOffersInfo(InstancePerson instancePerson, int days = -1)
        {
            List<OfferBillInfo> list = new List<OfferBillInfo>();

            list = days > 0 ? instancePerson.login.OffersBills.Where(o => TimeProvider.UTCNow.AddDays(-days) <= o.BoughtTime).ToList() : instancePerson.login.OffersBills;
            SendCheatChatMessage.Invoke("all offers: " + JsonConvert.SerializeObject(list, Formatting.Indented), instancePerson);
        }


        private void GetOffersSumm(InstancePerson instancePerson, int days = -1)
        {
            double summ = 0;
            summ = instancePerson.login.GetAverageBill(days < 0 ? 1 : days);
            SendCheatChatMessage.Invoke("offers sum: " + summ, instancePerson);
        }

        private void SetAuthReg(InstancePerson instancePerson, int days)
        {
            var authRegTime = instancePerson.login.authRegTime.AddDays(-days);
            instancePerson.login.authRegTime = authRegTime;
            SendCheatChatMessage.Invoke("authRegTime: " + instancePerson.login.authRegTime, instancePerson);
        }

        private void SaveAuthReg(InstancePerson instancePerson, int days)
        {
            var authRegTime = instancePerson.login.authRegTime.AddDays(days);
            ServerRpcListener.Instance.SendCheatSaveAuthReg(instancePerson.loginId, authRegTime);
        }

        private void SetOffersBoughtTime(InstancePerson instancePerson, int days)
        {
            foreach (var offer in instancePerson.login.OffersBills)
            {
                ILogger.Instance.Send($"SetOffersBoughtTime offer.BoughtTime: {offer.BoughtTime}", ErrorLevel.error);
                var newTime = offer.BoughtTime.AddDays(-days);
                offer.BoughtTime = newTime;
                ILogger.Instance.Send($"SetOffersBoughtTime offer.NewTime: {offer.BoughtTime}", ErrorLevel.error);
            }
        }

        private void GetOfferById(InstancePerson instancePerson, int id)
        {
			specialActionController.CheatGetOfferById(instancePerson, id);
        }

        private void OfferEndCooldown(InstancePerson person, int timeSec)
        {
            var offer = person.login.OffersDatas.FirstOrDefault(o => o.UpdateReason == UpdateOfferReason.Cooldown);
            if (offer != null)
            {
                offer.TimeForUpdate = TimeProvider.UTCNow.AddSeconds(timeSec);
				specialActionController.SpecialActionRequest(person);
            }
        }


        private void SendPeronsId(InstancePerson person)
        {
            var infos = new List<KeyValuePair<PersonType, int>>();
            foreach (var instancePerson in person.login.InstancePersons)
            {
                infos.Add(new KeyValuePair<PersonType, int>(instancePerson.personType, instancePerson.personId));
            }
            
            var personsIdStr = JsonConvert.SerializeObject(infos, Formatting.Indented);
            SendCheatChatMessage.Invoke(personsIdStr, person);
        }

        private void SendAllLoginOffersId(InstancePerson person)
        {
            var avalableOffers = person.login.OffersDatas;
            var offers = JsonConvert.SerializeObject(avalableOffers, Formatting.Indented);
            SendCheatChatMessage.Invoke(offers, person);
        }

        private void SendAvalableOffersId(InstancePerson person)
        {
            var avalableOffers = person.login.OffersDatas.FindAll(o =>
                o.TimeForUpdate > TimeProvider.UTCNow && o.UpdateReason == UpdateOfferReason.Duration);


            var offers = JsonConvert.SerializeObject(avalableOffers, Formatting.Indented);
            var offersForAdd = JsonConvert.SerializeObject(person.login.OffersIdForAdd, Formatting.Indented);
            SendCheatChatMessage.Invoke(offers + "\n " + offersForAdd, person);
        }

        public void SendCheatScenarioStrings(InstancePerson person)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine("person strings:");
            foreach (var pair in person.ScenarioStrings.scenarioParams)
                sb.AppendLine(pair.Key + " : " + pair.Value);

            sb.AppendLine("login strings:");
            foreach (var pair in person.login.scenarioStrings.scenarioParams)
                sb.AppendLine(pair.Key + " : " + pair.Value);

            SendCheatChatMessage.Invoke(sb.ToString(), person);
        }

        private void Bburning(InstancePerson p, int count, float density, float speed)
        {
            var startpoint = p.syn.position;
            Map map = p.syn.map;
            CoroutineRunner.Instance.Timer(3, () =>
            {
                double fi = (Math.Sqrt(5) - 1) / 2;
                double goldAngle = 2 * Math.PI * (1 - fi);
                for (int i = 0; i < count; i++)
                {
                    float r = (float)Math.Sqrt(i) * density;
                    Vector3 point = startpoint + new Vector3((float)Math.Sin(i * goldAngle) * r, 0, (float)Math.Cos(i * goldAngle) * r);

                    if (map.IsWaterSurface(point))
                        continue;

                    point = map.UltraCorrectYPosition(point, false);

                    CoroutineRunner.Instance.Timer(r / speed, () =>
                    {
                        var effect = map.CreateFx(point, EffectType.fire_wall);

                        CoroutineRunner.Instance.Timer(0.5f, () =>
                        {
                            map.RemoveFx(effect);
                        });
                    });
                }
            });
        }

        public MonsterBehaviour CreateMonster(InstancePerson p, string name, int level, string lootName, string kit = null)
        {
            CreateMonster cm = new CreateMonster();
            cm.Construct(p.gameXmlData);
            cm.Name = name;
            cm.Level = level.ToString();
            cm.LootName = lootName;
            cm.Kit = kit;

            var pos = p.syn.position + Helper.Vector3forward.normalized();

            MonsterBehaviour monster = cm.GetMonster(p.syn.map, pos);
            cm.AddMonsterOnMap(monster);

            return monster;
        }

        public void LoginKick(InstancePerson instancePerson)
        {
            ServerRpcListener.Instance.SendLoginKick(instancePerson);
        }

        public void CheatSendEmotionCheat(InstancePerson instancePerson)
        {
            //EmotionPerson.SendEmotionsInfoResponse();
            emotionController.EmotionsInfoRequest(instancePerson);
        }

        public void CheatSendEmotionBuy(InstancePerson instancePerson, int id)
        {
            emotionController.BuyEmotionRequest(instancePerson, id);
        }

        public void CheatEmotionRefresh(InstancePerson instancePerson)
        {
            instancePerson.EmotionPerson.SetLastRefreshTime(TimeProvider.UTCNow);
            emotionController.CheatsRefreshEmotions(instancePerson);
        }

        public void CheatDeleteAll(InstancePerson instancePerson)
        {
            emotionController.CheatDeleteAllEmitions(instancePerson);
            CheatEmotionRefresh(instancePerson);
        }

        private static void SendCheatDescription(InstancePerson person)
        {
            StringBuilder sb = new StringBuilder();
            foreach(var pair in cheatsDescr)
            {
                sb.AppendLine("<color=#90e364>" + pair.Key + "</color>");
                if(!string.IsNullOrEmpty(pair.Value))
                    sb.AppendLine(pair.Value);
            }
            SendCheatChatMessage.Invoke(sb.ToString(), person);
        }

        private static void AddMapObjectFx(InstancePerson person, int fxId, string objName)
        {
            var map = person.syn.map;
            if (map == null)
                return;
            var mapObjs = map.objects.FindAll(o => o.obj.name == objName);
            if (mapObjs.Count == 0)
            {
                ILogger.Instance.Send("AddMapObjectFx: mapObj == null", ErrorLevel.error);
                return;
            }

            foreach (var mapObj in mapObjs)
            {
                var effectType = (EffectType)fxId;
                var objEffect = new MapEffect(map)
                {
                    fxId = effectType,
                    position = mapObj.center
                };

                ServerRpcListener.Instance.SendCreateFxResponse(mapObj, objEffect);
            }
        }

        private static void CallMapObject(InstancePerson person, string objName)
        {
            var map = person.syn.map;
            if (map == null)
                return;

            string activeData = null;
            switch (objName)
            {
                case "guild_altar_worldboss": activeData = "Worldboss"; break;
                case "guild_altar_control": activeData = "Battleground"; break;
            }
            
            var objInfo = new MapObjectInfo
            {
                name = objName,
                id = MapTargetObject.objId++,
                ActiveData = activeData,
            };
            
            var mapObject = map.CreateMapObject(objInfo, person.syn.position);
			mapObject.view = uLinkHelper.CreateView(mapObject);

			ServerRpcListener.Instance.SendCreateEntity(mapObject);
        }

        private static void GuildAltarWorldbossFx(InstancePerson person)
        {
            
        }

        private static void SendWorldBossFx(InstancePerson person)
        {
            var map = person.syn.map;
            if (map == null)
                return;
            person.StatContainer.AddInfluence(InfluenceXmlDataList.Instance.GetInfluence(person.syn, 39, 10, 1));
            foreach (var targetObject in map.GetNearestTargetsForAttack(person.syn, 10, false))
            {
                var monster = targetObject as MonsterBehaviour;
                if (monster == null)
                    continue;
                monster.GetStatContainer().AddInfluence(InfluenceXmlDataList.Instance.GetInfluence(monster, 40, 10, -100));
            }
        }

        private void Full(InstancePerson p, int level)
        {
            level = Math.Min(Math.Max(1, level), p.gameXmlData.ExpData.MaxLevel);
            var exp = p.StatContainer.GetStatValue(Stats.Exp);
            var addexp = (int)(CommonFormulas.Instance.LevelToExp.Calc(level) - 1 - exp);
            p.AddExperience(addexp);
            CheatFull(p, level);
            if (Map.Edge.Type == LocationType.Camp)
                loginController.SendGetLobbyInfoResponse(p);
            

            p.StatContainer.SendAllStats();
        }

        public void CheatFull(InstancePerson p, int level )
        {
            p.DropItems();
            p.TryAddGold(10000000);
            shopManager.EmeraldCheat(p, 1000000);
            p.ResetPassiveSkills();

            var sqlData = new InstancePersonSqlData();
            p.InitItems(sqlData, npcTradeAlgorithm.GetInventaryTradeSaveItems());

            var tradeItems = npcTradeAlgorithm.GetNewInventaryTradeItems();

            foreach (var item in tradeItems)
            {
                if (!p.CanEquip(item))
                    continue;
                item.Count = 1000;
                p.AddItem(item);
            }

            var equipItem = p.GetItems().LastOrDefault(x => x.ItemType == ItemType.Helmet);
            if (equipItem != null)
                p.EquipItem(equipItem, out _);

             equipItem = p.GetItems().LastOrDefault(x => x.ItemType == ItemType.MainWeapon);
            if (equipItem != null)
                p.EquipItem(equipItem, out _);

            equipItem = p.GetItems().LastOrDefault(x => x.ItemType == ItemType.SecondWeapon);
            if (equipItem != null)
                p.EquipItem(equipItem, out _);

            equipItem = p.GetItems().LastOrDefault(x => x.ItemType == ItemType.Ring);
            if (equipItem != null)
                p.EquipItem(equipItem, out _);

            equipItem = p.GetItems().LastOrDefault(x => x.ItemType == ItemType.Amulet);
            if (equipItem != null)
                p.EquipItem(equipItem, out _);

            equipItem = p.GetItems().LastOrDefault(x => x.ItemType == ItemType.Artifact);
            if (equipItem != null)
                p.EquipItem(equipItem, out _);

            equipItem = p.GetItems().LastOrDefault(x => x.ItemType == ItemType.Armor);
            if (equipItem != null)
                p.EquipItem(equipItem, out _);

            equipItem = p.GetItems().LastOrDefault(x => x.ItemType == ItemType.Boots);
            if (equipItem != null)
                p.EquipItem(equipItem, out _);

            equipItem = p.GetItems().LastOrDefault(x => x.ItemType == ItemType.Belt);
            if (equipItem != null)
                p.EquipItem(equipItem, out _);

            equipItem = p.GetItems().LastOrDefault(x => x.ItemType == ItemType.Bracers);
            if (equipItem != null)
                p.EquipItem(equipItem, out _);

            equipItem = p.GetItems().LastOrDefault(x => x.ItemType == ItemType.Shoulders);
            if (equipItem != null)
                p.EquipItem(equipItem, out _);

            equipItem = p.GetItems().LastOrDefault(x => x.ItemType == ItemType.Skin);
            if (equipItem != null)
                p.EquipItem(equipItem, out _);

            p.ClearItemChanges();

            if (Map.Edge.Type == LocationType.Camp)
                ServerRpcListener.Instance.SendUpdateEntityPartResponse(p);
            else
                ServerRpcListener.Instance.UpdatePrefabName(p);
        }

		public static void BotInfo(InstancePerson person)
		{
			var targetBot = person.syn.Target;
			if (targetBot == null)
			{
				SendCheatChatMessage.Invoke("Выберите бота в таргет", person);
				return;
			}

			if(targetBot.characterType != CharacterType.Person)
			{
				SendCheatChatMessage.Invoke("Для этого чита нужен бот", person);
				return;
			}

			var wearItems = person.gameXmlData.ItemDataRepository.GetPrototypesByModels(targetBot.EquippedWears.Select(w => w.Model).ToList());
			var weaponItems = person.gameXmlData.ItemDataRepository.GetPrototypesByModels(targetBot.EquippedWeapons.Select(w => w.Model).ToList());

			var sb = new StringBuilder("\n");
			sb.Append($"---> botName: {targetBot.NickName}, botLevel: {targetBot.Level}\n");
			sb.Append($"Wears: {JsonConvert.SerializeObject(wearItems.Select(i => i.Name), Formatting.Indented)}\n");
			sb.Append($"Weapons: {JsonConvert.SerializeObject(weaponItems.Select(i => i.Name), Formatting.Indented)}\n");

			SendCheatChatMessage.Invoke(sb.ToString(), person);
		}


		public static void Info(InstancePerson p, MapTargetObject obj)
        {
            if (obj == null)
            {
                SendCheatChatMessage.Invoke("Выберите монстра в таргет", p);
                return;
            }

            MonsterBehaviour m = obj as MonsterBehaviour;
            if (m == null)
            {
                SendCheatChatMessage.Invoke("В таргете не монстр", p);
                return;
            }
            MonsterStatContainer statContainer = m.GetStatContainer() as MonsterStatContainer;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("State: " + m.state);
            sb.AppendLine("Level " + m.Level);
            sb.AppendLine("Name " + m.Data.Name);
            sb.AppendLine("Uniq " + m.Data.Uniq);
            sb.AppendLine("Element " + m.Data.Element);

            sb.AppendLine("HPcurrent " + statContainer.GetStatValue("HPcurrent"));

            List<string> statList = new List<string>();

            foreach (var s in statContainer.calcStats)
            {
                statList.Add(s.name + ": " + s.GetValue(statContainer.GetStatObjectValue) + " (CalcStat)");
            }

            foreach (Stats attr in Enum.GetValues(typeof(Stats)))
            {
                double value = statContainer.GetStatValue(attr);
                if (value != 0)
                    statList.Add(attr + ": " + value + " обычный стат");
            }

            statList.Sort();
            foreach (var s in statList)
                sb.AppendLine(s);

            SendCheatChatMessage.Invoke(sb.ToString(), p);
        }

        public static bool ShowMonsterName;
        public static bool ShowStatusChance;
        public static bool ShowCastSensor;
        public static void PrintStats(InstancePerson p)
        {
            var sb = new StringBuilder();
            sb.AppendLine("=====Main stats====");
            sb.AppendLine($"SummItemLvl = {p.StatContainer.GetStatValue("SummItemLvl")}");
            sb.AppendLine($"equippedItems = {p.StatContainer.GetStatValue("equippedItems")}");
            sb.AppendLine($"studiedAbility = {p.StatContainer.GetStatValue("studiedAbility")}");
            sb.AppendLine($"SummSkillsLvl = {p.StatContainer.GetStatValue("SummSkillsLvl")}");
            sb.AppendLine($"StudiedSkills = {p.StatContainer.GetStatValue("StudiedSkills")}");
            sb.AppendLine($"SummImproveLvl = {p.StatContainer.GetStatValue("SummImproveLvl")}");
            sb.AppendLine($"improvedItems = {p.StatContainer.GetStatValue("improvedItems")}");
            sb.AppendLine($"class = {p.StatContainer.GetStatValue("class")}");
            sb.AppendLine($"level = {p.StatContainer.GetStatValue("level")}");
            sb.AppendLine($"HPcurrent = {p.StatContainer.GetStatValue("HPcurrent")}");

            sb.AppendLine("=====Secondary stats====");
            var array = (Enum.GetValues(typeof(Stats)) as Stats[]).OrderBy(value => value.ToString()).ToArray();

            foreach (var attr in array)
                sb.AppendLine($"simple stat {attr} = {p.StatContainer.GetStatValue(attr)}");

            var calctStat = p.StatContainer.calcStats.OrderBy(x => x.name);
            foreach (var s in calctStat)
                sb.AppendLine($"calculated stat {s.name} = {s.GetValue(p.StatContainer.GetStatObjectValue)}");

            SendCheatChatMessage.Invoke(sb.ToString(), p);
        }

        public static void GetLootItem(InstancePerson p)
        {
            Item item = new Item();
            item.Rarity = ItemRarity.Usual;
            int countWar=0, countA=0, countWiz=0, countGun=0, countSha=0;

            for (int i = 0; i < 1000; i++)
            {
                var res = item.GetItem(p.syn, p);
                if (Helper.Contains(res.PersonType, PersonType.Warrior))
                    countWar++;

                if (Helper.Contains(res.PersonType, PersonType.Archer))
                    countA++;

                if (Helper.Contains(res.PersonType, PersonType.Wizard))
                    countWiz++;

                if (Helper.Contains(res.PersonType, PersonType.Gunner))
                    countGun++;

                if (Helper.Contains(res.PersonType, PersonType.Shaman))
                    countSha++;
            }
            SendCheatChatMessage.Invoke($"countWar {countWar} countA {countA} countWiz {countWiz} countGun {countGun} countSha {countSha}", p);
        }



        private static void Register(string name, int argsCount, Action<InstancePerson, string[]> func, string title, string description)
        {
            name = name.ToLower();
            cheatDic.Add(argsCount + name, func);
            cheatsDescr.Add(new KeyValuePair<string, string>(title, description));
        }

        public static void RegisterCheat(string name, Action<InstancePerson> f, string descr = "")
        {
            Register(name, 0, (p, x) => f(p), "/" + name, descr);
        }

        public static void RegisterCheat(string name, Action<InstancePerson, string> f, string descr = "")
        {
            Register(name, 1, (p, x) => f(p, x[0]), "/" + name + " string", descr);
        }

        public static void RegisterCheat(string name, Action<InstancePerson, string, bool> f, string descr = "")
        {
            Register(name, 2, (p, x) => f(p, x[0], bool.Parse(x[1])), "/" + name + " string bool", descr);
        }

        public static void RegisterCheat(string name, Action<InstancePerson, int> f, string descr = "")
        {
            Register(name, 1, (p, x) => f(p, int.Parse(x[0])), "/" + name + " int", descr);
        }


        public static void RegisterCheat(string name, Action<InstancePerson, int, int> f, string descr = "")
        {
            Register(name, 2, (p, x) => f(p, int.Parse(x[0]), int.Parse(x[1])), "/" + name + " int int", descr);
        }

        public static void RegisterCheat(string name, Action<InstancePerson, int, string> f, string descr = "")
        {
            Register(name, 2, (p, x) => f(p, int.Parse(x[0]), x[1]), "/" + name + " int string", descr);
        }

        public static void RegisterCheat(string name, Action<InstancePerson, string, int> f, string descr = "")
        {
            Register(name, 2, (p, x) => f(p, x[0], int.Parse(x[1])), "/" + name + " string int", descr);
        }

        public static void RegisterCheat(string name, Action<InstancePerson, string, string> f, string descr = "")
        {
            Register(name, 2, (p, x) => f(p, x[0], x[1]), "/" + name + " string string", descr);
        }

        public static void RegisterCheat(string name, Action<InstancePerson, string, string, string> f, string descr = "")
        {
            Register(name, 3, (p, x) => f(p, x[0], x[1], x[2]), "/" + name + " string string string", descr);
        }

        public static void RegisterCheat(string name, Action<InstancePerson, int, int, int> f, string descr = "")
        {
            Register(name, 3, (p, x) => f(p, int.Parse(x[0]), int.Parse(x[1]), int.Parse(x[2])), "/" + name + " int int int", descr);
        }

        public static void RegisterCheat(string name, Action<InstancePerson, int, float, int, float> f, string descr = "")
        {
            Register(name, 4, (p, x) => f(p, int.Parse(x[0]), float.Parse(x[1], CultureInfo.InvariantCulture), int.Parse(x[2]), float.Parse(x[3], CultureInfo.InvariantCulture)), "/" + name + " int float int float", descr);
        }

        public delegate void Action<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);

		public static void RegisterCheat<T1>(string name, Action<InstancePerson, T1> f, string descr = "")
		{
			var args = f.GetType().GetGenericArguments().ToList();
			args.RemoveAt(0);
			Register(name, args.Count,
				(p, x) => f(p,
					(T1)Convert.ChangeType(x[0], typeof(T1), CultureInfo.InvariantCulture)
				),
				"/" + name + " " + string.Join(" ", args.Select(x => x.Name).ToArray()),
				descr
			);
		}

		public static void RegisterCheat<T1, T2>(string name, Action<InstancePerson, T1, T2> f, string descr = "")
		{
			var args = f.GetType().GetGenericArguments().ToList();
			args.RemoveAt(0);
			Register(name, args.Count,
				(p, x) => f(p,
					(T1)Convert.ChangeType(x[0], typeof(T1), CultureInfo.InvariantCulture),
					(T2)Convert.ChangeType(x[1], typeof(T2), CultureInfo.InvariantCulture)
				),
				"/" + name + " " + string.Join(" ", args.Select(x => x.Name).ToArray()),
				descr
			);
		}

		public static void RegisterCheat<T1, T2, T3>(string name, Action<InstancePerson, T1, T2, T3> f, string descr = "")
		{
			var args = f.GetType().GetGenericArguments().ToList();
			args.RemoveAt(0);
			Register(name, args.Count,
				(p, x) => f(p,
					(T1)Convert.ChangeType(x[0], typeof(T1), CultureInfo.InvariantCulture),
					(T2)Convert.ChangeType(x[1], typeof(T2), CultureInfo.InvariantCulture),
					(T3)Convert.ChangeType(x[2], typeof(T3), CultureInfo.InvariantCulture)
				),
				"/" + name + " " + string.Join(" ", args.Select(x => x.Name).ToArray()),
				descr
			);
		}

		public static void RegisterCheat<T1, T2, T3, T4>(string name, Action<InstancePerson, T1, T2, T3, T4> f, string descr = "")
		{
			var args = f.GetType().GetGenericArguments().ToList();
			args.RemoveAt(0);
			Register(name, args.Count,
				(p, x) => f(p,
					(T1)Convert.ChangeType(x[0], typeof(T1), CultureInfo.InvariantCulture),
					(T2)Convert.ChangeType(x[1], typeof(T2), CultureInfo.InvariantCulture),
					(T3)Convert.ChangeType(x[2], typeof(T3), CultureInfo.InvariantCulture),
					(T4)Convert.ChangeType(x[3], typeof(T4), CultureInfo.InvariantCulture)
				),
				"/" + name + " " + string.Join(" ", args.Select(x => x.Name).ToArray()),
				descr
			);
		}
	}
}
