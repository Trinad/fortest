﻿using System;
using System.Diagnostics;
using System.Text;
using Fragoria.Common.Utils;
using LobbyInstanceLib.Networking;
using uLink;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer
{
	public class InstancePerformanceInfo : BaseGameManager
	{
		public bool Enable = false;
		public int TimeSecToLogInfo = 1;
		public bool EnableNetworkStatistic = false;
		private int countFrame;
		private float countTime;

	    private const int MATH_ROUND = 3;

		private readonly CPUUsage CPUUsage = new CPUUsage();

		private PersonsOnServer personsOnServer;

		[XmlInject]
		public void Construct(PersonsOnServer personsOnServer)
		{
			this.personsOnServer = personsOnServer;
		}

		public override void Update()
		{
			if (!Enable)
				return;
				
			countFrame++;
			countTime += Time.deltaTime;
			if (countTime >= TimeSecToLogInfo)
			{
                int newFPSValue = (int)Math.Round(countFrame / countTime);
				countTime = 0;
				countFrame = 0;

				const int bytesInMb = 1048576;
				long totalGcMemory = GC.GetTotalMemory(false) / bytesInMb;
				var process = Process.GetCurrentProcess();
				long processPrivateMemory = process.PrivateMemorySize64 / bytesInMb;
				long processWorkingMemory = process.PeakWorkingSet64 / bytesInMb;
				var cpuUsage = CPUUsage.Read();

				ILogger.Instance.Send($"Instance FPS = {newFPSValue}; CPU usage = {cpuUsage:0.00}%; GC memory = {totalGcMemory} MB, processPrivateMemory = {processPrivateMemory} MB, " +
									  $"processWorkingMemory {processWorkingMemory} MB");

				if (EnableNetworkStatistic)
				{
					double bytesSentPerSecond = 0,
						bytesReceivedPerSecond = 0,
						userBytesSentPerSecond = 0,
						userBytesReceivedPerSecond = 0;
					long totalMessageDuplicatesRejected = 0, 
						 totalMessagesResent = 0, 
						 totalMessagesSent = 0, 
						 totalMessagesStored = 0, 
						 totalMessageSequencesRejected = 0, 
						 totalMessagesUnsent = 0, 
						 totalMessagesWithheld = 0;

					foreach (var networkPlayer in uLink.Network.Instance.connections)
					{
						var statistic = uLink.Network.Instance.GetStatistics(networkPlayer);
						bytesSentPerSecond += statistic.bytesSentPerSecond;
						bytesReceivedPerSecond += statistic.bytesReceivedPerSecond;
						userBytesSentPerSecond += statistic.userBytesSentPerSecond;
						userBytesReceivedPerSecond += statistic.userBytesReceivedPerSecond;

						totalMessageDuplicatesRejected += statistic.messageDuplicatesRejected;
						totalMessagesResent += statistic.messagesResent;
						totalMessagesSent += statistic.messagesSent;
						totalMessagesStored += statistic.messagesStored;
						totalMessageSequencesRejected += statistic.messageSequencesRejected;
						totalMessagesUnsent += statistic.messagesUnsent;
						totalMessagesWithheld += statistic.messagesWithheld;

						var person = personsOnServer.GetPerson(networkPlayer);
						var ping = networkPlayer.averagePing();
						ILogger.Instance.Send($"Player {person.personName ?? networkPlayer.ToString()} ping {ping} ms");
					}

                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine($"bytesSentPerSecond = {Math.Round(bytesSentPerSecond, MATH_ROUND)}, bytesReceivedPerSecond = {Math.Round(bytesReceivedPerSecond, MATH_ROUND)}, userBytesSentPerSecond = {Math.Round(userBytesSentPerSecond, MATH_ROUND)}, userBytesReceivedPerSecond = {Math.Round(userBytesReceivedPerSecond, MATH_ROUND)}");

                    sb.AppendLine($"totalMessageDuplicatesRejected = {totalMessageDuplicatesRejected}, totalMessagesSent = {totalMessagesSent} totalMessagesResent = {totalMessagesResent}, totalMessagesStored = {totalMessagesStored}, " +
                                  $"totalMessageSequencesRejected = {totalMessageSequencesRejected}, totalMessagesUnsent = {totalMessagesUnsent}, totalMessagesWithheld = {totalMessagesWithheld}" );

                    ILogger.Instance.Send(sb.ToString());
				}
			}
		}
	}
}
