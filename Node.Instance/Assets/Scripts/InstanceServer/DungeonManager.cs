﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InstanceServer.Analytic;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Extensions;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries
{
    public class DungeonManager
    {
	    public Action<InstancePerson> SendDungeonInfoResponse;

		private readonly ShopManager shopManager;

		private readonly List<DungeonXmlData> dungeonXmlDatas;

        public DungeonManager(GameXmlData gameXmlData, ShopManager shopManager)
        {
			this.shopManager = shopManager;

			dungeonXmlDatas = gameXmlData.Dungeons;

			var dungeonEdge = gameXmlData.Edges.EdgeDatas.FirstOrDefault(x => x.Type == "Dungeon");
			if (dungeonEdge != null)
			{
				var locationXmlDatas = dungeonEdge.EdgeLocations.ToDictionary(x => x.LevelName);
				foreach (var dungeonXmlData in dungeonXmlDatas)
					foreach (var dungeon in dungeonXmlData.DungeonLocations)
						dungeon.location = locationXmlDatas[dungeon.LevelName];
			}

			Cheats.RegisterCheat("DOpen", p => ForceUpdateDungeonState(p), "Открывает меню с данжами. Возможно когда-то этот чит будет полезен");
			Cheats.RegisterCheat("DResetTime", (p, locationId, sec) =>
			{
				var loca = p.DungeonPersonLocationDatas.Find(data => data.LocationId == locationId);
				loca.SetTime(DateTime.UtcNow.AddSeconds(sec));
				ForceUpdateDungeonState(p);
			}, "Не работает. Должен сбрасывать таймер на вход в данж");
		}

        public List<DungeonRpcData> GetDungeonRpcDatas(InstancePerson person)
        {
			AddDungeonLocationPersonData(person); //TODO_deg пусть пока здесь, maximpr не нравится что при создании перса вызывалось

			var dungeonRpcDatas = new List<DungeonRpcData>();

            foreach (var dungeonXmlData in dungeonXmlDatas)
            {
				if (dungeonXmlData.Enable != null && !dungeonXmlData.Enable.Calc(person.StatContainer))
					continue;

                var personLocations = person.DungeonPersonLocationDatas.FindAll(location => location.EdgeId == dungeonXmlData.EdgeId);
                var newRpcData = new DungeonRpcData(dungeonXmlData, personLocations);
                dungeonRpcDatas.Add(newRpcData);
            }

            return dungeonRpcDatas;
        }

        // добавляем новые данжи
        private void AddDungeonLocationPersonData(InstancePerson person)
        {
            int locationCount = dungeonXmlDatas.Sum(xmlData => xmlData.DungeonLocations.Count);

            if (person.DungeonPersonLocationDatas != null && (person.DungeonPersonLocationDatas.Count == 0 || person.DungeonPersonLocationDatas.Count != locationCount))
            {
                foreach (var dungeonXml in dungeonXmlDatas)
                {
                    foreach (var locationXml in dungeonXml.DungeonLocations)
                    {
                        var fLoc = person.DungeonPersonLocationDatas.Find(personLocation => personLocation.LocationId == locationXml.LocationId);
                        if (fLoc != null)
                            continue;

                        var dungeonData = new DungeonPersonFloorData(dungeonXml.EdgeId, locationXml.LocationId);
						dungeonData.SetControlState(locationXml.Floor == 1 ? ControlState.Close : ControlState.Lock);
                        person.DungeonPersonLocationDatas.Add(dungeonData);
                    }
                }
            }
        }

		// Для читов
	    private void ForceUpdateDungeonState(InstancePerson person)
        {
            person.UpdateDungeonState();
	        SendDungeonInfoResponse?.Invoke(person);
        }

        public bool OpenDungeonLocationRequest(InstancePerson person, int dungeonId, NeedForResource resData, bool isBuy)
        {
			person.UpdateDungeonState();

			if (isBuy)
                return DungeonPressAction(person, dungeonId, resData);

			SendDungeonInfoResponse?.Invoke(person);
			return false;
        }

        private bool DungeonPressAction(InstancePerson person, int dungeonId, NeedForResource resData)
        {
            var personDungeonData = person.DungeonPersonLocationDatas.Find(location => location.LocationId == dungeonId);
            if (personDungeonData == null)
            {
                ILogger.Instance.Send("DungeonPressAction: personDungeonData == null", "currentDungeonData == null", ErrorLevel.error);
                return false;
            }

            var dungeonXmldata = dungeonXmlDatas.Find(location => location.EdgeId == personDungeonData.EdgeId);
            if (dungeonXmldata == null)
            {
                ILogger.Instance.Send("DungeonPressAction: dungeonXmldata == null", "currentDungeonData == null", ErrorLevel.error);
                return false;
            }

            switch (personDungeonData.ControlState)
            {
                case ControlState.Lock:// если кто захочет ломануть клиент и разлочить не по честному, то снять весь изюм и отправить в лок)))
                    //person.BuyForEmeralds(person.Emeralds, AccountChangeReason.ResetCoolDownTimer, (@params =>
                    //{
                    //    if (@params.success)
                    //    {
                    //        SetControlState(personDungeonData, ControlState.Lock);
                    //        ServerRpcListener.Instance.SendDungeonInfoResponse(person, GetDungeonRpcDatas(person));
                    //    }
                    //}));
                    ILogger.Instance.Send("DungeonPressAction Hack", ErrorLevel.error);
                    return false;

                case ControlState.Close:
                    //пытаемся открыть данж
                    if (person.AddResource(ResourceType.KeyStone, -personDungeonData.Price))
                    {
                        personDungeonData.SetControlState(ControlState.Open);
                        //ServerRpcListener.Instance.SendDungeonInfoResponse(person, GetDungeonRpcDatas(person));
                        return true;
                    }
                    else
                    {
                        resData.NeedResource(ResourceType.KeyStone, personDungeonData.Price - person.CountResource(ResourceType.KeyStone));
                        ServerRpcListener.Instance.SendRpc(person.ClientRpc.ErrorDataResponse, resData.GetError(person));
                    }
                    return false;

                case ControlState.Open:
                    //пытаемся зайти повторно в открытый данж
                    if (person.AddResource(ResourceType.KeyStone, -personDungeonData.Price))
                    {
                        ILogger.Instance.Send("DungeonToEnter: success");
                        return true;
                    }
                    else
                    {
                        resData.NeedResource(ResourceType.KeyStone, personDungeonData.Price - person.CountResource(ResourceType.KeyStone));
                        ServerRpcListener.Instance.SendRpc(person.ClientRpc.ErrorDataResponse, resData.GetError(person));
                    }
                    return false;

                case ControlState.CooldownTimer:
                    var timeSpan = (personDungeonData.Time - DateTime.UtcNow).TotalMinutes;
                    var price = CooldownDungeonFormula.Instance.Price.IntCalc(personDungeonData.Price, timeSpan);
                    shopManager.BuyForEmeralds(person, price, AccountChangeReason.ResetCoolDownTimer, result =>
                    {
                        if (result.success)
                        {
                            personDungeonData.SetControlState(ControlState.Close);
							SendDungeonInfoResponse?.Invoke(person);

                            var analyticInfo = new AnalyticCurrencyInfo()
                            {
                                needSendInWallet = false,
                                sendGain = false,
                                Amount = price,
                                Destination = SinkType.Unknown.ToString(),
                                DestinationType = "dangeod",
                                Currency = ResourceType.Emerald,
                                USDCost = 0f,
								PersonLevel = person.login.GetMaxPersonLevel(),
								TestPurchase = person.login.settings.testPurchase
							};

						AnalyticsManager.Instance.SendData(person, analyticInfo);
                        }
                        else
                        {
                            resData.NeedResource(ResourceType.Emerald, price - person.Emeralds);
                            ServerRpcListener.Instance.SendRpc(person.ClientRpc.ErrorDataResponse, resData.GetError(person));
                        }
                    });
                    return false;
				
				default:
					ILogger.Instance.Send("DungeonPressAction: personDungeonData.ControlState unknown state", ErrorLevel.error);
					return false;
			}
        }
    }
}
