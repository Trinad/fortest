﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.InstanceServer.Analytic;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking.ClientDataRPC;
using CSharpx;
using Newtonsoft.Json;
using UtilsLib;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer
{
	public class SpecialActionManager
	{
		public SpecialOffer SpecialOffer { get; private set; }

		ShopManager shopManager;
		public SpecialActionManager(SpecialOffer specialOffer, ShopManager shopManager)
		{
			SpecialOffer = specialOffer;
			this.shopManager = shopManager;
		}

		#region Cheats
		public SpecialRpcData CheatDelOffers(InstancePerson instancePerson)
		{
			instancePerson.login.OffersDatas.Clear();
			instancePerson.login.OffersIdForAdd.Clear();

			var result = new SpecialRpcData();
			result.SpecialOfferDatas = new List<SpecialRpcItemData>();
			result.OffersForAdd = new List<SpecialRpcItemData>();

			return result;
		}

		public SpecialRpcData CheatDelAllGetOfferById(InstancePerson instancePerson, int offerId, bool checkRules)
		{
			instancePerson.login.OffersDatas.Clear();
			instancePerson.login.OffersIdForAdd.Clear();

			var result = new SpecialRpcData();
			var activeOffers = new List<SpecialOfferItem>();

			var cheatOffer = SpecialOffer.SpecialOfferItems.Find(o => o.OfferId == offerId);
			if (cheatOffer == null)
			{
				ILogger.Instance.Send($" CheatDelAllGetOfferById specialOfferItem == null offerId: {offerId}", ErrorLevel.error);
				return null;
			}

			if (checkRules)
			{
				if (!cheatOffer.OfferUnlockRule(instancePerson))
				{
					ILogger.Instance.Send($" CheatDelAllGetOfferById checkRules = true OfferUnlockRule = false", ErrorLevel.error);
					return null;
				}
			}

			activeOffers.Add(cheatOffer);
			instancePerson.login.AddOffer(cheatOffer);

			result.SpecialOfferDatas = converToRpcSpecialOffers(instancePerson, activeOffers);
			return result;
		}

		/// <summary>
		/// удаляем первый из списка и выдаем по Ид или если меньше 3х то добиваем оффер по Ид
		/// </summary>
		/// <param name="instancePerson"></param>
		/// <param name="id"></param>
		public SpecialRpcData CheatGetOfferById(InstancePerson instancePerson, int id)
		{
			var login = instancePerson.login;

			var result = new SpecialRpcData();

			var activeOffers = GetOffersFromLogin(login);
			var cheatOffer = SpecialOffer.SpecialOfferItems.Find(o => o.OfferId == id);
			if (activeOffers.Count >= SpecialOffer.ClientOfferCount)
			{
				var stringBuilder = new StringBuilder();

				var toDelete = activeOffers.FirstOrDefault();
				if (toDelete != null)
				{
					stringBuilder.AppendFormat($" toDelete: {toDelete.OfferId}");

					activeOffers.Remove(toDelete);
					instancePerson.login.RemoveOffer(toDelete.OfferId);

					activeOffers.Add(cheatOffer);
					instancePerson.login.AddOffer(cheatOffer);
					stringBuilder.AppendFormat($"cheatOffer: {cheatOffer.OfferId}");

					result.SpecialOfferDatas = converToRpcSpecialOffers(instancePerson, activeOffers);

					ILogger.Instance.Send($"CheatGetOfferById I activeOffers.Count: {activeOffers.Count} stringBuilder: {stringBuilder}", $" activeOffers: {JsonConvert.SerializeObject(activeOffers, Formatting.Indented)}", ErrorLevel.warning);
				}
			}
			else
			{
				activeOffers.Add(cheatOffer);
				instancePerson.login.AddOffer(cheatOffer);

				result.SpecialOfferDatas = converToRpcSpecialOffers(instancePerson, activeOffers);

				ILogger.Instance.Send($"CheatGetOfferById II cheatOffer: {JsonConvert.SerializeObject(cheatOffer)}", $" activeOffers: {JsonConvert.SerializeObject(activeOffers, Formatting.Indented)}", ErrorLevel.warning);
			}
			return result;
		}


		#endregion

		private string Log(SpecialRpcData specialRpc)
		{
			var stringBuilder = new StringBuilder("SpecialOfferDatas");
			foreach (var offerData in specialRpc.SpecialOfferDatas)
			{
				stringBuilder.Append($"\n offerData: {offerData.OfferId} Duration: {TimeProvider.UTCNow.AddSeconds(offerData.Duration)}, ShowForcibly: {offerData.ShowForcibly}");
			}

			stringBuilder.Append("\nOffersForAdd");

			foreach (var offerData in specialRpc.OffersForAdd)
			{
				stringBuilder.Append($"\n OffersForAdd: {offerData}");

			}
			return stringBuilder.ToString();
		}


		/// <summary>
		/// выдаем новые/старые оферы обновляем текущие оферы
		/// актуальные отфильтрованные оферы игроку 
		/// </summary>
		/// <param name="person"></param>
		/// <returns></returns>
		public SpecialRpcData GetAllActualSpecialRpcData(InstancePerson person)
		{
			var login = person.login;

			var result = new SpecialRpcData();
			result.SpecialOfferDatas = new List<SpecialRpcItemData>();
			result.OffersForAdd = new List<SpecialRpcItemData>();
			if (login.MustApplyOffers()) // если есть не купленные то надо прислать старый список с кэшом не купленных
			{
				result.OffersForAdd = converToRpcSpecialOffers(person, GetCacheOffers(login));
				ILogger.Instance.Send($"GetAllActualSpecialRpcData result.OffersForAdd.Count > 0: {result.OffersForAdd.Count}", $"offers: {JsonConvert.SerializeObject(login.OffersIdForAdd, Formatting.Indented)}", ErrorLevel.info);
				return result;
			}

			UpdateOffers(login);
			ILogger.Instance.Send($"GetAllActualSpecialRpcData after UpdateOffers login.OffersDatas.Count: {login.OffersDatas.Count}", $"offers: {JsonConvert.SerializeObject(login.OffersDatas, Formatting.Indented)}", ErrorLevel.info);
			var availableOffers = availableOffersCountInLogin(login); // сколько уже есть доступных у игрока
			if (availableOffers >= SpecialOffer.ClientOfferCount)
			{
				// если у нас OfferCount акций ещё доступны то просто отдаем их клиенту
				var activeOffers = GetOffersFromLogin(login);
				result.SpecialOfferDatas = converToRpcSpecialOffers(person, activeOffers);
				ILogger.Instance.Send($"GetAllActualSpecialRpcData old availableOffers: {login.OffersDatas.Count}", $"offers: {JsonConvert.SerializeObject(login.OffersDatas, Formatting.Indented)}", ErrorLevel.info);
			}
			else
			{
				// если у нас меньше 3х акций то надо добрать что бы получилось OfferCount, возможно будет меньше в зависимости от условий. а может и вообще не одной тогда 
				// каждый раз будем ковырять пул при коннекте
				List<SpecialOfferItem> newOfferList = new List<SpecialOfferItem>();
				var activeLoginOffers = GetOffersFromLogin(login); //выбираем те что ещё активны
				newOfferList.AddRange(activeLoginOffers);

				var offersPool = getNewActualOffers(login); // получаем новые оферы после фильтрации 
				var randomOffers = getRandomOffers(offersPool, Math.Max(0, SpecialOffer.ClientOfferCount - availableOffers)); // выбираем рандомно по весу недостающее количество
				newOfferList.AddRange(randomOffers);// добавляем к активным

				person.login.AddOffers(randomOffers);// регистрируем новые у логина
				result.SpecialOfferDatas = converToRpcSpecialOffers(person, newOfferList);
				ILogger.Instance.Send($"GetAllActualSpecialRpcData add new offers: {login.OffersDatas.Count}", $"{JsonConvert.SerializeObject(login.OffersDatas, Formatting.Indented)}", ErrorLevel.info);
			}

			var log = Log(result);
			ILogger.Instance.Send($"SpecialActionResponse to client loginId: {person.login.loginId}, offers: {result.SpecialOfferDatas.Count} OffersForAdd: {result.OffersForAdd.Count}", log, ErrorLevel.info);

			return result;
		}

		private List<SpecialOfferItem> getNewActualOffers(LobbyLogin login)
		{
			var result = new List<SpecialOfferItem>();

			var currentOffers = SpecialOffer.SpecialOfferItems.FindAll(o => !o.Inactive);
			//currentOffers = GetOffersPool(login, currentOffers); TODO fil логика работы с пулами

			foreach (var specialOfferItem in currentOffers)
			{
				if (AddExistInLoginOffer(login, specialOfferItem, result))
					continue;

				if (AddNotExistInLoginOffer(login, specialOfferItem, result))
					continue;
			}

			return result;
		}

		private bool AddExistInLoginOffer(LobbyLogin login, SpecialOfferItem specialOfferItem, List<SpecialOfferItem> result)
		{
			var offer = login.OffersDatas.Find(o => o.OfferId == specialOfferItem.OfferId);
			if (offer != null)
			{
				ILogger.Instance.Send($"AddExistInLoginOffer offer is exist offer: {offer.OfferId}", ErrorLevel.info);
				return true;
			}

			return false;
		}

		private bool AddNotExistInLoginOffer(LobbyLogin login, SpecialOfferItem specialOfferItem,
			List<SpecialOfferItem> result)
		{
			if (specialOfferItem.OfferUnlockRule(login.Person))
			{
				result.Add(specialOfferItem);
				return true;
			}

			return false;
		}

		/// <summary>
		/// купили офер заполнить список на выдачу отправить команду что нужно выбрать на кого
		/// </summary>
		/// <param name="instancePerson"></param>
		/// <param name="offerId"></param>
		/// <param name="InApp"></param>
		public void BuySpecialOffer(InstancePerson instancePerson, SpecialOfferItem specialOfferItem)
		{
            var OfferBoughtInfo = new OfferBoughtInfo()
            {
                OfferID = specialOfferItem.OfferId,
                TransactionId = AnalyticsManager.GetSHA256(TimeProvider.UTCNow.ToString() + instancePerson.loginId.ToString())
            };

            instancePerson.login.AddToOffersIdForAdd(OfferBoughtInfo);

            var newBought = new OfferBillInfo() { BoughtTime = TimeProvider.UTCNow, Price = specialOfferItem.Price};

			instancePerson.login.RemoveOldBought(SpecialOffer.DaysForDelFromOffersBills);
			instancePerson.login.AddOffersBought(newBought);

            var analyticInfo = new AnalyticCurrencyInfo()
            {
                needSendInWallet = false,
                sendGain = false,
                Amount = specialOfferItem.Price,
                Destination = SinkType.ShopBundle.ToString(),
                DestinationType = specialOfferItem.GetName(),
                Currency = ResourceType.USD,
                USDCost = specialOfferItem.Price,
                RealMoneyPurchase = true,
                PersonLevel = instancePerson.login.GetMaxPersonLevel(),
                Sku = specialOfferItem.Sku,
                transactionId = OfferBoughtInfo.TransactionId,
                IsStock = true,
				TestPurchase = instancePerson.login.settings.testPurchase
			};

			AnalyticsManager.Instance.SendData(instancePerson, analyticInfo);
			GlobalEvents.Instance.OnPersonDonat(instancePerson, specialOfferItem.Price, specialOfferItem.GetName());
			ILogger.Instance.Send($"======= BuySpecialOffer AddToOffersIdForAdd offerId: {specialOfferItem.OfferId} ");
		}


		/// <summary>
		/// запрос на выдачу наград на выбранного перса
		/// </summary>
		/// <param name="instancePerson"></param>
		/// <param name="personId"></param>
		public void SpecialOfferReward(InstancePerson instancePerson, int personId)
		{
			var login = instancePerson.login;

            SpecialOfferItem specialOfferItemReward = null;
            OfferBoughtInfo offerBoughtInfo = null;
            foreach (var offerInfo in login.OffersIdForAdd)
            {
                var offerForAdd = SpecialOffer.SpecialOfferItems.Find(o => o.OfferId == offerInfo.OfferID);
                if (offerForAdd != null)
                {
                    offerBoughtInfo = offerInfo;
                    specialOfferItemReward = offerForAdd;
                    break;
                }
            }

            var offersToAdd = GetCacheOffers(login);
			if (!login.MustApplyOffers())
			{
				ILogger.Instance.Send($"SpecialOfferReward hack offers: {GetCacheOffers(login).Count}, instancePerson: {instancePerson.personId}", ErrorLevel.error);
				return;
			}

			if (specialOfferItemReward == null)
			{
				ILogger.Instance.Send($"SpecialOfferReward offersCacheId == null offersToAddList: {JsonConvert.SerializeObject(GetCacheOffers(login))}", ErrorLevel.error);
				return;
			}

			InstancePerson needPerson = null;
			needPerson = instancePerson.login.InstancePersons.Find(p => p.personId == personId);
			if (needPerson == null)
			{
				ILogger.Instance.Send($"specialOfferReward eedPerson == null personId: {personId} login: {instancePerson.login.loginName} specialOfferItem: {specialOfferItemReward.OfferId}", ErrorLevel.error);
				return;
			}

			//var items = specialOfferReward(specialOfferItemReward, instancePerson, personId); //выдаем награду

			var itemList = specialOfferItemReward.Content.GetItemList(null, needPerson);


            var analyticInfo = new AnalyticCurrencyInfo()
            {
                needSendInWallet = true,
                sendGain = true,
                USDCost = specialOfferItemReward.Price,
                Source = SourceGain.PurchaseShop,
                SourceType = specialOfferItemReward.OfferName,
                PersonLevel = instancePerson.login.GetMaxPersonLevel(),
                RealMoneyPurchase = true,
                Sku = specialOfferItemReward.Sku,
                transactionId = offerBoughtInfo.TransactionId,
                IsStock = true,
				TestPurchase = instancePerson.login.settings.testPurchase
			};

			shopManager.SendBuyItemsResponse(instancePerson, BuyItemResponseType.SpecialAction, itemList);
			foreach (var item in itemList)
			{
				analyticInfo.Amount = item.Count;
				analyticInfo.Currency = item.Resource;

				needPerson.AddItem(item, Source: ItemSourceType.PurchaseShop, analyticInfo: analyticInfo);
			}

			login.RemoveOffersCacheId(specialOfferItemReward.OfferId);

			RecalcLoginOfferState(login, specialOfferItemReward, specialOfferItemReward.OfferId);
			//if (items != null)
			//{
			//    //TODO  fil analytics
			//    //AnalyticsManager.Instance.AnaliticsInAppPack(person, items);
			//    //AnalyticsManager.Instance.SpendInnerCurrencyEvent(person, SourceType.shop_realmoney.ToString(), Dev2DevSource.shop_realmoney.ToString(), SpendType.Default.ToString(), specialOfferItem.MainPrice.Price);
			//}

			ILogger.Instance.Send($"SpecialOfferReward().. was rewarded loginName: {login.loginName} offer: {specialOfferItemReward.OfferId}", ErrorLevel.info);
		}

		private List<SpecialOfferItem> GetCacheOffers(LobbyLogin login)
		{
			var result = new List<SpecialOfferItem>();
			foreach (var offerInfo in login.OffersIdForAdd)
			{
				var offerForAdd = SpecialOffer.SpecialOfferItems.Find(o => o.OfferId == offerInfo.OfferID);
				if (offerForAdd != null)
				{
					result.Add(offerForAdd);
				}
			}

			return result;
		}

		private void RecalcLoginOfferState(LobbyLogin login, SpecialOfferItem specialOfferItem, int offersCacheId)
		{
			var soldOffer = login.OffersDatas.Find(o => o.OfferId == offersCacheId);
			if (soldOffer == null)
			{
				ILogger.Instance.Send($"SpecialOfferReward soldOffer == null offerId: {offersCacheId} login: {login.loginName}, personId: {login.Person.personId}", ErrorLevel.error);
				return;
			}
			soldOffer.UpdateReason = UpdateOfferReason.Cooldown;
			soldOffer.TimeForUpdate = TimeProvider.UTCNow.AddSeconds(specialOfferItem.Cooldown);

			ILogger.Instance.Send($"RecalcLoginOfferState().. was rewarded offerXml == null soldOffer: {JsonConvert.SerializeObject(soldOffer)}", ErrorLevel.info);
		}

		private int maxClassCount(InstancePerson instancePerson)
		{
			var classesGroupList = instancePerson.login.InstancePersons.GroupBy((person => person.personType)).ToList();
			var countGroups = classesGroupList.Select(g => g.Count());
			var maxClassCount = countGroups.Max();

			return maxClassCount;
		}
		public void UpdateOffers(LobbyLogin login)
		{
			for (var i = 0; i < login.OffersDatas.Count; i++)
			{
				var offer = login.OffersDatas[i];
				var offerXml = SpecialOffer.SpecialOfferItems.Find(o => o.OfferId == offer.OfferId);
				if (offerXml == null)
				{
					ILogger.Instance.Send($"UpdateOffers specialOffer was not finded OfferId: {offer.OfferId}", ErrorLevel.error);
					continue;
				}

				if (offerXml.Inactive)
				{
					login.OffersDatas.RemoveAt(i--);
					continue;
				}
				//TODO fil проверка на инактив обработка инактив

				//ILogger.Instance.Send($"UpdateOffers =======CHECK===== TimeProvider.UTCNow: {TimeProvider.UTCNow} >= {offer.TimeForUpdate}", $"offer: {JsonConvert.SerializeObject(offer)}", ErrorLevel.warning);
				if ((TimeProvider.UTCNow - offer.TimeForUpdate).TotalSeconds + 1 > 0 && offer.UpdateReason == UpdateOfferReason.Duration)
				{
					offer.TimeForUpdate = offer.TimeForUpdate.AddSeconds(offerXml.Cooldown);
					offer.UpdateReason = UpdateOfferReason.Cooldown;

					//ILogger.Instance.Send($"UpdateOffers to Cooldown TimeProvider.UTCNow: {TimeProvider.UTCNow} >= {offer.TimeForUpdate} offer:", $"{JsonConvert.SerializeObject(offer)}", ErrorLevel.warning);
				}

				if ((TimeProvider.UTCNow - offer.TimeForUpdate).TotalSeconds + 1 > 0 && offer.UpdateReason == UpdateOfferReason.Cooldown)
				{
					login.OffersDatas.RemoveAt(i--);
					//ILogger.Instance.Send($"UpdateOffers to Duration delete offer TimeProvider.UTCNow: {TimeProvider.UTCNow} >= {offer.TimeForUpdate}", $"offer: {JsonConvert.SerializeObject(offer)}", ErrorLevel.warning);
				}
			}
		}


		int availableOffersCountInLogin(LobbyLogin login)
		{
			var count = 0;
			foreach (var offer in login.OffersDatas)
			{
				if (AvailableOfferByTime(offer))
				{
					count++;
				}
			}
			return count;
		}

		// выдать недостающее рандомное количество оферов
		private List<SpecialOfferItem> getRandomOffers(List<SpecialOfferItem> actualOffers, int count)
		{
			var weightlList = actualOffers.Select(o => o.Weight).ToList();
			var offersCount = Math.Min(actualOffers.Count, count);
			var result = FRRandom.Random(actualOffers, weightlList, offersCount);
			return result;
		}

		private List<SpecialRpcItemData> converToRpcSpecialOffers(InstancePerson person, List<SpecialOfferItem> offers)
		{
			var list = new List<SpecialRpcItemData>();

			foreach (var offer in offers)
			{
				var actionData = GetSpecialRpcItemData(person, offer);
				if (actionData == null)
					continue;

				list.Add(actionData);
			}

			return list;
		}

		private SpecialRpcItemData GetSpecialRpcItemData(InstancePerson person, SpecialOfferItem offer)
		{
			var pricePoint = shopManager.GetSpecialOfferPricePoint(person, offer.OfferId);
			if (pricePoint == null)
			{
				ILogger.Instance.Send($"GetSpecialRpcItemData pricePoint == null offerData: {offer.OfferId}", ErrorLevel.error);
				return null;
			}

			var loginOffer = person.login.GetLoginOffers(offer);

			var res = new SpecialRpcItemData(person, offer.OfferGui, offer.Content);
			res.OfferId = offer.OfferId;
			res.OfferName = offer.OfferName;
			res.Sku = offer.Sku;
			res.TechnicalNameTitle = offer.TechnicalNameTitle;
			res.TechnicalNameDesc = offer.TechnicalNameDesc;
			res.PersonType = offer.PersonType;
			res.PersonLevel = offer.PersonLevel;
			res.Price = offer.Price;

			res.Duration = loginOffer == null ? offer.Duration : loginOffer.GetTime();

			res.ShowForcibly = offer.ShowForcibly;
			return res;
		}

		private List<SpecialOfferItem> GetOffersFromLogin(LobbyLogin login)
		{
			var result = new List<SpecialOfferItem>();

			for (var i = 0; i < login.OffersDatas.Count; i++)
			{
				var offerData = login.OffersDatas[i];
				var specialOffer = SpecialOffer.SpecialOfferItems.Find(o => o.OfferId == offerData.OfferId);
				if (specialOffer == null)
				{
					ILogger.Instance.Send($"GetOffersFromLogin specialOffer == null offerData: {offerData.OfferId} login: {login.loginId}", ErrorLevel.error);
					continue;
				}

				if (AvailableOfferByTime(offerData))
				{
					result.Add(specialOffer);
				}
			}

			return result;
		}


		private bool AvailableOfferByTime(LoginOffer offer)
		{
			switch (offer.UpdateReason)
			{
				case UpdateOfferReason.Cooldown:
					if (TimeProvider.UTCNow < offer.TimeForUpdate)
						return false;
					break;
				case UpdateOfferReason.Duration:
					if (TimeProvider.UTCNow < offer.TimeForUpdate)
						return true;
					break;
				default: break;
			}
			return false;
		}


		private List<SpecialOfferItem> GetOffersPool(LobbyLogin login, List<SpecialOfferItem> currentOffers)
		{
			var result = new List<SpecialOfferItem>();
			if (login.LastDeclineOffer != null)
			{
				var price = login.LastDeclineOffer.Price;
				var poolNumber = SpecialOffer.GetPoolNumber(price);

				foreach (var currentOffer in currentOffers)
				{
					var offerPool = SpecialOffer.GetPoolNumber(currentOffer.Price);

					if (offerPool < poolNumber || poolNumber == 0)
					{
						result.Add(currentOffer);
					}
				}
			}
			else if (login.LastBuyOffer != null)
			{
				var price = login.LastBuyOffer.Price;
				var poolNumber = SpecialOffer.GetPoolNumber(price);

				foreach (var currentOffer in currentOffers)
				{
					var offerPool = SpecialOffer.GetPoolNumber(currentOffer.Price);

					if (offerPool > poolNumber)
					{
						result.Add(currentOffer);
					}
				}
			}
			else
			{
				foreach (var currentOffer in currentOffers)
				{
					var offerPool = SpecialOffer.GetPoolNumber(currentOffer.Price);

					if (offerPool == 0)
					{
						result.Add(currentOffer);
					}
				}
			}
			return result;
		}


		/// <summary>
		/// выдать все оферы в основном для тестов
		/// </summary>
		/// <param name="person"></param>
		/// <returns></returns>
		public SpecialRpcData GetAllSpecialRpcData(InstancePerson person)
		{
			var result = new SpecialRpcData();
			result.SpecialOfferDatas = converToRpcSpecialOffers(person, SpecialOffer.SpecialOfferItems);
			return result;
		}
	}
}
