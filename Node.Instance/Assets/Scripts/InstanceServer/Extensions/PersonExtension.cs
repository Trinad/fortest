﻿using System;
using System.Collections.Generic;
using System.Text;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.DegSerializers;

namespace Assets.Scripts.InstanceServer.Extensions
{
    public static class PersonExtension
    {
        public static bool RewardLoots(this InstancePerson person, IConcreteLoot loot, bool sendMail, out List<InventoryItemData> items)
        {
            var rewardItems = new List<InventoryItemData>();
            Action<InventoryItemData> addItemInBag = item => rewardItems.Add(item.Copy());
            person.AddItemInBag += addItemInBag;
            
            bool success = loot.Reward(person);
            person.AddItemInBag -= addItemInBag;

            items = rewardItems;
            return success;
        }

		public static void UpdateDungeonState(this InstancePerson person)
		{
			foreach (var floorData in person.DungeonPersonLocationDatas)
				floorData.CheckTimeFloor();

			//person.LoggerDungeonPersonLocationDatas();
		}

		private static void LoggerDungeonPersonLocationDatas(this InstancePerson person)
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (var xmlData in person.DungeonPersonLocationDatas)
			{
				stringBuilder.AppendFormat("edge: {0}, LocationId: {1}, time: {2}, state: {3}", xmlData.EdgeId, xmlData.LocationId, xmlData.Time, xmlData.ControlState);
				stringBuilder.Append("\n");
			}
			ILogger.Instance.Send("UpdateDungeonPersonData:", stringBuilder.ToString(), ErrorLevel.warning);
		}

		public static int GetTargetId(this MapTargetObject target)
		{
			if (target == null)
				return 0;

			if (target is PersonBehaviour person)
				return person.owner.personId;

			return -target.Id;
		}

		public static int GetTargetId(this InstancePerson person)
		{
			return person.personId;
		}
	}
}
