﻿using System;
using System.Linq;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Logger;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Games
{
    public class DeathmatchSolo : MatchBase
    {
		[XmlAttribute]
		public int MaxScoreEnd = 500;
        
        [XmlAttribute]
        public int WinnerPositionBorder = 3;

        [XmlAttribute]
        public bool NotifyEnable = true;

        private const float SEND_RATE_STATISTICS = 1.0f;

        private DeathmatchSoloStatistic statistic;
        private StatisticSendMode m_statsSendMode;
        private float m_nextStatsSendTime;

        public override void Start(Map sceneMap)
        {
			base.Start(sceneMap);
            map.PlayerConnected += (person, reconnect, changePerson) => PlayerEnterGame(person, changePerson, false, reconnect);
            map.VisitLocation += (person) => PlayerEnterGame(person, false, true, false);

            map.bCanDropItemsInLoot = false;
            gameInfoData = InstanceGameManager.Instance.ArenaAlgorithm.GetGameInfoData(ArenaMatchType.Deathmatch, map.LevelName);
            matchTime = gameInfoData.GameTimeSec;
            
            statistic = map.Scenarios.OfType<DeathmatchSoloStatistic>().First();
            statistic.SendArenaStatisticsHandler += setNeedUpdateStatistics;

            map.CheatEndMatchEvent += EndMatch_cheat;
        }

        public void StartMatch()
        {
            roundStarted = true;

            CoroutineRunner.Instance.Timer(matchTime, EndMatch);
            setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
        }

        protected override void StartBattle()
        {
            if (startBattle)
                return;

            startBattle = true;
            roundStartTime = Time.time + StartFreezeTimeSec;
            setNeedUpdateStatistics(StatisticSendMode.PartialUpdate);
			//ServerRpcListener.Instance.SendRpc(map.ClientRpc.ShowDebugMessageBox, "Включился обратный отсчет. Ожидайте " + StartFreezeTimeSec + " сек");
			CoroutineRunner.Instance.Timer(StartFreezeTimeSec, StartMatch);
            base.StartBattle();
        }

        protected override void PlayerEnterGame(InstancePerson instancePerson, bool changePerson, bool bOnMap, bool reconnect)
        {
			if (!stopBattle)
				statistic.UpdateEntityId(instancePerson);

			base.PlayerEnterGame(instancePerson, changePerson, bOnMap, reconnect);
        }

        protected override void SendArenaInfoToAll(StatisticSendMode mode, bool reconnect = false)
        {
            if (stopBattle)
                foreach (var person in map.persons)
                    statistic.SyncEloRating(person.owner, PlayerTeam.None, true);

			ArenaMatchData amd = GetClientInfo();
			amd.FullInfo = mode == StatisticSendMode.FullUpdate;
			foreach (var person in map.persons)
				ArenaPanelResponse(person, mode, amd, reconnect);
		}

        protected void SendArenaResultInfoToAll()
        {
            statistic.CalcMVP(false);
            foreach (var person in map.persons)
                    statistic.SyncEloRating(person.owner, PlayerTeam.None, true);

            ArenaMatchData amd = GetClientInfo();
            ServerRpcListener.Instance.SendRpc(map.ClientRpc.DMResultDataResponse, amd);
        }


        public override void SendArenaInfoTo(InstancePerson person, bool leave = true)
		{
			var amd = GetClientInfo(leave);
			amd.FullInfo = true;
			ArenaPanelResponse(person.syn, StatisticSendMode.FullUpdate, amd, false);
		}

		private void ArenaPanelResponse(PersonBehaviour person, StatisticSendMode mode, ArenaMatchData amd, bool reconnect)
		{
			if (mode == StatisticSendMode.FullUpdate || reconnect)
				ServerRpcListener.Instance.SendRpc(person.ClientRpc.DMDataResponse, amd);
			else
				ServerRpcListener.Instance.SendRpc(person.ClientRpcUnr.DMDataResponse, amd);

			//ILogger.Instance.Send($"ArenaPanelResponse: person: {person.owner.personName}, mode: {mode} FullInfo: {amd.FullInfo}", ErrorLevel.error);
		}

        public override void Update()
        {
            float now = Time.time;
            if (m_statsSendMode != StatisticSendMode.None && now > m_nextStatsSendTime)
            {
                m_nextStatsSendTime = now + SEND_RATE_STATISTICS;
                SendArenaInfoToAll(m_statsSendMode);
                m_statsSendMode = StatisticSendMode.None;
            }

            if (statistic.GetMaxScore() >= MaxScoreEnd)
                EndMatch();
        }

        protected override void EndMatch()
        {
			if (stopBattle)
				return;

            stopBattle = true;

			foreach (var person in statistic.GetAllRegisterMapTargetObject())
			{
				if (person.IsDead)
					person.ResurrectNow();
			}


            foreach (var person in statistic.GetAllRegisterPersonBehaviours())
            {
                var personStat = statistic.getPlayerInfo(person);
                AddToRewardList(person.owner, personStat);

                person.owner.OnArenaGameFinish(ArenaMatchType.Deathmatch, personStat.Place <= WinnerPositionBorder);
				personStat.ArenaGameFinish = true;
            }

            teleportTime = Time.time + TimeToTeleport;

            SendArenaRewardAll();
            SendArenaResultInfoToAll();

			matchmakingController.RemoveArenaGroup(map.LevelParam);

            base.EndMatch();
        }

        public override void setNeedUpdateStatistics(StatisticSendMode mode)
		{
			if (mode > m_statsSendMode)
				m_statsSendMode = mode;
		}

        public override StatisticBase GetStatistic()
        {
            return statistic;
        }
        public override string ToString()
        {
            return "DeathmatchSolo";
        }

        private ArenaMatchData GetClientInfo(bool leave = false)
        {
            ArenaMatchData amd = new ArenaMatchData();
            amd.IsActive = !(stopBattle || leave);
            amd.IsRoundStarted = roundStarted && !(stopBattle || leave);
            amd.RoundId = startBattle ? 1 : 0;
            amd.TimeSec = GetTimeSec(leave);

            amd.ArenaType = ArenaMatchType.Deathmatch;
            amd.Players = statistic.GetPlayerList();

            return amd;
        }

        #region Cheats
        private void EndMatch_cheat(int sec)
        {
            if (stopBattle)
                return;

            if (!roundStarted)
            {
                matchTime = sec;
                return;
            }

            float timeToEnd = roundStartTime + matchTime - Time.time;
            if (timeToEnd < sec)
                return;

            roundStartTime = Time.time + sec - matchTime;

            setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
            CoroutineRunner.Instance.Timer(sec, EndMatch);
        }

        public PersonArenaReward CreateMatchReward_Cheat(InstancePerson instancePerson)
        {
            var personStat = statistic.getPlayerInfo(instancePerson);

            return ArenaRewardData.GetReward(instancePerson, personStat, false, GameResultForTeam.Win);
        }

        protected override PersonArenaReward CreateMatchReward(InstancePerson instancePerson, ArenaPlayerInfo playerInfo)
        {
            var isWin = playerInfo.Place <= WinnerPositionBorder;

            return ArenaRewardData.GetReward(instancePerson, playerInfo, true, isWin ? GameResultForTeam.Win : GameResultForTeam.Lose);
        }
        #endregion
    }
}
