﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Networking.ClientDataRPC.Arenas;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Common;
using Newtonsoft.Json;
using UtilsLib;
using UtilsLib.BatiyScript;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Games
{
    public class WavesCoop : MatchBase
    {
        [XmlAttribute]
        public int MaxWavesCount = 10;

		public FormulaX MonstersCount;
		public FormulaX ChampionCount;

		[XmlAttribute]
        public string WaveDurations
		{
            get { return FWaveDurations.Text; }
            set { FWaveDurations = new FormulaX("wave", value); }
        }

        [XmlIgnore]
        public FormulaX FWaveDurations = new FormulaX("wave", "120");

        [XmlAttribute]
        public int DeathFormTimer = 5;

        [XmlAttribute]
        public float SpawnDelay = 4.5f;

		[XmlAttribute]
		public int WaveWaiting = 6;

		private float wavestartTime = -1;

        private WavesCoopStatistic statistic;

        public MonsterXmlGenerators MonsterXmlGenerators;
        
        [XmlIgnore]
        public List<Vector3> GeneratorPositionList;

        public FormulaX StartMonsterLevel;
        public FormulaX WavesCompleteBonus;        
        public FormulaX DeathNumberBonus;
        public FormulaX DeathNumberMult;
        public FormulaX TimeBonus;
        public FormulaX AddToMonsterLevel;

		Dictionary<int, WavesResultMatchData> wavesMatchDatas = new Dictionary<int, WavesResultMatchData>();

		public void LoadGenerators(Map map)
		{
			GeneratorPositionList = new List<Vector3>();
			foreach (var mapobject in map.objects)
				if (mapobject != null && mapobject.obj.name.StartsWith("generator"))
					GeneratorPositionList.Add(mapobject.position);
		}

        public override void Start(Map sceneMap)
        {
			base.Start(sceneMap);

			map.PlayerConnected += (person, reconnect, changePerson) => PlayerEnterGame(person, changePerson, false, reconnect);
            map.VisitLocation += (person) => PlayerEnterGame(person, false, true, false);
            map.MonsterDie += OnMonsterDie;
            map.PersonDie += OnPersonDie;
            map.GetStartPointAction += GetStartPointAction;
			
			map.monsters.OnAddAction += m => AddEntity(m.Id, m.personType);
			map.persons.OnAddAction += p => AddEntity(p.Id, p.personType);

            map.bCanDropItemsInLoot = false;
            gameInfoData = InstanceGameManager.Instance.ArenaAlgorithm.GetGameInfoData(ArenaMatchType.Waves, map.LevelName);

            LoadGenerators(sceneMap);

            statistic = map.Scenarios.OfType<WavesCoopStatistic>().First();
            statistic.SendArenaStatisticsHandler += setNeedUpdateStatistics;
        }

        private HashSet<int> deadPersons = new HashSet<int>();

        private int getNewRating(InstancePerson person)
        {
            int baseRating, step;
            InstanceGameManager.Instance.ArenaAlgorithm.getBaseRatingForStars(person.rating_elo_coop, out baseRating, out step);
            int waveCompleteBonus = WavesCompleteBonus.IntCalc(waveId);
            var personStats = statistic.getPlayerInfo(person);
            int deathNumber = 0;
            if (!stopBattle && personStats != null)
                deathNumber = personStats.deathNumber;
            int deathNumberBonus = DeathNumberBonus.IntCalc(deathNumber);
            float gameTime = Time.time - matchStartTime;
            bool bWin = stopBattle && (waveId == MaxWavesCount);
            double timebonus = TimeBonus.Calc(bWin ? 1 : 0, gameTime);
            double deathNumberMult = DeathNumberMult.Calc(deathNumber);
            double personPart = statistic.getPersonEffectPart(person);

            int res = Math.Max(0, baseRating + (int)(step * (waveCompleteBonus + deathNumberBonus + timebonus) / 100.0f * deathNumberMult * personPart));
            return res;
        }

        // для отображения персонажей в форме сверху
        private void ReplaceEntityId(int id)
        {
            for (int i = 0; i < Entities.Count; i++)
                if (Entities[i].Id == id)
                    Entities[i].Id = -1;
        }

        private void OnPersonDie(InstancePerson person, MapTargetObject attacker)
        {
            if (stopBattle)
                return;

            if (deadPersons.Contains(person.personId))
            {
                ILogger.Instance.Send("WavesCoop: Dublicate OnPersonDie(...)", ErrorLevel.error);
                return;
            }
            deadPersons.Add(person.personId);

            statistic.OnPersonDie(person);
            ReplaceEntityId(person.syn.Id);

            PersonMatchEnd(person, false);
			var matchData = SetFinalArenaInfo(person, true); // костыль что бы у нас при смерти сразу кэшировались результаты а то бывает что за DeathFormTimer остальные переходят на новую волну

			CoroutineRunner.Instance.Timer(DeathFormTimer, () =>
			{
				person.RemoveFromMap();
				SendArenaInfoTo(person, false);
				SendWavesResultResponse(person, matchData);
			});

			matchmakingController.ArenaGameEndFor(person);
            //#warning тут же сформировать награду и рейтинги поправить и кинуть их на рейтинговый сервер
            //            SendArenaRewardResponse(person);
            //            ServerRpcListener.Instance.SendRpc(person.ClientRpc.WavesResultResponse, wrd);
        }

        void SendWavesResultResponse(InstancePerson person, WavesResultMatchData matchData)
        {
            if (person == null)
                return;

            ServerRpcListener.Instance.SendRpc(person.ClientRpc.WavesResultDataResponse, matchData);
        }

        int usedIndex = 0;
        public Vector3 GetStartPointAction(PlayerTeam team)
        {
            var startpoint = map.GetStartPointNameAction(team);
            return map.GetRandomPoint(startpoint, usedIndex++);
        }

        private bool ExistMonstersInWaves()
        {
            foreach (var mb in map.monsters)
            {
                if (getMonsterWaveId(mb, -1) >= 0)
                    return true;
            }
            return false;
        }

        private int getMonsterWaveId(MonsterBehaviour mb, int defaultValue = 0)
        {
            return int.TryParse(mb.ScenarioStrings.Get("waveid"), out var id) ? id : defaultValue;
        }

        private void OnMonsterDie(MonsterBehaviour target, MapTargetObject attacker)
        {
            if (stopBattle)
                return;

            
            if (string.IsNullOrEmpty(target.ScenarioStrings.Get("waveid")) && target.characterType != CharacterType.Person)
            {
                return;
            }

            statistic.OnMonsterDie(target, attacker, waveId);

            if (target.characterType != CharacterType.Person)
            {
                if (ExistMonstersInWaves())
                {
                    setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
                    return;
                }
                
                if(isMonsterSpawning)
                    return;

                roundStarted = false;
                if (waveId >= MaxWavesCount)
                {
                    EndMatch();
                }
                else
                {
                    setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
                }
            }
            else
            {
                //что-то в статистику отправить...

                ReplaceEntityId(target.Id);

                setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
            }
        }


        protected override void PlayerEnterGame(InstancePerson instancePerson, bool changePerson, bool bOnMap, bool reconnect)
        {
			//if (!stopBattle)
			//	statistic.UpdateEntityId(instancePerson);

			base.PlayerEnterGame(instancePerson, changePerson, bOnMap, reconnect);
        }

        protected override void StartBattle()
        {
            if (startBattle)
                return;

            startBattle = true;

            matchStartTime = Time.time;
            
            setNeedUpdateStatistics(StatisticSendMode.PartialUpdate);
            StartMatch();
            base.StartBattle();
        }

        public void StartMatch()
        {
            startMonsterLevel = StartMonsterLevel.IntCalc(instanceData.AverageLevel);
            setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
			WaveIdForClientStatistic = 1;//первую волну нужно стартануть досрочно только тогда появится таймер. TODO_maximpr нам нужен более понятный контроль над показом таймера
			StartNextWave(StartFreezeTimeSec, 1);
        }


		public override bool IsStopBattleFor(InstancePerson person)
		{
			return stopBattle || person.syn.IsDead;
		}

		private WavesResultMatchData GetClientInfoFinal(InstancePerson person, bool leave = false)
		{
			try
			{
				WavesResultMatchData amd = new WavesResultMatchData();
				if (person != null)
				{
					amd.IsActive = !(IsStopBattleFor(person) || leave);
					amd.IsAlive = !person.syn.IsDead;
				}
				amd.IsRoundStarted = roundStarted;
				amd.RoundId = WaveIdForClientStatistic;

				if (wavestartTime == -1)
				{
					amd.TimeSec = 0;
				}
				else
				{
					amd.TimeSec = stopBattle ?
						(teleportTime - Time.time)
					: (startBattle ?
						(roundStarted ?
							(nextTimeMark - Time.time)
							: (float)Math.Round(wavestartTime - Time.time)
						)
						: (float)Math.Ceiling(setupEndTime - Time.time));
					if (leave)
						amd.TimeSec = TimeToTeleport;
				}

				//amd.CompletedWaveId = WaveIdForClientStatistic;
				amd.MaxWaves = MaxWavesCount;
				amd.NeedKillPreviousWave = false;// haveActiveFailTimer || (waveId == MaxWavesCount && roundStarted);

				ILogger.Instance.Send($"GetClientInfoFinal RoundId {amd.RoundId} IsActive {amd.IsActive}");

				return amd;
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send($"WavesCoop GetClientInfoFinal - instancePerson: {person?.personId} person.syn: {person?.syn?.NickName}, roundStarted: {roundStarted}, waveId: {waveId} leave: {leave}",
					$"ex: {ex.ToString()}, stacktrace: {ex.StackTrace}", ErrorLevel.exception);
				throw;
			}
		}

		private WavesMatchData GetClientInfo(InstancePerson person, bool leave = false)
        {
            try
            {
                WavesMatchData amd = new WavesMatchData();
                if (person != null)
                {
                    amd.IsActive = !(IsStopBattleFor(person) || leave);
                }
                amd.IsRoundStarted = roundStarted;
				amd.RoundId = WaveIdForClientStatistic;

                if (wavestartTime == -1)
                {
                    amd.TimeSec = 0;
                }
                else
                {
                    amd.TimeSec = stopBattle ?
                        (teleportTime - Time.time)
                    : (startBattle ?
                        (roundStarted ?
                            (nextTimeMark - Time.time)
                            : (float)Math.Round(wavestartTime - Time.time)
                        )
                        : (float)Math.Ceiling(setupEndTime - Time.time));
                    if (leave)
                        amd.TimeSec = TimeToTeleport;
                }

                ILogger.Instance.Send($"GetClientInfo RoundId {amd.RoundId} IsActive {amd.IsActive}");

                return amd;
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"WavesCoop GetClientInfo - instancePerson: {person?.personId} person.syn: {person?.syn?.NickName}, roundStarted: {roundStarted}, waveId: {waveId} leave: {leave}", 
                    $"ex: {ex.ToString()}, stacktrace: {ex.StackTrace}", ErrorLevel.exception);
                throw;
            }
        }

        private int GetMonsterLeft()
        {
            int count = 0;
            for (int i = 0; i < map.monsters.Count; i++)
            {
                MonsterBehaviour mb = map.monsters[i];
                if (!mb.IsLiveObject)
                    continue;
                if (!mb.IsDead && mb.characterType != CharacterType.Person
                    && !string.IsNullOrEmpty(mb.ScenarioStrings.Get("waveid")))
                    count++;
            }
            return count;
        }

        protected override void SendArenaInfoToAll(StatisticSendMode mode, bool reconnect = false)
        {
            WavesMatchData wcmd = GetClientInfo(null);
            wcmd.MonstersLeft = GetMonsterLeft();

			var players = new List<WavesPlayerInfo>();
			foreach (var info in statistic.GetPlayerList())
			{
				players.Add(new WavesPlayerInfo(info));
			}
            wcmd.Players = players;

            foreach (var syn in map.persons)
            {
                InstancePerson p = syn.owner;
                if (p != null && !p.syn.IsDead)
                {
                    wcmd.IsActive = !IsStopBattleFor(p);
                    ServerRpcListener.Instance.SendRpc(p.ClientRpc.WavesDataResponse, wcmd);
                }
            }
        }

        protected void SendArenaResultInfoToAll()
        {
            statistic.CalcMVP(false);
            WavesResultMatchData wcmd = GetClientInfoFinal(null);
            wcmd.MonstersLeft = GetMonsterLeft();
            wcmd.LastWaveCompleted = stopBattle && waveId >= MaxWavesCount;
			wcmd.Players = statistic.GetPlayerListResult().OfType<WavesResultPlayerInfo>().ToList();

			foreach (var syn in map.persons)
            {
                InstancePerson p = syn.owner;
                if (p != null)
                {
                    wcmd.IsActive = !IsStopBattleFor(p);
                    wcmd.IsAlive = !p.syn.IsDead;
                    ServerRpcListener.Instance.SendRpc(p.ClientRpc.WavesResultDataResponse, wcmd);
				}
            }
        }

		private int waveId = 0;
        float nextTimeMark = 0;
        private bool isMonsterSpawning = false;

        private void startWaves()
        {
            if (stopBattle)
                return;

			OnWaveNextWaveStart();
			roundStarted = true;
            isMonsterSpawning = true;
            
            if (GeneratorPositionList.Count < MonsterXmlGenerators.ActiveGenerators)
            {
                ILogger.Instance.Send("GeneratoList Count < ActiveGenerators", ErrorLevel.exception);
                return;
            }

			GeneratorPositionList.RandomSwap(); // перемешиваем список
            var getActivePositions = GeneratorPositionList.Take(MonsterXmlGenerators.ActiveGenerators).ToList(); // получаем ActiveGenerators Count рандомных точек

			int maxMonstersCount = MonstersCount.IntCalc();
			int maxChampionCount = ChampionCount.IntCalc(waveId);

            int addToMonsterLevel = AddToMonsterLevel.IntCalc(waveId);
            int monsterLevel = Math.Min(100, startMonsterLevel + addToMonsterLevel);
            
            for (var g = 0; g < getActivePositions.Count; g++)
            {
                var position = getActivePositions[g]; // для замыкания
				MonsterXmlGenerators.getMonsters(waveId, out MonsterGroupData simpleMonster, out MonsterGroupData champion);

                var localMaxMonstersCount = maxMonstersCount;
                if (simpleMonster.Count > 0)
                {
                    localMaxMonstersCount = simpleMonster.Count;
                }

                if (maxChampionCount > 0)
                {
                    CoroutineRunner.Instance.Timer((g) * SpawnDelay, () => GenerateMonster(champion, position, waveId, monsterLevel));
                    maxChampionCount--;
                }

                for (int i = 0; i < localMaxMonstersCount; i++)
                {
                    var monstersNumber = i;
                    var generatorNumber = g;

                    CoroutineRunner.Instance.Timer((monstersNumber + generatorNumber) * SpawnDelay, 
						() => GenerateMonster(simpleMonster, position, waveId, monsterLevel, monstersNumber, generatorNumber, localMaxMonstersCount, getActivePositions.Count));
                }
            }

            int waveDurations = 0;
			if (waveId <= MaxWavesCount)
			{
				waveDurations = FWaveDurations.IntCalc(waveId);
			}

			nextTimeMark = Time.time + waveDurations;
            roundStarted = true;
            setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
        }

        public override void Update()
        {
            var count = map.monsters.Where(x => x.IsLiveObject && !x.IsDead && x.TeamColor == PlayerTeam.Team2).Count();
            if (!stopBattle && roundStarted && (Time.time > nextTimeMark || count == 0))
            {
                ILogger.Instance.Send($"Wave Update waveId = {waveId} Time.time: {Time.time} nextTimeMark = {nextTimeMark} count = {count}");
                InitNextwave();
            }
        }

		private int WaveIdForClientStatistic;

		internal void OnWaveNextWaveStart()
		{
			WaveIdForClientStatistic = waveId;
			statistic.OnWaveNextWaveStart(waveId);
		}
        private void InitNextwave()
        {
            ILogger.Instance.Send($"Wave NextWave inited");
			if (waveId < MaxWavesCount)
            {
                
                nextTimeMark = Time.time + WaveWaiting;
                StartNextWave(WaveWaiting, waveId + 1);
            }
            else
            {

                stopBattle = true;
                EndMatch();
            }
        }


        private void StartNextWave(float timeout, int nextWave)
        {
            if (stopBattle)
                return;

            //если у нас запущена сейчас 8-ая волна например.. то с номерами меньше 9 не обрабатываем запросы..
            if (waveId + 1 > nextWave)
                return;

            roundStarted = false;
			waveId = nextWave;
			wavestartTime = Time.time + timeout;
            if (timeout > 0)
            {
                setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
                CoroutineRunner.Instance.Timer(timeout, () => { startWaves(); });
            }
            else
            {
                startWaves();
            }
        }

        private DateTime spawnTime; 
        public void GenerateMonster(MonsterGroupData generateMonsterData, Vector3 monsterPos, int wave, int level, int monstersNumber = -1, int generatorNumber = -1, int maxMonstersCount = -1, int generatorMaxCount = -1)
        {
            var monsterData = InstanceGameManager.Instance.gameXmlData.MonsterXmlDatas.GetMonsterData(generateMonsterData.Name);

            CreateMonsterData createMonsterData = new CreateMonsterData()
            {
                monsterData = monsterData,
                monsterName = generateMonsterData.Name,
                lootName = string.Empty,
                level = level,
                startPoint = null,
                replaceTechnicalName = null
            };

            var monster = map.CreateMonsterWithoutPosition(createMonsterData);
            monster.ScenarioStrings.Add("waveid", wave.ToString());
            
            monster.ResurrectTimeSec = -1;
            monster.PridePosition = monster.position = map.UltraCorrectYPosition(monsterPos);

            monster.AggroRadius = 1000;
            monster.VisibleRadius = 1000;
            if (monster.aggroList != null)
                monster.aggroList.Aggression = AggressionType.Aggressive;

            monster.TeamColor = PlayerTeam.Team2;

            foreach(var person in map.persons)
            {
                monster.AddToAgroList(person, FRRandom.Next(1, 10));
            }
            for (int i = 0; i < currentBots.Count; i++)
            {
                monster.AddToAgroList(currentBots[i], FRRandom.Next(1, 10));
            }

			ServerRpcListener.Instance.SendCreateEntity(monster);

			if (TimeProvider.UTCNow > spawnTime)
			{
				spawnTime = TimeProvider.UTCNow.AddSeconds(SpawnDelay);
				setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
			}

			monster.CreateMobGenerator2(Helper.GetGeneratorFor());

            if (monstersNumber >= 0 && generatorNumber >= 0 && monstersNumber == maxMonstersCount - 1 && generatorNumber == generatorMaxCount - 1)
            {
                isMonsterSpawning = false;
            }
        }

        List<WavesEntityData> Entities = new List<WavesEntityData>();
        private void AddEntity(int entityId, PersonType pType)
        {
			if (pType == PersonType.None)
				return;

            Entities.Add(new WavesEntityData() { Id = entityId, Type = pType});
        }

        int startMonsterLevel = 1;

        protected override void EndMatch()
        {
            bool bWin = waveId == MaxWavesCount;

            for (int i = 0; i < map.monsters.Count; i++)
            {
                var monster = map.monsters[i];
                if (!monster.IsLiveObject)
                    continue;
                if (!monster.IsDead)
                    monster.GetStatContainer().AddInfluence(InfluenceXmlDataList.Instance.GetInfluence(monster, InfluenceHelper.StunWithoutIcon, 3600, 1));
            }

            foreach (var person in map.persons)
            {
                var p = person.owner;
                if (p == null || p.syn.IsDead)
                    continue;
                PersonMatchEnd(p, bWin);
				SendArenaInfoTo(p, true);
            }

            SendArenaResultInfoToAll();

			matchmakingController.RemoveArenaGroup(map.LevelParam);

            base.EndMatch();
        }

        public void PersonMatchEnd(InstancePerson person, bool bWin)
        {
            int newRating = getNewRating(person);

            person.rating_elo_coop = newRating;

            var personStat = statistic.getPlayerInfo(person);
            AddToRewardList(person, personStat);
            GiveRewardAndSend(person);

            person.OnArenaGameFinish(ArenaMatchType.Waves, bWin);
			if (personStat != null) //TODO_Deg что-то тесты падают
				personStat.ArenaGameFinish = true;
		}

		protected override PersonArenaReward CreateMatchReward(InstancePerson instancePerson, ArenaPlayerInfo playerInfo)
        {
			bool bWin = stopBattle && (waveId == MaxWavesCount);
			return ArenaRewardData.GetReward(instancePerson, playerInfo, true, bWin ? GameResultForTeam.Win : GameResultForTeam.Lose);
		}

		public override string ToString()
        {
            return "WavesCoop";
        }

        public override void setNeedUpdateStatistics(StatisticSendMode mode)
        {
            SendArenaInfoToAll(mode);
        }

		public override StatisticBase GetStatistic()
        {
            return statistic;
        }

		public WavesResultMatchData GetFinalWavesMatchData(InstancePerson person)
		{
			wavesMatchDatas.TryGetValue(person.personId, out WavesResultMatchData wcmd);
			return wcmd;
		}

		public override void SendArenaInfoTo(InstancePerson person, bool leave = false)
		{
			WavesResultMatchData wcmdResult = GetFinalWavesMatchData(person);
			if (wcmdResult != null)
				SendWavesResultResponse(person, wcmdResult);
			else
				ServerRpcListener.Instance.SendRpc(person.ClientRpc.WavesDataResponse, GetWavesMatchData(person, leave));
		}

		void AddFinalWaveMatchDatas(InstancePerson instancePerson, WavesResultMatchData wavesMatchData)
		{
			if (!wavesMatchDatas.ContainsKey(instancePerson.personId))
				wavesMatchDatas.Add(instancePerson.personId, wavesMatchData);
			else
				ILogger.Instance.Send($"AddWaveMatchDatas wavesMatchData not exists in wavesMatchDatas personId: {instancePerson.personId}", ErrorLevel.error);
		}

		private WavesResultMatchData SetFinalArenaInfo(InstancePerson person, bool leave)
		{
			var wcmd = GetClientInfoFinal(person, leave);
			wcmd.MonstersLeft = GetMonsterLeft();
			wcmd.LastWaveCompleted = stopBattle && waveId >= MaxWavesCount;
			wcmd.Players = statistic.GetPlayerListResult().OfType<WavesResultPlayerInfo>().ToList();

			AddFinalWaveMatchDatas(person, wcmd);
			return wcmd;
		}

		WavesMatchData GetWavesMatchData(InstancePerson person, bool leave)
		{
			WavesMatchData wcmd = GetClientInfo(person, leave);
			wcmd.MonstersLeft = GetMonsterLeft();
			wcmd.Players = statistic.GetPlayerListLight().OfType<WavesPlayerInfo>().ToList();
			//wcmd.NeedKillPreviousWave = wcmd.MonstersLeft > 0; //считали остаток мобов пока не требуется

			return wcmd;
		}

		public int GetAnalyticWiningScore()
        {
            return waveId;
        }

		#region Cheats
		internal void winWaves_Cheat(InstancePerson person, int newWaveId, int killall)
		{
			if (newWaveId < 1)
				return;

			waveId = newWaveId;

			if (killall == 1)
			{
				map.monsters.ForEach((behaviour =>
				{
					if (behaviour.Data.CharacterType == CharacterType.Mob)
					{
						behaviour.SetHealth(person.syn, 0);
					}
				}));
			}
			endWaveByTime_Cheat();
		}

		internal void endWaveByTime_Cheat(int pause = 3)
		{
			nextTimeMark = Time.time + pause;
			setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
		}

		#endregion
	}
}
