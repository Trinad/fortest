﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.ActiveObjects;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.JsonBlockClasses;
using Assets.Scripts.Utils.Logger;
using Fragoria.Common.Utils;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Games
{
    public class CTF : MatchBase
    {
        [XmlAttribute]
        public bool NotifyEnable = true;

        [XmlAttribute]
        public int PointsLimit = 3;

        private Interaction_BattlegroundRespawnBase red_resp;
        private Interaction_BattlegroundRespawnBase blue_resp;
        private CTFStatistic statistic;
        private StatisticSendMode statsSendMode = StatisticSendMode.None;

        private const float SEND_RATE_STATISTICS = 1.0f;
        private int roundId;
        private float nextStatsSendTime;

        public enum CTFAnouncementType
        {
            FlagCaptured,
            FlagDropped,
            FlagReturned,
            FlagSuccessed,
        }

		[XmlInject]
		public void Construct()
		{
			Cheats.RegisterCheat("redflag", (p) =>
				{
					CTF ctf = p.syn.map.Scenarios.OfType<CTF>().FirstOrDefault();
					ctf.CreateFlag(p.syn.position, true);
				}, "Падает красный флаг рядом с тобой. Механика для CTF");
			Cheats.RegisterCheat("blueflag", (p) =>
				{
					CTF ctf = p.syn.map.Scenarios.OfType<CTF>().FirstOrDefault();
					ctf.CreateFlag(p.syn.position, false);
				}, "Падает синий флаг рядом с тобой. Механика для CTF");
		}

        public override void Start(Map sceneMap)
        {
			base.Start(sceneMap);

            map.PlayerConnected += (person, reconnect, changePerson) 
                => PlayerEnterGame(person, changePerson, false, reconnect); //TODO удалить changePerson

            map.VisitLocation += (person) => PlayerEnterGame(person, false, true, false);
            map.MonsterDie += TryDropFlag;
            map.LeftLocation += (target) => TryDropFlag(target.syn, null);
            map.PersonDie += OnMapPersonDie;
            map.bCanDropItemsInLoot = false;
            gameInfoData = InstanceGameManager.Instance.ArenaAlgorithm.GetGameInfoData(ArenaMatchType.CaptureFlag, map.LevelName);
            matchTime = gameInfoData.GameTimeSec;

            statistic = map.Scenarios.OfType<CTFStatistic>().First();
            statistic.SendArenaStatisticsHandler += setNeedUpdateStatistics;

            map.FlagCaptured += OnFlagCaptured;
            map.PersonJump += target => TryDropFlag(target, null);
            map.FlagReturnedToBase += OnFlagReturnedToBase;
            map.GetFlagFromMap += OnGetFlagFromMap;

            map.CheatEndMatchEvent += EndMatch_cheat;

            red_resp = map.GetActiveMapObjectById<Interaction_BattlegroundRespawnBase>(amo => amo.IsRed);
            blue_resp = map.GetActiveMapObjectById<Interaction_BattlegroundRespawnBase>(amo => !amo.IsRed);

            if (red_resp == null)
                throw new NullReferenceException($"'{map.LevelName}' red resp not found");

            if (blue_resp == null)
                throw new NullReferenceException($"'{map.LevelName}' blue resp not found");

            red_resp.ResetFlag();
            blue_resp.ResetFlag();
        }

        #region Events
        private void OnMapPersonDie(InstancePerson target, MapTargetObject attaker)
        {
            TryDropFlag(target, attaker);
        }

        protected override void PlayerEnterGame(InstancePerson instancePerson, bool changePerson, bool bOnMap, bool reconnect)
        {
            if (!stopBattle)
                statistic.UpdateEntityId(instancePerson);

            base.PlayerEnterGame(instancePerson, changePerson, bOnMap, reconnect);
        }

        private void OnFlagCaptured(MapTargetObject targetObject, bool IsRed)
        {
            if (IsRed)
            {
                targetObject.GetStatContainer().RemoveInfluence(Stats.BlueFlag);
                blue_resp.ResetFlag();
            }
            else
            {
                targetObject.GetStatContainer().RemoveInfluence(Stats.RedFlag);
                red_resp.ResetFlag();
            }

            MessageAboutFlagAction(CTFAnouncementType.FlagSuccessed, IsRed);

            statistic.OnFlagCaptured(targetObject);
        }

        private void OnFlagReturnedToBase(Interaction_Flag flag)
        {
            if (flag.IsRed)
                red_resp.ResetFlag();
            else
                blue_resp.ResetFlag();

            MessageAboutFlagAction(CTFAnouncementType.FlagReturned, flag.IsRed);
        }

        private void OnGetFlagFromMap(MapTargetObject p, Interaction_Flag flag)
        {
            MessageAboutFlagAction(CTFAnouncementType.FlagCaptured, flag.IsRed);

            if (flag.IsRed)
                red_resp.FlagIsStolen(p);
            else
                blue_resp.FlagIsStolen(p);
        }
        #endregion

        protected override void StartBattle()
        {
            if (startBattle)
                return;

            startBattle = true;

            roundStartTime = Time.time + StartFreezeTimeSec;
            roundId = 1;
            setNeedUpdateStatistics(StatisticSendMode.PartialUpdate);
            CoroutineRunner.Instance.Timer(StartFreezeTimeSec, StartRound);
			base.StartBattle();
        }

        public void StartRound()
        {
            if (stopBattle)
                return;

            map.RemoveAllAuras();
            roundStarted = true;

            int stoppedRoundId = roundId;
            CoroutineRunner.Instance.Timer(matchTime, () => { EndRound(stoppedRoundId); });
            setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
        }

        public override void Update()
        {
            float now = Time.time;
            if (statsSendMode != StatisticSendMode.None && now > nextStatsSendTime)
            {
                nextStatsSendTime = now + SEND_RATE_STATISTICS;
                SendArenaInfoToAll(statsSendMode);
                statsSendMode = StatisticSendMode.None;
            }
        }

        public void EndRound(int stoppedRoundId)
        {
            if (stopBattle)
                return;

            if (!roundStarted)
                return;

            if (roundId != stoppedRoundId)
                return;

            CtfChangeState();

            roundStarted = false;
            statistic.EndMatch();
            int maxPoints = statistic.GetMaxTeamPoints();

            map.RemoveAllAuras();

            if (maxPoints < PointsLimit)
            {
                //пауза между матчами. воскршение всех. перенос в точки старта. заморозка.                    
                roundId++;
                roundStartTime = Time.time + 10;
                CoroutineRunner.Instance.Timer(10, StartRound);
                setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
            }
            else
            {
                EndMatch();
            }
        }

        protected override void EndMatch()
        {
			if (stopBattle)
				return;

            roundStarted = false;
            stopBattle = true;

			foreach (var person in statistic.GetAllRegisterMapTargetObject())
			{
				if (person.IsDead)
					person.ResurrectNow();
			}

			statistic.AddWinScore();

            foreach (var person in statistic.GetAllRegisterPersonBehaviours())
            {
                var owner = person.owner;

                var personStat = statistic.getPlayerInfo(person);
                AddToRewardList(owner, personStat);

                GameResultForTeam gameResult = statistic.GetGameResultForTeam(person.TeamColor);
                owner.OnArenaGameFinish(ArenaMatchType.CaptureFlag, gameResult == GameResultForTeam.Win);
				personStat.ArenaGameFinish = true;
            }

            teleportTime = Time.time + TimeToTeleport;
            SendArenaRewardAll();
            SendArenaResultInfoToAll();

			matchmakingController.RemoveArenaGroup(map.LevelParam);

            base.EndMatch();
        }

        protected override void SendArenaInfoToAll(StatisticSendMode mode, bool reconnect = false)
        {
            if (stopBattle)
            {
                foreach (var person in map.persons)
                    statistic.SyncEloRating(person.owner, person.TeamColor, true);
			}

            TeamDeathmatchData amd = GetClientInfo();
            statistic.FillArenaInfo(amd, stopBattle);
            foreach (var person in map.persons)
                CTFDataResponse(person, mode, amd, reconnect);
		}

        protected void SendArenaResultInfoToAll()
        {
            statistic.CalcMVP(true);
            foreach (var person in map.persons)
                statistic.SyncEloRating(person.owner, person.TeamColor, true);

            TeamDeathmatchData amd = GetClientInfo();
            statistic.FillArenaInfo(amd, stopBattle);
            ServerRpcListener.Instance.SendRpc(map.ClientRpc.CTFResultDataResponse, amd);
        }

        public override void SendArenaInfoTo(InstancePerson person, bool leave = true)
		{
			var amd = GetClientInfo(leave);
			statistic.FillArenaInfo(amd, true);
			CTFDataResponse(person.syn, StatisticSendMode.FullUpdate, amd, false);
		}

        private void CTFDataResponse(PersonBehaviour person, StatisticSendMode mode, TeamDeathmatchData amd, bool reconnect)
        {
            if (mode == StatisticSendMode.FullUpdate || reconnect)
                ServerRpcListener.Instance.SendRpc(person.ClientRpc.CTFDataResponse, amd);
            else
                ServerRpcListener.Instance.SendRpc(person.ClientRpcUnr.CTFDataResponse, amd);

            //ILogger.Instance.Send($"CTFDataResponse: person: {person.owner.personName}, mode: {mode} FullInfo: {amd.FullInfo}", ErrorLevel.error);
        }

        public override string ToString()
        {
            return "CTF";
        }

        public override void setNeedUpdateStatistics(StatisticSendMode mode)
        {
            if (mode > statsSendMode)
                statsSendMode = mode;
        }

        public override StatisticBase GetStatistic()
        {
            return statistic;
        }

        private void CtfChangeState()
        {
            foreach (var p in map.persons)
            {
                if (p.owner.StatContainer.GetStatValue(Stats.RedFlag) > 0)
                {
                    p.owner.StatContainer.RemoveInfluence(Stats.RedFlag);
                    break;
                }

                if (p.owner.StatContainer.GetStatValue(Stats.BlueFlag) > 0)
                {
                    p.owner.StatContainer.RemoveInfluence(Stats.BlueFlag);
                    break;
                }
            }

            red_resp.ResetFlag();
            blue_resp.ResetFlag();
        }

        private void CreateFlag(Vector3 position, bool needRed)
        {
            var objInfo = new MapObjectInfo
            {
                name = "ctf_flag",
                id = MapTargetObject.objId++,
                ActiveData = needRed ? "Flag red" : "Flag blue",
            };

            var mapObject = map.CreateMapObject(objInfo, position);
            var flagamo = map.GetActiveMapObjectById<Interaction_Flag>(mapObject.Id);
            flagamo.OnBase = false;
        }

        private void TryDropFlag(InstancePerson target, MapTargetObject attacker)
        {
            TryDropFlag(target.syn, attacker);
        }

        private void TryDropFlag(MapTargetObject target, MapTargetObject attacker)
        {
            var statContainer = target.GetStatContainer();
            bool redFlag = statContainer.GetStatValue(Stats.RedFlag) > 0;
            bool blueFlag = statContainer.GetStatValue(Stats.BlueFlag) > 0;
            if (!redFlag && !blueFlag)
                return;
            
            CreateFlag(target.position, redFlag);
            statContainer.RemoveInfluence(redFlag? Stats.RedFlag: Stats.BlueFlag);
            MessageAboutFlagAction(CTFAnouncementType.FlagDropped, redFlag);
        }

        private TeamDeathmatchData GetClientInfo(bool leave = false)
        {
            var amd = new TeamDeathmatchData
            {
                IsActive = !(stopBattle || leave),
                IsRoundStarted = roundStarted,
                RoundId = startBattle ? roundId : 0,
                TimeSec = GetTimeSec(leave),
            };

            amd.Players = statistic.GetPlayerList();
            amd.GameResult = statistic.GetGameResult();

            return amd;
        }

        internal void MessageAboutFlagAction(CTFAnouncementType ctfFlagAction, bool isRed)
        {
            int notifyId = 0;
            var notifyName = string.Empty;
            List<string> rpcParams = new List<string>();
            rpcParams.Add(((int)ctfFlagAction).ToString());
            rpcParams.Add(isRed.ToString()); //VictimMale

            ServerRpcListener.Instance.SendRpc(map.ClientRpc.ShowNotificationResponse, NotifyState.CTFAnnounce, notifyId, notifyName, rpcParams);
        }

        #region Cheats
        private void EndMatch_cheat(int sec = 10)
        {
            if (stopBattle)
                return;

            if (!roundStarted)
            {
                matchTime = sec;
                return;
            }

            float timeToEnd = roundStartTime + matchTime - Time.time;
            if (timeToEnd < sec)
                return;

            int maxPoints = statistic.GetMaxTeamPoints();
            while (maxPoints < PointsLimit)
            {
                statistic.EndMatch();
                maxPoints = statistic.GetMaxTeamPoints();
            }

            roundStartTime = Time.time + sec - matchTime;
            int stoppedRoundId = roundId;
            setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
            CoroutineRunner.Instance.Timer(sec, () => { EndRound(stoppedRoundId); });
        }

        protected override PersonArenaReward CreateMatchReward(InstancePerson instancePerson, ArenaPlayerInfo playerInfo)
        {
            PlayerTeam team = instancePerson.syn.TeamColor;
            GameResultForTeam gameResult = statistic.GetGameResultForTeam(team);

            return ArenaRewardData.GetReward(instancePerson, playerInfo, true, gameResult);
        }
        #endregion
    }

   
}
