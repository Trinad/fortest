﻿using System.Numerics;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Games
{
	public interface ITeamDistributor
	{
		PlayerTeam GetNextTeamId(MapTargetObject target);

		string GetStartPointByTeam(PlayerTeam team);

		Vector3 GetAreaPos(string name, int id = 0);
	}
}
