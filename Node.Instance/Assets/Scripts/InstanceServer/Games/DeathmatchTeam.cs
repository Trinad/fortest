﻿using System;
using System.Linq;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Logger;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Games
{
    public class DeathmatchTeam : MatchBase
    {
        [XmlAttribute]
        public int WinnerPositionBorder = 3;

        [XmlAttribute]
        public bool NotifyEnable = true;

        [XmlAttribute]
        public int PointsLimit = 3;

        private DeathmatchTeamStatistic statistic;
        private StatisticSendMode m_statsSendMode;

        private const float SEND_RATE_STATISTICS = 1.0f;
        private float m_nextStatsSendTime;
        private int roundId;

        public override void Start(Map sceneMap)
        {
			base.Start(sceneMap);

            map = sceneMap;
            map.PlayerConnected += (person, reconnect, changePerson) => PlayerEnterGame(person, changePerson, false, reconnect);
            map.VisitLocation += (person) => PlayerEnterGame(person, false, true, false);
            map.bCanDropItemsInLoot = false;
            gameInfoData = InstanceGameManager.Instance.ArenaAlgorithm.GetGameInfoData(ArenaMatchType.TeamDeathmatch, map.LevelName);
            matchTime = gameInfoData.GameTimeSec;

            statistic = map.Scenarios.OfType<DeathmatchTeamStatistic>().First();
            statistic.SendArenaStatisticsHandler += setNeedUpdateStatistics;

            map.CheatEndMatchEvent += EndMatch_cheat;
        }

        protected override void PlayerEnterGame(InstancePerson instancePerson, bool changePerson, bool bOnMap, bool reconnect)
        {
			if (!stopBattle)
				statistic.UpdateEntityId(instancePerson);

			base.PlayerEnterGame(instancePerson, changePerson, bOnMap, reconnect);
        }

        protected override void StartBattle()
        {
            if (startBattle)
                return;

            startBattle = true;

            roundStartTime = Time.time + StartFreezeTimeSec;
            roundId = 1;
            setNeedUpdateStatistics(StatisticSendMode.PartialUpdate);
            CoroutineRunner.Instance.Timer(StartFreezeTimeSec, StartRound);
            base.StartBattle();
        }

        protected override void SendArenaInfoToAll(StatisticSendMode mode, bool reconnect = false)
        {
            if (stopBattle)
                foreach (var person in map.persons)
                    statistic.SyncEloRating(person.owner, person.TeamColor, true);

			TeamDeathmatchData amd = GetClientInfo();
			statistic.FillArenaInfo(amd, stopBattle);
			foreach (var person in map.persons)
				TeamDeathmatchDataResponse(person, mode, amd, reconnect);
		}

        protected void SendArenaResultInfoToAll()
        {
            statistic.CalcMVP(true);
            foreach (var person in map.persons)
               statistic.SyncEloRating(person.owner, person.TeamColor, true);

            TeamDeathmatchData amd = GetClientInfo();
            statistic.FillArenaInfo(amd, true);
            ServerRpcListener.Instance.SendRpc(map.ClientRpc.TDMResultDataResponse, amd);
        }

        public override void SendArenaInfoTo(InstancePerson person, bool leave = true)
		{
			var amd = GetClientInfo(leave);
			statistic.FillArenaInfo(amd, true);
			TeamDeathmatchDataResponse(person.syn, StatisticSendMode.FullUpdate, amd, false);
		}

		private void TeamDeathmatchDataResponse(PersonBehaviour person, StatisticSendMode mode, TeamDeathmatchData amd, bool reconnect)
        {
            if (mode == StatisticSendMode.FullUpdate || reconnect)
                ServerRpcListener.Instance.SendRpc(person.ClientRpc.TDMDataResponse, amd);
            else
                ServerRpcListener.Instance.SendRpc(person.ClientRpcUnr.TDMDataResponse, amd);

            //ILogger.Instance.Send($"TeamDeathmatchDataResponse: person: {person.owner.personName}, mode: {mode} FullInfo: {amd.FullInfo}", ErrorLevel.error);
        }

        public void StartRound()
        {
            if (stopBattle)
                return;

            map.RemoveAllAuras();
            roundStarted = true;
            int stoppedRoundId = roundId;
            CoroutineRunner.Instance.Timer(matchTime, () => { EndRound(stoppedRoundId); });

			setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
		}

        public override void Update()
        {
            float now = Time.time;
            if (m_statsSendMode != StatisticSendMode.None && now > m_nextStatsSendTime)
            {
                m_nextStatsSendTime = now + SEND_RATE_STATISTICS;
                SendArenaInfoToAll(m_statsSendMode);
                m_statsSendMode = StatisticSendMode.None;
            }
        }

        public void EndRound(int stoppedRoundId)
        {
            if (stopBattle)
                return;

            if (!roundStarted)
                return;

            if (roundId != stoppedRoundId)
                return;

            roundStarted = false;
            statistic.EndRound();
            int maxPoints = statistic.GetMaxTeamPoints();

            map.RemoveAllAuras();

            if (maxPoints < PointsLimit)
            {
                //пауза между матчами. воскршение всех. перенос в точки старта.                    
                roundId++;
                roundStartTime = Time.time + 10;
                CoroutineRunner.Instance.Timer(10, StartRound);
                setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
            }
            else
            {
                EndMatch();
            }
        }

        protected override void EndMatch()
        {
			if (stopBattle)
				return;

            roundStarted = false;
            stopBattle = true;

			foreach (var person in statistic.GetAllRegisterMapTargetObject())
			{
				if (person.IsDead)
					person.ResurrectNow();
			}

			statistic.AddWinScore();

            foreach (var person in statistic.GetAllRegisterPersonBehaviours())
            {
                var personStat = statistic.getPlayerInfo(person);
                AddToRewardList(person.owner, personStat);

				GameResultForTeam gameResult = statistic.GetGameResultForTeam(person.TeamColor);
				person.owner.OnArenaGameFinish(ArenaMatchType.TeamDeathmatch, gameResult == GameResultForTeam.Win);
				personStat.ArenaGameFinish = true;
            }

            teleportTime = Time.time + TimeToTeleport;
            
            SendArenaRewardAll();
            SendArenaResultInfoToAll();

			matchmakingController.RemoveArenaGroup(map.LevelParam);

            base.EndMatch();
        }

        public override string ToString()
        {
            return "DeathmatchTeam";
        }

        public override StatisticBase GetStatistic()
        {
            return statistic;
        }

        public override void setNeedUpdateStatistics(StatisticSendMode mode)
		{
			if (mode > m_statsSendMode)
				m_statsSendMode = mode;
		}

        private TeamDeathmatchData GetClientInfo(bool leave = false)
        {
            var amd = new TeamDeathmatchData
            {
                IsActive = !(stopBattle || leave),
                IsRoundStarted = roundStarted,
                RoundId = startBattle ? roundId : 0,
                TimeSec = GetTimeSec(leave),
            };

            amd.Players = statistic.GetPlayerList();
            amd.GameResult = statistic.GetGameResult();

            return amd;
        }

        #region Cheats
        private void EndMatch_cheat(int sec = 10)
        {
            if (stopBattle)
                return;

            if (!roundStarted)
            {
                matchTime = sec;
                return;
            }

            float timeToEnd = roundStartTime + matchTime - Time.time;
            if (timeToEnd < sec)
                return;

            int maxPoints = statistic.GetMaxTeamPoints();
            while (maxPoints < PointsLimit)
            {
                statistic.EndRound();
                maxPoints = statistic.GetMaxTeamPoints();
            }

            roundStartTime = Time.time + sec - matchTime;
            int stoppedRoundId = roundId;
            setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
            CoroutineRunner.Instance.Timer(sec, () => { EndRound(stoppedRoundId); });
        }

        protected override PersonArenaReward CreateMatchReward(InstancePerson instancePerson, ArenaPlayerInfo playerInfo)
        {
            PlayerTeam team = instancePerson.syn.TeamColor;
            GameResultForTeam gameResult = statistic.GetGameResultForTeam(team);

            return ArenaRewardData.GetReward(instancePerson, playerInfo, true, gameResult);
        }
        #endregion
    }
}
