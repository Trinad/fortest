﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Analytic;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Networking.ClientDataRPC.Arenas;
using Assets.Scripts.Utils;
using Fragoria.Common.Utils;
using UtilsLib.DegSerializers;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using static UtilsLib.XmlData.ArenaAlgorithmData;

namespace Assets.Scripts.InstanceServer.Games
{
    public abstract class MatchBase : Scenario
    {
        [XmlAttribute]
        public float StartFreezeTimeSec = 15;
        [XmlAttribute]
        public float TimeToTeleport = 120;
        [XmlAttribute]
        public float WaitPersonsTime = 30;
        [XmlIgnore]
        protected List<MonsterBehaviour> currentBots = new List<MonsterBehaviour>();

        //заполняется из xml
        public BotGenerator Bots;
        public ArenaReward ArenaRewardData;

        protected MatchBaseData instanceData;
        protected float setupEndTime = -1;
        protected float matchStartTime = -1;
        protected float teleportTime = -1;
        protected float roundStartTime = -1;
        protected float matchTime = 300;
        protected int averageHeroPower = 0;
        protected bool startBattle;

		protected bool m_roundStarted;
		protected bool roundStarted
		{
			get => m_roundStarted;
			set => map.SetVariableBool("roundStarted", m_roundStarted = value);
		}

		private bool m_stopBattle;
		protected bool stopBattle
		{
			get => m_stopBattle;
			set => map.SetVariableBool("stopBattle", m_stopBattle = value);
		}

        public abstract StatisticBase GetStatistic();
        public abstract void setNeedUpdateStatistics(StatisticSendMode mode);
        protected abstract void SendArenaInfoToAll(StatisticSendMode mode, bool reconnect = false);
        public abstract void SendArenaInfoTo(InstancePerson person, bool leave = true);
        public GamesInfoData gameInfoData { get; protected set; }
                
        public event Action<GamesInfoData> OnBattleEnded;
        public event Action<GamesInfoData> OnBattlePlayerLeave;

	    public DateTime CreateTime = TimeProvider.UTCNow;

		[XmlIgnore]
		public MatchmakingController matchmakingController;

		[XmlInject]
		public void Construct(MatchmakingController _matchmakingController)
		{
			matchmakingController = _matchmakingController;
		}

        public override void Start(Map sceneMap)
        {
            map = sceneMap;
            map.SetVariableBool("IsArena", true);
            map.SetVariableBool("endMatch", false);
            ArenaRewardData = sceneMap.Scenarios.OfType<ArenaReward>().FirstOrDefault();
            map.SyncPerson += OnSyncPerson;
			map.Stopping += EndMatch;

			StartFreezeTimeSec++; //Чтоб на клиенте правильно отображалось

            var instData = map.GetVariableString("InstData");
            instanceData = JSON.JsonDecode(instData).JsonDecode<MatchBaseData>();
            if (instanceData == null)
            {
                ILogger.Instance.Send($"InstData was incorrect instanceData is null . InstData={instData} Map.GameId = {map.GameId}, GameType = {map.GetArenaMatchType()}", ErrorLevel.error);
                instanceData = new MatchBaseData();
            }
        }

        protected virtual void StartBattle()
        {
        }

        protected virtual void EndMatch()
        {
            map.SetVariableBool("endMatch", true);

            var statistic = GetStatistic();
            List<ArenaPlayerInfo> players = statistic.GetPlayerList();

            map.OnEndMatch(players, statistic);

            try
            {
                OnBattleEnded?.Invoke(gameInfoData);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"MatchBase OnBattleEnded ex = {ex}", ErrorLevel.error);
            }
        }

        protected virtual void PlayerEnterGame(InstancePerson instancePerson, bool changePerson, bool bOnMap, bool reconnect)
		{
			var statistic = GetStatistic();
			if (PlayerOnMap(instancePerson, changePerson, bOnMap, reconnect, statistic))
				return;

			if (bOnMap)
			{
				//если еще не делали этого - запускаем таймер на запуск игры
				if (setupEndTime < 0)
				{
					setupEndTime = Time.time + WaitPersonsTime;
					CoroutineRunner.Instance.Timer(WaitPersonsTime, StartCountdown);

                    var botRating = InstanceGameManager.Instance.ArenaAlgorithm.GetRankMinValue(instanceData.AverageRating);
					currentBots = Bots.CreateBots(map, instanceData.AverageLevel, instanceData.PersonsSubscribedCount, gameInfoData.BotCount, botRating, instanceData.PlayerTeams);
                    foreach (var monster in currentBots)
						statistic.AddArenaPlayerInfo(monster, 0);

					GlobalEvents.Instance.OnArenaBotCreate(instancePerson);
				}

				//если все уже собрались
				if (map.persons.Count(x => x.bOnMap) >= instanceData.PersonsSubscribedCount)
					StartBattle();

				foreach (var person in map.persons.Where(x => x.bOnMap && x.owner.loginId != instancePerson.loginId))
				{
					instancePerson.login.AddLastGame(person.owner.loginId, CreateTime);
					person.owner.login.AddLastGame(instancePerson.loginId, CreateTime);
				}
			}

			SendArenaInfoToAll(stopBattle ? StatisticSendMode.PartialUpdate : StatisticSendMode.FullUpdate, reconnect);
		}

        public void LeaveArena(InstancePerson person)
        {
            if (stopBattle)
                return;

			person.RemoveFromMap();

			var statistic = GetStatistic();
            person.OnArenaGameFinish(statistic.GetArenaMatchType(), false, true);

            OnSyncPerson(person, SyncReason.Disconnect);

			var personStat = statistic.getPlayerInfo(person);
			if (personStat == null)
			{
				ILogger.Instance.Send($"MatchBase.LeaveArena personStat == null personId={person.personId}", ErrorLevel.error); //TODO_Deg PIX-9700
			}
			else
			{
				personStat.Disconnected = true;
				personStat.ArenaGameFinish = true;
			}

            try
            {
                OnBattlePlayerLeave?.Invoke(gameInfoData);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"MatchBase OnBattlePlayerLeave ex = {ex}", ErrorLevel.error);
            }

			person.login.ticket = string.Empty;
			matchmakingController.ArenaGameEndFor(person);

            SendArenaInfoTo(person);
        }

        #region Rewards

        public class PersonArenaReward
        {
            public ArenaRewardChestType ChestType;
            public List<ArenaRewardData> rewardList;
            public bool IsTaken;
            public PersonExperienceData personExperienceData;
        }

        private readonly Dictionary<int, PersonArenaReward> rewardDatas = new Dictionary<int, PersonArenaReward>();

        protected void GiveRewardAndSend(InstancePerson instancePerson)
        {
            PersonArenaReward rewardData = GetPersonArenaReward(instancePerson); // забираем
            var expDat = rewardData.personExperienceData;
            ServerRpcListener.Instance.SendRpc(instancePerson.ClientRpc.ArenaExperieneResponse, expDat);
            ServerRpcListener.Instance.SendGetRewardResponse(instancePerson, rewardData); // отсылаем
            TakeArenaReward(instancePerson, rewardData); // выдаем
            //TODO_maximpr мне кажется этого события тут не должно быть. Оно уже есть в классе MatchesPlayedScenario, а тут дублирование
            AnalyticsManager.Instance.MatchePlayed(instancePerson,instancePerson.login.WinCounterCollection.GetTotalGameCount());
        }

        protected void SendArenaRewardAll()
        {
            var statistic = GetStatistic();
            foreach (var person in statistic.GetAllRegisterPersonBehaviours())
                GiveRewardAndSend(person.owner);
        }

        protected PersonArenaReward GetPersonArenaReward(InstancePerson instancePerson)
        {
            rewardDatas.TryGetValue(instancePerson.personId, out var rewardData);
            if (rewardData == null)
            {
                ILogger.Instance.Send($"GetPersonArenaReward rewardData == null loginId:{instancePerson.loginId} personId: {instancePerson.personId}", ErrorLevel.error);
                return new PersonArenaReward
                {
                    ChestType = ArenaRewardChestType.Wood,
                    rewardList = new List<ArenaRewardData>(),
                };
            }

            return rewardData;
        }

        protected void TakeArenaReward(InstancePerson instancePerson, PersonArenaReward rewardData)
        {
            if (rewardData == null)
            {
                ILogger.Instance.Send($"TakeArenaReward rewardData == null loginId = {instancePerson.loginId}", ErrorLevel.error);
                return;
            }

            if (rewardData.IsTaken)
            {
                ILogger.Instance.Send($"TakeArenaReward rewardData is taken loginId = {instancePerson.loginId}", ErrorLevel.warning);
                return;
            }

            var analyticInfo = new AnalyticCurrencyInfo()
            {
                needSendInWallet = true,
                sendGain = true,
                USDCost = 0f,
                Source = SourceGain.MatchReward,
                PersonLevel = instancePerson.Level,
				TestPurchase = instancePerson.login.settings.testPurchase
			};

            rewardData.IsTaken = true;
            foreach (var reward in rewardData.rewardList)
            {
                analyticInfo.Amount = reward.ItemData.Count;
                analyticInfo.Currency = reward.ItemData.Resource;
                analyticInfo.SourceType = gameInfoData.matchType.ToString();
                // начислить награду
                instancePerson.AddItem(reward.ItemData, Source: ItemSourceType.AnyChest, analyticInfo: analyticInfo);
            }
        }

		protected void AddToRewardList(InstancePerson instancePerson, ArenaPlayerInfo playerInfo)
		{
			if (rewardDatas.ContainsKey(instancePerson.personId))
				return;

			var reward = CreateMatchReward(instancePerson, playerInfo);
			//ILogger.Instance.Send($"AddToRewardList playerInfo: {JsonConvert.SerializeObject(playerInfo, Formatting.Indented)} " +
			//	$"reward: {JsonConvert.SerializeObject(reward, Formatting.Indented)}", ErrorLevel.error);
			rewardDatas.Add(instancePerson.personId, reward);
		}

        protected abstract PersonArenaReward CreateMatchReward(InstancePerson instancePerson, ArenaPlayerInfo playerInfo);
		
		#endregion

		//почему это не в статистике?
		protected void OnSyncPerson(InstancePerson person, SyncReason reason)
        {
            var statistic = GetStatistic();
            statistic.SyncEloRating(person, person.syn.TeamColor, stopBattle);
        }

		public virtual bool IsStopBattleFor(InstancePerson person)
		{
			return stopBattle;
		}

        public bool PlayerOnMap(InstancePerson instancePerson, bool changePerson, bool bOnMap, bool reconnect, StatisticBase statistic)
        {
            if (changePerson)
            {
				instancePerson.RemoveFromMap();
				return true;
            }

            if (IsStopBattleFor(instancePerson))
            {
                //sendArenaInfoToPerson(instancePerson, StatisticSendMode.PartialUpdate, reconnect);

                SendArenaInfoTo(instancePerson);

                var arenaInfo = statistic.getPlayerInfo(instancePerson);
                AddToRewardList(instancePerson, arenaInfo);// начисляем опоздунам

                GiveRewardAndSend(instancePerson);
                return true;
            }

            if (bOnMap)
            {
                if (instancePerson.syn.IsDead)
					map.OnPlayerDeathInfoResponse(instancePerson, instancePerson.syn);//TODO_maximpr Умереть еще раз, если во время смерти был дисконнект. Странно все это
            }

            return false;
        }

        public void StartCountdown()
        {
            if (startBattle == false && map.persons.Count < gameInfoData.MinCount)
            {
                EndMatchAbnormally();
                return;
            }

            StartBattle();
        }

        public void EndMatchAbnormally()
        {
            stopBattle = true;
            teleportTime = Time.time + 15;
            setNeedUpdateStatistics(StatisticSendMode.FullUpdate);
        }

        public float GetMatchTime()
        {
            return Time.time - matchStartTime;
        }

        public float GetTimeSec(bool leave = false)
        {
            if (leave)
                return TimeToTeleport;
            if (stopBattle)
                return (teleportTime - Time.time);
            if (roundStartTime == -1)
                return 0;
            if (startBattle)
                return roundStarted ? (roundStartTime + matchTime - Time.time) : (float)Math.Round(roundStartTime - Time.time);
            if (setupEndTime == -1)
                return 0;
            return (float)Math.Ceiling(setupEndTime - Time.time);
        }
    }
}
