﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Db;
using ModestTree;
using Node.Instance.Assets.Scripts.Utils;
using NodeUtilsLib.Utils;
using uLink;
using Utils.Events;
using UtilsLib.Interfaces;
using UtilsLib.ExternalLogs;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.InstanceServer
{
	public class InstanceGameManager : BaseGameManager
	{
		public static InstanceGameManager Instance;
        public IExternalLogsManager ExternalLogs = new EmptyExternalLogsManager();
		public ArenaAlgorithm ArenaAlgorithm;

		private readonly InstancePersonFactory instancePersonFactory;
		private AppConfig m_appConfig;

	    private AddinManager addinManager;
	    public GameXmlData gameXmlData;
	    private MapFactory mapFactory;
	    private InstanceSqlAdapter sqlAdapter;

	    public event Action Quit = () => { };

        public enum GameState
		{
			ServerStarting = 0,
			Started = 3,
			Stopping = 4,
			Stopped = 5,
			WaitingLobbyAnswer = 6,
			ErrorStop = 7
		}

        public InstanceGameManager(InstanceSqlAdapter sqlAdapter, MapFactory mapFactory, AddinManager addinManager, 
			ClanSettingsData guildSettings, TutorialAlgorithm tutorialAlgorithm, PersonsOnServer personsOnServer, 
			ShopManager shopManager, IInstanceGameSender instanceGameSender, InstancePersonFactory instancePersonFactory,
			 GameXmlData xmlData, ArenaAlgorithm arenaAlgorithm,
			AppConfig appConfig)
        {
            Instance = this;
            this.sqlAdapter = sqlAdapter;
            m_monoBehaviours = new List<MonoBehaviour>(146);

            this.gameXmlData = xmlData;

            this.addinManager = addinManager;
	        m_personsOnServer = personsOnServer;
			TutorialAlgorithm = tutorialAlgorithm;
		    ShopManager = shopManager;

		    this.instanceGameSender = instanceGameSender;
		    this.instancePersonFactory = instancePersonFactory;
            this.mapFactory = mapFactory;
			m_appConfig = appConfig;
			ArenaAlgorithm = arenaAlgorithm;

            //  _____   ____   ___     ____         ____    ___    _____ 
            // |_   _| | __ | |  _ \  | __ |       |  _ \  | __|  / ____|
            //   | |   ||  || | | | | ||  ||       | | | | ||_   | /  __
            //   | |   ||  || | | | | ||  ||       | | | | | _|  | | |_ \
            //   | |   ||__|| | |_| | ||__||  ___  | |_| | ||__  | |___| |
            //   |_|   |____| |____/  |____| |___| |____/  |___| |______/
            //
            // Сделать чтоб формулы подгружались при изменении файлов

            GuildSettings = guildSettings;

			EffectAlgorithm = new EffectAlgorithm();
			SoundAlgorithm = new SoundAlgorithm();

			MapObject.MapObjectConstants = ServerConstants.Instance.MapObjectConstants.ToDictionary(x => x.name);

            if (m_appConfig != null)
                ExternalLogs = new ExternalLogsManager(m_appConfig.EnableExternalLogs, m_appConfig.ConnectionStringExternalLogs);
        }

		public List<int> blockedLogins = new List<int>();

        //Клиент хочет присоединиться
        public void uLink_OnPlayerApproval(NetworkPlayerApproval approval)
        {
            string ticket;
            if (!approval.loginData.TryRead(out ticket))
            {
                ILogger.Instance.Send("Client connection attempt was denied, no ticket", ErrorLevel.warning);
                approval.Deny(NetworkConnectionError.IncorrectParameters);
                return;
            }

            int loginId;
            if (!approval.loginData.TryRead(out loginId))
            {
                ILogger.Instance.Send("Client connection attempt was denied, no loginId", ErrorLevel.warning);
                approval.Deny(NetworkConnectionError.IncorrectParameters);
                return;
            }

            if ((state == GameState.Stopped ||
                 state == GameState.Stopping ||
                 state == GameState.WaitingLobbyAnswer ||
                 state == GameState.ErrorStop))
            {
                ILogger.Instance.Send("Client connection attempt was denied, instance not ready", ErrorLevel.warning);
                approval.Deny(NetworkConnectionError.UserDefined1 + 1);
                return;
            }

            var instancePerson = m_personsOnServer.getPersonByLoginId(loginId);
            if (instancePerson == null)
            {
                ILogger.Instance.Send("instanceGameManager.getPersonByLoginId(loginId) == null loginId: " + loginId, ErrorLevel.error);
                approval.Deny(NetworkConnectionError.InvalidPassword);
                return;
            }

            if (instancePerson.login.ticket != ticket)
            {
                ILogger.Instance.Send($"instanceGameManager instancePerson.ticket = {instancePerson.login.ticket} receive ticket = {ticket} loginId: {loginId}", ErrorLevel.error);
                approval.Deny(NetworkConnectionError.InvalidPassword);
                return;
            }

			var map = GetMapByGameId(instancePerson.GameId);
			if (map == null)
			{
				ILogger.Instance.Send($"instanceGameManager instancePerson.GameId = {instancePerson.GameId} receive ticket = {ticket} loginId: {loginId}", ErrorLevel.error);
				approval.Deny(NetworkConnectionError.InvalidPassword);
				return;
			}

			if (blockedLogins.Contains(loginId))
			{
				ILogger.Instance.Send($"Client connection attempt was denied. login - blocked. LoginId = {loginId}");
				approval.Deny(NetworkConnectionError.InvalidPassword);
				return;
			}

			ILogger.Instance.Send($"Client is approved. loginId = {loginId} gameId = {instancePerson.GameId}");
            approval.localData = loginId;
            approval.Approve();
        }


        #region InstancePersons

        public void AddPerson(InstancePerson instancePerson)
        {
            instancePerson.bFromLobby = true;
            m_personsOnServer.AddReplacePerson(instancePerson);
            ILogger.Instance.Send("groupPersons.Add loginId = " + instancePerson.loginId + " " + instancePerson);

			UpdateMapEmpty(instancePerson.GameId);
        }

        public void UpdatePerson(InstancePerson instancePerson)
        {
            var existingPerson = m_personsOnServer.getPersonByLoginId(instancePerson.loginId);
            if (existingPerson == null)
                return;

            instancePerson.bFromLobby = true;
            m_personsOnServer.AddReplacePerson(instancePerson);

			UpdateMapEmpty(instancePerson.GameId);
        }

        #endregion

		#region Сохранение персов

        // сохранить весь инстанс
        private void syncPersonsAndMap(bool bLogout, SyncReason reason)
		{
		    foreach (var person in m_personsOnServer.GetAllPersons())
		        SyncPersons(person, bLogout, reason);

			foreach (var map in serverMaps)
				SyncMap(map, bLogout);
		}

		public void SyncPersons(InstancePerson currentPerson, bool bLogout, SyncReason reason, Action onSave = null)
		{
			//Текущего перса сохраняем всегда
			SyncPerson(currentPerson, bLogout, reason, onSave);

			//В лобби надо всех остальных персов сохранить
			if (Map.Edge.Type != LocationType.Camp)
				return;

			//В этих случаях сохраняем только онлайновых
			if (reason == SyncReason.StopInstance || reason == SyncReason.Timer)
				if (!currentPerson.bOnLine)
					return;

			foreach (var person in currentPerson.login.InstancePersons)
				if (person != currentPerson)// && person.bNeedSave) TODO fil сделать выборочное сохранение расставить везде bNeedSave
				{
					if (person.NextDateSave == DateTime.MinValue)
						person.NextDateSave = TimeProvider.UTCNow.AddSeconds(SyncTime);

					if (!bLogout && !(person.NextDateSave < TimeProvider.UTCNow))
						continue;

					person.syn?.map?.OnSyncData(person, reason);

					person.SaveToDB();

					person.bNeedSave = false;
					person.NextDateSave = TimeProvider.UTCNow.AddSeconds(SyncTime);
				}
		}

		private void SyncPerson(InstancePerson instancePerson, bool bLogout, SyncReason reason, Action onSave = null)
		{
			//В этих случаях сохраняем только онлайновых
			if (reason == SyncReason.StopInstance || reason == SyncReason.Timer)
				if (!instancePerson.bOnLine)
					return;

			if (instancePerson.NextDateSave == DateTime.MinValue)
				instancePerson.NextDateSave = TimeProvider.UTCNow.AddSeconds(SyncTime);

			//При логауте и новом персе всегда сохраняем
			if (!bLogout && reason != SyncReason.NewPerson)
				if(!instancePerson.bNeedSave || instancePerson.NextDateSave >= TimeProvider.UTCNow)
					return;

			if (bLogout && instancePerson.bRunTutorial && instancePerson.TutorialStateFinish)
				instancePerson.NextTutorialState();

			instancePerson.syn?.map?.OnSyncData(instancePerson, reason);

			//Сохраняем персонажа и логин
			instancePerson.SaveToDB(() => instancePerson.SaveLoginToDB(onSave));

            instancePerson.bNeedSave = false;
			instancePerson.NextDateSave = TimeProvider.UTCNow.AddSeconds(SyncTime);
		}

        public void SyncMap(Map map, bool bStop)
		{
			if (!map.NeedSaveMap)
				return;

			if (map.NextDateSave == DateTime.MinValue)
				map.NextDateSave = TimeProvider.UTCNow.AddSeconds(SyncTime);

			//При остановке всегда сохраняем
			if (!bStop && map.NextDateSave >= TimeProvider.UTCNow)
				return;

			map.NextDateSave = TimeProvider.UTCNow.AddSeconds(SyncTime);
		}

		#endregion

		private int StoppingTimeoutMs = 5000;
		private int StoppedTimeoutMs = 1000;
		private int StartingTimeoutSec = 60;
		private float SyncTimeCheck = 1;
		private float SyncTime = 900;

		float lastSyncTime;

		public List<Map> serverMaps = new List<Map>();

		public Map GetMapByGameId(int gameId)
		{
			return serverMaps.FirstOrDefault(x => x.GameId == gameId);
		}

	    public ShopManager ShopManager;
	    
        private IInstanceGameSender instanceGameSender;

        public ClanSettingsData GuildSettings;

		public EffectAlgorithm EffectAlgorithm { get; private set; }
		public SoundAlgorithm SoundAlgorithm { get; private set; }
		public TutorialAlgorithm TutorialAlgorithm { get; private set; }

		private readonly PersonsOnServer m_personsOnServer;

        public void MonoBehavioursUpdate()
        {
            for (int i = 0; i < m_monoBehaviours.Count; i++)
            {
                try
                {
                    m_monoBehaviours[i].Update();
                }
                catch (Exception ex)
                {
                    ILogger.Instance.Send(ex.ToString(), ErrorLevel.exception);
                }
            }
        }

        public override void Update()
		{
			if (state == GameState.Started)
			{
                MonoBehavioursUpdate();

                foreach (var map in serverMaps)
			    {
			        try
			        {
                        if (map.bStop)
                            continue;

                        if (!map.bStoping && map.dtGameEmpty < TimeProvider.UTCNow)
                        {
                            //Отправить запрос диспетчеру на остановку
                            SendStopPersonalMap(map);
                            continue;
                        }

                        map.MyUpdate();
						map.ScenariosUpdate();
                        //ILogger.Instance.Send(" map.MyUpdate(); name: " + map.LevelName);
			        }
			        catch (Exception ex)
			        {
			            ILogger.Instance.Send(ex.ToString(), ErrorLevel.exception);
			            ILogger.Instance.Send(new StackTrace().ToString(), ErrorLevel.exception);
                    }
			    }

				TutorialAlgorithm.Update();

				//Сохранить и выгрузить персов
				foreach (var instancePerson in m_personsOnServer.GetAllPersons())
				{
					foreach (var person in instancePerson.login.InstancePersons)
						person.MyUpdate();

					if (instancePerson.bOnLine || instancePerson.syn == null || !instancePerson.SaveAndExitTime.HasValue || instancePerson.SaveAndExitTime.Value >= TimeProvider.UTCNow)
						continue;

					instancePerson.SaveAndExitTime = null;

                    SyncPersons(instancePerson, true, SyncReason.Logout);

					instancePerson.RemoveFromMap();
					instancePerson.login.RemovePlayer();
                }

				if (Time.time > lastSyncTime + SyncTimeCheck)
				{
					syncPersonsAndMap(false, SyncReason.Timer);
					lastSyncTime = Time.time;
				}

				if (dtGameEmpty < TimeProvider.UTCNow)
				{
					//вот тут надо остановить игру...
					ILogger.Instance.Send("StoppingGame");
					StoppingGame();
				}
			}
			else if (state == GameState.Stopping)
			{
				if (dtGameStopping.AddMilliseconds(StoppingTimeoutMs) < TimeProvider.UTCNow)
				{
					//подождали некоторое время. можно остановить
					ILogger.Instance.Send("StopGame");
					StopGame();
				}
			}
			else if (state == GameState.Stopped)
			{
				if (dtGameStopped.AddMilliseconds(StoppedTimeoutMs) < TimeProvider.UTCNow)
				{
					//подождем некоторое время. и можно сообщать что все остановили.
					ILogger.Instance.Send("Notify lobby about stop");
				    instanceGameSender.NotifyLobbyAboutInstanceClear();

					state = GameState.ErrorStop;
					dtGameQuit = TimeProvider.UTCNow.AddSeconds(5);
				}
			}
			//обработать как-то другие состояния... 
			else if (state == GameState.ServerStarting)
			{
				if (Time.time > StartingTimeoutSec) // не стартанули
				{
					state = GameState.ErrorStop;
					dtGameQuit = TimeProvider.UTCNow.AddSeconds(5);
				    instanceGameSender.NotifyLobbyAboutInstanceClear();
					ILogger.Instance.Send("state == GameState.ServerStarting too long. quit after 5 seconds");
				}
			}
			else if (state == GameState.ErrorStop)
			{
				if (dtGameQuit != DateTime.MinValue && TimeProvider.UTCNow > dtGameQuit)
				{
					ILogger.Instance.Send("Allpication.Quit() by state = ErrorStop ticket = " + ServerStartGameManager.ticket, ErrorLevel.debug);
                    Quit();
				}
			}
		}

        private void SendStopPersonalMap(Map map)
        {
            ILogger.Instance.Send("SendStopPersonalMap name: " + map.LevelName);
            map.bStoping = true;

            instanceGameSender.SendStopPersonalMap(map.GameId);
        }

        public void StopMap(int gameId, bool stop)
        {
            var map = GetMapByGameId(gameId);
            if (map == null)
            {
                ILogger.Instance.Send("StopMap map == null gameId: " + gameId, ErrorLevel.error);
                return;
            }

            if (stop)
            {
                map.Stop();
            }
            else
            {
                map.bStoping = false;
                UpdateMapEmpty(gameId);
            }
            UpdateGameEmpty();
        }

        public void AddPersonToGame(int gameId, InstancePersonData personData, List<GuildData> gdList, bool rewrited)
        {
			//Загрузка логина

			sqlAdapter.LoadLogin(personData.loginId,
                (login, personIdSlots) =>
                {
	                login.ticket = personData.ticket;
                    login.rewrited = rewrited;
                    continueAddToGame(gameId, personData, login, personIdSlots);
				},
				(loginSavedId, error, exception) =>
				{
					ILogger.Instance.Send("error = " + error + " exception = " + exception, ErrorLevel.exception);
				}
			);

			if (gdList != null)
			{
				for (int i = 0; i < gdList.Count; i++)
				{
					Guild.SetGuild(gdList[i]);
					ILogger.Instance.Send("AddPersonToGame guild = " + gdList[i].ClanName + " GuildId = " + gdList[i].ClanId);
				}
			}
		}

		private void continueAddToGame(int gameId, InstancePersonData personData, LobbyLogin login,
			Dictionary<int, int> personIdSlots)
		{
			if (personData.personId > 0)
				login.lastPersonId = personData.personId;

            LoadPersonsFromDB(gameId, personData, login, personIdSlots);
        }

        void LoadPersonsFromDB(int gameId, InstancePersonData personData, LobbyLogin login,
            Dictionary<int, int> personIdSlots)
        {
            var lst = personIdSlots.Select(x => x.Key).ToList();
            sqlAdapter.LoadPersons(lst,
                persons =>
                {
                    if (persons.Count == 0)
                    {
                        persons.Add(
                            new InstancePersonSqlData
                            {
                                Type = PersonType.None,
                                PersonId = personData.personId,
                                bRunTutorial = login.tutorRun,
                                TutorialState = 1
                            }
                        );
                        personIdSlots[personData.personId] = -1;
                    }

                    foreach (var person in persons)
                    {
                        person.SlotId = personIdSlots[person.PersonId];
                        var createPersonData = new CreateInstancePersonData
                        {
                            login = login,
                            instancePersonSqlData = person,
							GuildId = personData.GuildId,
							GuildRole = personData.GuildRole,
                            gameId = gameId,
						};

                        var newInstancePerson = instancePersonFactory.Create(createPersonData);
                        login.AddPerson(newInstancePerson);
                    }

                    login.InitPersons();

					InitLogin(login);

                    var instancePerson = login.Person;
                    ILogger.Instance.Send("AddPersonToGame loginid = " + personData.loginId + " GuildId = " + instancePerson.GuildId + " name = " + instancePerson.personName + " personId = " + instancePerson.personId);
                    AddPerson(instancePerson);
                    instanceGameSender.AddPersonToGameConfirm(gameId, personData.loginId, login.loginName);
                },
                (result, ex) => ILogger.Instance.Send("error = " + result + " exception = " + ex, ErrorLevel.exception)
            );
        }

        public Map InitMap(int gameId, string levelParam, string instData)
        {
            if (serverMaps.Exists(x => x.GameId == gameId))//TODO deg
            {
                ILogger.Instance.Send("InitLoginList Duplicate Map levelParam: " + levelParam + " GameId: " + gameId, ErrorLevel.error);
	            return null;
	            //throw new ArgumentException("InitLoginList Duplicate Map levelParam: " + levelParam + " GameId: " + gameId);
            }
            var map = mapFactory.Create(gameId);

            serverMaps.Add(map);

            var edgeMap = gameXmlData.EdgesMapXml.FirstOrDefault(x => x.location == Map.Edge);
            map.SetVariableDouble("LocationId", edgeMap?.LocationId ?? 0);
            map.SetVariableString("InstData", instData);
            //загрузка карты
            map.RequestLevel(addinManager.GetScenariosClons(), ServerStartGameManager.levelName, levelParam);

			GlobalEvents.Instance.OnMapCreate(map);

			UpdateMapEmpty(gameId);
			UpdateGameEmpty();

			instanceGameSender.InitLoginListAnswer(gameId);
            ILogger.Instance.Send($"Map Inited. GameId = {map.GameId}, LevelName = {map.LevelName}", ErrorLevel.info);
            return map;
        }

        public void UpdateLogin(int gameId, InstancePersonData personData, List<GuildData> gdList,bool rewrited)
        {
            var target = m_personsOnServer.getPersonByLoginId(personData.loginId);

			if (target != null)
			{
				target.login.ticket = personData.ticket;
                target.login.rewrited = rewrited;

                if (target.login.InstancePersons.Exists(x => x.personId == personData.personId))
					target.login.lastPersonId = personData.personId;

				if (target.personId == personData.personId)
				{
					UpdatePerson(target);
					return;
				}

				if (target.loginId == personData.loginId)
				{
					var instancePerson = target.login.Person;
					if (instancePerson != null)
					{
						UpdatePerson(instancePerson);
						return;
					}
				}
			}

            //Загрузка логина
            sqlAdapter.LoadLogin(personData.loginId,
				(login, personIdSlots) =>
				{
					login.ticket = personData.ticket;
                    login.rewrited = rewrited;
                    continueUpdate(gameId, personData, login, personIdSlots);
				},
				(loginSavedId, error, exception) =>
				{
					ILogger.Instance.Send("error = " + error + " exception = " + exception, ErrorLevel.exception);
				}
			);

			if (gdList != null)
			{
				for (int i = 0; i < gdList.Count; i++)
				{
					Guild.SetGuild(gdList[i]);
					ILogger.Instance.Send("UpdateLogin guild = " + gdList[i].ClanName + " GuildId = " + gdList[i].ClanId);
				}
			}
        }


        private void continueUpdate(int gameId, InstancePersonData personData, LobbyLogin login,
            Dictionary<int, int> personIdSlots)
        {
            UpdatePersonsForLogin(gameId, personData, login, personIdSlots);
        }

        private void UpdatePersonsForLogin(int gameId, InstancePersonData personData, LobbyLogin login,
			Dictionary<int, int> personIdSlots)
		{
			if (personData.personId > 0)
				login.lastPersonId = personData.personId;

			//var lst = new List<int> { personData.personId };

			//Загрузить всех персонажей //TODO_Deg Пока пусть всегда грузится
			//if (Map.Edge.Type == EdgeLocationType.Camp)
			var lst = personIdSlots.Select(x => x.Key).ToList();

			sqlAdapter.LoadPersons(lst,
				persons =>
				{
					if (persons.Count==0)
					{
						persons.Add(
							new InstancePersonSqlData
                            {
                                Type = PersonType.None,
                                PersonId = personData.personId,
                                bRunTutorial = login.tutorRun,
                                TutorialState = 1
                            }
						);
						personIdSlots[personData.personId] = -1;
					}

					foreach (var person in persons)
					{
						person.SlotId = personIdSlots[person.PersonId];
						var createPersonData = new CreateInstancePersonData
						{
							login = login,
							instancePersonSqlData = person,
							GuildId = personData.GuildId,
							GuildRole = personData.GuildRole,
							gameId = gameId,
						};

						var newInstancePerson = instancePersonFactory.Create(createPersonData);
						login.AddPerson(newInstancePerson);
					}

					login.InitPersons();

					InitLogin(login);

					var instancePerson = login.Person;
					ILogger.Instance.Send("UpdateLogin loginid = " + instancePerson.loginId + " GuildId = " + instancePerson.GuildId + " name = " + instancePerson.personName + " personId = " + instancePerson.personId);
					UpdatePerson(instancePerson);
				},
				(result, ex) => { ILogger.Instance.Send("error = " + result + " exception = " + ex, ErrorLevel.exception); }
			);
		}

		private void InitLogin(LobbyLogin login)
		{
			var oldperson = m_personsOnServer.getPersonByLoginId(login.loginId);
			if (oldperson != null && oldperson.syn != null)
			{
				//SyncPersons(oldperson, true, SyncReason.Logout);

                oldperson.RemoveFromMap();
			}

			login.SetPlayer(oldperson != null ? oldperson.player : NetworkPlayer.unassigned);

			//В момент, когда мы получили данные логина (либо создали новый инстанс LobbyLogin, либо взяли уже загруженный)
			//Мы должны проконтролировать настройки клиента. В данном случае нас интересуют настройки читов.
			//По этой опции клиент определяет можно ли ему показывать пункт о читах в настройках игрока
			login.settings.isCheatsAllowed = RemoteXmlConfig.Instance.IsUserCanApplyCheats(login.loginId);
		}

		public InstancePerson CreateDefaultPerson(InstancePerson p)
		{
			var login = p.login;

            var personSql = new InstancePersonSqlData
            {
                Type = PersonType.None,
                PersonId = 0,
                bRunTutorial = p.login.tutorRun,
                TutorialState = 1,
                SlotId = -1,
            };

			return CreateNewPerson(p.GameId, login, personSql);
		}

		public InstancePerson CreateNewPerson(int gameId, LobbyLogin login, InstancePersonSqlData person)
		{
			var createPersonData = new CreateInstancePersonData
			{
				login = login,
				instancePersonSqlData = person,
				gameId = gameId,
			};

			var instancePerson = instancePersonFactory.Create(createPersonData);
			instancePerson.SlotId = person.SlotId;
            login.AddPerson(instancePerson);

			//Если выбранного перса нету, то выбрать этого
			if (login.Person == null)
				login.SelectPerson(instancePerson.personId);

			InitLogin(login);

			ILogger.Instance.Send("CreateNewPerson loginid = " + instancePerson.loginId + " GuildId = " + instancePerson.GuildId + " name = " + instancePerson.personName + " personId = " + instancePerson.personId);
			UpdatePerson(instancePerson);

			return instancePerson;
		}

        public void StoppingGame()
		{
			syncPersonsAndMap(true, SyncReason.StopInstance);

			state = GameState.Stopping;
			dtGameStopping = TimeProvider.UTCNow;

			//выслать финальную статистику. всех синхронизировать с лобби.
			instanceGameSender.SendStopGameInfo();

            //перса выкинуть
            
            foreach(var p in m_personsOnServer.GetPlayers())
            {
                var instancePerson = m_personsOnServer.getPersonByLoginId((int)p.localData);
                if (instancePerson != null)
                    ServerRpcListener.Instance.SendRpc(instancePerson.ClientRpc.StopServerResponse);
                else
                    ILogger.Instance.Send("StoppingGame instancePerson == null p.localData: " + p.localData, ErrorLevel.error);
            }

			ILogger.Instance.Send("InstanceGameManager groupPersons.Clear()");
			m_personsOnServer.ClearPersons();
		}

		public void StopGame()
		{
			state = GameState.Stopped;
			dtGameStopped = TimeProvider.UTCNow;

			//со всеми порвать соединения. если с кем-то остались еще.            
			foreach (NetworkPlayer p in m_personsOnServer.GetPlayers().ToList())
			{
				ILogger.Instance.Send("Disconnect player " + p + " from game");
				uLink.Network.Instance.CloseConnection(p, true);

				uLink.Network.Instance.RemoveRPCs(p);
				uLink.Network.Instance.DestroyPlayerObjects(p);
				uLink.Network.Instance.RemoveInstantiates(p);
			}

			m_personsOnServer.ClearPlayers();
			pingEvent?.Stop();
			ExternalLogs?.Stop();
		}

        public GameState state = GameState.ServerStarting;
		private DateTime dtGameStarted = DateTime.MinValue;
		private DateTime dtGameEmpty = DateTime.MaxValue;
		private DateTime dtGameStopping = DateTime.MinValue;
		private DateTime dtGameQuit = DateTime.MinValue;
		private DateTime dtGameStopped = DateTime.MinValue;
		private ActionEvent pingEvent;

		public override void Start()
		{
			ILogger.Instance.Send("Server is started...");
			ILogger.Instance.Send("ticket = " + ServerStartGameManager.ticket);
			ILogger.Instance.Send("levelName = " + ServerStartGameManager.levelName);

			dtGameStarted = TimeProvider.UTCNow;
			state = GameState.Started;
			
			UpdateGameEmpty();

			pingEvent = Period.EverySec(m_appConfig.PingTimeSec).Action(instanceGameSender.PingDispatcher);

			addinManager.Load(ServerStartGameManager.levelName);
		}

		internal void OnPlayerConnected(InstancePerson instancePerson, NetworkPlayer networkPlayer)
		{
			instancePerson.login.SetPlayer(networkPlayer);
			instancePerson.SaveAndExitTime = null;

			m_personsOnServer.AddPlayer(networkPlayer, instancePerson);
			instancePerson.Connected();

            //instancePerson.StatContainer.SendAllStats();

			UpdateMapEmpty(instancePerson.GameId);
        }

		public void OnPlayerDisconnected(NetworkPlayer networkPlayer, out bool bCurrentConnect)
		{
			var instancePerson = m_personsOnServer.GetPerson(networkPlayer);
			bCurrentConnect = instancePerson != null && instancePerson.player.id == networkPlayer.id;

			m_personsOnServer.RemovePlayer(networkPlayer);

            if (bCurrentConnect)
			{
				instancePerson.login.SetPlayer(NetworkPlayer.unassigned);
				if (instancePerson.syn != null)
					instancePerson.syn.sender = NetworkPlayer.unassigned;

				if (instancePerson.dtLoadLevel <= TimeProvider.UTCNow)
				{
					instancePerson.LogoutTime = TimeProvider.UTCNow;

					instancePerson.bNeedSave = true;
					SyncPersons(instancePerson, true, SyncReason.Disconnect);//TODO_deg

                    //Если карта пвп, то не убирать его с карты сразу
                    if (Map.Edge.WaitPersonDestroy)
						instancePerson.SaveAndExitTime = instancePerson.LastPing.AddSeconds(ServerConstants.Instance.SaveAndExitTimeSec);
					else
					{
						instancePerson.RemoveFromMap();
						instancePerson.login.RemovePlayer();
					}
                }
				else
				{
                    //Телепортировался, удалим сразу
					instancePerson.RemoveFromMap();
					instancePerson.login.RemovePlayer();
				}

				instancePerson.Disconnected();

				GlobalEvents.Instance.OnPlayerDisconnected(instancePerson);
			}

			if (instancePerson != null)
			{
				UpdateMapEmpty(instancePerson.GameId);
				ILogger.Instance.Send($"OnPlayerDisconnected {networkPlayer} loginId={instancePerson.loginId} name={instancePerson.personName}");
			}
			else
			{
				UpdateMapEmptyAll(); //Хз какая карта, надо все обновить
				ILogger.Instance.Send($"OnPlayerDisconnected {networkPlayer} person=null");
			}
		}

        private void UpdateGameEmpty()
        {
            if (serverMaps.IsEmpty())
                dtGameEmpty = TimeProvider.UTCNow.AddSeconds(m_appConfig.StopEmptyServerAfterTimeSec);
            else
                dtGameEmpty = DateTime.MaxValue;
        }

		public void UpdateMapEmptyAll()
		{
			foreach (var map in serverMaps)
			{
				if (map.dtGameEmpty == DateTime.MaxValue && !m_personsOnServer.HavePlayersByGameId(map.GameId))
				{
					map.dtGameEmpty = TimeProvider.UTCNow.AddSeconds(map.GameClearTimeSec);
					ILogger.Instance.Send($"UpdateMapEmptyAll()... GameId = {map.GameId} dtGameEmpty = {map.dtGameEmpty.ToShortTimeString()}");
				}
			}
		}

		public void UpdateMapEmpty(int gameId)
		{
			var map = GetMapByGameId(gameId);
			if (map == null)
			{
				ILogger.Instance.Send($"UpdateMapEmpty gameId = {gameId} map == null");
				return;
			}

			if (m_personsOnServer.HavePlayersByGameId(gameId))
				map.dtGameEmpty = DateTime.MaxValue;
			else
				map.dtGameEmpty = TimeProvider.UTCNow.AddSeconds(map.GameClearTimeSec);

			ILogger.Instance.Send($"UpdateMapEmpty()... GameId = {map.GameId} dtGameEmpty = {map.dtGameEmpty.ToShortTimeString()}");
		}

        internal bool havePlayer(NetworkPlayer player)
		{
			return m_personsOnServer.HavePlayer(player);
		}

		public EdgeLocationXmlData GetEdgeLocation(string levelName)
		{
			return gameXmlData.Edges.GetEdgeLocationXmlData(levelName)
                ?? new EdgeLocationXmlData { LevelName = levelName };
		}

		// TODO надо сделать продуманное добавление и удаление объектов, потому что в цикле апдейта
		// прямо в момент пробегания по этому листу могут и присунуть и убрать элемент
		private readonly List<MonoBehaviour> m_monoBehaviours;

		public void AddMonoBehaviour(MonoBehaviour monoBehaviour)
		{
			m_monoBehaviours.Add(monoBehaviour);
		}

		public void RemoveMonoBehaviour(MonoBehaviour monoBehaviour)
		{
			m_monoBehaviours.Remove(monoBehaviour);
		}
	}
}
