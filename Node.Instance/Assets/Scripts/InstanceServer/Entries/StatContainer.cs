﻿using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.HitActions;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilsLib.DegSerializers;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.InstanceServer.Entries
{
    public abstract class StatContainer
    {
        public InstancePerson personOwner = null;
        public MonsterBehaviour monsterOwner = null;

        public MapTargetObject GetOwner()
        {
            if (personOwner != null)
                return personOwner.syn;
            if (monsterOwner != null)
                return monsterOwner;

            return null;
        }

        public abstract double GetStatValue(string name);

        public double GetStatValue(Stats stat)
        {
            double res = 0;
            StatAccum.TryGetValue(stat, out res);
            return res;
        }

        public Dictionary<Stats, double> StatAccum = new Dictionary<Stats, double>();

        public static Dictionary<string, Stats> AttributeTypes = new Dictionary<string, Stats>();
        public static Dictionary<Stats, string> AttributeTypes2 = new Dictionary<Stats, string>();

		public bool bInfluencesChanged;

        static StatContainer()
        {
            foreach (Stats attr in Enum.GetValues(typeof(Stats)))
            {
                AttributeTypes.Add(attr.ToString(), attr);
                AttributeTypes2.Add(attr, attr.ToString());
            }
        }

        public void SetStatValue(Stats stat, double value)
        {
            StatAccum[stat] = value;
            //Sos.Debug("AddStat stat " + stat + " summ " + StatAccum[stat]);
            OnAttributeChange(stat);
        }

        public void AddStat(Stats stat, double value)
        {
            if (value == 0)
                return;

            if (!StatAccum.ContainsKey(stat))
                StatAccum[stat] = 0;

            StatAccum[stat] += value;
            //Sos.Error("AddStat stat " + stat + " summ " + StatAccum[stat]);
            OnAttributeChange(stat);
        }

        public event Action<Stats> AttributeChange;

        public virtual void OnAttributeChange(Stats stat)
        {
            if (AttributeChange != null)
            {
                AttributeChange(stat);
            }
        }

        public void RemoveStat(Stats stat, double value)
        {
            if (value == 0)
                return;

            if (!StatAccum.ContainsKey(stat))
                StatAccum[stat] = 0;

            StatAccum[stat] -= value;
            //Sos.Error("RemoveStat stat " + stat + " summ " + StatAccum[stat]);
            OnAttributeChange(stat);
        }

        public StatInfluence GetById(int inflId)
        {
            return Influences.Find(x => x.Id == inflId);
        }

        public List<StatInfluence> GetAllById(int inflId)
        {
            return Influences.FindAll(x => x.Id == inflId);
        }

        public bool ExistInfluenceId(int inflId)
        {
            return Influences.Exists(x => x.Id == inflId);
        }

        public List<StatInfluence> Influences = new List<StatInfluence>();
        public bool AddInfluence(StatInfluence infl)
        {
            var owner = GetOwner();

            if(owner == null)
                return false;

            if (owner.influenceImmunitet)
                return false;

            if (owner.IsDead && infl.DeathRemove)
                return false;

            if (personOwner != null && personOwner.bInSafeZone && infl is PeriodDamageInfluence)
            {
                return false;
            }
            infl.container = this;

            if (infl.Add())
            {
                Influences.Add(infl);
                //TODO_maximpr подумать и воткнуть в другое место
                //впереди должны быть инфлуенсы с максимальным значением. если выпил много бутылок лечения, то вначале должны действовать самые сильные для этого и сортирую
                Influences.Sort((x, y) => (int)(y.value - x.value));
                infl.Apply();
				bInfluencesChanged = true;

                return true;
            }
           
			return false;
        }

		public void RemoveInfluence(Stats stat, Func<StatInfluence, bool> check = null)
		{
		    ILogger.Instance.Send("RemoveInfluence stat=" + stat);
            for (int i = 0; i < Influences.Count; i++)
            {
                var infl = Influences[i];
                if (!infl.ContainsStat(stat))
                    continue;
                                
                if (!infl.bRemove && (check == null || check(infl)))
                {
                    Influences.RemoveAt(i--);
                    infl.Remove();
                    bInfluencesChanged = true;
                    InfluenceXmlDataList.Instance.BackPool(infl);
                }
            }
		}

        public void RemoveInfluence(StatInfluence infl)
        {
            infl.bRemove = true;
			bInfluencesChanged = true;
		}

        public void RemoveInfluence(int Id)
        {
            var infl = GetById(Id);
            if (infl != null)
                RemoveInfluence(infl);
        }

        public void RemoveAllInfluences(SignType removeInfluenceType = SignType.Any)
        {
            var influences = Influences.FindAll(x => removeInfluenceType == SignType.Any ||
            removeInfluenceType == SignType.Positive && x.GetStatValue(Stats.IsNegative) == 0 ||
            removeInfluenceType == SignType.Negative && x.GetStatValue(Stats.IsNegative) > 0);
            if (influences != null)
            {
                for (int i = 0; i < influences.Count; i++)
                {
                    if (influences[i] != null)
                        RemoveInfluence(influences[i]);
                }
            }
        }

        public void RemoveAllInfluences(int Id, int saveCount = 0)
        {
            var influences = GetAllById(Id);
            if (influences != null)
            {
                for (int i = 0; i < influences.Count - saveCount; i++)
                {
                    if (influences[i] != null)
                        RemoveInfluence(influences[i]);
                }
            }    
        }

        public void RemoveInfluences()
        {
            foreach (var infl in Influences)
            {
                infl.bRemove = true;
            }
			bInfluencesChanged = true;
		}

		public void RemoveInfluences(List<Stats> stats, Func<StatInfluence, bool> check = null)
		{
			foreach (var stat in stats)
			{
                RemoveInfluence(stat, check);
			}
		}
        public void RestoreInfluenceEffects()
        {
            var owner = GetOwner();
            foreach (var infl in Influences)
            {
                if (infl.EnableCondition == null || infl.EnableCondition.Check(owner.map, owner, null))
                    infl.SetEnable(true);
            }

			bInfluencesChanged = true;
		}
        public void DeathRemoveInfluences()
        {
            foreach (var infl in Influences)
            {
                if (infl.DeathRemove)
                    infl.bRemove = true;
                else
                    infl.SendEffectRemove();
            }

			bInfluencesChanged = true;
		}

        public bool ExistStatus()
        {
            foreach (var infl in Influences)
            {
                if (infl.bRemove)
                    continue;

                if (infl.Icon != PlayerStatus.None)
                    return true;
            }

            return false;
        }

        public virtual void Update()
        {
            //foreach (var infl in Influences)
            //{
            //    if (!infl.bRemove)
            //    {
            //        infl.Update();
            //    }
            //}

            for (int i = 0; i < Influences.Count; i++)
            {
                var infl = Influences[i];
                if (!infl.bRemove)
                {
                    infl.Update();
                }
            }

            for (int i = 0; i < Influences.Count;)
            {
                var infl = Influences[i];
                if (infl.bRemove)
                {
                    Influences.RemoveAt(i);
                    infl.Remove();
                    InfluenceXmlDataList.Instance.BackPool(infl);
					bInfluencesChanged = true;
				}
                else
                    i++;
            }
		}

        public List<InfluenceSaveData> GetInfluenceDatas()
        {
            List<InfluenceSaveData> res = new List<InfluenceSaveData>();
            foreach (var infl in Influences)
            {
                if (infl.Id != 0 && infl.NeedSave)
                    res.Add(infl.GetSaveData());
            }

            return res;
        }

        public  void SetInfluenceDatas(List<InfluenceSaveData> saveDatas)
        {
            foreach (var inflData in saveDatas)
            {
                var influenceXmlData = InstanceGameManager.Instance.gameXmlData.InfluencesXmlData.Influences.FirstOrDefault(x =>  x.Id == inflData.Id);
                if (influenceXmlData == null)
                    continue;

                var copy = influenceXmlData.Clone();
                copy.Init(this, inflData);
            }
        }
    }
}
