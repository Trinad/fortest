﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils.Common;
using UtilsLib.Logic.Enums;
using PersonData = Assets.Scripts.InstanceServer.Entries.XmlData.PersonData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData;
using Assets.Scripts.Utils.Logger;
using UtilsLib;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.Utils;
using Assets.Scripts.Networking.ClientDataRPC;

using Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using Assets.Scripts.InstanceServer.Entries.Person;
using System.Linq;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries.ActiveObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using LobbyInstanceLib.Networking;
using Node.Instance.Assets.Scripts.Utils;
using UtilsLib.NetworkingData;
using UtilsLib.Logic;
using Assets.Scripts.InstanceServer.Entries.XmlData.AttackDatas;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Newtonsoft.Json;

namespace Assets.Scripts.InstanceServer.Entries.TargetObjects
{
    public class PersonBehaviour : MapTargetObject
    {
        public override CharacterType characterType
        {
            get { return CharacterType.Person; }
        }

        public override FractionType fraction
        {
            get { return owner.personFraction; }
        }

		public uLink.NetworkPlayer sender;
	    public RpcClientEnum ClientRpc => owner.login.ClientRpc;
	    public RpcClientEnum ClientRpcUnr => owner.login.ClientRpcUnr;
	    public RpcClientEnum ClientRpcLight => owner.login.ClientRpcLight;

		public bool bOnLine
        {
            get { return InstanceGameManager.Instance.havePlayer(sender); }
        }

        public float MaxChangeTransformTime = 0;
        
        public bool SendRPC = true;
        public ChangeTransformReliability SendChangeTransformRPCMode;
		public bool bNeedUpdateMiniMapFull;
        
        public int InventoryId;

        public override List<EntityPartInfo> EquippedWears
        {
            get { return owner.EquippedWears; }
            set { throw new NotImplementedException(); }
        }

        //public bool bOnMap { get; set; }
        public bool bReconnect = false;

        private bool _bOnGetMapInfoRequest;

		public bool bOnGetMapInfoRequest
		{
			get { return _bOnGetMapInfoRequest && owner.login.bOnGetPlayerStatusOnServer; }
			set { _bOnGetMapInfoRequest = value; }
		}

        public Dictionary<int, float> OverflowLoot = new Dictionary<int, float>();

		public static int animState_die;

        public float timeMapWisited;
        
        public bool isNeedSendDpsInfo = false;

        public override Vector3 GetActualPosition
        {
            get
            {
                return actualClientPosition;
            }
        }

        public override bool CanPushByPerson
        {
            get { return true; }
        }

        public override bool bInSafeZone { get { return owner.bInSafeZone; } set { owner.bInSafeZone = value; } }

        public void SendExperience(InstancePerson person = null)
        {
            if (person != null && (person.syn == null || person.syn.sender == null))
                return;
            int requimentExperience = owner.gameXmlData.ExpData.GetCurrentRequimentExperience(owner);

            if(person == null)
                SendRpc(map.ClientRpc.SetExperienceResponse, owner.Level, (int)owner.StatContainer.GetStatValue(Stats.Exp), requimentExperience, false, 0);
            else
                SendRpc(person.ClientRpc.SetExperienceResponse, owner.Level, (int)owner.StatContainer.GetStatValue(Stats.Exp), requimentExperience, false, 0);
        }

        static PersonBehaviour()
		{
			animState_die = Animator.StringToHash("Base Layer.die");
		}

        public static float PersonRaduis = 0.4f;

        public PersonBehaviour()
        {
            Id = objId++;
            InventoryId = objId++;

            //MaxHealth = 100;

            bOnMap = false;

            Radius = PersonRaduis;
            HitRadius = Radius;
            Height = 2.1f * 0.18f * 2; //рост перса

            //packs.Add("rocket_pack_lava");
            //packs.Add("staff01");

            view = uLinkHelper.CreateView(this);
        }

        public override string GetAvatar()
        {
            return owner.login.AvatarName;
        }

        public override int Health { get => owner.Health; set => owner.Health = value; }
        public override int Energy { get => owner.Energy; set => owner.Energy = value; }

        public void Init(InstancePerson person, Map map, bool reconnect)
        {
            this.map = map;
            
            sessionId = 0;

            personType = person.personType;
            TargetName = person.PrefabName;
            NickName = person.personName;
			
            //ResurrectAtLocation = person.ResurrectAtLocation;
            sender = person.player;
            owner = person;

			DeathReason = DeathReasonType.Player;

			StatContainer = owner.StatContainer;
            bSilence = owner.StatContainer.GetStatValue(Stats.Silence) > 0;
            bStun = owner.StatContainer.GetStatValue(Stats.Stun) > 0;
            bRoot = owner.StatContainer.GetStatValue(Stats.Root) > 0;
            MaxHealth = (int)owner.StatContainer.GetStatValue("HP");
            MaxEnergy = (int)owner.StatContainer.GetStatValue("SP");
            Stamina = MaxStamina = (int)owner.StatContainer.GetStatValue("Stamina");

            if (!reconnect && TimeProvider.UTCNow > owner.ResurrectTime)
            {
                person.Health = MaxHealth;
                person.Energy = MaxEnergy;
            }

            SendHealth();
            SendEnergy();
            SendStamina();
            SendPeriodAttack();

			owner.potions.SetLockedSize((int)owner.StatContainer.GetStatValue("PotionsCount"));

            foreach (var ability in owner.activeAbility)
                ability.NeedSendStatus = true;

			SpellCastAction?.Reset();

			map.OnInitPerson(person, reconnect);
        }

        public void CheckPlayerDeathInfo()
        {
            if (IsDead)
            {
				var playerDeathData = owner.GetPlayerDeathData();
				if (playerDeathData == null)
				{
					playerDeathData = new PlayerDeathData(owner, null, owner.GetResurrectTime());
					ILogger.Instance.Send($"CheckPlayerDeathInfo playerDeathData == null personId: {owner.personId}", ErrorLevel.error);
				}
				
				ServerRpcListener.Instance.PlayerDeathInfoResponse(this, playerDeathData);
				//ILogger.Instance.Send($"CheckPlayerDeathInfo playerDeathData: {JsonConvert.SerializeObject(playerDeathData, Formatting.Indented)}", ErrorLevel.error);
			}
			else
			{
				SendRpc(ClientRpc.ResurrectResponse, position, direction, sessionId);
			}
        }

        internal void InitPosition()
        {
            position = map.GetStartPoint(owner);
            actualClientPosition = position;
        }

        public byte sessionId;
        private float OneSecCheck = 0;

        public float MinMonsterDistance = float.MaxValue;

        private readonly Dictionary<byte, AbilityErrors> buttonsAvalability = new Dictionary<byte, AbilityErrors>();

        [RPC]
        public void SetSpellDirection(Vector2 direction, byte sessionId)
        {
            if (this.sessionId != sessionId)
                return;
            if (SpellCastAction != null)
            {
                SpellCastAction.SetSpellDirection(direction);
            }
        }

        [RPC]
        void CastBreak(byte sessionId)
        {
            if (this.sessionId != sessionId)
                return;
            if (SpellCastAction != null)
            {
                SpellCastAction.Reset();
            }
        }

        public List<PersonBehaviourPart> behaviourParts = new List<PersonBehaviourPart>();

        public override void MyUpdate()
        {
            //ServerPathFinder.DebugPrintSimpleMap(map, Helper.GetCell(position.X), Helper.GetCell(position.Z), 17);

            foreach (var part in behaviourParts)
                part.Update();

            float now = Time.time;

            if (now > OneSecCheck)
            {
                if (owner != null)
                {
                    if (bNeedUpdateMiniMapFull)
                    {
						bNeedUpdateMiniMapFull = false;
                        ServerRpcListener.Instance.SendMiniMapFull(owner, map.MinimapManager.GetFullMinimap(owner));
                    }
				}

                //просто измеряем расстояние до ближайшего монстра, больше никакой логики
                MinMonsterDistance = float.MaxValue;
                foreach (var m in map.GetAllLiveTarget())
                {
                    if (CanAttackTarget(m) && (m.position - position).magnitude()< MinMonsterDistance)
                        MinMonsterDistance = (m.position - position).magnitude();
                }

                //дергаем событие в аддинах
                map.OnTimeOneSecCheck(owner);
                OneSecCheck = now + 1;

				//Почистить лут
				OverflowLoot.RemoveWhere(key => OverflowLoot[key] < Time.time);
			}

            if (now > MaxChangeTransformTime)
                directionMove = Vector2.Zero;

            buttonsAvalability.Clear();
            foreach (var ability in owner.activeAbility)
            {
                if (ability.SlotId == -1)
                    continue;

                ability.ButtonIsActive = ability.GetButtonIsActive();

                if (ability.NeedSendStatus)
                {
                    ability.NeedSendStatus = false;
                    buttonsAvalability.Add((byte)ability.SlotId, ability.ButtonIsActive);
                }
            }
            if (buttonsAvalability.Count > 0)
                ServerRpcListener.Instance.SendRpc(owner.ClientRpc.AbilityButtonStateResponse, buttonsAvalability);

            if (SpellCastAction != null)
            {
                SpellCastAction.AbilityUpdate();
            }

			if (map.ExplorationGameMode == GameModeType.Exploration)
			{
				bool relax = owner.StatContainer.GetStatValue(Stats.Adrenaline) <= 0;
				if (relax && now > ExplorationTime && !owner.StatContainer.ExistStatus())
					SetGameMode(GameModeType.Exploration);
			}

	        if (SpeedOverrideFunction == null & !isMotionControlledByServer)
		        owner.UpdateShiningPath();

			if (owner.TutorialIsRun)
			{
				foreach (var tutorMapObject in owner.TutorialActiveMapObjects)
					tutorMapObject.MyUpdate();

				//Данж закрылся, начать стейт заново
				if (owner.bTutorialResetIfDungeonClose.HasValue)
				{
					var dungeon = owner.DungeonPersonLocationDatas.FirstOrDefault(x => x.LocationId == owner.bTutorialResetIfDungeonClose);
					if (dungeon != null)
					{
						dungeon.CheckTimeFloor();
						if (dungeon.ControlState == ControlState.Close)
						{
							owner.TutorialStateAdd = 0;
							owner.ResetTutorialState();
							owner.bTutorialResetIfDungeonClose = null;
						}
					}
				}

				//Потратил деньги, начать стейт заново
				if (owner.bTutorialResetIfSpentMoney.HasValue && owner.Gold < owner.bTutorialResetIfSpentMoney.Value)
				{
					owner.TutorialStateAdd = 0;
					owner.ResetTutorialState();
					owner.bTutorialResetIfSpentMoney = null;
				}

				if (owner.bTutorialResetIfQuestOptionalNotComplite != null)
				{
					var questId = owner.bTutorialResetIfQuestOptionalNotComplite;
					var quest = owner.ActiveQuest.FirstOrDefault(x => x.Name == questId);
					if (quest != null && quest.State == QuestState.Active && quest.Tasks.Exists(task => task.IsOptional && task.Done < task.Count))
					{
						owner.TutorialStateAdd = 0;
						owner.ResetTutorialState();
						owner.bTutorialResetIfQuestOptionalNotComplite = null;
					}
				}
			}

            if (hpowChanged)
            {
                hpowChanged = false;
            }

			if (owner.NeedUpdatePotion)
			{
				owner.NeedUpdatePotion = false;
				ServerRpcListener.Instance.SendLocationPotionDataRequest(owner);
			}

			/*if (!map.CanAttack  && Target != null)
			{
				ServerRpcListener.Instance.SelectMonsterResponse(this, Target.eType, Target.Id, TargetIcon.Aim, false);
				Target = null;
			}*/

			//Проверяем находится персонаж с безопасной зоне, вошел в неё или вышел
			bool inSaveZone = map.IsSafeZone(position);
            if (owner.bInSafeZone != inSaveZone)
            {
                if (owner.bInSafeZone)
                    owner.OnSafeZoneExit();
                else
                    owner.OnSafeZoneEnter();
                owner.bInSafeZone = inSaveZone;
            }
        }

        public override void ResetSpeedOverride()
        {
            base.ResetSpeedOverride();
            directionMove = clientJoystickDirection;
        }

        public override void InterruptDash(bool CanMoveInCastTime)
        {
            if (overrideSpeedTargetTime > Time.time && overrideSpeedAbilityType == AbilityType.Dash2)
            {
                ResetSpeedOverride();
                if (!CanMoveInCastTime)
                {
                    directionMove = Vector2.Zero;
                }
                ServerRpcListener.Instance.StartAbilityResponseInterrupt(this, overrideSpeedAbilityType);
            }
        }

		/// <summary>
		/// Хранит вектор джойстика на клиенте. Даже когда поворот и движение игрока заблокированы, он всё равно может пытаться бежать в каком-то направлении
		/// Для того чтобы не было остановок в движении, мы будем использовать это направление в момент возвращения игроку управления 
		/// </summary>
		private Vector2 clientJoystickDirection;

        private float lastTimeSinceMotionControlledByServer;

		public float ping = 0.1f;
        public override void UpdatePhysics()
        {
            if (!isMotionControlledByServer && isClientCanMove && Time.time > lastTimeSinceMotionControlledByServer + 0.5f 
				&& actualClientPosition != Vector3.Zero 
				&& (lastSentPosition - actualClientPosition).magnitude() > (ping + SEND_RATE) * speed * 2 /*0.9f*/)
            {
				ILogger.Instance.Send($"Correct position time {Time.time} player {NickName}: " +
									  $"lastSentPosition {lastSentPosition}, actualClientPosition {actualClientPosition}, " +
									  $"speed {speed}, ping {ping}");

				position = actualClientPosition;
                position = Helper.UltraCorrectYPosition(map, position);
                lastSentPosition = position;
            }

            if (isMotionControlledByServer)
            {
                if (Time.time < overrideSpeedTargetTime)
                {
                    if (SpeedOverrideFunction != null)
                    {
                        float lastYSpeed = Speed.Y;
                        Speed = SpeedOverrideFunction(Math.Min(1f - (overrideSpeedTargetTime - Time.time) / overrideSpeedTime, 1f));
                        Speed.Y = lastYSpeed;
                    }
                }
                else
                {
                    if (SpeedOverrideFunction != null)
                    {
                        SpeedOverrideFunction = null;
                        directionMove = clientJoystickDirection;
                        isClientMustInvertMoveMode = false;
                    }
                }

                Speed.Y -= GravityInfluence.ACCELERATION * Time.deltaTime;
                position = Helper.UltraMegaCorrectXZPosition(map, position, position + Speed * (Time.deltaTime), IsFly, airway: IsAirway);
                Vector3 correctPosition = Helper.UltraCorrectYPosition(map, position, true, airway: IsAirway);
                var h = correctPosition.Y;
                IsFly = (position.Y >= h + Helper.VoxelSize);
                
                if (position.Y < h && Speed.Y <= 0)
                {
                    position = correctPosition;

                    if (SpeedOverrideFunction == null)
                    {
                        isMotionControlledByServer = false;
                        Speed = GeometryAlgorithm.Vector2ToVector3(directionMove) * speed;
                        //Sos.Debug("Движение не контролируется сервером " + directionMove);
                    }
                    
                    Speed.Y = 0;
                }
                else
                {
                    isMotionControlledByServer = true;
                }

                lastTimeSinceMotionControlledByServer = Time.time;
                actualClientPosition = position;
            }

            bool abilityMove = SpellCastAction == null || SpellCastAction.CanMove();
            
            if (!IsDead && !bStun && !bRoot && abilityMove && !isMotionControlledByServer && isClientCanMove)
            {
                Speed = GeometryAlgorithm.Vector2ToVector3(directionMove) * speed;
                if (directionMove != Vector2.Zero)
                {
                    position = Helper.UltraMegaCorrectXZPosition(map, position, position + Speed * Time.deltaTime, airway: IsAirway);
                    Vector3 correctPosition = Helper.UltraCorrectYPosition(map, position, true, airway: IsAirway);
                    var h = correctPosition.Y;
                    if (position.Y <= h + Helper.VoxelSize)
                    {
                        isMotionControlledByServer = false;
                        position = correctPosition;
                    }
                    else
                    {
                        //Sos.Debug("начали падение");
                        isMotionControlledByServer = true;
                        IsFly = true;
                        Speed = GeometryAlgorithm.Vector2ToVector3(directionMove) * (speed / 2);
                    }

                    owner.OnMove();
                }
            }
        }

        public InstancePerson owner { get; private set; }

        public override string PersonalParam
        {
            get { return owner.PersonalParam; }
        }

        [RPC("ENT")]
        public void EntityReadyRequest(InstancePerson instancePerson)
        {
            SendSetGameModeResponse(instancePerson);
            UpdateEquippedWeapons(instancePerson);
            SendRpc(instancePerson.ClientRpc.SetHealth, Health, MaxHealth);
            SendEnergy(instancePerson);
            SendStamina(instancePerson);
            SendExperience(instancePerson);

            if (instancePerson.player == sender)
            {
                //Только владельцу
                ServerRpcListener.Instance.SendRpc(instancePerson.ClientRpc.UpdateWeaponResponse, owner.GetPersonParts());
                ServerRpcListener.Instance.SendLocationPotionDataRequest(owner);
            }
            else
            {
                //Всем, кроме владельца
                SendPlayAnimationStateResponse(instancePerson);
				//SendRpc(info.ClientRpc.StartMoveResponse, move, movePos); //TODO_Deg в самом начале movePos почемуто Zero и на клиенте все ломается, потом починить надо
            }

            float period = (float)GetStatContainer().GetStatValue("PeriodAttack");
            SendRpc(instancePerson.ClientRpc.UpdateHitTimeResponse, period);

            foreach (var infl in owner.StatContainer.Influences)
            {
                infl.SendEffect(instancePerson);
            }

            foreach (var effect in effects)
                ServerRpcListener.Instance.SendCreateFxResponse(this, effect);

            foreach (var effect in map.effects)
                if (effect.ownerId == Id)
                    ServerRpcListener.Instance.CreateFxResponse_Regular(instancePerson, effect.id, effect.ownerId, EntityType.Entity, effect.GetFxId(instancePerson.syn), effect.position, 0, effect.scale);

            //owner.StatContainer.UpdateInfluences();

            map.OnEntityReadyRequest(owner);

            var container = (PersonStatContainer)StatContainer;
            //ILogger.Instance.Send("EntityReadyRequest " + instancePerson.player.id, ErrorLevel.error);
            SendRpc(instancePerson.ClientRpc.StatContainerResponse_Beh, container.GetClientStats(), container.GetClientStringsStats(), true); //TODO_deg _Beh
        }

        public void StatContainerSend( Dictionary<ushort, double> dict1, Dictionary<BuffString, double> dict2, bool full)
        {
            SendRpc(map.ClientRpc.StatContainerResponse_Beh, dict1, dict2, full); //TODO_deg _Beh
		}

        public Vector3 actualClientPosition;
        bool isClientCanMove = true;

        [RPC("CMDS")]
        public void ChangeMoveDirectionWithSkillDirectionRequest(InstancePerson instancePerson, Vector2DirectonPacked moveDirection, Vector2DirectonPacked skillJoystickDirection, 
	        Vector3PositionPacked clientPosition, bool isPlayerCanMoveOnClient, byte sessionId)
        {
            if (this.sessionId != sessionId)
                return;

            ProcessMoveDirectionRequest(moveDirection.Direction, skillJoystickDirection.Direction, clientPosition.Position, isPlayerCanMoveOnClient, true);
        }

        [RPC("CMD")]
        public void ChangeMoveDirectionRequest(InstancePerson instancePerson, Vector2DirectonPacked moveDirection, Vector3PositionPacked clientPosition, 
	        bool isPlayerCanMoveOnClient, byte sessionId)
		{
            if (this.sessionId != sessionId)
                return;

            ProcessMoveDirectionRequest(moveDirection.Direction, Vector2.Zero, clientPosition.Position, isPlayerCanMoveOnClient, false);
        }

        void ProcessMoveDirectionRequest(Vector2 moveDirection, Vector2 skillJoystickDirection, Vector3 clientPosition, 
	        bool isPlayerCanMoveOnClient, bool isNeedConsiderSkillJoystick)
        {
	        directionMove = moveDirection;
            isClientCanMove = isPlayerCanMoveOnClient;

            clientJoystickDirection = isNeedConsiderSkillJoystick ? skillJoystickDirection : moveDirection;

            MaxChangeTransformTime = Time.time + 0.5f;
            if (!bStun && !isMotionControlledByServer && (SpellCastAction == null || SpellCastAction.CanRotate()))
            {
                if (isNeedConsiderSkillJoystick)
                {
                    direction = skillJoystickDirection;
                }
                else if (moveDirection != Vector2.Zero)
                {
                    direction = moveDirection;
                }
                //direction = isNeedConsiderSkillJoystick ? skillJoystickDirection : directionMove;
            }

            //TODO_maximpr тут позже будет другая защита, чтобы сервер не поверил клиенту
            if (!isMotionControlledByServer && (actualClientPosition - clientPosition).magnitude() < 1)
            {
                actualClientPosition = clientPosition;
            }
            else
            {
                actualClientPosition = position;
                if (!isMotionControlledByServer && overrideSpeedTargetTime + 0.5f < Time.time)
                    SendRpc(map.ClientRpc.ApplyTeleportation, Id, position);
				if (!isMotionControlledByServer)
					ILogger.Instance.Send($"Too big difference between server and client position, clientPosition = {clientPosition}, actualClientPosition = {actualClientPosition}, position = {position}, LoginId = {owner.loginId}", ErrorLevel.info);
            }

			map.OnMoveDirectionRequest(owner);
        }

        public override void SendChangeTransformResponse()
		{
			bool positionChanged = CheckNeedSendPositionChange();
			
			foreach (var p in map.persons)
			{
				//if (p == null)
				//	continue;
				if (p.SendChangeTransformRPCMode != ChangeTransformReliability.NotSet)
				{
					map.RegisterForTransformSend(p.owner, p.SendChangeTransformRPCMode, this);
				}
				else if (positionChanged && p.SendRPC)
				{
					map.RegisterForTransformSend(p.owner, ChangeTransformReliability.Unreliable, this);
				}
			}
        }

		public override ChangeTransformData GetTransformData()
		{
			var changeTransformData = new ChangeTransformData(Id, position, direction,
				SpeedOverrideFunction != null, IsFly, isClientMustInvertMoveMode, SpellCastAction != null ? SpellCastAction.IsAimInProcess() : false);
			return changeTransformData;
		}

		public override ChangeTransformForceData GetTransformForceData()
		{
			var changeTransformForceData = new ChangeTransformForceData(Id, position, direction);
			return changeTransformForceData;
		}

		private bool lastSentSpeedOverride;
		private bool lastSentIsFly;
		private bool lastSentInvertMode;

		protected override bool IsPositionChanged()
		{
			return base.IsPositionChanged() ||
				   lastSentSpeedOverride != (SpeedOverrideFunction != null) ||
				   lastSentIsFly != IsFly ||
				   lastSentInvertMode != isClientMustInvertMoveMode;
		}

		protected override void SetLastSendPosition()
		{
			base.SetLastSendPosition();

			lastSentSpeedOverride = SpeedOverrideFunction != null;
			lastSentIsFly = IsFly;
			lastSentInvertMode = isClientMustInvertMoveMode;
		}

        [RPC]
        public void DpsDebugInfoRequest(bool state)
        {
            isNeedSendDpsInfo = state;
        }

        [RPC]
        public void PlayAnimationStateRequest(InstancePerson instancePerson, int animState, byte sessionId)
        {
            if (this.sessionId != sessionId || IsDead || CurrentAnimState == animState)
                return;

            CurrentAnimState = animState;

            SendPlayAnimationStateResponse();
        }

        [RPC]
        void InterruptAbilityRequest(InstancePerson instancePerson)
        {
            if (SpellCastAction != null)
            {
                SpellCastAction.Reset();
                SpellCastAction = null;
            }
        }

        [RPC]
        private void SetTargetAimRequest(InstancePerson instancePerson, int targetId, bool rotate)
        {
            MapTargetObject target;
            if (targetId <= 0)
                target = null;
            else
                target = map.GetAllTarget().FirstOrDefault(t => t.Id == targetId);
            if (target != null)
            {
                if (!CanAttackTarget(target))
                    target = null;
                else
                {
                    var sqrTapRadius = ServerConstants.Instance.TapSqrRadius;
                    if ((target.position - position).sqrMagnitude() > sqrTapRadius)
                        target = null;
                }
                if (target != null)
                {
                    if (rotate)
                        direction = (target.position - position).Vector3ToVector2().normalized();
                    ServerRpcListener.Instance.SelectMonsterResponse(this, target.eType, target.Id, TargetIcon.Aim, true);
                }
            }
            else
            {
                    ServerRpcListener.Instance.SelectMonsterResponse(this, EntityType.Entity, -1, TargetIcon.Aim, false);
            }
            Target = target;
        }

        [RPC]
        private void SetDirectionRequest(InstancePerson instancePerson, Vector2DirectonPacked dir)
        {
            //тут нужно будет добавить проверки на фозможность поворота персонажа, а то мало ли вдруг кто клиент поломает
            direction = dir.Direction;
        }

        [RPC]
        private void SetTargetLockRequest(InstancePerson instancePerson, int targetId)
        {
            MapTargetObject target;
            if (targetId <= 0)
                target = null;
            else
                target = map.GetAllTarget().FirstOrDefault(t => t.Id == targetId);
            TargetLockObject = target;
        }


        private void SendPlayAnimationStateResponse()
        {
            foreach (var person in map.persons)
                if (person != this)
                    SendPlayAnimationStateResponse(person.owner);
        }

        private void SendPlayAnimationStateResponse(InstancePerson person)
        {
            SendRpc(person.ClientRpc.PlayStateResponse, CurrentAnimState);
        }

        [RPC]
        public void StartMoveRequest(InstancePerson instancePerson, bool newMove, Vector3 endPos)
        {
            move = newMove;
            movePos = endPos;
            SendRpc(map.ClientRpc.StartMoveResponse, move, movePos);
        }

		#region GameMode

		private ActualPlaceTitle actualPlaceTitle;
        private float ExplorationTime = float.MaxValue; //Время, когда нужно перейти в мирный режим

        public override void SetGameMode(GameModeType gameMode, ActualPlaceTitle title = ActualPlaceTitle.None)
        {
            SetGameMode(gameMode, title, ServerConstants.Instance.BattleStateOnSpellCastTimeout);
        }

        public void SetGameMode(GameModeType gameMode, ActualPlaceTitle title, int timeSec)
        {
            if (GameMode != gameMode || actualPlaceTitle != title)
            {
                GameMode = gameMode;
                actualPlaceTitle = title;

                owner.OnGameModeChange(gameMode);

                SendSetGameModeResponse();

                //Если нельзя атаковать, то заблочить кнопку
                if (GameMode == GameModeType.Exploration && !map.CanAttack)
                    ServerRpcListener.Instance.SendAbilityLockButton(owner);
            }

            if (GameMode == GameModeType.Battle)
                ExplorationTime = Time.time + timeSec;
            else
                ExplorationTime = float.MaxValue;
        }


        private GameModeType GameModeForClient
        {
            get { return GameMode == GameModeType.Exploration ? map.ExplorationGameMode : GameMode; }
        }

        public void SendSetGameModeResponse()
        {
            SendRpc(map.ClientRpc.SetGameModeResponse, GameModeForClient, actualPlaceTitle);
        }

        public void SendSetGameModeResponse(InstancePerson person)
        {
            SendRpc(person.ClientRpc.SetGameModeResponse, GameModeForClient, actualPlaceTitle);
        }

		#endregion GameMode

        public bool hpowChanged = false;

        public void UpdateEquippedWeapons(InstancePerson person)
		{
            SendRpc(person.ClientRpc.UpdateEquipmentResponse, GameModeForClient, owner.EquippedWeapons);
        }

        public void UpdateEquippedWeapons()
        {
            SendRpc(map.ClientRpc.UpdateEquipmentResponse, GameModeForClient, owner.EquippedWeapons);
        }

        [RPC("STLFP")]
        public void SetTargetLockFromPriority(InstancePerson instancePerson)
        {

            var ability = owner.activeAbility.Find(x => x.SlotId == 0);
            if (ability == null)
                return;

            MapTargetObject target  = map.GetNearestTarget(this, ability.GetAutoPricelRadius(), false);

            SetTargetLock(target);
        }

        //Поиск цели
        [RPC]
        public void SearchTargetRequest(InstancePerson instancePerson, AbilityType abilityType)
        {
            //SetGameMode(GameModeType.Battle);

            var ability = owner.GetAbilityById(abilityType);
            if (ability == null)
                return;

            switch (abilityType)
            {
                case AbilityType.ImmolationTrap:
                    AutoPricel(center, ability.GetAutoPricelRadius(), false, false, true, false);
                    break;
                default:
                    if(owner.login.settings.enable_autoattack)
                        AutoPricel(center, ability.GetAutoPricelRadius(), true);
                    break;
            }

            if (owner.login.settings.enable_autoattack)
                SendRpc(ClientRpc.SearchTargetResponse, direction);
        }

        public override Vector3 AutoPricel(Vector3 pos, float AutoPricelR, out MapTargetObject target, bool isNeedApplyTargetLock, bool bAttackResponse = false, bool selectStones = true, bool bChangeDirection = true)
        {
            target = null;
            if (owner.bInSafeZone)
            {
                return Vector3.UnitX;
            }

            Vector3 targetDir;
            if (TargetLockObject != null)
            {
                var radius = Helper.GetDistanceIgnoreY(TargetLockObject.position, position) - TargetLockObject.Radius;
                if (radius <= AutoPricelR)
                    target = TargetLockObject;

                targetDir = (TargetLockObject.center - center).normalized();
            }
            else
            {
                targetDir = base.AutoPricel(pos, AutoPricelR, out target, isNeedApplyTargetLock, bAttackResponse, selectStones, bChangeDirection);
            }

            if (target != null && bChangeDirection)
            {
                direction = targetDir.Vector3ToVector2().normalized();
            }

            if (target != null && bAttackResponse)
				SendRpc(ClientRpc.AttackResponse, direction);

            /*if (isNeedApplyTargetLock && (TargetLockObject == null || target != null))
            {
                SetTargetLock(target);
            }*/

            return targetDir;
        }

        //Аттака персонажа
        //Этот метод больше не вызывается на клиенте. От 13.02.2018. dma.
        [RPC]
        public void AttackRequest(InstancePerson instancePerson, Vector3 position, Vector2 direction)
        {
            if (IsDead)
                return;

            this.position = position;
            this.direction = direction;

            SetGameMode(GameModeType.Battle);

	        var autoAtack = owner.activeAbility.Find(x => x.SlotId == 0);
			AutoPricel(center, autoAtack.GetAutoPricelRadius(), true);

            //SendRpc(RpcEnum.AttackResponse, sender, this.direction);
        }

        internal override Elements AttackElement()
        {
            var statContainer = GetStatContainer();
            return (Elements)(int)statContainer.GetStatValue(Stats.AttackElement);
        }
        
        public override bool Hit(MapTargetObject attacker, int damage, DamageType type, Elements atkElement, out bool crit, bool canMiss = true, 
            bool resetCast = true, bool absoluteHit = false, bool autoAttack = false, CritType critType = CritType.AbilityAttack, HitEffectXmlData hitFxData = null)
        {
            return StatsFormulas.Instance.Hit(attacker, this, damage, type, atkElement, out crit, canMiss, resetCast, absoluteHit, autoAttack, critType, hitFxData);
        }

        public override void OnInDamage(MapTargetObject attacker, ref int damage, bool resetCast)
        {
            owner.OnInDamage (attacker, ref damage, resetCast);
        }

        public override void OnOutDamage(StatContainer StatContainer, int damage, bool crit, ref double mult, DamageType type, bool autoAttack = false)
        {
            owner.OnOutDamage(StatContainer, damage, crit, ref mult, type, autoAttack);
        }

        public override void OnDodge()
        {
            owner.OnDodge();
        }

        public override void AddHealth(MapTargetObject attacker, int value, bool selfRegeneration = false, bool canCrit = true)
        {
            int health = Health;

            HealResult healResult = StatsFormulas.Instance.CalculateHeal(attacker, this, value, selfRegeneration, canCrit);
            
            SetHealth(attacker, Health + (int)healResult.HealValue);

            if (!selfRegeneration && value != 0)
                ServerRpcListener.Instance.SendAddCritFrameResponse(attacker, this, (int)healResult.HealValue, TextFrameType.Healing, Elements.Neutral, DamageType.Melee);

            if (!selfRegeneration)
            {
                int effectiveHeal = Health - health;
                if (effectiveHeal > 0)
                    map.OnPersonHeal(this, attacker, effectiveHeal);
            }
        }

        public void SendHealth()
        {
            SendRpc(map.ClientRpc.SetHealth, Health, MaxHealth);
        }

        public void SendEnergy(InstancePerson person = null)
        {
	        if (person != null)
		        SendRpc(person.ClientRpc.SetEnergy, (ushort)Energy, (ushort)MaxEnergy);
	        else
		        SendRpc(map.ClientRpc.SetEnergy, (ushort)Energy, (ushort)MaxEnergy);
        }

        public void SendStamina(InstancePerson person = null)
		{
			if (person != null)
				SendRpc(person.ClientRpc.SetStamina, Stamina, MaxStamina);
			else
				SendRpc(ClientRpc.SetStamina, Stamina, MaxStamina);
        }

        public void SendPeriodAttack()
        {
            float period = (float)GetStatContainer().GetStatValue("PeriodAttack");
            SendRpc(map.ClientRpc.UpdateHitTimeResponse, period);
        }

        public override void SetHealth(MapTargetObject attacker, int value, bool crit = false)
        {
            if (IsDead)
                return;

            var MinHeath = (int)(owner.StatContainer.GetStatValue(Stats.CanNotDie) * MaxHealth / 100);

            value = Math.Min(Math.Max(MinHeath, value), MaxHealth);

            if (value < Health)
            {
                if (owner.bInSafeZone || !bOnMap)
                    return;
            }

            Health = value;
            SendHealth();

            owner.StatContainer.ResetCalcStats("HPcurrent");

            if (IsDead)
            {
                ResetCooldown();
                SetTargetLock(null);
                owner.StatContainer.DeathRemoveInfluences();

                map.OnPersonDie(owner, attacker);

                SendRpc(map.ClientRpc.DeathResponse, owner.ResurrectSec);

				map.OnPlayerDeathInfoResponse(owner, attacker);

                CurrentAnimState = animState_die;
                SendPlayAnimationStateResponse();

                if (owner.SaveAndExitTime.HasValue)
                    owner.SaveAndExitTime = TimeProvider.UTCNow.AddSeconds(ServerConstants.Instance.SaveAndExitTimeSecAfterDead);

                foreach (var p in map.persons)
                {
                    p.owner.OnPersonDie(this);
                }

                for (int i = 0; i < map.monsters.Count; i++)
                {
                    map.monsters[i].OnTargetDie(Id);
                }

                if (SpellCastAction != null)
                {
                    SpellCastAction.Reset();
                    //ILogger.Instance.Send("SpellCastAction Reset", ErrorLevel.error);
                }

                if (!owner.bRunTutorial)
                    owner.HideShiningPath();

                if (attacker != null && attacker.characterType == CharacterType.Person)
                    map.OnKillAnnounce(attacker, this);
            }
        }

        public override void SetEnergy(int value)
        {
            if (IsDead)
                return;

            int oldVal = Energy;
            Energy = Math.Max(0, Math.Min(value, MaxEnergy));
            if (oldVal == Energy)
                return;

            SendEnergy();
        }

        public void SetStamina(int value)
        {
            if (IsDead)
                return;
            int oldVal = Stamina;
            Stamina = Math.Max(0, Math.Min(value, MaxStamina));
            if (oldVal == Stamina)
                return;
            
            SendStamina();
        }


        private float baseSpeed = 2;
        public override float speed { get { return RemoteXmlConfig.Instance.BaseSpeed * baseSpeed * speedMult; } }


        private float speedMult = 1.0f;

        public void SetSpeedMult()
        {
            speedMult = (float)Math.Max((100 + owner.StatContainer.GetStatValue(Stats.MoveSpeed)) / 100, 0);
            Sos.Debug("speedMult " + speedMult);
            SendRpc(ClientRpc.ChangeSpeedResponse, speed);
        }

		#region MapActiveObject

		private MapActiveObject m_CurrentMapActiveObject;
		public MapActiveObject CurrentMapActiveObject
		{
			get { return m_CurrentMapActiveObject; }
			set
			{
				m_CurrentMapActiveObject = value;

				if (value != null)
				{
					HistoryMapActiveObject.Add(value);
					if (HistoryMapActiveObject.Count > ServerConstants.Instance.ActiveMapObjectsHistoryCount)
						HistoryMapActiveObject.RemoveAt(0);
				}
			}
		}

		//История посещения активных объектов
		public List<MapActiveObject> HistoryMapActiveObject = new List<MapActiveObject>();

        public MapEffect MapActiveObjectEffect;

        public Dictionary<MapActiveObject, MapEffect> NearMapActiveObject = new Dictionary<MapActiveObject, MapEffect>();

		#endregion

        DateTime GlobalCooldown = DateTime.MinValue;
	    Dictionary<AbilityType, DateTime> Cooldowns = new Dictionary<AbilityType, DateTime>();

        public override AbilityErrors CheckEnergyCooldown(AbilityXmlData data)
        {
            //Глобальный кулдаун распространяется на все умения, но не на автоатаку
            if (GlobalCooldown > TimeProvider.UTCNow && data.SlotId != 0)
                return AbilityErrors.Cooldown;

            if (Cooldowns.ContainsKey(data.Type) && Cooldowns[data.Type] > TimeProvider.UTCNow)
                return AbilityErrors.Cooldown;

            if (data.GetEnergy() > Energy)
            {
                return AbilityErrors.NeedEnergy;
            }

            if (data.StaminaCost > Stamina)
            {
                return AbilityErrors.NeedStamina;
            }

            return AbilityErrors.Success;
        }

        public void ResetCooldown()
        {
            GlobalCooldown = TimeProvider.UTCNow;
            Cooldowns.Clear();
        }

        public void ResetCooldown(AbilityType type)
        {
            GlobalCooldown = TimeProvider.UTCNow;
            Cooldowns.Remove(type);
        }

        public override void RemoveEnergy(AbilityXmlData data)
        {
            SetEnergy(Energy - data.GetEnergy());
            SetStamina(Stamina - data.StaminaCost);
        }

        public override void SetCooldown(AbilityXmlData data)
        {
            GlobalCooldown = TimeProvider.UTCNow.AddSeconds(data.GlobalCooldownValue);
            float cooldown = data.CooldownValue;
            Cooldowns[data.Type] = TimeProvider.UTCNow.AddSeconds(cooldown);

            if (data.SlotId > 0)
                ServerRpcListener.Instance.SendRpc(owner.ClientRpc.ShowAbilityButtonProgressResponse, (byte)data.SlotId, cooldown.ToFloatUnsignedPacked());
        }

        public void ResetCooldown(AbilityXmlData data)
        {
            Cooldowns[data.Type] = TimeProvider.UTCNow;

            if (data.SlotId > 0)
                ServerRpcListener.Instance.SendRpc(owner.ClientRpc.ShowAbilityButtonProgressResponse, (byte)data.SlotId, 0f.ToFloatUnsignedPacked());
        }

        public bool bNearMapActiveObject(MapTargetObject exclude, out MapActiveObject obj)
        {
            var NearR = ServerConstants.Instance.ActiveMapObjectsNearRadius;

            foreach (var mapObject in owner.GetActiveMapObjects())
            {
                if (!mapObject.Enable)
                    continue;

                if (mapObject.target == exclude)
                    continue;

                if (mapObject.GetDistance(this) > NearR)
                    continue;

                obj = mapObject;
                return true;
            }

            obj = null;
            return false;
        }

        public void SendSetMotionLimits(bool stunStatus, bool rootStatus)
        {
            SendRpc(ClientRpc.SetMotionLimits, stunStatus, rootStatus);
        }

        public override int Level
        {
            get { return owner != null ? owner.Level : 0; }
            set { }
        }

        private bool AbilityPreparation(Vector3 abilityPos, AbilityType type)
        {
            if (SpellCastAction != null) // уже что-то другое кастует
            {
                if (SpellCastAction.IsAbilityCastingNow() || SpellCastAction.IsAbilityCanBeInterruptedInApplyTime)
                {
                    SpellCastAction.Reset();//прервем //TODO надо прерывать!!
                }
                else
                {
                    var abilityData = owner.GetAbilityById(type);
                    if(abilityData != null)
                    {
                        ServerRpcListener.Instance.StartAbilityResponseInterrupt(this, type);
                    }
                    return false;
                }
            }
            
            //Sos.Debug("AbilityPreparation " + type);
            var data = owner.GetAbilityById(type);
            if (data == null)
                return false;

            ///var castAction = data.GetSpellCastAction(abilityPos, data);
            data.AbilityPos = abilityPos;
            if (data.CanCast() != AbilityErrors.Success)
            {
                data.Reset();//чтобы клиент прервал то что успел начать
                return false;
            }

            Sos.Debug("Try set spell time " + data.GetProgressBarTime() + " " + data.ApplyTime);
            data.SetSpellTime();
            SpellCastAction = data;
            data.Preparation();

            return true;
        }

        [RPC]
        public void AbilityStartRequest(Vector3 abilityPos, AbilityType abilityType)
        {
            //Sos.Debug("AbilityStartRequest");
            AbilityPreparation(abilityPos, abilityType);
        }

        public void SendAbilityEndResponse(AbilityType ability, int instId, bool isCastComplete)
		{
			SendRpc(ClientRpc.AbilityEndResponse, ability, instId, isCastComplete);
		}

        public void TargetTap(MapTargetObject target)
        {
            if (target == TargetLockObject)
                target = null;//Повторный тап снимает метку (c)

            TargetLockObject = target;
        }

        /* public void SetTargetLock(MapTargetObject target)
         {

         }*/


        public void SetTargetLock(MapTargetObject target)
        {
            if (target == TargetLockObject)
                return;

            if (target is MapObject)
                return;

            if (target is MonsterBehaviour && (target as MonsterBehaviour).Data.MonsterType == MonsterType.Animal)
                return;

            if (target != null)
                ServerRpcListener.Instance.SelectMonsterResponse(this, target.eType, target.Id, TargetIcon.Lock, true);
            else
                ServerRpcListener.Instance.SelectMonsterResponse(this, TargetLockObject.eType, TargetLockObject.Id, TargetIcon.Lock, false);
            TargetLockObject = target;
        }

        public override void OnHit(MapTargetObject target, int damage)
        {
            owner.OnHit(target, damage);
        }

        public override void OnKill(MapTargetObject target, int damage)
        {
            owner.OnKill(target, damage);
        }

        protected override bool IsCanApplySpeedOverride => base.IsCanApplySpeedOverride && !owner.bInSafeZone;

        public override void KnockBack(Vector3 dir, float time, float dist, bool force = false, bool resetCast = false)
        {
            if (!force && (isMotionControlledByServer || !IsCanApplySpeedOverride))
                return;

            isMotionControlledByServer = true;
            Speed = dir.normalized() * dist / time;
            Speed.Y = GravityInfluence.ACCELERATION * time / 2;
            directionMove = Vector2.Zero;
            SetSpeedOverrideFunction(time, AbilityType.Charge, KnockBackSpeed);
            
            base.KnockBack(dir, time, dist, force, resetCast);
        }        

        public Vector3 KnockBackSpeed(float time)
        {
            if (Time.time > overrideSpeedTargetTime - 0.1f && IsFly)
                overrideSpeedTargetTime = Time.time + 0.1f;

            return Speed;
        }

        public override bool IsTargetAlly(MapTargetObject target)
        {
            if (target == null)
                return false;

            if (this == target)
                return true;

			if (TeamColor == PlayerTeam.None && target.TeamColor == PlayerTeam.None)
			{
				return false;
			}

			return TeamColor == target.TeamColor;
        }

        #region Resurrect
        
        internal override void ResurrectNow()
        {
            //owner.ResurrectTime = DateTime.MaxValue;
            var oldPos = position;

            Health = MaxHealth;
            SetEnergy(MaxEnergy);
            InitPosition();


            ActivateShiningPathToCorpse(oldPos);

            var statContainer = GetStatContainer();
	        var favorOfGods = InfluenceXmlDataList.Instance.GetInfluence(this, InfluenceHelper.FavorOfGodsInflId, ServerConstants.Instance.FavorOfGodsTimeSec, 1);
            statContainer.AddInfluence(favorOfGods);

            statContainer.RestoreInfluenceEffects();

            GlobalCooldown = TimeProvider.UTCNow.AddSeconds(favorOfGods.time);

            sessionId++;

            SendRpc(map.ClientRpc.ResurrectResponse, position, direction, sessionId);
            CoroutineRunner.Instance.Timer(favorOfGods.time, () => { SendRpc(map.ClientRpc.MakePlayerMortalResponse); });

			map.OnPersonResurrect(owner);
        }

        private void ActivateShiningPathToCorpse(Vector3 oldPos)
        {
            if (map.IsBattleground || map.IsBattlegroundArena)
                return;

            if (owner.bRunTutorial)
                return;

            owner.SetShiningPath(ActualPlaceType.ToPoint, oldPos, null, MinimapItemType.Corpse);
        }

        #endregion


        public void TeleportPersonToPoint(Vector3 teleportPosition)
        {
            SendRpc(ClientRpc.StartTeleportation);

            CoroutineRunner.Instance.Timer(0.35f, () =>
            {
                ForseSetPosition(teleportPosition);
            });

            CoroutineRunner.Instance.Timer(0.7f, () =>
            {
                SendRpc(ClientRpc.EndTeleportation);
            });
        }


        public void ForseSetPosition(Vector3 newPosition)
        {
            position = newPosition;
            actualClientPosition = newPosition;

            SendRpc(map.ClientRpc.ApplyTeleportation, Id, position);
        }

        public override double AutoPricelPriority(MapTargetObject person)
        {
            if (owner.bInSafeZone)
                return -1;
            if (owner.bFavorOfGods)
                return -1;
            return AutoPricelPriority(person, 2);
        }
	}
}
