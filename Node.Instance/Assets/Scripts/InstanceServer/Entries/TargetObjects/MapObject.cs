﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using UtilsLib.Logic.Enums;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.JsonBlockClasses;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Entries.TargetObjects
{
	public class MapObject : MapTargetObject
	{
		public static Dictionary<string, ServerConstants.MapObjectConstant> MapObjectConstants;

        //public CharacterType m_characterType;

        public override CharacterType characterType
		{
            //get { return m_characterType; }
            //set { m_characterType = value; }
            get { return IsLiveObject ? CharacterType.Res : CharacterType.None; }
		}

		public override FractionType fraction
		{
			get { return FractionType.None; }
		}

		public MapObjectInfo obj;
        public string ActiveData = string.Empty;

        /// <summary>
        /// Определяет возвожно ли проходить сквозь объект
        /// </summary>
        public bool isCanMoveOverObject = false;

        public override bool CanPushByPerson
        {
            get { return isCanMoveOverObject; }
        }

        public void Init(MapObjectInfo obj, Vector3 position, Map map)
		{
            TargetName = obj.name;
            BundleName = obj.bundleName;
			eType = EntityType.Object;
            ActiveData = obj.ActiveData;

			this.obj = obj;
			this.position = position;
			this.map = map;

            Radius = obj.radius;
            HitRadius = Radius;
            Height = obj.height;

            ServerConstants.MapObjectConstant mapObjectConstant;
            if (!string.IsNullOrEmpty(TargetName) && MapObjectConstants.TryGetValue(TargetName, out mapObjectConstant))
            {
                MaxHealth = mapObjectConstant.Health;
                HitRadius = mapObjectConstant.HitRadius;
            }

			Id = obj.id;

			Health = MaxHealth;
			Energy = MaxEnergy;
            Level = 1;

			if (IsLiveObject && Map.Edge.StoneLoots != null)
                Loots = LootLists.Instance.GetLoots(Map.Edge.StoneLoots);

            RespawnCooldownSec = map.ObjectsRespawnCooldownSec;

            //m_characterType = IsLiveObject ? CharacterType.Res : CharacterType.None;
        }

		[RPC("ENT")]
		public void EntityReadyRequest(InstancePerson instancePerson)
		{
			if (view != null)
				SendPlayAnimationStateResponse(instancePerson);

			OnEntityReadyRequest(instancePerson);
		}

        protected void SendPlayAnimationStateResponse(InstancePerson person)
        {
            SendRpc(person.ClientRpc.PlayStateResponse, CurrentAnimState);
        }

        public int RespawnCooldownSec = ServerConstants.Instance.DefaultObjectsRespawnCooldownSec;
		public DateTime RespawnTime = DateTime.MaxValue;

		public MyRect Transparency;//TODO_Deg для туториала

		public override void SetHealth(MapTargetObject attacker, int value, bool crit = false)
		{
			if (IsDead)
				return;

			Health = value;

			if (IsDead)
			{
			    RespawnTime = TimeProvider.UTCNow.AddSeconds(RespawnCooldownSec);
				ServerRpcListener.Instance.SendRpc(map.ClientRpc.DestroyObjectResponse, Id, true);

			    if (Transparency != null)
				    ServerRpcListener.Instance.SendUpdateMapTransparencyResponse(map, Transparency, true);

                var personAttacker = attacker as PersonBehaviour;
                if (personAttacker != null && Loots != null)
			    {
                    //var bigPersonHeroPower = (personAttacker.owner.HeroPower > personAttacker.owner.syn.Map.Edge.GSMax + 50) && !personAttacker.owner.syn.map.IsTutorial;
                    //Sos.Debug("разрушено " + TargetName);
				    var finalLoots = Loots.GetLootList(this, personAttacker.owner, loot => CheckCurrentLootChance(loot, personAttacker));
				    foreach (var finalLoot in finalLoots)
					    map.loots.Add(new MapLoot(finalLoot, this, personAttacker, position, map.GetLootPos(position)));
			    }
			}
		}

        public override bool Hit(MapTargetObject attacker, int damage, DamageType type, Elements atkElement, out bool crit, bool canMiss = true,
            bool resetCast = true, bool absoluteHit = false, bool autoAttack = false, CritType critType = CritType.AbilityAttack, HitEffectXmlData hitFxData = null)
        {
            crit = false;

            map.OnTargetOutDamage(attacker, this, damage);

            SetHealth(attacker, Health - damage);

			if (!IsDead)
			{
				PlayHitEffect(attacker, type, crit, hitFxData);
				attacker.OnHit(this, damage);
			}
			else
				attacker.OnKill(this, damage);

			return true;
            //ServerRpcListener.Instance.SendAddCritFrameResponse(this, -damage, TextFrameType.Damage);
        }

        public void Respawn()
		{
			if (!IsDead)
				return;

			RespawnTime = DateTime.MaxValue;
			Health = MaxHealth;

			ServerRpcListener.Instance.SendRpc(map.ClientRpc.CreateObjectResponse, Id);

			if (Transparency != null)
				ServerRpcListener.Instance.SendUpdateMapTransparencyResponse(map, Transparency, false);
		}

		public LootList Loots;

        public override bool CanCollision()
		{
			return Radius > 0;
		}

		public override string PersonalParam
		{
			get { return map.LevelParam; }
		}

        public override double AutoPricelPriority(MapTargetObject person)
        {
            return isTargetable ? AutoPricelPriority(person, 0.5) : -1;
        }
        
		public virtual void Explosion()
		{
            //скрываем объект на клиенте пр.: кусты
            ServerRpcListener.Instance.SendRpc(map.ClientRpc.DestroyObjectResponse, Id, IsLiveObject);
		}
        
	    public void RemoveEntity()
	    {
            // удаляем на клиенте
			ServerRpcListener.Instance.SendRpc(map.ClientRpc.RemoveEntity, eType, Id);
        }

        public void PublicDestroy()
        {
            // удаляем на клиенте как эффект
            //Sos.Debug("DestroyFxResponse " + Id);
            //ServerRpcListener.Instance.DestroyFxResponse(map, Id, Vector3.Zero);
        }

		public override void MyUpdate()
		{
			throw new NotImplementedException();
		}

		public override void UpdatePhysics()
		{
			throw new NotImplementedException();
		}

		public override ChangeTransformData GetTransformData()
		{
			throw new NotImplementedException();
		}

		public override ChangeTransformForceData GetTransformForceData()
		{
			throw new NotImplementedException();
		}
	}
}
