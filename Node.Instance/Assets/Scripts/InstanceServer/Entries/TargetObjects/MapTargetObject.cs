﻿using Assets.Scripts.InstanceServer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.Logic.Enums;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Common;
using UtilsLib;
using UtilsLib.NetworkingData;
using Batyi.TinyServer;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData;
using uLink;
using UtilsLib.Logic;
using Vector2 = System.Numerics.Vector2;
using Vector3 = System.Numerics.Vector3;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using LobbyInstanceLib.Networking;
using UtilsLib.low_engine_sharp.transport;

namespace Assets.Scripts.InstanceServer.Entries.TargetObjects
{
    public abstract class MapTargetObject //: uLink.MonoBehaviour
    {
        public static int objId = 2000000;

        public int Id;
        public bool isDestroyed;
        public virtual int Level { get; set; }
        public EntityType eType = EntityType.Entity;
        public PersonType personType = PersonType.None;
        public GameModeType GameMode = GameModeType.Exploration;
        public string TargetName;//имя префаба
        public string BundleName;//имя бандла
        public string NickName = string.Empty;//ник
        public virtual List<EntityPartInfo> EquippedWears { get; set; }
		public virtual List<EntityPartInfo> EquippedWeapons { get; set; }
        public abstract CharacterType characterType { get; }
        public virtual bool CanPushByPerson
        {
            get { return false; }
        }
        public virtual FractionType fraction { get; set; }
        public BehaviourType bType = BehaviourType.None;
		public DeathReasonType DeathReason = DeathReasonType.None;
        public bool bImmortality = false;
        public bool IsPossibleWalkInside = false;
        public bool isTargetable = true;

        public float Radius { get; set; }
        public float HitRadius { get; set; }

        public virtual float speed { get; }

        public abstract void MyUpdate();
        public abstract void UpdatePhysics();

        public string avatarName = string.Empty;
        public virtual string GetAvatar()
        {
            return avatarName;
        }

        public virtual void BeforeUpdatePhysics()
        {
            previousUpdatePosition = position;

            if (sentPositionChangeThisUpdate)
            {
                sentPositionChangeThisUpdate = false;
                SetLastSendPosition();
            }
        }

        public Vector2 directionMove = Vector2.Zero;

        internal virtual Elements AttackElement()
        {
            throw new NotImplementedException();
        }

        public List<MapEffect> effects = new List<MapEffect>();

        #region NetworkView

        public uLink.NetworkView view = null;

		#region SendRPC StronglyTyped

		public void SendRpc(Action rpc)
		{
			view.SendRpc(rpc.Method, rpc.Target);
		}

		public void SendRpc(Action<Connection> rpc)
		{
			view.SendRpc(rpc.Method, rpc.Target);
		}

		public void SendRpc<T1>(Action<T1> rpc, T1 v1)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1);
		}

		public void SendRpc<T1>(Action<T1, Connection> rpc, T1 v1)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1);
		}

		public void SendRpc<T1, T2>(Action<T1, T2> rpc, T1 v1, T2 v2)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2);
		}

		public void SendRpc<T1, T2>(Action<T1, T2, Connection> rpc, T1 v1, T2 v2)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2);
		}

		public void SendRpc<T1, T2, T3>(Action<T1, T2, T3> rpc, T1 v1, T2 v2, T3 v3)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3);
		}

		public void SendRpc<T1, T2, T3>(Action<T1, T2, T3, Connection> rpc, T1 v1, T2 v2, T3 v3)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3);
		}

		public void SendRpc<T1, T2, T3, T4>(Action<T1, T2, T3, T4> rpc, T1 v1, T2 v2, T3 v3, T4 v4)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4);
		}

		public void SendRpc<T1, T2, T3, T4>(Action<T1, T2, T3, T4, Connection> rpc, T1 v1, T2 v2, T3 v3, T4 v4)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4);
		}

		public void SendRpc<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> rpc, T1 v1, T2 v2, T3 v3, T4 v4, T5 v5)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5);
		}

		public void SendRpc<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5, Connection> rpc, T1 v1, T2 v2, T3 v3, T4 v4, T5 v5)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5);
		}

		public void SendRpc<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> rpc, T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5, v6);
		}

		public void SendRpc<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6, Connection> rpc, T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5, v6);
		}

		public void SendRpc<T1, T2, T3, T4, T5, T6, T7>(Action<T1, T2, T3, T4, T5, T6, T7> rpc,
			T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5, v6, v7);
		}

		public void SendRpc<T1, T2, T3, T4, T5, T6, T7>(Action<T1, T2, T3, T4, T5, T6, T7, Connection> rpc,
			T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5, v6, v7);
		}

		public void SendRpc<T1, T2, T3, T4, T5, T6, T7, T8>(Action<T1, T2, T3, T4, T5, T6, T7, T8> rpc,
			T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7, T8 v8)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5, v6, v7, v8);
		}

		public void SendRpc<T1, T2, T3, T4, T5, T6, T7, T8>(Action<T1, T2, T3, T4, T5, T6, T7, T8, Connection> rpc,
			T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7, T8 v8)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5, v6, v7, v8);
		}

		public void SendRpc<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> rpc,
			T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7, T8 v8, T9 v9)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5, v6, v7, v8, v9);
		}

		public void SendRpc<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, Connection> rpc,
			T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7, T8 v8, T9 v9)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5, v6, v7, v8, v9);
		}

		public void SendRpc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> rpc,
			T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7, T8 v8, T9 v9, T10 v10)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10);
		}

		public void SendRpc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, Connection> rpc,
			T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7, T8 v8, T9 v9, T10 v10)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10);
		}

		public void SendRpc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> rpc,
			T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7, T8 v8, T9 v9, T10 v10, T11 v11)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11);
		}

		public void SendRpc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, Connection> rpc,
			T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7, T8 v8, T9 v9, T10 v10, T11 v11)
		{
			view.SendRpc(rpc.Method, rpc.Target, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11);
		}

		#endregion

        #endregion

        public Map map { get; protected set; }

        public Vector3 position { get; set; }

        public virtual Vector3 GetActualPosition
        {
            get
            {
                return position;
            }
        }

        public bool isMotionControlledByServer;
        public bool IsFly = false;

		private bool isAirway = false;
		//Может использовать воздушный путь (не падать в ямы и пропасти)
		public virtual bool IsAirway
		{
			get { return isAirway || bCharging || bRapidRetreat; }
			set { isAirway = value; }
		}

        //двигает других, но сам при этом не двигается
        public bool StaticBody;

        public Vector3 Speed;
        public Vector3 Force;

        public Vector2 direction = new Vector2(1, 0);

        public virtual int Health { get; set; }
        public int MaxHealth;

        public virtual int Energy {get; set;}
		public int MaxEnergy = 100;

        public int Stamina;
        public int MaxStamina = 100;

        public bool IsDead
		{
			get { return Health <= 0 && MaxHealth > 0; }
		}

        public virtual bool bInSafeZone{  get;  set; }

        public bool bSilence = false;
        public bool bStun = false;
        public bool bRoot = false;
        public bool CanMove
        {
            get
            {
                return !bStun && !bRoot && !IsDead &&
                    (SpellCastAction == null || SpellCastAction.CanMove());
            }
        }

        public bool IsLiveObject
        {
            get { return MaxHealth > 0; }
        }

		public float Height;

		public Vector3 center
		{
			get { return position + new Vector3(0, Height / 2, 0); }
		}

        public Vector3 actualCenter
        {
            get
            {
                return GetActualPosition + new Vector3(0, Height / 2, 0);
            }
        }

        public float ScaleMult = 1.0f;

		public abstract string PersonalParam { get; }

		public int CurrentAnimState;
		public bool move;
		public Vector3 movePos;

		public virtual bool CanCollision()
		{
			return true;
		}

		public Func<InstancePerson, bool> FuncVisibleFor;

        public HealthType healthType;
        public HealthTypeVisual healthTypeVisual;
        public bool influenceImmunitet;

        public float maxLifeTime;
        private float remainingLifeTime;
        public float RemainingLifeTime
        {
            get
            {
                return Math.Max(remainingLifeTime - (Time.time - lifeTimeStamp), 0);
            }
        }
        private float lifeTimeStamp;

        public void SetLifeTime(float maxLifeTime, float remainingLifeTime)
        {
            this.maxLifeTime = maxLifeTime;
            this.remainingLifeTime = remainingLifeTime;

            lifeTimeStamp = Time.time;
        }

        //Виден ли объект для игрока
        public virtual bool VisibleFor(InstancePerson person)
		{
			return FuncVisibleFor == null || FuncVisibleFor(person);
		}

        public virtual void AddHealth(MapTargetObject attacker, int value, bool selfRegeneration = false, bool canCrit = true)
		{
            HealResult healResult = StatsFormulas.Instance.CalculateHeal(attacker, this, value, selfRegeneration, canCrit);
            int healValue = (int)healResult.HealValue;
            SetHealth(attacker, Health + healValue);

            if (!selfRegeneration && healValue != 0)
                ServerRpcListener.Instance.SendAddCritFrameResponse(attacker, this, healValue, TextFrameType.Healing, Elements.Neutral, DamageType.Melee);
        }

		public virtual void SetHealth(MapTargetObject attacker, int value, bool crit = false)
		{

		}

		public void PlayHitEffect(MapTargetObject attacker, DamageType DamageType, bool crit, HitEffectXmlData hitFx = null)
		{
            int upgrade;
			float height;
			var hitEffect = InstanceGameManager.Instance.EffectAlgorithm.GetHitEffect(this, attacker, crit, out upgrade, out height);

            if (hitFx != null)
            {
                hitEffect = hitFx.GetEffect(crit, attacker);
            }

            if (hitEffect != EffectType.None)
			{
                //Нам необходимо передать на клиент правильные позиции hitEffectPos и hitEffectEndPos, чтобы эффект был правильно повёрнут
                Vector3 hitEffectPos = position;
                Vector3 hitEffectEndPos = position;

                //Если по цели проходит рукопашный урон или урон дальнего боя, то будем сдвигать эффект из центра цели по направлению к атакующему игроку
                //Данная корректировка возымеет действие только на эффект MonsterHit и MonsterHitCritical (а так же на все новые эффекты сделанные по их схеме)
                //Так как для других эффектов попадания клиент игнорирует позиции присланные сервером и выставляет эффект в середину цели
                if (DamageType == DamageType.Melee || DamageType == DamageType.Range)
                {
                    Vector3 direction = (attacker.GetActualPosition - GetActualPosition).normalized();
                    hitEffectPos += direction * Radius;
                }
                
                //Высоту цели будем брать равной высоте атакующего игрока, чтобы эффект не задирался, когда игрок атакует высокую цель
                hitEffectEndPos.Y += attacker.Height / 2f;
                hitEffectPos.Y += attacker.Height / 2f;
                
                ServerRpcListener.Instance.SendCreateFxResponse_Missile(map, Id, hitEffect, hitEffectPos, hitEffectEndPos, 0, upgrade, ScaleMult);
            }

            var sound = InstanceGameManager.Instance.SoundAlgorithm.GetHitSound(this);
			ServerRpcListener.Instance.SendPlaySoundResponse(this, sound);
		}

		internal virtual void ResurrectNow()
		{

		}


		public virtual bool Hit(MapTargetObject attacker, int damage, DamageType type, Elements atkElement, out bool crit, bool canMiss = true, bool resetCast = true, 
            bool absoluteHit = false, bool autoAttack = false, CritType critType = CritType.AbilityAttack, HitEffectXmlData hitFxData = null)
        {
            crit = false;
            return true;
        }
        
        public virtual void OnInDamage(MapTargetObject attacker, ref int damage, bool resetCast)
        {
        }

        public virtual void OnOutDamage(StatContainer StatContainer, int damage, bool crit, ref double mult, DamageType type, bool autoAttack = false)
        {
        }

        public virtual void OnDodge()
        {
        }

        public void SetDamageAnimation(bool isCriticalDamage)
        {
            SendRpc(map.ClientRpcUnr.SetDamageAnimation, isCriticalDamage);
        }

        protected bool CheckCurrentLootChance(ILoot loot, PersonBehaviour personAttacker)
        {
            if (personAttacker.isDestroyed)
            {
                ILogger.Instance.Send("MapTargetObject: CheckCurrentLootChance some is null");
                return false;
            }

            var chanceLoot = loot.FChance.IntCalc(personAttacker.GetStatContainer(), this.GetStatContainer());
            //ILogger.Instance.Send(string.Format("Monster lootName:{0}, bigPersonHeroPower: {1}, chanceLoot: {2}", loot.LootName, bigPersonHeroPower, chanceLoot), ErrorLevel.warning);
            return FRRandom.CheckPercentChance(chanceLoot);
        }

        public virtual void SetEnergy(int p)
		{

		}

	    protected StatContainer StatContainer = null;
        public StatContainer GetStatContainer()
        {
            return StatContainer;
        }

        public void SetStatContainer(StatContainer container)
        {
            StatContainer = container;
        }
        public int HeroPower
        {
            get {
                if (StatContainer != null)
                    return (int)StatContainer.GetStatValue("HPOW");
                else
                    return 0;
            }
        }

        public bool bOnMap = true;

        public bool CanAttackTarget(MapTargetObject target, bool checkLiveObject = true)
        {
            var teamRules = TeamColor != PlayerTeam.None;
            return CanAttackTarget(target, checkLiveObject, teamRules);
        }
        public bool CanAttackTarget(MapTargetObject target, bool checkLiveObject, bool teamRules)
        {
            if (target == null || target == this || target.IsDead || !target.isTargetable || !target.CanCollision() || !target.bOnMap)
                return false;

            if (target.bImmortality)
                return false;

			if (checkLiveObject && !target.IsLiveObject)
				return false;

            if (characterType == CharacterType.Person && target.characterType == CharacterType.Person && !Map.Edge.PVP)
                return false;

            if (characterType == CharacterType.Mob && target.characterType == CharacterType.Mob &&
                (TeamColor == PlayerTeam.None || target.TeamColor == PlayerTeam.None))
                return false;

			if (characterType == CharacterType.NPC || target.characterType == CharacterType.NPC)
				return false;

            if (teamRules && TeamColor != PlayerTeam.None && TeamColor == target.TeamColor)
                return false;

            return true;
        }

        public virtual bool IsTargetAlly(MapTargetObject target)
        {
            if (target == null)
                return false;

            if (this == target)
                return true;

			if (TeamColor == PlayerTeam.None && target.TeamColor == PlayerTeam.None)
			{
				//Мобы без команды - союзники друг другу
				if (characterType == CharacterType.Mob && target.characterType == CharacterType.Mob)
					return true;

				return false;
			}

			return TeamColor == target.TeamColor;
        }

		//Автоприцел
		public Vector3 AutoPricel(Vector3 pos, float AutoPricelR, bool isNeedApplyTargetLock, bool bAttackResponse = false, bool selectStones = true, bool bChangeDirection = true)
		{
			MapTargetObject target;
			return AutoPricel(pos, AutoPricelR, out target, isNeedApplyTargetLock, bAttackResponse, selectStones, bChangeDirection);
		}

		public virtual Vector3 AutoPricel(Vector3 pos, float AutoPricelR, out MapTargetObject target, bool isNeedApplyTargetLock, bool bAttackResponse = false, bool selectStones = true, bool bChangeDirection = true)
		{
            Vector3 targetDir;
            target = map.GetNearestTarget(this, AutoPricelR, selectStones);//, out targetDir);

            if (target != null)
                targetDir = (target.center - center).normalized();
            else
                targetDir = direction.Vector2ToVector3();

            return targetDir;
		}

		public virtual double AutoPricelPriority(MapTargetObject person)
        {
            return AutoPricelPriority(person, 1.1);
        }

        public double AutoPricelPriority(MapTargetObject person, double MultBonus)
        {
            //int Angle = 30;
            //float DistMax = 10.0f;
            //IArea area = new SectorArea(person.position, person.direction, Angle, DistMax, true);
            //var dist =  (position - person.position).magnitude();
            //if (area.InArea(position))
            //    return (DistMax - dist) * MultBonus;
            //return 0;

            //построить картинку в вольфрам альфа
            //plot 100 / (sqrt((x - 1) * (x - 1) + y * y) + 4) + 2 / Max(| atan2(y, x) |, pi / 18 +| y | *0.02) + 3 * Max(Min(x, 1.5), 0.1), x = -10..10, y = -10..10

            var dir = position - person.position;
            var dir2d = new Vector2(dir.X, dir.Z);

            //double r = dir2d.magnitude;
            //Sin = (person.direction X (target.position - person.position).normalize)

            var toTarget = dir2d.normalized();
            double sinAngle = person.direction.X * toTarget.Y - person.direction.Y * toTarget.X;
            double angle = Math.Asin(sinAngle);
            double x = dir2d.X * person.direction.X + dir2d.Y * person.direction.Y;
            double y = dir2d.X * person.direction.Y - dir2d.Y * person.direction.X;
            double res = 100 / (Math.Sqrt((x - 1) * (x - 1) + y * y) + 4) + 2 / Math.Max(Math.Abs(angle), 3.14 / 18 + Math.Abs(y) * 0.02) + 3 * Math.Max(Math.Min(x, 1.5), 0.1);
            //if (person.TargetLockObject == this)
            //    res *= ServerConstants.Instance.TapPriorityMultiply;
            return res * MultBonus;
        }

        public virtual void OnHit(MapTargetObject target, int damage)
        {
        }

		public virtual void OnKill(MapTargetObject target, int damage)
        {
        }

        public void SendSetAnimatorParameterBoolResponse(string parameterName, bool newValue)
        {
            SendRpc(map.ClientRpc.SetAnimatorParameterBool, parameterName, newValue);
        }

        public void SendSetAnimatorParameterBoolResponse(PersonBehaviour person, string parameterName, bool newValue)
        {
            SendRpc(person.ClientRpc.SetAnimatorParameterBool, parameterName, newValue);
        }

        public void SendSetAnimatorParameterTriggerResponse(string parameterName)
        {
            SendRpc(map.ClientRpc.SetAnimatorParameterTrigger, parameterName);
        }

        public void SendSetAnimatorParameterTriggerResponse(PersonBehaviour person, string parameterName)
        {
            SendRpc(person.ClientRpc.SetAnimatorParameterTrigger, parameterName);
        }

		public void SetTransparency(float alpha)
		{
			SendRpc(map.ClientRpc.SetUpTransparencyResponse, alpha);
		}

		public void SetTransparency(InstancePerson person, float alpha)
		{
			SendRpc(person.ClientRpc.SetUpTransparencyResponse, alpha);
		}

		public virtual void SetGameMode(GameModeType gameMode, ActualPlaceTitle title = ActualPlaceTitle.None)
		{
		}

		public AbilityXmlData SpellCastAction;


		#region PersonBehaviour.Archer
		public bool bRapidRetreat;
		#endregion

		#region PersonBehaviour.Warrior
		public bool bCharging;
		#endregion

		public virtual void RemoveEnergy(AbilityXmlData data)
		{
		}

		public virtual void SetCooldown(AbilityXmlData data)
		{
		}

		public virtual AbilityErrors CheckEnergyCooldown(AbilityXmlData data)
		{
			return AbilityErrors.Success;
		}

        public virtual void KnockBack(Vector3 dir, float time, float dist, bool force = false, bool resetCast = false)
        {
            if (resetCast)
            {                
                SpellCastAction?.Reset();
            }
        }

        /// <summary>
        /// Время в течении которого планируется переопределять вектор движения
        /// Различные умения могут двигать персонажа специфическим образом, вот для них этот механизм и придуман
        /// </summary>
        protected float overrideSpeedTime;

        /// <summary>
        /// Глобальное время до которого нужно переопределять функцию скорости
        /// </summary>
        protected float overrideSpeedTargetTime;

        /// <summary>
        /// Тип умения, которое перехватило управление персонажем
        /// </summary>
        protected AbilityType overrideSpeedAbilityType;

        protected Func<float, Vector3> SpeedOverrideFunction;

        protected bool isClientMustInvertMoveMode = false;

        protected virtual bool IsCanApplySpeedOverride
        {
            get
            {
                return !(bRoot);
            }
        }
        
        public void SetSpeedOverrideFunction(float overrideTime, AbilityType controlAbilityType, Func<float, Vector3> overrideFunction, bool isNeedInvertMoveModeOnClient = false, float ySpeed = 0f)
        {
            if (isMotionControlledByServer && overrideSpeedAbilityType == controlAbilityType && SpeedOverrideFunction != null || !IsCanApplySpeedOverride)
            {
                return;
            }
            overrideSpeedTime = overrideTime;
            overrideSpeedTargetTime = Time.time + overrideTime;
            overrideSpeedAbilityType = controlAbilityType;
            SpeedOverrideFunction = overrideFunction;
            isMotionControlledByServer = true;
            isClientMustInvertMoveMode = isNeedInvertMoveModeOnClient;
            Speed.Y = Speed.Y + ySpeed;
        }

        public virtual void ResetSpeedOverride()
        {
            overrideSpeedTargetTime = 0;
            SpeedOverrideFunction = null;
            isClientMustInvertMoveMode = false;
            isMotionControlledByServer = false;
        }

        public bool IsMoveCanBeInterrupted()
        {
            if (!IsFly && (overrideSpeedTargetTime > Time.time && overrideSpeedAbilityType == AbilityType.Dash2 || overrideSpeedTargetTime < Time.time))
            {
                return true;
            }
            return false;
        }

        public virtual void InterruptDash(bool CanMoveInCastTime)
        {

        }


        public MapTargetObject Target = null;
		public MapTargetObject TargetLockObject = null;

		public PlayerTeam TeamColor = PlayerTeam.None;

		public event Action<InstancePerson> EntityReady;
		public void OnEntityReadyRequest(InstancePerson person)
		{
			EntityReady?.Invoke(person);
		}

		public abstract ChangeTransformData GetTransformData();
		public abstract ChangeTransformForceData GetTransformForceData();

		protected bool sentPositionChangeThisUpdate;
		protected Vector3 lastSentPosition;
		protected Vector2 lastSentDirection;
		protected int lastSentSamePositionCount;
		protected const int SamePositionSendCount = 2;

		protected bool CheckNeedSendPositionChange()
		{
			if (sentPositionChangeThisUpdate)
				return true;

			// для сглаживания движения на клиенте надо отправить ему две последних точки
			if (IsPositionChanged())
			{
				lastSentSamePositionCount = 0;
			}
			else
			{
				if (lastSentSamePositionCount < SamePositionSendCount)
				{
					lastSentSamePositionCount++;
				}
				else
				{
					return false;
				}
			}

			sentPositionChangeThisUpdate = true;
			return true;
		}

		protected virtual bool IsPositionChanged()
		{
			return lastSentPosition != position || lastSentDirection != direction;
		}

		protected virtual void SetLastSendPosition()
		{
			lastSentPosition = position;
			lastSentDirection = direction;
		}

		protected Vector3 previousUpdatePosition;

		protected bool CheckTargetWasOutOfRangeLastUpdate(MapTargetObject target, float ScopeR2)
		{
			var prevPositionSqrDist = Helper.GetSqrDistanceIgnoreY(target.position, previousUpdatePosition);
			bool targetWasOutOfRange = prevPositionSqrDist >= ScopeR2;
			if (!targetWasOutOfRange)
			{
				// возможно перс/моб/нпц/.. тоже одновременно с монстром сместился навстречу, посмотрим его старую позицию
				var prevTargetPositionSqrDist = Helper.GetSqrDistanceIgnoreY(target.previousUpdatePosition, previousUpdatePosition);
				targetWasOutOfRange = prevTargetPositionSqrDist >= ScopeR2;
			}

			return targetWasOutOfRange;
		}

		public const float SEND_RATE = 0.08f;
		private float updateAllTime;
		protected int updateAll;

        public virtual void SendChangeTransformResponse()
        {
        }

        public bool CheckLineForWalk(Vector3 start, Vector3 end)
        {
            return GeometryAlgorithm.CheckPoints(map, Helper.GetCell(start.X), Helper.GetCell(start.Z), Helper.GetCell(end.X), Helper.GetCell(end.Z), true);
        }

        /// <summary>
        /// только для проверок высоких препядствий, работает очень не точно
        /// </summary>
        /// <param name="verticalRadius">если разница высот больше этого раиуса то вернёт false</param>
        /// <returns></returns>
        public bool CheckLineInaccurate(Vector3 start, Vector3 end, float verticalRadius = -1, bool checkOnlySpecialWalls = false, bool checkShrub = false)
        {
            if (verticalRadius > 0)
            {
                if (Math.Abs(end.Y - start.Y) > verticalRadius)
                {
                    return false;
                }
            }
            for (int i = 0; i < map.DefenceWalls.Count; i++)
            {
                if (map.DefenceWalls[i].team == PlayerTeam.None || map.DefenceWalls[i].team != TeamColor)
                {
                    if (map.DefenceWalls[i].CheckIntersect(start, end))
                        return false;
                }
            }
            if (checkOnlySpecialWalls)
                return true;
            return GeometryAlgorithm.CheckPointsInaccurate(map, Helper.GetCell(start.X), Helper.GetCell(start.Z), start.Y, Helper.GetCell(end.X), Helper.GetCell(end.Z), end.Y, checkShrub);
        }


        public virtual void MarkPersonsForUpdate()
		{
			if (updateAllTime < Time.time)
			{
				updateAll++;
				updateAllTime += SEND_RATE;
			}
			float nearestScopeR2 = Map.Edge.VisibleScopeRadiusNearest * Map.Edge.VisibleScopeRadiusNearest;
			float bufferZoneScopeR2 = Map.Edge.VisibleScopeRadiusBufferZone * Map.Edge.VisibleScopeRadiusBufferZone;
			foreach (var p in map.persons)
			{
				p.SendChangeTransformRPCMode = ChangeTransformReliability.NotSet;
				p.SendRPC = true;

				if (p == this)
					continue;

				var sqrDist = Helper.GetSqrDistanceIgnoreY(p.position, position);
				bool personIsInNearestScope = sqrDist < nearestScopeR2;
				bool personIsInBufferZone = sqrDist < bufferZoneScopeR2;
				bool forceReliablePosUpdate = false;
				p.SendRPC = personIsInNearestScope;

				if (personIsInNearestScope)
				{
					// сейчас в ближней сфере апдейта.
					// если моб раньше был за границей сферы отсылки команд, а теперь зашёл в неё,
					// обязательно обновим позицию на клиенте
					forceReliablePosUpdate = CheckTargetWasOutOfRangeLastUpdate(p, nearestScopeR2);
				}
				else
				{
					if (personIsInBufferZone)
					{
						// сейчас в средней "буферной" зоне
						// обновляем положение либо по таймеру, либо при пересечении границы
						forceReliablePosUpdate = CheckTargetWasOutOfRangeLastUpdate(p, bufferZoneScopeR2);
						if (!forceReliablePosUpdate)
						{
							const float updateInBufferZoneEverySec = 1.0f;
							const int ticksToPass = (int)(updateInBufferZoneEverySec / SEND_RATE);
							if (updateAll % ticksToPass == 0)
								p.SendChangeTransformRPCMode = ChangeTransformReliability.UnreliableForced;
						}
					}
					else
					{
						// сейчас очень далеко. обновляем только по таймеру
						const float updateInFarZoneEverySec = 10.0f;
						const int ticksToPass = (int)(updateInFarZoneEverySec / SEND_RATE);
						if (updateAll % ticksToPass == 0)
							p.SendChangeTransformRPCMode = ChangeTransformReliability.UnreliableForced;
					}
				}

				if (forceReliablePosUpdate)
				{
					p.SendChangeTransformRPCMode = ChangeTransformReliability.ReliableForced;
				}
			}
		}
    }
}
