﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Common;
using UtilsLib.Logic.Enums;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib;
using Assets.Scripts.Utils.Logger;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries.XmlData.AttackDatas;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using LobbyInstanceLib.Networking;
using UtilsLib.DegSerializers;
using UtilsLib.NetworkingData;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.BatiyScript;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData;
using Assets.Scripts.InstanceServer.Entries.Person;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Entries.TargetObjects
{
	public class MonsterBehaviour : MapTargetObject
	{
        public override bool CanPushByPerson
        {
            get
            {
                return !(Data.MonsterType == MonsterType.Boss || Data.MonsterType == MonsterType.Champion || 
                    Data.MonsterType == MonsterType.SafeZone && (map.IsBattleground||map.IsBattlegroundArena));
            }
        }

		public override CharacterType characterType
		{
			get { return Data.CharacterType; }
		}

		public override FractionType fraction
		{
			get { return FractionType.None; }
		}

		public Vector3 PridePosition;

		public float AggroRadius = 2;
        public float VisibleRadius = 2;

		public float PrideRadius = 6;
        public Vector3 backPosition;
        public float RecommendedAttackRadius = 100;
        public float RetreatRadius = -1;
        public float DefenceVisibleRadius = 300;
        public float DefenceAggroRadius = 300;

        public float speedBase;
        public override float speed
        {
            get
            {
                return speedBase;
            }
        }

        public string AreaName = "";
        public bool m_bMoveable = true;
        public bool bSendCreateEntiy = true;
        public string ActiveData;//maximpr можно еще раз подумать и избавится от этого поля
        public AdditionalMoveCost MoveCost = ServerPathFinderAlgorithm.MonsterMoveCost;
        public AdditionalMoveCost MoveCostAngry = ServerPathFinderAlgorithm.MonsterAngryMoveCost;
	    public CheckSpawnPoint IsGoodSpawnPoint = ServerPathFinderAlgorithm.IsGoodSpawnPointForCommonMonster;

        public bool InSafeZone
        {
            //работает только для ботов
            get { return characterType == CharacterType.Person && map.IsSafeZone(position); }
        }

	    public bool TargetInLineOfSight = true;

	    public bool IsTargetMove
	    {
	        get
	        {
	            if (Target == null)
	                return false;
	            return Target.position != OldTargetPos;
            }
        }

	    public bool bMoveable
        {
            set
            {
                if (!value)
                    PathClear();
                m_bMoveable = value;
            }
            get { return m_bMoveable; }
        }
		public bool bRotatable = true;

	    public MonsterBehaviour ParentSummoner { get; set; }
	    
	    public List<MonsterBehaviour> ChildrenSummons = new List<MonsterBehaviour>();

		public bool IsRealBoss
		{
			get { return Data.MonsterType == MonsterType.Boss && ParentSummoner == null; }
		}

        /// <summary>
        /// Буффер для булевых параметров аниматора, которые необходимо переслать на клиент
        /// </summary>
        Dictionary<string, bool> boolParametersToSend = new Dictionary<string, bool>();

        /// <summary>
        /// Буффер для триггеров аниматора, которые неоходимо перслать на клиент
        /// </summary>
        List<string> triggerParametersToSend = new List<string>();

        public MonsterBehaviour(Map map, int id = 0)
		{
			this.map = map;

            if (id != 0)
                Id = id;
            else
                Id = objId++;

		    view = uLinkHelper.CreateView(this);
        }

        public bool InitPosition(bool oldSpawn, Func<Vector3> getPoint = null)
        {
            Vector3 pos;
            if (oldSpawn || characterType == CharacterType.Person) //TODO_Deg для ботов
            {
                PridePosition = position = getPoint == null ? map.GetEmptyOrRandomRespPoint(AreaName) : getPoint();
                return true;
            }
            else
            {
                if (map.GetRandomPoint(AreaName, out pos))
                {
                    PridePosition = position = pos;
                    return true;
                }
            }
            return false;
        }

		public MonsterData Data;
        public List<MonsterBehaviourPart> attacks;
        public MonsterBehaviourPart currentAttack;
        public void SetCurrentAttack(MonsterBehaviourPart nextAttack)
        {
            if (currentAttack != null && currentAttack != nextAttack)
            {
                currentAttack.Interrupt();
            }

            currentAttack = nextAttack;
        }

        public MonsterBehaviourPart common;

        public AggroList aggroList;
        public string AssistGroup = null;

        public string TechnicalName;

		public void Init(MonsterData data, string lootName, int level, string startPoint, string replaceTechnicalName)
		{
            updateAll = FRRandom.Next(0, 50);
			var deltaVec = new Vector3(float.Epsilon);
			previousUpdatePosition = lastSentPosition = position + deltaVec; // чтобы последняя позиция отличалась от текущей и на клиент ушла инфа о положении если станет надо

            if (data == null)
            {
                throw new ArgumentNullException("data == null");
            }

			timeforDeadAnimSec = data.DeadAnimTime;

			personType = data.PersonType;
			DeathReason = data.DeathReason;

			Level = level;
            Loots = LootLists.Instance.GetLoots(lootName);

			Data = data;
			StatContainer = new MonsterStatContainer(this);
            ((MonsterStatContainer)StatContainer).ResetCalcStats();

			TargetName = Data.Uniq;
			AreaName = startPoint;

            healthType = Data.MonsterHealthType;
            healthTypeVisual = Data.MonsterHealthTypeVisual;
            influenceImmunitet = Data.InfluenceImmunitet;
            isTargetable = Data.IsTargetable;

            if (replaceTechnicalName != null)
                TechnicalName = replaceTechnicalName;
            else
                TechnicalName = Data.TechnicalName;

            avatarName = Data.AvatarName;

			IsAirway = Data.MoveType == MoveType.Fly;

			AggroRadius = Data.AggroRange;
            VisibleRadius = Data.VisibleRadius;
			PrideRadius = Data.PrideRadius;

			Radius = Data.Size;
            HitRadius = Radius;
			Height = Data.Height;

			bRotatable = Data.bRotatable;
			state = MonsterState.FindTarget;

			ScaleMult = Data.ScaleMult;

			ResurrectTimeSec = ServerConstants.Instance.MonsterDefaultResurrectTime;
            attacks = new List<MonsterBehaviourPart>();
            
            foreach (var at in Data.Attacks)
            {
                var copy = at.Copy();
                copy.monster = this;
                attacks.Add(copy);
                copy.Init();
            }

            if (common == null)
            {
                common = new CommonMonster();
                common.monster = this;
                attacks.Add(common);
                common.Init();
            }

            speedBase = (float)StatContainer.GetStatValue("MoveSpeedReal");
            bMoveable = speedBase > 0;

			//MaxHealth = (int)StatContainer.GetStatValue("HP");

			Health = MaxHealth;
			Energy = MaxEnergy;

			//Добавим везде радиус персонажа на всякий случай

			if (Data.RecommendedAttackRadius !=-1)
                RecommendedAttackRadius = Data.RecommendedAttackRadius + Radius + PersonBehaviour.PersonRaduis; 
            else
            {
                foreach (var at in Data.Attacks)
                {
                    if (RecommendedAttackRadius > at.MaxApplyRadius)
                        RecommendedAttackRadius = at.MaxApplyRadius;
                }
                RecommendedAttackRadius += Radius + PersonBehaviour.PersonRaduis;
            }

            if (Data.RetreatRadius == -1)
                RetreatRadius = RecommendedAttackRadius - 1.5f;
            else
                RetreatRadius = Data.RetreatRadius + Radius + PersonBehaviour.PersonRaduis;

            DefenceVisibleRadius = Data.DefenceVisibleRadius + Radius + PersonBehaviour.PersonRaduis;
            DefenceAggroRadius = Data.DefenceAggroRadius + Radius + PersonBehaviour.PersonRaduis;

            SetCurrentAttack(null);

            var statContainer = GetStatContainer();
            if (statContainer.GetStatValue(Stats.BodyElement) <= 0)
                statContainer.AddStat(Stats.BodyElement, (int)data.Element);

            //неподвижные монстры с радиусом должны учавстваовать в рассталкиваниях, например башни
            if (Radius > 0 && !bMoveable)
                StaticBody = true;

            //персонажи не расталкиваются
            if (characterType == CharacterType.Person)
                StaticBody = true;
        }

        
		[RPC("ENT")]
		public void EntityReadyRequest(InstancePerson instancePerson)
		{
            //SendPlayAnimationStateResponse(info.sender);
            if (personType != PersonType.None)//костыли для Игоря, нужно для лучников в туториале
                SendRpc(instancePerson.ClientRpc.StartMoveResponse, move, movePos);

			if (personType != PersonType.None)
			{
				SendRpc(instancePerson.ClientRpc.SetGameModeResponse, GameMode, ActualPlaceTitle.None);

				if (EquippedWeapons != null && EquippedWeapons.Count > 0)
					SendRpc(instancePerson.ClientRpc.UpdateEquipmentResponse, GameMode, EquippedWeapons);
			}

			SendColliderEnable(instancePerson);
            if (MaxHealth > 0)
                SendRpc(instancePerson.ClientRpc.SetHealth, Health, MaxHealth);

			foreach (var effect in effects)
				ServerRpcListener.Instance.SendCreateFxResponse(instancePerson, effect);

            foreach (var effect in map.effects)
                if (effect.ownerId == Id)
                    ServerRpcListener.Instance.CreateFxResponse_Regular(instancePerson, effect.id, effect.ownerId, EntityType.Entity, effect.GetFxId(instancePerson.syn), effect.position, 0, effect.scale);

            instancePerson.OnMonsterEntityReady(this);

            OnEntityReadyRequest(instancePerson);
        }

		public PersonBehaviour TargetPerson
		{
			get { return Target as PersonBehaviour; }
			set { Target = value; }
		}

        public Vector3 OldTargetPos;
        public MonsterState state;
	    private float backToPrideRadius=2;

		DateTime nextRecalc;
		public DateTime nextAttack;

        public float nextWait;
        public DateTime endWait;

        public float Distance(Vector3 to)
		{
			var x = position.X - to.X;
			var z = position.Z - to.Z;
			return (float)Math.Sqrt(x * x + z * z);
		}

        public bool InRadius(Vector3 to, float r)
        {
            var x = position.X - to.X;
            var z = position.Z - to.Z;
            return (x * x + z * z) < (r * r);
        }

        public bool InRadius(Vector3 from, Vector3 to, float r)
        {
            var x = from.X - to.X;
            var z = from.Z - to.Z;
            return (x * x + z * z) < (r * r);
        }

        public bool InPride(Vector3 point)
        {
            //if (map.IsSafeZone((int)(point.X / Helper.VoxelSize), (int)(point.Z / Helper.VoxelSize)))
            //	return false;

            var x = PridePosition.X - point.X;
            var z = PridePosition.Z - point.Z;
            return (x * x + z * z) < (PrideRadius * PrideRadius);
        }

		private byte sessionId;

		public void AddToAgroList(MapTargetObject target, int aggro)
		{
            if (aggroList != null && target != null)
                aggroList.AddToAggroList(target, aggro);

            Target = target;
		}

        public void OnTargetDie(int targetId)
        {
			if (aggroList != null && aggroList.CanRemoveTarget)
				aggroList.Remove(targetId);

            if (TargetPerson != null && TargetPerson.Id == targetId)
                TargetPerson = null;
        }

        public bool IsNearby(MapTargetObject target)
        {
			var person = target as PersonBehaviour;
			if (person != null && person.owner.bInSafeZone)
				return false;

			if (target == Target)
                return InRadius(target.position, VisibleRadius) && InRadius(target.position, PridePosition, DefenceVisibleRadius);
            else
                return InRadius(target.position, AggroRadius) && InRadius(target.position, PridePosition, DefenceAggroRadius);
        }

        public bool MoveToPoint( float x, float z, float timelifePath, float radius = 0, AdditionalMoveCost moveCost = null)
        {
            var pathFindData = GetPathFindData();
            pathFindData.EndX = x;
            pathFindData.EndZ = z;
            pathFindData.timelifePath = timelifePath;
            pathFindData.radius = (int)(radius / Helper.VoxelSize);
            pathFindData.moveCost = moveCost;
            pathFindData.pointLimit = ServerPathFinderAlgorithm.iMaxPointSearched;

            //if (characterType == CharacterType.Person)
            //    pathFindData.altitude = 5;
            //else
                pathFindData.altitude = Helper.DELTA_HEIGH_WALK_MOB;

            return MoveToPoint(path, pathFindData);
        }

        public PathFindData GetPathFindData()
        {
            var pathFindData = new PathFindData();
            pathFindData.StartX = position.X;
            pathFindData.StartZ = position.Z;
            pathFindData.moveCost = MoveCostAngry;
            pathFindData.map = map;
			pathFindData.airway = IsAirway;
            return pathFindData;
        }

        public bool MoveToPoint(MapPath path, PathFindData pathFindData)
        {
            if (!bMoveable || speedBase <= 0 || isMotionControlledByServer || bStun)
                return false;

            int cooldownMoveMS = (int)(1000 / speed);
            if (path.Ended || path.SecondsPassed > pathFindData.timelifePath || path.LastMove(Time.time, cooldownMoveMS))
            {
                float oldPathLastRecalc = Time.time;
                //предидущий путь не закончили. надо продолжить без паузы
                if (!path.Ended)
                {
                    oldPathLastRecalc = path.dtLastRecalc;
                }

                RemoveNotPassableEndPoint();
                map.CurrentPathFinder.MakePath(path, pathFindData);

                path.Smooth(map);
                path.Smooth(map);
                path.Start(oldPathLastRecalc);

                if (path.Ended || path.PathLength == 0)
                    return false;

                if (!path.Ended)
                {
                    float endX, endY;
                    path.GetEndPoint(out endX, out endY);
                    //Sos.Debug(endX + " " + endY);
                    SetNotPassableEndPoint(new Vector3(endX, 0, endY));
                }
            }
          
            return true;
        }

        public bool SetPath(MapPath newPath, float timelifePath)
        {
            if (!bMoveable || speedBase <= 0 || isMotionControlledByServer || bStun)
                return false;

            int cooldownMoveMS = (int)(1000 / speed);

            if (path.Ended || path.SecondsPassed > timelifePath || path.LastMove(Time.time, cooldownMoveMS))
            {
                float oldPathLastRecalc = Time.time;
                //предидущий путь не закончили. надо продолжить без паузы
                if (!path.Ended)
                {
                    oldPathLastRecalc = path.dtLastRecalc;
                }

                RemoveNotPassableEndPoint();

                path.CopyFrom(newPath);

                path.Smooth(map);
                path.Smooth(map);
                path.Start(oldPathLastRecalc);

                if (path.Ended || path.PathLength == 0)
                    return false;

                if (!path.Ended)
                {
                    float endX, endY;
                    path.GetEndPoint(out endX, out endY);
                    //Sos.Debug(endX + " " + endY);
                    SetNotPassableEndPoint(new Vector3(endX, 0, endY));
                }

            }
            return true;
        }

        public void GetRandomPoint(out float x, out float z)
        {
            GetRandomPoint(out x, out z, PridePosition, 1f, PrideRadius);
        }

        public void GetRandomPoint(out float x, out float z, Vector3 center, float minR, float maxR)
        {
            int angle = FRRandom.Next(0, 360);
            float r = (float)Math.Sqrt(FRRandom.Next((int)(minR * minR), (int)(maxR * maxR)));
            x = (float)(center.X + r * Math.Sin(Math.PI / 180 * angle));
            z = (float)(center.Z + r * Math.Cos(Math.PI / 180 * angle));
        }

        //находится в состоянии атаки. используется для каст сенсора
        private bool AttackState = false;

        public RomanStingerIlinSunGearGamesCEO_MonsterState GetRomanMonsterState()
        {
            if (AttackState)
                return RomanStingerIlinSunGearGamesCEO_MonsterState.Attack;

            //if (Target == null)
            //    return RomanStingerIlinSunGearGamesCEO_MonsterState.Idle;

            //if (Target != null && !AttackState)
            //    return RomanStingerIlinSunGearGamesCEO_MonsterState.Chase;

            if(Target == null)
                return RomanStingerIlinSunGearGamesCEO_MonsterState.Idle;
            else
                return RomanStingerIlinSunGearGamesCEO_MonsterState.Chase;
        }

        public void CheckCastSensor(PersonBehaviour person)
        {
            if (IsDead)
                return;

            if ((person.position - position).magnitude()> 12)
                return;

            var romanState = GetRomanMonsterState();
            if ((Data.CastSensorAttack && romanState == RomanStingerIlinSunGearGamesCEO_MonsterState.Attack) || //в состоянии атаки
                (Data.CastSensorIdle && romanState == RomanStingerIlinSunGearGamesCEO_MonsterState.Idle) || //в состоянии свободен
                (Data.CastSensorChase && romanState == RomanStingerIlinSunGearGamesCEO_MonsterState.Chase)) //в состоянии гонится
            {
                if (Cheats.ShowCastSensor)
                {
					Cheats.SendCheatChatMessage($"у монстра {Data.Name} {Id} сработал каст сенсор", person.owner);
                }

                var ability0 = person.owner.activeAbility.Find(x => x.SlotId == 0);
                if (aggroList != null)
                    aggroList.AddToAggroList(person, ability0.GetDamage(this));
            }
        }

        public bool InPrideCenter(float radius)
        {
            return InRadius(PridePosition, radius);
        }

        public MapPath path = new MapPath();

        private DateTime now;


	    internal override void ResurrectNow()
        {
            ClearData();

            //stateError = 3;

            if (!InitPosition(false, null))
                return;

            sessionId++;

            CreateMobGenerator(Helper.GetGeneratorFor(),
                () =>
                {
                    SendRpc(map.ClientRpc.ResurrectResponse, position, direction, sessionId);
                    RestoreParams();
                    endWait = nextAttack = now.AddSeconds(3);

                    common.Resurrect(true);

                                //Здесь мы закидываем в буфер для отправки триггер о том, чтобы монстр из убитого положения перешёл в обычное
                                //Но меня волнует один момент, RpcClientEnum.ResurrectResponse будет отослан в этот момент, а буфер чистится, как я понял один раз в 200 мс
                                //Не вызовет ли это не правильного состояния, когда монстр будет видим трупом на клиенте в течении короткого промежутка времени
                                //Может отправку триггеров тоже стоит сделать мгновенной?
                                AddTriggerAnimatorParameterForSend("resurrect");
                    map.OnMonsterResurrect(this);
                }
            );
        }

	    public void RestoreParams()
	    {
	        Health = MaxHealth;
	        Energy = MaxEnergy;
	        SendRpc(map.ClientRpc.SetHealth, Health, MaxHealth);
        }

		public override void MyUpdate()
		{
			float stateError = 0;

            try
            {
                now = TimeProvider.UTCNow;
                if (now < nextRecalc)
                    return;

                stateError = 1;

                nextRecalc = now.AddSeconds(SEND_RATE);

                StatContainer.Update();

                stateError = 2;
                if (bCanResurrect)
                {
                    if (now > ResurrectTime)
                    {
                        ResurrectNow();
                        return; //Пусть CreateMobGenerator отработает сначала
                    }
                }
                else
                {
                    if (now > deadAnimTime)
                    {
                        ClearData();
                        stateError = 4;
                        Explosion();
                        return;
                    }
                }


                if (SpellCastAction != null)
                {
                    SpellCastAction.AbilityUpdate();
                }


                //считаем что у монстра состояние AttackTarget если хотябы одна атака дотягивается до цели по радиусу
                AttackState = false;
                foreach (var a in attacks)
                {
                    if (!AttackState && a.CanApplyAtDistance())
                        AttackState = true;

                    a.Update(now);
                }

                if (currentAttack != null)
                    currentAttack.Action(now);

                if (Target != null)
                    OldTargetPos = Target.position;

                attacks.RandomSwap();
                attacks.Sort((x,y) => y.OrderGroup - x.OrderGroup);

                stateError = 16;
        
                //Отправляем все параметры аниматора, которые накопились и очищаем буффер параметров
                //В идеале, надо бы отправлять сразу массив паратемтров, а не по одному
                foreach (var item in triggerParametersToSend)
                {
                    SendSetAnimatorParameterTriggerResponse(item);
                }
                triggerParametersToSend.Clear();
                
                foreach (var item in boolParametersToSend)
                {
                    SendSetAnimatorParameterBoolResponse(item.Key, item.Value);
                }
                boolParametersToSend.Clear();

                if (!IsDead && ParentSummoner != null && ParentSummoner.IsDead)
                {
                    SetHealth(null, 0);
                }
                if (ChildrenSummons.Count > 0)
                {
                    ChildrenSummons.RemoveAll(x => x == null || x.IsDead);
                }
            }
            catch (Exception ex)
            {
                state = MonsterState.FindTarget;
                SetCurrentAttack(null);
                Target = null;
                ILogger.Instance.Send($"State = {stateError} exception: {ex.ToString()}", ErrorLevel.exception);
                ILogger.Instance.Send(new System.Diagnostics.StackTrace().ToString(), ErrorLevel.exception);
            }
        }

        /// <summary>
        /// Добавляет булевый параметр для аниматора на клиенте в буфер на отправку
        /// </summary>
        /// <param name="parameterName">имя параметра в аниматоре на клиенте, который необходио изменить</param>
        /// <param name="newValue">значение параметра, которое нужно установить на клиенте</param>
        public void AddBoolAnimatorParameterForSend(string parameterName, bool newValue)
        {
            boolParametersToSend[parameterName] = newValue;
        }

        /// <summary>
        /// Добавляет в буфер на отправку триггер для аниматора на клиенте 
        /// </summary>
        /// <param name="parameterName">Имя триггера, который нужно устанвоить на клиенте</param>
        public void AddTriggerAnimatorParameterForSend(string parameterName)
        {
            if (!triggerParametersToSend.Contains (parameterName))
            {
                triggerParametersToSend.Add (parameterName);
            }
        }

        public void TrySendStartMoveResponse()
        {
            var newMove = !path.Ended;
            if (newMove != move || (!move && (movePos - position).sqrMagnitude() > float.Epsilon))
            {
                move = newMove;
                movePos = position;
                SendRpc(map.ClientRpc.StartMoveResponse, move, movePos);
            }
        }

        void ClearData()
	    {
            if (aggroList != null)
                aggroList.Clear();
            Target = null;
            SetResurrectTime(-1);
            state = MonsterState.FindTarget;
            SetCurrentAttack(null);
        }

        public void CommonAnimalUpdate(DateTime now)
        {
            bool canMove = CanMove;
            //animState = IsDead ? animState_die : animState_idle;

            //if (Target == null)
            //    Sos.Debug("Target == null");
            //else
            //    Sos.Debug("Target != null");

            //если за пределами прайда - возврат в прайд.
            if (canMove && state != MonsterState.BackToPride && Target == null)
            {
                //Sos.Debug("CommonUpdate 4");
                if (!InPride(position))
                {
                    //Sos.Debug("CommonUpdate 4.1");
                    state = MonsterState.BackToPride;
                    //Sos.Debug("возвращаемся в прайд");
                    //path.Clear();//путь специально не сбрасываем иначе все время сюда будем заходить и сбрасывать
                }
            }

            //тут просто ожидание.
            if (canMove && Target == null && state != MonsterState.BackToPride)
            {
                float timelifePath = 3f;
                //слишком долго не ходим
                if (!path.Ended && path.SecondsPassed > timelifePath)
                    PathClear();

                //иногда заставить походить...
                if (bMoveable && path.Ended
                    && endWait < now && FRRandom.CheckChance(0.03))
                {
                    float x, z;
                    GetRandomPoint(out x, out z);
                    MoveToPoint(x, z, timelifePath, 0, MoveCost);
                }

                //иногда заставить почесаться
                if (Data.Wait != null && path.Ended && Time.time > nextWait)
                {
                    endWait = now.AddSeconds(MonsterData.idleDelayAfterFullIdleAnimation);
                    nextWait = Time.time + MonsterData.idleDelayAfterFullIdleAnimation * 2f;
                }
            }


            //убегаем от цели
            if (canMove && Target != null && bMoveable && speedBase > 0 &&
                    InRadius(Target.position, RetreatRadius))//может не хватать места куда идти
            {
                //Sos.Debug("хотим убежать");
                state = MonsterState.Retreat;
                float x, z;
                var dir = (position - Target.position).normalized();
                x = position.X + dir.X * RetreatRadius;
                z = position.Z + dir.Z * RetreatRadius;
                if (!MoveToPoint(x, z, 3f, 0, MoveCostAngry))//пытаемся убежать по прямой, а если не получилось то в рандомную точку
                {
                    //Sos.Debug("хотим убежать. не получилось по прямой");
                    PathClear();
                    GetRandomPoint(out x, out z, Target.position, RetreatRadius + 1, RetreatRadius + 2);
                    MoveToPoint(x, z, 5f, 0, MoveCostAngry);
                }
            }

            //возвращаемся в прайд
            if (canMove && state == MonsterState.BackToPride)
            {
                //Sos.Debug("CommonUpdate 9");
                if (Target == null)
                {
                    if (InPrideCenter(backToPrideRadius))
                    {
                        state = MonsterState.FindTarget;
                    }
                    else
                    {
                        MoveToPoint(PridePosition.X, PridePosition.Z, 5f, backToPrideRadius, MoveCostAngry);
                    }
                }
                else
                    state = MonsterState.MoveToPoint;
            }

			if (!IsDead && path.Ended)
			{
				//не идем
				SetNotPassablePoint(position);
				RemoveNotPassableEndPoint();
			}
			else
			{
                //идем
                nextWait = Math.Max(nextWait, Time.time + 2f);
                RemoveNotPassablePoint();
			}

			if (IsDead)//&& path.Ended)
			{
				RemoveNotPassablePoint();
				RemoveNotPassableEndPoint();
			}

        }
        
        public void CommonUpdate(DateTime now)
        {
            bool canMove = CanMove;

            //Sos.Debug("CommonUpdate 1");

            if (!IsDead && Target != null)
                TargetInLineOfSight = CheckLineForWalk(position, Target.position);
            else
                TargetInLineOfSight = false;
            //animState = IsDead ? animState_die : animState_idle;

            //если за пределами прайда -возврат в прайд.
            if (canMove && state != MonsterState.BackToPride && Target == null)
            {
                //Sos.Debug("CommonUpdate 4");
                if (!InPride(position))
                {
                    //Sos.Debug("CommonUpdate 4.1");
                    state = MonsterState.BackToPride;
                    backToPrideRadius = 2;
                    //Sos.Debug("возвращаемся в прайд");
                    //path.Clear();//путь специально не сбрасываем иначе все время сюда будем заходить и сбрасывать
                }
            }

            //оставить в покое цели в зонах безопасности
            if (TargetPerson != null && TargetPerson.owner.bInSafeZone)
            {
                TargetPerson = null;
            }

            //обожглись в лаве.бежим назад в прайд
            if (GetStatContainer().GetStatValue(Stats.Lava) > 0 ||
                GetStatContainer().GetStatValue(Stats.Water3) > 0 ||
                Target == null && map.IsWaterSurface(position))
            {
                state = MonsterState.BackToPride;
                backToPrideRadius = 0.5f;
                Target = null;
            }

            //тут просто ожидание.
            if (canMove && Target == null && state != MonsterState.BackToPride)
            {
                
                float timelifePath = 3f;
                //слишком долго не ходим
                if (!path.Ended && path.SecondsPassed > timelifePath)
                    PathClear();

                //иногда заставить походить...
                if (bMoveable && path.Ended
                    && endWait < now && FRRandom.CheckChance(0.03))
                {
                    float x, z;
                    GetRandomPoint(out x, out z);
                    MoveToPoint(x, z, timelifePath, 0, MoveCost);
                }

                //иногда заставить почесаться
                if (Data.Wait != null && path.Ended && Time.time > nextWait)
                {
                    endWait = now.AddSeconds(MonsterData.idleDelayAfterFullIdleAnimation);
                    nextWait = Time.time + MonsterData.idleDelayAfterFullIdleAnimation * 2f;
                }
            }

            //бег к цели
            if (canMove && Target != null && bMoveable && speedBase > 0)
            {
                if (!InRadius(Target.position, Math.Max(RecommendedAttackRadius, Radius + Target.Radius + 0.1f)) || !TargetInLineOfSight)//может не хватать места куда идти
                {
                    state = MonsterState.MoveToTarget;
                    //Sos.Debug("хотим атаковать");
                    //если таргет движется или не находится в области прямой видимости, то бежим к нему. Если стоит и находится в прямой видимости , то подбегаем на расстояние атаки
                    float radius = (!IsTargetMove && TargetInLineOfSight) ? RecommendedAttackRadius : 0;
                    //Sos.Debug("radius " + radius);
                    MoveToPoint(Target.position.X, Target.position.Z, 1.5f, radius, MoveCostAngry);
                }
                else
                {
                    if (state == MonsterState.MoveToTarget)
                    {
                        //Sos.Debug("пришли атаковать. останавливаемся");
                        PathClear();
                    }
                }
            }

            //убегаем от цели
            if (canMove && Target != null && bMoveable && speedBase > 0 &&
                    InRadius(Target.position, RetreatRadius))//может не хватать места куда идти
            {
                //бежать уже надо но мы еще подождем чуть-чуть (Роман Ильин)
                if (Time.time > CanRetreatTime)
                {
                    //Sos.Debug("хотим убежать");
                    state = MonsterState.Retreat;
                    float x, z;
                    var dir = (position - Target.position).normalized();
                    x = position.X + dir.X * RetreatRadius;
                    z = position.Z + dir.Z * RetreatRadius;
                    if (!MoveToPoint(x, z, 7f, 0, MoveCostAngry))//пытаемся убежать по прямой, а если не получилось то в рандомную точку
                    {
                        PathClear();
                        GetRandomPoint(out x, out z, Target.position, RetreatRadius + 1, RetreatRadius + 2);
                        MoveToPoint(x, z, 5f, 0, MoveCostAngry);
                    }
                }
            }
            else
            {
                //не надо бежать и нельзя будет бежать еще 2 секунды
                CanRetreatTime = Time.time + 2;
            }

            //возвращаемся в прайд
            if (canMove && state == MonsterState.BackToPride)
            {
                //Sos.Debug("CommonUpdate 9");
                if (Target == null)
                {
                    if (InPrideCenter(backToPrideRadius))
                    {
                        state = MonsterState.FindTarget;
                    }
                    else
                    {
                        MoveToPoint(PridePosition.X, PridePosition.Z, 5f, backToPrideRadius, MoveCostAngry);
                    }
                }
                else
                    state = MonsterState.MoveToPoint;
            }

            /*
            if (camMove && path.Ended && Target!= null && bRotatable)
            {
                //Sos.Debug("поворачиваемся");
                var dir = (Target.position - position).normalized();
                backPosition = position;
                direction = new Vector2(dir.X, dir.Z);
            }
			*/


            if (!IsDead && path.Ended)
            {
                //не идем
                SetNotPassablePoint(position);
                RemoveNotPassableEndPoint();
            }
            else
            {
                //идем

                nextWait = Math.Max(nextWait, Time.time + 2f);

                RemoveNotPassablePoint();
                //надо найти точку где остановимся и отметить её как занятую
                //Vector3 notPassableEndPoint;
                //if (Target != null)
                //    notPassableEndPoint = GetEndPoint(path, RecommendedAttackRadius, Target.position);
                //else
                //    notPassableEndPoint = path.GetEndPoint();

                //SetNotPassableEndPoint(notPassableEndPoint);
            }


            if (IsDead)//&& path.Ended)
            {
                RemoveNotPassablePoint();
                RemoveNotPassableEndPoint();
            }
        }

		public void MoveToPointUpdate(DateTime now)
		{

			if (!IsDead && path.Ended)
			{
				//не идем
				SetNotPassablePoint(position);
				RemoveNotPassableEndPoint();
			}
			else
			{
				//идем
				RemoveNotPassablePoint();
				//надо найти точку где остановимся и отметить её как занятую
				//Vector3 notPassableEndPoint;
				//if (Target != null)
				//    notPassableEndPoint = GetEndPoint(path, RecommendedAttackRadius, Target.position);
				//else
				//    notPassableEndPoint = path.GetEndPoint();

				//SetNotPassableEndPoint(notPassableEndPoint);
			}

			if (IsDead)//&& path.Ended)
			{
				RemoveNotPassablePoint();
				RemoveNotPassableEndPoint();
			}
		}

        public void PathClear()
        {
            path.Clear();
        }

        public bool ExistNotPassablePoint = false;
        public Vector3 NotPassablePoint;

        public bool ExistNotPassableEndPoint = false;
        public Vector3 NotPassableEndPoint;

        public void RemoveNotPassablePoint()
        {
            if (ExistNotPassablePoint)
            {
                ServerPathFinderAlgorithm.SetTargetForMiniMap(map, NotPassablePoint.X, NotPassablePoint.Z, Radius * 2, false);
                ExistNotPassablePoint = false;
            }
        }

        public void SetNotPassablePoint(Vector3 pos)
        {
            if ((pos - NotPassablePoint).magnitude()< 0.1 && ExistNotPassablePoint)
                return;

            if (ExistNotPassablePoint)
                RemoveNotPassablePoint();

            ExistNotPassablePoint = true;
            NotPassablePoint = pos;
            ServerPathFinderAlgorithm.SetTargetForMiniMap(map, NotPassablePoint.X, NotPassablePoint.Z, Radius * 2, true);
        }


        public void RemoveNotPassableEndPoint()
        {
            if (ExistNotPassableEndPoint)
            {
                ServerPathFinderAlgorithm.SetTargetForMiniMap(map, NotPassableEndPoint.X, NotPassableEndPoint.Z, Radius * 2, false);
                ExistNotPassableEndPoint = false;
            }
        }

        public void SetNotPassableEndPoint(Vector3 pos)
        {
            if ((pos - NotPassableEndPoint).magnitude()< 0.1 && ExistNotPassableEndPoint)
                return;

            if (ExistNotPassableEndPoint)
                RemoveNotPassableEndPoint();

            ExistNotPassableEndPoint = true;
            NotPassableEndPoint = pos;
            ServerPathFinderAlgorithm.SetTargetForMiniMap(map, NotPassableEndPoint.X, NotPassableEndPoint.Z, Radius * 2, true);
        }

        public float CanRetreatTime = 0;
        
        public override string PersonalParam
		{
			get { return map.LevelParam; }
		}

	    public FormulaX ResurrectTimeFactor = new FormulaX("killrate pcount", "1");
        public int ResurrectTimeSec { get; set; }
        public bool bCanResurrect {get { return ResurrectTimeSec >= 0; } }

        public delegate void InDamageAction(MapTargetObject attacker, ref int damage, bool resetCast);
        public event InDamageAction InDamage;

        public override void OnInDamage(MapTargetObject attacker, ref int damage, bool resetCast)
        {
            if (aggroList != null && CanAttackTarget(attacker))
                aggroList.AddToAggroList(attacker, damage);

            InDamage?.Invoke(attacker, ref damage, resetCast);
        }

        public delegate void OutDamageAction(StatContainer StatContainer, int damage, bool crit, ref double mult, DamageType type, bool autoAttack = false);
        public event OutDamageAction OutDamage;

        public override void OnOutDamage(StatContainer StatContainer, int damage, bool crit, ref double mult, DamageType type, bool autoAttack = false)
        {
            OutDamage?.Invoke(StatContainer, damage, crit, ref mult, type, autoAttack);
        }

        public void SetDeadAnimTime(int animTimeSec)
	    {
	        ResurrectTimeSec = -1;
            timeforDeadAnimSec = animTimeSec;
	    }

        private DateTime deadAnimTime = DateTime.MaxValue;
        private float timeforDeadAnimSec = 10;


		#region Для плашки воскрешения после смерти на локации, после реконнекта
		public float ResurrectSec { get; private set; }
		public DateTime ResurrectTime { get; private set; }
		public int ResurrectCost;
		public void SetResurrectTime(float time)
		{
			ResurrectSec = time;
			ResurrectTime = time >= 0 ? TimeProvider.UTCNow.AddSeconds(time) : DateTime.MaxValue;
		}
		public float GetResurrectTime()
		{
			return ResurrectTime == DateTime.MaxValue ? -1f : (float) (ResurrectTime - TimeProvider.UTCNow).TotalSeconds;
		}
		#endregion

        internal override Elements AttackElement()
        {
            return Data.Element;
        }


        /// <summary>
        /// нанести урон, использовать броню и шансы промоха и крита
        /// </summary>
        /// <returns>было ли попадание</returns>
        public override bool Hit(MapTargetObject attacker, int damage, DamageType type, Elements atkElement, out bool crit, bool canMiss = true, 
            bool resetCast = true, bool absoluteHit = false, bool autoAttack = false, CritType critType = CritType.AbilityAttack, HitEffectXmlData hitFxData = null)
        {
            if(type == DamageType.Area && Data.AoEImmun)
            {
                crit = false;
                return false;
            }

            return StatsFormulas.Instance.Hit(attacker, this, damage, type, atkElement, out crit, canMiss, resetCast, absoluteHit, autoAttack, critType, hitFxData);
        }


        public override void SetHealth(MapTargetObject attacker, int value, bool crit = false)
		{
            if (IsDead)
            {
                return;
            }

            if ((bImmortality || InSafeZone) && value < Health)
            {
                return;
            }

		    var MinHeath = (int)(StatContainer.GetStatValue(Stats.CanNotDie) * MaxHealth / 100);
            bool bHeal = value >= Health;

           
		    if (!bHeal)
		    {
		        //если здоровье уменьшается и твое здоровье уже ниже CanNotDiePercent, то просто выйти. Иначе монстр отлечится, а нам этого не хочется
                if (Health < MinHeath)
                    return;
		        if (value < MinHeath)
		            value = MinHeath;
		    }

		    value = Math.Min(value, MaxHealth);
            Health = value;

			SendRpc(map.ClientRpcUnr.SetHealth, value, MaxHealth);
            if(MaxHealth != Health)
                map.OnMonsterSetHealth(this);
            if (bHeal)
            {
                //как-то отобразить эффект лечения или самолечения... 
                return;
            }
          
            if (IsDead)
			{
                //У ворот нет анимации смерти, поэтому будет отсылать триггер анимации смерти всем кроме них
                //Возможно было бы лучше отмечать такие вещи в xml у кого есть анимация смерти, а у кого нет
                if(Data.MonsterType != MonsterType.Gate)
                {
                    if (Data.MonsterType == MonsterType.Boss)
                    {
                        AddTriggerAnimatorParameterForSend("die_boss");
                    }
                    else
                    {
                        AddTriggerAnimatorParameterForSend("die");
                    }
                }

                foreach(var a in attacks)
                    a.OnDeath();

                StatContainer.DeathRemoveInfluences();

				int upgrade;
				var deathEffect = InstanceGameManager.Instance.EffectAlgorithm.GetDieEffect(this, out upgrade);
                if(deathEffect != EffectType.Empty && deathEffect != EffectType.None)
				    ServerRpcListener.Instance.SendCreateFxResponse(this, deathEffect, upgrade);

                
                if (bCanResurrect)
			    {
					SetResurrectTime((float)(ResurrectTimeSec * ResurrectTimeFactor.Calc(map.KillRateCounter.GetKillRate(), map.persons.Count)));
                }
			    else
			    {
                    deadAnimTime = TimeProvider.UTCNow.AddSeconds(timeforDeadAnimSec);
                }

				if (Data.TransparencyForOwner)
					SendRpc(map.ClientRpc.ColliderEnable, false);



                var personAttacker = attacker as PersonBehaviour;
                if (personAttacker != null)
                {
                    // если игрок перерос локацию TODO: Fil bigPersonHeroPower
                    //var bigPersonHeroPower = (personAttacker.owner.HeroPower > personAttacker.owner.syn.Map.Edge.GSMax + 50) && !personAttacker.owner.syn.map.IsTutorial;
					var finalLoots = Loots.GetLootList(this, personAttacker.owner, loot => CheckCurrentLootChance(loot, personAttacker));
					foreach (var finalLoot in finalLoots)
						map.loots.Add(new MapLoot(finalLoot, this, personAttacker, position, map.GetLootPos(position)));
                }

                if (characterType == CharacterType.Person && attacker?.characterType == CharacterType.Person)
                    map.OnKillAnnounce(attacker, this);

                map.OnMonsterDie(this, attacker);

				SendRpc(map.ClientRpc.DeathResponse, ResurrectSec);
            }
		}

        public override void AddHealth(MapTargetObject attacker, int value, bool selfRegeneration = false, bool canCrit = true)
        {
            int health = Health;
            base.AddHealth(attacker,value,selfRegeneration,canCrit);
            if (!selfRegeneration && (personType != PersonType.None))//Сделано что бы отхил ботов засчитывался, как отхил игроков
            {
                int effectiveHeal = Health - health;
                if (effectiveHeal > 0)
                    map.OnPersonHeal(this, attacker, effectiveHeal);
            }
        }

        public virtual void Explosion()
		{
			ServerRpcListener.Instance.SendRpc(map.ClientRpc.RemoveEntity, eType, Id);

            isDestroyed = true;

			//map.monsters.Remove(this); //В map.MyUpdate обновится
		}
        
        public bool bFavorOfGods = false;

        internal void StatChange(Stats stat)
        {
            switch (stat)
            {
                case Stats.Stun:
                    bStun = StatContainer.GetStatValue(Stats.Stun) > 0;
                    PathClear();
                    break;
                case Stats.Root:
                    bRoot = StatContainer.GetStatValue(Stats.Root) > 0;
                    PathClear();
                    break;
                case Stats.Silence:
                    bSilence = StatContainer.GetStatValue(Stats.Silence) > 0;
                    break;
                case Stats.FavorOfGods:
                    bFavorOfGods = StatContainer.GetStatValue(Stats.FavorOfGods) > 0;
                    break;
            }
        }

        internal void CalcStatChange(string characteristics)
        {
            switch (characteristics)
            {
                case "MoveSpeedReal":
                    speedBase = (float)StatContainer.GetStatValue(characteristics);
                    break;
                case "HP":
                    MaxHealth = (int)StatContainer.GetStatValue(characteristics);
                    int health = Math.Min(Health, MaxHealth);
                    SetHealth(this, health);
                    break;
            }
        }

        public override double AutoPricelPriority(MapTargetObject person)
        {
            if (bFavorOfGods)
                return -1;
            if (InSafeZone)
                return -1;

            switch (Data.MonsterType)
            {
                case MonsterType.Boss:
                    return AutoPricelPriority(person, 1.25);
                case MonsterType.Champion:
                    return AutoPricelPriority(person, 1.2);
                case MonsterType.NotTargeted:
                    return -1;
                default:
                    return AutoPricelPriority(person, 1);
            }
        }

        public override void OnHit(MapTargetObject target, int damage)
        {
            if (Data.HitActionList == null || Data.HitActionList.Count == 0)
                return;

            foreach (var action in Data.HitActionList)
                action.Execute(Level, target, this, null);
        }

		public override void OnKill(MapTargetObject target, int damage)
        {
            if (Data.KillActionList == null || Data.KillActionList.Count == 0)
                return;

            foreach (var action in Data.KillActionList)
                action.Execute(Level, target, this, null);

            //base.OnKill(target);
        }

        public LootList Loots;

		public void SendColliderEnable(InstancePerson instancePerson)
		{
			if (instancePerson == null || !Data.TransparencyForOwner)
				return;

			bool bTutorGateOpen = !instancePerson.bRunTutorial || instancePerson.bTutorialOpenFortGate; //В туторе ворота закрыты
			if (instancePerson.PersonalParam == PersonalParam && bTutorGateOpen)
				SendRpc(instancePerson.ClientRpc.ColliderEnable, false);
		}

        //Создать эффект над которым создастся монстр без задержки
        public void CreateMobGenerator2(EffectType createMobFxId)
        {
            MapEffect effect = new MapEffect(map) { position = position, fxId = createMobFxId };
            ServerRpcListener.Instance.SendCreateFxResponse(effect);

            nextRecalc = TimeProvider.UTCNow.AddSeconds(0.5);
        }

        //Создать эффект над которым создастся монстр с задержкой
        public void CreateMobGenerator(EffectType createMobFxId, Action action)
		{
			if (characterType == CharacterType.Person)
			{
                CoroutineRunner.Instance.RunNextFrame(action);
                return;
			}

            MapEffect effect = new MapEffect(map) { position = position, fxId = createMobFxId };
            ServerRpcListener.Instance.SendCreateFxResponse(effect);

            nextRecalc = DateTime.MaxValue;
            CoroutineRunner.Instance.Timer(0.5f, () =>
            {
                action();
                nextRecalc = TimeProvider.UTCNow;
            });
        }

        public ScenarioStringCollection ScenarioStrings = new ScenarioStringCollection(null);

        public void SetLevel(int level)
        {
            Level = level;
            ((MonsterStatContainer)GetStatContainer()).ResetCalcStats();
            MaxHealth = (int)StatContainer.GetStatValue("HP");
            Health = MaxHealth;
            SendRpc(map.ClientRpc.SetHealth, Health, MaxHealth);
			/*ServerRpcListener.Instance.SendRpc(map.ClientRpc.FortUpdateTowerLevelInfoResponse, Id, Level); 
            Отпправка лвла на клиент, на клиенте сейчас не используется но мало ли*/
        }

        protected override bool IsCanApplySpeedOverride => base.IsCanApplySpeedOverride && bMoveable;

        public override void KnockBack(Vector3 dir, float time, float dist, bool force = false, bool resetCast = false)
        {
            if (!force && (Data.NoKnockback || isMotionControlledByServer || !IsCanApplySpeedOverride))
                return;

            PathClear();
            isMotionControlledByServer = true;
            Speed = dir.normalized() * dist / time;
            Speed.Y = GravityInfluence.ACCELERATION * time / 2;

            base.KnockBack(dir, time, dist, force, resetCast);
        }

        public override void UpdatePhysics()
        {
			if (!bMoveable)
				return;

            if (isMotionControlledByServer)
            {
                if (Time.time < overrideSpeedTargetTime)
                {
                    if (SpeedOverrideFunction != null)
                    {
                        float lastYSpeed = Speed.Y;
                        Speed = SpeedOverrideFunction(Math.Min(1f - (overrideSpeedTargetTime - Time.time) / overrideSpeedTime, 1f));
                        Speed.Y = lastYSpeed;
                    }
                }
                else
                {
                    SpeedOverrideFunction = null;
                }


                Speed.Y -= GravityInfluence.ACCELERATION * Time.deltaTime;
                position = backPosition = Helper.UltraMegaCorrectXZPosition(map, position, position + Speed * (Time.deltaTime), true, false, Helper.DELTA_HEIGH_WALK_MOB, airway: IsAirway);
                Vector3 correctPosition = Helper.UltraCorrectYPosition(map, position, true, airway: IsAirway);
                var h = correctPosition.Y;
				IsFly = (position.Y >= h + Helper.VoxelSize);

                if (position.Y < h && Speed.Y <= 0)
                {
                    position = backPosition = correctPosition;
                    //Sos.Debug("закончили падение монстра");
                    if (SpeedOverrideFunction == null)
                    {
                        isMotionControlledByServer = false;
                        Speed = Vector3.Zero;
                    }
                    Speed.Y = 0;
                }
                else
                {
                    isMotionControlledByServer = true;
                }
            }
            else
            {
                Vector3 correctPosition = Helper.UltraCorrectYPosition(map, position, true, airway: IsAirway);
                var h = correctPosition.Y;
                if (position.Y <= h + Helper.VoxelSize)
                {
                    isMotionControlledByServer = false;
                    position = correctPosition;
                }
                else
                {
                    PathClear();
                    //Sos.Debug("начали падение монстра");
                    isMotionControlledByServer = true;
                    IsFly = true;
                    Speed = GeometryAlgorithm.Vector2ToVector3(directionMove) * (speed / 2);
                }
            }
        }

		// попытается отослать позицию всем персам в области видимости только при её изменении по обычному UDP (доставка не гарантируется)
		public override void SendChangeTransformResponse()
		{
			if (!bMoveable && !bRotatable)
				return;

			bool positionChanged = CheckNeedSendPositionChange();

			foreach (var p in map.persons)
			{
				if (p == null)
					continue;
				if (p.SendChangeTransformRPCMode != ChangeTransformReliability.NotSet)
				{
					map.RegisterForTransformSend(p.owner, p.SendChangeTransformRPCMode, this);
				}
				else if (positionChanged && p.SendRPC)
				{
					map.RegisterForTransformSend(p.owner, ChangeTransformReliability.Unreliable, this);
				}
			}
		}

		public override ChangeTransformData GetTransformData()
		{
			var changeTransformData = new ChangeTransformData(Id, position, direction, false, IsFly, false, false);
			return changeTransformData;
		}

		public override ChangeTransformForceData GetTransformForceData()
		{
			var changeTransformForceData = new ChangeTransformForceData(Id, position, direction);
			return changeTransformForceData;
		}
	}
}
