﻿using Assets.Scripts.InstanceServer.Entries.XmlData;
using System;
using System.Collections.Generic;
using System.Text;
using Zenject;

namespace Assets.Scripts.InstanceServer.Entries.TargetObjects
{
    public class MonsterBehaviourFactory : IMonsterBehaviourFactory
    {
        public MonsterBehaviourFactory()
        {
        }
  
        public MonsterBehaviour Create(Map map, CreateMonsterData createMonsterData)
        {
            MonsterData data = createMonsterData.monsterData;
            if (data == null)
                 data = InstanceGameManager.Instance.gameXmlData.MonsterXmlDatas.GetMonsterData(createMonsterData.monsterName);

            if (data == null)
            {
                throw new ArgumentNullException("data not found. monsterName=" + createMonsterData.monsterName);
            }

            var monster = new MonsterBehaviour(map, createMonsterData.id);
            monster.Init(data, createMonsterData.lootName, createMonsterData.level, createMonsterData.startPoint, createMonsterData.replaceTechnicalName);

            return monster;
        }
    }

    public interface IMonsterBehaviourFactory : IFactory<Map, CreateMonsterData, MonsterBehaviour>
    {
        
    }
}
