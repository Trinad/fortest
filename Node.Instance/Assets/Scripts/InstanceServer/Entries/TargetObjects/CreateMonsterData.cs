﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Enums;

namespace Assets.Scripts.InstanceServer.Entries.TargetObjects
{
    public class CreateMonsterData
    {
        public MonsterData monsterData = null;
        public int id = 0;
        public string monsterName;
        public Vector3 pos;
        public string lootName;
        public int level;
        public string startPoint = null;
        public string replaceTechnicalName;
        public EffectType CreateGeneratorId = EffectType.None;
    }
}
