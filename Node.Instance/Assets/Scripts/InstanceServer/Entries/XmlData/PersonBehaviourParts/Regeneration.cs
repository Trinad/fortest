﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using System;
using Assets.Scripts.Utils.Common;
using Assets.Scripts.InstanceServer.Addin;
using System.Linq;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic.Enums;
using System.Xml.Serialization;
using Assets.Scripts.Utils;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.AttackDatas
{
    public class RegenerationHealth : PersonBehaviourPart
    {
        [XmlAttribute]
        public float BoostPeriod = 1;

        [XmlAttribute]
        public float Period = 3;

        [XmlAttribute]
        public float NotDamageTime = 5;

        [XmlAttribute]
        public float BoostRegenerationDistance = 10;

        [XmlAttribute]
        public string Characteristic = "HPRegen";

        [XmlIgnore]
        private float RegenerationHPTime;

        public override void Init()
        {
            base.Init();
            person.owner.InDamage += (MapTargetObject attacker, ref int damage, bool resetCast) => 
                    RegenerationHPTime = Time.time + NotDamageTime;
        }

        public override void Update()
        {
            if (!person.IsDead && Time.time > RegenerationHPTime)
            {
                if (person.Health < person.MaxHealth)
                    person.AddHealth(person, Math.Max(0, (int)person.GetStatContainer().GetStatValue(Characteristic)), true);

                if (person.MinMonsterDistance > BoostRegenerationDistance)
                    RegenerationHPTime = Time.time + BoostPeriod;
                else
                    RegenerationHPTime = Time.time + Period;
            }
        }
    }

    public class RegenerationMana : PersonBehaviourPart
    {
        [XmlAttribute]
        public float Period = 3;

        [XmlAttribute]
        public string Characteristic = "SPRegen";

        private float RegenerationSPTime;
        public override void Update()
        {
            if (!person.IsDead && Time.time > RegenerationSPTime)
            {
                if (person.Energy < person.MaxEnergy)
                    person.SetEnergy(person.Energy + (int)person.GetStatContainer().GetStatValue(Characteristic));

                RegenerationSPTime = Time.time + Period;
            }
        }
    }

    public class RegenerationStamina : PersonBehaviourPart
    {
        [XmlAttribute]
        public float Period = 0.4f;

        [XmlAttribute]
        public int Increment = 2;

        private float RegenerationStaminaTime;
        public override void Update()
        {
            if (!person.IsDead && Time.time > RegenerationStaminaTime)
            {
                if (person.Stamina < person.MaxStamina)
                {
                    if (person.map.IsCamp)
                        person.SetStamina(person.MaxStamina);
                    else
                        person.SetStamina(person.Stamina + Increment);
                }

                RegenerationStaminaTime = Time.time + Period;
            }
        }
    }
}
