﻿using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using UtilsLib.BatiyScript;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.XmlData
{
	public class SpecialOfferItem:IElementInfo
	{
	    [XmlAttribute] public int OfferId;
	    [XmlAttribute] public string Sku;
	    [XmlAttribute] public float Price;
	    [XmlAttribute] public string OfferName;
	    [XmlAttribute] public string TechnicalNameTitle;
	    [XmlAttribute] public string TechnicalNameDesc;
	    [XmlAttribute] public PersonType PersonType = PersonType.None;
	    [XmlAttribute] public int PersonLevel = 0;
        [XmlAttribute] public int Duration;
	    [XmlAttribute] public int Weight;
	    [XmlAttribute] public bool IgnoresCooldown;
	    [XmlAttribute] public bool ShowForcibly;
	    [XmlAttribute] public int Cooldown;
	    [XmlAttribute] public bool Inactive;

	    [XmlElement("OfferGui")]
        public SpecialOfferGui OfferGui;

	    public LootList Content;

	    public UnlockRuleList UnlockRules = new UnlockRuleList();

		public bool OfferUnlockRule(InstancePerson person)
		{
			return UnlockRules.CanUnlock(person);
		}

		// если понадобится что то записывать и считывать в scenarioString
		[XmlAttribute]
		public string Condition
		{
			get { return fCondition?.Text; }
			set { fCondition = new FormulaBool("p", value); }
		}

	    [XmlIgnore]
		public FormulaBool fCondition;

	    // если понадобится что то записывать и считывать в scenarioString
	    public IActionList Actions;

        public string GetName()
        {
            return OfferName;
        }
    }
}
