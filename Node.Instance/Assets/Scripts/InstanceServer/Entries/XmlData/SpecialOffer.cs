﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.BatiyScript;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.XmlData
{
	public class SpecialOffer
	{
        [XmlAttribute]
	    public int ClientOfferCount;

	    [XmlAttribute] public int DaysForDelFromOffersBills;

        public FormulaX OfferPool;

	    public int GetPoolNumber(float price)
	    {
	        return OfferPool.IntCalc(price);
	    }

        [XmlElement("SpecialOfferItem")]
		public List<SpecialOfferItem> SpecialOfferItems;


		public SpecialOfferItem GetSpecialOfferItem(int offerId)
		{
			return SpecialOfferItems.FirstOrDefault(x => x.OfferId == offerId);
		}

	}

	public enum OfferPoolType
    {
        None =-1,
        Fry = 0,
        Dolphins = 1,
        Whales,
        Giants
    }
}
