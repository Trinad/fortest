﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using UtilsLib.Logic.Enums;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.BatiyScript;

namespace Assets.Scripts.InstanceServer.Entries
{

    public class UpgradeParams
    {
        public FormulaX CommonUpgradeItemCount;
        public FormulaX UncommonUpgradeItemCount;
        public FormulaX RareUpgradeItemCount;

        public FormulaX UpgradeCost;

        public int GetUpgradeItemCount(ItemRarity rarity, int upLevel)
        {
            switch (rarity)
            {
                case ItemRarity.Usual:
                    return (int)CommonUpgradeItemCount.Calc(upLevel, null);
                case ItemRarity.Unusual:
                    return (int)UncommonUpgradeItemCount.Calc(upLevel, null);
                case ItemRarity.Rare:
                    return (int)RareUpgradeItemCount.Calc(upLevel, null);
            }

            throw new Exception("undefined rarity " + rarity);
        }


        public int GetCost(InventoryItemData item)
        {
            int count = GetUpgradeItemCount(item.Rarity, item.UpgradeLevel);
            return UpgradeCost.IntCalc(item.StartUpgradeCost, count, (int)item.Rarity);
        }

        public void Upgrade(InstancePerson person, InventoryItemData item)
        {
            int upgradeItemCount = GetUpgradeItemCount(item.Rarity, item.UpgradeLevel);
            if (upgradeItemCount == -1)
                return;

            if (item.Count < upgradeItemCount)
                return;

            bool equiped = item.Equiped;

            if (equiped)
            {
                person.RemoveItemInEquiped(item);
            }

            person.UpgradeLevelsCollection.AddUpgardeLevel(item.Name);

            person.UpgradeLevelsCollection.RepareUpgradeLevel(item);

            item.Count = item.Count - upgradeItemCount + 1;

            if (equiped)
            {
                person.EquipItem(item, out _);
            }

			person.OnUpgradeItem(item);
        }
    }
}
