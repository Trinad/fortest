﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.Logic.Enums;
using UtilsLib.BatiyScript;
using Fragoria.Common.Utils;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData
{
	public class Item : ILoot
	{
		[XmlAttribute]
		public int Count = 1;

        [XmlIgnore]
        public FormulaX fLevel = new FormulaX("p targetLevel", "targetLevel + Rnd(-1, +1)");

        [XmlAttribute]
        public string Level
        {
            get { return fLevel.Text; }
            set { fLevel = new FormulaX("p targetLevel", value); }
        }

        [XmlAttribute]
        public ItemType ItemType = ItemType.None;

        [XmlAttribute]
		public ItemRarity Rarity = ItemRarity.Default;

        private GameXmlData gameXmlData;
        [XmlInject]
        public void Init(GameXmlData gameXmlData)
        {
            this.gameXmlData = gameXmlData;
        }

		public InventoryItemData GetItem(MapTargetObject target, InstancePerson person)
		{
            PersonType usePersonType = person != null ? person.personType : target.personType;

            int level = Math.Max(1, fLevel.IntCalc(person != null ? person.StatContainer : null, target != null ? (double)target.Level : 0));

			return gameXmlData.ItemDataRepository.GetRandomItem(Count, level, Rarity, null, usePersonType, ItemType);
		}

		public override IEnumerable<IConcreteLoot> GetILoot(MapTargetObject target, InstancePerson person)
		{
			var item = GetItem(target, person);
			yield return new ConcreteItem(item);
		}
	}
}