﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.BatiyScript;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData
{
	public abstract class ILoot
	{
        [XmlIgnore]
        public FormulaX FChance = new FormulaX("p target", "100");

        [XmlAttribute]
        public string Chance
        {
            get { return FChance.Text; }
            set { FChance = new FormulaX("p target", value); }
        }

        public ChestEntityDescriptionXML ChestEntityDescription;

        /// <summary>
        /// получить лут который будет лежать в MapLoot
        /// </summary>
        public abstract IEnumerable<IConcreteLoot> GetILoot(MapTargetObject target, InstancePerson person);

        public virtual ChestEntityDescription GetDescriptions(StatContainer statContainer)
        {
            return ChestEntityDescription?.GetDescriptionForClient();
        }
	}
}
