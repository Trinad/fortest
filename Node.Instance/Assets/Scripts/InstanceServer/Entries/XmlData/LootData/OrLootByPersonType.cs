﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using UtilsLib;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Assets.Scripts.Utils.Common;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData
{
	//С проверкой по персон тайпу
	public class OrLootByPersonType : OrLoot
	{
		private Dictionary<PersonType, List<ILoot>> preparedLoot;

		private List<ILoot> GetLootForPerson(InstancePerson person)
		{
			if (preparedLoot == null)
			{
				preparedLoot = new Dictionary<PersonType, List<ILoot>>();
				for (PersonType i = 0; i < PersonType.Count; i++)
					preparedLoot[i] = new List<ILoot>();

				foreach (var lootitem in Loots)
				{
					if (lootitem is ItemById lootitemById)
					{
						var item = lootitemById.GetItem();
						for (PersonType i = 0; i < PersonType.Count; i++)
							if (item == null || item.PersonType.Contains(i))
								preparedLoot[i].Add(lootitem);
					}
				}
			}

			List<ILoot> personalLoot;
			if (!preparedLoot.TryGetValue(person.personType, out personalLoot))
				ILogger.Instance.Send("OrLootByPersonType.GetLootForPerson: не нашли лут для " + person.personType, ErrorLevel.error);

			return personalLoot;
		}

		private int[] GetProbability(InstancePerson person, MapTargetObject target, List<ILoot> personalLoot)
		{
			var probability = new int[personalLoot.Count];

			var statContainer = person.StatContainer;
            var targetStatContainer = target == null ? null : target.GetStatContainer();
			for (int i = 0; i < personalLoot.Count; i++)
				probability[i] = personalLoot[i].FChance.IntCalc(statContainer, targetStatContainer);

			return probability;
		}

		public override IEnumerable<IConcreteLoot> GetILoot(MapTargetObject target, InstancePerson person)
		{
			for (int i = 0; i < Count; i++)
			{
				var personalLoot = GetLootForPerson(person);

				var personProbability = GetProbability(person, target, personalLoot);
				var res = FRRandom.Random(personalLoot, personProbability);

				foreach (var loot in res.GetILoot(target, person))
					yield return loot;
			}
		}
	}
}