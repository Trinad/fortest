﻿using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using UtilsLib;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Node.Instance.Assets.Scripts.InstanceServer.Entries.XmlData.LootData;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData
{
	public class OrLoot : ILoot
	{
		[XmlElement(typeof(AndLoot))]
		[XmlElement(typeof(OrLoot))]
		[XmlElement(typeof(OrLootByPersonType))]
		[XmlElement(typeof(Resource))]
		[XmlElement(typeof(Gold))]
		[XmlElement(typeof(LifeSphere))]
		[XmlElement(typeof(Item))]
		[XmlElement(typeof(ItemById))]
		[XmlElement(typeof(RandomPotion))]
		[XmlElement(typeof(FromLootList))]
		[XmlElement(typeof(SetScenarioStringLoot))]
		public List<ILoot> Loots;

		[XmlAttribute]
		public int Count = 1;

		private int[] probability;

		private int[] GetProbability(InstancePerson person, MapTargetObject target)
		{
			if (probability == null)
				probability = new int[Loots.Count];

			var statContainer = person?.StatContainer;
            var targetStatContainer = target == null ? null : target.GetStatContainer();
            for (int i = 0; i < Loots.Count; i++)
				probability[i] = Loots[i].FChance.IntCalc(statContainer, targetStatContainer);

			return probability;
		}

		public override IEnumerable<IConcreteLoot> GetILoot(MapTargetObject target, InstancePerson person)
		{
			for (int i = 0; i < Count; i++)
			{
				var personProbability = GetProbability(person, target);
				var res = FRRandom.Random(Loots, personProbability);
				if (res == null)
					continue;

				foreach (var loot in res.GetILoot(target, person))
					yield return loot;
			}
		}
	}
}