﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking.ClientDataRPC;
using Fragoria.Common.Utils;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData
{
	public class ItemById : ILoot
	{
        [XmlAttribute]
        public int Count = 1;
        [XmlAttribute]
		public string BaseItemId;
		[XmlAttribute]
		public ItemPersonType PersonType = ItemPersonType.Any;

        private GameXmlData gameXmlData;
        [XmlInject]
        public void Init(GameXmlData gameXmlData)
        {
            this.gameXmlData = gameXmlData;
        }

        public InventoryItemData GetItem()
        {
            return gameXmlData.ItemDataRepository.GetCopyInventoryItemData(BaseItemId, Count);
        }

		public override IEnumerable<IConcreteLoot> GetILoot(MapTargetObject target, InstancePerson person)
		{
			var item = GetItem();
			yield return new ConcreteItem(item);
		}
	}
}
