﻿using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using UtilsLib.Logic.Enums;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.BatiyScript;
using Fragoria.Common.Utils;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData
{
	public class Resource : ILoot
	{
        [XmlIgnore]
        public FormulaX fCount = new FormulaX("p", "1");

        [XmlAttribute]
        public string Count
        {
            get { return fCount.Text; }
            set { fCount = new FormulaX("p", value); }
        }

        [XmlAttribute]
		public ResourceType Type;

        private GameXmlData gameXmlData;

        [XmlInject]
        public void Init(GameXmlData gameXmlData)
        {
            this.gameXmlData = gameXmlData;
        }

        public InventoryItemData GetItem()
        {
            return gameXmlData.ItemDataRepository.GetCopyInventoryItemData(Type, fCount.IntCalc(null));
        }

		public override IEnumerable<IConcreteLoot> GetILoot(MapTargetObject target, InstancePerson person)
		{
			var item = GetItem();
			yield return new ConcreteItem(item);
		}
    }
}
