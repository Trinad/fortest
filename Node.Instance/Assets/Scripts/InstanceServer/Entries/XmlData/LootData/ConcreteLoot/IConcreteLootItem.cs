﻿using Assets.Scripts.Networking.ClientDataRPC;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot
{
	public interface IConcreteLootItem
	{
		InventoryItemData GetItem();
	}
}
