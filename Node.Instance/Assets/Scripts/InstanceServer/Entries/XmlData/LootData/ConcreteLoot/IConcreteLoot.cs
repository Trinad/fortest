﻿using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot
{
	public abstract class IConcreteLoot
	{
		public virtual string LootName => "random_equip_0";

		public virtual int GetLifeTimeSec()
		{
			return ServerConstants.Instance.ResourceLootLifeTimeoutSec;
		}

		public abstract bool Reward(InstancePerson person);
	}
}
