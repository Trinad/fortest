﻿namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot
{
	public class ConcreteLifeSphere : IConcreteLoot
	{
		public int Count = 1;

		public override string LootName => "original_life";

		public override bool Reward(InstancePerson person)
		{
			person.syn.AddHealth(person.syn, Count);
			return true;
		}
	}
}
