﻿using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot
{
	public class ConcreteItem : IConcreteLoot, IConcreteLootItem
	{
		private readonly InventoryItemData item;

		public override string LootName
		{
			get
			{
			    if (item.ItemType == ItemType.Resource)
			    {
                    switch (item.Resource)
                    {
                        case ResourceType.SparkCube:return "crystal_cube"; //Искра куба
                        //case ResourceType.Tissue:return "cloth"; break;//Ткань
                        //case ResourceType.Wood:return "wood";//Дерево
                        //case ResourceType.Stone:return "stone";//Камень
                        //case ResourceType.Metal:return "iron";//Металл
                        //case ResourceType.Leather:return "leather";

                        //case ResourceType.OriginalCloth:return "cloth";//TODO_Deg original_cloth
                        //case ResourceType.OriginalLife:return "original_life";
                        //case ResourceType.OriginalSeed:return "original_seed";
                        //case ResourceType.OriginalStone:return "original_stone";
                        //case ResourceType.OriginalOre:return "original_ore";

                        case ResourceType.Gold:return "gold";
                        case ResourceType.Emerald:return "emerald";
                        case ResourceType.KeyStone:return "key_stones";

                        //case ResourceType.PassiveAbilityPoint:return "original_stone";
                    }
                }

				switch (item.Rarity)
				{
					case ItemRarity.Usual: return "random_equip_0";
					case ItemRarity.Unusual: return "random_equip_1";
					case ItemRarity.Rare: return "random_equip_2";
					case ItemRarity.Epic: return "random_equip_3";
					case ItemRarity.Legendary: return "random_equip_4";
					default: return "random_equip_0";
				}
			}
		}

        public ConcreteItem(InventoryItemData item)
        {
            this.item = item;
        }

        public override int GetLifeTimeSec()
        {
			if (item.ItemType == ItemType.Resource)
				return ServerConstants.Instance.ResourceLootLifeTimeoutSec;

			return ServerConstants.Instance.ItemLootLifeTimeoutSec;
        }

        public override bool Reward(InstancePerson person)
        {
			person.AddItem(item);
			return true;
        }

        public InventoryItemData GetItem()
        {
           return item;
        }
    }
}
