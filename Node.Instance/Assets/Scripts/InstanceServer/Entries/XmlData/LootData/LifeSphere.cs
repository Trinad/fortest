﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData
{
	public class LifeSphere : ILoot
	{
		[XmlAttribute]
		public int Count = 1;

		public override IEnumerable<IConcreteLoot> GetILoot(MapTargetObject target, InstancePerson person)
		{
			yield return new ConcreteLifeSphere { Count = Count };
		}
	}
}
