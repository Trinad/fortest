﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using UtilsLib.BatiyScript;

namespace Node.Instance.Assets.Scripts.InstanceServer.Entries.XmlData.LootData
{
	public class SetScenarioStringLoot : ILoot
	{
		[XmlAttribute]
		public string Key
		{
			get { return key.Text; }
			set { key = new FormulaString("map p target", value); }
		}

		[XmlIgnore]
		public FormulaString key = new FormulaString("map p target", "'true'");

		[XmlAttribute]
		public string Value
		{
			get { return m_value.Text; }
			set { m_value = new FormulaString("map p target", value); }
		}

		[XmlIgnore]
		public FormulaString m_value = new FormulaString("map p target", "'true'");

		[XmlAttribute]
		public bool ForLogin;

		private void Reward(MapTargetObject target, InstancePerson person)
		{
			var pStatContaner = person.StatContainer;
			var tStatContaner = target?.GetStatContainer();

			var map = InstanceGameManager.Instance.GetMapByGameId(person.GameId);;

			var sKey = key.Calc(map, pStatContaner, tStatContaner);
			var sValue = m_value.Calc(map, pStatContaner, tStatContaner);

			if (ForLogin)
				person.login.scenarioStrings.Add(sKey, sValue);
			else
				person.ScenarioStrings.Add(sKey, sValue);
		}

		public override IEnumerable<IConcreteLoot> GetILoot(MapTargetObject target, InstancePerson person)
		{
			Reward(target, person);

			yield break;
		}
	}
}
