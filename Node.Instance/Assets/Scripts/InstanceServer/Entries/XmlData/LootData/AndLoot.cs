﻿using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Node.Instance.Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using UtilsLib;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData
{
	public class AndLoot : ILoot
	{
		[XmlElement(typeof(AndLoot))]
		[XmlElement(typeof(OrLoot))]
		[XmlElement(typeof(OrLootByPersonType))]
		[XmlElement(typeof(Resource))]
		[XmlElement(typeof(Gold))]
		[XmlElement(typeof(LifeSphere))]
		[XmlElement(typeof(Item))]
		[XmlElement(typeof(ItemById))]
		[XmlElement(typeof(RandomPotion))]
		[XmlElement(typeof(FromLootList))]
		[XmlElement(typeof(SetScenarioStringLoot))]
		public List<ILoot> Loots;

		public override IEnumerable<IConcreteLoot> GetILoot(MapTargetObject target, InstancePerson person)
		{
            foreach (var res in Loots)
            {
                var statContainer = person == null ? null : person.StatContainer;
                var targetStatContainer = target == null ? null : target.GetStatContainer();
                int chance = res.FChance.IntCalc(statContainer, targetStatContainer);
                if (FRRandom.CheckPercentChance(chance))
                    foreach (var loot in res.GetILoot(target, person))
                        yield return loot;
            }
		}
	}
}