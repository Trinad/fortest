﻿using Assets.Scripts.Networking.ClientDataRPC;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Serialization;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData
{
    [Serializable]
    public class ChestEntityDescriptionXML
    {
        [XmlAttribute]
        public string Icon;

        [XmlAttribute]
        public string NameKey;

        [XmlAttribute]
        public int MinCount;

        [XmlAttribute]
        public int MaxCount;

        [XmlAttribute]
        public string IconColor
        {
            get { return IconColorUint.ToString("X8"); }
            set { IconColorUint = uint.Parse(value, NumberStyles.HexNumber); }
        }

        [XmlAttribute]
        public string CounterColor
        {
            get { return CounterColorUint.ToString("X8"); }
            set { CounterColorUint = uint.Parse(value, NumberStyles.HexNumber); }
        }

        private uint IconColorUint;
        private uint CounterColorUint;

        public ChestEntityDescriptionXML()
        {
            MinCount = 1;
            MaxCount = 1;
            IconColor = "FFFFFFFF";
            CounterColor = "FFFFFFFF";
        }

        public ChestEntityDescription GetDescriptionForClient()
        {
            ChestEntityDescription clientDescription = new ChestEntityDescription();
            clientDescription.icon = Icon;
            clientDescription.nameKey = NameKey;
            clientDescription.minCount = MinCount;
            clientDescription.maxCount = MaxCount;
            clientDescription.iconColor = IconColorUint;
            clientDescription.counterColor = CounterColorUint;

            return clientDescription;
        }
    }    
}
