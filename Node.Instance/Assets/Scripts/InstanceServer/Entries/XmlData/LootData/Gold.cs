﻿using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.Logic.Enums;
using UtilsLib.BatiyScript;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Fragoria.Common.Utils;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData
{
	public class Gold : ILoot
	{
        [XmlAttribute]
        public string Count
        {
            get { return fCount.Text; }
            set { fCount = new FormulaX("p level", value); }
        }

        [XmlIgnore]
        public FormulaX fCount = new FormulaX("p level", "1");

        private GameXmlData gameXmlData;
        [XmlInject]
        public void Init(GameXmlData gameXmlData)
        {
            this.gameXmlData = gameXmlData;
        }

        public InventoryItemData GetItem(InstancePerson person)
        {
            return gameXmlData.ItemDataRepository.GetCopyInventoryItemData(ResourceType.Gold, fCount.IntCalc(person?.StatContainer, 1));
        }

		public override IEnumerable<IConcreteLoot> GetILoot(MapTargetObject target, InstancePerson person)
		{
			var item = GetItem(person);
			yield return new ConcreteItem(item);
		}

        public override ChestEntityDescription GetDescriptions(StatContainer statContainer)
        {
            ChestEntityDescription result = base.GetDescriptions(statContainer);

            //Может быть не настроено в xml
            if (result != null)
            {
                //Количество золота зависит от персонажа, пожтому переопределяем то, что было настроено в xml
                int count = fCount.IntCalc(statContainer, 1);
                result.minCount = count;
                result.maxCount = count;
            }

            return result;
        }
    }
}
