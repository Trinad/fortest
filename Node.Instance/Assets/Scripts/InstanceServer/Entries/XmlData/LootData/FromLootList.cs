﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking.ClientDataRPC;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData
{
	public class FromLootList : ILoot
	{
		[XmlAttribute]
		public string Name;

		[XmlAttribute]
		public int IconId;

		[XmlAttribute]
		public string TitleId;

		[XmlAttribute]
		public int Count;

		[XmlAttribute]
		public int Stars;

		public override IEnumerable<IConcreteLoot> GetILoot(MapTargetObject target, InstancePerson person)
		{
			var lootList = LootLists.Instance.GetLoots(Name);
			foreach (var loot in lootList.GetLootList(target, person))
				yield return loot;
		}

        public override ChestEntityDescription GetDescriptions(StatContainer statContainer)
        {
            ChestEntityDescription result = base.GetDescriptions(statContainer);

            //Если FromLootList не имеет собственного описания, то попробуем вытащить его из LootList
            if (result == null)
            {
                var lootList = LootLists.Instance.GetLoots(Name);
                result = lootList.ChestEntityDescription.GetDescriptionForClient();
            }

            return result;
        }
    }
}
