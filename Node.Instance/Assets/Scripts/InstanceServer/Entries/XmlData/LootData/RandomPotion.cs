﻿using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking.ClientDataRPC;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.LootData
{
	public class RandomPotion : AndLoot
	{
		[XmlAttribute]
		public int Count = 1;

		[XmlAttribute]
		public int CountMax = -1;

		[XmlAttribute]
		public int Stars;
	}
}
