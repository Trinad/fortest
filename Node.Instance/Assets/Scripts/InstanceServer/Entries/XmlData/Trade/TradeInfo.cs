﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UtilsLib.BatiyScript;
using UtilsLib.DegSerializers;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.Trade
{
    public class TradeInfoData : ICanCopy
    {
        [XmlAttribute]
        public DateTime StartDateTime = DateTime.MinValue;
        [XmlAttribute]
        public DateTime EndDateTime = DateTime.MaxValue;
		[XmlAttribute]
		public string Sku;
        [XmlAttribute]
        public int Cost;
        [XmlAttribute]
        public int PercentLabel;
        [XmlAttribute]
        public bool TopSales;
        [XmlAttribute]
        public ResourceType BuyCostType = ResourceType.Gold;
        [XmlAttribute]
        public bool FromChest;
        [XmlAttribute]
        public bool FromProgress;
        [XmlAttribute]
        public List<int> OfferIds = new List<int>();

        [XmlAttribute]
        public string UnlockRule
        {
            get { return fUnlockRule.Text; }
            set { fUnlockRule = new FormulaBool("p", value); }
        }

        [XmlIgnore]
        public FormulaBool fUnlockRule = new FormulaBool("p", "true");

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            TradeInfoData si = (TradeInfoData)value;

            stream.Write((long)(si.EndDateTime - TimeProvider.UTCNow).TotalSeconds);
            stream.Write(si.Cost);
            stream.Write(si.PercentLabel);
            stream.Write(si.TopSales);
            stream.Write(si.BuyCostType);
            stream.Write(si.FromChest);
            stream.Write(si.OfferIds);
            stream.Write(si.Sku);
			stream.Write(si.FromProgress);
        }
    }

    public struct TradeInfoList
    {
        [XmlElement("TradeInfoData", typeof(TradeInfoData))]
        public List<TradeInfoData> list;

        public TradeInfoData GetCurrentTradeOffer(DateTime time, InstancePerson p = null)
        {
            if (list != null)
                foreach (var offer in list)
					if (time < offer.EndDateTime && time > offer.StartDateTime && offer.fUnlockRule.Calc(p?.StatContainer))
						return offer;

            return null;
        }
    }
}
