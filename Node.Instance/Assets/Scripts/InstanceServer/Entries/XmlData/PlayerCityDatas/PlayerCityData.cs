﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData;
using Assets.Scripts.InstanceServer.Entries.XmlData.Trade;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.BatiyScript;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas
{
	//Город Игрока
	public class PlayerCityData
	{
		[XmlElement]
		public List<Category> Categories;

		[XmlElement]
		public List<BuildingKit> BuildingKits;

		[XmlElement]
		public List<Building> Buildings;

		public void Init()
		{
			foreach (var buildingKit in BuildingKits)
				buildingKit.Buildings = buildingKit.Buildings.Select(building => Buildings.First(x => x.Name == building.Name)).ToList();
		}
	}

	public class Category
	{
		public string Name => Type.ToString();
		public int Id => SortId;
		[XmlAttribute]
		public int SortId;
		[XmlAttribute]
		public bool Visible;
		[XmlAttribute]
		public bool LockPermanent;
		[XmlAttribute]
		public string Icon;
		[XmlAttribute]
		public string Title;
		[XmlAttribute]
		public LobbyBuildingCategory Type;

		[XmlElement]
		public List<Subcategory> Subcategories;

		public UnlockRuleList UnlockRules = new UnlockRuleList();
	}

	public class Subcategory
	{
		public string Name => Type.ToString();
		public int Id => SortId;
		[XmlAttribute]
		public int SortId;
		[XmlAttribute]
		public bool Visible;
		[XmlAttribute]
		public bool LockPermanent;
		[XmlAttribute]
		public string Icon;
		[XmlAttribute]
		public string Title;
		[XmlAttribute]
		public LobbyBuildingSubCategory Type;

		public UnlockRuleList UnlockRules = new UnlockRuleList();
	}

	//Набор
	public class BuildingKit: IElementInfo
    {
		private static int MaxId;
		private int? id;
		public int Id => id ?? (id = Interlocked.Increment(ref MaxId)).Value;

		[XmlAttribute]
		public string Name;
		[XmlAttribute]
		public int SortId;
		[XmlAttribute]
		public string Icon;
		[XmlAttribute]
		public string Title;
		[XmlAttribute]
		public LobbyBuildingCategory Category;
		[XmlAttribute]
		public LobbyBuildingSubCategory Subcategory;
		[XmlAttribute]
		public bool Visible;
		[XmlAttribute]
		public bool LockPermanent;
		[XmlAttribute]
		public bool IsTopSales;

        public UnlockRuleList UnlockRules = new UnlockRuleList();
		public TradeInfoList TradeInfos = new TradeInfoList { list = new List<TradeInfoData> { new TradeInfoData() } };

		[XmlElement]
		public List<Building> Buildings;

        public string GetName()
        {
            return Name;
        }
	}

	//Здания
	public class Building : IElementInfo
    {
		private static int MaxId;
		private int? id;
		public int Id => id ?? (id = Interlocked.Increment(ref MaxId)).Value;

		[XmlAttribute]
		public string Name;
		[XmlAttribute]
		public int SortId;
		[XmlAttribute]
		public string Icon;
		[XmlAttribute]
		public string Title;
		[XmlAttribute]
		public string Desc;
		[XmlAttribute]
		public string Prefab;
		[XmlAttribute]
		public LobbyBuildingCategory Category;
		[XmlAttribute]
		public LobbyBuildingSubCategory Subcategory;
		[XmlAttribute]
		public bool Visible;
		[XmlAttribute]
		public bool IsTopSales;
		[XmlAttribute]
		public bool Default;

		public List<LocalizationAttribute> LocalizationAttributes;

		public List<LocParameter> GetLocParameter()
		{
			return LocalizationAttributes.Select(a => new LocParameter {KeyName = a.Key, Value = (float) a.formula.Calc(0.0, null, 0.0, 0.0)}).ToList();
		}

		public UnlockRuleList UnlockRules = new UnlockRuleList();
		public TradeInfoList TradeInfos = new TradeInfoList { list = new List<TradeInfoData> { new TradeInfoData() } };

		[XmlElement("Stat")]
		public List<BuildingStat> BuildingStats;

        public string GetName()
        {
            return Name;
        }
    }

	public class BuildingStat
	{
		[XmlAttribute]
		public Stats Type;
		[XmlAttribute]
		public string Value
		{
			get { return valueFormula.Text; }
			set { valueFormula = new FormulaX("p", value); }
		}
		[XmlAttribute]
		public double? ClientValue;
		[XmlAttribute]
		public string Format = string.Empty;

		private FormulaX valueFormula = new FormulaX("p", "0");

		public double GetValue(StatContainer container)
		{
			return valueFormula.Calc(container);
		}
    }

	public class BuildingStatData
	{
		public Stats Type;
		public float Value;
		public string Format;

		public BuildingStatData(InstancePerson p, BuildingStat stat)
		{
			Type = stat.Type;
			Value = (float)(stat.ClientValue ?? stat.GetValue(p.StatContainer));
			Format = stat.Format;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			BuildingStatData si = (BuildingStatData)value;
			stream.Write((ushort)si.Type);
			stream.Write(si.Value);
			stream.WriteStringFromId(si.Format);
		}
	}

	public enum LobbyBuildingCategory : sbyte
	{
		None = -1, // Нет категории
		Set = 0, //Наборы
		Building = 1, //Здания
		Landscape = 2, //Ландшафт
		Sky = 3, //Небо
	}

	public enum LobbyBuildingSubCategory : sbyte
	{
		None = -1,
		// Здания
		Main = 0, //MainBase, // Основное здание
        Second = 1, //BigObject, // Дополнительное здание
        Gate = 2, // Ворота
		Fence = 3, // Забор - СЛ2
		Dynamic = 4, //DynamicObject, // Динамический объект(фишка)
		Stand = 5, //Сharstand, //Подставка под персом - СЛ2
		Devices = 6, //Generators, // Девайсы - СЛ2
		Portal = 7, // Портал - СЛ2
		// Окружение
		Terrane = 8, //Terrain, // Террейн, малый декор, дорога - СЛ2
		Static = 9, //Background, // Статический объект (задний план, горы и пр.) - СЛ2
		SkyAndWeather = 10, //Skybox // Небо и погода - СЛ2
	}

	public enum BuildingState
	{
		None = 0,
		Enable = 1,
		Selected = 2,
	}
}
