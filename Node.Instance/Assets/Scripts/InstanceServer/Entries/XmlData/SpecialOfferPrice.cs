﻿using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using Assets.Scripts.Networking;
using uLink;
using UtilsLib.BatiyScript;
using UtilsLib.ExternalLogs.Classes;

namespace Assets.Scripts.InstanceServer.Entries.XmlData
{
    //public class SpecialOfferInAppInfo
    //{
    //	[XmlAttribute]
    //	public int PointId;
    //	[XmlAttribute]
    //	public string Sku = string.Empty;
    //	[XmlAttribute]
    //	public int Price;

    //	[XmlAttribute]
    //	public string Emerald
    //	{
    //		get { return fEmerald?.Text; }
    //		set { fEmerald = new FormulaX("level", value); }
    //	}

    //	[XmlIgnore]
    //	public FormulaX fEmerald;
    //}

    public enum EffectImageType
    {
        None = -1,
        Gold = 0,
        Emerald = 1,
        Snowflake = 2,
		Heart = 3,
    }

    public class SpecialOfferGui
    {
        [XmlAttribute]
        public string PreviewIcon;
        [XmlAttribute]
        public string BackgroundImage;
        [XmlAttribute]
        public string ContentImage;
        [XmlAttribute]
        public EffectImageType EffectImageType = EffectImageType.None;

        [XmlAttribute]
        public bool isVisualBig;

        [XmlAttribute]
        public int SeparatePlus;
        [XmlAttribute]
        public int Discount = -1;
        [XmlAttribute]
        public string DiscountFormat;
        [XmlAttribute]
        public int Benefit = -1;
        [XmlAttribute]
        public string BenefitFormat;
        [XmlAttribute]
        public bool ShowOldPrice;

        public static object DeSerialize(BitStream stream, object[] codecoptions)
        {
            var si = new SpecialOfferGui();
            si.PreviewIcon = stream.Read<string>();
            si.BackgroundImage = stream.Read<string>();
            si.ContentImage = stream.Read<string>();
            si.EffectImageType = stream.Read<EffectImageType>();

            si.isVisualBig = stream.Read<bool>();

            si.SeparatePlus = stream.Read<int>();
            si.Discount = stream.Read<int>();
            si.Benefit = stream.Read<int>();
            si.ShowOldPrice = stream.Read<bool>();

            return si;
        }

        public static void Serialize(BitStream stream, object value, object[] codecoptions)
        {
            var si = (SpecialOfferGui)value;

            stream.WriteStringFromId(si.PreviewIcon);
            stream.WriteStringFromId(si.BackgroundImage);
            stream.WriteStringFromId(si.ContentImage);
            stream.Write(si.EffectImageType);

            stream.Write(si.isVisualBig);

            stream.Write(si.SeparatePlus);
            stream.Write(si.Discount);
            stream.Write(si.Benefit);
            stream.Write(si.ShowOldPrice);
        }
    }
}
