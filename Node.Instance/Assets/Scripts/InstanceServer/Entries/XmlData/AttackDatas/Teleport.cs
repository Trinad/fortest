﻿using System;
using System.Numerics;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils.Common;
using UtilsLib;
using Assets.Scripts.Utils;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.AttackDatas
{
	public class Teleport : SimpleAttack
	{

        [XmlAttribute]
        public float Radius = 4;

        public override void Cast(MonsterBehaviour owner, MapTargetObject target)
        {
            if (owner.IsDead)
                return;

            double angle = (FRRandom.Next(100) * 2 * Math.PI) / (100d);
            float x = (float)(target.position.X + Radius * Math.Sin(angle));
            float z = (float)(target.position.Z + Radius * Math.Cos(angle));
            Vector3 end = new Vector3(x, 0, z);

            end = Helper.UltraCorrectXYZPosition(monster.map, monster.position, end);
            monster.SendRpc(monster.map.ClientRpc.JinnTeleport, end);
            monster.bImmortality = true;
            monster.bMoveable = false;
            CoroutineRunner.Instance.Timer(0.6f, () =>
            {
                monster.position = end;

                CoroutineRunner.Instance.Timer(0.9f, () =>
                {
                    monster.bImmortality = false;
                    monster.bMoveable = true;
                });
            });
        }
    }


}
