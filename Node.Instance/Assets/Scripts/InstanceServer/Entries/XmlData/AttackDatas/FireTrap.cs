﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.AttackDatas
{
    public class FireTrap: SimpleAttack
    {
        private float halfDiagonalLength;

        public AuraXmlData DamageAura;

        public override void Init()
        {
            base.Init();

            halfDiagonalLength = (float)(MaxApplyRadius * Math.Sqrt(2)) / 2;
        }

        public override void Cast(MonsterBehaviour owner, MapTargetObject target)
        {
            base.Cast(owner, target);

            var area = new PolygonArea(GetPoints());
            DamageAura.CreateAura(monster, monster.position, monster.Level, area);
        }

        public List<Vector2> GetPoints()
        {
            var points = new List<Vector2>();
            points.Add(new Vector2(monster.position.X + halfDiagonalLength, monster.position.Z));
            points.Add(new Vector2(monster.position.X, monster.position.Z + halfDiagonalLength));
            points.Add(new Vector2(monster.position.X - halfDiagonalLength, monster.position.Z));
            points.Add(new Vector2(monster.position.X, monster.position.Z - halfDiagonalLength));

            return points;
        }
    }
}
