﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Conditions;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Logger;
using UtilsLib;
using UtilsLib.BatiyScript;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.AttackDatas
{
    public class TeleportToObj : SimpleAttack
    {
        [XmlAttribute]
        public List<string> TargetObjectName;

        private List<MonsterBehaviour> targetObjects;

        private bool active = false;

        public TeleportToObj()
        {
            Name = "start";
            PrevTime = 0.5f;
            AnimTime = 0.5f;
            Cooldown = 0f;
            GlobalCooldown = "0";
        }

        public override void Init()
        {
            base.Init();

            CoroutineRunner.Instance.RunNextFrame(InitOuts);
            
            monster.EntityReady += SendBool;
        }

        public void InitOuts()
        {
            targetObjects = new List<MonsterBehaviour>();
            foreach (var mapobject in monster.map.monsters)
            {
                if (mapobject != null && TargetObjectName.Contains(mapobject.ActiveData))
                {
                    ILogger.Instance.Send(mapobject.ActiveData + " один из выходов телепорта " + monster.ActiveData);
                    targetObjects.Add(mapobject);
                }
            }

            if (targetObjects.Count == 0)//пусть падает
                throw new Exception($"Teleport {monster.ActiveData} don't has endpoint on map");
        }

        public void SendBool(InstancePerson person)
        {
            monster.SendSetAnimatorParameterBoolResponse(person.syn, "active", active);
        }

        public override void Update(DateTime now)
        {
            bool activeNow = (Condition == null || Condition.Check(monster.map, monster, null));

            if (active != activeNow)
            {
                active = activeNow;
                monster.SendSetAnimatorParameterBoolResponse("active", active);
            }

            if (!active)
                return;

            base.Update(now);
        }

        public override void Cast(MonsterBehaviour owner, MapTargetObject target)
        {
            bool isTeleportSuccess = false;

            var targetPalce = targetObjects.Random();

            foreach (var p in monster.map.persons)
            {
                if (!CanApplyAtDistance(p, false))
                    continue;

                ILogger.Instance.Send("Teleport");
                
                p.TeleportPersonToPoint(targetPalce.position);

                var infl = InfluenceXmlDataList.Instance.GetInfluence(monster, InfluenceHelper.StunWithoutIcon, 1.3f, 1);
                p.GetStatContainer().AddInfluence(infl);

                isTeleportSuccess = true;
                break;
            }

            if (isTeleportSuccess)
            {
                CoroutineRunner.Instance.Timer(0.1f, () =>
                {
                    targetPalce.SendSetAnimatorParameterTriggerResponse("end");
                });
            }
        }
    }
}