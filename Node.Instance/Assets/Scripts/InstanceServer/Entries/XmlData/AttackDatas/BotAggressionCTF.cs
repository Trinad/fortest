﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using System;
using Assets.Scripts.Utils.Common;
using Assets.Scripts.InstanceServer.Addin;
using System.Linq;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic.Enums;
using System.Collections.Generic;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.AttackDatas
{
    public class BotAggressionCTF : MonsterBehaviourPart
    {
        private float nextRecalck = 0;
        private float interval = 0.5f;

        List<MapTargetObject> allies = new List<MapTargetObject>();
        List<MapTargetObject> enemies = new List<MapTargetObject>();
        Map map;
        Stats whantedFlag;

        public override void Init()
        {
            base.Init();
            OrderGroup = 100;

            map = monster.map;
            foreach (var p in map.GetAllLiveTarget())
            {
                if (p.TeamColor == monster.TeamColor)
                {
                    if(p.Id != monster.Id)
                        allies.Add(p);
                }
                else
                    enemies.Add(p);
            }

            whantedFlag = (monster.TeamColor == PlayerTeam.Team2) ? Stats.BlueFlag : Stats.RedFlag;
        }

        MapTargetObject GetNearestTarget(MapTargetObject mapObject)
        {
            MapTargetObject target = null;
            double minVal = double.MaxValue;
            foreach (var p in enemies)
            {
                if (!monster.CanAttackTarget(p) || p.GetStatContainer().GetStatValue(Stats.CanNotDie) != 0)
                    continue;

                float dist = (monster.position - p.position).sqrMagnitude();
                if (dist < minVal && monster.IsNearby(p))
                {
                    minVal = dist;
                    target = p;
                }
            }

            return target;
        }

        void UpdateAsCTF()
        {
            MapTargetObject target = null;
            MapTargetObject tryProtect = null;

            foreach (var ally in allies)
            {
                if (ally.GetStatContainer().GetStatValue(whantedFlag) > 0)
                {
                    tryProtect = ally;
                    break;
                }
            }

            if (tryProtect != null)
                target = GetNearestTarget(tryProtect);
            else
                target = GetNearestTarget(monster);

            monster.Target = target;
        }

        public override void Update(DateTime now)
        {
            if (nextRecalck > Time.time || monster.IsDead)
                return;

            nextRecalck = Time.time + interval;
            UpdateAsCTF();

            base.Update(now);
        }

    }
}
