﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.Utils.Common;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.AttackDatas
{
	//Ловушка с шипами
	public class PitTrap : SimpleAttack
	{
		[XmlAttribute]
		public float Depth = 0.3f;

		[XmlAttribute]
		public float Interval = 0.5f;

		[XmlAttribute]
		public float KnockBackRadius = 2;

		[XmlAttribute]
		public float FlyDuration = 0.8f;

		private float nextCast;

		private bool CheckDepth(MapTargetObject target)
		{
			//чтобы не дамажило когда вверх летишь
			if (target.Speed.Y > 0)
				return false;

			return target.position.Y - monster.position.Y < Depth;
		}

		public override void Init()
		{
			int yMax = Helper.GetCell(monster.position.Y + Depth);
			int xMin = Helper.GetCell(monster.position.X - MaxApplyRadius);
			int xMax = Helper.GetCell(monster.position.X + MaxApplyRadius);
			int zMin = Helper.GetCell(monster.position.Z - MaxApplyRadius);
			int zMax = Helper.GetCell(monster.position.Z + MaxApplyRadius);

			for (int x = xMin; x <= xMax; x++)
				for (int z = zMin; z <= zMax; z++)
				{
					int dH = yMax - monster.map.GetHeight(x, z);
					if (dH >= 0)
					{
						//Чтоб монстр сам не прыгал в яму
						for (int dx = -1; dx <= 1; dx++)
							for (int dz = -1; dz <= 1; dz++)
								monster.map.SetSurface(x + dx, z + dz, SurfaceType.PitTrap);

						monster.map.SetAirwayHeight(x, z, dH + 1); //Чтоб летающие мобы не падали на шипы
					}
				}

			//MapToPicture.SaveSurface(monster.map, $"D:\\path\\{monster.Id}.png");
		}

		public override void Update(DateTime now)
		{
			if (nextCast > Time.time)
				return;

			nextCast = Time.time + Interval;

			foreach (var p in monster.map.persons)
			{
				if (CheckDepth(p) && CanApplyAtDistance(p, false) && !p.IsAirway)
					Cast(monster, p);
			}

			foreach (var m in monster.map.monsters)
			{
				if (m == monster || !m.bMoveable)
					continue;

				if (CheckDepth(m) && CanApplyAtDistance(m, false) && !m.IsAirway)
					Cast(monster, m);
			}
		}

		public override void Cast(MonsterBehaviour owner, MapTargetObject target)
		{
			int dmg = formulaDmg.IntCalc((double)owner.Level, owner.GetStatContainer(), target.GetStatContainer());
			if (dmg == 0 || target.Hit(owner, dmg, DamageType, Elements.Neutral, out bool crit, absoluteHit: true, hitFxData: HitFxData/*, resetCast: ResetCast*/))
			{
				foreach (var action in HitActionList)
					action.Execute(owner.Level, target, owner, null);
			}

			/*if (!target.IsDead)
			{
				//откидываем по направлению движения. Если скорости нет, то по направлению взгляда
				var speedDir = target.Speed.Vector3ToVector2();
				var knockBackDir = speedDir.sqrMagnitude() > 0.05f ? speedDir : target.direction;
				var pos = target.position + knockBackDir.normalized().Vector2ToVector3() * KnockBackRadius;
				pos = monster.map.UltraCorrectYPosition(pos);

				var diff = pos - target.position;
				target.KnockBack(diff, FlyDuration, diff.magnitude(), force: true);
			}*/
		}
	}
}
