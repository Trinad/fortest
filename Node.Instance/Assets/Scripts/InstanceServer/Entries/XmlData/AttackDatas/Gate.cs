﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Utils.Common;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Conditions;
using Assets.Scripts.Utils.Logger;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.Networking;
using UtilsLib.Logic.Enums;
using System.Numerics;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.Networking.ClientDataRPC;
using Fragoria.Common.Utils;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.AttackDatas
{
    public class Gate : MonsterBehaviourPart
    {
        [XmlAttribute]
        public float Width = 2;

        [XmlAttribute]
        public bool PlayVideoClip;

        [XmlAttribute]
        public string MsgId;

		[XmlAttribute]
		public MinimapItemType Minimap;

        public FormulaCondition Condition;

		private bool active;
		private List<Vector2i> points;

        [XmlIgnore]
        public INotifyController notifyController;

        [XmlInject]
        public void Construct(INotifyController notifyController)
        {
            this.notifyController = notifyController;
        }

        public override void Update(DateTime now)
        {
            bool activeTemp = Condition.Check(monster.map, monster, monster.Target);

            if (activeTemp != active)
            {
                active = activeTemp;
                monster.SendSetAnimatorParameterBoolResponse ("open_door", active);
                monster.map.SetAdditionalTransparenty(points, active);
                ServerRpcListener.Instance.SendUpdateMapTransparencyResponse(monster.map, points, active);
                Sos.Debug("Gate " + active);

                if (active)
                {
                    if (PlayVideoClip)
                    {
                        var dir = new Vector3(0, -1, -1).normalized(); //утсанавливаем дополнительную камеру
                        var pos = monster.position + new Vector3(0, 0.5f, 0) - 4 * dir;
                        ServerRpcListener.Instance.SendRpc(monster.map.ClientRpc.PlayVideoClipResponse, pos, dir, 3f);
                    }

	                // показываем сообщение об открытии ворот
					if (!string.IsNullOrEmpty(MsgId))
					    notifyController.SendShowNotificationTehNameResponse(monster.map, NotifyState.CommonNotify, MsgId);

					if (Minimap != MinimapItemType.None)
					{
						if (active)
							monster.map.MinimapManager.SendRemoveObjectToAll(monster, Minimap);
						else
							monster.map.MinimapManager.SendAddObjectToAll(monster, Minimap);
					}
                }
            }
        }

        public override void Init()
        {
            monster.EntityReady += EntityReady;

			if (Minimap != MinimapItemType.None)
				monster.map.MinimapManager.MinimapResultByMapType += MinimapResultByMapType;

            points = new List<Vector2i>();
            var lineOrientation = GeometryAlgorithm.RotateMinus90(monster.direction).Vector2ToVector3();
            var rightEnd = monster.position - Width / 2 * lineOrientation;
            var leftEnd = monster.position + Width / 2 * lineOrientation;

            GeometryAlgorithm.ForEachCells(rightEnd.X, rightEnd.Z, leftEnd.X, leftEnd.Z, (x, y) =>
                {
                    points.Add(new Vector2i(x, y));
                    return true;
                });

            if (!active)
                monster.map.SetAdditionalTransparenty(points, active);

            base.Init();
        }

        public void EntityReady(InstancePerson person)
        {
            monster.SendSetAnimatorParameterBoolResponse (person.syn, "open_door", active);
            ServerRpcListener.Instance.SendUpdateMapTransparencyResponse(monster.map, points, active);
        }

		private void MinimapResultByMapType(InstancePerson person, List<MinimapData> minimapDatas)
		{
			if (!active)
				minimapDatas.Add(new MinimapData(monster, Minimap));
		}
    }
}
