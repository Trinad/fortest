﻿using System;
using System.Linq;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Utils.Common;
using Assets.Scripts.Networking;
using UtilsLib;
using System.Numerics;
using System.Xml.Serialization;
using Batyi.TinyServer;
using System.Collections.Generic;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.InstanceServer.Algorithms;
using Fragoria.Common.Utils;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Entries.XmlData.AttackDatas
{
    public class BuffStandTimers : MonsterBehaviourPart
    {
        [XmlAttribute]
        public int Cooldown = 10;

        [XmlAttribute]
        public int StartTimer = 10;

        [XmlAttribute("Ids")]
		public List<int> buffIds;

        private Buff BuffInfo;

        MapEffect MapActiveObjEffect = null;

        private float createDate = 0;

        [XmlInject]
        public void Construct(GameXmlData gameXmlData)
        {
            this.gameXmlData = gameXmlData;
            if (gameXmlData == null)
                throw new Exception("gameXmlData == null");
        }

        [XmlIgnore]
        public GameXmlData gameXmlData;

        public override void Update(DateTime now)
        {
            if (Time.time > createDate)
            {
                Cast(null);
                AddBuff();
            }
            if (MapActiveObjEffect == null)
                return;

            foreach (var target in monster.map.GetAllLiveTarget())
            {
                if (target == null || target.IsDead)
                    continue;
                if (target.characterType != CharacterType.Person)
                    continue;

                if (GeometryAlgorithm.InRadius(0.8f, target.position, monster.position))
                {
                    Cast(target);
                }
            }
        }

        public override void Init()
        {
            monster.ScenarioStrings.Add("IsBuff", "true");
            createDate = Time.time + StartTimer;

            monster.map.MinimapManager.MinimapResultByMapType += BuffPoints;
            base.Init();
        }

        private void BuffPoints(InstancePerson instancePerson, List<MinimapData> minimapDatas)
        {
            var data = new MinimapData(monster,
                BuffInfo == null ? MinimapItemType.BuffEmpty : BuffInfo.MinimapIconId);
            minimapDatas.Add(data);
        }


        public void AddBuff()
        {
            if (BuffInfo != null)
                return;

            int buffId = buffIds.Random();
            createDate = Time.time + Cooldown;

            BuffInfo = gameXmlData.Buffs.Find(x => x.BaseId == buffId);
			MapActiveObjEffect = monster.map.CreateFx(monster.position + new Vector3(0, 0.6f, 0), (EffectType)BuffInfo.FxId);// создаем тип бафа
			monster.map.MinimapManager.SendAddObjectToAll(monster, BuffInfo.MinimapIconId);
            monster.ScenarioStrings.Add("IsBuffPresent", "true");
        }

        public void Cast(MapTargetObject targetObject)
        {
            if (BuffInfo == null)
                return;

            monster.map.RemoveFx(MapActiveObjEffect);
			monster.map.MinimapManager.SendAddObjectToAll(monster, MinimapItemType.BuffEmpty);
			if (targetObject != null)
				ServerRpcListener.Instance.SendCreateFxResponse(targetObject, EffectType.GettingBuff, 0);
            MapActiveObjEffect = null;

			if (targetObject != null)
				BuffInfo.Use(targetObject);
			BuffInfo = null;
			monster.ScenarioStrings.Add("IsBuffPresent", "false");
        }
    }
}
