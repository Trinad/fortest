﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.XmlData
{
    public class EmotionSettings
    {
        [XmlArrayItem(typeof(EmotionXmlData))]
        public List<EmotionXmlData> EmotionList;
        public int SlotCount;
    }
}
