﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.XmlData
{
    public class EmotionXmlData
    {
        [XmlAttribute]
        public int Id;

        [XmlAttribute]
        public EmotionGroup Group;

        [XmlAttribute]
        public int SlotId = -1;
    }
}
