﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.Utils;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.BatiyScript;

namespace Assets.Scripts.InstanceServer.Entries
{
    public class DungeonXmlData
    {
        [XmlAttribute]
        public int EdgeId;
		[XmlAttribute]
        public int ImageId;
        [XmlAttribute]
        public string TechnicalName = string.Empty;
        public FormulaBool Enable;
	    public List<DungeonLocationXmlData> DungeonLocations;
    }

	public class DungeonLocationXmlData
	{
		[XmlAttribute]
		public string LevelName;
		[XmlAttribute]
        public int LocationId;
        [XmlAttribute]
        public int RecommendedGS;
        [XmlAttribute]
        public int Floor;
        [XmlAttribute]
        public int ImageId;

        public UnlockRuleList UnlockRules = new UnlockRuleList();

		[XmlIgnore]
		public EdgeLocationXmlData location;

		internal List<UnlockData> GetUnlockData(InstancePerson person)
        {
            List<UnlockData> res = new List<UnlockData>();

            res.Add(new UnlockData { TaskType = UnlockTaskType.BuyKeyStone, TaskGoal = ServerConstants.Instance.ToOpenFloorPrice, isDone = false });

            foreach (var r in UnlockRules.RulesList)
            {
                bool isDone = r.Check(person);
                res.Add(new UnlockData { TechnicalName = r.TechnicalName, TaskType = r.Type, TaskGoal = r.Count, isDone = isDone });
            }

            return res;
        }
    }
}
