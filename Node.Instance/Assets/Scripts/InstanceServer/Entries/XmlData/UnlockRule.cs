using System.Xml.Serialization;
using System.Collections.Generic;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.BatiyScript;

namespace Assets.Scripts.InstanceServer.Entries
{
    public class UnlockRule
    {
        [XmlAttribute]
        public UnlockTaskType Type;
        [XmlAttribute]
        public string TechnicalName = string.Empty;
        [XmlAttribute]
        public string Description = string.Empty;
        [XmlAttribute]
        public int Count;

        public UnlockRuleFormulaCondition FormulaCondition;

		public class UnlockRuleFormulaCondition
		{
			[XmlAttribute]
			public string Text
			{
				get { return fValue.Text; }
				set { fValue = new FormulaBool("map p Count", value); }
			}

			[XmlIgnore]
			public FormulaBool fValue = new FormulaBool("map p Count", "false");
		}

		public bool Check(InstancePerson person)
		{
			if (Type == UnlockTaskType.Collect)
			{
				int count, total;
				person.Collection.GetCount(out count, out total);
				return count == total && person.Collection.CanCollect(TechnicalName);
			}

			return FormulaCondition.fValue.Calc(person.syn != null ? person.syn.map : null, person.StatContainer, Count);
		}
    }

    public class UnlockRuleList
    {
        [XmlElement("UnlockRule", typeof(UnlockRule))]
        public List<UnlockRule> RulesList = new List<UnlockRule>();

        internal bool CanUnlock(InstancePerson person)
        {
            foreach (var r in RulesList)
                if (!r.Check(person))
                    return false;

            return true;
        }

        public int GetByType(UnlockTaskType type)
        {
            var rule = RulesList.Find(x => x.Type == type);
            return rule?.Count ?? 0;
        }
	}
}