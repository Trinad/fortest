﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils;
using Batyi.TinyServer;
using Node.Instance.Assets.Scripts.Utils;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.XmlData
{
    public class Buff : IActionList
    {
        [XmlAttribute]
        public int BaseId;

        [XmlAttribute]
        public int FxId;

        [XmlAttribute]
        public int InfluenceIdForTitle;

        [XmlAttribute]
        public string TechnicalName;

        [XmlAttribute]
        public MinimapItemType MinimapIconId = MinimapItemType.None;

        [XmlIgnore] private List<PersonType> types = new List<PersonType>(); // типы персонажей которые могут подбирать бафф

        [XmlAttribute]
        public string Types
        {
            get { return ConvertUtils.ConvertToString(types); }
            set { types = ConvertUtils.ConvertToListStr(value).Select(x => (PersonType)Enum.Parse(typeof(PersonType), x)).ToList(); }
        }

        public void Use(MapTargetObject person)
        {
            if (types == null || types.Count == 0 || types.Contains(person.personType))
            {
                if (person is PersonBehaviour personBehaviour)
                {
                    string technicalName = GetTechnicalName();
                    ServerRpcListener.Instance.SendRpc(personBehaviour.ClientRpc.PickUpBuffResponse, technicalName);
                }

                var context = new ContextIAction(person.map);
                context.target = person;
                CoroutineRunner.Instance.Timer(0.3f, () => person.map.Run(Actions, context));
            }
        }

        public string GetTechnicalName()
        {
            string technicalName = null;
            if (InfluenceIdForTitle != 0)
                technicalName = InfluenceXmlDataList.Instance.GetInfluenceTechnicalName(InfluenceIdForTitle);
            else
                technicalName = TechnicalName;

            return technicalName;
        }
    }
}
