﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using Fragoria.Common.Utils;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.ActiveObjects
{
	public class Interaction_Teleport : MapPortal
	{
        public const float TIME_ACTIVATE_SEC = 3;

		[XmlAttribute]
        public string PortalType;
		public PortalXmlData PortaData;

	    List<TeleportPerson> teleportPersons;

		[XmlIgnore]
	    public Action<InstancePerson> ExtendedEnterAction;

		[XmlIgnore]
		public EdgesMapController edgesMapController;
		[XmlIgnore]
		public MatchmakingController matchmakingController;

		[XmlInject]
		public void Construct(EdgesMapController _edgesMapController, MatchmakingController _matchmakingController)
		{
			edgesMapController = _edgesMapController;
			matchmakingController = _matchmakingController;
		}

        public override void MyUpdate()
	    {
	        if (teleportPersons != null && teleportPersons.Count > 0)
	        {
	            CheckTeleportPerson();
                RemoveNullPerson();
	        }
	    }

	    void CheckTeleportPerson()
	    {
	        var now = TimeProvider.UTCNow;

			foreach (var teleportPerson in teleportPersons.Where(teleportPerson => now > teleportPerson.timeFinish))
			{
				if(GetInteraction(teleportPerson.Person) == GameModeType.Interaction_Teleport)
				{
					//Открыть карту граней
					teleportPerson.EnterTeleport();
				}
				else
				{
					//Телепортировать
					matchmakingController.teleportOpenRequest(teleportPerson.Person);
					teleportPerson.HideProgress();
				}
			}
	    }

	    private void RemoveNullPerson()
	    {
            teleportPersons.RemoveAll(tp => tp.Person == null);
        }

		public override GameModeType GetInteraction(InstancePerson person)
		{
			if (person != null)
			{
				if (PortaData.IsActive != null && !PortaData.IsActive.Calc(person.syn.map))
					return person.syn.GameMode;
				if (PortaData.IsOwner)
					return person.PersonalParam == person.syn.map.LevelParam ? PortaData.GameModeType : GameModeType.Exploration;
				if (PortaData.IsEnemy)
					return person.PersonalParam != person.syn.map.LevelParam ? PortaData.GameModeType : GameModeType.Exploration;
				if (PortaData.AllLevelOpen)
				{
					var nextLevel = edgesMapController.GetNextLevel(person);
					return nextLevel == null || nextLevel.DisableFebVersion ? GameModeType.Interaction_Activate : GameModeType.Interaction_Teleport;
				}
			}

			return PortaData.GameModeType;
		}

		public override BuildingStatusType GetBuildingStatus(InstancePerson person)
		{
			if (PortaData.IsOwner && person.PersonalParam != person.syn.map.LevelParam)
				return BuildingStatusType.EnemyGray;
			if (PortaData.IsEnemy && person.PersonalParam == person.syn.map.LevelParam)
				return BuildingStatusType.Unavailable;

			return BuildingStatusType.Available;
		}

		public override ActualPlaceType GetActualPlace(InstancePerson person)
		{
			if (PortaData.IsOwner && person.PersonalParam != person.syn.map.LevelParam)
				return ActualPlaceType.None;
			if (PortaData.IsEnemy && person.PersonalParam == person.syn.map.LevelParam)
				return ActualPlaceType.None;

			return PortaData.ActualPlaceType;
		}

		public override ActualPlaceIcon GetActualPlaceIcon(InstancePerson person)
		{
			return PortaData.ActualPlaceIcon ?? base.GetActualPlaceIcon(person);
		}

		public override ActualPlaceTitle GetActualPlaceTitle(InstancePerson person)
		{
			return PortaData.ActualPlaceTitle ?? base.GetActualPlaceTitle(person);
		}

		public override MinimapItemType GetMinimapType(InstancePerson person)
		{
			if (PortaData.IsOwner && person.PersonalParam != person.syn.map.LevelParam)
				return MinimapItemType.None;
			//if (PortaData.IsEnemy && person.PersonalParam == person.syn.map.LevelParam)
			//	return MinimapItemType.None; //Владелец видит откуда нападают

			return PortaData.MinimapType;
		}

		public override void Init()
		{
            PortaData = InstanceGameManager.Instance.gameXmlData.PortalDatas.Find(x => x.PortalType == PortalType);

			teleportPersons = new List<TeleportPerson>();
        }

		public override string GetLevelName(InstancePerson person)
		{
			if (PortaData.GoToCamp)
				return person.gameXmlData.Edges.GetCampLevel(person).LevelName;

			return PortaData.LocName;
		}

		public override string GetLevelParam(InstancePerson person)
		{
            if (PortaData.IsPersonal)
				return person.PersonalParam;

			return string.Empty;
		}

	    public override ClientErrors TryEnter(InstancePerson person, bool useForce)
		{
            if (PortaData.IsActive != null && !PortaData.IsActive.Calc(person.syn.map))
                return ClientErrors.LOGIC_ERROR;

			return ClientErrors.SUCCESS;
		}

		internal override void Enter(InstancePerson person)
		{
            ExtendedEnterAction?.Invoke(person);
        }
	}

	public class TeleportPerson
	{
		public InstancePerson Person { get; private set; }
        public int PersonId { get; private set; }

		public DateTime timeStart;
		public DateTime timeFinish;

		public TeleportPerson(InstancePerson person, float timeSecActivate)
		{
			Person = person;
            PersonId = person.personId;
			Person.InDamage += _person_InDamage;
			Person.PersonDie += _person_PersonDie;
			Person.GameModeChange += Syn_GameModeChange;
			timeStart = TimeProvider.UTCNow;
			timeFinish = TimeProvider.UTCNow.AddSeconds(timeSecActivate);
			ILogger.Instance.Send("TeleportPerson: ", $"_person: {Person.PersonalParam}, GameMode: {Person.syn.GameMode}", ErrorLevel.info);

			if (timeSecActivate > 0)
				ServerRpcListener.Instance.SendEnterToTeleportResultResponse(Person, TeleportResult.StartActivate, timeSecActivate);
		}

		private void Syn_GameModeChange(GameModeType gameType)
		{
			HideProgress();
		}

		private void _person_InDamage(MapTargetObject attacker, ref int damage, bool resetCast)
		{
            if(resetCast)
                HideProgress();
		}

		private void _person_PersonDie(PersonBehaviour deadPerson)
		{
			if (Person.syn == deadPerson)
				HideProgress();
		}

		public void EnterTeleport()
		{
			ServerRpcListener.Instance.SendEnterToTeleportResultResponse(Person, TeleportResult.EnterNow, 0);
			Release();
		}

		public void HideProgress()
		{
			ServerRpcListener.Instance.SendEnterToTeleportResultResponse(Person, TeleportResult.Break, 0);
			Release();
		}

		protected virtual void Release()
		{
			timeFinish = DateTime.MaxValue;
			Person.InDamage -= _person_InDamage;
			Person.PersonDie -= _person_PersonDie;
			Person.GameModeChange -= Syn_GameModeChange;
			Person = null;
		}
	}
}
