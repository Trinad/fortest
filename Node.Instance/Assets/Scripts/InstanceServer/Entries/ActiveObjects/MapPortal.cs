﻿using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.ActiveObjects
{
	//Портал
	public class MapPortal : AInteraction_Activate
	{
		[XmlAttribute]
		public string LevelName;

		public override GameModeType GetInteraction(InstancePerson person)
		{
			return GameModeType.Interaction_Portal;
		}

		public override ActualPlaceType GetActualPlace(InstancePerson person)
		{
			return ActualPlaceType.Portal;
		}

		public override MinimapItemType GetMinimapType(InstancePerson person)
		{
			return MinimapItemType.Portal;
		}

		public virtual string GetLevelName(InstancePerson person)
		{
			return LevelName;
		}

		public virtual string GetLevelParam(InstancePerson person)
		{
			return string.Empty;
		}

		public override void TryActivate(InstancePerson person)
		{
			
		}

		public virtual ClientErrors TryEnter(InstancePerson person, bool useForce)
        {
			return ClientErrors.SUCCESS;
        }

		internal virtual void Enter(InstancePerson person)
		{

		}
	}
}
