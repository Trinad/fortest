﻿using System;
using System.Linq;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils.Common;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.ActiveObjects
{
    public class Interaction_Flag : MapActiveObject
	{
		[XmlAttribute]
		public PlayerTeam TeamColor;

		[XmlAttribute]
		public EffectType EffectId;

        [XmlAttribute]
        public EffectType AlternativeFxId;

        [XmlAttribute]
		public MinimapItemType MinimapIcon;

        [XmlAttribute]
		public int InfluenceId;

	    [XmlAttribute]
	    public float FlagReturnTimeSec = 10.0f;

	    [XmlAttribute]
	    public float PersonFlagReturnTimeSec = 3.0f;

        TeamMapEffect mapActiveObjTeamEffect;

        internal bool IsRed
		{
			get { return TeamColor == PlayerTeam.Team2; }
		}

		public bool OnBase = false;
        private DateTime createDate;

		public override void Init()
		{
            createDate = TimeProvider.UTCNow;
            mapActiveObjTeamEffect = new TeamMapEffect(map) { fxId = EffectId, position = pos };
            mapActiveObjTeamEffect.sourceTeam = TeamColor;
            mapActiveObjTeamEffect.alternativefxId = AlternativeFxId;
            map.CreateFx(mapActiveObjTeamEffect);
        }

		public override GameModeType GetInteraction(InstancePerson person)
		{
            return GameModeType.Battle;
		}

		public override ActualPlaceType GetActualPlace(InstancePerson person)
		{
			return ActualPlaceType.None;
		}

		public override MinimapItemType GetMinimapType(InstancePerson person)
		{
            return MinimapIcon;
		}

		private float flagDist = 1.0f;

        private bool canGetFlag(MapTargetObject pb)
        {
            if (pb == null)
                return false;

            if (pb.IsDead)
                return false;

            if (pb.isMotionControlledByServer)
                return false;

            return Helper.GetDistanceIgnoreY(pb.position, pos) < flagDist && TeamColor != pb.TeamColor;
        }

	    private bool canReturnFlag(MapTargetObject pb)
	    {
	        if (pb == null)
	            return false;

	        if (pb.IsDead)
	            return false;

	        if (pb.isMotionControlledByServer)
	            return false;

	        if (OnBase)
	            return false;

	        return Helper.GetDistanceIgnoreY(pb.position, pos) < flagDist && TeamColor == pb.TeamColor;
	    }

        private bool isBlink = false;
	    private InstancePerson instancePersonReturner = null;
	    private DateTime instancePersonReturnerEnterData;

        public override void MyUpdate()
        {
            //проверить время. если 10 секунд его не трогают и он не на базе - вернуть его туда...
            if (!OnBase && createDate.AddSeconds(FlagReturnTimeSec) < DateTime.UtcNow)
            {
                ReturnFlag();
                return;
            }

            if (!OnBase && instancePersonReturnerEnterData.AddSeconds(PersonFlagReturnTimeSec) < DateTime.UtcNow && instancePersonReturner != null)
            {
                ReturnFlag();
                return;
            }

            if (!OnBase && createDate.AddSeconds(FlagReturnTimeSec * 0.7) < DateTime.UtcNow && !isBlink)
            {
                ServerRpcListener.Instance.CtfFlagFxStartBlinkResponse(map, mapActiveObjTeamEffect.id);
                isBlink = true;
            }

            foreach (var p in map.GetAllLiveTarget())
            {
                if (canGetFlag(p))
                {
                    //взять флаг...
                    map.ActiveMapObjectRemove(this);
                    map.objects.Remove(target as MapObject);
                    map.DestroyMapObject(target as MapObject);
                    map.MinimapManager.SendRemoveObjectToAll(target, MinimapIcon);

                    if (mapActiveObjTeamEffect != null)
                        map.RemoveFx(mapActiveObjTeamEffect);
                    p.GetStatContainer().AddInfluence(InfluenceXmlDataList.Instance.GetInfluence(p, InfluenceId, int.MaxValue, 100));

                    map.OnGetFlagFromMap(p, this);
                    break;
                }
                // подходим к флагу и пытаемся его вернуть
                if (canReturnFlag(p) && instancePersonReturner == null)
                {
                    if (p is PersonBehaviour)
                    {
                        var person = p as PersonBehaviour;
                        instancePersonReturner = person.owner;
                        instancePersonReturnerEnterData = TimeProvider.UTCNow;
                        instancePersonReturner.InDamage += InstancePersonReturnerOnInDamage;
                        ServerRpcListener.Instance.CtfFlagProgressBar(instancePersonReturner, true, PersonFlagReturnTimeSec);
                    }
                }

                if (instancePersonReturner != null && !canReturnFlag(instancePersonReturner.syn))
                {
                    ResetInstancePersonReturner();
                }
            }

            base.MyUpdate();
        }

	    private void InstancePersonReturnerOnInDamage(MapTargetObject attacker, ref int damage, bool resetCast)
	    {
	        ResetInstancePersonReturner();
        }

	    private void ResetInstancePersonReturner()
	    {
	        ServerRpcListener.Instance.CtfFlagProgressBar(instancePersonReturner, false, PersonFlagReturnTimeSec);
	        instancePersonReturner.InDamage -= InstancePersonReturnerOnInDamage;
	        instancePersonReturner = null;
        }


        public void ReturnFlag()
	    {
	        map.ActiveMapObjectRemove(this);
	        map.objects.Remove(target as MapObject);
	        map.DestroyMapObject(target as MapObject);

	        map.MinimapManager.SendRemoveObjectToAll(target, MinimapIcon);

	        if (mapActiveObjTeamEffect != null)
                map.RemoveFx(mapActiveObjTeamEffect);

            if (instancePersonReturner != null)
	        {
	            ResetInstancePersonReturner();
	        }

            map.OnFlagReturnedToBase(this);
        }
    }
}
