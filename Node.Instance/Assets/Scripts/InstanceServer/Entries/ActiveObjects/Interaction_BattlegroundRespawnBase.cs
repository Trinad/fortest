﻿using System.Linq;
using System.Numerics;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Utils.Common;
using Assets.Scripts.Utils.JsonBlockClasses;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.ActiveObjects
{
    public class Interaction_BattlegroundRespawnBase : MapActiveObject
    {
		[XmlAttribute]
		public bool IsRed;

        [XmlAttribute]
        public MinimapItemType MinimapIcon;

        bool flagOnTheBase = false;

        private float flagDist = 1.0f;

        public override GameModeType GetInteraction(InstancePerson person)
        {
            if (person == null)
                return GameModeType.Interaction_BattlegroundRespawnBase;
            return GameModeType.Battle;
        }

        public override ActualPlaceType GetActualPlace(InstancePerson person)
        {
            return ActualPlaceType.None;
        }

        public void ResetFlag()
        {
            if (flagOnTheBase)
                return;

            flagOnTheBase = true;
            var target = CreateFlag(pos, IsRed);
            map.MinimapManager.SendAddObjectToAll(target, MinimapIcon);

        }

        internal MapObject CreateFlag(Vector3 position, bool needRed)
        {
            var objInfo = new MapObjectInfo
            {
                name = "ctf_flag",//"guild_altar_worldboss",
                id = MapTargetObject.objId++,
                ActiveData = needRed ? "Flag red" : "Flag blue",
            };

            var mapObject = map.CreateMapObject(objInfo, position);
            var flagamo = map.GetActiveMapObjectById<Interaction_Flag>(mapObject.Id);
            flagamo.OnBase = true;

            return mapObject;
        }

        public void FlagIsStolen(MapTargetObject p)
        {
            flagOnTheBase = false;
        }

        public override void MyUpdate()
        {
            Stats reqStat = IsRed ? Stats.BlueFlag : Stats.RedFlag;
            Stats enemyStat = !IsRed ? Stats.BlueFlag : Stats.RedFlag;

            MapTargetObject enemyWithFlag = null;
            var getAllLiveTarget = map.GetAllLiveTarget().ToList();
            enemyWithFlag = getAllLiveTarget.Find(p => p.GetStatContainer().GetStatValue(enemyStat) > 0);
            foreach (var p in getAllLiveTarget)
            {
                if (Helper.GetDistanceIgnoreY(p.position, pos) < flagDist)
                {
                    if (p.GetStatContainer().GetStatValue(reqStat) > 0 && (flagOnTheBase || enemyWithFlag == null))
                    {
                        map.OnFlagCaptured(p, IsRed);
                        break;
                    }
                }
            }
        }
        
    }
}
