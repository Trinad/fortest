﻿using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.ActiveObjects
{
	//Непись-Торговец
	public class MapNPC : MapActiveObject
	{
		[XmlAttribute]
		public int NpcId;

		[XmlAttribute]
		public ActualPlaceType PlaceType = ActualPlaceType.NpcTrader;

		[XmlAttribute]
		public ActualPlaceIcon PlaceIcon = ActualPlaceIcon.NpcTrader;

		[XmlAttribute]
		public ActualPlaceTitle PlaceTitle = ActualPlaceTitle.NpcTrader;

		public override GameModeType GetInteraction(InstancePerson person)
		{
			return GameModeType.Interaction_NPC;
		}

		public override ActualPlaceType GetActualPlace(InstancePerson person)
		{
			return PlaceType;
		}

		public override ActualPlaceIcon GetActualPlaceIcon(InstancePerson person)
		{
			return PlaceIcon;
		}

		public override ActualPlaceTitle GetActualPlaceTitle(InstancePerson person)
		{
			return PlaceTitle;
		}

		public override MinimapItemType GetMinimapType(InstancePerson person)
		{
			return MinimapItemType.Trader;
		}
	}
}
