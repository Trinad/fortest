﻿using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.ActiveObjects
{
    public class Interaction_Battleground : MapActiveObject
	{
        public override GameModeType GetInteraction(InstancePerson person)
        {
            return GameModeType.Interaction_Battleground;
        }

        public override ActualPlaceType GetActualPlace(InstancePerson person)
        {
            return ActualPlaceType.BattleGround;
        }

        public override MinimapItemType GetMinimapType(InstancePerson person)
        {
            return MinimapItemType.BattleGround;
        }
    }
}
