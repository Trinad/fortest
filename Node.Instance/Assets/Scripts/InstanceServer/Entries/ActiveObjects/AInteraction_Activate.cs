﻿using Assets.Scripts.InstanceServer.Addin;
using Zenject;

namespace Assets.Scripts.InstanceServer.Entries.ActiveObjects
{
	public abstract class AInteraction_Activate : MapActiveObject
	{
        //подошли и нажали шестеренку
        public abstract void TryActivate(InstancePerson person);
	}
}
