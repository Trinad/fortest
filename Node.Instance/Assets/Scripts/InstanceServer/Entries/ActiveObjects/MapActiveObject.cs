﻿using System.Numerics;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Common;
using UtilsLib.BatiyScript;
using UtilsLib.DegSerializers;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.ActiveObjects
{
	// Объект на локации, с которым можно взаимодействовать (прим.: появляется кнопка взаимодействия вместо оружия)
	public abstract class MapActiveObject: ICanCopy
	{
		[XmlAttribute]
		public string ObjectName;

		[XmlAttribute]
		public string ActiveData;

		[XmlAttribute]
		public string Condition
		{
			get { return fCondition.Text; }
			set { fCondition = new FormulaBool("map", value); }
		}

		[XmlIgnore]
		public FormulaBool fCondition = new FormulaBool("map", "true");

		[XmlIgnore]
		public MapTargetObject target;

		[XmlIgnore]
		public Map map;

        protected ServerConstants ServerConstants { get; private set; }

        public MapActiveObject()
	    {
            ServerConstants = ServerConstants.Instance;
	    }

		public virtual Vector3 pos
		{
			get { return target.position; }
		}

		public abstract GameModeType GetInteraction(InstancePerson person);
		public abstract ActualPlaceType GetActualPlace(InstancePerson person);

		public virtual ActualPlaceIcon GetActualPlaceIcon(InstancePerson person)
		{
			return (ActualPlaceIcon)(int)GetActualPlace(person);
		}

		public virtual ActualPlaceTitle GetActualPlaceTitle(InstancePerson person)
		{
			return (ActualPlaceTitle)(int)GetActualPlace(person);
		}

		public virtual MinimapItemType GetMinimapType(InstancePerson person)
		{
			return MinimapItemType.None;
		}

		public virtual bool Enable
		{
			get { return true; }
		}

        public virtual BuildingStatusType GetBuildingStatus(InstancePerson person)
        {
            var gameMode = GetInteraction(person);
            if (gameMode == GameModeType.Interaction_Inactive)
                return BuildingStatusType.Unavailable;

            return BuildingStatusType.Available;
        }

        public virtual void Init()
		{

		}

		public virtual void MyUpdate()
		{

		}

		[XmlIgnore]
		public EffectType CurrentFxId { get; set; } //Эффект будет проигрываться на выбранном объекте
		[XmlIgnore]
		public Vector3 CurrentFxPos { get; set; }

        internal virtual float GetDistance(PersonBehaviour personBehaviour)
        {
            return Helper.GetDistanceIgnoreY(personBehaviour.position, pos) - target.Radius;
        }
    }
}
