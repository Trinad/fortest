﻿using System.Numerics;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.InstanceServer.Entries
{
	public class MapEffect
	{
		private static int _objId = 3000000;
        
        public int id { get; }
		public EffectType fxId;
		public Map map;
        /// <summary>
        /// Эту позицию можно использовать как глобальную, так и локальнаю, в зависимости от того какие настройки забиты в fx.xml на клиенте
        /// </summary>
		public Vector3 position;
        public Vector3? endPosition;
		public int upgrade;
        public float scale = 1;
        public int ownerId = 0;

		public MapEffect(Map map, int effectId = -1)
		{
		    id = effectId == -1 ? GetNewEffectId() : effectId;
		    this.map = map;
		}

        public virtual EffectType GetFxId(PersonBehaviour personBehaviour)
        {
            return fxId;
        }


        public static int GetNewEffectId()
	    {
	        return _objId++;
	    }
	}
}
