﻿using Assets.Scripts.Networking.ClientDataRPC;
using System;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries.Person;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using System.Linq;
using UtilsLib.DegSerializers;
using System.Collections;
using Assets.Scripts.InstanceServer.Entries.Login.DataRPC;
using Fragoria.Common.Utils;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.Perks;

namespace Assets.Scripts.InstanceServer.Entries
{
    //Используется Hashtable_Serializer сохраняет в БД
    public class InstancePersonSqlData
    {
        public int SlotId;
        public int PersonId;
        public int AnalyticId;
        public PersonType Type = PersonType.None;
        public FractionType Fraction;
        public string PrefabName;
        public int HeroPower;
        public DateTime LogoutTime;
        public bool bRunTutorial;
        public int TutorialState;
        public int EnableEdgeLocationId;
        public List<AbilitySaveData> Abilities = new List<AbilitySaveData>();
		public List<AbilityStatus> AbilityStatuses = new List<AbilityStatus>();
        public List<List<AbilitySaveData>> PassiveAbilitiesBuilds = new List<List<AbilitySaveData>>();
        public PerksCollection PerksCollection = new PerksCollection();
        public List<MailData> Mails = new List<MailData>();
        public List<string> ScenarioStrings = new List<string>();
        public List<DungeonPersonFloorData> DungeonData = new List<DungeonPersonFloorData>();
        public List<InfluenceSaveData> InfluenceDatas = new List<InfluenceSaveData>();
        public List<Emotion> AvailableEmotions = new List<Emotion>();
        public List<Emotion> AvailableEmotionsForBuy = new List<Emotion>();
        public List<string> ItemsPotionsLast2 = new List<string>();
        public List<string> UpgradeItemLevels = new List<string>();
        public int rating_elo_team;
        public int rating_elo_coop;
        public int GuildId;
        public int Experience;
        public int Level = 1;
        public int activeTalentBuild;
        public bool isNeedHideHelmet;
		public List<string> Notifications = new List<string>();
		public int SessionId;

		#region  Эти поля сохранять отдельно
		[DegHashtableIgnore]
		public List<ItemSaveData> Items2 = new List<ItemSaveData>();
		[DegHashtableIgnore]
		public List<ItemSaveData> ItemsPotions2 = new List<ItemSaveData>();
		[DegHashtableIgnore]
		public List<ItemSaveData> ItemsEquiped2 = new List<ItemSaveData>();
		[DegHashtableIgnore]
		public List<string> TradeItems2 = new List<string>(); //Хранить только просмотренные
		#endregion

		public InstancePersonSqlData()
		{
		}

        public InstancePersonSqlData(InstancePerson person)
        {
            SlotId = person.SlotId;
            AnalyticId = person.AnalyticId;
            PersonId = person.personId;
            Type = person.personType;
            Fraction = person.personFraction;
            PrefabName = person.PrefabName;
            HeroPower = person.HeroPower;
            rating_elo_coop = person.rating_elo_coop;
            rating_elo_team = (int)person.StatContainer.GetStatValue(Stats.EloRatingTeam);
            LogoutTime = person.LogoutTime;
            bRunTutorial = person.TutorialStateAdd > 0 ? person.bRunTutorialOld : person.bRunTutorial;
            TutorialState = person.TutorialState;
            EnableEdgeLocationId = person.EnableEdgeLocationId;

			//Скины не сохранять, они хранятся у логина
            Items2 = person.items.Where(x => x.IsItem && x.ItemType != ItemType.Skin).Select(x => x.GetItemSaveData()).ToList();
            ItemsEquiped2 = person.equipedItems.GetItemsForSave();
            ItemsPotions2 = person.potions.GetItemsForSave(true);
            ItemsPotionsLast2 = person.potions.LastItems;
            UpgradeItemLevels = person.UpgradeLevelsCollection.GetSaveData();
            TradeItems2 = person.TradeItems.Where(x => x.IsItem && !x.IsNew).Select(x => x.Name).ToList();

            Abilities = person.SaveAbilities();
			AbilityStatuses = person.AbilityStatuses;
            PassiveAbilitiesBuilds = person.passiveAbilityStorage.SavePassiveAbilities();
            PerksCollection = person.perksCollection;
            DungeonData = person.DungeonPersonLocationDatas;

            ScenarioStrings = person.ScenarioStrings.GetSaveData();

            InfluenceDatas = person.StatContainer.GetInfluenceDatas();
            GuildId = person.GuildId;
            AvailableEmotions = person.EmotionPerson.AvailableEmotions;
            AvailableEmotionsForBuy = person.EmotionPerson.AvailableEmotionsForBuy;

            Experience = (int)person.StatContainer.GetStatValue(Stats.Exp);
            Level = person.Level;
            activeTalentBuild = person.passiveAbilityStorage.ActiveTalentBuild;
            isNeedHideHelmet = person.isNeedHideHelmet;
			Notifications = person.Notifications;
			SessionId = person.SessionId;
		}

        public InstancePersonSqlData(PersonType type, FractionType fraction, int slotId)
		{
            SlotId = slotId;
            Type = type;
            Fraction = fraction;
			PrefabName = Type.ToString().ToLower();
		}
    }
}
