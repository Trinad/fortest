﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries
{
	public class NeedForResource
	{
		public bool IsSuccess = true;
		public Dictionary<ResourceType, int> Resources = new Dictionary<ResourceType, int>();

		public void NeedResource(ResourceType resType, int count)
		{
			if (count <= 0)
				return;

			IsSuccess = false;

			if (Resources.ContainsKey(resType))
				Resources[resType] += count;
			else
				Resources[resType] = count;
		}

		public void Clear()
		{
			IsSuccess = true;
			Resources.Clear();
		}

		public ErrorData GetError(InstancePerson person)
		{
			var errorData = new ErrorData();
			if (!IsSuccess)
			{
				errorData.Error = ClientErrors.NOT_ENOUGH_RESOURCES;
				errorData.ItemsNeeded = Resources.Select(x => 
                        person.gameXmlData.ItemDataRepository.GetCopyInventoryItemData(x.Key, x.Value)
                    ).ToList();
			}

			return errorData;
		}
	}
}
