﻿using Assets.Scripts.InstanceServer.Algorithms;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.AdditionalObjects
{
    public class DefenceWall
    {
        public Vector3 point1;
        public Vector3 point2;
        public PlayerTeam team = PlayerTeam.None;

        float Area(Vector2 a, Vector2 b, Vector2 c)
        {
            return (b.X - a.X) * (c.Y - a.Y) - (b.Y - a.Y) * (c.X - a.X);
        }

        public bool CheckIntersect(Vector3 point3, Vector3 point4)
        {
            return Intersect(point1.Vector3ToVector2(), point2.Vector3ToVector2(), point3.Vector3ToVector2(), point4.Vector3ToVector2());
        }

        bool Intersect(Vector2 a, Vector2 b, Vector2 c, Vector2 d)
        {
            return Area(a, b, c) * Area(a, b, d) <= 0
                && Area(c, d, a) * Area(c, d, b) <= 0;
        }

        public bool CheckCircleIntersect(Vector3 positon, float radius)
        {
            Vector3 localPoint1 = point1 - positon;
            Vector3 localPoint2 = point2 - positon;

            float dx = localPoint2.X - localPoint1.X;
            float dy = localPoint2.Z - localPoint1.Z;

            float a = dx * dx + dy * dy;
            float b = 2 * (localPoint1.X * dx + localPoint1.Z * dy);
            float c = localPoint1.X * localPoint1.X + localPoint1.Z * localPoint1.Z - radius * radius;

            if (b >= 0)
                return c < 0;
            if (b >= -2 * a)
                return (b * b - 4 * a * c) >= 0;

            return (a + b + c < 0);
        }
    }
}
