﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UtilsLib.NetworkingData;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using UtilsLib.Logic.Enums;
using System.Text;
using Assets.Scripts.InstanceServer.Entries.ActiveObjects;
using Assets.Scripts.InstanceServer.Entries.Login.DataRPC;
using Assets.Scripts.Utils;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.PassiveAbilitys;
using Assets.Scripts.InstanceServer.Analytic;

namespace Assets.Scripts.InstanceServer.Entries
{
    public partial class Map
    {
        public event Action<MonsterBehaviour> MonsterResurrect;
        public event Action<MonsterBehaviour> MonsterSetOnMap;
        public event Action<MonsterBehaviour, InstancePerson> MonsterKilledByPerson;
        public event Action<MonsterBehaviour, MapTargetObject> MonsterDie;
        public event Action<MonsterBehaviour> MonsterSetHealth;
        public event Action<InstancePerson, MapTargetObject> PersonDie;
        public event Action<InstancePerson> PersonResurrect;
        public event Action<MapTargetObject, MapTargetObject, int> PersonHeal;
        public event Action<MapTargetObject, MapTargetObject, int> OnOutDamage;
        public event Action<MapTargetObject, Interaction_Flag> GetFlagFromMap;
        public event Action<Interaction_Flag> FlagReturnedToBase;
        public event Action<int> CheatEndMatchEvent;
        public event Action<List<ArenaPlayerInfo>,StatisticBase> EndMatch;
        public event Action<InstancePerson> LeaveArenaEvent;
        public event Action<MapTargetObject, bool> FlagCaptured;
		public event Action<MapTargetObject, PersonBehaviour> CreateActiveObject;
        public event Action<MapTargetObject> PersonJump;

        public event Action<InstancePerson, bool> InitPerson;
        public event Action<PersonBehaviour> CreatePersonBehaviour;
        public event Action<InstancePerson> VisitLocation;
        public event Action<InstancePerson, bool> PersonSetOnMapRequest;
        public event Action<InstancePerson, bool, bool> PlayerConnected;
        public event Action<int> LoginEnterTheGame;
        public event Action<InstancePerson> PlayerDisconnected;
        public event Action<InstancePerson> LeaveLocation;
        public event Action<InstancePerson> EntityReadyRequest;
        public event Action<InstancePerson> MoveDirectionRequest;
        public event Action<InstancePerson> PlayerAFK;
        public event Action<InstancePerson> LeftLocation;

        public event Action<InstancePerson, LobbyInfo> GetLobbyInfoRequest;
        public event Action<InstancePerson> TeleportPerson;
        public event Action<InstancePerson, SyncReason> SyncPerson;
        public event Action<InstancePerson> NewPersonCheck;
        public event Action<InstancePerson, int, int> PersonLevelUp;
        public event Action<InstancePerson> TimeOneSecCheck;//персонаж просит проверить его квесты. может быть ему пора выдавать новые
        public event Action<QuestItemData, InstancePerson> QuestDone;//персонаж выполнил квест
        public event Action<QuestItemData, InstancePerson> QuestProgress;
        public event Action<QuestItemData, InstancePerson, bool> QuestGranted;
        public event Action<QuestItemData, InstancePerson> QuestRejected;
        public event Action<MapTargetObject, MapTargetObject> KillAnnounce;//персонаж убил персонажа
        public delegate void PlayerDeathInfoResponseDelegate(Map map, InstancePerson person, MapTargetObject attacker);
        public event PlayerDeathInfoResponseDelegate PlayerDeathInfoResponse;//персонаж умер. надо об этом уведомить и цену сообщить.

        public event Action<InstancePerson, InventoryItemData> PersonUsePotion;
        public event Action<InstancePerson, AnlyticItemInfo> PersonEquipItem;
        public event Action<InstancePerson, PassiveAbilityBase> PersonPassiveAbilityLearned;
        public event Action<InstancePerson, int> PersonAddExp;

        public event Action<InstancePerson,string> PersonOpenForm;
        public event Action Stopping;

        public void SetPlayerDeathInfoResponseDelegate(PlayerDeathInfoResponseDelegate playerDeathInfoResponseDelegate)
        {
            PlayerDeathInfoResponse = playerDeathInfoResponseDelegate;
        }
        public void OnPlayerDeathInfoResponse(InstancePerson person, MapTargetObject attacker)
        {
            PlayerDeathInfoResponse?.Invoke(this, person, attacker);
        }
        public event Action<PersonBehaviour, ShopManager, bool> ResurrectRequestAction;
        public void SetResurrectRequestDelegate(Action<PersonBehaviour, ShopManager, bool> action)
        {
            ResurrectRequestAction = action;
        }
        public void OnResurrectRequest(PersonBehaviour person, ShopManager shopManager, bool isEmeralds)
        {
            ResurrectRequestAction?.Invoke(person, shopManager, isEmeralds);
        }

        public event Action<InstancePerson> PersonChangeGuild;//персонаж вступил/вышел из гильдии
		public event Action<InstancePerson, PricePoint> PricePointBuy;

        public void OnMonsterResurrect(MonsterBehaviour monsterBehaviour)
        {
			MonsterResurrect?.Invoke(monsterBehaviour);
		}

        public void OnMonsterSetOnMap(MonsterBehaviour monsterBehaviour)
        {
            MonsterSetOnMap?.Invoke(monsterBehaviour);
        }

        public void OnMonsterDie(MonsterBehaviour monsterBehaviour, MapTargetObject attacker)
        {
            try
            {
                var person = attacker as PersonBehaviour;
                if (person != null && person.owner != null)
                {
                    MonsterKilledByPerson?.Invoke(monsterBehaviour, person.owner);

                    person.owner.AddExperience(monsterBehaviour);
                }
                KillRateCounter.AddKill();

                MonsterDie?.Invoke(monsterBehaviour, attacker);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnMonsterDie Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

        public void OnMonsterSetHealth(MonsterBehaviour monsterBehaviour)
        {
			MonsterSetHealth?.Invoke(monsterBehaviour);
		}

        public void OnPersonDie(InstancePerson instancePerson, MapTargetObject attacker)
        {
            try
            {
                PersonDie?.Invoke(instancePerson, attacker);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"map PersonDie exception in:{ex.StackTrace} message:{ex.Message}", ErrorLevel.error);
            }
		}

		public void OnPersonResurrect(InstancePerson instancePerson)
        {
			PersonResurrect?.Invoke(instancePerson);
		}

        public void OnPersonHeal(MapTargetObject target, MapTargetObject attacker, int heal)
        {
            if (PersonHeal != null)
                PersonHeal(target, attacker, heal);
        }

        public void OnTargetOutDamage(MapTargetObject attacker, MapTargetObject target, int damage)
        {
			OnOutDamage?.Invoke(attacker, target, damage);
		}

        public void OnFlagReturnedToBase(Interaction_Flag flag)
        {
            FlagReturnedToBase?.Invoke(flag);
        }

        public void OnGetFlagFromMap(MapTargetObject person, Interaction_Flag flag)
        {
            GetFlagFromMap?.Invoke(person, flag);
        }

        public void OnFlagCaptured(MapTargetObject person, bool IsRed)
        {
			FlagCaptured?.Invoke(person, IsRed);
		}

        public void OnPersonJump(MapTargetObject person)
        {
            PersonJump?.Invoke(person);
        }

        public void CheatEndMatch(int sec = 0)
        {
            CheatEndMatchEvent?.Invoke(sec);
        }

        public void OnEndMatch(List<ArenaPlayerInfo> personIds, StatisticBase statistic)
        {
            EndMatch?.Invoke(personIds, statistic);
        }

        public void OnLeaveArena(InstancePerson instancePerson)
        {
            try
            {
                LeaveArenaEvent?.Invoke(instancePerson);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnLeaveArena Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

		public void OnCreateActiveObject(MapTargetObject target, PersonBehaviour owner = null)
		{
			CreateActiveObject?.Invoke(target, owner);
		}

        public void OnInitPerson(InstancePerson person, bool reconnect)
        {
            try
            {
                InitPerson?.Invoke(person, reconnect);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnInitPerson Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

        public void OnCreatePersonBehaviour(PersonBehaviour person)
        {
            CreatePersonBehaviour?.Invoke(person);
        }

		public void OnPersonSetOnMapRequest(InstancePerson person, bool reconnect)
		{
			PersonSetOnMapRequest?.Invoke(person, reconnect);
		}

        public void OnVisitLocation(InstancePerson person)
        {
			VisitLocation?.Invoke(person);
		}

        public void OnPlayerConnected(InstancePerson person, bool reconnect, bool changePerson)
        {
            PlayerConnected?.Invoke(person, reconnect, changePerson);
        }

        public void OnLoginEnterTheGame(int loginId)
        {
            try
            {
                LoginEnterTheGame?.Invoke(loginId);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnLoginEnterTheGame Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

        public void OnPlayerDisconnected(InstancePerson person)
        {
            PlayerDisconnected?.Invoke(person);
        }

        public void OnLeaveLocation(InstancePerson person)
        {
			LeaveLocation?.Invoke(person);
		}

        public void OnEntityReadyRequest(InstancePerson instancePerson)
        {
			EntityReadyRequest?.Invoke(instancePerson);
		}

		public void OnMoveDirectionRequest(InstancePerson instancePerson)
		{
			MoveDirectionRequest?.Invoke(instancePerson);
		}
        public void OnPlayerAFK(InstancePerson person)
        {
            try
            {
                PlayerAFK?.Invoke(person);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnPlayerAFK Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }


        public void OnLeftLocation(InstancePerson person)
        {
			LeftLocation?.Invoke(person);
		}

        public void OnNewPersonCheck(InstancePerson person)
        {
			NewPersonCheck?.Invoke(person);
		}

		public void OnPersonLevelUp(InstancePerson person, int oldLevel, int newLevel)
		{
			PersonLevelUp?.Invoke(person, oldLevel, newLevel);
		}

        public void OnQuestDone(QuestItemData quest, InstancePerson person)
        {
            try
            {
                QuestDone?.Invoke(quest, person);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnQuestDone Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

        public void OnQuestProgress(QuestItemData quest, InstancePerson person)
        {
            try
            {
                QuestProgress?.Invoke(quest, person);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnQuestProgress Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

        public void OnQuestRejected(QuestItemData quest, InstancePerson person)
        {
            try
            {
                QuestRejected?.Invoke(quest, person);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnQuestRejected Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

        public void OnQuestGranted(QuestItemData quest, InstancePerson person, bool isFirstGranted = false)
        {
            try
            {
                QuestGranted?.Invoke(quest, person, isFirstGranted);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnQuestGranted Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

        public void OnTimeOneSecCheck(InstancePerson person)
        {
			TimeOneSecCheck?.Invoke(person);
		}

		public void OnPersonChangeGuild(InstancePerson person)
		{
			PersonChangeGuild?.Invoke(person);
		}

		public void OnPricePointBuy(InstancePerson person, PricePoint pricePoint)
		{
			PricePointBuy?.Invoke(person, pricePoint);
		}

        public void OnTeleport(InstancePerson person)
        {
			TeleportPerson?.Invoke(person);
		}

        public void OnSyncData(InstancePerson person, SyncReason reason)
        {
            //много логики подписывается на это событие. Если быдет падать внутри, то застопорится сохранение, поэтому обернул в try
            try
            {
                SyncPerson?.Invoke(person, reason);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnSyncData Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

        public void OnKillAnnounce(MapTargetObject killer, MapTargetObject target)
        {
			KillAnnounce?.Invoke(killer, target);
		}

        public void OnGetLobbyInfoRequest(InstancePerson person, LobbyInfo loginInfo)
        {
            GetLobbyInfoRequest?.Invoke(person, loginInfo);
        }

        public void OnPersonUsePotion(InstancePerson instancePerson, InventoryItemData inventoryItemData)
        {
            try
            {
                PersonUsePotion?.Invoke(instancePerson, inventoryItemData);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnPersonUsePotion Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

        public void OnPersonEquipItem(InstancePerson instancePerson, AnlyticItemInfo anlyticItemInfo)
        {
            try
            {
                PersonEquipItem?.Invoke(instancePerson, anlyticItemInfo);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnPersonGotItem Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

        
        public void OnPersonPassiveAbilityLearned(InstancePerson instancePerson, PassiveAbilityBase abilityBase)
        {
            try
            {
                PersonPassiveAbilityLearned?.Invoke(instancePerson, abilityBase);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnPersonPassiveAbilityLearned Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

        
        public void OnPersonAddExp(InstancePerson instancePerson, int exp)
        {
            try
            {
                PersonAddExp?.Invoke(instancePerson, exp);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnPersonPassiveAbilityLearned Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

        public void OnPersonOpenForm(InstancePerson instancePerson)
        {
            try
            {
                PersonOpenForm?.Invoke(instancePerson, string.Empty);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("OnPersonOpenForm Exception: " + ex.ToString(), ErrorLevel.exception);
            }
        }

        public void OnStopping()
        {
			try
			{
				Stopping?.Invoke();
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send("OnStopping Exception: " + ex, ErrorLevel.exception);
			}
        }

        private Dictionary<string, double> variablesDouble = new Dictionary<string, double>();
        private Dictionary<string, bool> variablesBool = new Dictionary<string, bool>();
        private Dictionary<string, string> variablesString = new Dictionary<string, string>();
        public readonly Dictionary<string, MyTrigger> MyTriggerDic = new Dictionary<string, MyTrigger>();
        public readonly Dictionary<string, MyPeriodTrigger> MyPeriodTriggerDic = new Dictionary<string, MyPeriodTrigger>();

        public void RunMyTrigger(string nameTrigger, ContextIAction context)
        {
            MyTrigger t = null;
            if (MyTriggerDic.TryGetValue(nameTrigger, out t))
                t.Check(context);
            else
                ILogger.Instance.Send($"Trigger {nameTrigger} not found", ErrorLevel.error);
        }

        public void RunMyTrigger(string nameTrigger)
        {
            RunMyTrigger(nameTrigger, new ContextIAction(this));
        }

        public void SendCheatScenarioStrings(InstancePerson person)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("=====числовые переменные=====");
            foreach (var pair in variablesDouble)
                sb.AppendLine(pair.Key + " : " + pair.Value);

            sb.AppendLine("=====логические переменные=====");
            foreach (var pair in variablesBool)
                sb.AppendLine(pair.Key + " : " + pair.Value);

            sb.AppendLine("=====строковые переменные=====");
            foreach (var pair in variablesString)
                sb.AppendLine(pair.Key + " : " + pair.Value);

			Cheats.SendCheatChatMessage(sb.ToString(), person);
        }

        public void SetVariableDouble(string name, double value)
        {
            ILogger.Instance.Send("SetVariable name " + name + " value " + value);
            variablesDouble[name] = value;
        }

        public double GetVariableDouble(string name)
        {
            double value;
            variablesDouble.TryGetValue(name, out value);
            //ILogger.Instance.Send("GetVariable name " + name + " value " + value);
            return value;
        }

        public void SetVariableBool(string name, bool value)
        {
            ILogger.Instance.Send("SetVariable name " + name + " value " + value);
            variablesBool[name] = value;
        }

        public bool GetVariableBool(string name)
        {
            bool value;
            variablesBool.TryGetValue(name, out value);
            return value;
        }

        public void SetVariableString(string name, string value)
        {
            //ILogger.Instance.Send("SetVariable name " + name + " value " + value);
            variablesString[name] = value;
        }

        public string GetVariableString(string name)
        {
            string value;
            variablesString.TryGetValue(name, out value);
            return value;
        }

        internal void Run(List<IAction> list, InstancePerson person)
        {
			int i = 0;
			CoroutineRunner.Instance.RunConditionalCoroutine(() =>
			{
				while (i < list.Count)
				{
					if (list[i].Execute(person != null ? person.syn : null))
						i++;
					else
						return false;
				}

				return true;
			});
        }

        internal void Run(List<IAction> list, ContextIAction context)
        {
			int i = 0;
			CoroutineRunner.Instance.RunConditionalCoroutine(() =>
			{
				while (i < list.Count)
				{
					if (context.map.bStop)
						return true;

					if (list[i].Execute(context))
						i++;
					else
						return false;
				}

				return true;
			});
        }
    }
}
