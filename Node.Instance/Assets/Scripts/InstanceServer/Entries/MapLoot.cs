﻿using System;
using System.Linq;
using System.Numerics;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Common;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Entries
{
	public class MapLoot
	{
		public int LootId;
		public IConcreteLoot Data;
		public int PersonId = -1;
        public bool isPersonal;
        public MapTargetObject Target;
		public DateTime CreateTime;
		public DateTime CanCollectTime;
        public DateTime RemoveTime;
        public Vector3 pos;
		public Vector3 dest;
	    public Vector3i lootPos;

		public MapLoot(ILoot loot, MapTargetObject target, PersonBehaviour person, Vector3 pos, Vector3i lootPos, int removeTime = -1)
		{
			var data = loot.GetILoot(target, person.owner).FirstOrDefault();
			Init(data, target, person, pos, lootPos, removeTime);
		}

		public MapLoot(IConcreteLoot data, MapTargetObject target, PersonBehaviour person, Vector3 pos, Vector3i lootPos, int removeTime = -1)
		{
			Init(data, target, person, pos, lootPos, removeTime);
		}

		private void Init(IConcreteLoot data, MapTargetObject target, PersonBehaviour owner, Vector3 pos, Vector3i lootPos, int removeTime = -1)
        {
            LootId = MapTargetObject.objId++;
            Data = data;
            if(owner != null)
                PersonId = owner.owner.personId;
            isPersonal = owner != null;
            Target = target;
            CreateTime = TimeProvider.UTCNow;
	        CanCollectTime = CreateTime.AddSeconds(ServerConstants.Instance.LootUnavailAfterCreateSec);
            if(removeTime < 0)
                RemoveTime = CreateTime.AddSeconds(data.GetLifeTimeSec());
            else
                RemoveTime = CreateTime.AddSeconds(removeTime);

            this.pos = pos;
            this.lootPos = lootPos;
            var voxPosX = lootPos.x * Helper.VoxelSize;
            var voxPosZ = lootPos.z * Helper.VoxelSize;
            //ILogger.Instance.Send(string.Format("MapLoot: voxPosX - {0}, voxPosZ - {1}, objPos - {2}", voxPosX, voxPosZ, pos), ErrorLevel.debug);

            dest = new Vector3(voxPosX, 0, voxPosZ);

            ServerRpcListener.Instance.CreateLootResponse(this, owner);
        }
	}
}
