﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.InstanceServer.Entries.Login.DataRPC
{
	public class LobbyPersonData
	{
		public int Id;
		public int AnalyticId;
		public PersonType Type;
		public string PrefabName;
		public string NickName;
		public int Level;
		public int Slot;
		public FractionType Fraction;
		public List<EntityPartInfo> Parts;
		public List<InventoryItemData> items;
        public List<AbilityType> availableAbilities;
        public Dictionary<int, AbilityType> equippedAbilities;
        public List<AbilityStatus> AbilityStatuses;
		public List<byte> PotionLevelUnlock;
		public Dictionary<ushort, double> ClientStats;
		public Dictionary<BuffString, double> ClientStringsStats;
        public int activeTalentBuild;
        public List<List<TalentItem>> talentsData;
        public bool isNeedHideHelmet;

        public LobbyPersonData()
		{
		}

		public LobbyPersonData(InstancePerson person)
		{
			Id = person.personId;
			AnalyticId = person.AnalyticId;
			Type = person.personType;
			Fraction = person.personFraction;
			PrefabName = person.PrefabName;
			NickName = person.personName ?? "";
			Level = person.Level;
			Slot = person.SlotId;
			Parts = person.GetPersonParts();
			items = person.GetEquipedItemsAndPotions();
			items.AddRange(person.GetItems().Where(i => i.IsItem));
			items.AddRange(person.GetTradeItems());
			foreach (var item in items)
				item.TempTradeInfoData = item.GetTradeInfoData(person);
            availableAbilities = person.GetActiveSkillsId();
            equippedAbilities = person.GetEquippedSkillsId();
			AbilityStatuses = person.AbilityStatuses;
			PotionLevelUnlock = person.gameXmlData.ServerConstantsXml.PotionLevelUnlock.Select(x => x.Level).ToList();
			ClientStats = person.StatContainer.GetClientStats();
			ClientStringsStats = person.StatContainer.GetClientStringsStats();
            activeTalentBuild = person.passiveAbilityStorage.ActiveTalentBuild;
            talentsData = person.passiveAbilityStorage.GetTalentsForClient();
            isNeedHideHelmet = person.isNeedHideHelmet;
        }

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			LobbyPersonData si = (LobbyPersonData)value;
			stream.Write(si.Id);
			stream.Write(si.AnalyticId);
			stream.Write((int)si.Type);
			stream.WriteStringFromId(si.PrefabName);
			stream.Write(si.NickName);
			stream.Write(si.Level);
			stream.Write(si.Slot);
			stream.Write((int)si.Fraction);
			stream.Write<List<EntityPartInfo>>(si.Parts);
			stream.Write<List<InventoryItemData>>(si.items);
            stream.Write<List<AbilityType>>(si.availableAbilities);
            stream.Write<Dictionary<int, AbilityType>>(si.equippedAbilities);
            stream.Write(si.AbilityStatuses);
            stream.Write(si.PotionLevelUnlock);
            stream.Write<Dictionary<ushort, double>>(si.ClientStats);
            stream.Write<Dictionary<BuffString, double>>(si.ClientStringsStats);
            stream.Write(si.activeTalentBuild);
            stream.Write<List<List<TalentItem>>>(si.talentsData);
            stream.Write(si.isNeedHideHelmet);
        }

		public override string ToString()
		{
			return Id.ToString();
		}
	}
}
