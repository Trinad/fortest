﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Node.Instance.Assets.Scripts.InstanceServer.Entries.XmlData;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.Login.DataRPC
{
	public class LobbyInfo
	{
		public enum PlayerLoginType
		{
			Tutorial, //Запуск туториала
			Normal, //Обычное лобби, список персонажей
            ReplaceDeviceID, //была замена DeviceId на FbIf
		}

		public PlayerLoginType LoginType;
		public bool RepeatTutorial;
		public int PersonId;
		public bool Sale;
		public List<LobbyPersonData> Persons;
		public List<LobbyBuildingAccountData> LobbyBuildingStatus;
		public List<LobbyBuildingAccountData> LobbyBuildingSetStatus;
		public int Slots;
		public List<InventoryItemData> Skins;
		public string AvatarName;
		public ProgressLineState ProgressLineState;
		public int MaxLevel;
        public bool rewrited;

        public LobbyInfo(LobbyLogin login, bool sale, PlayerCityData playerCityData, ItemDataRepository itemDataRepository, ExperienceData ExpData)
		{
            rewrited = login.rewrited;
            var persont = login.Person;
			if (persont != null && persont.bRunTutorial && persont.TutorialStateAdd == 0) //Если идет основной тутор
				LoginType = PlayerLoginType.Tutorial;
			else
				LoginType = PlayerLoginType.Normal;

			RepeatTutorial = !login.tutorRun;

			//TODO: fil ввиду того что загружались слоты то на клиенте появляется Александер Биг
			if (login.Person != null)
			{
				if (login.Person.bRunTutorial && (login.Person.TutorialState == 8 || login.Person.TutorialState == 9)) //TODO_Deg
					PersonId = -1;
				else if (login.Person.personType == PersonType.None || login.Person.personId <= 0)
					PersonId = -1;
				else
					PersonId = login.lastPersonId;
			}
			else
			{
				var person = login.InstancePersons.FirstOrDefault();
				PersonId = person?.personId ?? login.lastPersonId;
				ILogger.Instance.Send("LobbyInfo: login.Person == null PersonId: " + PersonId, ErrorLevel.warning);
			}

			Sale = sale;

			Persons = new List<LobbyPersonData>();
			foreach (var person in login.InstancePersons)
			{
				if (person.personType == PersonType.None || person.personId <= 0)
					continue;

				if (person.bRunTutorial && (person.TutorialState == 8 || person.TutorialState == 9)) //TODO_Deg
					continue;

				Persons.Add(new LobbyPersonData(person));
			}

			Slots = RemoteXmlConfig.Instance.MaxSlotsInLogin;

			LobbyBuildingStatus = new List<LobbyBuildingAccountData>();
			foreach (var building in playerCityData.Buildings)
				LobbyBuildingStatus.Add(new LobbyBuildingAccountData(login, building));

			LobbyBuildingSetStatus = new List<LobbyBuildingAccountData>();
			//TODO_Deg PIX-6455

			Skins = itemDataRepository.GetPrototypesByType(ItemType.Skin);
			foreach (var skin in Skins)
				skin.TempTradeInfoData = skin.GetTradeInfoData(persont);

			AvatarName = login.AvatarName;

			ProgressLineState = new ProgressLineState(); //Заполняется в ProgressLineScenario
			MaxLevel = ExpData.MaxLevel;
		}

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            LobbyInfo si = (LobbyInfo)value;
            stream.Write(si.LoginType);
            stream.Write(si.RepeatTutorial);
            stream.Write(si.PersonId);
            stream.Write(si.Sale);
            stream.Write(RemoteXmlConfig.Instance.PaymentsEnabled);
            stream.Write(si.Persons);
            stream.Write(si.LobbyBuildingStatus);
            stream.Write(si.LobbyBuildingSetStatus);
            stream.Write(si.Slots);
            stream.Write(si.Skins);
            stream.WriteStringFromId(si.AvatarName);
            stream.Write(si.ProgressLineState);
            stream.Write(si.MaxLevel);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			throw new NotImplementedException();
		}
	}
}
