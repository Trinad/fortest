﻿using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.Login.DataRPC
{
	public class AbilityStatus
	{
		public AbilityType Type;
		public bool IsNew;
		public bool IsNotViewed;

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			AbilityStatus si = (AbilityStatus)value;
			stream.Write(si.Type);
			stream.Write(si.IsNew);
			stream.Write(si.IsNotViewed);
		}
	}
}
