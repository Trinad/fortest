﻿using System;
using System.Collections.Generic;
using System.Text;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Zenject;

namespace Assets.Scripts.InstanceServer.Entries
{
    public class MapFactory : IMapFactory
    {
        private MonsterBehaviourFactory monsterBehaviourFactory;
        private INotifyController notifyController;
        public MapFactory(MonsterBehaviourFactory monsterBehaviourFactory, INotifyController notifyController)
        {
            this.monsterBehaviourFactory = monsterBehaviourFactory;
            this.notifyController = notifyController;
        }
        public Map Create(int gameId)
        {
            return new Map(monsterBehaviourFactory, notifyController) {GameId = gameId};
        }
    }

    public interface IMapFactory : IFactory<int, Map> { }

}
