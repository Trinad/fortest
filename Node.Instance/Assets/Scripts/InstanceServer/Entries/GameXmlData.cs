﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.PassiveAbilitys;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.Perks;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Assets.Scripts.Utils;
using Node.Instance.Assets.Scripts.InstanceServer.Entries.XmlData;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Entries
{
    public sealed class GameXmlData
	{
		public PersonDataList PersonAttacksDatas;
        public MonsterDataList MonsterXmlDatas;
        public List<PortalXmlData> PortalDatas;
		public Edges Edges;
		public List<EdgeMapXmlData> EdgesMapXml;
		public List<DungeonXmlData> Dungeons;
		public LootLists LootListsXml;
		public List<NickNameGenerator> NickNameGenerators;
		public PassiveAbilityXmlDataList PassiveAbilitys;
        public PerkDataList PerkList;
        public InfluenceXmlDataList InfluencesXmlData;
		public List<Elixir> Elixirs;
        public List<Buff> Buffs;
		public List<AddinLocation> AddinLocations;
        public MonstersBehaviorData MonstersBehaviorData;
	    
        public ServerConstants ServerConstantsXml;
        public Formulas Formulas;
        public ClientConstants ClientConstants;
	    public QuestCollection QuestCollection;
        public EmotionSettings EmotionSettings;
        public TutorialAlgorithmData TutorialAlgorithmData;
        public ClanSettingsData ClanSettings;
		public FriendSettingsData FriendSettings;
        public ExperienceData ExpData;
        public ItemStatsCollection ItemStatsCollection;
        private InstancePerformanceInfo InstancePerformanceInfo;
        private SpecialOffer SpecialOffers;
        public ItemDataRepository ItemDataRepository;
		public PlayerCityData PlayerCityData;
		public CreditRatingData CreditRatingData;
        public ChannelsXmlData ChanelsXmlData;


        public static GameXmlData LoadAllXmlsInParallel(PathBuilder pathBuilder)
        {
			var tasks = new LoadXmlTasks(50);
			var localGameXmlData = new GameXmlData {ItemStatsCollection = new ItemStatsCollection()};

            tasks.Add<PersonDataList>(pathBuilder.PersonXml, data => localGameXmlData.PersonAttacksDatas = data);
			tasks.Add<MonsterDataList>(pathBuilder.MonsterXml, data => localGameXmlData.MonsterXmlDatas = data);
			tasks.Add<List<NickNameGenerator>>(pathBuilder.NickNameGeneratorXml, data => localGameXmlData.NickNameGenerators = data);
			tasks.Add<List<PortalXmlData>>(pathBuilder.PortalXml, data => localGameXmlData.PortalDatas = data);
			tasks.Add<Edges>(pathBuilder.EdgesXml, data => localGameXmlData.Edges = data);
			tasks.Add<List<EdgeMapXmlData>>(pathBuilder.EdgesMapXml, data => localGameXmlData.EdgesMapXml = data);
			tasks.Add<List<DungeonXmlData>>(pathBuilder.DungeonsXml, data => localGameXmlData.Dungeons = data);
			tasks.Add<LootLists>(pathBuilder.LootListsXml, pathBuilder.TraderDataXml, (data, trade) =>
			{
				data.loots.AddRange(trade.loots);
				data.Init();
				localGameXmlData.LootListsXml = data;
			});
			tasks.Add<PassiveAbilityXmlDataList>(pathBuilder.PassiveAbilitysXml, data => localGameXmlData.PassiveAbilitys = data);
            tasks.Add<PerkDataList>(pathBuilder.PerksXml, data => localGameXmlData.PerkList = data);
            tasks.Add<InfluenceXmlDataList>(pathBuilder.InfluenceXml, data =>
			{
				data.Init();
				localGameXmlData.InfluencesXmlData = data;
			});
			tasks.Add<List<Elixir>>(pathBuilder.ElixireXml, data => localGameXmlData.Elixirs = data);
			tasks.Add<List<Buff>>(pathBuilder.BuffXml, data => localGameXmlData.Buffs = data);
			tasks.Add<List<AddinLocation>>(pathBuilder.AddinLocationXml, data => localGameXmlData.AddinLocations = data);
			tasks.Add<MonstersBehaviorData>(pathBuilder.MonstersBehaviorDataXml, data => localGameXmlData.MonstersBehaviorData = data);
			tasks.Add<ServerConstants>(pathBuilder.ServerConstantsXml, data => localGameXmlData.ServerConstantsXml = data);
			tasks.Add<CreditRatingData>(pathBuilder.CreditRatingXml, data => localGameXmlData.CreditRatingData = data);
            tasks.Add<ChannelsXmlData>(pathBuilder.Chat, data => localGameXmlData.ChanelsXmlData = data);
            tasks.Add<Formulas>(pathBuilder.FormulasXml, data =>
			{
				data.StatsFormulas.InitStatInterpolations();
				localGameXmlData.Formulas = data;
			});
			tasks.Add<ClientConstants>(pathBuilder.ClientConstantsXml, data => localGameXmlData.ClientConstants = data);
			tasks.Add<QuestCollection>(pathBuilder.QuestDataXml, pathBuilder.ProgressLineXml, (questData, progressLine) =>
			{
				questData.QuestDatas.AddRange(progressLine.QuestDatas);
				localGameXmlData.QuestCollection = questData;
			});
			tasks.Add<EmotionSettings>(pathBuilder.EmotionXml, data => localGameXmlData.EmotionSettings = data);
			tasks.Add<TutorialAlgorithmData>(pathBuilder.TutorialDataXml, data => localGameXmlData.TutorialAlgorithmData = data);
			tasks.Add<SocialSettingsData>(pathBuilder.SocialXml, data =>
			{
				localGameXmlData.ClanSettings = data.ClanSettingsData;
				localGameXmlData.FriendSettings = data.FriendSettingsData;
			});
			tasks.Add<ExperienceData>(pathBuilder.ExperienceDataXml, data => localGameXmlData.ExpData = data);
			//foreach (string file in pathBuilder.GetStatsCollectionXmls())
			//	tasks.Add<List<ItemStats>>(file, data => localGameXmlData.ItemStatsCollection.AddItemStats(data));
			tasks.Add<InstancePerformanceInfo>(pathBuilder.InstancePerformanceInfo, data => localGameXmlData.InstancePerformanceInfo = data);
			tasks.Add<SpecialOffer>(pathBuilder.SpecialOffersXml, data => localGameXmlData.SpecialOffers = data);
            tasks.Add<ItemDataRepository>(pathBuilder.ItemDataRepositoryDataXml, data => localGameXmlData.ItemDataRepository = data);
			tasks.Add<PlayerCityData>(pathBuilder.PlayerCityXml, data =>
			{
				data.Init();
				localGameXmlData.PlayerCityData = data;
			});

            Task.WhenAll(tasks.GetSortedTasks()).Wait();
            return localGameXmlData;
        }

        public static GameXmlData LoadAllXmlsInParallel(PathBuilder pathBuilder, out List<object> objects)
        {
            GameXmlData gameXmlData = LoadAllXmlsInParallel(pathBuilder);

            objects = new List<object>();
            objects.Add(gameXmlData.ServerConstantsXml );
            objects.Add(gameXmlData.Formulas );
            objects.Add(gameXmlData.ClientConstants);
            objects.Add(gameXmlData.EmotionSettings );
            objects.Add(gameXmlData.TutorialAlgorithmData );
            objects.Add(gameXmlData.LootListsXml );
            objects.Add(gameXmlData.ClanSettings);
            objects.Add(gameXmlData.FriendSettings);
            objects.Add(gameXmlData.InstancePerformanceInfo );
            objects.Add(gameXmlData.SpecialOffers);
            objects.Add(gameXmlData.CreditRatingData);
            objects.Add(gameXmlData.ItemDataRepository);
            objects.Add(gameXmlData.PlayerCityData);
            objects.Add(gameXmlData.ChanelsXmlData);

            return gameXmlData;
        }

		class LoadXmlTasks
		{
			class XmlTask
			{
				public long FileSize;
				public Action AfterLoad;
			}

			private readonly List<XmlTask> files;

			public LoadXmlTasks(int capacity)
			{
				files = new List<XmlTask>(capacity);
			}

			public void Add<T>(string fileName, Action<T> afterLoad)
			{
				UtilsLib.Logic.XmlUtils.PreLoadXml<T>();
				files.Add(new XmlTask
				{
					FileSize = new System.IO.FileInfo(fileName).Length,
					AfterLoad = () => afterLoad(fileName.LoadFromFileXml<T>())
				});
			}

			public void Add<T>(string fileName1, string fileName2, Action<T, T> afterLoad)
			{
				UtilsLib.Logic.XmlUtils.PreLoadXml<T>();
				files.Add(new XmlTask
				{
					FileSize = new System.IO.FileInfo(fileName1).Length+ new System.IO.FileInfo(fileName2).Length,
					AfterLoad = () => afterLoad(fileName1.LoadFromFileXml<T>(), fileName2.LoadFromFileXml<T>())
				});
			}

			public IEnumerable<Task> GetSortedTasks()
			{
				files.Sort((a, b) => Math.Sign(b.FileSize - a.FileSize));
				return files.Select(x => Task.Run(() => x.AfterLoad()));
			}
		}
    }
}
