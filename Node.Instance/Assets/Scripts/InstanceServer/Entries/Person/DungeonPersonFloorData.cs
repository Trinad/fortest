﻿using Assets.Scripts.Utils;
using System;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.Person
{
	public class DungeonPersonFloorData
	{
		public int EdgeId { get; private set; }
		public int LocationId { get; private set; }
		public ControlState ControlState { get; private set; }
		public DateTime Time { get; private set; }
		public ResourceType CurrencyType { get; private set; }
		public int Price { get; private set; }

		public DungeonPersonFloorData(int edgeId, int locationId)
		{
			EdgeId = edgeId;
			LocationId = locationId;
			Time = DateTime.MaxValue;
			CurrencyType = ResourceType.Emerald;
			Price = int.MaxValue;
		}

		public DungeonPersonFloorData()
		{
		}

		public void SetData(ControlState controlState, ResourceType currencyType, int price, DateTime time)
		{
			this.ControlState = controlState;
			CurrencyType = currencyType;
			Price = price;
			Time = time;
		}

		public void SetTime(DateTime time)
		{
			Time = time;
		}

		public void CheckTimeFloor()
		{
			switch (ControlState)
			{
				case ControlState.Lock:
					break;
				case ControlState.Close:
					break;
				case ControlState.Open:
					EndTimeToClose();
					break;
				case ControlState.CooldownTimer:
					EndTimeToClose();
					break;
			}
		}

		public void EndTimeToClose()
		{
			TimeSpan timeSpan = Time - DateTime.UtcNow;
			if (timeSpan.TotalSeconds <= 0)
				SetControlState(ControlState.Close);
		}

		public void SetControlState(ControlState controlState)
		{
			switch (controlState)
			{
				case ControlState.Lock:
					SetData(ControlState.Lock, ResourceType.Emerald, int.MaxValue, DateTime.MaxValue);
					break;
				case ControlState.Close:
					SetData(ControlState.Close, ResourceType.KeyStone, ServerConstants.Instance.ToOpenFloorPrice, DateTime.MaxValue);
					break;
				case ControlState.Open:
					SetData(ControlState.Open, ResourceType.KeyStone, ServerConstants.Instance.ToEnterFloorPrice,
							DateTime.UtcNow.AddMinutes(ServerConstants.Instance.TimeOpenFloor));
					break;
				case ControlState.CooldownTimer:
					SetData(ControlState.CooldownTimer, ResourceType.Emerald, ServerConstants.Instance.ToResetCoolDown,
							DateTime.UtcNow.AddMinutes(ServerConstants.Instance.TimeCooldownFloor));
					break;
			}
		}
	}

	public enum ControlState
	{
		Lock,
		Close,
		Open,
		CooldownTimer,
	}
}