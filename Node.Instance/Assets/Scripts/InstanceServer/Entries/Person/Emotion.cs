﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.Networking.ClientDataRPC;

namespace Assets.Scripts.InstanceServer.Entries.Person
{
    public class Emotion
    {
        public int Id { get; private set; }
        public EmotionGroup Group { get; private set; }
        public int SlotId { get; private set; }
        public int Price { get; private set; }
        public DateTime RestoreTime { get; private set; }
        public bool IsAvailable { get; private set; }

        public Emotion()
        {
            
        }

        public Emotion(int id, EmotionGroup group, int slotId = -1)
        {
            Id = id;
            Group = group;
            SlotId = slotId;
            Price = -1;
            RestoreTime = DateTime.UtcNow;
        }

        public Emotion(int id, EmotionGroup group, ProductItemData productItemData)
        {
            Id = id;
            Group = group;
            SlotId = -1;
            Price = (int)productItemData.Price;
            RestoreTime = DateTime.UtcNow;
        }

        public void UpdateData(int slot, bool isAvalable = true)
        {
            SlotId = slot;
            this.IsAvailable = isAvalable;
        }

        public void SetTime(DateTime time)
        {
            RestoreTime = time;
        }

        public static explicit operator Emotion(EmotionXmlData emotionXml)
        {
            var newEmotionData = new Emotion(emotionXml.Id, emotionXml.Group);
            return newEmotionData;
        }
    }
}
