﻿using Assets.Scripts.InstanceServer.Entries.Person.EventArgs;

namespace Assets.Scripts.InstanceServer
{
	public class PersonDisconnectArgs : PersonBaseArgs
	{
		public PersonDisconnectArgs(InstancePerson person) : base(person)
		{
		}
	}
}