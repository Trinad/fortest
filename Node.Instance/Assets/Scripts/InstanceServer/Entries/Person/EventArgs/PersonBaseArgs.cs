﻿namespace Assets.Scripts.InstanceServer.Entries.Person.EventArgs
{
	public abstract class PersonBaseArgs : System.EventArgs
	{
		public readonly InstancePerson Person;

		protected PersonBaseArgs(InstancePerson person)
		{
			Person = person;
		}
	}
}
