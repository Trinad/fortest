﻿using Assets.Scripts.InstanceServer.Entries.Person.EventArgs;

namespace Assets.Scripts.InstanceServer
{
	public class PersonEnterGameArgs : PersonBaseArgs
	{
		public PersonEnterGameArgs(InstancePerson person) : base(person)
		{
		}
	}
}