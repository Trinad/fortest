﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Db;
using Assets.Scripts.Utils.Logger;
using UtilsLib.DegSerializers;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using Zenject;

namespace Assets.Scripts.InstanceServer.Entries.Person
{
    public class InstancePersonFactory : IInstancePersonFactory, IValidatable
    {
        private GameXmlData gameXmlData;
        private InstanceSqlAdapter sqlAdapter;

        public ShopManager shopManager;
        public IEmotionController emotionController;
        public NPCTradeAlgorithm npcTradeAlgorithm;
        public GlobalEvents events;

        private INotifyController notifyController;

        public InstancePersonFactory(GlobalEvents events, GameXmlData gameXmlData, InstanceSqlAdapter sqlAdapter,
            ShopManager shopManager, IEmotionController emotionController, NPCTradeAlgorithm npcTradeAlgorithm, INotifyController notifyController)
        {
            this.events = events;
            this.gameXmlData = gameXmlData;
            this.sqlAdapter = sqlAdapter;

            this.shopManager= shopManager;
            this.emotionController= emotionController;
            this.npcTradeAlgorithm= npcTradeAlgorithm;

            this.notifyController = notifyController;
        }

        public InstancePerson Create(CreateInstancePersonData createInstancePersonData)
        {
            var person = new InstancePerson(createInstancePersonData.login.loginId, sqlAdapter);
			person.login = createInstancePersonData.login;
            person.GameId = createInstancePersonData.gameId;
            person.SlotId = createInstancePersonData.instancePersonSqlData.SlotId;
            person.AnalyticId = createInstancePersonData.instancePersonSqlData.AnalyticId;
            person.personId = createInstancePersonData.instancePersonSqlData.PersonId;
            person.personType = createInstancePersonData.instancePersonSqlData.Type;
            person.personFraction = createInstancePersonData.instancePersonSqlData.Fraction;
            person.SetGameXmlData(gameXmlData);
            person.GuildId = createInstancePersonData.GuildId;
			person.GuildRole = createInstancePersonData.GuildRole;
            person.rating_elo_coop = createInstancePersonData.instancePersonSqlData.rating_elo_coop;
            //person.rating_elo_solo = createInstancePersonData.instancePersonSqlData.rating_elo_solo;
            //person.rating_elo_team = createInstancePersonData.instancePersonSqlData.rating_elo_team;
            person.Wallet = new Wallet(person, events, shopManager);
            person.LogoutTime = createInstancePersonData.instancePersonSqlData.LogoutTime;
            person.bRunTutorial = createInstancePersonData.instancePersonSqlData.bRunTutorial;
            person.TutorialState = createInstancePersonData.instancePersonSqlData.TutorialState;
            //personSqlData.TutorialStateAdd = InstancePersonSqlData.TutorialStateAdd;
            person.EnableEdgeLocationId = createInstancePersonData.instancePersonSqlData.EnableEdgeLocationId;

            person.PersonalParam = person.loginId + "_" + person.personId;

            person.ScenarioStrings.Clear();
            Sos.Debug("загрузка сценарных заметок");
            person.ScenarioStrings = new ScenarioStringCollection(createInstancePersonData.instancePersonSqlData.ScenarioStrings);

            //person.ShopSender = createInstancePersonData.shopSender;

            person.StatContainer = new PersonStatContainer(person);
            person.StatContainer.AddStat(Stats.EloRatingTeam, createInstancePersonData.instancePersonSqlData.rating_elo_team);
            //personSqlData.StatContainer.SetInfluenceDatas(data.InfluenceDatas);
            person.InitFriends();
            person.InitItems(createInstancePersonData.instancePersonSqlData, npcTradeAlgorithm.GetInventaryTradeSaveItems());
            person.InitAbility(createInstancePersonData.instancePersonSqlData);
            person.InitPerks(createInstancePersonData.instancePersonSqlData);// TODO_maximpr доделать чтение из базы
            person.isNeedHideHelmet = createInstancePersonData.instancePersonSqlData.isNeedHideHelmet;
			person.Notifications = createInstancePersonData.instancePersonSqlData.Notifications;
            //mailManager.LoadMails(person);

            //person.CurrentChat = ChatSection.Location;
            //person.ChatNotification = new bool[(int)ChatSection.Count];

            person.DungeonPersonLocationDatas = createInstancePersonData.instancePersonSqlData.DungeonData ?? new List<DungeonPersonFloorData>();

            person.InfluenceDatas = createInstancePersonData.instancePersonSqlData.InfluenceDatas;
            emotionController.CreateEmotionPerson(person, createInstancePersonData.instancePersonSqlData.AvailableEmotions, 
                createInstancePersonData.instancePersonSqlData.AvailableEmotionsForBuy);//TODO_maximpr хочу видеть эту строчку в виде присвоения
            //person.AvengerCooldown = createInstancePersonData.instancePersonSqlData.AvengerCooldown;

            person.StatContainer.AddStat(Stats.Exp, createInstancePersonData.instancePersonSqlData.Experience);
            person.SetLevel(createInstancePersonData.instancePersonSqlData.Level);

            person.Collection = new Collection(person, gameXmlData, notifyController);
			person.SessionId = createInstancePersonData.instancePersonSqlData.SessionId;

			ILogger.Instance.Send($"InitInstancePerson(): login: {person.login.loginId} person {person.personId}");
            
            return person;
        }

        public void Validate()
        {

        }
    }

    public interface IInstancePersonFactory : IFactory<CreateInstancePersonData, InstancePerson>
    {
    }
}
