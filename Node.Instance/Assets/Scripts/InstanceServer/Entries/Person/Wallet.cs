﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Assets.Scripts.InstanceServer.Analytic;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.DegSerializers;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.InstanceServer.Entries.Person
{
	//Для хранения всяких ресурсов
	public class Wallet
	{
        private readonly InstancePerson owner;
		private readonly Dictionary<ResourceType, int> login; //У логина
		private readonly ShopManager shopManager;
        private readonly GlobalEvents events;

        public Wallet(InstancePerson owner, GlobalEvents events, ShopManager shopManager)
		{
			this.owner = owner;
			this.events = events;
            this.shopManager = shopManager;

			login = owner.login.Wallet;
		}

		//Что может лежать в кошельке
		public bool Exist(ResourceType resource)
		{
			switch (resource)
			{
				case ResourceType.Gold:
				case ResourceType.Emerald:
                case ResourceType.Token:
					return true;
			}

			return false;
		}

		public int Count(ResourceType resource)
		{
			return login[resource];
		}

		public bool HaveResource(Dictionary<ResourceType, int> res)
		{
			for (ResourceType i = 0; i < ResourceType.Last; i++)
			{
				if (!Exist(i))
					continue;

				if (res.ContainsKey(i) && Count(i) < res[i])
					return false;

				res.Remove(i);
			}

			return true;
		}

		public void HaveResource2(Dictionary<ResourceType, int> res)
		{
			for (ResourceType i = 0; i < ResourceType.Last; i++)
			{
				if (!Exist(i))
					continue;

				if (res.ContainsKey(i))
					res[i] = Math.Max(0, res[i] - Count(i));
			}
		}

		public void Set(ResourceType resource, int count, AnalyticCurrencyInfo analyticInfo)
		{
            login[resource] = count;
            if(analyticInfo!= null && analyticInfo.needSendInWallet)
                AnalyticsManager.Instance.SendData(owner, analyticInfo);
		}

        public bool TryAdd(ResourceType resource, int count, ItemSourceType Reason = ItemSourceType.None, AnalyticCurrencyInfo analyticInfo = null)
        {
            if (count == 0)
                return true;

            if (Count(resource) + count < 0)
                return false;

            if (analyticInfo == null)
            {
                analyticInfo = new AnalyticCurrencyInfo
                {
                    needSendInWallet = true,
                    sendGain = count > 0,
                    Amount = Math.Abs(count),
                    Destination = SinkType.Unknown.ToString(),
                    DestinationType = SinkType.Unknown.ToString(),
                    Source = SourceGain.Unknown,
                    SourceType = SourceGain.Unknown.ToString(),
                    PersonLevel = owner.login.GetMaxPersonLevel(),
                    Currency = resource,
					TestPurchase = owner.login.settings.testPurchase
				};

			var stack = new StackTrace();
                ILogger.Instance.Send($"Analitic Wallet Add Unknown: StackTrace: {stack}", ErrorLevel.warning);
            }

            if (resource == ResourceType.Emerald)
			{
                ILogger.Instance.Send($"Analitic Wallet add Emeralds ={count} info= { JSON.JsonEncode(analyticInfo.JsonEncode())}");
                var info = analyticInfo.Copy();
                shopManager.EmeraldCheat(owner, count, (AccountChangeParams y) => {
                    var person = owner;
                    if (info.needSendInWallet)                    
                        AnalyticsManager.Instance.SendData(person, info);
                    ILogger.Instance.Send($"Analitic Wallet Deligate Emeralds ={count} info= { JSON.JsonEncode(info.JsonEncode())}");
                });
				return true; //Изумруд обновим когда c платежки ответ придет
			}

			login[resource] = Math.Max(0, login[resource] + count);

			owner.bNeedSendCurrency = true;

			owner.OnWalletResourceChange(resource, count, Reason);
            events.OnWalletResourceChange(owner, resource, count, Reason);

            switch (resource)
            {
                case ResourceType.Gold:
                    owner.OnGiveGold(count);
                    break;
                case ResourceType.None:
                case ResourceType.Last:
                    throw new ArgumentOutOfRangeException(nameof(resource), "TryAdd(): not valid ResourceType");
            }

            if(analyticInfo.needSendInWallet)
                AnalyticsManager.Instance.SendData(owner, analyticInfo);

            return true;
        }
	}
}
