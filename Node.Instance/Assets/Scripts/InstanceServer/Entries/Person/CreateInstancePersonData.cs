﻿using Assets.Scripts.InstanceServer.Entries.Login;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.InstanceServer.Entries.Person
{
	public class CreateInstancePersonData
	{
		public int gameId;
		public LobbyLogin login;
		public InstancePersonSqlData instancePersonSqlData;
		public int GuildId;
		public ClanRole GuildRole;
	}
}
