﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils.Common;
using UtilsLib.DegSerializers;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using Assets.Scripts.Utils.Logger;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData;
using Assets.Scripts.InstanceServer.Groups;
using Assets.Scripts.Utils;
using Assets.Scripts.InstanceServer.Entries.ActiveObjects;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.Login.DataRPC;
using Assets.Scripts.Utils.Db;
using LobbyInstanceLib.Networking;
using UtilsLib.ExternalLogs.Classes;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using UtilsLib.Logic;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.Perks;
using Assets.Scripts.InstanceServer.Analytic;

namespace Assets.Scripts.InstanceServer
{
	public class InstancePerson
	{
		public int GameId;
		public PersonBehaviour syn;

	    public PersonData Data { get; private set; }
	    public GameXmlData gameXmlData { get; private set; }
	    public void SetGameXmlData(GameXmlData gameXmlData)
	    {
	        this.gameXmlData = gameXmlData;
	        Data = gameXmlData.PersonAttacksDatas.PersonDatas.Find(x => x.PersonType == personType);
	    }

		public LobbyLogin login;

		public int SlotId;
		public int AnalyticId;

        public int loginId;
		public bool loginConflict;
		public DateTime LastPayment;

		public int personId;
		public PersonType personType;
		public FractionType personFraction;
		public string personName => login.loginName;
		public Wallet Wallet;
		public Collection Collection;

		public int GuildId { get => login.GuildId; set => login.GuildId = value; }
		public ClanRole GuildRole { get => login.GuildRole; set => login.GuildRole = value; }

        public int upgradeRequestId = 0;

		public int Gold
		{
			get { return CountResource(ResourceType.Gold); }
		}

		public int Emeralds
		{
			get { return CountResource(ResourceType.Emerald); }
		}

		//Персонаж загружается на другой уровень. Не пересчитывать и не сохранять его на старом уровне
        public DateTime dtLoadLevel = DateTime.MinValue;

        public int rating_elo_coop = 0;

        public bool bFromLobby;
        public bool bInSafeZone;

        public int Health { get; set; }
        public int Energy { get; set; }

	    public int Level = 1;
		public DateTime LogoutTime = DateTime.MinValue;
		public DateTime? SaveAndExitTime = null;
		public bool bFullItemsListSent;

        public bool isNeedHideHelmet;

		public List<string> Notifications;
		public int SessionId { get; set; }

		public void NotificationAdd(string str)
		{
			if (Notifications.Contains(str))
				return;

			login.NeedUpdateNotifications = true;
			Notifications.Add(str);
		}

		public float ConnectedAtTime { get; private set; }

		public event Action<ArenaMatchType, bool, bool> ArenaGameFinish;

		public void OnArenaGameFinish(ArenaMatchType matchType, bool bWin, bool bLeave = false)
        {
			ArenaGameFinish?.Invoke(matchType, bWin, bLeave);
			GlobalEvents.Instance.OnArenaGameFinish(this, matchType, bWin, bLeave);

			WinCounter winCounter = login.WinCounterCollection.Get(personType);
            winCounter.OnArenaGameFinish(matchType, bWin);

			if (bRunTutorial)
				Dialogs.Add("ArenaGameFinish");
        }

        #region Для плашки воскрешения после смерти на локации, после реконнекта
		public float ResurrectSec { get; private set; }
		public DateTime ResurrectTime { get; private set; }
		public int ResurrectCost;
		public void SetResurrectTime(float time)
		{
			ResurrectSec = time;
			ResurrectTime = time >= 0 ? TimeProvider.UTCNow.AddSeconds(time) : DateTime.MaxValue;
		}

		PlayerDeathData playerDeathData;
		public PlayerDeathData GetPlayerDeathData()
		{
			playerDeathData.timeInSeconds = GetResurrectTime();
			return playerDeathData;
		}

		public void SetPlayerDeathData(PlayerDeathData playerDeathData)
		{
			this.playerDeathData = playerDeathData;
		}
		public float GetResurrectTime()
		{
			return ResurrectTime == DateTime.MaxValue ? -1f : (float) (ResurrectTime - TimeProvider.UTCNow).TotalSeconds;
		}
        #endregion

        #region Для туториала
        public bool bRunTutorial; //Персонаж проходит тутор вообще
		public int TutorialState; //Текущий стейт тутора
		public int TutorialStateAdd; //Дополнительный стейт тутора
		public bool bRunTutorialOld; //Состояние перед дополнительным тутором
		public bool TutorialStateFinish;
		public bool TutorialIsStart; //Нужно запустить тутор
		public bool TutorialIsRun; //Тутор запущен
		public List<MapActiveObject> TutorialActiveMapObjects = new List<MapActiveObject>();
		public bool TutorialInit;
		public int TutorialAction;
		public DateTime TutorialWaitTime = DateTime.MinValue;
		public bool bTutorialFreeSkillUpgrade;
		public bool bTutorialFreeAltarRepair;
		public bool bTutorialFreeItemUpgrade;
		public bool bTutorialInfiniteEnergy;
		public bool bTutorialAddItemReplace;
		public int bTutorialUnlockLocation;
		public bool bTutorialNotSendQuestNotification;
		public bool bTutorialEnableDailyQuest;
		public bool bTutorialOpenFortGate;
		public bool bTutorialShowDailyQuests;
		public bool bTutorialFreeRessurect;
		public bool bTutorialFixActiveGameObject;
		public string TutorialNextStateInLevelName; //Включить следующий стейт при телепортации на эту карту
		public bool bTutorialPauseIfDead;
		public int? bTutorialResetIfDungeonClose; //Начать стейт тутора заново если данжен закроется
		public int? bTutorialResetIfSpentMoney; //Начать стейт тутора заново если потратил деньги
		public string bTutorialResetIfQuestOptionalNotComplite; //Начать стейт тутора заново если квест не выполнен и необязательные таски тоже не выполненые
		public bool bTutorialResetIfRefreshTradeOffers; //Начать стейт тутора заново если обновился список товаров
		public bool bTutorialCanNotPutOn; //Нельзя надевать туторные вещи
		public bool bTutorialCanNotSell; //Нельзя продавать туторные вещи
		public bool bTutorialUseCommonActiveMapObjects; //Использовать общий список объектов на локации
		public int? bTutorialNotTeleport; //Не телепортировать перса по нажатию кнопки
		public List<string> TutorialHideNPC; //Список npc, которых надо прятать
		public bool bTutorialAvengerEnable; //Мстители

        public void ResetTutorialState(bool clearQuests = true)
		{
			if (clearQuests)
			{
				foreach (var q in ActiveQuest.Where(x => x.Type == QuestType.Tutorial))
					ServerRpcListener.Instance.SendUpdateTaskResponse_TaskCompleted(this, q);

				QuestsClear(x => x.Type == QuestType.Tutorial);
			}

			TutorialAction = 0;
			Dialogs.Clear();

			bTutorialFixActiveGameObject = false;
		}

		public void NextTutorialState()
		{
			if (TutorialStateAdd > 0)
			{
				TutorialStateAdd = 0;
				bRunTutorial = bRunTutorialOld;
			}
			else
				TutorialState++;

			TutorialStateFinish = false;
			TutorialAction = 0;
			Dialogs.Clear();
		}

		#endregion

		public List<AnaliticsEvent> Analitics = new List<AnaliticsEvent>();

	    public bool InGuild()
        {
            return GuildId > 0;
        }

		public void ResetActiveMapObjects()
		{
			TutorialActiveMapObjects = TutorialIsRun ? new List<MapActiveObject>(syn.map.activeMapObjects) : null;
		}

        public List<MapActiveObject> GetActiveMapObjects()
		{
			return TutorialIsRun && !bTutorialUseCommonActiveMapObjects ? TutorialActiveMapObjects : syn.map.activeMapObjects;
		}
		public int EnableEdgeLocationId; //Номер доступной локации

        public List<DungeonPersonFloorData> DungeonPersonLocationDatas { get; set; }

		public uLink.NetworkPlayer player => login.player;
		public RpcClientEnum ClientRpc => login.ClientRpc;
		public RpcClientEnum ClientRpcUnr => login.ClientRpcUnr;
		public RpcClientEnum ClientRpcLight => login.ClientRpcLight;

		public bool bOnLine
		{
			get { return InstanceGameManager.Instance.havePlayer(player); }
		}

        public EmotionPerson EmotionPerson { get; private set; }

		public IInstanceSqlAdapter sqlAdapter { get; private set; }

        public InstancePerson(int loginId, IInstanceSqlAdapter sqlAdapter)
		{
			this.loginId = loginId;

		    this.sqlAdapter = sqlAdapter;

			m_mails = new List<MailItemData>();
			m_changedItems = new List<InventoryItemData>();
        }


	    public void CreatePersonBehaviour(Map map, bool bReconnect)
	    {
            dtLoadLevel = DateTime.MinValue;

            if (syn == null || !map.persons.Contains(syn))
            {
                syn = new PersonBehaviour();

                syn.Init(this, map, bReconnect);
                map.AddPerson(syn);

                if (bRunTutorial && map.IsTutorial)
                {
                    if (syn.IsDead)
                        syn.ResurrectNow();
                    else
                        syn.InitPosition();
                }
                else
                {
                    syn.InitPosition();
                }

                map.OnCreatePersonBehaviour(syn);
            }
            else
            {
                syn.Init(this, map, true);//если есть персонаж, то тогда это реконнект
            }

            InstanceGameManager.Instance.TutorialAlgorithm.TutorialStop(this);

            TutorialIsStart = true;

            //TODO_maximpr сейчас не используется. В тестах GuildSettings = null закоментировал чтобы не чинить тесты
            //ServerRpcListener.Instance.SendRpc(ClientRpc.GuildSettingsResponse, InstanceGameManager.Instance.GuildSettings);
            //Инфа о себе
            //ServerRpcListener.Instance.SendAndLogRpc(RpcClientEnum.CreatePlayerResponse, this.player, new PlayerData(this));
        }

		public void SaveToDB(Action onSuccess = null)
		{
			//Дефолтный перс, не сохранять его
			if (personType == PersonType.None || personId == 0)
			{
				onSuccess?.Invoke();
				return;
			}

			ILogger.Instance.Send($"Save person: loginId={login.loginId} personId={personId}");
			sqlAdapter.SavePerson(new InstancePersonSqlData(this),
				personSQLData => onSuccess?.Invoke(),
				(personSQLData, a, b) => ILogger.Instance.Send("Error on SavePerson instancePerson with Id = " + personSQLData.PersonId + " error = " + a + " ex = " + b, ErrorLevel.exception)
			);
		}

		public void SaveLoginToDB(Action onSuccess = null)
		{
			ILogger.Instance.Send($"Save login: loginId={login.loginId}");
			sqlAdapter.SaveLogin(login,
				l => onSuccess?.Invoke(),
				(l, result, ex) => ILogger.Instance.Send($"Error on SaveLogin with loginId={login.loginId} result={result} ex = {ex}", ErrorLevel.exception)
			);
		}

        public string PrefabName
		{
			get { return personType.ToString().ToLower(); }
        }

		public string PersonalParam { get; set; }

	    public override string ToString()
		{
			StringBuilder sb = ObjectPoolSB.GetObject();
			sb.Append("Id = ").Append(personId);
			sb.Append(" LoginId = ").Append(loginId);
			sb.Append(" personType = ").Append(personType);
			sb.Append(" personName = ").Append(personName);
			string res = sb.ToString();
			ObjectPoolSB.Back(sb);
			return res;
		}


        public List<EntityPartInfo> GetPersonParts()
        {
            var parts = new List<EntityPartInfo>();

            foreach (var item in equipedItems.GetInventoryItems())
                if (item.IsWear || item.IsWeapon)
                    parts.Add(item.GetEntityPartInfo());

            return parts;
        } 

	    public void CreatEmotionPerson(EmotionPerson emotionPerson)
	    {
	        EmotionPerson = emotionPerson;
	    }

        public event EventHandler<PersonEnterGameArgs> OnPersonEnterGame;
		public event EventHandler<PersonDisconnectArgs> OnPersonDisconnected;

		public void Connected()
		{
			ConnectedAtTime = Time.time;

			if (OnPersonEnterGame != null)
				OnPersonEnterGame(this, new PersonEnterGameArgs(this));
		}

		public void Disconnected()
		{
			if (OnPersonDisconnected != null)
				OnPersonDisconnected(this, new PersonDisconnectArgs(this));
		}

        public DateTime LastPing = TimeProvider.UTCNow;

        public bool bNeedSave { get; set; }
		public DateTime NextDateSave { get; set; }

		public bool NeedUpdatePotion;

        public ItemsStorage equipedItems;// = new ItemsStorage(Slots.MainWeapon, Slots.SecondWeapon, Slots.Armor, Slots.Amulet, Slots.Ring, Slots.Artifact,
                                         //Slots.HideSlot, Slots.Boots, Slots.Bracers, Slots.Helmet, Slots.Shoulders, Slots.Belt, Slots.Skin);

        public ItemsStorage potions;// = new ItemsStorage(Slots.Potion1, Slots.Potion2, Slots.Potion3, Slots.Potion4);
        public List<InventoryItemData> items;

	    public void InitItems(InstancePersonSqlData personSQLData, List<ItemSaveData> TradeSaveItems)
        {
            UpgradeLevelsCollection = new ItemUpgradeCollection(gameXmlData.ItemDataRepository, personSQLData.UpgradeItemLevels);

            equipedItems = new ItemsStorage(gameXmlData.ItemDataRepository, Slots.MainWeapon, Slots.SecondWeapon, Slots.Armor, Slots.Amulet, Slots.Ring, Slots.Artifact,
            Slots.HideSlot, Slots.Boots, Slots.Bracers, Slots.Helmet, Slots.Shoulders, Slots.Belt, Slots.Skin);

            potions = new ItemsStorage(gameXmlData.ItemDataRepository, Slots.Potion1, Slots.Potion2, Slots.Potion3, Slots.Potion4);

            items = new List<InventoryItemData>();
            TradeItems = new List<InventoryItemData>();

            foreach (var itemData in personSQLData.Items2)
            {
                InventoryItemData item = null;
                try
                {
                    item = gameXmlData.ItemDataRepository.GetCopyInventoryItemData(itemData.Name, itemData.Count);
					item.Count = Math.Min(item.Count, item.StackCount);

					if (!item.PersonType.Contains(personType))
					{
						ILogger.Instance.Send($"InstancePerson.InitItems: предмет чужого класс, будет удален person={personId} {personType}, item={item.Name}", ErrorLevel.error);
						continue;
					}

					if (item.ItemType == ItemType.Skin && !login.Skins.Contains(item.Name))
						login.Skins.Add(item.Name);

                    UpgradeLevelsCollection.RepareUpgradeLevel(item);
                    item.IsNew = itemData.IsNew;

					var itemStack = items.Find(x => x.CanStackWith(item));
					if (itemStack != null)
						itemStack.TryAddStack(item);
					else
						items.Add(item);
                    StatContainer.RecalcItemConstsForAddOwnership(item);
                }
                catch (Exception ex)
                {
                    ILogger.Instance.Send($"произошла ошибка. Предмет будет снят и потерян. LoginId = {loginId} Ошибка: {ex}", ErrorLevel.error);
                    if (item != null)
                    {
                        items.Remove(item);
                    }
                }
            }

            potions.Add(personSQLData.ItemsPotions2, true);
            potions.LastItems = personSQLData.ItemsPotionsLast2;

            foreach (var itemData in personSQLData.ItemsEquiped2)
            {

                InventoryItemData item = null;
                try
                {
                    item = gameXmlData.ItemDataRepository.GetCopyInventoryItemData(itemData.Name, itemData.Count);
					item.Count = Math.Min(item.Count, item.StackCount);

					if (item.ItemType == ItemType.Skin && !login.Skins.Contains(item.Name))
						login.Skins.Add(item.Name);

                    UpgradeLevelsCollection.RepareUpgradeLevel(item);
                    item.IsNew = itemData.IsNew;

                    var itemStack = items.Find(x => x.CanStackWith(item));
                    if (itemStack != null)
                    {
                        item.TryAddStack(itemStack);
                        items.Remove(itemStack);
                    }


                    equipedItems.AddItem(item, item.SlotId);
                    StatContainer.RecalcItemConstsForAddEquip(item);
                    StatContainer.RecalcItemConstsForAddOwnership(item);
                }
                catch (Exception ex)
                {
                    ILogger.Instance.Send($"произошла ошибка. Предмет будет снят и потерян. LoginId = {loginId} Ошибка: {ex}", ErrorLevel.error);
                    if (item != null)
                    {
                        equipedItems.RemoveItem(item);
                    }
                }
            }

            foreach (var itemData in TradeSaveItems)
            {
                InventoryItemData item = gameXmlData.ItemDataRepository.GetCopyInventoryItemData(itemData.Name, itemData.Count);
                if (!item.PersonType.Contains(personType))
                    continue;

                if (!HaveTradeItem(item))
                {
                    UpgradeLevelsCollection.RepareUpgradeLevel(item);
                    item.IsNew = !personSQLData.TradeItems2.Contains(itemData.Name);
                    item.SlotMode = InventorySlotMode.SHOP;
                    TradeItems.Add(item);
                }
            }
        }

	    public void InitStartPack()
		{
			var context = new ContextIAction(null) {actorOwner = this};
			foreach (var action in ServerConstants.Instance.StartPack.Actions)
				action.Execute(context);
		}

		private readonly  List<InventoryItemData> m_changedItems;

		public event Action<InventoryItemData> InventorySlotFill;

		//Положили что-то в слот сумки
		private void OnInventorySlotFill(InventoryItemData item)
		{
			if (InventorySlotFill != null)
				InventorySlotFill(item);

            AddChangedItems(item);
            //Обновить счетчик на клиенте
            bUpdateInventoryItems = true;

			if (item.ItemType == ItemType.Resource && !Wallet.Exist(item.Resource))
				bUpdateResources = true;

            if (item.ItemType == ItemType.Potion)
				NeedUpdatePotion = true;

			if (item.ItemType == ItemType.Skin)
				login.SkinAdd(item);
		}

		public event Action<InventoryItemData, int> InventorySlotStack;

		//Доложили что-то в слот сумки
		private void OnInventorySlotStack(int slotId, InventoryItemData item, int count = 0)
		{
            AddChangedItems(item);

			if (InventorySlotStack != null)
				InventorySlotStack(item, count);

			if (item.ItemType == ItemType.Resource && !Wallet.Exist(item.Resource))
				bUpdateResources = true;

            if (item.ItemType == ItemType.Potion)
				NeedUpdatePotion = true;
		}

		public event Action<InventoryItemData> InventorySlotEmpty;

		//Забрали из слота сумки
		private void OnInventorySlotEmpty(InventoryItemData item)
		{
			//m_removedItems[InventorySlotMode.BACKPACK].Add(item);

			if (InventorySlotEmpty != null)
				InventorySlotEmpty(item);

			//Обновить счетчик на клиенте
			bUpdateInventoryItems = true;

			if (item.ItemType == ItemType.Resource && !Wallet.Exist(item.Resource))
				bUpdateResources = true;

            if (item.ItemType == ItemType.Potion)
				NeedUpdatePotion = true;
		}

		public void MyUpdate()
		{
			if (bUpdateInventoryItems)
			{
				bUpdateInventoryItems = false;
				//ServerRpcListener.Instance.SendUpdateInventoryItemsResponse(owner);
			}

			if (bNeedSendCurrency)
			{
				bNeedSendCurrency = false;
				ServerRpcListener.Instance.SendCurrencyResponse(this);
			}

			if (bUpdateResources)
			{
				bUpdateResources = false;
				ServerRpcListener.Instance.SendUpdateResourcesResponse(this);
			}

			if (login.NeedUpdateGloryPoint)
			{
				login.NeedUpdateGloryPoint = false;
				ServerRpcListener.Instance.SendGloryPointResponse(this);
			}

			if (login.NeedUpdateNotifications)
			{
				login.NeedUpdateNotifications = false;
				ServerRpcListener.Instance.SendNotifications(login);
			}

			ServerRpcListener.Instance.SendChangeItem(this, m_changedItems);
            StatContainer.Update();
		}

		public void ClearItemChanges()
		{
			m_changedItems.Clear();
		}

		public InventoryItemData GetItemById(int ItemId)
		{
            InventoryItemData res = GetItems().FirstOrDefault(x => x.ItemId == ItemId);
            if (res != null)
                return res;

            InventoryItemData potion = potions.GetItemById(ItemId);
            if (potion != null)
                return potion;

            return null;
        }

        internal InventoryItemData GetItem(Func<InventoryItemData, bool> predicate)
        {
            return GetItems().FirstOrDefault(predicate);
        }

        public List<InventoryItemData> GetItems()
        {
            return items;
        }

	    public List<InventoryItemData> GetItems(Predicate<InventoryItemData> predicate)
	    {
	        return items.FindAll(predicate);
	    }

        public virtual List<InventoryItemData> GetEquipedItems()
        {
            return equipedItems.GetInventoryItems().ToList();
        }

		public virtual List<InventoryItemData> GetEquipedItemsAndPotions()
		{
			var equippedItems = GetEquipedItems();
			var potionsItems = potions.GetInventoryItems().ToList();

			int totalItemsCount = equippedItems.Count + potionsItems.Count;
			var equipped = new List<InventoryItemData>(totalItemsCount);

			equipped.AddRange(equippedItems);
			equipped.AddRange(potionsItems);

			return equipped;
		}

        internal InventoryItemData GetEquipedItem(Func<InventoryItemData, bool> predicate)
        {
            return GetEquipedItems().FirstOrDefault(predicate);
        }

		public List<InventoryItemData> TradeItems = new List<InventoryItemData>();

		public List<InventoryItemData> GetTradeItems()
		{
			return TradeItems;
		}

		public void RemoveTradeItem(InventoryItemData item)
		{
			var tradeItem = TradeItems.Find(x => x.EqualTradeItem(item));
			if (tradeItem != null)
				TradeItems.Remove(tradeItem);
		}

		//Есть ли у персонажа эта шмотка
		public bool HaveTradeItem(InventoryItemData item)
		{
			//В сумке
			if (GetItems().Exists(x => x.EqualTradeItem(item)))
				return true;

			//Надето
			var equiped = equipedItems.GetItemBySlot(item.SlotId);
			if (equiped != null && equiped.EqualTradeItem(item))
				return true;

			//В поясе
			if (potions.GetInventoryItems().FirstOrDefault(x => x.EqualTradeItem(item)) != null)
				return true;

			return false;
		}

        /// <summary>
        /// Добавить персонажу итем.
        /// ВНИМАНИЕ: у <paramref name="item"/> скорее всего будет изменен <see cref="InventoryItemData.Count"/>!
        /// </summary>
        public void AddItem(InventoryItemData item, AnalyticCurrencyInfo analyticInfo = null, ItemSourceType Source = ItemSourceType.None)
		{
			if (item == null)
			{
				ILogger.Instance.Send("InstancePerson.AddItem item = null");
				return;
			}

            if (item.Resource == ResourceType.None && analyticInfo != null)
            {
                var resourseType = analyticInfo.RealMoneyPurchase ? ResourceType.USD : analyticInfo.Currency;
                var currency = analyticInfo.RealMoneyPurchase ? analyticInfo.USDCost : analyticInfo.Amount;
                var cost = resourseType == ResourceType.None ? 0 : currency;

				analyticInfo.transactionId = AnalyticsManager.GetSHA256(DateTime.UtcNow.ToString() + loginId.ToString());

                var itemInfo = new AnlyticItemInfo()
                {
                    Count = item.Count,
                    Name = item.Name,
                    ItemType = item.ItemType,
                    CharacterLevel = Level,
                    Cost = cost,
                    Source = analyticInfo.Source.ToString(),
                    SourceType = analyticInfo.SourceType,
                    Currency = resourseType,
					transactionId = analyticInfo.transactionId,
					IsStock = analyticInfo.IsStock
				};

                GlobalEvents.Instance.OnPersonGotItem(this, itemInfo);
            }

            item.Equiped = false;
			if (item.ItemType == ItemType.Potion)
				item.SlotId = Slots.Potion1;

            OnAddItemInBag(item);

			//Эти вещи не класть в рюкзак
			if (Wallet.Exist(item.Resource))
            {
				Wallet.TryAdd(item.Resource, item.Count, Source, analyticInfo);
				return;
            }

            //Это здание, отдать логину
            if (item.ItemType == ItemType.Building || item.ItemType == ItemType.BuildingKit)
            {
                login.AddBuilding(item);
                return;
            }

            //Попробуем в пояс доложить
            if (item.ItemType == ItemType.Potion)
			{
				var beltItem = potions.GetItemByBaseId(item.Name);
				if (beltItem != null)
				{
					int count = item.Count;
					beltItem.TryAddStack(item);
					count -= item.Count;
					OnEquipmentSlotStack(beltItem, count);
				}
			}

			if (!item.PersonType.Contains(personType))
			{
				//Это скин, отдать логину
				if (item.ItemType == ItemType.Skin)
				{
					login.SkinAdd(item);
					return;
				}

				ILogger.Instance.Send($"InstancePerson.AddItem: предмет чужого класс, будет удален person={personId} {personType}, item={item.Name}", ErrorLevel.error);
				return;
			}

            UpgradeLevelsCollection.RepareUpgradeLevel(item);

            //одетые вещи тоже стекаются
            if (item.CanEquip)
			{
				var i = equipedItems.GetItemBySlot(item.SlotId);
				if (i != null && i.CanStackWith(item))
				{
					int count = item.Count;
					i.TryAddStack(item);
					count -= item.Count;
					Sos.Debug("предмет добавили к стопке");
					OnEquipmentSlotStack(i, count);
				}
			}

			//вещи в сумке стекаются
			for (int slotId = 0; slotId < items.Count; slotId++)
			{
				var i = items[slotId];
				if (item.Count <= 0)
					break;

				if (i.CanStackWith(item))
				{
                    int count = item.Count;
                    i.TryAddStack(item);
                    count -= item.Count;
                    Sos.Debug("предмет добавили к стопке");
					OnInventorySlotStack(slotId, i, count);
				}
			}

			if (item.Count <= 0)
				return;

			items.Add(item);
			Sos.Debug("предмет добавили в новый слот");

			if (item.IsNew && !item.CanBeNew)
				item.IsNew = false;

			StatContainer.RecalcItemConstsForAddOwnership(item);

			OnInventorySlotFill(item);
		}

		internal void RemoveItemInBag(int ItemId, int count)
		{
			int itemInd = items.FindIndex(x => x.ItemId == ItemId);
			if (itemInd >= 0)
			{
				items[itemInd].Count -= count;
				if (items[itemInd].Count == 0)
				{
                    var item = items[itemInd];
                    items.RemoveAt(itemInd);
					OnInventorySlotEmpty(item);
				}
				else
				{
					var item = items[itemInd];
					OnInventorySlotStack(itemInd, item);
				}
			}
		}

		internal void RemoveItemInPotionSlots(int ItemId, int count)
        {
            var item = potions.GetItemById(ItemId);
            if (item == null)
                return;

			if (item.Count < count)
				throw new ArgumentOutOfRangeException("RemoveItemInPotionSlots item.Count < count");

			potions.RemoveItem(ItemId, count);
			OnEquipmentSlotStack(item, -count);

            NeedUpdatePotion = true;
        }

        internal bool RemoveItemInBag(InventoryItemData item)
        {
            bool isRemoveSuccess = items.Remove(item);
            if (isRemoveSuccess)
            {
				StatContainer.RecalcItemConstsForRemoveOwnership(item);

                OnInventorySlotEmpty(item);
            }
            return isRemoveSuccess;
        }

        internal void RemoveItemInEquiped(InventoryItemData item)
        {
            if (equipedItems.RemoveItem(item))
            {
                StatContainer.RecalcItemConstsForRemoveEquip(item);
				StatContainer.RecalcItemConstsForRemoveOwnership(item);

                OnEquipmentSlotEmpty(item);
            }
        }

		private void OnEquipmentSlotFill(InventoryItemData item)
		{
		}

		private void OnEquipmentSlotEmpty(InventoryItemData item)
		{
		}


		public event Action<InventoryItemData, int> EquipmentSlotStack;

		private void OnEquipmentSlotStack(InventoryItemData item, int count)
		{
            AddChangedItems(item);

			if (EquipmentSlotStack != null)
				EquipmentSlotStack(item, count);
		}

		internal void RemoveAllItemInEquiped()
		{
            equipedItems.ForEachItem(RemoveItemInEquiped);
        }

        internal void RemoveAllItemInPotions()
        {
            potions.ForEachItem((item) => {
                equipedItems.RemoveItem(item);
                OnEquipmentSlotEmpty(item);
            });
        }

        internal void DropItems()
        {
            var bagItems = GetItems();
            for (int i = bagItems.Count - 1; i >= 0; i--)
                RemoveItemInBag(bagItems[i]);

			RemoveAllItemInEquiped();
            RemoveAllItemInPotions();
        }

        public ItemUpgradeCollection UpgradeLevelsCollection;

        public bool EquipItem(InventoryItemData item, out InventoryItemData oldItem)
		{
            UpgradeLevelsCollection.RepareUpgradeLevel(item);


            oldItem = null;

            if (!CanEquip(item))
            {
                ILogger.Instance.Send("Cant equip item " + item.Name, ErrorLevel.error);
                return false;
            }

			bool bInventory = GetItemById(item.ItemId) != null;

			if (bInventory)
				RemoveItemInBag(item);

            oldItem = equipedItems.GetItemByType(item.ItemType);

            if (oldItem != null)
            {
                RemoveItemInEquiped(oldItem);
                if (bInventory)
					AddItem(oldItem);
            }

			//item.Equiped = true;
			item.IsNew = false;

            equipedItems.AddItem(item, item.SlotId);
			StatContainer.RecalcItemConstsForAddEquip(item);
			StatContainer.RecalcItemConstsForAddOwnership(item);
            OnEquipItem(item);

			return true;
        }

        public bool EquipPotionItem(InventoryItemData item, Slots slot, out InventoryItemData oldItem)
        {
	        oldItem = null;

            if (item.ItemType!= ItemType.Potion)
                return false;

			if (potions.IsLocked(slot))
				return false;

			var oldSlot = item.SlotId;
			if (item.Equiped && oldSlot == slot)
				return false;

            bool bInventory = GetItems().Exists(x => x.ItemId == item.ItemId);
			bool bBelt = potions.GetItemById(item.ItemId) != null;

            if (bInventory)
                RemoveItemInBag(item);
			else if (bBelt)
				potions.RemoveItem(item);

			oldItem = potions.RemoveItem(slot);

	        potions.AddItem(item, slot);

            if (oldItem != null)
            {
                if (bInventory)
                    AddItem(oldItem);
				else if(bBelt)
					potions.AddItem(oldItem, oldSlot);
				else
					AddItem(oldItem); //Чтоб не потерять
            }

            OnEquipItem(item);

			return true;
        }

        void ReferPotions(ItemSlot slot)
        {
            if (slot.Locked)
            {
                if (slot.Item != null)
                {
                    //Слот заблочен - положить эликсир в сумку
                    var potion = slot.RemoveItem();
                    AddItem(potion);
                }
            }
            else
            {
                if (slot.Item == null)
                {
                    //Слот доступен - положить из сумки в пояс
                    var itemInBag = GetItem(x => x.Name == slot.LastBaseItemId);
                    if (itemInBag != null && itemInBag.Count > 0)
                    {
                        RemoveItemInBag(itemInBag);
                        potions.AddItem(itemInBag, slot.Slot);
                    }
                }
            }
        }

		public void CheckPotions()
		{
            potions.ForEachSlot(ReferPotions);

			NeedUpdatePotion = true;
		}

        public List<EntityPartInfo> EquippedWeapons
		{
			get { return GetEquipedItems().Where(x => x.IsWeapon).Select(x => x.GetEntityPartInfo()).ToList(); }
		}

		public List<EntityPartInfo> EquippedWears
		{
			get { return GetEquipedItems().Where(x => x.IsWear).Select(x => x.GetEntityPartInfo()).ToList(); }
		}

		public List<string> EquippedNames
		{
			get { return GetEquipedItems().Where(x => x.IsWeapon || x.IsWear).Select(x => x.Name).ToList(); }
		}

		public bool bUpdateInventoryItems;
		public bool bNeedSendCurrency;
		public bool bUpdateResources;

		public List<ResourceCount> GetResourcesCount()
		{
			bool isResource(InventoryItemData item)
			{
				return item.Resource != ResourceType.None;
			}

			ResourceCount getResourceCount(InventoryItemData item)
			{
				return new ResourceCount(item.Resource, item.Count);
			}

			IEnumerable<ResourceCount> resources = items.Where(isResource).Select(getResourceCount);

            resources = resources.ToList()
					.GroupBy(item => item.Resource)
					.Select(grouping => new ResourceCount(grouping.Key, grouping.Sum(g => g.Count)));

			return resources.ToList();
		}

		public bool AddResource(ResourceType resource, int count, AnalyticCurrencyInfo analyticInfo = null)
		{
			if (count == 0)
				return true;

			if (count > 0)
			{
				AddItem(gameXmlData.ItemDataRepository.GetCopyInventoryItemData(resource, count), analyticInfo, ItemSourceType.None);
				return true;
			}

			//Надо удалить ресы
			count = -count;
			if (CountResource(resource) < count)
				return false;

			RemoveResource(resource, count, analyticInfo);
			return true;
		}

		public int CountResource(ResourceType Resource)
		{
			if (Wallet.Exist(Resource))
				return Wallet.Count(Resource);

            int result = 0;
            foreach (var item in GetItems())
				if (item.Resource == Resource)
					result += item.Count;

            return result;
		}

        public bool HaveResource(List<ResourcesToUpdateItemData> resources)
        {
            var res = resources.ResourcesListToDictionary();

			if (!Wallet.HaveResource(res))
                return false;

            foreach (var item in items)
                if (res.ContainsKey(item.Resource))
                    res[item.Resource] = Math.Max(0, res[item.Resource] - item.Count);

            return res.Values.Sum() <= 0;
        }

		public Dictionary<ResourceType, int> HaveResource2(List<ResourcesToUpdateItemData> resources)
		{
			var res = resources.ResourcesListToDictionary();

			Wallet.HaveResource2(res);

			foreach (var item in items)
				if (res.ContainsKey(item.Resource))
					res[item.Resource] = Math.Max(0, res[item.Resource] - item.Count);

            return res;
		}

		public void RemoveResource(List<ResourcesToUpdateItemData> resources)
		{
			RemoveResource(resources.ResourcesListToDictionary());
		}
		
		public void RemoveResource(Dictionary<ResourceType, int> res)
		{
			foreach (var removeResource in res)
			{
				RemoveResource(removeResource.Key, removeResource.Value);
			}
		}

        private List<InventoryItemData> GetResourceList(List<InventoryItemData> lst, ResourceType resourceType)
        {
            List<InventoryItemData> res = null;
            for (int i = 0; i < lst.Count; i++)
            {
            	var item = lst[i];
            	if (item.ItemType != ItemType.Resource || item.Resource != resourceType)
            		continue;

                if (res == null)
                    res = new List<InventoryItemData>();

                res.Add(item);
            }

            return res;
        }
        /// <summary>
        /// Списать некоторое количество ресурсов. 
        /// По умолчанию <paramref name="count"/> равен int.MinValue, в этом случае удалит все ресурсы такого типа.
        /// </summary>
        public void RemoveResource(ResourceType resourceType, int count = int.MinValue, AnalyticCurrencyInfo analyticInfo = null)
		{
			//removeAll
			if (count == int.MinValue)
				count = CountResource(resourceType);

			//Эти вещи не класть в рюкзак
			if (Wallet.Exist(resourceType))
			{
				Wallet.TryAdd(resourceType, -count,ItemSourceType.None, analyticInfo);
				return;
			}

			var personItems = GetItems(); //TODO_Deg не убирать, а то юнит тесты падают

            List<InventoryItemData> items = GetResourceList(personItems, resourceType);
            if (items != null && items.Count > 0)
            {
                items.Sort((x, y) => x.Count - y.Count);
                //пробежаться по списку и поудалять...

                for (int i = 0; i < items.Count; i++)
                {
                    var item = items[i];
                    int countToRemove = Math.Min(item.Count, count);

                    int idx = personItems.IndexOf(item);

                    item.Count -= countToRemove;
                    if (item.Count == 0)
                    {
						personItems.RemoveAt(idx);
                        OnInventorySlotEmpty(item);
                    }
                    else
                    {
                        OnInventorySlotStack(idx, item);
                    }

                    count -= countToRemove;
                    if (count == 0)
                        return;
                }
            }
		}

		#region чат

		public bool[] ChatNotification;

		public List<int> ChatGroup = new List<int>();
		public List<int> BlockPersons = new List<int>();

		#endregion

		#region Алтарь с пассивками

		public void ResetPassiveSkills()
        {
            passiveAbilityStorage.ResetSpendedTalentPoints();
        }

		#endregion

		#region Абилки

        public List<AbilityXmlData> activeAbility = null;
		public List<AbilityStatus> AbilityStatuses = new List<AbilityStatus>();
        public PassiveAbilityStorage passiveAbilityStorage = null;
        public PerksCollection perksCollection = new PerksCollection();//TODO_maximpr доделать чтение из базы

        public void ResetAbilitys()
        {
            activeAbility.Clear();
        }
        
		public void InitAbility(InstancePersonSqlData personSQLData)
		{
            activeAbility = new List<AbilityXmlData>();
			AbilityStatuses = personSQLData.AbilityStatuses;

            //выдаем все абилки в порядке Data.Abilities, это важно
            foreach (var abilityXmlData in Data.Abilities)
            {
                var copy = abilityXmlData.Copy();
                var abilitySaveData = personSQLData.Abilities.FirstOrDefault(x => x.Id == (int)abilityXmlData.Type);
                if (abilitySaveData != null)
                {
                    copy.SlotId = activeAbility.Exists(aa => aa.SlotId == abilitySaveData.SlotId) ? -1 : abilitySaveData.SlotId;
                }
                copy.Init(this);
                ReleaseAbilitySlot(copy.SlotId);
                activeAbility.Add(copy);

				if(!AbilityStatuses.Exists(x => x.Type == abilityXmlData.Type) && abilityXmlData.Tier > 0)
				{
					AbilityStatuses.Add(new AbilityStatus
					{
						Type = abilityXmlData.Type,
						IsNew = !abilityXmlData.IsAvailable(Level),
						IsNotViewed = !abilityXmlData.IsAvailable(Level)
					});
				}
            }

            passiveAbilityStorage = new PassiveAbilityStorage(this);
            passiveAbilityStorage.LoadPassiveAbilities(personSQLData.PassiveAbilitiesBuilds);
            passiveAbilityStorage.ActivateTalentBuild(personSQLData.activeTalentBuild);
		}

		public void InitPerks(InstancePersonSqlData personSQLData)
		{
			perksCollection = personSQLData.PerksCollection;
			if (personType != PersonType.None && perksCollection.UsedPerkList.Count == 0)
				perksCollection = gameXmlData.PerkList.GetStartPerksCollection(personType);
		}

		public List<AbilitySaveData> SaveAbilities()
		{
			var result = new List<AbilitySaveData>();
            foreach (var ability in activeAbility)
                result.Add(new AbilitySaveData { Id = (int)ability.Type, Level = 1, SlotId = ability.SlotId });
			return result;
		}

		public List<AbilityType> GetActiveSkillsId()
        {
            List<AbilityType> resultLit = new List<AbilityType>();
            foreach (var item in activeAbility)
            {
                if(!item.IsHidden)
                    resultLit.Add(item.Type);
            }
            return resultLit;
        }

        //Какие активные абилки использует перс
        public Dictionary<int, AbilityType> GetEquippedSkillsId()
        {
            Dictionary<int, AbilityType> resultLit = new Dictionary<int, AbilityType>();
            foreach (var item in activeAbility)
            {
                if (item.SlotId != -1 && !item.IsHidden)
                    resultLit.Add(item.SlotId, item.Type);
            }
            return resultLit;
        }

		public AbilityXmlData GetAbilityById(int AbilityId)
		{
            return activeAbility.Find(x => (int)x.Type == AbilityId);
		}

        public AbilityXmlData GetAbilityById(AbilityType AbilityId)
		{
            return activeAbility.Find(x => x.Type == AbilityId);
		}

        public void ReleaseAbilitySlot(int slotIndex)
        {
            foreach (var ab in activeAbility)
            {
                if (ab.SlotId == slotIndex)
                {
                    ab.SlotId = -1;
                }
            }
            bNeedSave = true;
        }

        public Errors SetAbilitySlot(AbilityType abilityType, int abilitySlot, bool ignoreUnlock = false)
        {
            //TODO: MAXIMPR or DMA пересчитать статы после смены умений в слотах
            var ability = GetAbilityById(abilityType);
            if (ability == null)
                return Errors.LOGIC_ERROR;

            var unlocked = ignoreUnlock ? ignoreUnlock : ability.IsAvailable(Level);

            if (abilitySlot <= 0 || abilitySlot >= 4 || ability.SlotId == 0 || ability.SlotId == 4 || !unlocked)
                return Errors.LOGIC_ERROR;


            int replacedToSlotId = -1;
            var fromTier = activeAbility.Find(x => x.Tier == ability.Tier && x.SlotId >= 0);
            if (fromTier != null)
            {
                replacedToSlotId = fromTier.SlotId;
                fromTier.SlotId = -1;
            }

            foreach (var ab in activeAbility)
            {
                if (ab.SlotId == abilitySlot)
                {
                    ab.SlotId = ability.SlotId;
                    if (replacedToSlotId != -1)
                        ab.SlotId = replacedToSlotId;
                }
            }

            ability.SlotId = abilitySlot;

            GlobalEvents.Instance.OnPersonEquippedAbility(this, ability);

            bNeedSave = true;
            return Errors.SUCCESS;
        }

		#endregion

        #region Квесты

        //Красная точка, если новые квесты или нужно забрать команду
		public bool ExistNewQuest => login.ExistNewQuest;

		public bool ExistRewardQuest => login.ExistRewardQuest;

		public void UpdateNotifyNewQuest()
		{
			login.UpdateNotifyNewQuest();
		}

		public List<QuestItemData> ActiveQuest => login.ActiveQuest;

        public bool HaveQuest(string baseQuestId)
        {
            return login.HaveQuest(baseQuestId);
        }

        public void QuestsClear(Func<QuestItemData, bool> filter)
        {
	        login.QuestsClear(filter);
        }

        public void AddNewQuest(string questBaseId)
        {
            login.AddNewQuest(questBaseId);
        }

		#endregion

		#region Друзья

		public bool[] FriendsNotification;

		//Друзья
		public List<FriendItemData> Friends { get; private set; }

		//Приглашения
		public List<FriendItemData> Offers { get; private set; }

	    public void InitFriends()
		{
			FriendsNotification = new bool[(int)FriendSection.Count];
			Friends = new List<FriendItemData>();
			Offers = new List<FriendItemData>();
		}

		#endregion

        #region Events

        public event Action LightHouseActivate;

        public void OnLightHouseActivate()
        {
            if (LightHouseActivate != null)
                LightHouseActivate();
        }

		public event Action<MailItemData> GetMail;
		public void OnGetMail(MailItemData mail)
		{
		    GetMail?.Invoke(mail);
		}


		public void OnNewQuests(QuestItemData quest)
		{
			//bNotifyQuest = true;
			//ServerRpcListener.Instance.SendNotifyQuestResponse(this);

			ServerRpcListener.Instance.SendUpdateTaskResponse_NewTask(this, quest);
		}

		public void OnProgressQuests(QuestTaskData task)
		{
			if (task.quest.Type == QuestType.Tutorial && task.Done >= task.Count)
				task = task.quest.Tasks.FirstOrDefault(x => x.quest != null && x.Done < x.Count) ?? task;

			ServerRpcListener.Instance.SendUpdateTaskResponse_UpdateTask(this, task);
		}

        public event Action<QuestItemData> CompleteQuests;

        public void OnCompleteQuests(QuestItemData quest)
        {
            if (CompleteQuests != null)
                CompleteQuests(quest);

			UpdateNotifyNewQuest();
			ServerRpcListener.Instance.SendUpdateTaskResponse_TaskCompleted(this, quest);
        }

        public event Action<int> UpgradePassiveAbility;

        public void OnUpgradePassiveAbility(int abilityId)
        {
            if (UpgradePassiveAbility != null)
                UpgradePassiveAbility(abilityId);
        }

        public event Action<AbilityType> UpgradeActiveAbility;

        public void OnUpgradeActiveAbility(AbilityType abilityId)
        {
            if (UpgradeActiveAbility != null)
                UpgradeActiveAbility(abilityId);
        }

		public event Action<ResourceType, int, ItemSourceType> WalletResourceChange;

        public void OnWalletResourceChange(ResourceType resource, int count, ItemSourceType source = ItemSourceType.None)
		{
			if (WalletResourceChange != null)
				WalletResourceChange(resource, count, source);
		}

        public event Action<int> GiveGold;

		public void OnGiveGold(int count)
        {
            if (GiveGold != null)
				GiveGold(count);
        }

        public event Action<int> UnlockLocation;

        public void OnUnlockLocation(int locationId)
        {
            if (UnlockLocation != null)
                UnlockLocation(locationId);
        }

        public event Action<string> StatChange;

        public void OnStatChange(string stat)
        {
            if (StatChange != null)
                StatChange(stat);
        }

		public event Action<MapTargetObject, int> Hit;

		public void OnHit(MapTargetObject target, int damage)
		{
			if (Hit != null)
				Hit(target, damage);
		}

        public event Action<MapTargetObject, int> Kill;

		public void OnKill(MapTargetObject target, int damage)
        {
            if (Kill != null)
				Kill(target, damage);
        }

		public event Action<MapLoot> GiveLoot;

        internal void OnGiveLoot(MapLoot loot)
        {
            if (GiveLoot != null)
                GiveLoot(loot);

			if (m_shiningPathData != null && m_shiningPathData.Place == ActualPlaceType.MapLoot)
				m_shiningPathData.NeedRecreatePath = true;
        }

        public event Action<InventoryItemData> BuyItem;

        public void AddChangedItems(InventoryItemData item)
        {
            if (!m_changedItems.Contains(item))
                m_changedItems.Add(item);
        }

        public void OnBuyItem(InventoryItemData item)
        {
            if (BuyItem != null)
                BuyItem(item);

			if(bRunTutorial)
				Dialogs.Add("OnBuyItem_" + item.Name);
        }

        public event Action<InventoryItemData> EquipItemEvent;

        public void OnEquipItem(InventoryItemData item)
        {
            if (EquipItemEvent != null)
                EquipItemEvent(item);

            var map = InstanceGameManager.Instance.GetMapByGameId(GameId);
            map?.OnPersonEquipItem(this, new AnlyticItemInfo {Name = item.Name, Count = item.Count});

            //Обновить счетчик на клиенте
            bUpdateInventoryItems = true;

			if (item.ItemType == ItemType.Potion)
				NeedUpdatePotion = true;

			OnEquipmentSlotFill(item);

			//Получили итем - удалим у торговца!
			RemoveTradeItem(item);

            if (item.ItemType == ItemType.Skin)
                login.SkinAdd(item);
        }

        public event Action<InventoryItemData> AddItemInBag;

        public void OnAddItemInBag(InventoryItemData item)
        {
            if (AddItemInBag != null)
                AddItemInBag(item);

			//Получили итем - удалим у торговца!
			RemoveTradeItem(item);
        }

        public event Action<InventoryItemData> UpgradeItem;

		public void OnUpgradeItem(InventoryItemData item)
		{
			UpgradeItem?.Invoke(item);
		}

		public event Action<InventoryItemData> UseItem;

		public void OnUseItem(InventoryItemData item)
		{
			UseItem?.Invoke(item);
		}

		public event Action<InventoryItemData> ApplyItem;

		public void OnApplyItem(InventoryItemData item)
		{
			ApplyItem?.Invoke(item);
		}

		public event Action<ArenaRewardChestType> OpenChest;

		public void OnOpenChest(ArenaRewardChestType chestType)
		{
			OpenChest?.Invoke(chestType);
		}

        public void OnSafeZoneEnter()
        {
            //при в ходе в безопасную зону вешаем инфлюенс безопасной зоны
            var infl = InfluenceXmlDataList.Instance.GetInfluence(syn, InfluenceHelper.SafeZone, 604800, 1);
            bool successApply = StatContainer.AddInfluence(infl);
            if (!successApply)
                InfluenceXmlDataList.Instance.BackPool(infl);

            if (syn.SpellCastAction != null)
            {
                syn.SpellCastAction.Reset();
                syn.SpellCastAction = null;
            }
        }

        public void OnSafeZoneExit()
        {
            //при выходе из безопасной зоны удаляем инфлюенс безопасной зоны
            StatContainer.RemoveInfluence(Stats.SafeZone);
        }


        //персонаж начинает наносить урон
        public delegate void OutDamageAction(StatContainer StatContainer, int damage, bool crit, ref double mult, DamageType type, bool autoAttack = false);
        public event OutDamageAction OutDamage;

        internal void OnOutDamage(StatContainer StatContainer, int damage, bool crit, ref double mult, DamageType type, bool autoAttack = false)
        {
            OutDamage?.Invoke(StatContainer, damage, crit, ref mult, type, autoAttack);
        }

        public delegate void InDamageAction(MapTargetObject attacker, ref int damage, bool resetCast);
        public event InDamageAction InDamage;

        internal void OnInDamage(MapTargetObject attacker, ref int damage, bool resetCast)
        {
            if (InDamage != null)
                InDamage(attacker, ref damage, resetCast);
        }

        public event Action Dodge;

        internal void OnDodge()
        {
            if (Dodge != null)
                Dodge();
        }

        public event Action<InstancePerson> Move;
        public void OnMove()
        {
            if (Move != null)
                Move(this);
        }

        public event Action<InstancePerson> VisitLocation;
        public void OnVisitLocation()
        {
            if (VisitLocation != null)
                VisitLocation(this);
        }

		public event Action<GameModeType> GameModeChange;
		public void OnGameModeChange(GameModeType gameType)
		{
			if (GameModeChange != null)
				GameModeChange(gameType);
		}

        public event Action<PersonBehaviour> PersonDie;
        public void OnPersonDie(PersonBehaviour person)
        {
            if (PersonDie != null)
                PersonDie(person);
        }

		public event Action<MonsterBehaviour> MonsterEntityReady;
		public void OnMonsterEntityReady(MonsterBehaviour monsterBehaviour)
		{
			if (MonsterEntityReady != null)
			{
				MonsterEntityReady(monsterBehaviour);
			}
		}

        #endregion

        #region Stats
        public PersonStatContainer StatContainer;
        #endregion

		#region покупка за реал

		//public int TransactionId;

		//public Dictionary<int, Action<AccountChangeParams>> Transactions = new Dictionary<int, Action<AccountChangeParams>>();
  //      public Dictionary<AccountChangeReason, DateTime> LastActionDate = new Dictionary<AccountChangeReason, DateTime>();

		//public void EmeraldCheat(int summ)
		//{
		//	var data = new AccountChangeParams();
		//	data.tranId = -1;
		//	data.loginId = loginId;
		//	data.summ = -summ; // Чтобы добавились
		//	data.itemId = -1;
		//	data.reason = AccountChangeReason.GameReward;
		//	data.source = GetCurrencySource.Cheat;
		//	data.fromInst = true;
		//	data.buyParams = string.Empty;

		//	ServerRpcListener.Instance.LoginAccountChangeRequest(data);
		//}

        //private DateTime getLastActionDate(AccountChangeReason reason)
        //{
        //    if (LastActionDate.ContainsKey(reason))
        //        return LastActionDate[reason];
        //    return DateTime.MinValue;
        //}

        //public void ResetLastActionDate(AccountChangeReason reason)
        //{
        //    LastActionDate[reason] = DateTime.MinValue;
        //}

        //public bool CanRepeatEvent(AccountChangeReason reason)
        //{
        //    return reason == AccountChangeReason.BuyItemFromNPC || reason == AccountChangeReason.BuyShopItem;
        //}

		//public void BuyForEmeralds(int price, AccountChangeReason reason, Action<AccountChangeParams> action)
		//{
  //          //можно ли повторить запрос если такой уже есть...
  //          if (!CanRepeatEvent(reason))
  //          {
  //              DateTime last = getLastActionDate(reason);
  //              if (last != DateTime.MinValue && last.AddSeconds(ServerConstants.Instance.BuyForEmeraldTimeout) > TimeProvider.UTCNow)
  //                  return;
  //          }

		//	ILogger.Instance.Send("ЫЫЫ: BuyForEmeralds " + loginId + " " + price);

		//	var data = new AccountChangeParams();
		//	data.tranId = TransactionId++;
		//	data.loginId = loginId;
		//	data.summ = price;
		//	data.itemId = -1;
  //          data.reason = reason;
		//	data.source = GetCurrencySource.None;
		//	data.fromInst = true;
		//	data.buyParams = string.Empty;
  //          // если мы делаем что то бесплатно например в туторе, не стучаться к платежке, иначе блочит тутор если плтежка не доступна
  //          if (price == 0 && reason != AccountChangeReason.AccountRequest)
  //          {
  //              data.success = true;
  //              action(data);
  //              return;
  //          }

  //          Transactions[data.tranId] = action;

		//	ServerRpcListener.Instance.LoginAccountChangeRequest(data);
		//}

		//public void LoginAccountChangeResponse(AccountChangeParams data)
		//{
		//	ILogger.Instance.Send("ЫЫЫ: LoginAccountChangeResponse " + loginId + " " + JSON.JsonEncode(data.JsonEncode()));

		//	if (data.success)
		//	{
		//		Wallet.Set(ResourceType.Emerald, data.lAfterUpdate);
		//		ServerRpcListener.Instance.SendCurrencyResponse(this);
		//	}

		//	if (!data.fromInst || data.tranId == -1)
		//		return;

		//	if (Transactions.ContainsKey(data.tranId))
		//	{
  //              ShopManager.ShutUpAndTakeMyMoney(this, data);

		//		Transactions[data.tranId](data);
		//		Transactions.Remove(data.tranId);

  //              if (!CanRepeatEvent(data.reason))
  //                  ResetLastActionDate(data.reason);
		//	}
		//	else
		//	{
		//		ILogger.Instance.Send("LoginAccountChangeResponse: не найдена транзакция! data=" + JSON.JsonEncode(data.JsonEncode()));
		//	}
		//}

		#endregion

		#region почта

		private List<MailItemData> m_mails;

		public int MailCount { get { return m_mails.Count; } }

		public List<MailItemData> GetMails()
		{
			m_mails.RemoveAll(x => x.RemoveTime < TimeProvider.UTCNow);
		    return m_mails;
		}

        public void SetMails(List<MailItemData> mails)
		{
			m_mails = mails;
			m_mails.RemoveAll(x => x.RemoveTime < TimeProvider.UTCNow);
		}

		public MailItemData GetMailById(int mailId)
		{
            return m_mails.FirstOrDefault(x => x.MailId == mailId);
		}

        #endregion

		#region group

		private Group m_group;

		public virtual Group Group
		{
			get { return m_group; }
			set { m_group = value; }
		}

		#endregion

		//Список ресурсов нехватающих для текущего действия
		public NeedForResource NeedForResource = new NeedForResource();

		public NeedForResource GetNeedForResource()
		{
			NeedForResource.Clear();
			return NeedForResource;
		}

        //public PersonItemStorage AssetStorage;

		internal bool CanEquip(InventoryItemData a/*, ref Errors err*/)
        {
            if (a == null || !a.CanEquip)
				return false;
            if (a.Level > Level)
                return false;
            if (!a.PersonType.Contains(personType))
                return false;

            return true;
        }

        public bool bFavorOfGods = false;

        /// <summary>
        /// тут реагируем на изменения обычных статов
        /// </summary>
        /// <param name="stat"></param>
        internal void AttributeStatChange(Stats stat)
        {
            if (syn == null)
                return;

            double statValue = StatContainer.GetStatValue(stat);

            switch (stat)
            {
                case Stats.MoveSpeed:
                    syn.SetSpeedMult();
                    break;
                case Stats.Stun:
                    syn.bStun = statValue > 0;
                    if (syn.bStun)
                    {
                        if (syn.SpellCastAction != null)
                            syn.SpellCastAction.Reset();
                    }
                    syn.SendSetMotionLimits(syn.bStun, syn.bRoot);
                    break;
                case Stats.FavorOfGods:
                    bFavorOfGods = statValue > 0;
                    break;
                case Stats.CanNotDie:
                    if(!bRunTutorial)
                        syn.SetTransparency(statValue > 0 ? 0.4f : 1);
                    break;
                case Stats.Silence:
                    syn.bSilence = statValue > 0;
                    break;
                case Stats.RapidFire:
                    if (statValue == 0)
                    {
                        RapidFireXmlData rapidFire = (RapidFireXmlData)GetAbilityById(AbilityType.RapidFire);
                        rapidFire.OnRemoveinfluence();
                    }
                    break;

                case Stats.Frozen:
                    //заморозка снимает горение
                    if (statValue > 0)
                        StatContainer.RemoveInfluence(Stats.Burning);
                    break;

                case Stats.Burning:
                    //горение снимает заморозку
                    if (statValue > 0)
                        StatContainer.RemoveInfluence(Stats.Frozen);
                    break;
                case Stats.MoveMode:
                    bool enable = statValue > 0;
                    syn.SendRpc(ClientRpc.SetMoveMode, enable);
                    break;
                case Stats.Root:
                    syn.bRoot = statValue > 0;
                    syn.SendSetMotionLimits(syn.bStun, syn.bRoot);
                    break;
            }
        }
        

        /// <summary>
        /// тут реагируем на изменения статов вычисляемых по формулам
        /// </summary>
        /// <param name="stat"></param>
        internal void CalcStatChange(string stat)
        {
			if (StatContainer == null)
                return;

            //Sos.Debug("CalcStatChange " + stat);
            double statValue = StatContainer.GetStatValue(stat);
			if (syn != null)
			{
				switch (stat)
				{
					case "HP":
						syn.MaxHealth = (int) statValue;
						syn.Health = Math.Min(syn.Health, syn.MaxHealth);
						syn.SendHealth();
						break;
					case "SP":
						syn.MaxEnergy = (int) statValue;
						syn.Energy = Math.Min(syn.Energy, syn.MaxEnergy);
						syn.SendEnergy();
						break;
					case "Stamina":
						syn.MaxStamina = (int)statValue;
						syn.Stamina = Math.Min(syn.Stamina, syn.MaxStamina);
						syn.SendStamina();
						break;
					case "HPOW":
						syn.hpowChanged = true;
						break;
					case "ASPD":
						syn.SendPeriodAttack();
						break;
				}
			}

			switch (stat)
			{
				case "PotionsCount":
					potions.SetLockedSize((int)statValue);
					break;
            }

            OnStatChange(stat);
        }

		public virtual double MaxHealth { get { return StatContainer.GetStatValue("HP"); } }
		public virtual double MaxEnergy { get { return StatContainer.GetStatValue("SP"); } }

        [Obsolete("Используй перегрузку с параметром AnalyticInfo. что туда вписывать узнай у Миши")]
        public bool TryAddGold(int delta)
        {
            return AddResource(ResourceType.Gold, delta, null);
        }
        /// <summary>
        /// Отнять или добавить голду персонажу в количестве <paramref name="delta"/>.
        /// <para>
        /// В случае неуспеха (например, не хватает денег для списания требуемой суммы) вернет false.
        /// </para>
        /// </summary>
        public bool TryAddGold(int delta, AnalyticCurrencyInfo analyticInfo)
		{
			return AddResource(ResourceType.Gold, delta, analyticInfo);
        }

        public int MaxHeroPower;
        /// <summary>
        /// gearscore
        /// </summary>
        public int HeroPower
		{
			get { return (int)StatContainer.GetStatValue("HPOW"); }
		}

        public GSRelatedToLocation BigGearScore
        {
            get
            {
	            var map = syn.map;
	            if (map.IsCamp)
		            return GSRelatedToLocation.Equal;

                return GetBigGearScore(Map.Edge.RecommendedGS);
            }
        }

		public GSRelatedToLocation GetBigGearScore(int LocationGSMax)
        {
            if (HeroPower < LocationGSMax + ServerConstants.Instance.LevelGsMinOffset)
                return GSRelatedToLocation.Below;

            if (HeroPower > LocationGSMax + ServerConstants.Instance.LevelGsMaxOffset)
                return GSRelatedToLocation.More;

            return GSRelatedToLocation.Equal;
        }

		public List<string> Dialogs = new List<string>();

		private ShiningPathData m_shiningPathData;

		internal void SetShiningPath(ActualPlaceType place, Vector3? pos = null, List<string> data = null, MinimapItemType? minimapType = null,
			int showCount = 7, bool canReturn = true, bool hideWhenDestinationReached = true)
		{
			var oldDestPoint = m_shiningPathData?.DestinationPoint;
			var oldMiniMap = m_shiningPathData?.MiniMap;

			if (m_shiningPathData == null)
			{
				var shiningPathData = new ShiningPathData(place, showCount, canReturn, hideWhenDestinationReached);
				if (pos != null)
				{
					shiningPathData.Pos = pos.Value;
				}
				shiningPathData.Data = data;
				shiningPathData.MiniMap = minimapType;

				m_shiningPathData = shiningPathData;
			}
			else
			{
				m_shiningPathData.UpdateSettings(place, pos, data, minimapType, showCount, canReturn, hideWhenDestinationReached);
			}

			if (oldDestPoint != m_shiningPathData.DestinationPoint || oldMiniMap != m_shiningPathData.MiniMap)
			{
				syn?.map.MinimapManager.SendRemoveObject(this, ShiningPathGetMinimap(oldDestPoint, oldMiniMap));
				syn?.map.MinimapManager.SendAddObject(this, ShiningPathGetMinimap());
			}
		}

		internal void HideShiningPath(bool updateMinimap = true)
		{
			if (m_shiningPathData == null)
				return;

			if (updateMinimap)
				syn?.map.MinimapManager.SendRemoveObject(this, ShiningPathGetMinimap());

			m_shiningPathData.HideEffects(this);
			m_shiningPathData = null;
		}

		// обновляет инфу про вообще всю миникарту. пользоваться редко и с умом
        public void UpdateMinimapFull()
        {
            if (syn == null)
            {
                ILogger.Instance.Send("UpdateMinimapFull instancePerson.syn == null loginId: " + loginId + " personId: " + personId, ErrorLevel.error);
                return;
            }

            syn.bNeedUpdateMiniMapFull = true;
        }

        public void UpdateShiningPath()
		{
			if (m_shiningPathData == null)
				return;

			m_shiningPathData.Update(this);
		}

		public MinimapData ShiningPathGetMinimap()
		{
			return ShiningPathGetMinimap(m_shiningPathData?.DestinationPoint, m_shiningPathData?.MiniMap);
		}

		public MinimapData ShiningPathGetMinimap(Vector3? point, MinimapItemType? minimap)
		{
			if (!point.HasValue)
				return null;

			if (!minimap.HasValue)
				minimap = bRunTutorial ? MinimapItemType.Quest : MinimapItemType.FamousSpot;

			return new MinimapData(minimap.Value, new Vector2(point.Value.X, point.Value.Z));
		}


        public ScenarioStringCollection ScenarioStrings = new ScenarioStringCollection(null);

        public List<InfluenceSaveData> InfluenceDatas = null; //поле для сохранения инфлуенсов в БД. отдает свою инфу и стирается 
	    

	    public void OnReconnected()
		{
			if (m_shiningPathData != null)
				m_shiningPathData.OnPersonReconnected(this);
        }

	    public void RemoveFromMap()
	    {
			if (syn == null)
				return;

            for (int i = 0; i < activeAbility.Count; i++)
            {
                activeAbility[i].OnPersonDestroy();
            }
            

			var persons = syn.map.SelectPersons(x => x.sender != player);
			ServerRpcListener.Instance.SendRpc(persons.ClientRpc.RemoveEntity, syn.eType, syn.Id);

			syn.bOnMap = false;
	        syn.bOnGetMapInfoRequest = false;
	        syn.map.OnLeftLocation(this);
	        syn.map.RemovePerson(syn);
	    }

        public void SetLevel(int newLevel)
        {
            Level = newLevel;
            StatContainer.ResetCalcStats("level");
			syn?.SendExperience();
			CheckPotions();
        }

		public void AddExpForCollection()
		{
			AddExperience(gameXmlData.ExpData.ExpForCollection(this));
		}

		public void AddExperience(MonsterBehaviour monster)
		{
			AddExperience(gameXmlData.ExpData.CalcExp(monster, this));
		}

		public void AddExperience(int exp)
		{
			AddExperience(ref exp);
		}

		public void AddExperience(ref int exp)
		{
            var map = InstanceGameManager.Instance.GetMapByGameId(GameId);
            if (exp > 0)
			{
				int maxExp = (int) CommonFormulas.Instance.LevelToExp.Calc(gameXmlData.ExpData.MaxLevel - 1);
				int curExp = (int)StatContainer.GetStatValue(Stats.Exp);
				exp = Math.Min(curExp + exp, maxExp) - curExp;
            }
			if (exp == 0)
				return;

            map.OnPersonAddExp(this, exp);
            StatContainer.AddStat(Stats.Exp, exp);

			var newLevel = (int)StatContainer.GetStatValue("LevelTemp");

            if (Level != newLevel)
			{
				int oldLevel = Level;
                SetLevel(newLevel);

				map.OnPersonLevelUp(this, oldLevel, newLevel);
			}
            else
                syn?.SendExperience();
		}
	}
}
