﻿using Assets.Scripts.InstanceServer.Entries.XmlData;
using System.Collections.Generic;
using System.Xml.Serialization;
using UtilsLib.DegSerializers;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Entries.Influences
{
    [Grid]
    public class StatMultiInfluence : StatInfluence
    {
        public StatMultiInfluence()
        {

        }

        public StatMultiInfluence(StatContainer container, string Group, float time, Stats stats, PlayerStatus Icon, double value): base(container, Group, time, stats, Icon, value)
        {
        }

        [XmlElement(typeof(SetStatInfluence))]
        [XmlElement(typeof(InnerStatInfluence))]
        [XmlElement(typeof(WaterInfluence))]
        [XmlElement(typeof(ChargingInfluence))]
        [XmlElement(typeof(ActionListInfluence))]
        [XmlElement(typeof(FirstOutDamageRemoveInfluence))]
        [XmlElement(typeof(DamageNearbyEnemy))]
        [XmlElement(typeof(PeriodicalHeal))]
        [XmlElement(typeof(InfluenceTimeRefresher))]
        public List<InnerStatInfluence> list;

        public void AddInner(Stats stat)
        {
            var inner = new InnerStatInfluence();
            inner.parentInfluence = this;
            inner.stat = stat;
            list.Add(inner);
        }

        public override void Apply()
        {
            var owner = container.GetOwner();
            if (EnableCondition == null || EnableCondition.Check(owner.map, owner, null))
            {
                foreach (var infl in list)
                {
                    infl.parentInfluence = this;
                    infl.SetEnable(true, value);
                }
                SetEnable(true);
            }
        }

        public override void Remove()
        {
            foreach (var infl in list)
            {
                infl.SetEnable(false, value);
            }
            base.Remove();
        }

        internal override void Restore()
        {
            base.Restore();
            foreach (var infl in list)
            {
                infl.Restore();
            }
        }

        public override void Update()
        {
            foreach (var infl in list)
            {
                infl.ParentUpdate();
            }
            base.Update();
        }

        internal override StatInfluence Clone()
        {
            var copy =  this.Copy();
            copy.list = list.CopyList();
            return copy;
        }

        public override double GetStatValue(Stats type)
        {
            double res = 0;

            foreach (var infl in list)
            {
                if (infl.stat == type)
                    res += infl.appliedValue;
            }

            return res;
        }

        public override bool ContainsStat(Stats type)
        {
            return list.Exists(i => i.stat == type);
        }
    }
}
