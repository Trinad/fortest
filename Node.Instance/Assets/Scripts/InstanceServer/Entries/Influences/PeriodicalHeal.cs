﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using System.Xml.Serialization;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Entries.Influences
{
    public class PeriodicalHeal : InnerStatInfluence
    {
        [XmlAttribute] public float HealPeriod;

        private float nextRecalc;

        public override void Restore()
        {
            nextRecalc = 0;
            base.Restore();
        }

        public override void SetEnable(bool enable, double value)
        {
            base.SetEnable(enable, value);
            if (enable)
                nextRecalc = Time.time;
        }

        public override void ParentUpdate()
        {
            if (Time.time < nextRecalc)
                return;

            Heal();
            nextRecalc = Time.time + HealPeriod;
        }

        private void Heal()
        {
            var owner = container.GetOwner();

            MapTargetObject attacker;

            if (parentInfluence.attacker != null)
                attacker = parentInfluence.attacker;
            else
                attacker = owner;

            if (owner != null && owner.Health < owner.MaxHealth)
                owner.AddHealth(attacker, (int)appliedValue);
        }
    }
}
