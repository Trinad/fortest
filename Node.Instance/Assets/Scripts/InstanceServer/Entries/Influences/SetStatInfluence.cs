﻿using System;
using UtilsLib.Logic.Enums;
using System.Xml.Serialization;
using UtilsLib.BatiyScript;

namespace Assets.Scripts.InstanceServer.Entries.Influences
{
    public class SetStatInfluence : InnerStatInfluence
    {
        double startAppliedValue;
        public override void SetEnable(bool enable, double value)
        {
            if (!m_Enable && enable)
            {
                startAppliedValue = fvalue.IntCalc(value);
                double currentStatvalue = container.GetStatValue(stat);
                appliedValue = startAppliedValue - currentStatvalue;
                container.AddStat(stat, appliedValue);
            }

            if (m_Enable && !enable)
            {
                container.RemoveStat(stat, appliedValue);
            }

            m_Enable = enable;
        }

        public override void ParentUpdate()
        {
            //вдруг ктото пытается менять стат. надо менять тут обратно на startAppliedValue
            if (m_Enable)
            {
                double currentStatvalue = container.GetStatValue(stat);
                if (currentStatvalue != startAppliedValue)
                {
                    appliedValue += startAppliedValue - currentStatvalue;
                    container.AddStat(stat, startAppliedValue - currentStatvalue);
                }
            }
        }
    }
}
