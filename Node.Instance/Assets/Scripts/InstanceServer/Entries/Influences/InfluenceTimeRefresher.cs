﻿using System;
using UtilsLib.Logic.Enums;
using System.Xml.Serialization;
using UtilsLib.BatiyScript;

namespace Assets.Scripts.InstanceServer.Entries.Influences
{
    public class InfluenceTimeRefresher : InnerStatInfluence
    {
        public override void SetEnable(bool enable, double value)
        {
            if (!m_Enable && enable)
            {

                var inflList = container.Influences.FindAll(x => x.Id == parentInfluence.Id);
                foreach (var item in inflList)
                {
                    item.SetFinishTime(parentInfluence.finishTime);
                }
            }

            m_Enable = enable;
        }
    }
}
