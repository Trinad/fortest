﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils.Logger;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using Assets.Scripts.Networking.ClientDataRPC;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.DegSerializers;
using Assets.Scripts.Utils.Common;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Entries.Influences
{
	public class GravityInfluence : StatInfluence
	{

	    public bool UseTotalHeight = true;

        public override void Apply()
        {
            base.Apply();
            startJumpTime = Time.time;
        }

        public override void Remove()
        {
            PersonBehaviour person = container.personOwner.syn;
            person.position = endJumpPos;
            base.Remove();
        }

        public float JumpFlyTime;

        public override void Update()
        {
            PersonBehaviour person = container.personOwner.syn;

            float elapsed = Time.time - startJumpTime;
            var curPos = startJumpPos + (endJumpPos - startJumpPos) * Math.Min(1, elapsed / JumpFlyTime); //В плоскости XZ двигается равномерно

            //По оси Y двигается по параболе
            float vy = (endJumpPos.Y - startJumpPos.Y) / JumpFlyTime + ACCELERATION * JumpFlyTime / 2;
            float y = startJumpPos.Y + vy * elapsed - ACCELERATION * elapsed * elapsed / 2;
            person.position = new Vector3(curPos.X, y, curPos.Z);

            var h = person.map.GetHeightFloat(person.position.X, person.position.Z, UseTotalHeight);
            if (person.position.Y < h)
            {
                person.position = Helper.UltraCorrectYPosition(person.map, person.position, UseTotalHeight);
            }

            base.Update();
        }

        private float startJumpTime;
        public Vector3 startJumpPos;
        public Vector3 endJumpPos;
        public const float ACCELERATION = 10f;
    }
}
