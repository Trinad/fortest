﻿using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic.Enums;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Entries.Influences
{
    public class PeriodDamageInfluence : StatMultiInfluence
    {
        [XmlAttribute]
        public Elements Element = Elements.Neutral;

        [XmlAttribute]
        public float Period;

        //сбивает ли каст урон от этого инфлуенса
        [XmlAttribute]
        public bool ResetCast = false;

        public HitEffectXmlData HitFxData;

        public Elements GetAttackElement()
        {
            if (Element == Elements.FromWeapon)
            {
                if (attacker == null)
                    return Elements.Neutral;
                else
                    return attacker.AttackElement();
            }
            else
                return Element;
        }

        public PeriodDamageInfluence()
        {
        }

        public PeriodDamageInfluence(MapTargetObject attacker, StatContainer container, string group, float time, Stats stat, PlayerStatus status, double value, float period)
            : base(container, group, time, stat, status, value)
        {
            this.Period = period;
            this.attacker = attacker;
        }

        float lastHit = 0;
        public override void Update()
        {
            base.Update();

            if (value == 0)
                return;

            if (!(Time.time - lastHit > Period))
                return;

            var target = container.GetOwner();
            if (target == null || target.IsDead)
                return;

            target.Hit(attacker, (int)value, DamageType.Dot, GetAttackElement(), out bool crit, canMiss: false, resetCast: ResetCast,
                absoluteHit: false, critType: CritType.DotAttack, hitFxData: HitFxData);
            lastHit = Time.time;
        }
    }
}
