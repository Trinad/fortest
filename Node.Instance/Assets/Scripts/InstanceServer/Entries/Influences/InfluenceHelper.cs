﻿

namespace Assets.Scripts.InstanceServer.Entries.Influences
{
	public enum InfluenceHelper
    {
        //RapidFireHitRate = 1, //+ к точности стрельбы (компенсация штрафа) при беглом огне
        //Blessing = 19,//благословение со статуи
        //ConcentratedShooting = 37,//концентрация (веер стрел)
        Freeze = 49,//заморозка в играх по подписке
        CanNotDie = 50,//обычная неуязвимость без эффекта
        StunWithoutIcon = 55,
        MoveMode = 57,//движение контролирует сервер
        SafeZone = 58,
        PeriodDamageMoveMode = 59,//движение контролирует сервер и наносится периодический урон
        FavorOfGodsInflId = 69,
        //FrozenImmunity = 83,
        //PlayerKiller = 93,
        //Gladiator = 97, больше не выдается
        //KeyBlue = 98,
        //KeyGreen = 99,
        //KeyYellow = 100,
        //GoldenShield = 101,
        //KeyBlueNoDeath = 103,
        //KeyGreenNoDeath = 104,
        //KeyYellowNoDeath = 105,
        //MultiShot = 107,
        //ShieldBlockSlow = 133,
    }
}
