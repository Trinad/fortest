﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils.Logger;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using Assets.Scripts.Networking.ClientDataRPC;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.DegSerializers;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.HitActions;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Entries.Influences
{
    [Flags]
    public enum ApplyMode
    {
        None = 0,
        Remove = 1,
        Add = 2,
        Update = 4,
    }

    public class ActionListInfluence : InnerStatInfluence
    {
        [XmlAttribute]
        public ApplyMode Mode = ApplyMode.None;

		[XmlAttribute]
		public float Period = 0;

		[XmlElement(typeof(SendCreateFxResponseHitAction))]
        [XmlElement(typeof(Chance))]
        [XmlElement(typeof(AddInfluence))]
        [XmlElement(typeof(KillTarget))]
        [XmlElement(typeof(AddHP))]
        [XmlElement(typeof(AddEnergyHitAction))]
        [XmlElement(typeof(InfluenceChance))]
        [XmlElement(typeof(RemoveInfl))]
        [XmlElement(typeof(RemoveInflType))]
        [XmlElement(typeof(Knockback))]
        [XmlElement(typeof(HitTarget))]
		[XmlElement(typeof(WaitTimer))]
		public List<HitActionBase> HitActionList;

		float time = 0;
		public override void SetEnable(bool enable, double value)
        {
            bool need = (!enable && (Mode & ApplyMode.Remove) > 0) || (enable && (Mode & ApplyMode.Add) > 0);

            if (need)
            {
                foreach (var action in HitActionList)
                    action.Execute((int)value, container.GetOwner(), parentInfluence.attacker, null);
            }

			time = Time.time;
		}

		public override void ParentUpdate()
        {
            base.ParentUpdate();
            if ((Mode & ApplyMode.Update) > 0 && Time.time > time)
            {
                foreach (var action in HitActionList)
                    action.Execute((int)parentInfluence.value, container.GetOwner(), parentInfluence.attacker, null);
				time = Time.time + Period;
			}
        }
    }
}
