﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils.Logger;
using UtilsLib.Logic.Enums;
using Assets.Scripts.Networking.ClientDataRPC;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.DegSerializers;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.HitActions;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Conditions;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using System.ComponentModel;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer.Entries.Influences
{
    [Grid]
	public abstract class StatInfluence: ICanCopy
	{
        [TextField(false)]
        [XmlAttribute]
        public int Id;

        [DefaultValueAttribute(-1)]
        [TextField(false)]
        [XmlAttribute]
        public int FxId = -1;
        [XmlAttribute]
        public int AlternativeFxId = -1;
        [XmlAttribute]
        public PlayerTeam TeamColor;

        [TextField(false)]
        [DefaultValueAttribute(1)]
        [XmlAttribute]
        public float FxScale = 1;

        [DefaultValueAttribute(true)]
        [TextField(false)]
        [XmlAttribute]
        public bool DeathRemove = true;

        [DefaultValueAttribute(false)]
        [TextField(false)]
        [XmlAttribute]
	    public bool OnAttackerDeathRemove = false; //Если true когда атакующий умрет этот инфлюенс будет снят с цели

        [DefaultValueAttribute(true)]
        [TextField(false)]
        [XmlAttribute]
        public bool NeedSave = true;

        [DefaultValueAttribute(Stats.None)]
        [TextField(false)]
        [XmlAttribute]
        public Stats ConflictStat = Stats.None;

        [XmlIgnore]
        public bool NotBackPool = false;

        [XmlIgnore]
        public double value;
		[XmlIgnore]
		public float addTime;
        [XmlIgnore]
        public float startTime;
        [XmlIgnore]
        public float finishTime;
        [XmlIgnore]
        public float time;
        [XmlIgnore]
        public float originTime;

        [TextField(false)]
        [DefaultValueAttribute(PlayerStatus.None)]
        [XmlAttribute]
        public PlayerStatus Icon = PlayerStatus.None;

        [TextField(false)]
        [DefaultValueAttribute(InfluenceHintType.None)]
        [XmlAttribute]
        public InfluenceHintType HintType = InfluenceHintType.None;

        [TextField(false)]
        [XmlAttribute]
        public string Group = null;

		[XmlIgnore]
		public int SubGroup;

        [DefaultValueAttribute(1)]
        [TextField(false)]
        [XmlAttribute]
        public int MaxStack = 1;

        [XmlAttribute]
        public bool RefreshTimeWhenAdd = false;

        internal virtual void Restore()
        {
            attacker = null;
            container = null;
            bRemove = false;
            m_Enable = false;
			SubGroup = 0;
        }

        [DefaultValueAttribute("")]
        [TextField(false)]
        [XmlAttribute]
        public string TechnicalName = string.Empty;

        [DefaultValueAttribute(typeof(List<LocalizationAttribute>), null)]
        public List<LocalizationAttribute> LocalizationAttributes = null;
        public List<LocParameter> GetAttributes()
        {
            var CurrentAttributes = new List<LocParameter>();

            if (LocalizationAttributes != null)
                foreach (var a in LocalizationAttributes)
                    CurrentAttributes.Add(new LocParameter() { KeyName = a.Key, Value = (float)a.formula.Calc(0.0, container, (double)value, (double)originTime) });

            return CurrentAttributes;
        }


        internal virtual StatInfluence Clone()
        {
            return this.Copy();
        }

        [XmlIgnore]
        public StatContainer container;
        [XmlIgnore]
        public bool bRemove = false;

        [XmlIgnore]
        public MapTargetObject attacker;

        [DefaultValueAttribute(false)]
        [TextField(false)]
        [XmlAttribute]
        public bool AstroTime = false;

        [TextField(false)]
        [XmlAttribute]
        public bool HideTimer = false;

        //Сколько прошло
        [XmlIgnore]
        public float Elapsed
        {
            get { return Time.time - startTime; }
        }

        private MapEffect clientEffect;

        //TODO fil подумать вынести в Контейнер
        public void Init(StatContainer container, InfluenceSaveData saveData)
        {
            value = saveData.value;

            var now = DateTime.UtcNow;
            if (!AstroTime)
            {
                time = saveData.Time;
            }
            else
            {
                time = (float)(saveData.FinishTime - now).TotalSeconds;
            }


            if (time < 0)
                return;

            container.AddInfluence(this);
        }

        public InfluenceSaveData GetSaveData()
        {
            var savedata = new InfluenceSaveData();
            savedata.Id = Id;
            savedata.value = value;
            if (!AstroTime)
            {
                savedata.Time = finishTime - Time.time;
            }
            else
            {
                var now = DateTime.UtcNow;
                savedata.FinishTime = now.AddSeconds(finishTime - Time.time);
            }
            return savedata;
        }

        public StatInfluence()
        {
        }

        public StatInfluence(StatContainer container, string Group, float time, Stats stat, PlayerStatus Icon, double value)
		{
			this.time = time;
			this.originTime = time;
			//this.stat = stat;
			this.value = value;
			this.container = container;
            this.Group = Group;
            this.Icon = Icon;
            //когда создаем инфлуенс через конструктор, то не надо его возвращать в пул. В пулл кладем только взятые из пула
            NotBackPool = true;
        }

		public void SetFinishTime(float finishTime)
		{
			this.finishTime = finishTime;
			startTime = finishTime - time;
			//time = finishTime - startTime;

            if (container.personOwner != null && !HideTimer && m_Enable && Icon != PlayerStatus.None)
                container.bInfluencesChanged = true;
		}

		public bool CheckGroup(StatInfluence infl)
		{
			return Group == infl.Group && SubGroup == infl.SubGroup;
		}

        public bool Add()
        {
            //иммунитет к определенному стату
            if (ConflictStat != Stats.None && container.GetStatValue(ConflictStat) > 0)
                return false;

            addTime = Time.time;
            startTime = Time.time;
            finishTime = startTime + time;

            if (Group != null)
            {
                StatInfluence replaceInfl = null;
                if (MaxStack == 1)
                {
                    replaceInfl = container.Influences.Find(x => !x.bRemove && CheckGroup(x));
                }
                else if (RefreshTimeWhenAdd)
                {
                    var allInfl = container.Influences.FindAll(x => !x.bRemove && CheckGroup(x));
                    foreach (var item in allInfl)
                    {
                        item.SetFinishTime(Time.time + time);
                    }

                    return allInfl != null && allInfl.Count < MaxStack;
                }
                else
                {

                    List<StatInfluence> oldInfls = container.Influences.FindAll(x => !x.bRemove && CheckGroup(x));
                    //Последний наложенный эффект всегда заменяет действующий с наименьшим таймером.
                    if (oldInfls != null && oldInfls.Count >= MaxStack)
                    {
                        float minFinish = float.MaxValue;
                        foreach (var inf in oldInfls)
                        {
                            if (inf.finishTime < minFinish)
                            {
                                minFinish = inf.finishTime;
                                replaceInfl = inf;
                            }
                        }
                    }

                }
                if (replaceInfl != null && (replaceInfl.value != value || replaceInfl.Id != Id))
                {
                    container.RemoveInfluence(replaceInfl);
                    replaceInfl = null;
                }

                if (replaceInfl != null)
                {
                    if (replaceInfl.finishTime < finishTime)
                        replaceInfl.SetFinishTime(Time.time + time);

                    return false;
                }
            }
            //Sos.Debug($"вешаем инфлуенс Id={Id} stat={stat}");
            //Apply();
            return true;
        }

		public virtual void Remove()
		{
            SetEnable(false);
        }

        public FormulaCondition EnableCondition;
        public virtual void Apply()
        {
            var owner = container.GetOwner();
            if (EnableCondition == null || EnableCondition.Check(owner.map, owner, null))
                SetEnable(true);
        }

        public bool Enable
        {
            get
            {
                return m_Enable;
            }
        }

        private bool m_Enable = false;
        public void SetEnable(bool enable)
        {
            
            if (!m_Enable && enable)
            {                
                container.bInfluencesChanged = true;
                SendEffectAdd();
            }

            if (m_Enable && !enable)
            {                
                container.bInfluencesChanged = true;
                SendEffectRemove();
            }

            m_Enable = enable;
        }

        public void SendEffectAdd()
        {
            if (FxId == -1)
                return;

            //уже отсылали. не будем второй раз слать
            if (clientEffect != null)
                return;

            MapTargetObject owner = container.GetOwner();

            if (owner == null)
                return;

            if (AlternativeFxId != -1)
            {
                TeamMapEffect teamEffect = new TeamMapEffect(owner.map) { fxId = (EffectType)FxId, position = owner.position + new Vector3(0, owner.Height + 0.5f, 0), scale = FxScale };
                teamEffect.sourceTeam = TeamColor;
                teamEffect.alternativefxId = (EffectType)AlternativeFxId;
                clientEffect = teamEffect;
                clientEffect.ownerId = owner.Id;
                ServerRpcListener.Instance.SendCreateFxResponse(teamEffect);
            }
            else
            {
                clientEffect = new MapEffect(owner.map) { fxId = (EffectType)FxId, position = owner.position + new Vector3(0, owner.Height + 0.5f, 0), scale = FxScale };
                ServerRpcListener.Instance.SendCreateFxResponse(owner, clientEffect);
            }
        }

        public void SendEffect(InstancePerson person)
        {
            if (clientEffect == null)
                return;

            MapTargetObject owner = container.GetOwner();
            if (owner == null || person.syn == null)
            {
                ILogger.Instance.Send($"SendEffect() Commander was null, loginId = {person.loginId}, personId = {person.personId}, person.syn was null = {person.syn == null}, ", ErrorLevel.warning);
                return;
            }
            ServerRpcListener.Instance.CreateFxResponse_Regular(person, clientEffect.id, owner.Id, owner.eType, clientEffect.GetFxId(person.syn), owner.position, clientEffect.upgrade, clientEffect.scale);
        }

        public void SendEffectRemove()
        {
            
            if (FxId == -1)
                return;

            if (clientEffect == null)
                return;



            MapTargetObject owner = container.GetOwner();

            if (owner == null)
            {
                Sos.Error("SendEffectRemove owner == null");
                return;
            }

            ServerRpcListener.Instance.DestroyFxResponse(clientEffect);
            clientEffect = null;
        }

        public virtual void Update()
		{
		    if (Time.time > finishTime || OnAttackerDeathRemove && (attacker == null || attacker.IsDead))
		    {
                container.RemoveInfluence(this);
            }
        }

        public virtual bool ContainsStat(Stats type)
        {
            return false;
        }
        public virtual double GetStatValue(Stats type)
        {
            throw new NotImplementedException($"InflId = {Id}");
        }
    }
}
