﻿using System;
using UtilsLib.Logic.Enums;
using System.Xml.Serialization;
using UtilsLib.BatiyScript;
using UtilsLib.DegSerializers;

namespace Assets.Scripts.InstanceServer.Entries.Influences
{
	public class InnerStatInfluence: ICanCopy
	{
        [XmlAttribute]
        public Stats stat { get; set; }

        [XmlAttribute]
        public string value
        {
            get { return fvalue.Text; }
            set { fvalue = new FormulaX("p value", value); }
        }

        [XmlIgnore]
        public FormulaX fvalue = new FormulaX("p value", "0");

        [XmlIgnore]
        public StatContainer container
        {
            get { return parentInfluence.container; }
        }

        [XmlIgnore]
        public StatInfluence parentInfluence;

        public InnerStatInfluence()
        {
        }

        public virtual void Restore()
        {
            if (m_Enable)
                throw new Exception("InnerStatInfluence in use");

            m_Enable = false;
        }

        protected bool m_Enable = false;
        [XmlIgnore]
        public double appliedValue { protected set; get; }
        public virtual void SetEnable(bool enable, double value)
        {
            if (!m_Enable && enable)
            {
                appliedValue = fvalue.Calc(container, value);
                container.AddStat(stat, appliedValue);
            }

            if (m_Enable && !enable)
            {
                container.RemoveStat(stat, appliedValue);
            }

            m_Enable = enable;
        }

        public virtual void ParentUpdate()
        {
        }
    }
}
