﻿using System;
using System.Collections.Generic;
using System.Data;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.Networking.ClientDataRPC;

namespace Assets.Scripts.InstanceServer
{
    public class EmotionPerson
    {
        public List<Emotion> AvailableEmotions { get; private set; }
        public List<Emotion> AvailableEmotionsForBuy { get; private set; }
        public Emotion InUse { get; private set; }
        public DateTime UseDuration { get; private set; }
        public DateTime RefreshEmotionsAfter { get; private set; }
        public EmotionPerson()
        {
            AvailableEmotions = new List<Emotion>();
            AvailableEmotionsForBuy = new List<Emotion>();
        }
        
        public void Load(List<Emotion> availableEmotions, List<Emotion> forBuy)
        {
            AvailableEmotions = availableEmotions;
            AvailableEmotionsForBuy = forBuy;
        }

        public void SetLastRefreshTime(DateTime time)
        {
            RefreshEmotionsAfter = time; //timeProvider.TimeUTCNow.AddHours(RefreshTimeHours);
        }

        public void UpdateEmotionsForBuy(List<Emotion> emotions)
        {
            AvailableEmotionsForBuy.Clear();
            AvailableEmotionsForBuy = emotions;
        }

        public void UpdateUseEmotion(Emotion useEmotion, DateTime duration)
        {
            InUse = useEmotion;
            UseDuration = duration;
        }

		/// <summary>
		/// обавляет эмотикон в слот
		/// </summary>		
		/// <param name="id"></param>
		/// <param name="slotNumber"></param>
		/// <returns></returns>
		public bool PutInSlot(int id, int slotNumber)
		{
			if (slotNumber < 0)
			{
				ILogger.Instance.Send("PutInSlot nslotNumber < 0", ErrorLevel.error);
				return false;
			}

			var avalibleEmotion = AvailableEmotions.Find(e => e.Id == id);
			var oldavalibleEmotion = AvailableEmotions.Find(e => e.SlotId == slotNumber);
			if (oldavalibleEmotion != null)
			{
				oldavalibleEmotion.UpdateData(-1);
			}

			if (avalibleEmotion != null)
			{
				avalibleEmotion.UpdateData(slotNumber);
				return true;
			}
			else
			{
				ILogger.Instance.Send("PutInSlot emotion = null id: " + id, ErrorLevel.error);
				return false;
			}
		}

		public EmotionData GetEmotionData(int id)
        {
            var emotion = AvailableEmotions.Find(e => e.Id == id);
            if (emotion != null)
            {
                return (EmotionData)emotion;
            }
            return null;
        }

		public bool InAvailableEmotionsList(int baseId)
		{
			return AvailableEmotions.Exists(e => e.Id == baseId);
		}

		public bool InAvailableEmotionsForBuyList(int baseId)
		{
			return AvailableEmotionsForBuy.Exists(e => e.Id == baseId);
		}

		public void ResetInUse()
        {
            InUse = null;
        }
    }
}
