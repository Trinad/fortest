﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using UtilsLib.DegSerializers;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using LobbyInstanceLib.Networking;
using UtilsLib.ExternalLogs.Classes;
using UtilsLib.ExternalLogs.Enums;
using Assets.Scripts.InstanceServer.Analytic;
using Assets.Scripts.InstanceServer.Entries.Login;
using UtilsLib.Logic;

namespace Assets.Scripts.InstanceServer
{
	public class ShopManager
	{
		public List<Prices> PricePoints { get; private set; }
		public List<ShopItemData> ShopItems { get; private set; }
		IShopSender shopSender;
		public bool ShopOff { get; set; }

		public ShopManager(IShopSender shopSender)
		{
			this.shopSender = shopSender;
		}

		public void SetPricePoints(List<Prices> pricePoints)
		{
			ILogger.Instance.Send($"ShopManager.SetPricePoints: сount = {pricePoints?.Count}");

			PricePoints = pricePoints;

			if (PricePoints == null || PricePoints.Count == 0)
			{
				ILogger.Instance.Send("ShopManager SetPricePoints Shop is off", ErrorLevel.error);
				ShopOff = true;
			}
			else
			{
				ILogger.Instance.Send("ShopManager SetPricePoints Shop is On", ErrorLevel.info);
				ShopOff = false;
			}
		}

		public void SetShopItems(List<ShopItemData> shopItems)
		{
			ShopItems = shopItems;
		}

		public PricePoint GetPricePointById(InstancePerson instancePerson, int pricePointId)
		{
			if (ShopOff)
				return null;
			var prices = PricePoints.FirstOrDefault(x => x.priceType == instancePerson.login.PaymentPlatform) ?? PricePoints.First(x => x.priceType == "base");
			var prices2 = prices.PaymentPointGroups.FirstOrDefault(x => x.Currency == instancePerson.login.PaymentCurrency) ?? prices.PaymentPointGroups.First(x => x.Currency == prices.defaultCurrency);
			var pricePoint = prices2.PricePoints.FirstOrDefault(x => x.Id == pricePointId);
			return pricePoint;
		}

		public PricePoint GetEmeraldPricePoint(InstancePerson instancePerson, int extId)
		{
			if (ShopOff)
				return null;

			var prices = PricePoints.FirstOrDefault(x => x.priceType == instancePerson.login.PaymentPlatform) ?? PricePoints.First(x => x.priceType == "base");
			var prices2 = prices.PaymentPointGroups.FirstOrDefault(x => x.Currency == instancePerson.login.PaymentCurrency) ?? prices.PaymentPointGroups.First(x => x.Currency == prices.defaultCurrency);
			var pricePoint = prices2.PricePoints.FirstOrDefault(x => x.TotalSumm == extId && x.Item == PriceItem.Emerald);
			return pricePoint;
		}

		public PricePoint GetGoldPricePoint(InstancePerson instancePerson, int extId)
		{
			if (ShopOff)
				return null;

			var prices = PricePoints.FirstOrDefault(x => x.priceType == instancePerson.login.PaymentPlatform) ?? PricePoints.First(x => x.priceType == "base");
			var prices2 = prices.PaymentPointGroups.FirstOrDefault(x => x.Currency == instancePerson.login.PaymentCurrency) ?? prices.PaymentPointGroups.First(x => x.Currency == prices.defaultCurrency);
			var pricePoint = prices2.PricePoints.FirstOrDefault(x => x.TotalSumm == extId && x.Item == PriceItem.Gold);
			return pricePoint;
		}

		public PricePoint GetSpecialOfferPricePoint(InstancePerson instancePerson, int extId)
		{
			if (ShopOff)
				return null;

			var prices = PricePoints.FirstOrDefault(x => x.priceType == instancePerson.login.PaymentPlatform) ?? PricePoints.First(x => x.priceType == "base");
			var prices2 = prices.PaymentPointGroups.FirstOrDefault(x => x.Currency == instancePerson.login.PaymentCurrency) ?? prices.PaymentPointGroups.First(x => x.Currency == prices.defaultCurrency);
			var pricePoint = prices2.PricePoints.FirstOrDefault(x => x.TotalSumm == extId && x.Item == PriceItem.SpecialOffer);
			return pricePoint;
		}

		public PricePoint GetPricePoint(InstancePerson instancePerson, Func<PricePoint, bool> checker)
		{
			if (ShopOff)
				return null;

			var prices = PricePoints.FirstOrDefault(x => x.priceType == instancePerson.login.PaymentPlatform) ?? PricePoints.First(x => x.priceType == "base");
			var prices2 = prices.PaymentPointGroups.FirstOrDefault(x => x.Currency == instancePerson.login.PaymentCurrency) ?? prices.PaymentPointGroups.First(x => x.Currency == prices.defaultCurrency);
			var pricePoint = prices2.PricePoints.FirstOrDefault(checker);
			return pricePoint;
		}

		public List<ProductItemData> GetShopItems(LobbyLogin login, int filterId)
		{
			if (ShopOff)
				return null;

			var prices = PricePoints.FirstOrDefault(x => x.priceType == login.PaymentPlatform) ?? PricePoints.First(x => x.priceType == "base");
			var prices2 = prices.PaymentPointGroups.FirstOrDefault(x => x.Currency == login.PaymentCurrency) ?? prices.PaymentPointGroups.First(x => x.Currency == prices.defaultCurrency);

			var shopItem = ShopItems.FirstOrDefault(x => x.FilterId == filterId);
			if (shopItem == null)
				return null;

			var result = new List<ProductItemData>();
			foreach (var p in shopItem.ProductItemDatas)
			{
				if (!p.Check(prices.priceType, prices2.Currency))
					continue;

				if (p.InApp)
				{
					var pricePoint = prices2.PricePoints.FirstOrDefault(x => x.Id == p.BaseId);
					if (pricePoint == null)
					{
						ILogger.Instance.Send($"GetShopItems: не найден pricePoint {login.PaymentPlatform} {login.PaymentCurrency} {p.BaseId}", ErrorLevel.error);
						continue;
					}

					//TODO_deg Для каждого логина перетирается инфа о стоимости внутриигровых покупок
					p.Sku = pricePoint.ExternPoint;
					p.Price = (float)Convert.ToDouble(pricePoint.Price, CultureInfo.InvariantCulture); //Клиент цену через sku узнает
					p.Count = pricePoint.TotalSumm;
					p.Bonus = pricePoint.Bonus;
				}

				result.Add(p);
			}

			return result;
		}


		public void LoginAccountChangeResponse(InstancePerson instancePerson, AccountChangeParams data, AnalyticCurrencyInfo analyticInfo)
		{
			if (ShopOff)
				return;

			ILogger.Instance.Send($"LoginAccountChangeResponse {data.loginId}, json = { JSON.JsonEncode(data.JsonEncode())}", ErrorLevel.info);

			if (data.success)
			{
				var oldEmerald = instancePerson.Emeralds;

				instancePerson.Wallet.Set(ResourceType.Emerald, data.lAfterUpdate, analyticInfo);
				if (data.reason == AccountChangeReason.AccountRequest)
				{
					GlobalEvents.Instance.OnLoginEmeraldsActualised(instancePerson);
				}
				else
					instancePerson.OnWalletResourceChange(ResourceType.Emerald, data.lAfterUpdate - oldEmerald);

				ServerRpcListener.Instance.SendCurrencyResponse(instancePerson);
			}

			if (!data.fromInst || data.tranId == -1)
				return;

			if (Transactions.TryGetValue(data.tranId, out var action) && action != null)
			{
				try
				{
					action(data);
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send($"LoginAccountChangeResponse: {ex}", ErrorLevel.exception);
				}

				if (!instancePerson.bOnLine)
					ILogger.Instance.Send("LoginAccountChangeResponse: игрок в офлайне! data=" + JSON.JsonEncode(data.JsonEncode()), ErrorLevel.warning);

				Transactions.Remove(data.tranId);

				if (!CanRepeatEvent(data.reason))
					ResetLastActionDate(data.reason);
			}
			else
			{
				ILogger.Instance.Send("LoginAccountChangeResponse: не найдена транзакция! data=" + JSON.JsonEncode(data.JsonEncode()), ErrorLevel.error);
			}
		}

		public void Buy(InstancePerson instancePerson, int Cost, ResourceType CostType, AccountChangeReason Reason, AnalyticCurrencyInfo analyticInfo, Action success)
		{
			if (Cost <= 0)
			{
				ILogger.Instance.Send($"ShopManager.Buy: Нельзя покупать на {Cost} {CostType} {Reason}", ErrorLevel.error);
				return;
			}

			if (CostType != ResourceType.Emerald)
			{
				if (!instancePerson.AddResource(CostType, -Cost, analyticInfo: analyticInfo))
				{
					var resData = instancePerson.GetNeedForResource();
					resData.NeedResource(CostType, Cost - instancePerson.CountResource(CostType));
					shopSender.ErrorDataResponse(instancePerson, resData.GetError(instancePerson));
					return;
				}

				success();
			}
			else if (CostType == ResourceType.Emerald)
			{
				if (ShopOff)
					return;

				BuyForEmeralds(instancePerson, Cost, Reason, data =>
				{
					if (!data.success)
					{
						var resData = instancePerson.GetNeedForResource();
						resData.NeedResource(ResourceType.Emerald, Cost - instancePerson.Emeralds);
						shopSender.ErrorDataResponse(instancePerson, resData.GetError(instancePerson));
						return;
					}

					success();
				});
			}
			else
			{
				ILogger.Instance.Send($"ShopManager.Buy: Не смогли купить {Cost} {CostType} {Reason}", ErrorLevel.error);
			}
		}

		public void BuyShopItemRequest(InstancePerson instancePerson, int itemId)
		{
			if (ShopOff)
				return;

			var productDatas = GetShopItems(instancePerson.login, 0);
			var product = productDatas.Find(x => x.BaseId == itemId && !x.InApp);
			if (product == null)
			{
				ILogger.Instance.Send($"BuyShopItemRequest: не найден товар! itemId={itemId}", ErrorLevel.error);
				return;
			}

			BuyForEmeralds(instancePerson, (int)product.Price, AccountChangeReason.BuyShopItem, data =>
			{
				if (data.success)
				{
					var buyItem = instancePerson.gameXmlData.ItemDataRepository.GetCopyInventoryItemData(product.ItemName, product.Count);
					//выдать предмет

					AnalyticCurrencyInfo analyticInfo = new AnalyticCurrencyInfo()
					{
						needSendInWallet = false,
						sendGain = true,
						Amount = buyItem.Count,
						Currency = ResourceType.Gold,
						USDCost = 0f,
						Source = SourceGain.Exchange,
						SourceType = ResourceType.Emerald.ToString(),
						Destination = SinkType.Exchange.ToString(),
						DestinationType = ResourceType.Gold.ToString(),
						PersonLevel = instancePerson.login.GetMaxPersonLevel(),
						TestPurchase = instancePerson.login.settings.testPurchase,
						transactionId = AnalyticsManager.GetSHA256(DateTime.UtcNow.ToString() + instancePerson.loginId.ToString())
					};

					instancePerson.AddItem(buyItem, analyticInfo, ItemSourceType.BoughtForEmeralds);

					switch (buyItem.Resource)
					{
						case ResourceType.Gold:
							//var gold = instancePerson.gameXmlData.ItemDataRepository.GetCopyInventoryItemData(ResourceType.Gold, product.Count);
							AnalyticsManager.Instance.SendData(instancePerson, analyticInfo);
							analyticInfo.sendGain = false;
							analyticInfo.Currency = ResourceType.Emerald;
							analyticInfo.Amount = product.Price;
							AnalyticsManager.Instance.SendData(instancePerson, analyticInfo);

							//AnalyticsManager.Instance.AccuralInnerCurrencyEvent(instancePerson, ResourceType.Gold, SourceType.shopdonate, product.Count);
							break;
							//case ShopItemIcon.UniversalResource0:
							//    AnalyticsManager.Instance.BuyItemAnalytics(instancePerson, new AnalyticsData(AnalyticsEventType.BuyHandfulOfUniversalResource, product.Count));
							//    break;
							//case ShopItemIcon.UniversalResource1:
							//    AnalyticsManager.Instance.BuyItemAnalytics(instancePerson, new AnalyticsData(AnalyticsEventType.BuyCupOfUniversalResource, product.Count));
							//    break;
							//case ShopItemIcon.UniversalResource2:
							//    AnalyticsManager.Instance.BuyItemAnalytics(instancePerson, new AnalyticsData(AnalyticsEventType.BuySackOfUniversalResource, product.Count));
							//    break;
							//case ShopItemIcon.UniversalResource3:
							//    AnalyticsManager.Instance.BuyItemAnalytics(instancePerson, new AnalyticsData(AnalyticsEventType.BuyChestOfUniversalResource, product.Count));
							//    break;
							//case ShopItemIcon.UniversalResource4:
							//    AnalyticsManager.Instance.BuyItemAnalytics(instancePerson, new AnalyticsData(AnalyticsEventType.BuySafeboxOfUniversalResource, product.Count));
							//    break;
					}

					shopSender.SendBuyItemsResponse(instancePerson, BuyItemResponseType.ShopItem, new List<InventoryItemData>() { buyItem });
				}
				else
				{
					var resData = instancePerson.GetNeedForResource();
					resData.NeedResource(ResourceType.Emerald, (int)product.Price - instancePerson.Emeralds);
					shopSender.ErrorDataResponse(instancePerson, resData.GetError(instancePerson));
				}
			});
		}

		public void SendBuyItemsResponse(InstancePerson instancePerson, BuyItemResponseType responceType, List<InventoryItemData> rewardItems, int actionType = -1, int vipPoints = 0)
		{
			shopSender.SendBuyItemsResponse(instancePerson, responceType, rewardItems, actionType, vipPoints);
		}

		public int TransactionId;

		public int GetTransactionId(Action<AccountChangeParams> action)
		{
			if (action == null)
				return -1;

			var tranId = TransactionId++;
			Transactions[tranId] = action;
			return tranId;
		}

		public Dictionary<int, Action<AccountChangeParams>> Transactions = new Dictionary<int, Action<AccountChangeParams>>();
		public Dictionary<AccountChangeReason, DateTime> LastActionDate = new Dictionary<AccountChangeReason, DateTime>();

		public bool CanRepeatEvent(AccountChangeReason reason)
		{
			return reason == AccountChangeReason.BuyItemFromNPC || reason == AccountChangeReason.BuyShopItem;
		}

		private DateTime getLastActionDate(AccountChangeReason reason)
		{
			if (LastActionDate.ContainsKey(reason))
				return LastActionDate[reason];
			return DateTime.MinValue;
		}

		public void ResetLastActionDate(AccountChangeReason reason)
		{
			LastActionDate[reason] = DateTime.MinValue;
		}

		internal void GetTotalAccountSumm(InstancePerson instancePerson, Action<AccountChangeParams> action = null)
		{
			var data = new AccountChangeParams();
			data.instTicket = ServerStartGameManager.ticket;
			data.tranId = GetTransactionId(action);
			data.loginId = instancePerson.loginId;
			data.reason = AccountChangeReason.AccountRequest;
			data.summ = 0;
			data.itemId = -1;
			data.source = GetCurrencySource.None;
			data.fromInst = false;
			data.buyParams = string.Empty;

			shopSender.LoginAccountChangeRequest(data);
		}

		public void BuyForEmeralds(InstancePerson instancePerson, int price, AccountChangeReason reason, Action<AccountChangeParams> action)
		{
			if (ShopOff)
				return;

			//можно ли повторить запрос если такой уже есть...
			if (!CanRepeatEvent(reason))
			{
				DateTime last = getLastActionDate(reason);
				if (last != DateTime.MinValue && last.AddSeconds(ServerConstants.Instance.BuyForEmeraldTimeout) > TimeProvider.UTCNow)
					return;
			}

			ILogger.Instance.Send($"BuyForEmeralds().. loginId = {instancePerson.loginId } price = {price}  ", ErrorLevel.info);

			var data = new AccountChangeParams();
			data.instTicket = ServerStartGameManager.ticket;
			data.tranId = GetTransactionId(action);
			data.loginId = instancePerson.loginId;
			data.summ = price;
			data.itemId = -1;
			data.reason = reason;
			data.source = GetCurrencySource.None;
			data.fromInst = true;
			data.buyParams = string.Empty;
			// если мы делаем что то бесплатно например в туторе, не стучаться к платежке, иначе блочит тутор если плтежка не доступна
			if (price == 0 && reason != AccountChangeReason.AccountRequest)
			{
				data.success = true;
				action(data);
				return;
			}

			//ServerRpcListener.Instance.LoginAccountChangeRequest(data);

			shopSender.LoginAccountChangeRequest(data);
		}

		public void GetProductItemsRequest(InstancePerson instancePerson)
		{
			if (ShopOff)
			{
				ILogger.Instance.Send("GetProductItemsRequest FAILED ShopOff", ErrorLevel.exception);
				return;
			}

			var products = GetProductItemDatas(instancePerson);
			var data = new ShopData(instancePerson, products);
			shopSender.GetProductItemsResponse(instancePerson, data);
		}

		public List<ProductItemData> GetProductItemDatas(InstancePerson instancePerson)
		{
			if (ShopOff)
			{
				ILogger.Instance.Send("GetProductItemDatas FAILED ShopOff list count = 0", ErrorLevel.exception);
				return new List<ProductItemData>();
			}

			var data = GetShopItems(instancePerson.login, 0);

			ILogger.Instance.Send($"GetProductItemDatas Success data.Count: {data.Count}", ErrorLevel.info);
			return data;
		}

		//public void GetShopItemsRequest(InstancePerson instancePerson, string pointKey, string pointCurrency)
		//{
		//    if (ShopOff)
		//        return;

		//    instancePerson.PointKey = pointKey;
		//    instancePerson.PointCurrency = pointCurrency;

		//    bool sale = false; //Акция //Распродажа //TODO_Deg вынести куда надо
		//    shopSender.GetShopItemsResponse(instancePerson.player, sale, GetShopItems(instancePerson));
		//}
		public void EmeraldBuyCheat(InstancePerson instancePerson, int summ)
		{
			if (ShopOff)
				return;

			EmeraldCheat(instancePerson, summ);

			var data = new AccountChangeParams();
			data.instTicket = ServerStartGameManager.ticket;
			data.tranId = -1;
			data.loginId = instancePerson.loginId;
			data.summ = summ;
			data.itemId = -1;
			data.reason = AccountChangeReason.RealPayment;
			data.source = GetCurrencySource.Cheat;
			data.fromInst = true;
			data.buyParams = string.Empty;
			shopSender.LoginAccountChangeRequest(data);
		}

		public void EmeraldCheat(InstancePerson instancePerson, int summ, Action<AccountChangeParams> action = null)
		{
			if (ShopOff)
				return;

			var data = new AccountChangeParams();
			data.instTicket = ServerStartGameManager.ticket;
			data.tranId = GetTransactionId(action);
			data.loginId = instancePerson.loginId;
			data.summ = -summ; // Чтобы добавились
			data.itemId = -1;
			data.reason = AccountChangeReason.GameReward;
			data.source = GetCurrencySource.Cheat;
			data.fromInst = true;
			data.buyParams = string.Empty;

			shopSender.LoginAccountChangeRequest(data);
		}


		public void GoldBuyCheat(InstancePerson instancePerson, int summ)
		{
			if (ShopOff)
				return;

			var data = new AccountChangeParams();
			data.instTicket = ServerStartGameManager.ticket;
			data.tranId = -1;
			data.loginId = instancePerson.loginId;
			data.summ = summ;
			data.itemId = -1;
			data.reason = AccountChangeReason.Gold;
			data.source = GetCurrencySource.Cheat;
			data.fromInst = true;
			data.buyParams = string.Empty;

			shopSender.LoginAccountChangeRequest(data);
		}

		public void BuySpecialOfferCheat(InstancePerson instancePerson, AccountChangeReason reason, int summ)
		{
			if (ShopOff)
				return;

			var data = new AccountChangeParams();
			data.tranId = -1;
			data.instTicket = ServerStartGameManager.ticket;
			data.loginId = instancePerson.loginId;
			data.summ = summ;
			data.itemId = -1;
			data.reason = reason;
			data.source = GetCurrencySource.Cheat;
			data.fromInst = true;
			data.buyParams = string.Empty;
			shopSender.LoginAccountChangeRequest(data);
		}

		public Dictionary<string, int> GetPointIds(InstancePerson instancePerson)
		{
			var prices = PricePoints.FirstOrDefault(x => x.priceType == instancePerson.login.PaymentPlatform) ?? PricePoints.First(x => x.priceType == "base");
			var prices2 = prices.PaymentPointGroups.FirstOrDefault(x => x.Currency == instancePerson.login.PaymentCurrency) ?? prices.PaymentPointGroups.First(x => x.Currency == prices.defaultCurrency);
			var skuData = prices2.PricePoints.ToDictionary(
				x => x.ExternPoint ?? "",
				x => prices.priceId * 256 * 256 + prices2.Mask * 256 + x.Id
			);
			ILogger.Instance.Send($"===========ProductListRequest ProductListResponse instancePerson: {instancePerson.login.loginId} name: {instancePerson.login.loginName} priceType: {prices.priceType} skuDataCount: {skuData.Count}");
			return skuData;
		}

		public void ProductListRequest(InstancePerson instancePerson)
		{
			var skuData = GetPointIds(instancePerson);
			shopSender.ProductListResponse(instancePerson, skuData);
		}
	}
}
