﻿
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;

namespace Assets.Scripts.InstanceServer.Algorithms
{
    public class PositionSortedList<T>:IEnumerable<T> 
        where T: MapTargetObject
    {

        public event Action<T> OnAddAction;
        public PositionSortedList()
        {
        }

        private float MaxElementRadius = 0;
        private List<T> list = new List<T>();
        private List<T> sortedList = new List<T>();
        private List<T> listForAdd = new List<T>();
        private List<T> listForRemove = new List<T>();

        /// <summary>
        /// Добаывляет элемент в конец списка. Элемент будет доступен после вызова Refresh()
        /// </summary>
        /// <param name="item"></param>
        public void AddSoft(T item)
        {
            listForAdd.Add(item);
        }

        /// <summary>
        /// Добаывляет элемент в конец списка. Элемент будет доступен сразу
        /// </summary>
        /// <param name="item"></param>
        public void Add(T item)
        {
            list.Add(item);
            sortedList.Add(item);
            if (MaxElementRadius < item.Radius)
                MaxElementRadius = item.Radius;

            OnAddAction?.Invoke(item);
        }

        public void AddRange(IEnumerable<T> collection)
        {
            foreach (var item in collection)
                Add(item);
        }

        /// <summary>
        /// Удаляет элемент из списка. Элемент будет удален после вызова Refresh()
        /// </summary>
        /// <param name="item"></param>
        public void RemoveSoft(T item)
        {
            listForRemove.Add(item);
        }

        /// <summary>
        /// Удаляет элемент из списка. Элемент будет удален сразу
        /// </summary>
        /// <param name="item"></param>
        public void Remove(T item)
        {
            list.Remove(item);
            sortedList.Remove(item);
        }

        public void RemoveAt(int i)
        {
            T item = list[i];
            sortedList.Remove(item);
            list.RemoveAt(i);
        }

        public void Refresh()
        {
            if (listForRemove.Count != 0)
            {
                foreach (var element in listForRemove)
                    Remove(element);

                listForRemove.Clear();
            }

            if (listForAdd.Count != 0)
            {
                AddRange(listForAdd);
                listForAdd.Clear();
            }

            try
            {
                sortedList.Sort((x, y) => Math.Sign(x.position.X - y.position.X));
            }
            catch (ArithmeticException)
            {
                FixNaNs();
            }
        }

        public void FixNaNs()
        {
            foreach (var obj in sortedList)
            {
                if (float.IsNaN(obj.position.X) || float.IsNaN(obj.position.Y) || float.IsNaN(obj.position.Z))
                {
                    ILogger.Instance.Send($"Объект {obj} имеет координаты {obj.position}. Переставлен в точку (0,0,0)");
                    obj.position = Vector3.Zero;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        public IEnumerable<T> GetXNearest(float x, float maxDeltaX, bool useMaxElementRadius = true)
        {
            if (sortedList.Count == 0)
                yield break;

            int index = BinarySearchIndex(x, 0, sortedList.Count - 1);

            if (useMaxElementRadius)
                maxDeltaX += MaxElementRadius;

            for (int i = index + 1; i < sortedList.Count && sortedList[i].position.X <= x + maxDeltaX; i++)
            {
                yield return sortedList[i];
            }

            for (int i = index; i >= 0 && sortedList[i].position.X >= x - maxDeltaX; i--)
            {
                yield return sortedList[i];
            }
        }

        private int BinarySearchIndex(float x, int minindex, int maxIndex)
        {
            if (maxIndex - minindex <= 1)
                return minindex;

            int indexDiv = (minindex + maxIndex) / 2;

            if (sortedList[indexDiv].position.X > x)
                return BinarySearchIndex(x, minindex, indexDiv);
            else
                return BinarySearchIndex(x, indexDiv, maxIndex);
        }

        public T this[int index]
        {
            get { return list[index]; }
            set { list[index] = value; }
        }

        public List<T> FindAll(Predicate<T> match)
        {
            return list.FindAll(match);
        }

        public T Find(Predicate<T> match)
        {
            return list.Find(match);
        }

        public bool Exists(Predicate<T> match)
        {
            return list.Exists(match);
        }

        public int Count
        {
            get { return list.Count; }
        }

        public void Clear()
        {
            list.Clear();
            sortedList.Clear();
            listForAdd.Clear();
            listForRemove.Clear();
        }

        public void ForEach(Action<T> action)
        {
            list.ForEach(action);
        }

        public bool Contains(T element)
        {
            return list.Contains(element) || listForAdd.Contains(element);
        }
    }
}
