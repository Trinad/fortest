
using System;
using System.Numerics;

namespace Assets.Scripts.InstanceServer.Algorithms
{
    /// <summary>
    /// A integer 3D vector class
    /// </summary>
    [System.Serializable]
    public class Vector3i
    {
        public int x;
        public int y;
        public int z;

        public Vector3i()
        {
        }

        public Vector3i(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3i(Vector3 v)
        {
            x = (int)v.X;
            y = (int)v.Y;
            z = (int)v.Z;
        }

        public static Vector3i zero
        {
            get
            {
                return new Vector3i(0, 0, 0);
            }
        }

        public static Vector3i one
        {
            get
            {
                return new Vector3i(1, 1, 1);
            }
        }

        public static Vector3i left
        {
            get
            {
                return new Vector3i(-1, 0, 0);
            }
        }

        public static Vector3i right
        {
            get
            {
                return new Vector3i(1, 0, 0);
            }
        }

        public static Vector3i up
        {
            get
            {
                return new Vector3i(0, 1, 0);
            }
        }

        public static Vector3i down
        {
            get
            {
                return new Vector3i(0, -1, 0);
            }
        }

        public static Vector3i forward
        {
            get
            {
                return new Vector3i(0, 0, 1);
            }
        }

        public static Vector3i back
        {
            get
            {
                return new Vector3i(0, 0, -1);
            }
        }

        public static Vector3i operator +(Vector3i c1, Vector3i c2)
        {
            return new Vector3i(c1.x + c2.x, c1.y + c2.y, c1.z + c2.z);
        }

        public static Vector3i operator -(Vector3i c1, Vector3i c2)
        {
            return new Vector3i(c1.x - c2.x, c1.y - c2.y, c1.z - c2.z);
        }

        public static Vector3i operator *(Vector3i c1, int c2)
        {
            return new Vector3i(c1.x * c2, c1.y * c2, c1.z * c2);
        }

        public static Vector3 operator *(Vector3i c1, float c2)
        {
            return new Vector3(c1.x * c2, c1.y * c2, c1.z * c2);
        }

        public static Vector3i operator *(Vector3i c1, Vector3i c2)
        {
            return new Vector3i(c1.x * c2.x, c1.y * c2.y, c1.z * c2.z);
        }

        public static Vector3i operator *(int c1, Vector3i c2)
        {
            return new Vector3i(c1 * c2.x, c1 * c2.y, c1 * c2.z);
        }

        // allow callers to initialize
        public int this[int idx]
        {
            get { return idx == 0 ? x : y; }
            set
            {
                switch (idx)
                {
                    case 0:
                        x = value;
                        break;

                    default:
                        y = value;
                        break;
                }
            }
        }

        public Vector3 toVector3()
        {
            return new Vector3(x, y, z);
        }

        public float magnitude
        {
            get { return (float)Math.Sqrt(x * x + y * y + z * z); }
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Vector3i p = obj as Vector3i;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (x == p.x) && (y == p.y) && (z == p.z);
        }

        public bool Equals(Vector3i p)
        {
            // If parameter is null return false:
            if ((object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (x == p.x) && (y == p.y) && (z == p.z);
        }

        public static bool operator ==(Vector3i a, Vector3i b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            return a.x == b.x && a.y == b.y && a.z == b.z;
        }

        public static bool operator !=(Vector3i a, Vector3i b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return x ^ y ^ z;
        }

        public override string ToString()
        {
            return "[" + x + ", " + y + ", " + z + "]";
        }
    }
}
