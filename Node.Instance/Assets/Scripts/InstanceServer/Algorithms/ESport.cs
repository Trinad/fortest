﻿using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.InstanceServer.Algorithms
{
    public class ESport
    {
        //private string eSportUrl;
        //private string eSportResultUrl;
        //private string eSportGameToken;
        //private string eSportGameSlug;
        //private string eSportGameName;
        //private bool eSportEnable;

        public ESport(/*AppConfig config*/)
        {
            //if (config.eSportEnable)
            //{
            //    eSportGameToken = config.eSportGameToken;
            //    eSportGameSlug = config.eSportGameSlug;
            //    eSportGameName = config.eSportGameName;
            //    eSportUrl = config.eSportUrl.Replace("{gameSlug}", eSportGameSlug);
            //    eSportResultUrl = eSportUrl + "/result";
            //}
            //eSportEnable = config.eSportEnable;
        }

        private void SendData(ESportGameStatus gameStatus, string postData)
        {
            //var request = (HttpWebRequest)WebRequest.Create(url);
            //request.Method = "POST";
            //request.ContentType = "application/json";

            //request.Headers.Add("x-gc-token", eSportGameToken);

            //byte[] requestBody = Encoding.UTF8.GetBytes(postData);
            //request.BeginGetRequestStream(GetRequestStreamCallback, new object[] { request, requestBody });

			ServerRpcListener.Instance.SentESportData(gameStatus, postData);
        }

        //private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        //{
        //    var request = (HttpWebRequest)((object[])asynchronousResult.AsyncState)[0];
        //    using (var postStream = request.EndGetRequestStream(asynchronousResult))
        //    {
        //        var byteArray = (byte[])((object[])asynchronousResult.AsyncState)[1];

        //        // Write to the request stream.
        //        postStream.Write(byteArray, 0, byteArray.Length);

        //    }
        //    request.BeginGetResponse(GetResponseCallback, request);
        //}

        //private void GetResponseCallback(IAsyncResult asynchronousResult)
        //{
        //    var request = (HttpWebRequest)asynchronousResult.AsyncState;
        //    try
        //    {
        //        var response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
        //        if (response != null)
        //        {
        //            var reader = new StreamReader(response.GetResponseStream());
        //            string responseString = reader.ReadToEnd();
        //            ILogger.Instance.Send("Responce = " + responseString, ErrorLevel.error);
        //        }
        //    }
        //    catch (WebException we)
        //    {
        //        var reader = new StreamReader(we.Response.GetResponseStream());
        //        string responseString = reader.ReadToEnd();
        //        ILogger.Instance.Send("Responce WebException error = " + responseString, ErrorLevel.error);
        //    }
        //    catch (Exception ex)
        //    {
        //        ILogger.Instance.Send("Responce Exception error = " + ex.ToString(), ErrorLevel.error);
        //    }
        //}
        //private bool isGuest(ArenaPlayerInfo actor)
        //{
        //    return actor.PersonId >= 0 && (string.IsNullOrEmpty(actor.esportId) || actor.esportId.ToLower() == "unauthenticateduser");
        //}

        private bool isBot(ArenaPlayerInfo actor)
        {
            return actor.PersonId < 0;
        }

        //private string getArenaId(ArenaPlayerInfo actor)
        //{
        //    return string.IsNullOrEmpty(actor.esportId) ? actor.PersonId.ToString() : actor.esportId;
        //}

        //public void SendSheduledData(string eSportGameName, string matchId, List<ArenaPlayerInfo> actors, bool team)
        //{
        //    if (string.IsNullOrEmpty(eSportGameName) || string.IsNullOrEmpty(matchId))
        //        return;
        //    //if (!eSportEnable)
        //    //    return;

        //    Hashtable hashtable = new Hashtable();
        //    hashtable["status"] = ESportGameStatus.SCHEDULED.ToString();
        //    hashtable["matchId"] = matchId;
        //    hashtable["game"] = eSportGameName;
        //    hashtable["teamMatch"] = team;
        //    hashtable["startTime"] = XmlUtils.GetEpochTimeMilliSec();
        //    ArrayList arraylist = new ArrayList();
        //    for (int i = 0; i < actors.Count; i++)
        //    {
        //        Hashtable ht = new Hashtable();

        //        string arenaId = getArenaId(actors[i]);
        //        int characterId = actors[i].PersonId;

        //        ht["id"] = arenaId;
        //        ht["characterId"] = characterId;
        //        if (isBot(actors[i]))
        //            ht["type"] = "BOT";
        //        else
        //        {
        //            if (isGuest(actors[i]))
        //                ht["type"] = "GUEST";
        //            else
        //                ht["type"] = "PLAYER";
        //            ht["solo_rating"] = actors[i].startRating;
        //        }
        //        ht["username"] = actors[i].Name;
        //        arraylist.Add(ht);
        //    }
        //    hashtable["actors"] = arraylist;
        //    SendData(ESportGameStatus.SCHEDULED, JsonConvert.SerializeObject(hashtable));
        //}

        //public void SendPvpPlayingData(string eSportGameName, string matchId, string winner, List<ArenaPlayerInfo> actors)
        //{
        //    if (string.IsNullOrEmpty(eSportGameName) || string.IsNullOrEmpty(matchId))
        //        return;
        //    //if (!eSportEnable)
        //    //    return;

        //    Hashtable hashtable = new Hashtable();
        //    hashtable["status"] = ESportGameStatus.PLAYING.ToString();
        //    hashtable["matchId"] = matchId;
        //    hashtable["game"] = eSportGameName;
        //    hashtable["winner"] = winner;
        //    Hashtable scoresHt = new Hashtable();
        //    for (int i = 0; i < actors.Count; i++)
        //    {
        //        Hashtable ht = new Hashtable();
        //        ht["points"] = actors[i].Score;
        //        string arenaId = getArenaId(actors[i]);
        //        scoresHt[arenaId] = ht;
        //    }
        //    hashtable["score"] = scoresHt;
        //    SendData(ESportGameStatus.PLAYING, JsonConvert.SerializeObject(hashtable));
        //}

        //public void SendPvpFinishedData(string eSportGameName, string matchId, string winner, List<ArenaPlayerInfo> actors, object details)
        //{
        //    if (string.IsNullOrEmpty(eSportGameName) || string.IsNullOrEmpty(matchId))
        //        return;
        //    //if (!eSportEnable)
        //    //    return;

        //    Hashtable hashtable = new Hashtable();
        //    hashtable["status"] = ESportGameStatus.FINISHED.ToString();
        //    hashtable["matchId"] = matchId;
        //    hashtable["game"] = eSportGameName;
        //    hashtable["winner"] = winner;
        //    Hashtable scoresHt = new Hashtable();
        //    for (int i = 0; i < actors.Count; i++)
        //    {
        //        Hashtable ht = new Hashtable();
        //        ht["points"] = actors[i].Score;
        //        string arenaId = getArenaId(actors[i]);
        //        //if (!isBot(actors[i]))
        //        //    ht["solo_rating"] = Math.Max(0, actors[i].startRating + actors[i].RatingDiff);
        //        scoresHt[arenaId] = ht;
        //    }
        //    hashtable["score"] = scoresHt;
        //    hashtable["details"] = (details != null) ? details : new object();
        //    SendData(ESportGameStatus.FINISHED, JsonConvert.SerializeObject(hashtable));
        //}
    }
}
