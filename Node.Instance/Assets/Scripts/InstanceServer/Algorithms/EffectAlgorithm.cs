﻿using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Algorithms
{
	public class EffectAlgorithm
	{
		public readonly List<EffectType> HitEffects = new List<EffectType>
		{
			EffectType.MonsterHit_nestman,
			EffectType.MonsterHit_moko,
			EffectType.MonsterHit_swamp,
			EffectType.MonsterHit_wasp,
		};

		public EffectType GetHitEffect(MapTargetObject target, MapTargetObject attacker, bool crit, out int upgrade, out float height)
		{
			upgrade = 0;
			height = 0;

            if (target != null && string.IsNullOrEmpty(target.TargetName))
                return EffectType.None;

		    var statContainer = target?.GetStatContainer();
		    if (statContainer != null && statContainer.GetStatValue(Stats.CanNotDie) > 0)
		    {
		        return EffectType.None;
		    }

		    if (attacker != null)
			{
                return crit ? EffectType.MonsterHitCritical : EffectType.MonsterHit;
			}

			upgrade = FRRandom.Next(0, 5);
			return HitEffects.Random();
		}

		internal EffectType GetDieEffect(MonsterBehaviour monster, out int upgrade)
		{
            if(monster.Data.DeathEffect!= EffectType.None)
            {
                upgrade = 0;
                return monster.Data.DeathEffect;
            }

			if (monster.Data.MonsterType == MonsterType.Boss)
			{
				upgrade = 0;
				return EffectType.Death_boss;
			}
			else
			{
				upgrade = FRRandom.Next(0, 2);
				return EffectType.MonsterDeath;
			}
		}
    }
}
