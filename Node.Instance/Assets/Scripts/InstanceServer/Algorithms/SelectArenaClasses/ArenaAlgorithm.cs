﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils.Common;
using Assets.Scripts.Utils.Logger;
using UtilsLib;
using UtilsLib.DegSerializers;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using UtilsLib.NetworkingData.Party;
using UtilsLib.XmlData;
using static UtilsLib.XmlData.ArenaAlgorithmData;

namespace Assets.Scripts.InstanceServer.Algorithms
{
    public class SelectMapItemData
    {
        public string LevelName = string.Empty;
	    public bool IsNew;
	    public bool IsNotViewed;

		public SelectMapItemData(InstancePerson person, GamesInfoData gameInfo)
		{
			var edgeLocation = person.gameXmlData.Edges.GetEdgeLocationXmlData(gameInfo.Map);
			LevelName = edgeLocation.LevelName;

			var jsonName = person.gameXmlData.Edges.GetEdgeLocationXmlData(gameInfo.Map)?.GetJsonName();
			var status = person.login.MapItemDataStatuses.Find(x => x.JsonName == jsonName);
			if (status != null)
			{
				IsNew = status.IsNew;
				IsNotViewed = status.IsNotViewed;
			}
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            SelectMapItemData si = (SelectMapItemData)value;
            stream.Write<string>(si.LevelName);
            stream.Write<bool>(si.IsNew);
            stream.Write<bool>(si.IsNotViewed);
		}
    }

	public class SelectArenaMatchData
    {
        public List<SelectMapItemData> Maps { get; set; }
        
        public bool RandomMap;
        public ArenaMatchType MatchType;
        public int MatchButtonId;
		public bool IsRating;
	    public bool IsNew;
	    public bool IsNotViewed;
		public ArenaUnlockType Enable;

		public SelectArenaMatchData(InstancePerson person, PlayModeType playModeType, Match matchInfo)
		{
			MatchType = matchInfo.MatchType;
			MatchButtonId = matchInfo.MatchButtonId;
			RandomMap = matchInfo.IsRandomMap;
			IsRating = matchInfo.IsRating;
			Enable = matchInfo.GetRule(person.StatContainer, playModeType).Type;
			Maps = matchInfo.ArenaGameInfo.Where(x => x.Enable).Select(x => new SelectMapItemData(person, x)).ToList();

			var status = person.login.ArenaMatchDataStatuses.Find(x => x.ButtonId == matchInfo.MatchButtonId);
			if (status != null)
			{
				IsNew = status.IsNew;
				IsNotViewed = status.IsNotViewed;
			}
		}

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            SelectArenaMatchData si = (SelectArenaMatchData)value;
            stream.Write<List<SelectMapItemData>>(si.Maps);
            stream.Write<bool>(si.RandomMap);
            stream.Write(si.MatchType);
            stream.Write(si.MatchButtonId);
            stream.Write(si.IsRating);
            stream.Write(si.IsNew);
            stream.Write(si.IsNotViewed);
			stream.Write(si.Enable);
        }
    }

	public class SelectMapData
    {
		public Dictionary<PlayModeType, List<SelectArenaMatchData>> Arenas;

		public SelectMapData(InstancePerson person, ArenaAlgorithm arenaAlgorithm)
		{
			Arenas = new Dictionary<PlayModeType, List<SelectArenaMatchData>>();

			foreach (PlayModeType playModeType in Enum.GetValues(typeof(PlayModeType)))
			{
				var arenas = new List<SelectArenaMatchData>();
				foreach (var matchInfo in arenaAlgorithm.GetMatchInfoData())
					if (matchInfo.CheckRule(person.StatContainer) && matchInfo.ArenaGameInfo.Exists(x => x.Enable))
						arenas.Add(new SelectArenaMatchData(person, playModeType, matchInfo));

				Arenas[playModeType] = arenas;
			}
		}

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            SelectMapData si = (SelectMapData)value;
            stream.Write(si.Arenas);
        }
    }

    public sealed class ArenaAlgorithm
	{
		private ArenaAlgorithmData m_arenaData;
		private GameXmlData gameXmlData;
		private DataHashController dataHashController;

		public ArenaAlgorithm(DataHashController dataHashController, GameXmlData gameXmlData)
		{
			this.dataHashController = dataHashController;
			this.gameXmlData = gameXmlData;
		}

		public void SetXmlData(ArenaAlgorithmData xmlData)
		{
            m_arenaData = xmlData;
			var battleXmlData = BattleXmlData.GetBattleXmlData(gameXmlData.Edges, this);

			var battleXmlDataString = JSON.JsonEncode(battleXmlData.JsonEncode());

			//Sos.Error(battleXmlDataString);
			dataHashController.LoadString(DataHashController.DataHashType.battles, battleXmlDataString);
			ILogger.Instance.Send("ArenaAlgorithm.SetXmlData(): received data, do init");
		}

        internal void getBaseRatingForStars(int rating, out int baseRating, out int step)
        {
            m_arenaData.getBaseRatingForStars(rating, out baseRating, out step);
        }

        internal int GetRankMinValue(int rating)
        {
            if (m_arenaData == null)
                return 0;

            int minRating;
            m_arenaData.GetCurrentRankMinValue(rating, out minRating);
            return minRating;
        }

		internal ArenaAlgorithmData.GamesInfoData GetGameInfoData(ArenaMatchType matchType, string levelName)
		{
			return m_arenaData.getGameInfoData(matchType, levelName);
		}

        public Match GetMatchData(int matchButtonId)
        {
            return m_arenaData.GetMatchData(matchButtonId);
        }

	    public int GetActualButtonId(InstancePerson person, int lastMatchButtonId)
	    {
	        var match = GetMatchData(lastMatchButtonId);
	        Match actualMatch = null;

	        if (match != null)
	        {
	            actualMatch = GetActualMatchData(person, match.MatchType);
	        }

	        if (actualMatch != null)
	        {
	            return actualMatch.MatchButtonId;
	        }

            if (lastMatchButtonId != -1)
                ILogger.Instance.Send($"Не найден матч для LastButtonId = {lastMatchButtonId}", ErrorLevel.warning);

	        return GetFirstMatchData(person)?.MatchButtonId ?? -1;
        }

	    public Match GetActualMatchData(InstancePerson person, ArenaMatchType matchType)
	    {
	        foreach (var match in m_arenaData.ArenaMatchInfo)
	        {
	            if (match.MatchType == matchType && match.CheckRule(person.StatContainer) && match.CheckEnable(person.StatContainer, person.login.currentPlayModeType))
	                return match;
	        }

	        return null;
	    }

	    public Match GetFirstMatchData(InstancePerson person)
	    {
	        foreach (var match in m_arenaData.ArenaMatchInfo)
	        {
	            if (match.CheckRule(person.StatContainer) && match.CheckEnable(person.StatContainer, person.login.currentPlayModeType))
	                return match;
	        }

	        return null;
	    }

        internal List<Match> GetMatchInfoData()
        {
            return m_arenaData.ArenaMatchInfo;
        }
    }
}
