﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.InstanceServer.Algorithms
{
	public class BattleXmlData
	{
		public List<MatchConfig> matchConfigs = new List<MatchConfig>();
		public List<MapConfig> mapConfigs = new List<MapConfig>();

		public static BattleXmlData GetBattleXmlData(Edges edges, ArenaAlgorithm arenaAlgorithm)
		{
			var xmlData = new BattleXmlData();
			foreach (var location in edges.GetAllLocationXmlDatas())
			{
				var mapConfig = new MapConfig()
				{
					LevelName = location.LevelName,
					IconId = location.IconId,
					miniIconId = location.MiniIconId,
					descriptionKey = location.DescriptionKey,
					nameKey = location.TechnicalName,//TODO
					playersCountInCustomGame = 5,//TODO
					playersCountInRatingGame = 5,//TODO
				};

				xmlData.mapConfigs.Add(mapConfig);
			}

			foreach(var match in arenaAlgorithm.GetMatchInfoData())
			{
				var matchConfig = new MatchConfig()
				{
					arenaId = match.MatchButtonId,//  matchId++,//TODO
					MatchType = match.MatchType,
					ButtonBackgroundName = match.ButtonBackgroundName,
					ButtonSelectionName = match.ButtonSelectionName,
					DescriptionImageName = match.DescriptionImageName,
					DescriptionLocKey = match.DescriptionLocKey,
					EventIconName = match.EventIconName,
					EventLocKey = match.EventLocKey,
					IconName = match.IconName,
					Notification = match.Notification,
					TextColor = match.TextColor,
					TitleLocKey = match.TitleLocKey,
					IsEvent = match.IsEvent,
				};
				xmlData.matchConfigs.Add(matchConfig);
			}

			return xmlData;
		}
	}
	public class MatchConfig
	{
		public int arenaId;
		public ArenaMatchType MatchType;
		public string TitleLocKey;
		public string IconName;
		public string ButtonBackgroundName;
		public string ButtonSelectionName;
		public string DescriptionImageName;
		public string DescriptionLocKey;
		public string TextColor;
		public string Notification;
		public string EventIconName;
		public string EventLocKey;
		public bool IsEvent;
	}
	public class MapConfig
	{
		public string LevelName;

		public string IconId;
		public string miniIconId;

		public string nameKey;
		public string descriptionKey;

		public int playersCountInRatingGame;
		public int playersCountInCustomGame;
	}
}
