﻿using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.AttackDatas;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils.Common;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text.RegularExpressions;
using UtilsLib;

namespace Assets.Scripts.InstanceServer.Algorithms
{
    public static class GeometryAlgorithm
    {
        public static int LinearInterpolation(int[] arrx, int[] arry, int x)
        {
            if (x < arrx[0])
                return arry[0];

            if (x > arrx[arrx.Length - 1])
                return arry[arry.Length - 1];

            int i = 0;
            for (; i < arrx.Length - 1; i++)
            {
                if (/*x >= arrx[i] &&*/ x <= arrx[i + 1])
                    break;
            }
            int dy = arry[i + 1] - arry[i];
            int dx = arrx[i + 1] - arrx[i];
            return arry[i] + (x - arrx[i]) * dy / dx;
        }

        /// <summary>
        /// если x между arrx[i] и  arrx[i + 1] возвращает arrx[i + 1]
        /// </summary>
        public static int StepInterpolationMax(int[] arrx, int[] arry, int x)
        {
            if (x < arrx[0])
                return arry[0];

            if (x > arrx[arrx.Length - 1])
                return arry[arry.Length - 1];

            int i = 0;
            for (; i < arrx.Length - 1; i++)
            {
                if (/*x >= arrx[i] &&*/ x <= arrx[i + 1])
                    break;
            }
            return arry[i + 1];
        }

        /// <summary>
        /// если x между arrx[i] и  arrx[i + 1] возвращает arrx[i]
        /// </summary>
        public static int StepInterpolationMin(int[] arrx, int[] arry, int x)
        {
            if (x < arrx[0])
                return arry[0];

            if (x > arrx[arrx.Length - 1])
                return arry[arry.Length - 1];

            int i = 0;
            for (; i < arrx.Length - 1; i++)
            {
                if (/*x >= arrx[i] &&*/ x <= arrx[i + 1])
                    break;
            }
            return arry[i];
        }

        public static List<ServerVectorInt> ParseListPoint(string str)
        {
            List<ServerVectorInt> res = new List<ServerVectorInt>();
            //string subjectString = "(-1,-2) (2 ,3)(-4,5)(-5, 6)";
            str = str.Replace(" ", "");
            Regex regexObj = new Regex(@"\((-?\d+),(-?\d+)\)+");
            Match matchResults = regexObj.Match(str);
            while (matchResults.Success)
            {
                var point = new ServerVectorInt(int.Parse(matchResults.Groups[1].Value) * 100,
                                                int.Parse(matchResults.Groups[2].Value) * 100);

                res.Add(point);
                matchResults = matchResults.NextMatch();
            }
            return res;
        }

        public static double InInterval(double val, double downEdge, double upEdge)
        {
            if (val > upEdge)
                val = upEdge;
            if (val < downEdge)
                val = downEdge;

            return val;
        }

        public static Vector3 Vector2ToVector3(this Vector2 v2)
        {
            return new Vector3(v2.X, 0, v2.Y);
        }

        internal static Vector2 Vector3ToVector2(this Vector3 v3)
        {
            return new Vector2(v3.X, v3.Z);
        }

        public static float Lerp(float startValue, float endValue, float time)
        {
            return (1f - time) * startValue + time * endValue;
        }

        /// <summary>
        /// Положительное направление поворота - от положительного направления x к положительному направлению y
        /// </summary>
        public static ServerVectorDouble RotatePlus90(ServerVectorDouble v)
        {
            return new ServerVectorDouble(-v.y, v.x);
        }

        public static ServerVectorInt RotatePlus90(ServerVectorInt v)
        {
            return new ServerVectorInt(-v.y, v.x);
        }

        /// <summary>
        /// Положительное направление поворота - от положительного направления x к положительному направлению y
        /// </summary>
        public static ServerVectorDouble RotateMinus90(ServerVectorDouble v)
        {
            return new ServerVectorDouble(v.y, -v.x);
        }

        public static ServerVectorInt RotateMinus90(ServerVectorInt v)
        {
            return new ServerVectorInt(v.y, -v.x);
        }

        public static Vector3 RotateMinus90(Vector3 v)
        {
            return new Vector3(v.Z, v.Y, -v.X);
        }

        public static Vector2 RotateMinus90(Vector2 v)
        {
            return new Vector2(v.Y, -v.X);
        }

        public static void GetCenterCell(int X, int Y, out int iCenterX, out int iCenterY)
        {
            GetCenterCell(X, Y, out iCenterX, out iCenterY, 100);
        }

        public static void GetCenterCell(int X, int Y, out int iCenterX, out int iCenterY, int iCellSize)
        {
            iCenterX = (int)Math.Floor(X / (double)iCellSize) * iCellSize + iCellSize / 2;
            iCenterY = (int)Math.Floor(Y / (double)iCellSize) * iCellSize + iCellSize / 2;
        }

        public static bool CheckMapOpacity(int[] mapPoints, int x, int y)
        {
            return (Array.BinarySearch(mapPoints, (x << 16) + y) >= 0);
        }

        public static int[] CreateMapRound(int radius)
        {
            int height = radius * 2; //сторона квадрата
            int points = height * height;//площадь квадрата

            List<int> _pointList = new List<int>(points + 1);
            for (var y = 0; y <= height; ++y)
            {
                for (var x = 0; x <= height; ++x)
                {
                    if (HitTestRound(radius, x, y))
                        _pointList.Add((x << 16) + y);
                }
            }

            _pointList.Sort();
            return _pointList.ToArray();
        }

        public static bool HitTestRound(int radius, int x, int y)
        {
            return (x - radius) * (x - radius) + (y - radius) * (y - radius) <= radius * radius;
        }

        /// <summary>
        /// Округление до ближайшего целого, не менее текущего
        /// </summary>
        private static int RoundToIntNotLessCurrent(double current)
        {
            int result = (int)current;
            return result < current ? result + 1 : result;
        }

        public static long DistanceInSquare(int iX1, int iY1, int iX2, int iY2)
        {
            long dx = iX2 - iX1;
            long dy = iY2 - iY1;
            return dx * dx + dy * dy;
        }

        public static float DistanceInSquare(float iX1, float iY1, float iX2, float iY2)
        {
            float dx = iX2 - iX1;
            float dy = iY2 - iY1;
            return dx * dx + dy * dy;
        }

        /// <summary>
        /// Расстояние между указанными координатами строго меньше maxRadius и больше minRadius
        /// </summary>
        public static bool InRing(float minRadius, float maxRadius, Vector3 v1, Vector3 v2)
        {
            float sqrMagnitude = (v1 - v2).sqrMagnitude();
            return sqrMagnitude > (minRadius * minRadius) && sqrMagnitude < (maxRadius * maxRadius);
        }

        /// <summary>
        /// Расстояние между указанными координатами строго меньше iRadius
        /// </summary>
        public static bool InRadius(int iRadius, int iX1, int iY1, int iX2, int iY2)
        {
            return DistanceInSquare(iX1, iY1, iX2, iY2) < (iRadius * iRadius);
        }

        /// <summary>
        /// Расстояние между указанными координатами строго меньше iRadius
        /// </summary>
        public static bool InRadius(float radius, Vector3 v1, Vector3 v2)
        {
            return (v1 - v2).sqrMagnitude() < (radius * radius);
        }

        /// <summary>
        /// Расстояние между указанными координатами строго меньше iRadius
        /// </summary>
        public static bool InRadius(float iRadius, float iX1, float iY1, float iX2, float iY2)
        {
            return DistanceInSquare(iX1, iY1, iX2, iY2) < (iRadius * iRadius);
        }

        /// <summary>
        /// Расстояние между указанными координатами не превышает dRadius
        /// </summary>
        public static bool InRadius(double dRadius, int iX1, int iY1, int iX2, int iY2)
        {
            return DistanceInSquare(iX1, iY1, iX2, iY2) < (dRadius * dRadius);
        }

        //public static bool InRadius(int iRadius, Coordinates c, int mapId2, int iX2, int iY2)
        //{
        //	return c.iMapId == mapId2 && InRadius(iRadius, c.iXCoord, c.iYCoord, iX2, iY2);
        //}

        public static bool InRadius(float iRadius, Vector3 c, float iX2, float iY2)
        {
        	return InRadius(iRadius, c.X, c.Z, iX2, iY2);
        }

        public static bool InRadius(float iRadius, Vector2 c, float iX2, float iY2)
        {
            return InRadius(iRadius, c.X, c.Y, iX2, iY2);
        }

        //public static bool InRadius(int iRadius, Coordinates c1, Coordinates c2)
        //{
        //	return InRadius(iRadius, c1.iXCoord, c1.iYCoord, c2.iXCoord, c2.iYCoord);
        //}

        //public static bool InRadius(int iRadius, IAttacker lo, IAttacker lo2)
        //{
        //	if (lo == null || lo2 == null)
        //		return false;

        //	Coordinates coord = lo.GetCoordinates();
        //	Coordinates coord2 = lo2.GetCoordinates();
        //	if (coord.iMapId != coord2.iMapId)
        //		return false;

        //	if (lo.bHidden || lo2.bHidden)
        //		return false;

        //	return InRadius(iRadius, coord.iXCoord, coord.iYCoord, coord2.iXCoord, coord2.iYCoord);
        //}

        //public static bool InRadius(int iRadius, ITarget lo, ITarget lo2)
        //{
        //	if (lo == null || lo2 == null)
        //		return false;

        //	Coordinates coord = lo.GetCoordinates();
        //	Coordinates coord2 = lo2.GetCoordinates();
        //	if (coord.iMapId != coord2.iMapId)
        //		return false;



        //	return InRadius(iRadius, coord.iXCoord, coord.iYCoord, coord2.iXCoord, coord2.iYCoord);
        //}

        //public static bool InRadius(int iRadius, ITarget lo, int iMapId2, int iX2, int iY2)
        //{
        //	return InRadius(iRadius, lo, iMapId2, iX2, iY2, false);
        //}

        //public static bool InRadius(double iRadius, ITarget lo, int iMapId2, int iX2, int iY2, bool info)
        //{
        //	if (lo == null)
        //		return false;

        //	Coordinates coord = lo.GetCoordinates();
        //	if ((info ? coord.iMapInfoId : coord.iMapId) != iMapId2)
        //		return false;

        //	return InRadius(iRadius, coord.iXCoord, coord.iYCoord, iX2, iY2);
        //}

        //TODO: тут можно сразу определить ближайшую точку, а не пробовать все варианты
        public static bool InRadius(double iRadius, int cellSize, int iX1, int iY1, int iX2, int iY2, int iSizeX2, int iSizeY2)
        {
            if (iX1 < (iX2 + iSizeX2) && iX1 >= iX2 && iY1 < (iY2 + iSizeY2) && iY1 >= iY2)
                return true;

            for (int i = iX2; i < iX2 + iSizeX2; i++)
            {
                if (InRadius(iRadius + cellSize, iX1, iY1, i, iY2))
                    return true;

                if (InRadius(iRadius + cellSize, iX1, iY1, i, iY2 + iSizeY2 - 1))
                    return true;
            }

            for (int i = iY2 + 1; i < iY2 + iSizeY2 - 1; i++)
            {
                if (InRadius(iRadius + cellSize, iX1, iY1, iX2, i))
                    return true;

                if (InRadius(iRadius + cellSize, iX1, iY1, iX2 + iSizeX2 - 1, i))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// lo2 ближе к lo чем lo1
        /// </summary>
        //public static bool SecondTargetIsCloser(ITarget lo, ITarget lo1, ITarget lo2)
        //{
        //	if (lo == null || lo2 == null)
        //		return false;
        //	if (lo1 == null)
        //		return true;

        //	return Distance(lo, lo2) < Distance(lo, lo1);
        //}

        /// <summary>
        /// 2 соордината к coord ближе чем 1
        /// </summary>
        //public static bool SecondPointIsCloser(Coordinates coord, int iX1, int iY1, int iX2, int iY2)
        //{
        //	return Distance(coord.iXCoord, coord.iYCoord, iX2, iY2) < Distance(coord.iXCoord, coord.iYCoord, iX1, iY1);
        //}

        public static double GetOrientation(int iX1, int iY1, int iX2, int iY2)
        {
            double res = Math.Atan2(iY2 - iY1, iX2 - iX1);
            if (res < 0)
                res += Math.PI * 2;

            return res;
        }

        //public static double GetOrientation(ITarget t1, ITarget t2)
        //{
        //	var c1 = t1.GetCoordinates();
        //	var c2 = t2.GetCoordinates();
        //	return GetOrientation(c1.iXCoord, c1.iYCoord, c2.iXCoord, c2.iYCoord);
        //}

        public static double GetOrientation(Vector2 v)
        {
            double res = Math.Atan2(v.Y, v.X);
            if (res < 0)
                res += Math.PI * 2;

            return res;
        }

        ///<summary>
        /// Метод, вычесляющий расстояние между двумя точками на карте
        ///</summary>
        public static int Distance(int iX1, int iY1, int iX2, int iY2)
        {
            return RoundToIntNotLessCurrent(Math.Sqrt((iX1 - iX2) * (iX1 - iX2) + (iY1 - iY2) * (iY1 - iY2)));
        }

        ///<summary>
        /// Метод, вычесляющий расстояние между двумя точками на карте
        ///</summary>
        public static double DistanceExact(int iX1, int iY1, int iX2, int iY2)
        {
            return Math.Sqrt((iX1 - iX2) * (iX1 - iX2) + (iY1 - iY2) * (iY1 - iY2));
        }

        ///<summary>
        /// Метод, вычесляющий расстояние между двумя точками на карте
        ///</summary>
        public static int Distance(int iX1, int iY1, int iX2, int iY2, bool toNPC)
        {
            if (toNPC)
            {
                int xDistance = iX1 > iX2 ? iX1 - iX2 : iX2 - iX1;
                int yDistance = iY1 > iY2 ? iY1 - iY2 : iY2 - iY1;

                return (xDistance >= yDistance) ? xDistance : yDistance;
            }

            return RoundToIntNotLessCurrent(Math.Sqrt((iX1 - iX2) * (iX1 - iX2) + (iY1 - iY2) * (iY1 - iY2)));
        }

        ///<summary>
        /// Метод, вычесляющий расстояние между двумя точками на картах
        ///</summary>
        public static int Distance(int mapId1, int iX1, int iY1, int mapId2, int iX2, int iY2)
        {
            return mapId1 != mapId2 ? int.MaxValue : Distance(iX1, iY1, iX2, iY2);
        }

        /////<summary>
        ///// Метод, вычесляющий расстояние между двумя точками на картах
        /////</summary>
        //public static int Distance(Coordinates coord1, Coordinates coord2)
        //{
        //	if (coord1 == null || coord2 == null || coord1.iMapId != coord2.iMapId)
        //		return int.MaxValue;
        //	return Distance(coord1.iXCoord, coord1.iYCoord, coord2.iXCoord, coord2.iYCoord);
        //}

        ///<summary>
        /// Метод, вычесляющий расстояние между "живым объектом" - мобом, персом, петом
        ///  и точкой на карте
        ///</summary>
        //public static int Distance(ITarget lo, Coordinates coord2)
        //{
        //	Coordinates coord1 = lo.GetCoordinates();
        //	if (coord1 == null || coord2 == null || coord1.iMapId != coord2.iMapId)
        //		return int.MaxValue;
        //	return Distance(coord1.iXCoord, coord1.iYCoord, coord2.iXCoord, coord2.iYCoord);
        //}

        ///<summary>
        /// Метод, вычесляющий расстояние между "живым объектом" - мобом, персом, петом
        ///  и точкой на карте
        ///</summary>
        //public static int Distance(ITarget lo, CoordinatesInfo mp)
        //{
        //	Coordinates pCoord = lo.GetCoordinates();

        //	if (mp.MapInfo.iId == pCoord.iMapInfoId)
        //		return Distance(mp.iXCoord, mp.iYCoord, pCoord.iXCoord, pCoord.iYCoord);

        //	return int.MaxValue;
        //}

        ///<summary>
        /// Метод, вычесляющий расстояние между двумя точками
        ///</summary>
        //public static int Distance(CoordinatesInfo mp1, CoordinatesInfo mp2)
        //{
        //	if (mp1.MapInfo == mp2.MapInfo)
        //		return Distance(mp1.iXCoord, mp1.iYCoord, mp2.iXCoord, mp2.iYCoord);

        //	return int.MaxValue;
        //}

        ///<summary>
        /// Метод, вычесляющий расстояние между двумя "живыми объектами"
        ///</summary>
        //public static int Distance(ITarget lo, ITarget lo2)
        //{
        //	if (lo == null || lo2 == null)
        //		return int.MaxValue;

        //	Coordinates coord = lo.GetCoordinates();
        //	Coordinates coord2 = lo2.GetCoordinates();
        //	if (coord == null || coord2 == null || coord.iMapId != coord2.iMapId)
        //		return int.MaxValue;

        //	switch (lo.TargetType)
        //	{
        //		case TargetType.Person:
        //			if (lo2.TargetType == TargetType.ActiveMapObject)
        //				coord = lo.GetCoordinates();
        //			break;
        //	}
        //	switch (lo2.TargetType)
        //	{
        //		case TargetType.Person:
        //			if (lo.TargetType == TargetType.ActiveMapObject)
        //				coord2 = lo2.GetCoordinates();
        //			break;
        //	}

        //	bool toNPC = lo.TargetType == TargetType.NPC || lo.TargetType == TargetType.ActiveMapObject ||
        //		lo2.TargetType == TargetType.NPC || lo2.TargetType == TargetType.ActiveMapObject;

        //	return Distance(coord.iXCoord, coord.iYCoord, coord2.iXCoord, coord2.iYCoord, toNPC);
        //}

        ///<summary>
        /// Метод, вычесляющий расстояние между "живым объектом" и точкой
        ///</summary>
        //public static int Distance(ITarget lo, int iMapId, int iXCoord, int iYCoord)
        //{
        //    if (lo == null)
        //        return int.MaxValue;

        //    Coordinates coord = lo.GetCoordinates();
        //    if (coord.iMapId != iMapId)
        //        return int.MaxValue;


        //    bool toNPC = lo.TargetType == TargetType.NPC || lo.TargetType == TargetType.ActiveMapObject;
        //    return Distance(coord.iXCoord, coord.iYCoord, iXCoord, iYCoord, toNPC);
        //}

        /// <summary>
        /// тангенс угла ABC
        /// </summary>
        public static double Tang(int Ax, int Ay, int Bx, int By, int Cx, int Cy)
        {
            int BAx = Ax - Bx;
            int BAy = Ay - By;
            int BCx = Cx - Bx;
            int BCy = Cy - By;
            return VectorMultMagnitude(BAx, BAy, BCx, BCy) / (double)ScalarMult(BAx, BAy, BCx, BCy);
        }

        public static float VectorMultMagnitude(Vector2 a, Vector2 c)
        {
            return a.X * c.Y - a.Y * c.X;
        }

        public static float VectorMultMagnitudeIgnoreY(Vector3 a, Vector3 c)
        {
            return a.X * c.Z - a.Z * c.X;
        }

        public static int VectorMultMagnitude(ServerVectorInt a, ServerVectorInt c)
        {
            return a.x * c.y - a.y * c.x;
        }

        public static double VectorMultMagnitude(ServerVectorDouble a, ServerVectorInt c)
        {
            return a.x * c.y - a.y * c.x;
        }

        public static double VectorMultMagnitude(ServerVectorInt a, ServerVectorDouble c)
        {
            return a.x * c.y - a.y * c.x;
        }

        public static double VectorMultMagnitude(ServerVectorDouble a, ServerVectorDouble c)
        {
            return a.x * c.x - a.y * c.x;
        }

        /// <summary>
        /// векторное произведение
        /// </summary>
        /// <returns>[A x C]</returns>
        public static int VectorMultMagnitude(int Ax, int Ay, int Cx, int Cy)
        {
            return Ax * Cy - Ay * Cx;
        }

        /// <summary>
        /// векторное произведение
        /// </summary>
        /// <returns>[A x C]</returns>
        public static float VectorMultMagnitude(float Ax, float Ay, float Cx, float Cy)
        {
            return Ax * Cy - Ay * Cx;
        }

        /// <summary>
        /// векторное произведение
        /// </summary>
        /// <returns>[A x C]</returns>
        public static double VectorMultMagnitude(double Ax, double Ay, double Cx, double Cy)
        {
            return Ax * Cy - Ay * Cx;
        }

        /// <summary>
        /// скалярное произведение
        /// </summary>
        /// <returns>(A * C)</returns>
        public static int ScalarMult(int Ax, int Ay, int Cx, int Cy)
        {
            return Ax * Cx + Ay * Cy;
        }

        public static float ScalarMult(float Ax, float Ay, float Cx, float Cy)
        {
            return Ax * Cx + Ay * Cy;
        }

        public static int ScalarMult(ServerVectorInt a, ServerVectorInt c)
        {
            return a.x * c.x + a.y * c.y;
        }

        public static double ScalarMult(ServerVectorDouble a, ServerVectorDouble c)
        {
            return a.x * c.x + a.y * c.y;
        }

        public static float ScalarMult(Vector2 a, Vector2 c)
        {
            return a.X * c.X + a.Y * c.Y;
        }

        public static float ScalarMult(Vector3 a, Vector3 c)
        {
            return a.X * c.X + a.Y * c.Y + a.Z * c.Z;
        }

        /// <summary>
        /// Дистанция для атаки. Вычисление расстояния не через корни (как на клиенте)
        /// </summary>
        //public static int DistanceForAttack(Coordinates c, int mapId2, int iX2, int iY2)
        //{
        //    return c.iMapId == mapId2 ? Distance(c.iXCoord, c.iYCoord, iX2, iY2, true) : int.MaxValue;
        //}

        public static bool PointsNear(int x1, int y1, int x2, int y2)
        {
            return ((Math.Abs(x1 - x2) <= 1) && (Math.Abs(y1 - y2) <= 1));
        }

        /// <summary>
        /// расстояние от точки 3 до отрезка ограниченного точками A и B
        /// </summary>
        /// <returns></returns>
        public static int DistanceToPiece(int aiXCoord, int aiYCoord, int biXCoord, int biYCoord, int ix3, int iy3)
        {
            int t, w;
            int P = (ix3 - aiXCoord) * (biXCoord - aiXCoord) + (iy3 - aiYCoord) * (biYCoord - aiYCoord);
            int L = (ix3 - biXCoord) * (biXCoord - aiXCoord) + (iy3 - biYCoord) * (biYCoord - aiYCoord);
            if ((P * L) >= 0)
            {
                t = (ix3 - aiXCoord) * (ix3 - aiXCoord) + (iy3 - aiYCoord) * (iy3 - aiYCoord);
                w = (ix3 - biXCoord) * (ix3 - biXCoord) + (iy3 - biYCoord) * (iy3 - biYCoord);
                if (w < t) t = w;
            }
            else
            {
                w = (ix3 - aiXCoord) * (biYCoord - aiYCoord) - (iy3 - aiYCoord) * (biXCoord - aiXCoord);
                t = w * w / ((biXCoord - aiXCoord) * (biXCoord - aiXCoord) + (biYCoord - aiYCoord) * (biYCoord - aiYCoord));
            }
            return (int)Math.Sqrt(t);
        }

		public static void GetRandomPointInCircle(int iX, int iY, int iR, out int iXRes, out int iYRes)
		{
			double angle = ((double)FRRandom.Next(100) * 2 * Math.PI) / (100);
			int rad = (int)Math.Sqrt(FRRandom.Next(iR * iR));
			iXRes = iX + (int)(rad * Math.Sin(angle));
			iYRes = iY + (int)(rad * Math.Cos(angle));
		}

        public static void GetRandomPointInRing(float iX, float iY, float iR, out float iXRes, out float iYRes)
        {
            double angle= ((double)FRRandom.Next(100) * 2 * Math.PI) / (100);

            iXRes = iX + (int)(iR * Math.Sin(angle));
            iYRes = iY + (int)(iR * Math.Cos(angle));
        }

        public static void GetRandomPointInRing(int iX, int iY, int iRInner, int iROuter, out int iXRes, out int iYRes)
		{
			double angle = ((double)FRRandom.Next(100) * 2 * Math.PI) / (100);
			int rad = iRInner + (int)Math.Sqrt(FRRandom.Next((iROuter - iRInner) * (iROuter - iRInner)));
			iXRes = iX + (int)(rad * Math.Sin(angle));
			iYRes = iY + (int)(rad * Math.Cos(angle));
		}

        public static void GetRandomPointInRing(float iX, float iY, float iRInner, float iROuter, out float iXRes, out float iYRes)
        {
            double angle = ((double)FRRandom.Next(100) * 2 * Math.PI) / (100);
            float rad = iRInner + (float)Math.Sqrt(FRRandom.Next((iROuter - iRInner) * (iROuter - iRInner)));
            iXRes = iX + (float)(rad * Math.Sin(angle));
            iYRes = iY + (float)(rad * Math.Cos(angle));
        }

        public static void GetRandomPointInRing(int iX, int iY, double iRInner, double iROuter, out int iXRes, out int iYRes)
        {
            double angle = ((double)FRRandom.Next(100) * 2 * Math.PI) / (100);
            var rad = iRInner + Math.Sqrt(FRRandom.Next(1000) * (iROuter - iRInner) * (iROuter - iRInner) / 1000);
            iXRes = iX + (int)(rad * Math.Sin(angle));
            iYRes = iY + (int)(rad * Math.Cos(angle));
        }

        //public static void GetAccessiblePointInRing(Map mp, byte TransperentMask, int iStartX, int iStartY, double iRMin, double iRMax, out int iResX, out int iResY)
        //{
        //    ServerPathFinder.GetAccessiblePointInRing(mp, TransperentMask, iStartX, iStartY, iRMin, iRMax, out iResX, out iResY);
        //}

        //public static void RunAwayPoint(CoordinatesInfo PrideCoord, int iPrideRadius, ITarget Danger, Map map, int MapPoint_x, int MapPoint_y, out int iX, out int iY)
        //{
        //    Coordinates dangerCoords = Danger.GetCoordinates();
        //    CalcRunAwayPoint(out iX, out iY, PrideCoord.iXCoord, PrideCoord.iYCoord, iPrideRadius, MapPoint_x, MapPoint_y, dangerCoords.iXCoord, dangerCoords.iYCoord);
        //}

        /// <summary>
        /// Вычисление точки, куда надо убегать
        /// </summary>
        public static void CalcRunAwayPoint(out int iX, out int iY, int iPX, int iPY, int iPrideR, int iMx, int iMy, int iDx, int iDy)
        {
            int iPrideX = 0;
            int iPrideY = 0;
            int iMonsterX = iMx - iPX;
            int iMonsterY = iMy - iPY;
            int iDangerX = iDx - iPX;
            int iDangerY = iDy - iPY;

            double x = iPrideX;
            double y = iPrideY;

            if (iDangerX == iMonsterX)
            {
                int iabs_mx_px = Math.Abs(iMonsterX - iPrideX);
                if (iPrideR >= iabs_mx_px)
                {
                    x = iDangerX;
                    if (iDangerY > iMonsterY)
                        y = -Math.Pow(iPrideR * iPrideR - iabs_mx_px * iabs_mx_px, 0.5) + iPrideY;
                    else
                        y = Math.Pow(iPrideR * iPrideR - iabs_mx_px * iabs_mx_px, 0.5) + iPrideY;
                }
            }
            else
            {
                double a = (1.0 + Math.Pow((double)(iDangerY - iMonsterY) / (iDangerX - iMonsterX), 2.0));
                double b = iMonsterX * Math.Pow((double)(iDangerY - iMonsterY) / ((iDangerX - iMonsterX)), 2.0) - (double)iMonsterY * (iDangerY - iMonsterY) / ((iDangerX - iMonsterX));
                double c = (double)iPrideX * iPrideX - (double)iPrideR * iPrideR + Math.Pow(iMonsterY - (double)iMonsterX * (iDangerY - iMonsterY) / (iDangerX - iMonsterX), 2);

                //if (a == 0) //TODO_deg a = 1 + x*x всегда больше 0
                //{
                //    x = -c / (2 * b);
                //}
                //else
                {
                    if (b * b - a * c > 0)
                        x = (b + Math.Pow(b * b - a * c, 0.5)) / a;
                    else
                    {
                        iX = (int)Math.Round(x) + iPX;
                        iY = (int)Math.Round(y) + iPY;
                        return;
                    }
                }

                y = (double)(iDangerY - iMonsterY) / (iDangerX - iMonsterX) * (x - iMonsterX) + iMonsterY;

                //if (a != 0) //TODO_deg a = 1 + x*x всегда больше 0
                {
                    if ((iDangerX - iMonsterX) * (x - iMonsterX) + (iDangerY - iMonsterY) * (y - iMonsterY) > 0)
                    {
                        double x1 = (b - Math.Pow(b * b - a * c, 0.5)) / a;
                        double y1 = (double)(iDangerY - iMonsterY) / (iDangerX - iMonsterX) * (x1 - iMonsterX) + iMonsterY;
                        //x1, y1 дальше от опасности -- бежим к ней
                        iX = (int)Math.Round(x1) + iPX;
                        iY = (int)Math.Round(y1) + iPY;
                        return;
                    }
                }
            }

            //осталось бежать только к x, y
            iX = (int)Math.Round(x) + iPX;
            iY = (int)Math.Round(y) + iPY;
        }

        /// <summary>
        /// Точка находится в заданной области
        /// </summary>
        public static bool InArea(int iX, int iY, int iStartX, int iStartY, int iEndX, int iEndY)
        {
            return !(iX < iStartX || iX > iEndX || iY < iStartY || iY > iEndY);
        }

        public static int iMaxIntervalForActionUnderPoint = 150;

        //public static bool MakePath(Path path, byte TransperentMask, int StartX, int StartY, int EndX, int EndY, Map map)
        //{
        //    return ServerPathFinder.MakePath(path, StartX, StartY, EndX, EndY, map);
        //}

        /// <summary>
        /// Определяет пересечение отрезков A(iAX1, iAY1, iAX2, iAY2) и B (iBX1, iBY1, iBX2, iBY2),
        /// функция возвращает TRUE - если отрезки пересекаются, а если пересекаются 
        /// в концах или вовсе не пересекаются, возвращается FALSE
        /// </summary>
        public static bool ExistIntersection(float iAX1, float iAY1, float iAX2, float iAY2,
            float iBX1, float iBY1, float iBX2, float iBY2)
        {
            float v1 = (iBX2 - iBX1) * (iAY1 - iBY1) - (iBY2 - iBY1) * (iAX1 - iBX1);
            float v2 = (iBX2 - iBX1) * (iAY2 - iBY1) - (iBY2 - iBY1) * (iAX2 - iBX1);
            float v3 = (iAX2 - iAX1) * (iBY1 - iAY1) - (iAY2 - iAY1) * (iBX1 - iAX1);
            float v4 = (iAX2 - iAX1) * (iBY2 - iAY1) - (iAY2 - iAY1) * (iBX2 - iAX1);

            return (v1 * v2 < 0) && (v3 * v4 < 0);
        }

        /// <summary>
        /// Определяет пересечение отрезков A(iAX1, iAY1, iAX2, iAY2) и B (iBX1, iBY1, iBX2, iBY2),
        /// функция возвращает TRUE - если отрезки пересекаются, а если пересекаются 
        /// в концах или вовсе не пересекаются, возвращается FALSE
        /// </summary>
        public static bool ExistIntersection(int iAX1, int iAY1, int iAX2, int iAY2,
            int iBX1, int iBY1, int iBX2, int iBY2)
        {
            int v1 = (iBX2 - iBX1) * (iAY1 - iBY1) - (iBY2 - iBY1) * (iAX1 - iBX1);
            int v2 = (iBX2 - iBX1) * (iAY2 - iBY1) - (iBY2 - iBY1) * (iAX2 - iBX1);
            int v3 = (iAX2 - iAX1) * (iBY1 - iAY1) - (iAY2 - iAY1) * (iBX1 - iAX1);
            int v4 = (iAX2 - iAX1) * (iBY2 - iAY1) - (iAY2 - iAY1) * (iBX2 - iAX1);

            return (v1 * v2 < 0) && (v3 * v4 < 0);
        }

        /// <summary>
        /// расстояние от точки (x2,y2) до прямой проходящей через точки (x0,y0) и (x1,y1)
        /// </summary>
        public static double DistanceFromPointToLine(int x0, int y0, int x1, int y1, int x2, int y2)
        {
            int a1, b1, c1;
            GuidesVectors(x0, y0, x1, y1, out a1, out b1, out c1);
            return Math.Abs(a1 * x2 + b1 * y2 + c1) / Math.Sqrt(a1 * a1 + b1 * b1);
        }

        /// <summary>
        /// направляющий вектор проходящий через точки (x0,y0) и (x1,y1)
        /// </summary>
        public static void GuidesVectors(int x0, int y0, int x1, int y1, out int a2, out int b2, out int c2)
        {
            a2 = y0 - y1;
            b2 = x1 - x0;
            c2 = x0 * y1 - x1 * y0;
        }

        /// <summary>
        /// точка на векторе проходящем через точку (x0,y0) под углом alpha на расстоянии 100 от указанной точки
        /// </summary>
        public static void PointOnLine(int x0, int y0, double alpha, out int x1, out int y1)
        {
            PointOnLine(x0, y0, 100, alpha, out x1, out y1);
        }

        /// <summary>
        /// точка на векторе проходящем через точку (x0,y0) под углом alpha на расстоянии radius от указанной точки
        /// </summary>
        public static void PointOnLine(int x0, int y0, int radius, double alpha, out int x1, out int y1)
        {
            //TODO: вместо alpha можно сразу хранить вектор единичной длины. Слишком часто считаем тяжелые косинусы и синусы
            x1 = (int)(x0 + radius * Math.Cos(alpha));
            y1 = (int)(y0 + radius * Math.Sin(alpha));
        }

        //public static bool AttackPossible(Map map, Coordinates c1, Coordinates c2)
        //{
        //    return CheckPoints(map, map.MapCellXCoord(c1.iXCoord), map.MapCellYCoord(c1.iYCoord), map.MapCellXCoord(c2.iXCoord), map.MapCellYCoord(c2.iYCoord));
        //}

        //public static bool CheckPointsFor(ITarget lo, Map map, int x1, int y1, int x2, int y2)
        //{
        //    return CheckPoints(map, map.MapCellXCoord(x1), map.MapCellYCoord(y1), map.MapCellXCoord(x2), map.MapCellYCoord(y2), lo.TargetType);
        //}

        private static bool checkHeight(float h0, float h1, float h_middle, int sqr_d_full, int sqr_d_middle)
        {
            double d_middle = Math.Sqrt(sqr_d_middle);
            double d_full = Math.Sqrt(sqr_d_full);

            return (h1 - h0) * d_middle >= (h_middle - h0) * d_full;
        }

        /// <summary>
        /// проходит по клеткам на отрезке (x1,y1) - (x2,y2) и что-то делает пока func(x,y) == true
        /// координаты в см
        /// </summary>
        public static void ForEachCells(float x1cm, float y1cm, float x2cm, float y2cm, Func<int, int, bool> func)
        {
            int x1 = (int)(x1cm / Helper.VoxelSize);
            int y1 = (int)(y1cm / Helper.VoxelSize);

            if (!func(x1, y1))
                return;

            int x2 = (int)(x2cm / Helper.VoxelSize);
            int y2 = (int)(y2cm / Helper.VoxelSize);

            float absDx = Math.Abs(x2cm - x1cm);
            float absDy = Math.Abs(y2cm - y1cm);

            int dx = Math.Sign(x2 - x1);
            int dy = Math.Sign(y2 - y1);
            int xx = x1, yy = y1;

            float nextDiffX; // расстояние до следующего пересечения с сеткой двигаясь вдоль X
            float nextDiffY; // вдоль Y
            if (x2cm - x1cm >= 0)
                nextDiffX = (x1 + 1) * Helper.VoxelSize - x1cm;
            else
                nextDiffX = x1cm - x1 * Helper.VoxelSize;

            if (y2cm - y1cm >= 0)
                nextDiffY = (y1 + 1) * Helper.VoxelSize - y1cm;
            else
                nextDiffY = y1cm - y1 * Helper.VoxelSize;

            int count = 0;
            while (xx != x2 || yy != y2)
            {
                if (++count > 1000)
                    break;

                float profitY = nextDiffY * absDx;
                float profitX = nextDiffX * absDy;

                if (profitX == profitY)
                    //сейчас будет скачек по диагонали. Мы можем проскочить конечную точку. проверим дополнительно две координаты
                    if ((xx + dx == x2 && yy == y2)
                        || (xx == x2 && yy + dy == y2))
                    {
                        func(x2, y2);
                        break;
                    }

                if (profitY >= profitX)
                {
                    xx += dx;
                    nextDiffX += Helper.VoxelSize;
                }

                if (profitY <= profitX)
                {
                    yy += dy;
                    nextDiffY += Helper.VoxelSize;
                }

                if (!func(xx, yy))
                    return;
            }
        }

        static float SIN45 = (float)(Math.Sqrt(2) / 2);

        /// <summary>
        /// проходит по клеткам на отрезке (x1,y1) - (x2,y2) и что-то делает пока func(x,y) == true
        /// координаты в см
        /// </summary>
        public static bool Raycast(Map map, Vector3 position, Vector3 position2, out Vector3 normalCollision,
            bool isFly, bool applyObjects = true, int DELTA_HEIGHT = Helper.DELTA_HEIGH_WALK_PERSON,
            bool airway = false, bool absDeltaH = false, bool shrub = false)
        {
            if (applyObjects)
            {
                foreach (var obj in map.GetAllTargetXNearest(position2.X, PersonBehaviour.PersonRaduis + 0.1f))
                {
                    if ((obj.IsLiveObject && obj.IsDead) || obj.Radius <= 0)
                        continue;

                    if (obj.CanPushByPerson)
                        continue;

                    float radius = obj.Radius + PersonBehaviour.PersonRaduis;

                    if (obj.IsPossibleWalkInside && Vector3.DistanceSquared(position, obj.position) < radius * radius*.9f)
                        continue;

                    float sqrDistance = 0f;
                    if (obj is MapObject)
                    {
                        sqrDistance = Helper.GetSqrDistanceIgnoreY(position2, obj.position);
                    }
                    else
                    {
                        sqrDistance = Vector3.DistanceSquared(position2, obj.position);
                    }

                    if (sqrDistance < radius * radius)
                    {
                        //отдаляться от кустов можно без коллизии
                        if (Vector3.Dot(position2 - position, obj.position - position2) < 0)
                            continue;

                        normalCollision = (obj.position - position);
                        normalCollision.Y = 0f;
                        normalCollision = normalCollision.normalized();
                        return false;
                    }
                }
            }

            float x1cm = position.X;
            float y1cm = position.Z;

            float x2cm = position2.X;
            float y2cm = position2.Z;

            float h = position.Y;

            int x1 = (int)(x1cm / Helper.VoxelSize);
            int y1 = (int)(y1cm / Helper.VoxelSize);

            int x2 = (int)(x2cm / Helper.VoxelSize);
            int y2 = (int)(y2cm / Helper.VoxelSize);

            float absDx = Math.Abs(x2cm - x1cm);
            float absDy = Math.Abs(y2cm - y1cm);

            int dxcell = Math.Sign(x2 - x1);
            int dycell = Math.Sign(y2 - y1);

            int dx = Math.Sign(x2cm - x1cm);
            int dy = Math.Sign(y2cm - y1cm);

            int xx = x1, yy = y1;
            int validX = xx;
            int validY = yy;

            float nextDiffX; // расстояние до следующего пересечения с сеткой двигаясь вдоль X
            float nextDiffY; // вдоль Y
            if (x2cm - x1cm >= 0)
                nextDiffX = (x1 + 1) * Helper.VoxelSize - x1cm;
            else
                nextDiffX = x1cm - x1 * Helper.VoxelSize;

            if (y2cm - y1cm >= 0)
                nextDiffY = (y1 + 1) * Helper.VoxelSize - y1cm;
            else
                nextDiffY = y1cm - y1 * Helper.VoxelSize;

            normalCollision = Vector3.Zero;

            int count = 0;
            bool xDir = false;
            bool yDir = false;
            while (xx != x2 || yy != y2)
            {
                if (++count > 1000)
                    break;

                float profitY = nextDiffY * absDx;
                float profitX = nextDiffX * absDy;

                xDir = false;
                yDir = false;

                if (profitY >= profitX)
                {
                    xx += dxcell;
                    nextDiffX += Helper.VoxelSize;
                    xDir = true;
                }

                if (profitY <= profitX)
                {
                    yy += dycell;
                    nextDiffY += Helper.VoxelSize;
                    yDir = true;
                }

                float nextH = map.GetHeight(xx, yy, true, airway, shrub) * Helper.VoxelSize;
                if (isFly)
                {
                    nextH = Math.Max(nextH, h);
                }

                float currentDeltaH = nextH - h;
                currentDeltaH = absDeltaH ? Math.Abs(currentDeltaH): currentDeltaH;

                if (currentDeltaH > (isFly ? 0f : DELTA_HEIGHT * Helper.VoxelSize) + 0.01f || !map.IsTransparentSimple(xx, yy))
                {
                    //Sos.Debug($"next {nextH} h {h}");
                    normalCollision = new Vector3(xDir ? dxcell : 0, 0, yDir ? dycell : 0).normalized();
                    break;
                    //return false;
                }
                else
                {
                    validX = xx;
                    validY = yy;
                    h = nextH;
                }
            }

            float y_new = map.GetHeight(validX + 1, validY, true, airway, shrub) * Helper.VoxelSize;
            bool canxPlus = (y_new - h) <= DELTA_HEIGHT * Helper.VoxelSize + 0.01f && map.IsTransparentSimple(validX + 1, validY);

            y_new = map.GetHeight(validX - 1, validY, true, airway, shrub) * Helper.VoxelSize;
            bool canxMinus = (y_new - h) <= DELTA_HEIGHT * Helper.VoxelSize + 0.01f && map.IsTransparentSimple(validX - 1, validY);

            y_new = map.GetHeight(validX, validY + 1, true, airway, shrub) * Helper.VoxelSize;
            bool canzPlus = (y_new - h) <= DELTA_HEIGHT * Helper.VoxelSize + 0.01f && map.IsTransparentSimple(validX, validY + 1);

            y_new = map.GetHeight(validX, validY - 1, true, airway, shrub) * Helper.VoxelSize;
            bool canzMinus = (y_new - h) <= DELTA_HEIGHT * Helper.VoxelSize + 0.01f && map.IsTransparentSimple(validX, validY - 1);


            if ((dx>0 || dy>0 ) && !canxPlus && !canzPlus)
            {
                if (ExistIntersection(position.X, position.Z, position2.X, position2.Z, (validX + 1) * Helper.VoxelSize, validY * Helper.VoxelSize, (validX) * Helper.VoxelSize, (validY + 1) * Helper.VoxelSize))
                {
                    normalCollision = new Vector3(SIN45, 0, SIN45);
                    return false;
                }
            }

            if ((dx < 0 || dy < 0) && !canxMinus && !canzMinus)
            {
                if (ExistIntersection(position.X, position.Z, position2.X, position2.Z, (validX + 1) * Helper.VoxelSize, validY * Helper.VoxelSize, (validX) * Helper.VoxelSize, (validY + 1) * Helper.VoxelSize))
                {
                    normalCollision = new Vector3(-SIN45, 0, -SIN45);
                    return false;
                }
            }

            if ((dx > 0 || dy < 0) && !canxPlus && !canzMinus)
            {
                if (ExistIntersection(position.X, position.Z, position2.X, position2.Z, (validX) * Helper.VoxelSize, validY * Helper.VoxelSize, (validX + 1) * Helper.VoxelSize, (validY + 1) * Helper.VoxelSize))
                {
                    normalCollision = new Vector3(-SIN45, 0, +SIN45);
                    return false;
                }
            }

            if ((dx < 0 || dy > 0) && !canxMinus && !canzPlus)
            {
                if (ExistIntersection(position.X, position.Z, position2.X, position2.Z, (validX) * Helper.VoxelSize, validY * Helper.VoxelSize, (validX + 1) * Helper.VoxelSize, (validY + 1) * Helper.VoxelSize))
                {
                    normalCollision = new Vector3(+SIN45, 0, -SIN45);
                    return false;
                }
            }

            return normalCollision == Vector3.Zero;
        }


        //public static bool CheckPoints2(Map map, int x1, int y1, int x2, int y2, out int iEndX, out int iEndY)
        //{
        //    return CheckPoints2(map, x1, y1, x2, y2, TargetType.None, out iEndX, out iEndY);
        //}


#if MultiServer
		/// <summary>
		/// проверяет возможно ли пройти по отрезку из (x1,y1) в (x2,y2) таргету типа type
		/// </summary>
		public static bool CheckPoints2(MapInfo mapInfo, int x1, int y1, int x2, int y2, TargetType type, out int iEndX, out int iEndY)
		{
			try
			{
				MapInfoPointData pointsData = mapInfo.GetMapInfoPointData();

				int iStartCellX = Map.MapCellXCoord(pointsData, x1);
				int iStartCellY = Map.MapCellYCoord(pointsData, y1);

				int iPrevH = Map.GetHeight(pointsData, Map.MapCellXCoord(pointsData, x1), Map.MapCellYCoord(pointsData, y1));
				int endX = x1;
				int endY = y1;
				bool border = false;
				ForEachCells(pointsData.iCellSize, x1, y1, x2, y2, (x, y) =>
				{
					int iNextH = Map.GetHeight(pointsData, x, y);
					//ILogger.Instance.Send(MType.QueryExecution, SType.Info, "SVRH высота x " + x + " y " + y + " h " + iNextH);

					// наша клетка
					if (iStartCellX == x && iStartCellY == y)
						return true;

					if (Math.Abs(iPrevH - iNextH) <= pointsData.iMaxHeightForWalk &&
						(pointsData.iPointState[x - pointsData.iMinX][y - pointsData.iMinY] & Map.CommonTransparentMask) == 0)
					{
						iPrevH = iNextH;
						endX = x * pointsData.iCellSize + pointsData.iCellSize / 2;
						endY = y * pointsData.iCellSize + pointsData.iCellSize / 2;
						return true;
					}

					border = true;
					return false;
				});

				if (border)
				{
					iEndX = endX;
					iEndY = endY;
				}
				else
				{
					iEndX = x2;
					iEndY = y2;
				}
				//ILogger.Instance.Send(MType.QueryExecution, SType.Info, "SVRH раезультат (" + iEndX + "," + iEndY + ")");
				return !border;
			}
			catch (Exception e)
			{
                ILogger.Instance.Send(e);
			}
			iEndX = -1;
			iEndY = -1;
			return false;
		}
#endif

        //      /// <summary>
        ///// проверяет возможно ли пройти по отрезку из (x1,y1) в (x2,y2)
        ///// </summary>
        //public static bool CheckPoints2(Map map, int x1, int y1, int x2, int y2)
        //      {
        //          int iEndX, iEndY;
        //          return CheckPoints2(map, x1, y1, x2, y2, TargetType.None, out iEndX, out iEndY);
        //      }

        /// <summary>
        /// проверяет возможно ли пройти по отрезку из (x1,y1) в (x2,y2) таргету типа type
        /// </summary>
        public static bool CheckPoints2(Map map, float x1, float y1, float x2, float y2, out float iEndX, out float iEndY)
        {
            iEndX = x2;
            iEndY = y2;

            int iStartCellX = Helper.GetCell(x1);// map.MapCellXCoord(x1);
            int iStartCellY = Helper.GetCell(y1);//map.MapCellYCoord(y1);

            if (iStartCellX < 0 || iStartCellY < 0 || iStartCellX >= map.heightX || iStartCellY >= map.heightZ)
                return false;


            int iPrevH = map.GetHeight(iStartCellX, iStartCellY, true);
            float endX = x1;
            float endY = y1;
            bool border = false;
            ForEachCells(x1, y1, x2, y2, (x, y) =>
            {
                int iNextH = map.GetHeight(x, y, true);
                // наша клетка
                if (iStartCellX == x && iStartCellY == y)
                    return true;

                if (!map.IsTransparentOrLiveObject(x, y))
                {
                    border = true;
                    return false;
                }

                if (Math.Abs(iPrevH - iNextH) <= Helper.DELTA_HEIGH_WALK_PERSON)
                {
                    iPrevH = iNextH;
                    endX = x * Helper.VoxelSize + Helper.VoxelSize / 2;
                    endY = y * Helper.VoxelSize + Helper.VoxelSize / 2;
                    return true;
                }

                border = true;
                return false;
            });

            if (border)
            {
                iEndX = endX;
                iEndY = endY;
            }
            else
            {
                iEndX = x2;
                iEndY = y2;
            }

            return !border;
        }

        //private static bool CheckPoints(Map map, int x1, int y1, int x2, int y2)
        //{
        //    return CheckPoints(map, x1, y1, x2, y2, TargetType.None);
        //}

        /// <summary>
        /// проверяет задевает ли отрезок (x1, y1, h1) (x2, y2, h2) рельеф или живые объекты. h = map.GetHeight(x, y) + 150;
        /// </summary>
        public static bool CheckPoints(Map map, int x1, int y1, float h1, int x2, int y2, float h2, out int x, out int y, out float h)
        {
            h = h1;
            x = x1;
            y = y1;
            int absDx = Math.Abs(x2 - x1);
            int absDy = Math.Abs(y2 - y1);
            if (absDx == 0 && absDy == 0)
                return true;

            int sqr_d_full = absDx * absDx + absDy * absDy;
            int xx = x1, yy = y1;
            int nextDiffX = 1;
            int nextDiffY = 1;
            int dx = Math.Sign(x2 - x1);
            int dy = Math.Sign(y2 - y1);

            while (xx != x2 || yy != y2)
            {
                int profitY = nextDiffY * absDx;
                int profitX = nextDiffX * absDy;
                if (profitY >= profitX)
                {
                    xx += dx;
                    nextDiffX += 2;
                }

                if (profitY <= profitX)
                {
                    yy += dy;
                    nextDiffY += 2;
                }

                if (!checkHeight(h1, h2, map.GetHeight(xx, yy, true) * Helper.VoxelSize, sqr_d_full, (yy - y1) * (yy - y1) + (xx - x1) * (xx - x1)))
                {
                    h = h1 + (h2 - h1) * (float)Math.Sqrt(((y - y1) * (y - y1) + (x - x1) * (x - x1)) / (float)sqr_d_full);
                    return false;
                }
                
                x = xx;
                y = yy;
            }

            h = h2;
            x = x2;
            y = y2;
            return true;
        }

        public static Vector3 GetSpeedForKnockBack(Vector3 dir, float time, float dist)
        {
            Vector3 speed = dir.normalized() * dist / time;
            speed.Y = GravityInfluence.ACCELERATION * time / 2;
            return speed;
        }

        public static Vector3 GetSpeedForKnockBack(Vector3 start, float time, Vector3 end, float acceleration = GravityInfluence.ACCELERATION)
        {
            float dy = end.Y - start.Y;
            Vector3 speed = (end - start) / time;
            speed.Y = dy / time + acceleration * time / 2;
            return speed;
        }

        public static void CheckPointsParabola(Map map, Vector3 startPoint, Vector3 speed, out Vector3 endPoint, out float flyTime, float accuracy, bool checkTransparency = false, float acceleration = GravityInfluence.ACCELERATION)
        {
            endPoint = startPoint;
            for (flyTime = 0; flyTime < 10; flyTime += accuracy)
            {
                //MapEffect effect = new MapEffect(map) { position = pos, fxId = Enums.EffectType.Mortar };
                //ServerRpcListener.Instance.SendCreateFxResponse(effect);

                //speed.Y -= GravityInfluence.ACCELERATION * accuracy;
                //pos += speed * accuracy;

                Vector3 pos = startPoint + speed * flyTime;
                if (checkTransparency && !map.IsTransparentSimple(Helper.GetCell(pos.X), Helper.GetCell(pos.Z)))
                    break;

                pos.Y = startPoint.Y + speed.Y * flyTime - acceleration * flyTime * flyTime / 2f;

                if (pos.Y < Helper.UltraCorrectYPosition(map, pos).Y)
                    break;

                endPoint = pos;
            }
        }

        public static bool SpecialRaycast(Map map, Vector3 position, Vector3 position2, float radius)
        {
            int distance = Math.Max(1, Helper.GetCell(Math.Abs(position.X - position2.X) + Math.Abs(position.Z - position2.Z)));
            Vector3 direction = (position2 - position) / distance;
            Vector3 currentPos = Vector3.Zero;
            for (int i = 0; i <= distance; i++)
            {
                currentPos = position + direction * i;
                float sqreDistance = (currentPos - position2).sqrMagnitude();
                if (sqreDistance < radius * radius)
                    return true;

				var h = map.GetHeightFloat(currentPos.X, currentPos.Z, true) + map.GetShrubHeightFloat(currentPos.X, currentPos.Z);
				if (h > currentPos.Y)
                    return false;
            }
            return true;
        }

        /// <summary>
        /// проверяет задевает ли отрезок (x1, y1, h1) (x2, y2, h2) рельеф или живые объекты. h = map.GetHeight(x, y) + 150;
        /// </summary>
        public static bool CheckPoints(Map map, int x1, int y1, int x2, int y2, bool shrub = false)
        {
            int absDx = Math.Abs(x2 - x1);
            int absDy = Math.Abs(y2 - y1);
            if (absDx == 0 && absDy == 0)
                return true;
            int xx = x1, yy = y1;
            int nextDiffX = 1;
            int nextDiffY = 1;
            int dx = Math.Sign(x2 - x1);
            int dy = Math.Sign(y2 - y1);

            int iPrevH = map.GetHeight(xx, yy, true, shrubHeight: shrub);
            while (xx != x2 || yy != y2)
            {
                int profitY = nextDiffY * absDx;
                int profitX = nextDiffX * absDy;
                //проверка прохода по диагонали если соседние клетки недостижимы то и клетка по диагонали тоже
                if (profitX == profitY)
                {
                    int height1 = map.GetHeight(xx, yy+ dy, true, shrubHeight: shrub);
                    int height2 = map.GetHeight(xx+ dx, yy, true, shrubHeight: shrub);
                    if (Math.Abs(iPrevH - height1) > Helper.DELTA_HEIGH_WALK_PERSON && Math.Abs(iPrevH - height2) > Helper.DELTA_HEIGH_WALK_PERSON)
                    {
                        return false;
                    }

                }
                if (profitY >= profitX)
                {
                    xx += dx;
                    nextDiffX += 2;
                }

                if (profitY <= profitX)
                {
                    yy += dy;
                    nextDiffY += 2;
                }
                int iNextH = map.GetHeight(xx, yy, true, shrubHeight: shrub);
                if (Math.Abs(iPrevH - iNextH) > Helper.DELTA_HEIGH_WALK_PERSON)
                    return false;

                iPrevH = iNextH;
            }
            return true;
        }

        //только для проверок высоких препядствий на пути снарядов, работает очень не точно
        public static bool CheckPointsInaccurate(Map map, int x1, int y1, float h1, int x2, int y2, float h2, bool checkShrub = false)
        {
            float hShift = .5f;
            int absDx = Math.Abs(x2 - x1);
            int absDy = Math.Abs(y2 - y1);
            if (absDx == 0 && absDy == 0)
                return true;
            float dist = absDx + absDy;
            float currentDist = 0;
            float h = h2 - h1;
            int xx = x1, yy = y1;
            int nextDiffX = 1;
            int nextDiffY = 1;
            int dx = Math.Sign(x2 - x1);
            int dy = Math.Sign(y2 - y1);

            while (xx != x2 || yy != y2)
            {
                int profitY = nextDiffY * absDx;
                int profitX = nextDiffX * absDy;
                //проверка прохода по диагонали если соседние клетки недостижимы то и клетка по диагонали тоже
                if (profitX == profitY)
                {
                    float height1 = map.GetHeight(xx, yy + dy, true, shrubHeight: checkShrub) * Helper.VoxelSize;
                    float height2 = map.GetHeight(xx + dx, yy, true, shrubHeight: checkShrub) * Helper.VoxelSize;
                    float hnext = (currentDist + 2) / dist * h + h1;
                    if (height1 - hShift > hnext && height2 - hShift > hnext)
                    {
                        return false;
                    }

                }
                if (profitY >= profitX)
                {
                    xx += dx;
                    nextDiffX += 2;
                    currentDist++;
                }

                if (profitY <= profitX)
                {
                    yy += dy;
                    nextDiffY += 2;
                    currentDist++;
                }
                float iNextH = map.GetHeight(xx, yy, true, shrubHeight: checkShrub) * Helper.VoxelSize - hShift;

                if (iNextH>currentDist/dist*h+h1)
                    return false;
            }
            return true;
        }
    }

public struct ServerVectorInt
    {
        public int x, y;
        public ServerVectorInt(int ix, int iy)
        {
            x = ix;
            y = iy;
        }

        public static ServerVectorInt operator +(ServerVectorInt v1, ServerVectorInt v2)
        {
            return new ServerVectorInt(v1.x + v2.x, v1.y + v2.y);
        }

        public static ServerVectorInt operator -(ServerVectorInt v1, ServerVectorInt v2)
        {
            return new ServerVectorInt(v1.x - v2.x, v1.y - v2.y);
        }

        public static ServerVectorInt operator -(ServerVectorInt v1)
        {
            return new ServerVectorInt(-v1.x, -v1.y);
        }

        public static ServerVectorInt operator *(ServerVectorInt v1, int b)
        {
            return new ServerVectorInt(v1.x * b, v1.y * b);
        }

        public static ServerVectorInt operator *(int b, ServerVectorInt v1)
        {
            return new ServerVectorInt(v1.x * b, v1.y * b);
        }

        public static ServerVectorInt operator /(ServerVectorInt v1, int b)
        {
            return new ServerVectorInt(v1.x / b, v1.y / b);
        }

        public static implicit operator ServerVectorDouble(ServerVectorInt v)
        {
            return new ServerVectorDouble(v.x, v.y);
        }

        public int SqrLength
        {
            get
            {
                return x * x + y * y;
            }
        }

        public double Length
        {
            get
            {
                return Math.Sqrt(x * x + y * y);
            }
        }

        /// <summary>
        /// приводим к единичной длине
        /// </summary>
        public ServerVectorDouble Normal
        {
            get
            {
                double len = Length;
                return new ServerVectorDouble(x / len, y / len);
            }
        }

        public static ServerVectorInt Parse(string str)
        {
            str = str.Replace(" ", "");
            Regex regexObj = new Regex(@"\((?<x>.*),(?<y>.*)\)");
            Match matchResults = regexObj.Match(str);
            var point = new ServerVectorInt(int.Parse(matchResults.Groups["x"].Value),
                                            int.Parse(matchResults.Groups["y"].Value));
            return point;
        }
    }

    public struct ServerVectorDouble
    {
        public static ServerVectorDouble Zero = new ServerVectorDouble(0, 0);

        public double x, y;
        public ServerVectorDouble(double ix, double iy)
        {
            x = ix;
            y = iy;
        }

        public static ServerVectorDouble operator +(ServerVectorDouble v1, ServerVectorDouble v2)
        {
            return new ServerVectorDouble(v1.x + v2.x, v1.y + v2.y);
        }

        public static ServerVectorDouble operator +(ServerVectorInt v1, ServerVectorDouble v2)
        {
            return new ServerVectorDouble(v1.x + v2.x, v1.y + v2.y);
        }

        public static ServerVectorDouble operator +(ServerVectorDouble v1, ServerVectorInt v2)
        {
            return new ServerVectorDouble(v1.x + v2.x, v1.y + v2.y);
        }

        public static ServerVectorDouble operator -(ServerVectorDouble v1, ServerVectorDouble v2)
        {
            return new ServerVectorDouble(v1.x - v2.x, v1.y - v2.y);
        }

        public static ServerVectorDouble operator -(ServerVectorInt v1, ServerVectorDouble v2)
        {
            return new ServerVectorDouble(v1.x - v2.x, v1.y - v2.y);
        }

        public static ServerVectorDouble operator -(ServerVectorDouble v1, ServerVectorInt v2)
        {
            return new ServerVectorDouble(v1.x - v2.x, v1.y - v2.y);
        }

        public static ServerVectorDouble operator -(ServerVectorDouble v1)
        {
            return new ServerVectorDouble(-v1.x, -v1.y);
        }

        public static ServerVectorDouble operator *(ServerVectorDouble v1, int b)
        {
            return new ServerVectorDouble(v1.x * b, v1.y * b);
        }

        public static ServerVectorDouble operator *(ServerVectorDouble v1, double b)
        {
            return new ServerVectorDouble(v1.x * b, v1.y * b);
        }

        public static ServerVectorDouble operator *(int b, ServerVectorDouble v1)
        {
            return new ServerVectorDouble(v1.x * b, v1.y * b);
        }

        public static ServerVectorDouble operator /(ServerVectorDouble v1, int b)
        {
            return new ServerVectorDouble(v1.x / b, v1.y / b);
        }

        public static ServerVectorDouble operator /(ServerVectorDouble v1, double b)
        {
            return new ServerVectorDouble(v1.x / b, v1.y / b);
        }

        public static bool operator ==(ServerVectorDouble v1, ServerVectorDouble v2)
        {
            return Math.Abs(v1.x - v2.x) <= Double.Epsilon && Math.Abs(v1.y - v2.y) <= Double.Epsilon;
        }

        public static bool operator !=(ServerVectorDouble v1, ServerVectorDouble v2)
        {
            return Math.Abs(v1.x - v2.x) > Double.Epsilon || Math.Abs(v1.y - v2.y) > Double.Epsilon;
        }

        public static explicit operator ServerVectorInt(ServerVectorDouble v)
        {
            return new ServerVectorInt((int)v.x, (int)v.y);
        }

        public double SqrLength
        {
            get
            {
                return x * x + y * y;
            }
        }

        public double Length
        {
            get
            {
                return Math.Sqrt(x * x + y * y);
            }
        }

        /// <summary>
        /// приводим к единичной длине
        /// </summary>
        public ServerVectorDouble Normal
        {
            get
            {
                double len = Length;
                return new ServerVectorDouble(x / len, y / len);
            }
        }

        public void Normalize()
        {
            double len = Length;
            x = x / len;
            y = y / len;
        }

        public void Normalize(double resLen)
        {
            double len = Length;
            x = x * resLen / len;
            y = y * resLen / len;
        }

        public bool Equals(ServerVectorDouble other)
        {
            return other.x.Equals(x) && other.y.Equals(y);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (obj.GetType() != typeof(ServerVectorDouble)) return false;
            return Equals((ServerVectorDouble)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (x.GetHashCode() * 397) ^ y.GetHashCode();
            }
        }
    }

    public struct LineArea : IArea
    {
        private readonly Vector2 Orientation;
        private readonly Vector2 Center;
        private readonly double Width;
        private readonly double Length;
        private readonly bool addTargetRadius;

        public LineArea(Vector2 c1, double orientation, double width, double len, bool addTargetRadius = false)
        {
            Orientation = new Vector2((float)Math.Cos(orientation), (float)Math.Sin(orientation));
            Center = c1;
            Width = width;
            Length = len;
            this.addTargetRadius = addTargetRadius;
        }

        public LineArea(Vector2 c1, Vector2 orientation, double width, double len, bool addTargetRadius = false)
        {
            Orientation = orientation;// new Vector2((float)Math.Cos(orientation), (float)Math.Sin(orientation));
            Center = c1;
            Width = width;
            Length = len;
            this.addTargetRadius = addTargetRadius;
        }

		public bool InArea(MapTargetObject t)
		{
			return InArea(t.center, t.Radius);
		}

        public bool InArea(Vector3 coord, float Radius = 0)
        {
            Vector2 sub = new Vector2(coord.X, coord.Z) - Center;

            //длина проекции на прямую
            var len = GeometryAlgorithm.ScalarMult(Orientation, sub);
            //отклонение от прямой
            var y = GeometryAlgorithm.VectorMultMagnitude(Orientation, sub);

            if (!addTargetRadius)
                return Math.Abs(y) * 2 <= Width && len >= 0 && len <= Length;
            else
                return (Math.Abs(y) - Radius) * 2 <= Width && len >= 0 && len - Radius <= Length;
        }
    }

    /// <summary>
    /// задает пространство ограниченное многоугольником. Точки задавать в порядке обхода
    /// </summary>
    public struct PolygonArea : IArea
    {
        readonly List<Vector2> points;
        readonly float maxY;
        readonly float maxX;
        readonly float minY;
        readonly float minX;

        public PolygonArea(List<Vector2> Points)
        {
            if (Points.Count < 3)
                throw new ArgumentException();

            points = Points;
            var p0 = points[0];
            float imaxY = p0.Y;
            float imaxX = p0.X;
            float iminY = p0.Y;
            float iminX = p0.X;

            for (int i = 0; i < points.Count; i++)
            {
                var p = points[i];

                if (imaxY < p.Y) imaxY = p.Y;
                if (imaxX < p.X) imaxX = p.X;
                if (iminY > p.Y) iminY = p.Y;
                if (iminX > p.X) iminX = p.X;
            }

            maxX = imaxX;
            maxY = imaxY;
            minX = iminX;
            minY = iminY;
        }

        public bool InArea(Vector3 Position, float Radious=0)
        {
            var coord = GeometryAlgorithm.Vector3ToVector2(Position);

            if (coord.X > maxX || coord.X < minX || coord.Y > maxY || coord.Y < minY)
                return false;

            
            //луч из coord в любом направлении пересекает границу нечетное количество раз => мы внутри полигона
            //                                                     четное количество раз => мы снаружи полигона

            // пусть для определенности луч идет вдоль Y
            float subY = maxY - minY;
            int countIntersection = 0;
            for (int i = 0; i < points.Count; i++)
            {
                var p1 = points[i];
                var p2 = points[(i + 1) % points.Count];
                if (GeometryAlgorithm.ExistIntersection(coord.X, coord.Y, coord.X, coord.Y + subY,
                    p1.X, p1.Y, p2.X, p2.Y))
                    countIntersection++;
            }
            return countIntersection % 2 != 0;
        }

        public bool InArea(MapTargetObject v)
        {
            return InArea(v.position);
        }
    }

    public class RadiusAreaRef : IArea
    {
        readonly float radius;
        readonly bool addTargetRadius;
        public float centerX { get; set; }
        public float centerZ { get; set; }

        public RadiusAreaRef(Vector3 centerObj, float radiusCm, bool _addTargetRadius = false)
        {
            radius = radiusCm;
            centerX = centerObj.X;
            centerZ = centerObj.Z;
            addTargetRadius = _addTargetRadius;
        }

        public bool InArea(MapTargetObject t)
        {
            return InArea(t.center, t.Radius);
        }

        public bool InArea(Vector3 coord, float Radius = 0)
        {
            if (addTargetRadius)
                return GeometryAlgorithm.InRadius(radius + Radius, centerX, centerZ, coord.X, coord.Z);
            else
                return GeometryAlgorithm.InRadius(radius, centerX, centerZ, coord.X, coord.Z);
        }
    }

    public struct RadiusArea : IArea
	{
		readonly float radius;
	    readonly bool addTargetRadius;
	    readonly float centerX;
	    readonly float centerY;

		public RadiusArea(float iXCoord, float iYCoord, float radiusCm)
		{
			radius = radiusCm;
			centerX = iXCoord;
			centerY = iYCoord;
			addTargetRadius = true;
		}

		public RadiusArea(Vector3 centerObj, float radiusCm, bool _addTargetRadius = false)
		{
			radius = radiusCm;
			centerX = centerObj.X;
			centerY = centerObj.Z;
			addTargetRadius = _addTargetRadius;
		}

		public RadiusArea(MapTargetObject centerObj, float radiusCm)
		{
			radius = centerObj.Radius + radiusCm;
			var coord = centerObj.position;
			centerX = coord.X;
			centerY = coord.Z;
			addTargetRadius = true;
		}

		public bool InArea(MapTargetObject t)
		{
            return InArea(t.center, t.Radius);
        }

		public bool InArea(Vector3 coord, float Radius = 0)
		{
			if (addTargetRadius)
				return GeometryAlgorithm.InRadius(radius + Radius, centerX, centerY, coord.X, coord.Z);
			else
				return GeometryAlgorithm.InRadius(radius, centerX, centerY, coord.X, coord.Z);
		}
	}

    public interface IArea
    {
        bool InArea(MapTargetObject v);
		bool InArea(Vector3 coord, float Radius = 0);
	}

    /// <summary>
    /// Внимание! не более 90 градусов в каждую сторону
    /// </summary>
    public struct SectorArea : IArea
    {
        readonly double m_kSinAngle;
        readonly double m_kCosAngle;
        readonly Vector2 orientation;
        readonly float centerX;
        readonly float centerY;
        readonly float radius;
        readonly bool addTargetRadius;

        public SectorArea(Vector3 c, Vector2 orient, int angle, float rad, bool addTargetRadius = false)
        {
            m_kCosAngle = Math.Cos(Math.PI * angle / 180);
            m_kSinAngle = Math.Sin(Math.PI * angle / 180);
            centerX = c.X;
            centerY = c.Z;
            orientation = orient;//считаем нормализованной
            radius = rad;
			this.addTargetRadius = addTargetRadius;

            if (m_kCosAngle < 0)//запретим углы больше 90 
                throw new NotImplementedException();
        }

		public bool InArea(MapTargetObject t)
		{
            return InArea(t.center, t.Radius);
        }

		public bool InArea(Vector3 coord, float Radius = 0)
        {
            Vector2 v = new Vector2(coord.X - centerX, coord.Z - centerY);
            //черная магия
            //координаты цели в системе с базисным вестором orientation всместо ости x
            double y = GeometryAlgorithm.VectorMultMagnitude(v, orientation); 
            double x = GeometryAlgorithm.ScalarMult(v, orientation);
            //представим грани сектора в виде неравенства |y| <= |kx + a| и все станет ясно
            if (addTargetRadius)
            {
                return (x >= -Radius && Math.Abs(x * m_kSinAngle + Radius) >= Math.Abs(y * m_kCosAngle)
                && GeometryAlgorithm.InRadius( radius + Radius, coord, centerX, centerY));
            }
            else
            {
                return (x >= 0 && Math.Abs(x * m_kSinAngle) >= Math.Abs(y * m_kCosAngle)
                && GeometryAlgorithm.InRadius( radius, coord, centerX, centerY));
            }
        }
    }

    ///// <summary>
    ///// позволяет отсортировать таргетов в соответствии с их расположением в секторе персонажа. те что ближе к центру сектора и к персонажу в начало списка
    ///// </summary>
    //   public struct SectorComparator : IComparer<IAttacker>
    //{
    //	/// в системе координат где Orientation направленно вдоль y и персонаж стоит в координате (0, 0) 
    //	/// ставим каждому таргету в соответствие оценку равную
    //	/// A = |x| + 0.5*|y-100| 
    //	/// при сравнении таргетов сравниваем их оценки.
    //	/// В отсоритированном списке первым будет элемент с меньшей оценкой.
    //	/// 
    //	/// x = [Orientation , targetCoord1]  векторное произведение
    //	/// y = (Orientation , targetCoord1)  скалярное произведение
    //	/// Orientation -- нормаированный вектор

    //	public SectorComparator(Person p)
    //	{
    //		Orientation = new ServerVectorDouble(Math.Cos(p.OrientationNew), Math.Sin(p.OrientationNew));
    //		persCoord = (ServerVectorInt)p.GetCoordinates();
    //	}

    //	public SectorComparator(ServerVectorDouble orient, ServerVectorInt CenterCoord)
    //	{
    //		Orientation = orient.Normal;
    //		persCoord = CenterCoord;
    //	}

    //	readonly ServerVectorDouble Orientation;
    //	readonly ServerVectorInt persCoord;
    //       public int Compare(IAttacker t1, IAttacker t2)
    //       {
    //           var vectorT1 = (ServerVectorInt)t1.GetCoordinates() - persCoord;
    //           var vectorT2 = (ServerVectorInt)t2.GetCoordinates() - persCoord;

    //           double VectorMult1 = GeometryAlgorithm.VectorMultMagnitude(Orientation, vectorT1);
    //           double VectorMult2 = GeometryAlgorithm.VectorMultMagnitude(Orientation, vectorT2);

    //           double ScalarMult1 = GeometryAlgorithm.ScalarMult(Orientation, vectorT1);
    //           double ScalarMult2 = GeometryAlgorithm.ScalarMult(Orientation, vectorT2);

    //           double A1 = Math.Abs(VectorMult1) + 0.5 * Math.Abs(ScalarMult1 - 100);
    //           double A2 = Math.Abs(VectorMult2) + 0.5 * Math.Abs(ScalarMult2 - 100);
    //           return (int)(A1 - A2);
    //       }
    //}

    ///// <summary>
    ///// позволяет отсортировать таргетов в соответствии с расстоянием
    ///// </summary>
    //   public struct RadiusComparator : IComparer<IAttacker>
    //{
    //	public RadiusComparator(ITarget p)
    //	{
    //		var persCoord = p.GetCoordinates();
    //		centerX = persCoord.iXCoord;
    //		centerY = persCoord.iYCoord;
    //	}

    //	public RadiusComparator(int x, int y)
    //	{
    //		centerX = x;
    //		centerY = y;
    //	}

    //	readonly int centerX;
    //	readonly int centerY;

    //       public int Compare(IAttacker t1, IAttacker t2)
    //	{
    //		Coordinates coord1 = t1.GetCoordinates();
    //		Coordinates coord2 = t2.GetCoordinates();
    //		long d1 = GeometryAlgorithm.DistanceInSquare(centerX, centerY, coord1.iXCoord, coord1.iYCoord);
    //		long d2 = GeometryAlgorithm.DistanceInSquare(centerX, centerY, coord2.iXCoord, coord2.iYCoord);
    //		return Math.Sign(d1 - d2);
    //	}
    //}
}
