﻿using System;
using System.Numerics;
using System.Text.RegularExpressions;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.Utils.Logger;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.Utils.Common;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic;

/// <summary>
/// Summary description for Path.
/// </summary>
public class MapPathArray
{
	public static int iMaxSize = 100;
	public static int iMaxSizeBig = 1000;

    public int max_size { get; private set; }
    public bool overlapped;

    public MapPathArray()
    {
	    max_size = iMaxSize;
        Init();
    }

    public MapPathArray(int maxSize)
    {
		max_size = maxSize;
        Init();
    }

    private void Init()
    {
        points = new float[max_size];
        lens = new float[max_size / 2 + 1];
        size = 0;//в количестве чисел...
    }

    public int PathLenght
    {
        get
        {
            return size / 2;
        }
    }

    public void Clear()
    {
        size = 0;
        Ended = true;
        StopForce = true;
    }

    public double Distance(float x1, float y1, float x2, float y2)
    {
        return Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    public void AddPoint(float ix, float iy)
    {
        if (size == max_size)
            //просто не будет таких длинных путей
            return;

        if (size != 0)
        {
            float dx = points[size - 2] - ix;
            float dy = points[size - 1] - iy;
            lens[size / 2 - 1 ] = (float)Math.Sqrt(dx * dx + dy * dy);
        }

        points[size++] = ix;
        points[size++] = iy;
    }

    public void RemoveLastPoints(int count)
    {
        size = Math.Max(0, size - 2 * count);
    }

    public Vector2 GetPointByIndex(int index)
    {
        float x = points[index * 2];
        float y = points[index * 2 + 1];
        return new Vector2(x, y);
    }

    public void GetPointByIndex(int index, out float x, out float y)
    {
        x = points[index * 2];
        y = points[index * 2 + 1];
    }

    public void GetLastPoint(out float x, out float y)
    {
        x = points[size - 2];
        y = points[size - 1];
    }

    public float GetPointXByIndex(int index)
    {
        return points[index * 2];
    }

    public float GetPointYByIndex(int index)
    {
        return points[index * 2 + 1];
    }

    public int size;
    public float[] points;
    public int step;

    public int currentPointIndex = 0;
    private float[] lens;

    public DateTime dtLastRecalc;
    public DateTime dtStart;

    public void Start(DateTime lastRecalc)
    {
        
        currentPointIndex = 0;
        dtStart = dtLastRecalc = lastRecalc;
        m_iCoordX = points[0];
        m_iCoordY = points[1];
        if (size == 0)
            Ended = true;
        else
            Ended = false;
        StopForce = false;
        //Sos.Debug("создали новый путь " + ToString());
    }
    public void Start()
    {
        Start(TimeProvider.UTCNow);
    }

    ////сейчас будет последнее движение или еще нет?
    public bool LastMove(DateTime now, int iCooldown)
    {
        if (now == dtLastRecalc)
            return false;

        if (size == 0)
            return false;

        double timeSec = (now - dtLastRecalc).TotalMilliseconds;
        double length = timeSec / iCooldown;

        return (size / 2 - 1 - currentPointIndex) * Helper.VoxelSize < length;
    }

    public bool Ended = true;
    public bool StopForce = true;

    public bool IsEnded()
    {
        return Ended && StopForce;
    }

    /// <summary>
    /// давно не пересчитывался
    /// </summary>
    public bool Old
    {
        get
        {
            return SecondsPassed > 1;
        }
    }

    public double SecondsPassed
    {
        get
        {
            return (TimeProvider.UTCNow - dtStart).TotalSeconds;
        }
    }

    public Vector2 CurentForce;


    public float ScalarMult(float Ax, float Ay, float Cx, float Cy)
    {
        return Ax * Cx + Ay * Cy;
    }

    public float VectorMultMagnitude(float Ax, float Ay, float Cx, float Cy)
    {
        return Ax * Cy - Ay * Cx;
    }

    public void GetDistanceForvard(Vector2 point, int startIndex, out float distanceToLine, out float distanceToStart)
    {
        float bestPointX, bestPointY, postPointX, postPointY;
        GetPointByIndex(startIndex, out bestPointX, out bestPointY);
        GetPointByIndex(startIndex + 1, out postPointX, out postPointY);

        float dx = bestPointX - postPointX;
        float dy = bestPointY - postPointY;

        distanceToStart = ScalarMult(dx, dy, bestPointX - point.X, bestPointY - point.Y) / lens[startIndex];
        distanceToLine = VectorMultMagnitude(dx, dy, bestPointX - point.X, bestPointY - point.Y) / lens[startIndex];
    }

    public void GetDistanceBackvard(Vector2 point, int startIndex, out float distanceToLine, out float distanceToStart)
    {
        float bestPointX, bestPointY, prevPointX, prevPointY;
        GetPointByIndex(startIndex, out bestPointX, out bestPointY);
        GetPointByIndex(startIndex - 1, out prevPointX, out prevPointY);

        float dx = bestPointX - prevPointX;
        float dy = bestPointY - prevPointY;

        distanceToStart = ScalarMult(dx, dy, bestPointX - point.X, bestPointY - point.Y) / lens[startIndex - 1];
        distanceToLine = VectorMultMagnitude(dx, dy, bestPointX - point.X, bestPointY - point.Y) / lens[startIndex - 1];
    }

    public Vector2 GetForce(Vector2 point)
    {
        if (PathLenght <= 1)
        {
            Ended = true;
            return Vector2.Zero;
        }

        //это полный перебор, он правильный но медленный. не стирать его
        //Vector2 bestPoint = points[currentPointIndex];
        //float sqDist = (point - bestPoint).sqrMagnitude;
        //int bestIndex = currentPointIndex;
        //for (int i = 0; i < points.Count; i++)
        //{
        //    var p = points[i];
        //    float sq = (point - p).sqrMagnitude;
        //    if (sq < sqDist)
        //    {
        //        bestPoint = p;
        //        sqDist = sq;
        //        bestIndex = i;
        //    }
        //}

        //это перебор до ближайшего минимума
        int startPoint = Math.Max(0, currentPointIndex - 1);
        Vector2 bestPoint = GetPointByIndex(startPoint);
        float sqDist = (point - bestPoint).sqrMagnitude();
        int bestIndex = startPoint;
        for (int i = startPoint + 1; i < size/2; i++)
        {
            var p = GetPointByIndex(i);
            float sq = (point - p).sqrMagnitude();
            if (sq < sqDist)
            {
                bestPoint = p;
                sqDist = sq;
                bestIndex = i;
            }
            else
                break;
        }

        currentPointIndex = bestIndex;

        if (sqDist > 1)
        {
            //Sos.Debug("отошли от пути больше чем на 1 метр");
            Ended = true;
            return Vector2.Zero;
        }

        float dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;

        float distanceToLine1 = 0;
        float distanceToStart1 = 0;
        if (bestIndex != size / 2 - 1)
        {
            var nextPoint = GetPointByIndex(bestIndex + 1);//points[bestIndex + 1];
            dx1 = nextPoint.X - bestPoint.X;
            dy1 = nextPoint.Y - bestPoint.Y;
            GetDistanceForvard(point, bestIndex, out distanceToLine1, out distanceToStart1);
        }


        float distanceToLine2 = 0;
        float distanceToStart2 = 0;
        if (bestIndex != 0)
        {
            var prevPoint = GetPointByIndex(bestIndex - 1);//points[bestIndex - 1];
            dx2 = bestPoint.X - prevPoint.X;
            dy2 = bestPoint.Y - prevPoint.Y;
            GetDistanceBackvard(point, bestIndex, out distanceToLine2, out distanceToStart2);
            distanceToLine2 = -distanceToLine2;
        }

        //мы на конце пути
        if (bestIndex == size / 2 - 1)
        {
            if (distanceToStart2 < 0)
            {
                //путь закончился
                //Sos.Debug("путь закончился");
                Ended = true;
                return Vector2.Zero;
            }
            return new Vector2(dx2, dy2).normalized();
            //return new Vector2(dx1 - dy1 * distanceToLine1, dy1 + dx1 * distanceToLine1).normalized();
        }

        //мы на начале пути
        if (bestIndex == 0)
        {
            return new Vector2(dx1, dy1).normalized();
            //return new Vector2(dx1 - dy1 * distanceToLine1, dy1 + dx1 * distanceToLine1).normalized();
        }

        if (distanceToStart1 > distanceToStart2)
        {
            //прибавки dy1 * distanceToLine1, чтобы сила стремилась вывести на линию а не просто вела параллельно
            return new Vector2(dx1 + dy1 * distanceToLine1, dy1 - dx1 * distanceToLine1).normalized();
        }
        else
        {
            return new Vector2(dx2 + dy2 * distanceToLine2, dy2 - dx2 * distanceToLine2).normalized();
        }
    }

    public void GetEndPoint(out float x, out float y)
    {
        if (size == 0)
            throw new IndexOutOfRangeException();

        x = points[size - 2];
        y = points[size - 1];
    }

    public void SetForce(Vector3 point3, DateTime now, int iCooldown)
    {
        try
        {
            if (now <= dtLastRecalc)
                return;

            var point = GeometryAlgorithm.Vector3ToVector2(point3);

            float timeSec = (float)(now - dtLastRecalc).TotalMilliseconds;
            float length = timeSec / iCooldown;

            CurentForce = GetForce(point) * length;

            dtLastRecalc = now;
        }
        catch(Exception ex)
        {
            CurentForce = Vector2.Zero;
            ILogger.Instance.Send(ex.ToString(), ErrorLevel.exception);
            ILogger.Instance.Send("MapPath.SetForce() Exception Path: " + ToString() + " point: " + point3 + " currentPointIndex: " + currentPointIndex);
        }
    }

    //движение по рельсам. Пока оставить это метод, может вернемся к нему
    //public void Moving(DateTime now, double length, MonsterBehaviour monster)
    //{
    //    //Sos.Debug("length1 " + length);
    //    //var l1 = length;
    //    if (RealLenght - positionSlider > MapPath.LEAD_LENGTH && RealLenght - (positionSlider + length) < MapPath.LEAD_LENGTH)
    //    {
    //        //Sos.Debug("сколько осталось" + (RealLenght - positionSlider));// сколько осталось
    //        //Sos.Debug("сколько останется" + (RealLenght - (positionSlider + length)));//RealLenght - (positionSlider + length) сколько останется
    //        //Sos.Debug("на сколько мы пройдем дельше чем надо" + ((positionSlider + length)- (RealLenght - MapPath.LEAD_LENGTH)));//(RealLenght - MapPath.LEAD_LENGTH) - (positionSlider + length) на сколько мы пройдем дельше чем надо
    //        length -= ((positionSlider + length) - (RealLenght - MapPath.LEAD_LENGTH)) / 2;//(RealLenght - positionSlider)/2 - length / 2
    //    }
    //    else if (RealLenght - positionSlider < MapPath.LEAD_LENGTH)
    //        length = length / 2;

    //    //if (monster == null)
    //    //    Sos.Debug("length " + l1 + "  " + length + " monster == null");
    //    //else
    //    //    Sos.Debug("length " + l1 + "  " + length + " monster != null");

    //    if (size == 0)
    //        return;

    //    if (Ended)
    //        return;

    //    dtLastRecalc = now;
    //    positionSlider += length;

    //    if (monster != null)
    //        CurentForce = GetForce(monster);

    //    //Sos.Debug("positionSlider = " + positionSlider);
    //    //Sos.Debug("lens[size / 2 - 1] = " + lens[size / 2 - 1]);

    //    //Sos.Debug("positionSlider " + positionSlider);
    //    if (positionSlider > lens[size / 2 - 1])
    //    {
    //        m_iCoordX = points[size - 2];
    //        m_iCoordY = points[size - 1];
    //        step = PathLenght;
    //        Ended = true;
    //        //Sos.Debug("Ended = true;");
    //        return;
    //    }

    //    int i;//номер отрезка на котором мы находимся
    //    for (i = 1; i < size - 1; i++)
    //    {
    //        if (lens[i] > positionSlider)
    //            break;
    //    }


    //    double rest = positionSlider - lens[i - 1];//остаток пути до следующей точки
    //    double segmentLen = lens[i] - lens[i - 1];

    //    float ix = points[(i - 1) * 2];
    //    float iy = points[(i - 1) * 2 + 1];
    //    float ix_plus_1 = points[i * 2];
    //    float iy_plus_1 = points[i * 2 + 1];
    //    float dx = ix_plus_1 - ix;
    //    float dy = iy_plus_1 - iy;

    //    m_iCoordX = ix + (float)(dx * rest / segmentLen);
    //    m_iCoordY = iy + (float)(dy * rest / segmentLen);

    //    step = i;
    //}

    /// <summary>
    /// сгладить путь
    /// </summary>
    public void Smooth()
    {
        if (PathLenght <= 2)
            return;

        float xPrev, yPrev, xCur, yCur, xNext = 0, yNext = 0;
        GetPointByIndex(0, out xPrev, out yPrev);
        GetPointByIndex(1, out xCur, out yCur);

        int len = PathLenght;
        size = 0;//сбрасываем путь. соберем его заново

        AddPoint(xPrev, yPrev);
        for (int i = 2; i < len; i++)
        {
            GetPointByIndex(i, out xNext, out yNext);

            float xRound = (2 * xCur + xPrev + xNext) / 4;
            float yRound = (2 * yCur + yPrev + yNext) / 4;

            AddPoint(xRound, yRound);

            xPrev = xCur;
            yPrev = yCur;

            xCur = xNext;
            yCur = yNext;
        }

        AddPoint(xNext, yNext);
    }

    private float m_iCoordX;
    public float iCoordX
    {
        get
        {
            return m_iCoordX;
        }
    }

    private float m_iCoordY;
    public float iCoordY
    {
        get
        {
            return m_iCoordY;
        }
    }

    public override string ToString()
    {
        if (size == 0)
            return "Empty";

        int len = PathLenght;

        string str = " steplen = " + len;
        for (int i = 0; i < len; i++)
            str += "p[" + i + "]={" + points[i * 2] + "," + points[i * 2 + 1] + "}, ";

        return str;
    }

    public void CopyFrom(MapPathArray p)
    {
        size = p.size;
        step = p.step;
        for (int i = 0; i < size; i++)
        {
            points[i] = p.points[i];
        }

        for (int j = 0; j < size / 2; j++)
            lens[j] = p.lens[j];
    }

    static Regex regexObj = new Regex(@"\((-?\d+),(-?\d+)\)+");
    public static MapPathArray Parse(string str)
    {
        var path = new MapPathArray();
        Match matchResults = regexObj.Match(str);
        while (matchResults.Success)
        {
            int x = int.Parse(matchResults.Groups[1].Value) * 100;
            int y = int.Parse(matchResults.Groups[2].Value) * 100;
            path.AddPoint(x, y);
            matchResults = matchResults.NextMatch();
        }
        return path;
    }
}

public class MapPath
{
    public static int iMaxSize = 100;
    public static int iMaxSizeBig = 1000;
    public int max_size { get; private set; }

    bool overlapped = false;
    bool lengthCalculated = false;

    public MapPath()
    {
        max_size = iMaxSize;
        Init();
    }

    public MapPath(int maxSize)
    {
        max_size = maxSize;
        Init();
    }

    private void Init()
    {
        points = new float[max_size];
        lens = new float[max_size / 2 + 1];
        size = 0;//в количестве чисел...
    }

    public void Reorganise()
    {
        if (overlapped)
        {
            Array.Reverse(points, 0, max_size - size); // реверс начала массива
            Array.Reverse(points, max_size - size, size); // реверс конца массива
            Array.Reverse(points, 0, max_size); // реверс всего циком

            overlapped = false;
            size = max_size;
        }

        if (!lengthCalculated)
        {
            for (int i = max_size - size; i < max_size-2; i += 2)
            {
                float dx = points[i] - points[i + 2];
                float dy = points[i+1] - points[i + 3];
                lens[i / 2] = (float)Math.Sqrt(dx * dx + dy * dy);
            }

            lengthCalculated = true;
        }

    }

    public int PathLength
    {
        get { return size / 2; }
    }

    public void Clear()
    {
        size = 0;
		overlapped = false;
        Ended = true;
        lengthCalculated = false;
    }

    public double Distance(float x1, float y1, float x2, float y2)
    {
        return Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    public void AddPointCentered(int iX, int iY)
    {
        AddPoint(iX * Helper.VoxelSize + Helper.HalfVoxelSize, iY * Helper.VoxelSize + Helper.HalfVoxelSize);
    }

    public void AddPoint(float x, float y)
    {
        if (size == max_size)
        //просто не будет таких длинных путей
        {
            overlapped = true;
            size = 0;
        }

		int shift = max_size - size;

        size += 2;
        points[shift - 2] = x;
        points[shift - 1] = y;
    }

    public void RemoveLastPoints(int count)
    {
        size = Math.Max(0, size - 2 * count);
    }

    public Vector2 GetPointByIndex(int index)
    {

        float x = points[max_size - size + index * 2];
        float y = points[max_size - size + index * 2 + 1];
        return new Vector2(x, y);
    }

    public void GetPointByIndex(int index, out float x, out float y)
    {
        x = points[max_size - size + index * 2];
        y = points[max_size - size + index * 2 + 1];
    }

    public void GetLastPoint(out float x, out float y)
    {
        x = points[max_size - 2 ];
        y = points[max_size - 1];
    }

    public float GetPointXByIndex(int index)
    {
        return points[max_size - size + index * 2];
    }

    public float GetPointYByIndex(int index)
    {
        return points[max_size - size + index * 2 + 1];
    }

    public int size;
    public float[] points;
    public int step;

    public int currentPointIndex = 0;
    private float[] lens;

    public float dtLastRecalc;
    public float dtStart;

    public void Start(float lastRecalc)
    {
        Reorganise();
        currentPointIndex = 0;
        dtStart = dtLastRecalc = lastRecalc;

        Ended = PathLength <= 1;
        if (!Ended)
            GetPointByIndex(currentPointIndex, out m_iCoordX, out m_iCoordY);
        else
            m_iCoordX = m_iCoordY = 0;

        //Sos.Debug("создали новый путь " + ToString());
    }
    public void Start()
    {
        Start(Time.time);
    }

    ////сейчас будет последнее движение или еще нет?
    public bool LastMove(float now, int iCooldown)
    {
        if (now == dtLastRecalc)
            return false;

        if (size == 0)
            return false;

        double timeSec = (now - dtLastRecalc) * 1000;
        double length = timeSec / iCooldown;

        return (size / 2 - currentPointIndex) * Helper.VoxelSize < length;
    }

    public bool Ended = true;

    public bool IsEnded()
    {
        return Ended;
    }

    /// <summary>
    /// давно не пересчитывался
    /// </summary>
    public bool Old
    {
        get{return SecondsPassed > 1;}
    }

    public double SecondsPassed
    {
        get{ return Time.time - dtStart; }
    }

    public float ScalarMult(float Ax, float Ay, float Cx, float Cy)
    {
        return Ax * Cx + Ay * Cy;
    }

    public float VectorMultMagnitude(float Ax, float Ay, float Cx, float Cy)
    {
        return Ax * Cy - Ay * Cx;
    }

    public void GetDistanceForvard(Vector2 point, int startIndex, out float distanceToLine, out float distanceToStart)
    {
        float bestPointX, bestPointY, postPointX, postPointY;
        GetPointByIndex(startIndex, out bestPointX, out bestPointY);
        GetPointByIndex(startIndex + 1, out postPointX, out postPointY);

        float dx = bestPointX - postPointX;
        float dy = bestPointY - postPointY;
        var len = lens[(max_size - size) / 2 + startIndex];

        distanceToStart = ScalarMult(dx, dy, bestPointX - point.X, bestPointY - point.Y) / len;
        distanceToLine = VectorMultMagnitude(dx, dy, bestPointX - point.X, bestPointY - point.Y) / len;
    }

    public void GetDistanceBackvard(Vector2 point, int startIndex, out float distanceToLine, out float distanceToStart)
    {
        float bestPointX, bestPointY, prevPointX, prevPointY;
        GetPointByIndex(startIndex, out bestPointX, out bestPointY);
        GetPointByIndex(startIndex - 1, out prevPointX, out prevPointY);

        float dx = bestPointX - prevPointX;
        float dy = bestPointY - prevPointY;

        var len = lens[(max_size - size) / 2 + startIndex - 1];
        distanceToStart = ScalarMult(dx, dy, bestPointX - point.X, bestPointY - point.Y) / len;
        distanceToLine = VectorMultMagnitude(dx, dy, bestPointX - point.X, bestPointY - point.Y) / len;
    }

    public Vector2 GetForce(Vector2 point)
    {
        if (PathLength <= 1)
        {
            Ended = true;
            return Vector2.Zero;
        }

        //это перебор до ближайшего минимума
        int startPoint = Math.Max(0, currentPointIndex);
        Vector2 bestPoint = GetPointByIndex(startPoint);
        float sqDist = (point - bestPoint).sqrMagnitude();
        //while (sqDist == 0 && startPoint < (size / 2-1))
        //{
        //    startPoint++;
        //    bestPoint = GetPointByIndex(startPoint);
        //    sqDist = (point - bestPoint).sqrMagnitude();
        //}
        int bestIndex = startPoint;
        for (int i = startPoint + 1; i < size / 2; i++)
        {
            var p = GetPointByIndex(i);
            float sq = (point - p).sqrMagnitude();
            if (sq < sqDist)
            {
                bestPoint = p;
                sqDist = sq;
                bestIndex = i;
            }
            else
                break;
        }

        currentPointIndex = bestIndex;

        if (sqDist > 1)
        {
            //Sos.Debug("отошли от пути больше чем на 1 метр");
            Ended = true;
            return Vector2.Zero;
        }

        float dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;

        float distanceToLine1 = 0;
        float distanceToStart1 = 0;
        if (bestIndex != size / 2 - 1)
        {
            var nextPoint = GetPointByIndex(bestIndex + 1);//points[bestIndex + 1];
            dx1 = nextPoint.X - bestPoint.X;
            dy1 = nextPoint.Y - bestPoint.Y;
            GetDistanceForvard(point, bestIndex, out distanceToLine1, out distanceToStart1);
        }


        float distanceToLine2 = 0;
        float distanceToStart2 = 0;
        if (bestIndex != 0)
        {
            var prevPoint = GetPointByIndex(bestIndex - 1);//points[bestIndex - 1];
            dx2 = bestPoint.X - prevPoint.X;
            dy2 = bestPoint.Y - prevPoint.Y;
            GetDistanceBackvard(point, bestIndex, out distanceToLine2, out distanceToStart2);
            distanceToLine2 = -distanceToLine2;
        }

        //мы на конце пути
        if (bestIndex == size / 2 - 1)
        {
            if (distanceToStart2 < 0)
            {
                //путь закончился
                //Sos.Debug("путь закончился");
                Ended = true;
                return Vector2.Zero;
            }
			return new Vector2(dx2 + dy2 * distanceToLine2, dy2 - dx2 * distanceToLine2).normalized();
            //return new Vector2(dx1 - dy1 * distanceToLine1, dy1 + dx1 * distanceToLine1).normalized();
        }

        //мы на начале пути
        if (bestIndex == 0)
        {
            return new Vector2(dx1, dy1).normalized();
            //return new Vector2(dx1 - dy1 * distanceToLine1, dy1 + dx1 * distanceToLine1).normalized();
        }

        if (distanceToStart1 > 0 || distanceToStart2 <= 0)
        {
            //прибавки dy1 * distanceToLine1, чтобы сила стремилась вывести на линию а не просто вела параллельно
            return new Vector2(dx1 + dy1 * distanceToLine1, dy1 - dx1 * distanceToLine1).normalized();
        }
        else
        {
            return new Vector2(dx2 + dy2 * distanceToLine2, dy2 - dx2 * distanceToLine2).normalized();
        }
    }

    public void GetEndPoint(out float x, out float y)
    {
        if (size == 0)
            throw new IndexOutOfRangeException();

        GetPointByIndex(size / 2 - 1, out x, out y);
    }

    public Vector2 GetForce(Vector3 point3, float now, int iCooldown)
    {
        if (now <= dtLastRecalc)
            return Vector2.Zero;

        var point = GeometryAlgorithm.Vector3ToVector2(point3);

        float timeSec = (now - dtLastRecalc) * 1000;
        float length = timeSec / iCooldown;

        dtLastRecalc = now;

        var force = GetForce(point) * length;
        if (force.IsNaN())
        {
            force = Vector2.Zero;
            var stackTrace = new StackTrace();
            ILogger.Instance.Send($"CurentForce.IsNaN() Path: {ToString()}, point: {point3}? currentPointIndex: {currentPointIndex}", stackTrace.ToString(), ErrorLevel.exception);
        }

        return force;
    }

    /// <summary>
    /// сгладить путь
    /// </summary>
    public void Smooth(Map map)
    {
        if (PathLength <= 2)
            return;

		lengthCalculated = true;
		Reorganise();

        GetPointByIndex(0, out var xPrev, out var yPrev);
        GetPointByIndex(1, out var xCur, out var yCur);

        int len = size / 2;
        int firstPos = max_size - size;

        for (int i = 2; i < len; i++)
        {
            var xNext = points[firstPos + i * 2];
            var yNext = points[firstPos + i * 2 + 1];

			if (map.CanSmooth(Helper.GetCell(xCur), Helper.GetCell(yCur)))
			{
				float xRound = (2 * xCur + xPrev + xNext) / 4;
				float yRound = (2 * yCur + yPrev + yNext) / 4;
				if (map.CanSmooth(Helper.GetCell(xRound), Helper.GetCell(yRound)))
				{
					points[firstPos + (i - 1) * 2] = xRound;
					points[firstPos + (i - 1) * 2 + 1] = yRound;
				}
			}

            xPrev = xCur;
            yPrev = yCur;

            xCur = xNext;
            yCur = yNext;
        }

		lengthCalculated = false;
    }

    private float m_iCoordX;
    public float iCoordX
    {
        get
        {
            return m_iCoordX;
        }
    }

    private float m_iCoordY;
    public float iCoordY
    {
        get
        {
            return m_iCoordY;
        }
    }

    public override string ToString()
    {
        if (size == 0)
            return "Empty";

        int len = PathLength;

        string str = " steplen = " + len;
        for (int i = 0; i < len; i++)
            str += "p[" + i + "]={" + points[max_size - 2- i * 2] + "," + points[max_size-1 - i * 2] + "}, ";

        return str;
    }

    public void CopyFrom(MapPath p)
    {
        size = p.size;
        step = p.step;
        overlapped = p.overlapped;
		lengthCalculated = p.lengthCalculated;
        Array.Copy(p.points, points, p.points.Length);
		Array.Copy(p.lens, lens, p.lens.Length);
    }

    static Regex regexObj = new Regex(@"\((-?\d+),(-?\d+)\)+");
    public static MapPath Parse(string str)
    {
        var path = new MapPath();
        Match matchResults = regexObj.Match(str);
        while (matchResults.Success)
        {
            int x = int.Parse(matchResults.Groups[1].Value) * 100;
            int y = int.Parse(matchResults.Groups[2].Value) * 100;
            path.AddPoint(x, y);
            matchResults = matchResults.NextMatch();
        }
        return path;
    }

	//Для дебага, чтоб посмотреть путь на картинке
	public void SavePicturePath(Map map, string fileName, Action<Bitmap> paint = null)
	{
		var bitmap = new Bitmap(map.heightX * 3, map.heightZ * 3);

		for(int x = 0; x < map.heightX; x++)
			for (int z = 0; z < map.heightZ; z++)
			{
				int h = Math.Min(Math.Max(0, 64+(map.GetHeight(x, z) - map.heightY)*2), 255);
				var color = Color.FromArgb(h, h, h);
				var colorRed = Color.FromArgb(h, 0, 0);
				var color2 = Color.FromArgb(h - 10, h - 10, h - 10);

				var sur = map.GetSurface(x, z);
				switch (sur)
				{
					//case SurfaceType.WaterLow: color = color2 = Color.Aqua; break;
					//case SurfaceType.WaterMedium: color = color2 = Color.DodgerBlue; break;
					//case SurfaceType.WaterHigh: color = color2 = Color.DarkBlue; break;
					case SurfaceType.Lava: color = color2 = Color.DarkOrange; break;
				}

				/*if (!map.CanSmooth(x, z))
				{
					color = Color.FromArgb(h, h, 255);
					color2 = Color.FromArgb(h - 10, h - 10, 255);
				}*/

				var start = map.UltraCorrectYPosition(new Vector3(Helper.CellToPosition(x), 0, Helper.CellToPosition(z)));
				for (int dx = -1; dx <= 1; dx++)
					for (int dz = -1; dz <= 1; dz++)
					{
						if (!map.IsTransparentSimple(x, z))
						{
							bitmap.SetPixel(x * 3 + 1 + dx, z * 3 + 1 + dz, colorRed);
							continue;
						}

						if (dx == 0 && dz == 0)
						{
							bitmap.SetPixel(x * 3 + 1, z * 3 + 1, color);
							continue;
						}

						var end = map.UltraCorrectYPosition(new Vector3(Helper.CellToPosition(x + dx), 0, Helper.CellToPosition(z + dz)));

						var end1 = map.UltraCorrectXZPosition(start, end);
						/*var end2 = map.UltraMegaCorrectXZPosition(start, end, false, false);

						bool b1 = Helper.GetCell(end1.X) == x + dx && Helper.GetCell(end1.Z) == z + dz;
						bool b2 = Helper.GetCell(end2.X) == x + dx && Helper.GetCell(end2.Z) == z + dz;
						if(b1 && b2)
							bitmap.SetPixel(x * 3 + 1 + dx, z * 3 + 1 + dz, Color.FromArgb(0, 0, h));
						else*/ if (Helper.GetCell(end1.X) == x + dx && Helper.GetCell(end1.Z) == z + dz)
							bitmap.SetPixel(x * 3 + 1 + dx, z * 3 + 1 + dz, color2);
						else
							bitmap.SetPixel(x * 3 + 1 + dx, z * 3 + 1 + dz, colorRed);
					}
			}

		var pen = new Pen(Color.BlueViolet);
		var pen2 = new Pen(Color.Firebrick);
		var gr = Graphics.FromImage(bitmap);

		foreach (var reperPoint in DeepPathFinderAlgorithm.GetReperPointsHash())
		{
			for (var index = 0; index < reperPoint.Value.Neighbors.Count; index++)
			{
				var neighbor = reperPoint.Value.Neighbors[index];
				var dist = reperPoint.Value.Neigh_Distance[index];

				var dx = (neighbor.iXCoord - reperPoint.Value.iXCoord) / 2;
				var dy = (neighbor.iYCoord - reperPoint.Value.iYCoord) / 2;

				//gr.DrawLine(dist > 100000 ? pen2 : pen, reperPoint.Value.iXCoord * 3 + 1, reperPoint.Value.iYCoord * 3 + 1,
				//	(reperPoint.Value.iXCoord + dx) * 3 + 1, (reperPoint.Value.iYCoord + dy) * 3 + 1);
			}
		}

		for (int i = 0; i < PathLength; i++)
		{
			var x1 = Helper.GetCell(GetPointByIndex(i).X);
			var y1 = Helper.GetCell(GetPointByIndex(i).Y);
			var c1 = bitmap.GetPixel(x1 * 3 + 1, y1 * 3 + 1);
			bitmap.SetPixel(x1 * 3 + 1, y1 * 3 + 1, Color.FromArgb(c1.R, c1.B == 0 ? 128 : 255, c1.B));

			if (i < PathLength - 1)
			{
				var x2 = Helper.GetCell(GetPointByIndex(i + 1).X);
				var y2 = Helper.GetCell(GetPointByIndex(i + 1).Y);
				var c2 = bitmap.GetPixel(x1 * 3 + 1 + Math.Sign(x2 - x1), y1 * 3 + 1 + Math.Sign(y2 - y1));
				bitmap.SetPixel(x1 * 3 + 1 + Math.Sign(x2 - x1), y1 * 3 + 1 + Math.Sign(y2 - y1),
					Color.FromArgb(c2.R, c2.B == 0 ? 120 : 240, c2.B));
			}
		}

		foreach (var reperPoint in DeepPathFinderAlgorithm.GetReperPointsHash())
		{
			//bitmap.SetPixel(reperPoint.Value.iXCoord * 3 + 1, reperPoint.Value.iYCoord * 3 + 1, Color.FromArgb(0, 0, 255));
		}

		if (paint != null)
			paint(bitmap);

		bitmap.Save(fileName);
	}

	public void SavePicturePathLoopa(Map map, string fileName, int x, int z, int zoom, Action<Bitmap> paint = null)
	{
		var bitmap = new Bitmap(1024, 1024);

		var pen0 = new Pen(Color.DarkGray);
		var pen = new Pen(Color.Green);
		var gr = Graphics.FromImage(bitmap);

		for (int i = 0; i < map.heightX; i++)
		{
			var x1 = (int)((i - x) * Helper.VoxelSize * zoom) + bitmap.Width / 2;
			if (x1 < 0 || x1 >= bitmap.Width)
				continue;

			gr.DrawLine(pen0, x1, 0, x1, bitmap.Height);
		}

		for (int i = 0; i < map.heightZ; i++)
		{
			var z1 = (int)((i - z) * Helper.VoxelSize * zoom) + bitmap.Height / 2;
			if (z1 < 0 || z1 >= bitmap.Height)
				continue;

			gr.DrawLine(pen0, 0, z1, bitmap.Width, z1);
		}

		for (int i = 0; i < PathLength-1; i++)
		{
			var x1 = (int)((GetPointByIndex(i).X - x * Helper.VoxelSize) * zoom) + bitmap.Width / 2;
			var y1 = (int)((GetPointByIndex(i).Y - z * Helper.VoxelSize) * zoom) + bitmap.Height / 2;

			var x2 = (int)((GetPointByIndex(i+1).X - x * Helper.VoxelSize) * zoom) + bitmap.Width / 2;
			var y2 = (int)((GetPointByIndex(i+1).Y - z * Helper.VoxelSize) * zoom) + bitmap.Height / 2;

			gr.DrawLine(pen, x1, y1, x2, y2);
			gr.DrawEllipse(pen, x1 - 2, y1 - 2, 5, 5);
		}

		if (paint != null)
			paint(bitmap);

		bitmap.Save(fileName);
	}
}
