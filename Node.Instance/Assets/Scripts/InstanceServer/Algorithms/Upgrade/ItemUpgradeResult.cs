﻿using Assets.Scripts.Networking.ClientDataRPC;

namespace Assets.Scripts.InstanceServer.Algorithms.Upgrade
{
	public sealed class ItemUpgradeResult
	{
		public float UpgradeDuration;
		public bool CanCancel;
		public bool ContinueUpgrade;
		public ErrorData Result;
		public bool UpgradeSuccessful;
        public double baseChance;
	}
}
