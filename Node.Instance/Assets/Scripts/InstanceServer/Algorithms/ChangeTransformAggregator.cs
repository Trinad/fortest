﻿using System;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.Networking;

namespace Assets.Scripts.InstanceServer.Algorithms
{
    public sealed class ChangeTransformAggregator
	{
		private readonly IChangeTransformSender m_sender;

		private readonly Dictionary<int, HashSet<int>> m_unreliableTargets;
		private readonly Dictionary<int, HashSet<int>> m_reliableTargets;
		private readonly Dictionary<int, HashSet<int>> m_unreliableForceTargets;
		private readonly Dictionary<int, HashSet<int>> m_reliableForceTargets;

		public ChangeTransformAggregator(IChangeTransformSender sender)
		{
			m_sender = sender;

			m_unreliableTargets = new Dictionary<int, HashSet<int>>();
			m_reliableTargets = new Dictionary<int, HashSet<int>>();
			m_unreliableForceTargets = new Dictionary<int, HashSet<int>>();
			m_reliableForceTargets = new Dictionary<int, HashSet<int>>();
		}

		public void SendChangesAndReset()
		{
			sendChangesAndClear(m_unreliableTargets, ChangeTransformReliability.Unreliable);
			sendChangesAndClear(m_reliableTargets, ChangeTransformReliability.Reliable);
			sendChangesAndClear(m_unreliableForceTargets, ChangeTransformReliability.UnreliableForced);
			sendChangesAndClear(m_reliableForceTargets, ChangeTransformReliability.ReliableForced);
		}

		private void sendChangesAndClear(Dictionary<int, HashSet<int>> dict, ChangeTransformReliability reliability)
		{
			foreach (var kvp in dict)
			{
				int personId = kvp.Key;
				HashSet<int> targetObjectIds = kvp.Value;

				try
				{
					m_sender.SendChangeTransformResponce(personId, reliability, targetObjectIds);
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send($"ChangeTransformAggregator.sendChangesAndClear(): {ex}", ErrorLevel.exception);
				}
			}
			dict.Clear();
		}

		public void Register(InstancePerson person, ChangeTransformReliability reliability, MapTargetObject target)
		{
			Dictionary<int, HashSet<int>> targetDictionary;
			switch (reliability)
			{
				case ChangeTransformReliability.Reliable:
					targetDictionary = m_reliableTargets;
					break;
				case ChangeTransformReliability.Unreliable:
					targetDictionary = m_unreliableTargets;
					break;
				case ChangeTransformReliability.ReliableForced:
					targetDictionary = m_reliableForceTargets;
					break;
				case ChangeTransformReliability.UnreliableForced:
					targetDictionary = m_unreliableForceTargets;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(reliability), reliability, $"Unsupported reliability {reliability}");
			}

			register(targetDictionary, person.personId, target.Id);
		}

		private void register(Dictionary<int, HashSet<int>> dict, int personId, int targetObjectId)
		{
			if (!dict.TryGetValue(personId, out HashSet<int> datas))
			{
				datas = new HashSet<int>();
				dict[personId] = datas;
			}

			datas.Add(targetObjectId);
		}
	}
}
