﻿using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Groups;
using Assets.Scripts.Networking;
using LobbyInstanceLib;

namespace Assets.Scripts
{
	/// <summary>
	/// все менеджеры регистрируются тут, 
	/// чтобы не увеличивать лишний раз ServerStartGameManager и InstanceGameManager
	/// </summary>
	public class DIListManagers : BaseDIListManagers
	{
		public DIListManagers(uLinkManager uLinkManager, InstanceGameManager instanceGameManager, GroupManager groupManager,
							InstancePerformanceInfo performanceInfo, EmotionManager emotionManager,
							EventManager eventManager, SQLManager sqlManager)
		{
			AddManager(uLinkManager);
			AddManager(instanceGameManager);
			AddManager(groupManager);
			AddManager(performanceInfo);
			AddManager(emotionManager);
			AddManager(eventManager);
			AddManager(sqlManager);
		}
	}
}