﻿using Assets.Scripts.Utils.Logger;
using System.IO;

namespace Assets.Scripts
{
    public class PathBuilder
	{
		private readonly string streamingAssetsPath;
		private readonly string streamingAssetsPathZone;

		public PathBuilder(string streamingAssetsPath)
		{
			this.streamingAssetsPath = streamingAssetsPath;
			this.streamingAssetsPathZone = Path.Combine(streamingAssetsPath, "zones");
		}

		public PathBuilder(string streamingAssetsPath, string streamingAssetsPathZone)
		{
			this.streamingAssetsPath = streamingAssetsPath;
			this.streamingAssetsPathZone = streamingAssetsPathZone;
		}

		public string GetZoneUrl(string zone)
		{
			return Path.Combine(streamingAssetsPathZone, $"{zone}.json");
		}

		public string InstanceXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/instance.xml"); }
		}

		public string MonsterXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/monsters.xml"); }
		}

		public string PortalXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/portals.xml"); }
		}

		public string EdgesXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/edges.xml"); }
		}

		public string EdgesMapXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/edgesMap.xml"); }
		}

		public string DungeonsXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/dungeons.xml"); }
		}

		public string LootListsXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/lootlists.xml"); }
		}

		public string SpecialOffersXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/specialOffers.xml"); }
		}

		public string TraderDataXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/tradersItems.xml"); }
		}

		public string ItemUpgradeXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/itemUpgrade.xml"); }
		}

		public string QuestDataXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/quests.xml"); }
		}

		public string ProgressLineXml => Path.Combine(streamingAssetsPath, "cfg/progressLine.xml");
		public string ProgressLineXMLData => Path.Combine(streamingAssetsPath, "cfg/ProgressLineXMLData.xml");

		public string PersonXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/persons.xml"); }
		}

		public string LootsXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/loots.xml"); }
		}

        public string ItemStatsCollectionXml
        {
            get { return Path.Combine(streamingAssetsPath, "cfg/itemStats.xml"); }
        }

		public string PassiveAbilitysXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/passiveAbility.xml"); }
		}

        public string PerksXml
        {
            get { return Path.Combine(streamingAssetsPath, "cfg/perks.xml"); }
        }

        public string InfluenceXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/influences.xml"); }
		}

		public string ElixireXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/elixirs.xml"); }
		}

        public string BuffXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/buffs.xml"); }
		}

		public string EmotionXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/emotions.xml"); }
		}

		public string AnalyticsXmlData
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/analytics.xml"); }
		}

		public string FormulasXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/formulas.xml"); }
		}

		public string SocialXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/social.xml"); }
		}

		public string ServerConstantsXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/constants.xml"); }
		}

		public string CreditRatingXml => Path.Combine(streamingAssetsPath, "cfg/creditRating.xml");
        public string Chat => Path.Combine(streamingAssetsPath, "cfg/chat.xml");

        public string ClientConstantsXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/clientConstants.xml"); }
		}

		public string TutorialDataXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/tutorialData.xml"); }
		}

		public string AddinLocationXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/Addin/addinlist.xml"); }
		}

		public string AddinPath
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/Addin"); }
		}

		public string InstancePerformanceInfo
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/performanceSettings.xml"); }
		}

		//public string AvengersSettingsXml
		//{
		//	get { return Path.Combine(streamingAssetsPath, "cfg/avengers.xml"); }
		//}

	    public string ExperienceDataXml
        {
	        get { return Path.Combine(streamingAssetsPath, "cfg/experience.xml"); }
	    }

        public string MonstersBehaviorDataXml
        {
            get { return Path.Combine(streamingAssetsPath, "cfg/monstersBehaviorData.xml"); }
        }

		public string NickNameGeneratorXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/nickNameGenerator.xml"); }
		}

		public string PlayerCityXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/playerCity.xml"); }
		}

        public string ItemDataRepositoryDataXml
        {
            get { return Path.Combine(streamingAssetsPath, "cfg/items.xml"); }
        }
    }

    public class AuthPathBuilder
    {
        private readonly string xmlPath;

        public AuthPathBuilder(string xmlPath)
        {
            this.xmlPath = xmlPath;
        }

        public string ArenaDataXml
        {
            get { return Path.Combine(xmlPath, "arenaData.xml"); }
        }

        public string RemoteConfigXml
        {
            get { return Path.Combine(xmlPath, "remoteconfig.xml"); }
        }

		public string ShopXml
		{
			get { return Path.Combine(xmlPath, "shop.xml"); }
		}
    }

	public class PaymentsPathBuilder
	{
		private readonly string xmlPath;

		public PaymentsPathBuilder(string xmlPath)
		{
			this.xmlPath = xmlPath;
		}

		public string AppStoreXml
		{
			get { return Path.Combine(xmlPath, "appstore.xml"); }
		}

		public string BaseXml
		{
			get { return Path.Combine(xmlPath, "base.xml"); }
		}

		public string GooglePlayXml
		{
			get { return Path.Combine(xmlPath, "googleplay.xml"); }
		}
	}
}
