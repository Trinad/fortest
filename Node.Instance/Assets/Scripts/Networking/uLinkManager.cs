﻿using System;
using Assets.Scripts.InstanceServer;
using Lidgren.Network;
using LobbyInstanceLib.Networking;
using Node.Instance.Assets.Scripts;
using UtilsLib.Logic;

namespace Assets.Scripts.Networking
{
	public sealed class uLinkManager : BaseGameManager
	{
		private readonly AppConfig cfg;
		private readonly PersonsOnServer personsOnServer;

		private readonly uLink.Network m_network;
		private readonly uLink.NetworkP2P m_networkP2P;
		public readonly uLink.NetworkP2P p2pFromNewLobby;

		private readonly uLink.NetworkConfig.ResendDelayFunction m_defaultResendDelayFunction;

		public uLinkP2PConnector p2pConn;
		public string p2pLobbyConnect_host;
		public int p2pLobbyConnect_port;

		public int ListenPort;

		private int startPort = 10000;
		private int endPort = 20000;

		public uLinkManager(AppConfig _cfg, uLink.Network _network, uLink.NetworkP2P _networkP2P, PersonsOnServer _personsOnServer,
			InstanceCommandLineOptions m_commandLineOptions)
		{
			cfg = _cfg;
			personsOnServer = _personsOnServer;

			m_network = _network;
			m_networkP2P = _networkP2P;
			p2pFromNewLobby = new uLink.NetworkP2P(m_network);

			m_defaultResendDelayFunction = NetConnection.DefaultResendFunction;
			m_network.config.resendDelayFunction = NetworkResendDelayFunction;

			p2pLobbyConnect_host = m_commandLineOptions.p2pLobbyConnect_host;
			p2pLobbyConnect_port = m_commandLineOptions.p2pLobbyConnect_port;

			uLink.NetworkLog.SetLevel(uLink.NetworkLogFlags.All, uLink.NetworkLogLevel.Info);
		}

		private double NetworkResendDelayFunction(NetConnection conn, int numsent, double avgrtt)
		{
			// как только клиент приконнектился, ему уходит много сообщений с сервера, на медленных девайсах
			// они обрабатываются долго, и улинк думает, что сообщения не дошли и их надо пересылать (каждые ~200 мс). 
			// Чтобы этого избежать, стандартная функция подменяется нашим интервалом для недавно приконнектившихся персов

			var networkPlayer = m_network._ServerFindConnectionPlayerID(conn);
			if (!networkPlayer.isUnassigned)
			{
				var person = personsOnServer.GetPerson(networkPlayer);
				if (person != null)
				{
					const float enterGameDelay = 15.0f;
					const float resendDelay = 3.0f;
					if (Time.time - person.ConnectedAtTime < enterGameDelay)
						return resendDelay;
				}
			}

			return m_defaultResendDelayFunction(conn, numsent, avgrtt);
		}

		public override void Start()
		{
			ListenPort = GetFreePort();

			AddP2PNetworkComponent();
		}

		private int GetFreePort()
		{
			int freePort = 0;
			try
			{
				ILogger.Instance.Send($"GetFreePort startPort = {startPort} endPort = {endPort}", ErrorLevel.debug);

				uLink.NetworkConnectionError error = uLink.Network.Instance.InitializeServer(ServerRpcListener.MAX_CONNECTIONS, startPort, endPort, ServerRpcListener.USE_NAT);

				if (error == uLink.NetworkConnectionError.NoError)
					freePort = uLink.Network.Instance.listenPort;
				else
					throw new Exception($"uLink.Network.Instance.InitializeServer() failed with reason {error}");

				if (freePort == 0)
				{
					ILogger.Instance.Send("NotFoundFreePort from " + startPort + " to " + endPort, ErrorLevel.error);
				}
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send(ex.ToString(), ex.StackTrace, ErrorLevel.exception);
			}

			ILogger.Instance.Send("GetFreePort return value " + freePort, ErrorLevel.debug);

			return freePort;
		}

		private void AddP2PNetworkComponent()
		{
			m_networkP2P.peerType = "InstanceServer";
			m_networkP2P.peerName = "InstanceServer";
			m_networkP2P.comment = "InstanceServer";
			m_networkP2P.incomingPassword = cfg.p2pSelfConnect.pwd;
			int listenPort = ListenPort + cfg.p2pSelfConnect.shiftport;
			m_networkP2P.Open(listenPort, cfg.MaxConnections);
			if (!m_networkP2P.isListening)
			{
				throw new Exception($"AddP2PNetworkComponent(): m_networkP2P.Open() failed to bind to port {listenPort}");
			}

			ILogger.Instance.Send($"AddP2PNetworkComponent p2p.listenPort = {m_networkP2P.listenPort}");
			ILogger.Instance.Send($"AddP2PNetworkComponent p2p.maxConnections = {m_networkP2P.maxConnections}");
			ILogger.Instance.Send($"AddP2PNetworkComponent p2p.incomingPassword = {m_networkP2P.incomingPassword}");

			p2pFromNewLobby.peerType = "p2pFromNewLobby";
			p2pFromNewLobby.peerName = "p2pFromNewLobby";
			p2pFromNewLobby.comment = "p2pFromNewLobby";
			p2pFromNewLobby.incomingPassword = cfg.p2pSelfConnect.pwd;
			listenPort = ListenPort + cfg.ShiftPortForConnectFromLobby;
			p2pFromNewLobby.Open(listenPort, cfg.MaxConnections);
			if (!p2pFromNewLobby.isListening)
			{
				throw new Exception($"AddP2PNetworkComponent(): p2pFromNewLobby.Open() failed to bind to port {listenPort}");
			}

			ILogger.Instance.Send($"AddP2PNetworkComponent p2pFromNewLobby.listenPort = {p2pFromNewLobby.listenPort}");
			ILogger.Instance.Send($"AddP2PNetworkComponent p2pFromNewLobby.maxConnections = {p2pFromNewLobby.maxConnections}");
			ILogger.Instance.Send($"AddP2PNetworkComponent p2pFromNewLobby.incomingPassword = {p2pFromNewLobby.incomingPassword}");

			p2pConn = new uLinkP2PConnector(m_networkP2P);

			if (!string.IsNullOrEmpty(p2pLobbyConnect_host))
				p2pConn.host = p2pLobbyConnect_host;
			else
				p2pConn.host = cfg.p2pLobbyConnect.host;

			if (p2pLobbyConnect_port != -1)
				p2pConn.port = p2pLobbyConnect_port;
			else
				p2pConn.port = cfg.p2pLobbyConnect.port;

			p2pConn.incomingPassword = cfg.PasswordForP2P;
			p2pConn.interval = cfg.Interval / 1000.0f;
			p2pConn.connectingTimeout = 1;
			p2pConn.tryConnectCountMax = 30;

			ILogger.Instance.Send($"AddP2PNetworkComponent p2pConn host={p2pConn.host} port={p2pConn.port}",
				$"incomingPassword={p2pConn.incomingPassword} interval={p2pConn.interval} connectingTimeout={p2pConn.connectingTimeout}", ErrorLevel.debug);

			//managers.AddManager(p2pConn); //TODO_deg не смог добавить, из-за циклической зависимости
		}

		public override void Update()
		{
			// read input
			try
			{
				m_network.Update();
				m_networkP2P.Update();
				p2pFromNewLobby.Update();

				p2pConn?.Update();
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send(ex.ToString(), ErrorLevel.exception);
			}
		}

		public override void LateUpdate()
		{
			// send rpc's
			try
			{
				m_network.Update();
				m_networkP2P.Update();
				p2pFromNewLobby.Update();
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send(ex.ToString(), ErrorLevel.exception);
			}
		}
	}
}
