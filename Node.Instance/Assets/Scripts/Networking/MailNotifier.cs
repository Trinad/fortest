﻿using Assets.Scripts.InstanceServer;
using Assets.Scripts.Networking.ClientDataRPC;
using uLink;

namespace Assets.Scripts.Networking
{
	public sealed class MailNotifier : BaseInstanceRpcListener
	{
		public MailNotifier(NetworkP2P networkP2P, uLink.Network network, uLink.INetworkView view) : base(networkP2P, network, view)
		{
		}

		//Показать красную точку, что есть новое письмо
		public void SendNotifyMailResponse(InstancePerson person)
		{
			SendRpc(person.ClientRpc.NotifyMailResponse, person.MailCount);
		}

		//Предмет отправлен на почту
		public void SendItemSendToMailResponse(InstancePerson person)
		{
			SendRpc(person.ClientRpc.ItemSendToMailResponse);
			SendNotifyMailResponse(person);
		}

		public void SendTakeGift(InstancePerson person)
		{
			SendRpc(person.ClientRpc.TakeGiftResponse, Errors.Success);
		}

		public void SendTakeMailItem(InstancePerson person, bool success, MailItemData mail)
		{
			SendRpc(person.ClientRpc.TakeMailItemResponse, success, mail.MailId);
		}
	}
}
