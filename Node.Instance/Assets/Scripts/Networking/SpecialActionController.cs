﻿using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.Networking.ClientDataRPC;
using System.Linq;
using uLink;
using UtilsLib.Logic;
using Zenject;

namespace Assets.Scripts.Networking
{
	public interface ISpecialActionController
	{
		void SpecialActionRequest(InstancePerson instancePerson);
		void SpecialActionResponse(InstancePerson instancePerson, SpecialRpcData sections);
		void SpecialActionRequestCheat(InstancePerson instancePerson, int timeSec = -1);
		void BuySpecialOffer(InstancePerson instancePerson, int offerId);
		void SpecialActionBuyRequest(InstancePerson instancePerson, int personId);

		void CheatDelOffers(InstancePerson instancePerson);
		void CheatDelAllGetOfferById(InstancePerson instancePerson, int offerId, bool checkRules);
		void CheatGetOfferById(InstancePerson instancePerson, int id);
	}

	public class SpecialActionController : BaseInstanceRpcListener, ISpecialActionController
	{
		SpecialActionManager specialActionManager;
		ShopManager shopManager;
		IPersonsOnServer personsOnServer;

		public SpecialActionController(NetworkP2P networkP2P, uLink.Network network, uLink.INetworkView view) : base(networkP2P, network, view)
		{
		}

		[Inject]
		public void Constructor(IPersonsOnServer personsOnServer, SpecialActionManager specialActionManager, ShopManager shopManager)
		{
			this.personsOnServer = personsOnServer;
			this.specialActionManager = specialActionManager;
			this.shopManager = shopManager;

		}

		[RPC]
		public void SpecialActionRequest(InstancePerson instancePerson)
		{
			var specialRpc = specialActionManager.GetAllActualSpecialRpcData(instancePerson);
			SpecialActionResponse(instancePerson, specialRpc);
		}

		public void BuySpecialOffer(InstancePerson instancePerson, int offerId)
		{
			var specialOfferItem = specialActionManager.SpecialOffer.GetSpecialOfferItem(offerId);
			if (specialOfferItem == null)
			{
				ILogger.Instance.Send($"======= BuySpecialOffer specialOfferItem == null offerId: {offerId}", ErrorLevel.error);
				return;
			}

			specialActionManager.BuySpecialOffer(instancePerson, specialOfferItem);

			var specialRpc = specialActionManager.GetAllActualSpecialRpcData(instancePerson);
			SpecialActionResponse(instancePerson, specialRpc);
		}

		[RPC]
		public void SpecialActionBuyRequest(InstancePerson instancePerson, int personId)
		{
			//TODO fil сделать булл подумать нужна ли проверка
			specialActionManager.SpecialOfferReward(instancePerson, personId);
			SpecialActionRequest(instancePerson);

		}

		public void SpecialActionResponse(InstancePerson instancePerson, SpecialRpcData specialRpcData)
		{
			SendRpc(instancePerson.ClientRpc.SpecialActionResponse, specialRpcData);
		}

		#region Cheats
		public void SpecialActionRequestCheat(InstancePerson instancePerson, int timeSec)
		{
			var specialRpc = specialActionManager.GetAllActualSpecialRpcData(instancePerson);

			var offer = specialRpc.SpecialOfferDatas.FirstOrDefault();
			if (offer != null)
			{
				var loginOffer = instancePerson.login.OffersDatas.Find(o => o.OfferId == offer.OfferId);
				loginOffer.TimeForUpdate = TimeProvider.UTCNow.AddSeconds(timeSec);
				offer.Duration = loginOffer.GetTime();
			}

			SpecialActionResponse(instancePerson, specialRpc);
		}

		public void CheatDelOffers(InstancePerson instancePerson)
		{
			var data = specialActionManager.CheatDelOffers(instancePerson);
			SpecialActionResponse(instancePerson, data);
		}

		public void CheatDelAllGetOfferById(InstancePerson instancePerson, int offerId, bool checkRules)
		{
			var data = specialActionManager.CheatDelAllGetOfferById(instancePerson, offerId, checkRules);
			if (data == null)
			{
				ILogger.Instance.Send($"CheatDelAllGetOfferById data == null offerId: {offerId} checkRules: {checkRules}", ErrorLevel.error);
				return;
			}
			SpecialActionResponse(instancePerson, data);
		}

		public void CheatGetOfferById(InstancePerson instancePerson, int id)
		{
			var data = specialActionManager.CheatGetOfferById(instancePerson, id);
			SpecialActionResponse(instancePerson, data);
		}

		#endregion
	}
}
