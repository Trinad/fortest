﻿using System.Linq;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Utils;
using Node.Instance.Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public enum PlayerServerStatus : sbyte
	{
		NONE,
		IDLE, // В лобби
		IN_BATTLE // сражается на арене
	}

	public class PlayerServerStatusData
	{
		public GameSettings settings;
		public PlayerServerStatus PlayerServerStatus;
		public string LevelName;
		public MapInfoData MapInfo;
		public ArenaMatchType ArenaMatchType;
        public int BattleId;
        public bool IsMatchActive;

        public PlayerServerStatusData()
		{
		}

		public PlayerServerStatusData(Map map, InstancePerson person)
		{
			settings = person.login.settings;
			PlayerServerStatus = Map.Edge.Type == LocationType.Camp ? PlayerServerStatus.IDLE : PlayerServerStatus.IN_BATTLE;
			LevelName = Map.Edge.GetJsonName();
			MapInfo = new MapInfoData(map, person);
			ArenaMatchType = map.GetArenaMatchType();
            BattleId = map.GameId % ServerConstants.Instance.Dev2DevLimit;
            IsMatchActive = !map.GetVariableBool("stopBattle");
        }

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (PlayerServerStatusData) value;
			stream.Write(si.settings);
			stream.Write((sbyte)si.PlayerServerStatus);
			stream.WriteStringFromId(si.LevelName);
			stream.Write(si.MapInfo);
			stream.Write(si.ArenaMatchType);
            stream.Write(si.BattleId);
            stream.Write(si.IsMatchActive);
        }
	}
}
