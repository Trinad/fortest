﻿using UtilsLib.Logic.Enums;

namespace UtilsLib.NetworkingData
{
	public class ResourceCount
	{
		public ResourceType Resource;
		public int Count;

		public ResourceCount()
		{
		}

		public ResourceCount(ResourceType resource, int count)
		{
			Resource = resource;
			Count = count;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			ResourceCount si = (ResourceCount)value;
			stream.Write((int)si.Resource);
			stream.Write(si.Count);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			ResourceCount si = new ResourceCount();
			si.Resource = (ResourceType)stream.Read<int>();
			si.Count = stream.Read<int>();
			return si;
		}
	}
}
