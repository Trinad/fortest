﻿using System;
using System.Numerics;
using Assets.Scripts.Utils.Common;
using LobbyInstanceLib.Networking;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public interface IChangeTransformData { }

	public sealed class ChangeTransformData : IChangeTransformData
	{
		public int TargetId;
		public Vector3 Position;
		public Vector2 Direction;
		public bool IsMotionControlledByServerNew;
		public bool IsFly;
		public bool IsNeedInvertMoveMode;
        public bool IsAimInProcess;
        public ChangeTransformDataFlags Flags;

		public ChangeTransformData(int targetId, Vector3 position, Vector2 direction, bool isMotionControlledByServerNew, 
			bool isFly, bool isNeedInvertMoveMode, bool isAimInProcess)
		{
			TargetId = targetId;
			Position = position;
			Direction = direction;
			IsMotionControlledByServerNew = isMotionControlledByServerNew;
			IsFly = isFly;
			IsNeedInvertMoveMode = isNeedInvertMoveMode;
            IsAimInProcess = isAimInProcess;

            if (isMotionControlledByServerNew)
				Flags |= ChangeTransformDataFlags.IsMotionControlledByServerNew;
			if (isFly)
				Flags |= ChangeTransformDataFlags.IsFly;
			if (isNeedInvertMoveMode)
				Flags |= ChangeTransformDataFlags.IsNeedInvertMoveMode;
            if (isAimInProcess)
                Flags |= ChangeTransformDataFlags.IsSkillAimingInProgress;
        }

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (ChangeTransformData)value;
			stream.WriteVariableInt32(si.TargetId);
			stream.WriteVec3Short(si.Position);
			stream.WriteVec2Byte(si.Direction);
			stream.WriteByte((byte)si.Flags);
		}
	}

	[Flags]
	public enum ChangeTransformDataFlags : byte
	{
		IsMotionControlledByServerNew = 0b0001,
		IsFly = 0b0010,
		IsNeedInvertMoveMode = 0b0100,
        IsSkillAimingInProgress = 0b1000
    }
}
