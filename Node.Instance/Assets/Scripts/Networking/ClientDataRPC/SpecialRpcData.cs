﻿using System.Collections.Generic;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class SpecialRpcData
    {
        public List<SpecialRpcItemData> SpecialOfferDatas;
        public List<SpecialRpcItemData> OffersForAdd;
        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            var si = (SpecialRpcData)value;
            stream.Write(si.SpecialOfferDatas);
            stream.Write(si.OffersForAdd);
        }

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            var si = new SpecialRpcData();
            si.SpecialOfferDatas = stream.Read<List<SpecialRpcItemData>>();
            si.OffersForAdd = stream.Read<List<SpecialRpcItemData>>();
            return si;
        }
    }

    public class SpecialRpcItemData
    {
        public int OfferId;
        public string OfferName;
        public string Sku;
        public string TechnicalNameTitle;
        public string TechnicalNameDesc;
        public PersonType PersonType = PersonType.None;
        public int PersonLevel = 0;
        public int Duration;

        public bool ShowForcibly;

        public SpecialOfferGui OfferGui;
        public List<InventoryItemData> Content;

        public float Price;

        public SpecialRpcItemData()
        {
        }

        public SpecialRpcItemData(InstancePerson instancePerson, SpecialOfferGui offerGui, LootList content)
        {
            OfferGui = offerGui;
            Content = content.GetItemList(null, instancePerson);
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            var si = (SpecialRpcItemData)value;
            stream.Write(si.OfferId);
            stream.Write(si.OfferName);
            stream.Write(si.Sku);
            stream.Write(si.TechnicalNameTitle);
            stream.Write(si.TechnicalNameDesc);
            stream.Write(si.PersonType);
            stream.Write(si.PersonLevel);
            stream.Write(si.Duration);
            stream.Write(si.ShowForcibly);

            stream.Write(si.OfferGui);
            stream.Write(si.Content);
            stream.Write(si.Price);
        }

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            var si = new SpecialRpcItemData();
            si.OfferId = stream.Read<int>();
            si.OfferName = stream.Read<string>();
            si.Sku = stream.Read<string>();
            si.TechnicalNameTitle = stream.Read<string>();
            si.TechnicalNameDesc = stream.Read<string>();
            si.PersonType = (PersonType)stream.Read<int>();
            si.PersonLevel = stream.Read<int>();
            si.Duration = stream.Read<int>();
            si.ShowForcibly = stream.Read<bool>();

            si.OfferGui = stream.Read<SpecialOfferGui>();
            si.Content = stream.Read<List<InventoryItemData>>();
            si.Price = stream.ReadSingle();
            return si;
        }
    }
}
