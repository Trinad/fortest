﻿using System;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries.Quests;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.InstanceServer.Extensions;
using Assets.Scripts.Utils.Logger;
using Node.Instance.Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using UtilsLib;
using UtilsLib.BatiyScript;
using UtilsLib.NetworkingData;
using UtilsLib.DegSerializers;
using UtilsLib.Logic.Enums;
using Fragoria.Common.Utils;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public enum QuestType
    {
        Daily = 0,
        Scenario = 1,
        Tutorial = 2,
		Termometr = 3,
    }

	public enum QuestPathType
	{
		InventoryPotion,
		InventoryWeapon,
		SelectGameMode,
		LobbyMenu,
		ShopEmerald,
		SelectGameModeDM,
		SelectGameModeTDM,
		SelectGameModeCTF,
		SelectGameModeWaves,
		SelectGameModeWarrior,
		SelectGameModeWizard,
		SelectGameModeArcher,
		SelectGameModeGunner,
	}

	public class QuestItemData: ICanCopy
    {
		[XmlIgnore]
		public int GroupQuestId;
		[XmlIgnore]
		public AnalyticDailyQuestType GroupType;
		[XmlIgnore]
		private string name;
		[XmlAttribute]
		public string Name
		{
			get => name;
			set
			{
				name = value;

				//Имя квеста типа "dq_items.01_buy_potion_for_gold"
				if(value.Contains('.'))
				{
					var s = value.Split('.');
					Enum.TryParse(s[0], out GroupType);
					if (s[1].Contains('_'))
						GroupQuestId = int.Parse(s[1].Substring(0, s[1].IndexOf('_')));
				}
			}
		}
        [XmlAttribute]
        public string TechnicalName;
	    [XmlAttribute]
	    public string IconName;

		public List<LocalizationAttributeQuest> LocalizationAttributes;

		[XmlAttribute]
        public bool Save = true;

        [XmlAttribute]
        public QuestType Type = QuestType.Daily;

		[XmlAttribute]
		public QuestPathType QuestPathType;

		[XmlIgnore]
        public int QuestId;

        public QuestState State;

		public bool IsView;

		[XmlIgnore]
		public List<string> TakeRewardList = new List<string>();

        public int TasksTotal
        {
            get
            {
                int total = 0;
                foreach (var t in Tasks)
                    if (t.AddToJobProgress)
                        total += t.Count;
                return total;
            }
            set { }
        }

        public int TasksDone
        {
            get
            {
                int done = 0;
                foreach (var t in Tasks)
                    if (t.AddToJobProgress)
                        done += t.Done;
                return done;
            }
            set { }
        }

	    private List<InventoryItemData> m_rewardList;

		//награды из этого списка не выдавать. тут общие итемы на весь сервер
		public List<InventoryItemData> RewardList
	    {
		    get
			{
				if (m_rewardList == null && Rewards != null)
				{
					var rewards = Rewards.FirstOrDefault(x => x.Count == null || x.Count == TasksTotal);
					m_rewardList = rewards?.GetItemList(null, null);
				}

				return m_rewardList;
		    }
	    }

		//Старые
        [XmlArrayItem(typeof(WaitEquipItem))]
        [XmlArrayItem(typeof(GiveStat))]
        [XmlArrayItem(typeof(UnlockLocation))]
        [XmlArrayItem(typeof(LightHouseActivate))]
        [XmlArrayItem(typeof(CollectResource))]
        [XmlArrayItem(typeof(CollectGold))]
        [XmlArrayItem(typeof(GiveGold))]
        [XmlArrayItem(typeof(GiveLoot))]
		[XmlArrayItem(typeof(NearActiveMapObject))]
		[XmlArrayItem(typeof(GetMail))]
		[XmlArrayItem(typeof(Hit))]
		[XmlArrayItem(typeof(CollectItemInBag))]
        [XmlArrayItem(typeof(VisitLocation))]
        [XmlArrayItem(typeof(VisitAnyLocation))]
        [XmlArrayItem(typeof(DamagePerson))]
        [XmlArrayItem(typeof(QuestTaskData))]
		//Новые
		[XmlArrayItem(typeof(BuyItem))]
		[XmlArrayItem(typeof(CompleteQuests))]
		[XmlArrayItem(typeof(GetGloryPoint))]
		[XmlArrayItem(typeof(GetItem))]
		[XmlArrayItem(typeof(GetСurrency))]
		[XmlArrayItem(typeof(KillCharacter))]
		[XmlArrayItem(typeof(KillMonster))]
		[XmlArrayItem(typeof(OpenChest))]
		[XmlArrayItem(typeof(PlayMatch))]
		[XmlArrayItem(typeof(RefineEquipment))]
		[XmlArrayItem(typeof(SpendСurrency))]
		[XmlArrayItem(typeof(UseItem))]
		[XmlArrayItem(typeof(WinMatch))]
		public List<QuestTaskData> Tasks { get; set; }

        public class QuestLootList : LootList
		{
			[XmlAttribute]
			public string rewardId;

			[XmlAttribute]
			public int? Count;

			[XmlAttribute]
			public List<string> autoreward;

			[XmlAttribute]
			public ArenaRewardChestType ChestType = ArenaRewardChestType.None;

			[XmlAttribute]
			public string DescName;

			[XmlAttribute]
			public List<string> Notification;
		}

		[XmlElement("Rewards")]
		public List<QuestLootList> Rewards;

		public QuestProgress GetQuestProgress()
        {
            var qp = new QuestProgress();
            qp.Name = Name;
            qp.State = State;
	        qp.IsView = IsView;
			qp.TakeRewardList = TakeRewardList;
			qp.progress = Tasks.Select(t => t.Done).ToList();

            return qp;
        }

		[XmlIgnore]
	    public LobbyLogin owner { get; private set; }

	    bool inited;
        public void Init(LobbyLogin p)
        {
            if (State == QuestState.Active)
            {
                Sos.Debug("QuestItemData.Init");
                owner = p;
                foreach (var t in Tasks)
                {
                    t.owner = p;
                    t.quest = this;
                    if (!t.Complete || t.CanRollback)
                    {
                        t.Init();
                    }
                }
				inited = true;
            }
        }

        public void Dispose()
        {
            if (inited)
            {
                foreach (var t in Tasks)
                {
                    t.Dispose();
                }
            }
        }

        public List<InventoryItemData> GiveReward(InstancePerson p)
        {
			var rewards = Rewards.FirstOrDefault(x => x.Count == null || x.Count == TasksTotal);

			var reward = rewards?.GetItemList(p.syn, p);

            ServerRpcListener.Instance.SendCurrencyResponse(p);
			return reward;
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            QuestItemData si = (QuestItemData)value;
            stream.Write<int>(si.QuestId);
            stream.Write<string>(si.TechnicalName);
            stream.Write<string>(si.IconName);
			stream.Write<int>((int)si.State);
            stream.Write<int>(si.TasksTotal);
            stream.Write<int>(si.TasksDone);
            stream.Write<List<InventoryItemData>>(si.RewardList);
			stream.Write<bool>(si.IsView);
			stream.Write(si.QuestPathType);
		}

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            return null;
        }

        public override string ToString()
        {
            return Name;
        }

        
        internal void IncrementProgress(QuestTaskData taskData)
        {
			if (taskData.Complete && !taskData.CanRollback)
				taskData.Dispose();

            bool success = true;
            foreach (var t in Tasks)
            {
                if (!t.Complete && !t.IsOptional)
                    success = false;
            }

            var map = InstanceGameManager.Instance.GetMapByGameId(owner.Person.GameId);
            

            if (success)
            {
				if (State == QuestState.Completed)
				{
					ILogger.Instance.Send("QuestItemData.IncrementProgress: дернули второй раз для выполненого квеста", ErrorLevel.warning);
					return;
				}

                State = QuestState.Completed;
                Sos.Debug("IncrementProgress State = QuestState.Completed");
                owner.Person.OnCompleteQuests(this);

                map.OnQuestDone(this, owner.Person);

                foreach (var t in Tasks)
					if (t.CanRollback)
						taskData.Dispose();
			}
			else
			{
				owner.Person.OnProgressQuests(taskData);
                map.OnQuestProgress(this, owner.Person);
            }
		}
    }

	public class LocalizationAttributeQuest
	{
		[XmlAttribute]
		public string Key = string.Empty;

		[XmlAttribute]
		public string Text
		{
			get { return formula.Text; }
			set { formula = new FormulaX("Done Total", value); }
		}

		[XmlIgnore]
		public FormulaX formula = new FormulaX("Done Total", "0");
	}
}