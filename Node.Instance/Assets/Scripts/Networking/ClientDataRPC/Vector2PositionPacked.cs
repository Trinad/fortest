﻿using System.Numerics;
using LobbyInstanceLib.Networking;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public struct Vector2PositionPacked
    {
		public Vector2 Position { get; }

		public Vector2PositionPacked(Vector2 pos)
		{
			Position = pos;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (Vector2PositionPacked)value;
			stream.WriteVec2Short(si.Position);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			Vector2 position = stream.ReadVector2Short();
			var si = new Vector2PositionPacked(position);
			return si;
		}
	}
}
