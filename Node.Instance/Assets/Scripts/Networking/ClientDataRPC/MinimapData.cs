﻿using System;
using System.Numerics;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using LobbyInstanceLib.Networking;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class MinimapData
	{
		public int Id { get; private set; }
		public Vector2 Position { get; private set; }
        public MinimapItemType Type { get; private set; }

        public MinimapData()
	    {
	    }

	    public MinimapData(MapTargetObject mapTargetObject, MinimapItemType minimapItemType)
	    {
            Id = mapTargetObject.Id;
            Type = minimapItemType;
            Position = new Vector2(mapTargetObject.position.X, mapTargetObject.position.Z);
        }

		public MinimapData(MinimapItemType minimapItemType, Vector2 position)
        {
            Id = -1;
            Type = minimapItemType;
            Position = position;
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			MinimapData si = (MinimapData)value;
			stream.Write<int>(si.Id);
			stream.WriteVec2Short(si.Position);
			stream.Write<sbyte>((sbyte)si.Type);
		}

		public override string ToString()
		{
			return Id.ToString();
		}
	}
}
