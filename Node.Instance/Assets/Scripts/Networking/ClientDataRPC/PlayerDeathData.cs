﻿using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class PlayerDeathData
	{
		public string nickName = string.Empty;
        public string technicalName = string.Empty;
        public string avatar;
		public double timeInSeconds;
		public int entityId;
		public EntityType eType;
		public DeathReasonType deathReason;

		public int killerGS = -1;
		public int killerElo = -1;
		public int killerLevel = -1;

		public PlayerDeathData(InstancePerson instancePerson, MapTargetObject attacker, float ressurectSec)
		{
			if (attacker != null)
			{
                nickName = attacker.NickName;
                if (attacker is MonsterBehaviour monster)
                    technicalName = monster.TechnicalName;
				avatar = attacker.GetAvatar();
				timeInSeconds = ressurectSec;
				entityId = attacker.Id;
				eType = attacker.eType;
				deathReason = attacker.DeathReason;

				int gearScore = -1;
				int elo = -1;

				var statContainer = attacker.GetStatContainer();
				if (statContainer != null)
				{
					gearScore = (int)statContainer.GetStatValue("HPOW");
					elo = (int)statContainer.GetStatValue(Stats.EloRatingTeam);
				}

				killerGS = gearScore;
				killerElo = elo;
				
				killerLevel = attacker.Level;
			}
			else
			{
                nickName = "None";
				avatar = "None";
				timeInSeconds = ressurectSec;
				entityId = -1;
				eType = EntityType.Object;
				deathReason = DeathReasonType.None;
				ILogger.Instance.Send($"PlayerDeathData mapTargetObject == null personId: {instancePerson.personId}", ErrorLevel.warning);
			}
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			PlayerDeathData playerDeathData = (PlayerDeathData)value;

			stream.Write(playerDeathData.nickName);
            stream.Write(playerDeathData.technicalName);
            stream.Write(playerDeathData.avatar);
			stream.Write(playerDeathData.timeInSeconds);
			stream.Write(playerDeathData.entityId);
			stream.Write(playerDeathData.eType);
			stream.Write(playerDeathData.deathReason);

			stream.Write(playerDeathData.killerGS);
			stream.Write(playerDeathData.killerElo);
			stream.Write(playerDeathData.killerLevel);
		}
	}
}
