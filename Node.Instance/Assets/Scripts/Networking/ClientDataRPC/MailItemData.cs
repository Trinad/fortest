﻿using System;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class MailItemData : MailData
	{
		public MailItemData()
		{
		}

		public MailItemData(int personId, MailType mailType, int loginId = 0)
	    {
	        PersonId = personId;
		    MailType = mailType;
	        LoginId = loginId;
	    }

        public int GetRemaningTimeSec()
        {
            return (int)(RemoveTime - TimeProvider.UTCNow).TotalSeconds;
        }

  //      public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		//{
		//	MailItemData si = (MailItemData)value;
		//	stream.Write(si.MailId);
		//	stream.Write((int)si.MailType);
  //          bool haveItem = si.LostReward != null;
  //          stream.Write(haveItem);
		//	if (haveItem)
		//	{
		//		var inventoryItem = ItemGenerator.Instance.GetItem(si.LostReward);
		//		stream.Write(inventoryItem);
		//	}
		//	stream.Write(si.GetRemaningTimeSec());
  //          stream.Write(string.IsNullOrEmpty(si.TitleKey) ? string.Empty : si.TitleKey);
  //          stream.Write(si.TitleParams ?? new Dictionary<string, string>());
  //          stream.Write(string.IsNullOrEmpty(si.BodyKey) ? string.Empty : si.BodyKey);
  //          stream.Write(si.BodyParams ?? new Dictionary<string, string>());
		//}

		public override string ToString()
		{
			return "id " + MailId.ToString() + " type " + MailType.ToString();
		}
    }
}
