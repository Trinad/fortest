﻿namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class UpgradeItemData
	{
		public int ItemId;
		public bool Upgrage;

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			UpgradeItemData si = (UpgradeItemData)value;
			stream.Write(si.ItemId);
			stream.Write(si.Upgrage);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			UpgradeItemData si = new UpgradeItemData();
			si.ItemId = stream.Read<int>();
			si.Upgrage = stream.Read<bool>();
			return si;
		}
	}
}
