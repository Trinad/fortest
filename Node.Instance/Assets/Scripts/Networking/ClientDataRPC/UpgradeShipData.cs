﻿using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class UpgradeShipData
    {
        public MapObjectTitle TitleId = MapObjectTitle.None;
        public ActualPlaceIcon IconId = ActualPlaceIcon.None;
        Dictionary<string, float> numParams = new Dictionary<string, float>();
        Dictionary<string, string> strParams = new Dictionary<string, string>();

        UpgradeShipData() { }
        public UpgradeShipData(UpgradeShipElementData elementData)
        {
            IconId = elementData.PlaceIcon;
            TitleId = elementData.ObjectTitle;

            foreach (var el in elementData.parametreInfoData)
            {
                if (float.TryParse(el.Val, out float value))
                    numParams.Add(el.Key, value);
                else
                    strParams.Add(el.Key, el.Val);
            }
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            UpgradeShipData si = (UpgradeShipData)value;
            stream.Write<int>((int)si.TitleId);
            stream.Write<int>((int)si.IconId);
            stream.Write<Dictionary<string, float>>(si.numParams);
            stream.Write<Dictionary<string, string>>(si.strParams);
        }
    }
}
