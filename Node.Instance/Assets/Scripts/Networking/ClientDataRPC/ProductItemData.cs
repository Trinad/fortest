﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class ProductItemData
    {
        [XmlAttribute]
		public string IconId;
		[XmlAttribute]
		public string TechnicalName;
        [XmlAttribute]
        public string ItemName;

        [XmlAttribute]
        public int BaseId;

        [XmlAttribute]
        public int Count;
        [XmlAttribute]
        public float Price;
        [XmlAttribute]
        public bool InApp;
        [XmlAttribute]
        public string Bonus;

        public string Sku;

        // Новые параметры
        [XmlAttribute]
		public ShopSection ShopSection; // К какой вкладке магазина относится товар

		[XmlAttribute]
		public List<string> PaymentPlatforms; // На каких платформах можно покупать

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            ProductItemData si = (ProductItemData)value;
            stream.Write(si.BaseId);
            stream.Write(si.IconId);
            stream.Write(si.TechnicalName);
            stream.Write(si.Price);
            stream.Write(si.Count);
            stream.Write(si.Bonus);
            stream.Write(si.InApp);
            if (si.InApp)
            {
                stream.Write(si.Sku);
				stream.Write(0);// si.VipPoint
				stream.Write(0);// si.VipBonus
            }
            stream.Write(si.ShopSection);
        }

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            ProductItemData si = new ProductItemData();
            si.BaseId = stream.Read<int>();
            si.IconId = stream.Read<string>();
            si.TechnicalName = stream.Read<string>();
            si.Price = stream.Read<int>();
            si.Count = stream.Read<int>();
            si.Bonus = stream.Read<string>();
            si.InApp = stream.Read<bool>();
            if (si.InApp)
            {
                si.Sku = stream.Read<string>();
                /*si.VipPoint =*/ stream.Read<int>();
                /*si.VipBonus =*/ stream.Read<int>();
            }
	        si.ShopSection = stream.Read<ShopSection>();
            return si;
        }

        public override string ToString()
        {
            return TechnicalName;
        }

		public bool Check(string priceType, string currency)
		{
			return PaymentPlatforms.Count == 0 || PaymentPlatforms.Exists(x => x == priceType);
		}
    }

    public enum ShopSection
    {
        None = -1,
        Emeralds,
        Exchange,
        Gold,
        Sets,
        Ads,
    }

    public enum ShopDiscountType
    {
        None = -1,
        Discount,
        Benefit,
    }

    public enum SpecialOfferType
    {
        None = -1,
        FirstBuy,
        BestOffer,
        PlayerChoice
    }
}
