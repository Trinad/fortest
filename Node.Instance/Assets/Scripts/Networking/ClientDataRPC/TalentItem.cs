﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class TalentItem
    {
        public int id;
        public int level;

        public TalentItem(int id, int level)
        {
            this.id = id;
            this.level = level;
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            TalentItem si = (TalentItem)value;
            stream.Write(si.id);
            stream.Write(si.level);
        }
    }
}
