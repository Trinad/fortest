﻿using System.Collections.Generic;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class ErrorData
	{
		public ClientErrors Error { get; set; }
		public List<InventoryItemData> ItemsNeeded { get; set; }

		public ErrorData(ClientErrors error = ClientErrors.SUCCESS)
		{
			Error = error;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			ErrorData si = (ErrorData)value;
			stream.Write<int>((int)si.Error);
			if (si.Error != ClientErrors.SUCCESS)
			{
				stream.Write(si.ItemsNeeded);
			}
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			return null;
		}

		public override string ToString()
		{
			return string.Format("Error = {0}", Error);
		}
	}
}
