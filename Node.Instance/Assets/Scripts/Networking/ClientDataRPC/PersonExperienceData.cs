﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class PersonExperienceData
    {
        public bool NewLevel;
        public int Level;
        public int Exp;
        public int GoldAdd;
        public int EmeraldsAdd;
        public int OldElo;
        public int DifElo;
        public int OldGlory;
        public int Glory;

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            PersonExperienceData si = (PersonExperienceData)value;
            stream.Write<bool>(si.NewLevel);
            stream.Write<int>(si.Level);
            stream.Write<int>(si.Exp);
            stream.Write<int>(si.GoldAdd);
            stream.Write<int>(si.EmeraldsAdd);
            stream.Write<int>(si.OldElo);
            stream.Write<int>(si.DifElo);
            stream.Write<int>(si.OldGlory);
            stream.Write<int>(si.Glory);
        }

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            PersonExperienceData si = new PersonExperienceData();
            si.NewLevel = stream.Read<bool>();
            si.Level = stream.Read<int>();
            si.Exp = stream.Read<int>();
            si.GoldAdd = stream.Read<int>();
            si.EmeraldsAdd = stream.Read<int>();
            si.OldElo = stream.Read<int>();
            si.DifElo = stream.Read<int>();
            si.OldGlory = stream.Read<int>();
            si.Glory = stream.Read<int>();
            return si;
        }
    }
}
