﻿
using Assets.Scripts.InstanceServer;
using System;
using System.Collections.Generic;
using System.Linq;
using Batyi.TinyServer;
using UtilsLib.DegSerializers;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using System.Collections;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.XmlData.Trade;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class ItemSlot
    {
        public Slots Slot;
        public int PlaceId;
        public InventoryItemData Item;
		public string LastBaseItemId;
		public bool Locked;

        public InventoryItemData RemoveItem()
        {
            if (Item != null)
            {
                var item = Item;
                Item = null;
                return item;
            }
            return null;
        }
    }

    public class ItemsStorage
    {
        private ItemSlot[] slots;
        private ItemDataRepository repo;

        public ItemsStorage(ItemDataRepository repo, int size)
        {
            this.repo = repo;
            slots = new ItemSlot[size];
            for (int i = 0; i < size; i++)
            {
                slots[i] = new ItemSlot();
                slots[i].PlaceId = i;
            }
        }

        public IEnumerable<InventoryItemData> GetInventoryItems(bool applyEmpty = false)
        {
            foreach (var slot in slots)
            {
                if (slot.Locked)
                    continue;

                if (slot.Item != null)
                {
                    yield return slot.Item;
                }
                else if (applyEmpty )
                {
                    var item = new InventoryItemData();// ItemGenerator.Instance.GetItem(slot.Locked ? ItemType.Locked : ItemType.None);
                    item.TradeInfos = new TradeInfoList()
                    {
                        list = new List<TradeInfoData>() { new TradeInfoData() }
                    };

                    item.SlotId = slot.Slot;
                    yield return item;
                }
            }
        }

        public ItemsStorage(ItemDataRepository repo, params Slots[] slotNames)
        {
            this.repo = repo;
            slots = new ItemSlot[slotNames.Length];
            for (int i = 0; i < slotNames.Length; i++)
            {
                slots[i] = new ItemSlot();
                slots[i].PlaceId = i;
                slots[i].Slot = slotNames[i];
            }
        }

		public void SetLockedSize(int size)
		{
			for (int i = 0; i < slots.Length; i++)
				slots[i].Locked = i >= size;
		}

		public bool IsLocked(Slots slotType)
		{
			foreach (var slot in slots)
				if (slot.Slot == slotType)
					return slot.Locked;

			return true;
		}

        public void ForEachItem(Action<InventoryItemData> action)
        {
            foreach (var slot in slots)
                if (slot.Item != null)
                    action(slot.Item);
        }

        public void ForEachSlot(Action<ItemSlot> action)
        {
            foreach (var slot in slots)
                action(slot);
        }

        internal List<ItemSaveData> GetItemsForSave(bool saveEmpty = false)
        {
            List<ItemSaveData> res = new List<ItemSaveData>();

            foreach (var slot in slots)
                if (slot.Item != null && slot.Item.IsItem)
                    res.Add(slot.Item.GetItemSaveData());
                else if(saveEmpty)
                {
                    res.Add(null);
                }

            return res;
        }

		internal List<string> LastItems
		{
			get { return slots.Select(x => x.LastBaseItemId).ToList(); }
			set
			{
				foreach (var slot in slots)
					if (slot.PlaceId < value.Count)
						slot.LastBaseItemId = value[slot.PlaceId];
			}
		}

        internal List<InventoryItemData> GetAllByBaseId(string baseId)
        {
            List<InventoryItemData> res = new List<InventoryItemData>();

            foreach (var slot in slots)
                if (slot.Item != null && slot.Item.Name == baseId)
                    res.Add(slot.Item);

            return res;
        }

        public void Add(List<ItemSaveData> itemsPotions, bool applyEmpty = false)
        {
            int i = 0;
            foreach (ItemSaveData item in itemsPotions)
            {
                if (applyEmpty && item == null)
                {
                    i++;
					continue;
                }

                try
                {
                    var realItem = repo.GetCopyInventoryItemData(item.Name, item.Count);// ItemGenerator.Instance.GetItem(item);
					realItem.IsNew = item.IsNew;

                    var slot = slots[i++];
                    slot.Item = realItem;
                    slot.LastBaseItemId = realItem.Name;
                    realItem.SlotId = slot.Slot;
                    realItem.Equiped = true;
                }
                catch (Exception ex)
                {
                    ILogger.Instance.Send($"Не валидные данные по предмету. Предмет будет уничтожен. Ошибка: {ex.ToString()}", ErrorLevel.exception);
                }
            }
        }

        internal InventoryItemData GetItemById(int itemId)
        {
            foreach (var slot in slots)
                if (slot.Item != null && slot.Item.ItemId == itemId)
                    return slot.Item;

            return null;
        }

		internal InventoryItemData GetItemByBaseId(string itemBaseId)
		{
			foreach (var slot in slots)
				if (slot.Item != null && slot.Item.Name == itemBaseId)
					return slot.Item;

			return null;
		}

        internal InventoryItemData GetItemByType(ItemType itemType)
        {
            InventoryItemData findedItem = null;
            foreach (var slot in slots)
                if (slot.Item != null && slot.Item.ItemType == itemType)
                    findedItem = slot.Item;
            
            return findedItem;
        }

        public void ClearEmptySlots()
        {
            foreach (var slot in slots)
                if (slot.Item != null && slot.Item.Count <= 0)
                    slot.Item = null;
        }

        public bool RemoveItem(int itemId, int count)
        {
            ItemSlot slot = null;
            foreach (var s in slots)
                if (s.Item != null && s.Item.ItemId == itemId)
                    slot = s;

            if (slot == null)
                return false;

            if (slot.Item.Count < count)
                throw new Exception("RemoveItem Item.Count < 0");

            slot.Item.Count -= count;
            if (slot.Item.Count == 0)
            {
                slot.Item.Equiped = false;
                slot.Item = null;
            }

            return true;
        }

        public bool RemoveItem(InventoryItemData item)
        {
            if (item == null)
                return false;

            if (!item.Equiped)
                return false;

            foreach (var s in slots)
                if (s.Item == item)
                {
                    s.Item.Equiped = false;
                    s.Item = null;
                    return true;
                }

            return false;
        }

        public InventoryItemData RemoveItem(Slots slotType)
        {
            foreach (var slot in slots)
                if (slot.Item != null && slot.Slot == slotType)
                {
                    var item = slot.Item;
                    item.Equiped = false;
                    slot.Item = null;
                    return item;
                }

            return null;
        }

        internal InventoryItemData GetItemBySlot(Slots slotType)
        {
            foreach (var slot in slots)
                if (slot.Item != null && slot.Slot == slotType)
                    return slot.Item;

            return null;
        }

        public void AddItem(InventoryItemData item, Slots slotType)
        {
            if(item.Equiped)
                throw new Exception("AddItem item.Equiped");

            foreach (var slot in slots)
                if (slot.Slot == slotType)
                {
					if (slot.Item != null)
						throw new Exception("AddItem slot.Item != null");

                    slot.Item = item;
					slot.LastBaseItemId = item.Name;
                    slot.Item.SlotId = slotType;
                    slot.Item.Equiped = true;
                }
        }

		internal bool AddItemInEmptySlot(InventoryItemData item)
		{
			foreach (var slot in slots)
				if (slot.Item == null)
				{
					slot.Item = item;
					slot.LastBaseItemId = item.Name;
					slot.Item.SlotId = slot.Slot;
					slot.Item.Equiped = true;
					return true;
				}

			return false;
		}

        public void Clear()
        {
            foreach (var slot in slots)
                if (slot.Item != null)
                {
                    var item = slot.Item;
                    item.Equiped = false;
                    slot.Item = null;
                }
        }
    }
}
