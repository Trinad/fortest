﻿namespace Assets.Scripts.Networking.ClientDataRPC
{
    public enum UnlockType
    {
        Location = 0,
        Abilities = 1,
		LocationShip = 2,
	}

    public enum UnlockTaskType
    {
        None = -1,
        GoldBuy = 0, // разблокировать за золото
        Kill = 1, // пройти этаж подземелья (убив босса)
        GearScore = 2, // набрать определенное значение GS
        Visit = 3, // посетить указанную локацию
        BuyKeyStone = 4, // разблокировать за кейстоуны
        LastDungeon = 5, // разблокируется, когда открыты все данжи
        SparkBuy = 6,// разблокировать за кубы
		Collect = 7,// собрать коллекцию
		Participate = 8, //Принять участие в режиме "{*}"
		Level = 9, //Получить уровень {*}
		SingleWin = 10, //Победить в режиме "{*}"
		Wins = 11, //Одержать {*} побед в режиме "{*}"
		Rating = 12, //Войти в топ {*} в режиме "{*}"
		KillBoss = 13, //Убить босса в режиме "{*}"
		Skill = 14,//Навыков освоено
		UpgradeItem = 15, //Улучшить любую вещь
		UpgradeItemLevel = 16, //Улучшить любую вещь до +{*}
		MaxLevel = 17, //Максимальный уровень персонажей
        Glory = 18, //Количество очков Славы
    }

	//Для UnlockTaskType.Participate
	//{"arena_group_0", 1343}, //Одиночная игра
	//{"arena_group_1", 1344}, //Командная игра
	//{"arena_group_2", 1345}, //Кооператив
	//{"arena_match_0", 1346}, //Каждый за себя
	//{"arena_match_1", 1347}, //Выживание
	//{"arena_match_2", 1348}, //Преследователь
	//{"arena_match_100", 1349}, //Командная дуэль
	//{"arena_match_101", 1350}, //Захват флага
	//{"arena_match_102", 1351}, //Удержание точек
	//{"arena_match_200", 1352}, //Убийство боссов
	//{"arena_match_201", 1353}, //Волны монстров

    public class UnlockData
    {
		public UnlockTaskType TaskType;
		public int TaskGoal;
		public bool isDone;
		public string TechnicalName;

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            UnlockData si = (UnlockData)value;
            stream.Write(si.TaskType);
            stream.Write(si.TaskGoal);
            stream.Write(si.isDone);
            stream.Write(si.TechnicalName);
        }

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            UnlockData si = new UnlockData();
            si.TaskType = stream.Read<UnlockTaskType>();
            si.TaskGoal = stream.Read<int>();
            si.isDone = stream.Read<bool>();
            si.TechnicalName = stream.Read<string>();
            return si;
        }
    }
}
