﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Utils;
using LobbyInstanceLib.Networking;
using UtilsLib.Logic.Enums;
using UtilsLib.Logic;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class PlayerData
	{
		public int CharacterId { get; private set; }
		public int InventoryId { get; private set; }
		public string PrefabName { get; private set; }
		public string NickName { get; private set; }
		public Vector3 Position { get; private set; }
		public Vector2 Direction { get; private set; }
		public uLink.NetworkViewID ViewId { get; private set; }
		public float Speed { get; private set; }
		public PersonType PersonType { get; private set; }
		public CharacterType CharacterType { get; private set; }
		public FractionType Fraction { get; private set; }
		public float Radius { get; private set; }
		public float Height { get; private set; }
		public List<EntityPartInfo> Parts { get; set; }
        public int Level { get; private set; }
        public int GuildId { get; private set; }
        public ShortClanData GuildData { get; set; }
        public int colorName { get; set; }//-1 - белый, 1 - красный, 0 - синий
        public bool IsDead;
		public float ResurrectTime;
		public float CurrentResurrectTime;
        public float SendRate = PersonBehaviour.SEND_RATE;
        public bool isNeedHideHelmet;
        public HealthTypeVisual healthTypeVisual;
        public float maxLifeTime;
        public float remainingLifeTime;
        public int health;
        public int maxHealth;
        public bool isTargetable;

        public PlayerData()
        {
        }

        public PlayerData(InstancePerson person)
		{
			CharacterId = person.syn.Id;
			InventoryId = person.syn.InventoryId;
			PrefabName = person.syn.TargetName;
			NickName = person.personName;
			Position = person.syn.position;
			Direction = person.syn.direction;
			ViewId = person.syn.view.viewID;
			Speed = person.syn.speed;
			PersonType = person.personType;
			CharacterType = person.syn.characterType;
			Fraction = person.syn.fraction;
			Radius = person.syn.Radius;
			Height = person.syn.Height;
			Parts = person.EquippedWears;
            Level = person.HeroPower;
            IsDead = person.syn.IsDead;
			ResurrectTime = person.ResurrectSec;
			CurrentResurrectTime = person.GetResurrectTime();
			colorName = (int)person.syn.TeamColor;
            GuildId = person.GuildId;

            if (GuildId > 0)
            {
                GuildData = new ShortClanData(Guild.GetGuildDataById(GuildId), person.GuildRole);
            }
            isNeedHideHelmet = person.isNeedHideHelmet;
            healthTypeVisual = person.syn.healthTypeVisual;
            maxLifeTime = person.syn.maxLifeTime;
            remainingLifeTime = person.syn.RemainingLifeTime;
            health = person.syn.Health;
            maxHealth = person.syn.MaxHealth;
            isTargetable = person.syn.isTargetable;
        }

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			PlayerData si = (PlayerData)value;
			stream.Write<int>(si.CharacterId);
			stream.Write<int>(si.InventoryId);
			stream.Write<string>(si.PrefabName);
			stream.Write<string>(si.NickName);
			stream.WriteVec3Short(si.Position);
			stream.WriteDirectionByteAngle(si.Direction);
			stream.WriteNetworkViewID(si.ViewId);
			stream.Write<float>(si.Speed);
			stream.WriteSByte((sbyte)si.PersonType);
			stream.WriteSByte((sbyte)si.CharacterType);
			stream.WriteSByte((sbyte)si.Fraction);
			stream.Write(si.Radius);
			stream.Write(si.Height);
			stream.Write(si.Parts);
            stream.Write(si.Level);
            stream.Write(si.colorName);
            stream.Write<int>(si.GuildId);
            if (si.GuildId > 0)
                stream.Write(si.GuildData);
            stream.Write(si.IsDead);
			if (si.IsDead)
			{
				stream.Write(si.ResurrectTime);
				stream.Write(si.CurrentResurrectTime);
			}
            stream.Write(si.SendRate);
            stream.Write(si.isNeedHideHelmet);
            stream.Write(si.healthTypeVisual);

            stream.Write(si.maxLifeTime);
            stream.Write(si.remainingLifeTime);

            stream.Write(si.health);
            stream.Write(si.maxHealth);
            stream.Write(si.isTargetable);
        }

		public override string ToString()
		{
			return CharacterId.ToString();
		}
	}
}
