﻿using System;
using Assets.Scripts.InstanceServer.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public sealed class LargeMapMonsterData
    {
		public string Name { get; set; }
		public string TechnicalName { get; set; }
		public int Level { get; set; }
		public Elements Element { get; set; }

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			LargeMapMonsterData si = (LargeMapMonsterData)value;
			stream.Write<string>(si.Name);
			stream.Write<string>(si.TechnicalName);
			stream.Write<byte>((byte)si.Level);
			stream.Write<byte>((byte)si.Element);
		}
	}
}
