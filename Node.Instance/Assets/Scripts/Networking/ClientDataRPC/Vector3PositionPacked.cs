﻿using System.Numerics;
using LobbyInstanceLib.Networking;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public struct Vector3PositionPacked
    {
		public Vector3 Position { get; }

		public Vector3PositionPacked(Vector3 pos)
		{
			Position = pos;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (Vector3PositionPacked)value;
			stream.WriteVec3Short(si.Position);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			Vector3 position = stream.ReadVector3Short();
			var si = new Vector3PositionPacked(position);
			return si;
		}
	}
}
