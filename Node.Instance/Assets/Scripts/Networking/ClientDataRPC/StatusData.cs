﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.InstanceServer.Entries.Influences;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class StatusData
    {
        public StatusData()
        {

        }

        public int Id;
        public PlayerStatus Icon;
        public InfluenceHintType HintType;
        public float TotalTime;
        public float Elapsed;
        public bool IsFull;
        public int CountInQueue;
        public int CountInStack;
        public List<LocParameter> Attributes;
        public string TechnicalName;
		public StatInfluence infl;

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            StatusData si = (StatusData)value;
            stream.Write<PlayerStatus>(si.Icon);
            stream.Write<InfluenceHintType>(si.HintType);
            stream.Write<int>((int)si.Id);
            stream.Write<float>(si.TotalTime);
            stream.Write<float>(si.Elapsed);
            stream.Write<bool>(si.IsFull);
            stream.Write<int>(si.CountInQueue);
            stream.Write<int>(si.CountInStack);
            stream.Write<List<LocParameter>>(si.Attributes);
            stream.WriteStringFromId(si.TechnicalName);
        }

        public override string ToString()
        {
            return string.Format("Icon = {0}", Icon);
        }
    }

    public class LocParameter
    {
        public string KeyName;
        public float Value;

        public LocParameter()
        {

        }

        public LocParameter(string keyName, float value)
        {
            KeyName = keyName;
            Value = value;
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            LocParameter si = (LocParameter)value;
            stream.WriteStringFromId(si.KeyName);
            stream.Write<float>(si.Value);
        }

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            LocParameter si = new LocParameter();
            si.KeyName = stream.ReadStringFromId();
            si.Value = stream.Read<float>();
            return si;
        }
    }
}
