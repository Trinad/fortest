﻿using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Enums;
using Batyi.TinyServer;
using System.Collections.Generic;
using System.Linq;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class DailyQuestCommonData
	{
		public List<QuestItemData> DailyQuestList;
		public List<DailyQuestRewardData> DailyRewardData;
		public int QuestRefreshTime;
		public int QuestRefreshCounter;

		public DailyQuestCommonData(InstancePerson p, int questRefreshTime, QuestItemData quest)
		{
			string sQuestRefresh = p.login.scenarioStrings.Get("QuestRefresh");
			if (!int.TryParse(sQuestRefresh, out var iQuestRefresh))
				iQuestRefresh = 0;

			DailyQuestList = p.ActiveQuest.Where(x => x.Type == QuestType.Daily).ToList();
			DailyRewardData = quest.Rewards.Select(reward => new DailyQuestRewardData(p, quest, reward)).ToList();
			QuestRefreshTime = questRefreshTime + 1;
			QuestRefreshCounter = iQuestRefresh;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (DailyQuestCommonData)value;
			stream.Write(si.DailyQuestList);
			stream.Write(si.DailyRewardData);
			stream.Write(si.QuestRefreshTime);
			stream.Write(si.QuestRefreshCounter);
		}
	}

	public class DailyQuestRewardData
	{
		public int RewardId;
		public ArenaRewardChestType ChestType = ArenaRewardChestType.None;
		public string ChestDescription;
		public List<InventoryItemData> DailyRewardList;
		public QuestState RewardState;
		public int ProgressScore;

		public DailyQuestRewardData(InstancePerson p, QuestItemData quest, QuestItemData.QuestLootList reward)
		{
			int.TryParse(reward.rewardId, out RewardId);
			ProgressScore = reward.Count ?? quest.TasksTotal;

            if (quest.TakeRewardList.Contains(reward.rewardId))
                RewardState = QuestState.RewardTaken;
            else if (ProgressScore <= quest.TasksDone)
            {
                RewardState = QuestState.Completed;
            }
            else
                RewardState = QuestState.Active;

			if (reward.ChestType != ArenaRewardChestType.None)
			{
				ChestType = reward.ChestType;
				ChestDescription = reward.DescName;
				DailyRewardList = new List<InventoryItemData>();
			}
			else
			{
				DailyRewardList = reward.GetItemList(null, p);
			}
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			DailyQuestRewardData si = (DailyQuestRewardData)value;
			stream.Write(si.RewardId);
			stream.Write(si.ChestType);
			stream.Write(si.ChestDescription);
			stream.Write(si.DailyRewardList);
			stream.Write(si.RewardState);
			stream.Write(si.ProgressScore);
		}
	}
}