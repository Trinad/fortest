﻿using LobbyInstanceLib.Networking;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public struct FloatPacked
	{
		public float Float { get; }

		public FloatPacked(float f)
		{
			Float = f;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (FloatPacked)value;
			stream.WriteFloatPacked(si.Float);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			float f = stream.ReadFloatPacked();
			var si = new FloatPacked(f);
			return si;
		}
	}
}
