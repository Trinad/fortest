﻿namespace Node.Instance.Assets.Scripts.Networking.ClientDataRPC
{
	public enum CollectionStatus : byte
	{
		None = 0,
		New = 1,
		Collected = 2,
	}

	public class CollectionData
	{
		public string IconId;
		public CollectionStatus Status;

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			CollectionData si = (CollectionData)value;
			stream.Write(si.IconId);
			stream.Write(si.Status);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			CollectionData si = new CollectionData();
			si.IconId = stream.Read<string>();
			si.Status = stream.Read<CollectionStatus>();
			return si;
		}
	}
}
