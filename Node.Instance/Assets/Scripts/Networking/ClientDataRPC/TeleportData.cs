﻿namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class TeleportData
	{
		public bool PvEOnly { get; set; }
		public int PvEStoneRequired { get; set; }
		public int PvEGSRequired { get; set; }
		public int PvPStoneRequired { get; set; }
		public int PvPGSRequired { get; set; }

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			TeleportData si = (TeleportData)value;
			stream.Write<bool>(si.PvEOnly);
			stream.Write<int>(si.PvEStoneRequired);
			stream.Write<int>(si.PvEGSRequired);
			if (!si.PvEOnly)
			{
				stream.Write<int>(si.PvPStoneRequired);
				stream.Write<int>(si.PvPGSRequired);
			}
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			TeleportData si = new TeleportData();
			si.PvEOnly = stream.Read<bool>();
			si.PvEStoneRequired = stream.Read<int>();
			si.PvEGSRequired = stream.Read<int>();
			if (!si.PvEOnly)
			{
				si.PvPStoneRequired = stream.Read<int>();
				si.PvPGSRequired = stream.Read<int>();
			}
			return si;
		}

		public override string ToString()
		{
			return PvEOnly.ToString();
		}
	}
}
