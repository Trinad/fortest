﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC.Arenas
{
	public abstract class ArenaPlayerInfoBase
	{
		public int PersonId { get; protected set; }
		public int EntityId { get; protected set; }
		public PersonType PersonType { get; protected set; }
		public int Place { get; set; }
		public string Name { get; protected set; }
		public string AvatarName { get; protected set; }
		public int Score { get; protected set; }
		public bool Disconnected { get; protected set; }
		public bool IsDead { get; protected set; }
		public PlayerTeam PlayerTeam { get; protected set; }
	}
}
