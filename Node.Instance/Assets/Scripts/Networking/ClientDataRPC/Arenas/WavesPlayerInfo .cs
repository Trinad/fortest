﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC.Arenas
{
	public class WavesPlayerInfo : ArenaPlayerInfoBase
	{
		public int LastCompletedWaveId { get; set; }

		public WavesPlayerInfo() { }
		public WavesPlayerInfo(ArenaPlayerInfo info)
		{
			//ArenaPlayerInfoBase
			PersonId = info.PersonId;
			EntityId = info.EntityId;
			Place = info.Place;
			Name = info.Name;
			Score = info.FullScore;
			AvatarName = info.AvatarName;
			Disconnected = info.Disconnected;
			IsDead = info.bDead;
			PlayerTeam = info.playerTeam;
			PersonType = info.PersonType;
			LastCompletedWaveId = info.LastComplitedWaveId;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			WavesPlayerInfo si = (WavesPlayerInfo)value;
			stream.Write<int>(si.PersonId);
			stream.Write<int>(si.EntityId);
			stream.Write<short>((short)si.Score);
			stream.Write<byte>((byte)si.Place);
			stream.Write<string>(si.Name);
			stream.Write<sbyte>((sbyte)si.PersonType);
			stream.Write<bool>(si.Disconnected);
			stream.Write<bool>(si.IsDead);
			stream.Write<sbyte>((sbyte)si.PlayerTeam);
			stream.Write<sbyte>((sbyte)si.LastCompletedWaveId);
			stream.Write<string>(si.AvatarName);
		}
		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			WavesPlayerInfo si = new WavesPlayerInfo();
			si.PersonId = stream.ReadInt32();
			si.EntityId = stream.ReadInt32();
			si.Score = stream.ReadInt16();
			si.Place = stream.ReadByte();
			si.Name = stream.ReadString();
			si.PersonType  = (PersonType)stream.ReadSByte();
			si.Disconnected = stream.ReadBoolean();
			si.IsDead = stream.ReadBoolean();
			si.PlayerTeam = (PlayerTeam)stream.ReadSByte();
			si.LastCompletedWaveId = stream.ReadSByte();
			si.AvatarName = stream.ReadString();
			return si;
		}
	}
}
