﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC.Arenas
{
	public class WavesResultPlayerInfo : ArenaResultPlayerInfoBase
	{
		public int LastCompletedWaveId { get; set; }
		public int WaveScore { get; set; }
		public int Kill { get; set; }
		public int KillScore { get; set; }
		public int Assist { get; set; }
		public int AssistsScore { get; set; }
		public int HealCount { get; set; }
		public int HealScore { get; set; }
		public uint DamageDealt { get; set; }
		public uint DamageReceived { get; set; }
		public int HealAmount { get; set; }

		public override int FullScore()
		{
			return KillScore + AssistsScore + HealScore + WaveScore;
		}

		public WavesResultPlayerInfo() { }

		public WavesResultPlayerInfo(ArenaPlayerInfo info)
		{
			//ArenaPlayerInfoBase
			PersonId = info.PersonId;
			Place = info.Place;
			Name = info.Name;
			AvatarName = info.AvatarName;
			Disconnected = info.Disconnected;
			IsDead = info.bDead;
			PlayerTeam = info.playerTeam;

			//ArenaResultPlayerInfoBase
			PersonType = info.PersonType;
			GearScore = info.GearScore;
			Level = info.Level;
			IsMVP = info.IsMVP;
			RatingScore = info.startRating;
			gameCount = info.gameCount;

			//WavesResultPlayerInfo
			LastCompletedWaveId = info.LastComplitedWaveId;
			WaveScore = info.WaveScore;
			Kill = info.Kills;
			KillScore = info.KillScore;
			Assist = info.Assists;
			AssistsScore = info.AssistsScore;
			HealCount = info.HealCount;
			HealScore = info.HealScore;
			DamageDealt = (uint)info.DamageDealed;
			DamageReceived = (uint)info.DamageReseaved;
			HealAmount = info.HealAmount;

			Score = FullScore();
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			WavesResultPlayerInfo si = (WavesResultPlayerInfo)value;
			stream.Write<int>(si.PersonId);
			stream.Write<byte>((byte)si.Kill);
			stream.Write<byte>((byte)si.Assist);
			stream.Write<short>((short)si.Score);
			stream.Write<byte>((byte)si.Place);
			stream.Write<string>(si.Name);
			stream.Write<bool>(si.Disconnected);
			stream.Write<bool>(si.IsDead);
			stream.Write<sbyte>((sbyte)si.PlayerTeam);
			stream.Write<sbyte>((sbyte)si.LastCompletedWaveId);
			stream.Write<uint>((uint)si.DamageDealt);
			stream.Write<uint>((uint)si.DamageReceived);
			stream.Write<sbyte>((sbyte)si.PersonType);
			stream.Write<ushort>((ushort)si.GearScore);
			stream.Write<byte>((byte)si.Level);
			stream.Write<ushort>((ushort)si.RatingScore);
			stream.Write<bool>(si.IsMVP);
			stream.Write<uint>((uint)si.HealAmount);
			stream.Write<string>(si.AvatarName);
			stream.Write<ushort>((ushort)si.KillScore);
			stream.Write<ushort>((ushort)si.AssistsScore);
			stream.Write<ushort>((ushort)si.HealCount);
			stream.Write<ushort>((ushort)si.HealScore);
			stream.Write<ushort>((ushort)si.WaveScore);
		}
		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			WavesResultPlayerInfo si = new WavesResultPlayerInfo();
			si.PersonId = stream.ReadInt32();
			si.Kill = stream.ReadByte();
			si.Assist = stream.ReadByte();
			si.Score = stream.ReadInt16();
			si.Place = stream.ReadByte();
			si.Name = stream.ReadString();
			si.Disconnected = stream.ReadBoolean();
			si.PlayerTeam = (PlayerTeam)stream.ReadSByte();
			si.LastCompletedWaveId = stream.ReadSByte();
			si.DamageDealt = stream.ReadUInt32();
			si.DamageReceived = stream.ReadUInt32();
			si.PersonType = (PersonType)stream.ReadSByte();
			si.GearScore = stream.ReadUInt16();
			si.Level = stream.ReadByte();
			si.RatingScore = stream.ReadUInt16();
			si.IsMVP = stream.ReadBoolean();
			si.HealAmount = (int)stream.ReadUInt32();
			si.AvatarName = stream.ReadString();
			si.KillScore = stream.ReadUInt16();
			si.AssistsScore = stream.ReadUInt16();
			si.HealCount = stream.ReadUInt16();
			si.HealScore = stream.ReadUInt16();
			si.WaveScore = stream.ReadUInt16();
			return si;
		}
	}
}
