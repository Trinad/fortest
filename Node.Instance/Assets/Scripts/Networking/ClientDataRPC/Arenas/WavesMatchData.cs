﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assets.Scripts.Networking.ClientDataRPC.Arenas
{
	public class WavesMatchData : MatchDataBase
	{
		public List<WavesPlayerInfo> Players { get;  set; }
		public int MonstersLeft { get;  set; }
		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			WavesMatchData si = (WavesMatchData)value;
			stream.Write<bool>(si.IsActive);
			stream.Write<float>(si.TimeSec);
			stream.Write<bool>(si.IsRoundStarted);
			stream.Write<byte>((byte)si.RoundId);
			stream.Write<List<WavesPlayerInfo>>(si.Players);
			stream.Write<short>((short)si.MonstersLeft);
		}
		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			WavesMatchData si = new WavesMatchData();
			si.IsActive = stream.ReadBoolean();
			si.TimeSec = stream.ReadSingle();
			si.IsRoundStarted = stream.ReadBoolean();
			si.RoundId = stream.ReadByte();
			si.Players = stream.Read<List<WavesPlayerInfo>>();
			si.MonstersLeft = stream.ReadUInt16();
			return si;
		}
	}
}
