﻿using System;
using System.Collections.Generic;
using System.Text;
using Assets.Scripts.InstanceServer.Addin;

namespace Assets.Scripts.Networking.ClientDataRPC.Arenas
{
	public abstract class MatchDataBase
	{
		public bool IsActive { get; set; }
		public float TimeSec { get; set; }
		public bool IsRoundStarted { get; set; }
		public int RoundId { get; set; }
		public GameResult GameResult { get; set; }
		public int BattleId { get; set; }
	}
}
