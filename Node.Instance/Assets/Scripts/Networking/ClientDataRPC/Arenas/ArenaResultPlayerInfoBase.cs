﻿using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC.Arenas
{

	public abstract class ArenaResultPlayerInfoBase : ArenaPlayerInfoBase
	{
		public int GearScore { get; set; }
		public int Level { get; set; }
		public bool IsMVP { get; set; }
		public int RatingScore { get; set; }

		public int gameCount;

		public abstract int FullScore();
	}
}
