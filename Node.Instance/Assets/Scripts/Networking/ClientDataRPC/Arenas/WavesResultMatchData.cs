﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assets.Scripts.Networking.ClientDataRPC.Arenas
{
	public class WavesResultMatchData : WavesMatchData
	{
		public new List<WavesResultPlayerInfo> Players { get; set; }
		public bool LastWaveCompleted { get; set; }
		public int MaxWaves { get; set; }
		public bool NeedKillPreviousWave { get; set; }
		public bool IsAlive { get; set; }
		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			WavesResultMatchData si = (WavesResultMatchData)value;
			stream.Write<bool>(si.IsActive);
			stream.Write<byte>((byte)si.RoundId);
			stream.Write<List<WavesResultPlayerInfo>>(si.Players);
			stream.Write<ushort>((ushort)si.MonstersLeft);
			stream.Write<bool>(si.LastWaveCompleted);
			stream.Write<byte>((byte)si.MaxWaves);
			stream.Write<bool>(si.IsAlive);
		}
		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			WavesResultMatchData si = new WavesResultMatchData();
			si.IsActive = stream.ReadBoolean();
			si.RoundId = stream.ReadByte();
			si.Players = stream.Read<List<WavesResultPlayerInfo>>();
			si.MonstersLeft = stream.ReadUInt16();
			si.LastWaveCompleted = stream.ReadBoolean();
			si.MaxWaves = stream.ReadByte();
			si.IsAlive = stream.ReadBoolean();
			return si;
		}
	}
}
