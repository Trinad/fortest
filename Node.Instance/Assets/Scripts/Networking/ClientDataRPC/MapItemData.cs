﻿using System;
using Assets.Scripts.InstanceServer.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class MapItemData
	{
		public int Id { get; }
		public ActualPlaceType TypeId { get; }
		public ActualPlaceTitle TitleId { get; }
		public ActualPlaceIcon IconId { get; }
		public bool Disable { get; }

		public MapItemData(int id, ActualPlaceType placeType, ActualPlaceTitle placeTitle, ActualPlaceIcon placeIcon, bool disable)
		{
			Id = id;
			TypeId = placeType;
			TitleId = placeTitle;
			IconId = placeIcon;
			Disable = disable;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			MapItemData si = (MapItemData)value;
			stream.Write<int>(si.Id);
			stream.Write<short>((short)si.TypeId);
			stream.Write<sbyte>((sbyte)si.TitleId);
			stream.Write<sbyte>((sbyte)si.IconId);
			stream.Write<bool>(si.Disable);
		}
	}
}
