﻿namespace Assets.Scripts.Networking.ClientDataRPC
{
	public sealed class LocParamData
	{
		public bool IsTemplate;
		public string Text;

		public LocParamData()
		{
		}

		public LocParamData(string text)
		{
			IsTemplate = false;
			Text = text;
		}

		public LocParamData(int group, int template)
		{
			IsTemplate = true;
			Text = ((group << 24) + template).ToString();
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (LocParamData)value;
			stream.Write(si.IsTemplate);
			stream.Write(si.Text);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			var si = new LocParamData();
			si.IsTemplate = stream.Read<bool>();
			si.Text = stream.Read<string>();
			return si;
		}
	}
}
