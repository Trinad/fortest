namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class ChestEntityDescription
    {
        public string icon;
        public string nameKey;
        public int minCount;
        public int maxCount;
        public uint iconColor;
        public uint counterColor;

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            ChestEntityDescription si = (ChestEntityDescription)value;
            stream.Write(si.icon);
            stream.Write(si.nameKey);
            stream.Write(si.minCount);
            stream.Write(si.maxCount);
            stream.Write(si.iconColor);
            stream.Write(si.counterColor);
        }
    }
}
