﻿using System;
using Assets.Scripts.InstanceServer;
using UtilsLib;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public enum LoginType
	{
		Guest,
		Facebook
	}

	public class FriendItemData
	{
		public int LoginId { get; private set; }
		public string LoginAvatar { get; private set; }
		public string LoginName { get; private set; }
		public int PersonId { get; private set; }
		public string PersonName { get; private set; }
		public int GearScore { get; private set; }
		public string ExternId { get; private set; }
		public LoginType LoginType { get; private set; }
		public bool IsOffer { get; set; }
		public bool IsFriend { get; set; }

		public PersonType PersonType;
		public FractionType Fraction;
		
		
		public DateTime LastGiftTime;
		public int GiftTimeLeftSec
		{
			get 
            {
                DateTime nextGift = LastGiftTime.AddSeconds(CooldownFormulas.Instance.GiftSec);
                DateTime now = TimeProvider.UTCNow;
                return nextGift > now ? (int)(nextGift - now).TotalSeconds : 0; 
            }
		}

		public DateTime LastAskTime;
		public int AskTimeLeftSec
		{
			get { return LastAskTime.AddSeconds(CooldownFormulas.Instance.AskSec) > TimeProvider.UTCNow ? (int)(LastAskTime.AddSeconds(CooldownFormulas.Instance.AskSec) - TimeProvider.UTCNow).TotalSeconds : 0; }
		}

		public FriendItemData()
		{
		}

		public FriendItemData(InstancePerson person, bool bOffer, bool bFriend)
		{
			PersonId = person.personId;
			LoginId = person.loginId;
			LoginAvatar = person.login.AvatarName;
			PersonName = person.personName;
			GearScore = person.HeroPower;
			LoginName = person.login.loginName;
			ExternId = String.Empty;
			LoginType = LoginType.Guest;
			PersonType = person.personType;
			Fraction = person.personFraction;
			LastGiftTime = DateTime.MinValue;
			IsOffer = bOffer;
			IsFriend = bFriend;
		}

		public FriendItemData(int loginId, bool bOffer, bool bFriend)
		{
			PersonId = -1;
			LoginId = loginId;
			//LoginName = person.loginName;
			//LoginAvatar = person.loginAvatar;
			//PersonName = person.personName;
			//GearScore = person.GearScore;
			ExternId = String.Empty;
			LoginType = LoginType.Guest;
			//PersonType = person.personType;
			//Fraction = person.personFraction;
			//LastGiftTime = person.LastGiftTime;
			//LastAskTime = person.LastAskTime;
			IsOffer = bOffer;
			IsFriend = bFriend;
		}

		public FriendItemData(SocialFriend person, bool bOffer, bool bFriend)
		{
			LoginId = 0;
			LoginName = person.Name;
			LoginAvatar = person.Avatar;

			PersonId = 0;
			PersonName = "";

			GearScore = FRRandom.Next(1, 100);

			ExternId = person.Id;
			LoginType = LoginType.Facebook;

			IsOffer = bOffer;
			IsFriend = bFriend;
		}

		//public LoginItemData ToLoginItemData()
		//{
		//	return new LoginItemData
		//	{
		//		LoginId = LoginId,
		//		Picture = LoginAvatar,
		//		LoginName = LoginName
		//	};
		//}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			FriendItemData si = (FriendItemData)value;
			stream.Write<int>(si.PersonId);
			stream.Write<int>(si.LoginId);
			stream.Write<string>(si.PersonName);
			stream.Write<int>(si.GearScore);
			stream.Write<string>(si.LoginName);
			stream.Write<string>(si.ExternId);
			stream.Write<int>((int)si.LoginType);
			stream.Write<int>((int)si.PersonType);
			stream.Write<int>((int)si.Fraction);
			stream.Write<int>(si.GiftTimeLeftSec);
			stream.Write<bool>(si.IsOffer);
			stream.Write<bool>(si.IsFriend);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			FriendItemData si = new FriendItemData();
			si.PersonId = stream.Read<int>();
			si.LoginId = stream.Read<int>();
			si.PersonName = stream.Read<string>();
			si.GearScore = stream.Read<int>();
			si.LoginName = stream.Read<string>();
			si.ExternId = stream.Read<string>();
			si.LoginType = (LoginType)stream.Read<int>();
			si.PersonType = (PersonType)stream.Read<int>();
			si.Fraction = (FractionType)stream.Read<int>();
			var TimeLeftSec = stream.Read<int>(); //TODO_Deg
			si.IsOffer = stream.Read<bool>();
			si.IsFriend = stream.Read<bool>();
			return si;
		}
	}
}
