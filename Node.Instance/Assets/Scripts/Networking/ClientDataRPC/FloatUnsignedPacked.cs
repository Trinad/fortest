﻿using LobbyInstanceLib.Networking;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public struct FloatUnsignedPacked
	{
		public float Float { get; }

		public FloatUnsignedPacked(float f)
		{
			Float = f;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (FloatUnsignedPacked)value;
			stream.WriteFloatUnsignedPacked(si.Float);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			float f = stream.ReadFloatUnsignedPacked();
			var si = new FloatUnsignedPacked(f);
			return si;
		}
	}
}
