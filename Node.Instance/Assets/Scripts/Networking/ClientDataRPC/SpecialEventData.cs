﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class SpecialEventData
	{
        public int ProgressScoreValue;
        public List<DailyQuestRewardData> RewardDataList;
        public List<InventoryItemData> ItemDataList;
        public int Duration;
        public SpecialEventScenarioData specialEvent;
		public int CompensationCurrencyCount;

        public SpecialEventData()
        {
            RewardDataList = new List<DailyQuestRewardData>();
            ItemDataList = new List<InventoryItemData>();
            Duration = 0;
            specialEvent = new SpecialEventScenarioData();
        }

        public SpecialEventData(InstancePerson p, SpecialEventScenarioData specialEvent, List<InventoryItemData> itemDataList, int duration, QuestItemData quest)
		{
            this.specialEvent = specialEvent;

			ProgressScoreValue = quest.TasksDone;
			RewardDataList = quest.Rewards.Select(reward => new DailyQuestRewardData(p, quest, reward)).ToList();
			ItemDataList = itemDataList;
			Duration = duration;
        }

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			SpecialEventData si = (SpecialEventData)value;
			stream.WriteStringFromId(si.specialEvent.Text);

            stream.WriteStringFromId(si.specialEvent.FormBackgroundImage);
            stream.WriteStringFromId(si.specialEvent.SpecialEventBackgroundImage);
            stream.WriteStringFromId(si.specialEvent.SpecialEventOverlayImage);
            stream.WriteStringFromId(si.specialEvent.ProgressBarImage);
            stream.WriteStringFromId(si.specialEvent.ProgressBarGradientImage);
            stream.WriteStringFromId(si.specialEvent.ProgressBarHeadImage);
            stream.WriteStringFromId(si.specialEvent.CollectedItemImage);
            stream.WriteStringFromId(si.specialEvent.CollectedItemsBgImage);

            stream.WriteStringFromId(si.specialEvent.LobbyIconImage);
            stream.WriteStringFromId(si.specialEvent.ShowItemsButtonImage);
            stream.Write(si.specialEvent.GetDecorImages());

            stream.WriteStringFromId(si.specialEvent.ShowItemsButtonText);
            stream.WriteStringFromId(si.specialEvent.FirstStepHint);
            stream.WriteStringFromId(si.specialEvent.SecondStepHint);
            stream.WriteStringFromId(si.specialEvent.ThirdStepHint);

            stream.WriteStringFromId(si.specialEvent.TopColor);
			stream.WriteStringFromId(si.specialEvent.BottomColor);

            stream.WriteStringFromId(si.specialEvent.BackButtonColor);
            stream.WriteStringFromId(si.specialEvent.ProgressNextColor);
            stream.WriteStringFromId(si.specialEvent.ProgressCurrentColor);
            stream.WriteStringFromId(si.specialEvent.ProgressPrevColor);

            stream.Write(si.ProgressScoreValue);
			stream.Write(si.RewardDataList);
			stream.Write(si.ItemDataList);
			stream.Write(si.Duration);

            stream.WriteStringFromId(si.specialEvent.ResourceIcon);
            stream.Write((int)si.specialEvent.ResourceType);
            stream.WriteStringFromId(si.specialEvent.CompensationHint);
            stream.Write(si.CompensationCurrencyCount);
            stream.WriteStringFromId(si.specialEvent.NotEnoughTokenHint);
        }
	}
}
