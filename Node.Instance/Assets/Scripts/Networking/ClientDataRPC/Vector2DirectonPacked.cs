﻿using System.Numerics;
using LobbyInstanceLib.Networking;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public struct Vector2DirectonPacked
    {
		public Vector2 Direction { get; }

		public Vector2DirectonPacked(Vector2 dir)
		{
			Direction = dir;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (Vector2DirectonPacked)value;
			stream.WriteVec2Byte(si.Direction);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			Vector2 directon = stream.ReadVector2Byte();
			var si = new Vector2DirectonPacked(directon);
			return si;
		}
	}
}
