﻿using Assets.Scripts.InstanceServer;
using UtilsLib.Logic.Enums;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData;
using UtilsLib.BatiyScript;
using System.Xml.Serialization;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class PriceCollection
    {
        [XmlElement("PriceFormula", typeof(PriceFormula))]
        public List<PriceFormula> PriceFormulas;

        public List<ResourcesToUpdateItemData> GetResourcesToUpdate(int level, InstancePerson person)
        {
            List<ResourcesToUpdateItemData> res = new List<ResourcesToUpdateItemData>();
            foreach (var p in PriceFormulas)
            {
                int count = p.fCount.IntCalc(level);
                if (count != 0)
                    res.Add(new ResourcesToUpdateItemData(person, p.Resource, count));
            }
            return res;
        }

        public List<ResourcesToUpdateItemData> GetResourcesToUpdateWithZeros(int level, InstancePerson person)
        {
            List<ResourcesToUpdateItemData> res = new List<ResourcesToUpdateItemData>();
            foreach (var p in PriceFormulas)
            {
                int count = p.fCount.IntCalc(level);
                res.Add(new ResourcesToUpdateItemData(person, p.Resource, count));
            }
            return res;
        }
    }

    public class PriceFormula
    {
        [XmlAttribute]
        public ResourceType Resource;

        [XmlAttribute]
        public string Count
        {
            get { return fCount.Text; }
            set { fCount = new FormulaX("level", value); }
        }

        [XmlIgnore]
        public FormulaX fCount = new FormulaX("level", "1");
    }

    public class ResourcesToUpdateItemData
	{
		public ResourceType Resource;
		public int CountRequired; //Сколько ресурсов нужно
		public int CountStock; //Сколько ресурсов имеется

		public ResourcesToUpdateItemData()
		{
		}

		public ResourcesToUpdateItemData(InstancePerson person, ResourceType resource, int count)
		{
			Resource = resource;
			CountRequired = count;
			CountStock = person.CountResource(resource);
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (ResourcesToUpdateItemData)value;
			stream.Write((int)si.Resource);
			stream.Write(si.CountRequired);
			stream.Write(si.CountStock);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			var si = new ResourcesToUpdateItemData();
			si.Resource = (ResourceType)stream.Read<int>();
			si.CountRequired = stream.Read<int>();
			si.CountStock = stream.Read<int>();
			return si;
		}
	}
}
