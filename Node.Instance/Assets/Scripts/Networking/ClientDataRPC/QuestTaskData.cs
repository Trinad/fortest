﻿using Assets.Scripts.InstanceServer;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using UtilsLib.DegSerializers;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class QuestTaskData: ICanCopy
	{
        [XmlAttribute]
        public int Count = 1;

        private int done = 0;
		public int Done
        {
            get{ return done; }
            set{ done = Math.Min(value, Count); }
        }

		[XmlAttribute]
		public string TechnicalName;

		public List<LocalizationAttributeQuest> LocalizationAttributes;

		[XmlAttribute]
        public bool AddToJobProgress = true;

		[XmlAttribute]
		public bool CanRollback; //Таск из выполненого может стать невыполненым

		[XmlAttribute]
		public bool IsOptional; //Таск необязателен для выполнения квеста

        public bool Complete
        {
            get { return Done >= Count; }
        }

		[XmlIgnore]
		public LobbyLogin owner;

		[XmlIgnore]
		public InstancePerson person => owner.Person;

		[XmlIgnore]
		public QuestItemData quest;

		//Список всех объектов для квеста
		public virtual List<MapTargetObject> GetTargets()
		{
			return null;
		}

		//Проверить подходит ли цель для квеста
		public virtual bool CheckTarget(MapTargetObject target)
		{
            return false;
		}

        public virtual void Init()
        {

        }

        public virtual void Dispose()
        {

        }

		public QuestTaskData()
		{

		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			QuestTaskData si = (QuestTaskData)value;
            stream.Write<int>(0); //si.DescId); //TODO_deg если квесты вернут надо TechnicalName сделать
			stream.Write<int>(si.Count);
			stream.Write<int>(si.Done);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			QuestTaskData si = new QuestTaskData();
            var DescId = stream.Read<int>();
			si.Count = stream.Read<int>();
			si.Done = stream.Read<int>();
			return si;
		}
	}
}
