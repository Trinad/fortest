﻿using System;
using System.Numerics;
using LobbyInstanceLib.Networking;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public sealed class ChangeTransformForceData : IChangeTransformData
	{
		public int TargetId;
		public Vector3 Position;
		public Vector2 Direction;

		public ChangeTransformForceData(int targetId, Vector3 position, Vector2 direction)
		{
			TargetId = targetId;
			Position = position;
			Direction = direction;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (ChangeTransformForceData)value;
			stream.WriteVariableInt32(si.TargetId);
			stream.WriteVec3Short(si.Position);
			stream.WriteVec2Byte(si.Direction);
		}
	}
}
