﻿using UtilsLib;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class RatingItemData
	{
		public int PersonId;
		public int LoginId;
		public string Name;
		public string Login;
		public int Position;

		public PersonType PersonType;
		public int GS;
		public int PvP;

		public RatingItemData()
		{
		}

		public RatingItemData(int position, int personId, int loginId)
		{
			PersonId = personId;
			LoginId = loginId;
			Name = "Персонаж" + personId;
			Name = "Логин" + loginId;
			Position = position;

			PersonType = (PersonType)FRRandom.Next(0, 3);
			GS = FRRandom.Next(0, 1000);
			PvP = FRRandom.Next(0, 1000);
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			RatingItemData si = (RatingItemData)value;
			stream.Write<int>(si.PersonId);
			stream.Write<int>(si.LoginId);
			stream.Write<string>(si.Name);
			stream.Write<string>(si.Login);
			stream.Write<int>(si.Position);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			RatingItemData si = new RatingItemData();
			si.PersonId = stream.Read<int>();
			si.LoginId = stream.Read<int>();
			si.Name = stream.Read<string>();
			si.Login = stream.Read<string>();
			si.Position = stream.Read<int>();
			return si;
		}

		public override string ToString()
		{
			return PersonId.ToString();
		}
	}
}
