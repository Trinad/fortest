﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.InstanceServer.Entries.XmlData.Trade;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking
{
	public class LobbyBuildingAccountData
	{
		public int ID;
		public InventorySlotMode status;
		public bool IsNew;
		public bool IsNotViewed;

		public LobbyBuildingAccountData()
		{
		}

		public LobbyBuildingAccountData(LobbyLogin login, Building building)
		{
			ID = building.Id;

			var loginBuilding = login.Buildings.Find(x => x.Name == building.Name);
			if (loginBuilding == null)
			{
				status = InventorySlotMode.SHOP;
				IsNew = true;
				IsNotViewed = true;
			}
			else
			{
				switch (loginBuilding.State)
				{
					case BuildingState.None:
						status = InventorySlotMode.SHOP;
						break;
					case BuildingState.Enable:
						status = InventorySlotMode.BACKPACK;
						break;
					case BuildingState.Selected:
						status = InventorySlotMode.EQUIPMENT;
						break;
				}
				IsNew = loginBuilding.IsNew;
				IsNotViewed = loginBuilding.IsNotViewed;
			}
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (LobbyBuildingAccountData)value;
			stream.Write<int>(si.ID);
			stream.Write<int>((int)si.status);
			stream.Write<bool>(si.IsNew);
			stream.Write<bool>(si.IsNotViewed);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			var si = new LobbyBuildingAccountData();
			si.ID = stream.ReadInt32();
			si.status = (InventorySlotMode)stream.ReadInt32();
			si.IsNew = stream.ReadBoolean();
			si.IsNotViewed = stream.ReadBoolean();
			return si;
		}
	}

	public class LobbyBuildingStorageData
	{
		public List<LobbyBuildingData> LobbyBuildings;
		public List<LobbyBuildingCategoryData> LobbyBuildingCategory;
		public List<LobbyBuildingSubCategoryData> LobbyBuildingSubCategory;
		public List<LobbyBuildingSetData> LobbyBuildingSet;

		public LobbyBuildingStorageData()
		{
		}

		public LobbyBuildingStorageData(InstancePerson p, ShopManager shopManager, PlayerCityData playerCityData)
		{
			LobbyBuildings = new List<LobbyBuildingData>();
		    foreach (var building in playerCityData.Buildings)
		    {
		        var data = new LobbyBuildingData(p, building);
		        if (!string.IsNullOrEmpty(data.TradeInfoData.Sku) && shopManager.ShopOff)
		            continue;

                LobbyBuildings.Add(data);
            }

			LobbyBuildingCategory = new List<LobbyBuildingCategoryData>();
			LobbyBuildingSubCategory = new List<LobbyBuildingSubCategoryData>();
			foreach (var category in playerCityData.Categories)
			{
				LobbyBuildingCategory.Add(new LobbyBuildingCategoryData(p, category));
				foreach (var subcategory in category.Subcategories)
					LobbyBuildingSubCategory.Add(new LobbyBuildingSubCategoryData(p, category, subcategory));
			}

			LobbyBuildingSet = new List<LobbyBuildingSetData>();
		    foreach (var buildingKit in playerCityData.BuildingKits)
		    {
		        var data = new LobbyBuildingSetData(p, buildingKit);
		        if (!string.IsNullOrEmpty(data.TradeInfoData.Sku) && shopManager.ShopOff)
		            continue;

                LobbyBuildingSet.Add(data);
            }
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (LobbyBuildingStorageData)value;
			stream.Write(si.LobbyBuildings);
			stream.Write(si.LobbyBuildingCategory);
			stream.Write(si.LobbyBuildingSubCategory);
			stream.Write(si.LobbyBuildingSet);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			var si = new LobbyBuildingStorageData();
			si.LobbyBuildings = stream.Read<List<LobbyBuildingData>>();
			si.LobbyBuildingCategory = stream.Read<List<LobbyBuildingCategoryData>>();
			si.LobbyBuildingSubCategory = stream.Read<List<LobbyBuildingSubCategoryData>>();
			si.LobbyBuildingSet = stream.Read<List<LobbyBuildingSetData>>();
			return si;
		}
	}

	public class LobbyBuildingData
	{
		public string Name;
		public string TechnicalName;
		public string DescriptionName;
		public string ModelName;
		public string IconName;
		public LobbyBuildingCategory BuildingCategory;
		public LobbyBuildingSubCategory BuildingSubCategory;
		public bool IsVisible;
		public int ID;
		public int SortId;
		public int UnlockLevel; // Возможно будет не только уровень??????????????????
		public TradeInfoData TradeInfoData;
		public bool IsDiscont;
		public int DiscontCount;
		public int DiscontDuration;
		public bool IsTopSales;
		public List<BuildingStatData> BuildingStats;
		public List<LocParameter> LocParameters;

		public LobbyBuildingData()
		{
		}

		public LobbyBuildingData(InstancePerson p, Building building)
		{
			var loginBuilding = p.login.Buildings.Find(x => x.Name == building.Name);
			bool isBuy = loginBuilding != null && loginBuilding.State != BuildingState.None;

		    Name = building.Name;
			TechnicalName = building.Title;
			DescriptionName = building.Desc;
			ModelName = building.Prefab;
			IconName = building.Icon;
			BuildingCategory = building.Category;
			BuildingSubCategory = building.Subcategory;
			IsVisible = building.Visible || isBuy;
			ID = building.Id;
			SortId = building.SortId;
			UnlockLevel = building.UnlockRules.GetByType(UnlockTaskType.MaxLevel);
			TradeInfoData = building.TradeInfos.GetCurrentTradeOffer(TimeProvider.UTCNow, p) ?? new TradeInfoData();
			DiscontCount = TradeInfoData.PercentLabel;
			DiscontDuration = 0;
			IsDiscont = DiscontCount > 0;
			IsTopSales = building.IsTopSales;
            BuildingStats = building.BuildingStats.Select(x => new BuildingStatData(p, x)).ToList();
			LocParameters = building.GetLocParameter();
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (LobbyBuildingData)value;

			stream.WriteStringFromId(si.Name);
			stream.WriteStringFromId(si.TechnicalName);
			stream.WriteStringFromId(si.DescriptionName);
			stream.WriteStringFromId(si.ModelName);
			stream.WriteStringFromId(si.IconName);

			stream.Write<sbyte>((sbyte)si.BuildingCategory);
			stream.Write<sbyte>((sbyte)si.BuildingSubCategory);
			stream.Write<bool>(si.IsVisible);
			stream.Write<int>(si.ID);
			stream.Write<int>(si.SortId);
			stream.Write<int>(si.UnlockLevel);
			stream.Write<TradeInfoData>(si.TradeInfoData);
			stream.Write<bool>(si.IsDiscont);
			stream.Write<int>(si.DiscontCount);
			stream.Write<int>(si.DiscontDuration);
			stream.Write<bool>(si.IsTopSales);
			stream.Write<List<BuildingStatData>>(si.BuildingStats);
			stream.Write(si.LocParameters);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			var si = new LobbyBuildingData();
			si.Name = stream.ReadString();
			si.TechnicalName = stream.ReadString();
			si.DescriptionName = stream.ReadString();
			si.ModelName = stream.ReadString();
			si.IconName = stream.ReadString();
			si.BuildingCategory = (LobbyBuildingCategory)stream.ReadSByte();
			si.BuildingSubCategory = (LobbyBuildingSubCategory)stream.ReadSByte();
			si.IsVisible = stream.ReadBoolean();
			si.ID = stream.ReadInt32();
			si.SortId = stream.ReadInt32();
			si.UnlockLevel = stream.ReadInt32();
			si.TradeInfoData = stream.Read<TradeInfoData>();
			si.IsDiscont = stream.ReadBoolean();
			si.DiscontCount = stream.ReadInt32();
			si.DiscontDuration = stream.ReadInt32();
			si.IsTopSales = stream.ReadBoolean();
			si.BuildingStats = stream.Read<List<BuildingStatData>>();
			si.LocParameters = stream.Read<List<LocParameter>>();
			return si;
		}
	}

	public class LobbyBuildingCategoryData
	{
		public string TechnicalName;
		public LobbyBuildingCategory BuildingCategory;
		public string IconName;
		public int ID;
		public int SortId;
		public int UnlockLevel; // Возможно будет не только уровень??????????????????
		public bool IsVisible;
		public bool LockPermanent;
		public bool IsNew;
		public bool IsNotViewed;

		public LobbyBuildingCategoryData()
		{
		}

		public LobbyBuildingCategoryData(InstancePerson p, Category category)
		{
			TechnicalName = category.Title;
			BuildingCategory = category.Type;
			IconName = category.Icon;
			ID = category.Id;
			SortId = category.SortId;
			UnlockLevel = category.UnlockRules.GetByType(UnlockTaskType.MaxLevel);
			IsVisible = category.Visible;
			LockPermanent = category.LockPermanent;
			IsNew = p.login.BuildingCategoryStatuses.Find(x => x.Name == category.Name)?.IsNew ?? false;
			IsNotViewed = p.login.BuildingCategoryStatuses.Find(x => x.Name == category.Name)?.IsNotViewed ?? false;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (LobbyBuildingCategoryData)value;
			stream.WriteStringFromId(si.TechnicalName);
			stream.Write<sbyte>((sbyte)si.BuildingCategory);
			stream.WriteStringFromId(si.IconName);
			stream.Write<int>(si.ID);
			stream.Write<int>(si.SortId);
			stream.Write<int>(si.UnlockLevel);
			stream.Write<bool>(si.IsVisible);
			stream.Write<bool>(si.LockPermanent);
			stream.Write<bool>(si.IsNew);
			stream.Write<bool>(si.IsNotViewed);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			var si = new LobbyBuildingCategoryData();
			si.TechnicalName = stream.ReadString();
			si.BuildingCategory = (LobbyBuildingCategory)stream.ReadSByte();
			si.IconName = stream.ReadString();
			si.ID = stream.ReadInt32();
			si.SortId = stream.ReadInt32();
			si.UnlockLevel = stream.ReadInt32();
			si.IsVisible = stream.ReadBoolean();
			si.LockPermanent = stream.ReadBoolean();
			si.IsNew = stream.ReadBoolean();
			si.IsNotViewed = stream.ReadBoolean();
			return si;
		}
	}

	public class LobbyBuildingSubCategoryData
	{
		public string TechnicalName;
		public LobbyBuildingCategory BuildingCategory;
		public LobbyBuildingSubCategory BuildingSubCategory;
		public string IconName;
		public int ID;
		public int SortId;
		public int UnlockLevel; // Возможно будет не только уровень??????????????????
		public bool IsVisible;
		public bool LockPermanent;
		public bool IsNew;
		public bool IsNotViewed;

		public LobbyBuildingSubCategoryData()
		{
		}

		public LobbyBuildingSubCategoryData(InstancePerson p, Category category, Subcategory subcategory)
		{
			TechnicalName = subcategory.Title;
			BuildingCategory = category.Type;
			BuildingSubCategory = subcategory.Type;
			IconName = subcategory.Icon;
			ID = subcategory.Id;
			SortId = subcategory.SortId;
			UnlockLevel = subcategory.UnlockRules.GetByType(UnlockTaskType.MaxLevel);
			IsVisible = subcategory.Visible;
			LockPermanent = subcategory.LockPermanent;
			IsNew = p.login.BuildingSubCategoryStatuses.Find(x => x.Name == subcategory.Name)?.IsNew ?? false;
			IsNotViewed = p.login.BuildingSubCategoryStatuses.Find(x => x.Name == subcategory.Name)?.IsNotViewed ?? false;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (LobbyBuildingSubCategoryData)value;
			stream.Write<string>(si.TechnicalName);
			stream.Write<sbyte>((sbyte)si.BuildingCategory);
			stream.Write<sbyte>((sbyte)si.BuildingSubCategory);
			stream.Write<string>(si.IconName);
			stream.Write<int>(si.ID);
			stream.Write<int>(si.SortId);
			stream.Write<int>(si.UnlockLevel);
			stream.Write<bool>(si.IsVisible);
			stream.Write<bool>(si.LockPermanent);
			stream.Write<bool>(si.IsNew);
			stream.Write<bool>(si.IsNotViewed);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			var si = new LobbyBuildingSubCategoryData();
			si.TechnicalName = stream.ReadString();
			si.BuildingCategory = (LobbyBuildingCategory)stream.ReadSByte();
			si.BuildingSubCategory = (LobbyBuildingSubCategory)stream.ReadSByte();
			si.IconName = stream.ReadString();
			si.ID = stream.ReadInt32();
			si.SortId = stream.ReadInt32();
			si.UnlockLevel = stream.ReadInt32();
			si.IsVisible = stream.ReadBoolean();
			si.LockPermanent = stream.ReadBoolean();
			si.IsNew = stream.ReadBoolean();
			si.IsNotViewed = stream.ReadBoolean();
			return si;
		}
	}

	public class LobbyBuildingSetData
	{
		public string Name;
		public string TechnicalName;
		public string IconName;
		public LobbyBuildingCategory BuildingCategory;
		public LobbyBuildingSubCategory BuildingSubCategory;
		public bool IsVisible;
		public int ID;
		public int SortId;
		public int UnlockLevel; // Возможно будет не только уровень??????????????????
		public TradeInfoData TradeInfoData;
		public bool IsDiscont;
		public int DiscontCount;
		public int DiscontDuration;
		public bool IsTopSales;
		public bool IsNew;
		public bool IsNotViewed;
		public List<LobbyBuildingData> buildings;

		public LobbyBuildingSetData()
		{
		}

		public LobbyBuildingSetData(InstancePerson p, BuildingKit buildingSet)
		{
		    Name = buildingSet.Name;
			TechnicalName = buildingSet.Title;
			IconName = buildingSet.Icon;
			BuildingCategory = LobbyBuildingCategory.None;
			BuildingSubCategory = LobbyBuildingSubCategory.None;
			IsVisible = buildingSet.Visible;
			ID = buildingSet.Id;
			SortId = buildingSet.SortId;
			UnlockLevel = buildingSet.UnlockRules.GetByType(UnlockTaskType.MaxLevel);
			TradeInfoData = buildingSet.TradeInfos.GetCurrentTradeOffer(TimeProvider.UTCNow, p) ?? new TradeInfoData();
			DiscontCount = TradeInfoData.PercentLabel;
			DiscontDuration = 0;
			IsDiscont = DiscontCount > 0;
			IsTopSales = buildingSet.IsTopSales;
			IsNew = p.login.BuildingSetStatuses.Find(x => x.Name == buildingSet.Name)?.IsNew ?? false;
			IsNotViewed = p.login.BuildingSetStatuses.Find(x => x.Name == buildingSet.Name)?.IsNotViewed ?? false;
			buildings = buildingSet.Buildings.Select(x => new LobbyBuildingData(p, x)).ToList();
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (LobbyBuildingSetData)value;

			stream.WriteStringFromId(si.Name);
			stream.WriteStringFromId(si.TechnicalName);
			stream.WriteStringFromId(si.IconName);

			stream.Write<sbyte>((sbyte)si.BuildingCategory);
			stream.Write<sbyte>((sbyte)si.BuildingSubCategory);
			stream.Write<bool>(si.IsVisible);
			stream.Write<int>(si.ID);
			stream.Write<int>(si.SortId);
			stream.Write<int>(si.UnlockLevel);
			stream.Write<TradeInfoData>(si.TradeInfoData);
			stream.Write<bool>(si.IsDiscont);
			stream.Write<int>(si.DiscontCount);
			stream.Write<int>(si.DiscontDuration);
			stream.Write<bool>(si.IsTopSales);
			stream.Write<bool>(si.IsNew);
			stream.Write<bool>(si.IsNotViewed);
			stream.Write<List<LobbyBuildingData>>(si.buildings);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			var si = new LobbyBuildingSetData();
			si.TechnicalName = stream.ReadString();
			si.IconName = stream.ReadString();
			si.BuildingCategory = (LobbyBuildingCategory)stream.ReadSByte();
			si.BuildingSubCategory = (LobbyBuildingSubCategory)stream.ReadSByte();
			si.IsVisible = stream.ReadBoolean();
			si.ID = stream.ReadInt32();
			si.SortId = stream.ReadInt32();
			si.UnlockLevel = stream.ReadInt32();
			si.TradeInfoData = stream.Read<TradeInfoData>();
			si.IsDiscont = stream.ReadBoolean();
			si.DiscontCount = stream.ReadInt32();
			si.DiscontDuration = stream.ReadInt32();
			si.IsTopSales = stream.ReadBoolean();
			si.IsNew = stream.ReadBoolean();
			si.IsNotViewed = stream.ReadBoolean();
			si.buildings = stream.Read<List<LobbyBuildingData>>();
			return si;
		}
	}
}
