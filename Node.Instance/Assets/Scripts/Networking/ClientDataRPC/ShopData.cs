﻿using System.Collections.Generic;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries.Influences;
using UtilsLib.Logic;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class ShopData
	{
		public List<ProductItemData> Products;

		public ShopData(InstancePerson person, List<ProductItemData> Products)
		{
			this.Products = Products;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			ShopData si = (ShopData)value;
			stream.Write<List<ProductItemData>>(si.Products);
			stream.Write<bool>(false);
			stream.Write<int>(0);// si.VipLevel);
			stream.Write<int>(0);// si.VipLevelMax);
			stream.Write<int>(0);// si.VipCurrentScore);
			stream.Write<int>(0);// si.VipMaxScore);
		}
	}
}
