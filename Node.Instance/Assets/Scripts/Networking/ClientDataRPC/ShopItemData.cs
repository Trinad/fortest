﻿using System.Collections.Generic;
using System.Xml.Serialization;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class ShopItemData
	{
		[XmlAttribute]
		public int FilterId;

		[XmlElement("ProductItemData")]
		public List<ProductItemData> ProductItemDatas = new List<ProductItemData>();

		public ShopItemData()
		{
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			ShopItemData si = (ShopItemData)value;
			stream.Write(si.FilterId);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			ShopItemData si = new ShopItemData();
			si.FilterId = stream.Read<int>();
			return si;
		}

		public override string ToString()
		{
            return FilterId.ToString();
		}
	}
}
