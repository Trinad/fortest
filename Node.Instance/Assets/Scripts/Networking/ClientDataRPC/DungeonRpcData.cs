﻿using System;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Person;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class DungeonRpcData
    {
        public int EdgeId { get; private set; }
        public int IconId { get; private set; }
        public List<FloorData> DungeonLevelDatas { get; set; }

        public string TechnicalName = string.Empty;

        public DungeonRpcData()
        {
            DungeonLevelDatas = new List<FloorData>();
        }

        public DungeonRpcData(DungeonXmlData dungeonXmlData, List<DungeonPersonFloorData> dungeonPersonDatas) : this()
        {
            TechnicalName = dungeonXmlData.TechnicalName;
            EdgeId = dungeonXmlData.EdgeId;
            IconId = dungeonXmlData.ImageId;

            foreach (var xmlData in dungeonXmlData.DungeonLocations)
            {
                var personData = dungeonPersonDatas.Find(data => data.LocationId == xmlData.LocationId);
                if (personData == null)
                {
                    ILogger.Instance.Send("DungeonRpcData: ", 
                        string.Format("personFloorData == null, xmlData.LocationId: {0}, dungeonPersonDatas.Count: {1}", xmlData.LocationId, dungeonPersonDatas.Count), ErrorLevel.error);
                    continue;
                }
                
                var floorData = new FloorData(xmlData, personData);
                DungeonLevelDatas.Add(floorData);
            }
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            DungeonRpcData si = (DungeonRpcData)value;

            stream.Write<int>(si.EdgeId);
            stream.Write<int>(si.IconId);
            stream.Write<List<FloorData>>(si.DungeonLevelDatas);
            stream.Write<string>(si.TechnicalName);
        }

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            DungeonRpcData si = new DungeonRpcData();

            si.EdgeId = stream.Read<int>();
            si.IconId = stream.Read<int>();
            si.DungeonLevelDatas = stream.Read<List<FloorData>>();
            si.TechnicalName = stream.Read<string>();
            return si;
        }
    }

    public class FloorData
    {
        public int LocationId { get; private set; }
        public int Floor { get; private set; }
        public int ImageId { get; private set; }
        public int BossGearScore { get; private set; }
        public ResourceType CurrencyType { get; private set; }
        public int Price { get;private set; }
        public string TechnicalName;

        public DateTime Time { get; private set; }
        public ControlState ControlState { get;private set; }

        public FloorData()
        {
            
        }

        public FloorData(DungeonLocationXmlData dungeon, DungeonPersonFloorData personFloorData):this()
        {
            TechnicalName = dungeon.location.TechnicalName;
            LocationId = dungeon.LocationId;
            Floor = dungeon.Floor;
            ImageId = dungeon.ImageId;
            BossGearScore = dungeon.RecommendedGS;

            CurrencyType = personFloorData.CurrencyType;
            Price = personFloorData.Price;
            Time = personFloorData.Time;
            ControlState = personFloorData.ControlState;
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            FloorData si = (FloorData)value;
            stream.Write<int>(si.LocationId);
            stream.Write<int>(si.Floor);
            stream.Write<int>(si.ImageId);
            stream.Write<int>(si.BossGearScore);
            stream.Write<ResourceType>(si.CurrencyType);
            stream.Write<int>(si.Price);

            stream.Write<DateTime>(si.Time);
            stream.Write<ControlState>(si.ControlState);
            stream.Write<string>(si.TechnicalName);
        }

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            FloorData si = new FloorData();

            si.LocationId = stream.Read<int>();
            si.Floor = stream.Read<int>();
            si.ImageId = stream.Read<int>();
            si.BossGearScore = stream.Read<int>();
            si.CurrencyType = stream.Read<ResourceType>();
            si.Price = stream.Read<int>();

            si.Time = stream.Read<DateTime>();
            si.ControlState = stream.Read<ControlState>();
            si.TechnicalName = stream.Read<string>();

            return si;
        }
    }
}
