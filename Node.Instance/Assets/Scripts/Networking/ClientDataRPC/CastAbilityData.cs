﻿using System;
using Assets.Scripts.InstanceServer.Enums;
using System.Collections.Generic;
using System.Numerics;
using LobbyInstanceLib.Networking;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class CastFxData
    {
        public EffectType fxId;
        public int instanceId;
        public Vector3 dest;
        public float speed;
        public float scale = 1;
        public float time;

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            CastFxData si = (CastFxData)value;
            stream.Write(si.fxId);
            stream.Write<int>(si.instanceId);
            stream.WriteVec3Short(si.dest);
            stream.Write<float>(si.speed);
            stream.Write<float>(si.scale);
            stream.Write<float>(si.time);
        }

        public override string ToString()
        {
            return fxId.ToString();
        }
    }

    public class CastAbilityData
    {
        public int castorId;
        public AbilityType abilityType;
        public bool IsInterrupted;
        public List<CastFxData> fxList = new List<CastFxData>();
        public bool showInterruptMessage = true;
        public string payload;
        
        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            CastAbilityData si = (CastAbilityData)value;
            stream.Write<int>(si.castorId);
            stream.Write(si.abilityType);
            stream.Write<bool>(si.IsInterrupted);
            if (!si.IsInterrupted)
            {
                stream.Write<List<CastFxData>>(si.fxList);
            }
            else
            {
                stream.Write<bool>(si.showInterruptMessage);
            }
            stream.Write<string>(si.payload);
        }

        public override string ToString()
        {
            return castorId.ToString();
        }
    }
}
