﻿using Assets.Scripts.InstanceServer.Addin;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class ArenaRewardData
    {
        public int SlotId { get; set; }
        public ArenaRewardSlotType SlotType { get; set; }
        public int SlotCurrentParam { get; set; }
        public int SlotRequiredParam { get; set; }
        public InventoryItemData ItemData { get; set; }
        public bool Available = true;

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            ArenaRewardData si = (ArenaRewardData)value;
            stream.Write<int>(si.SlotId);
            stream.Write<int>((int)si.SlotType);
            stream.Write<int>(si.SlotCurrentParam);
            stream.Write<int>(si.SlotRequiredParam);
            stream.Write<InventoryItemData>(si.ItemData);
            stream.Write<bool>(si.Available);
        }

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            ArenaRewardData si = new ArenaRewardData();
            si.SlotId = stream.ReadInt32();
            si.SlotType = (ArenaRewardSlotType)stream.ReadInt32();
            si.SlotCurrentParam = stream.ReadInt32();
            si.SlotRequiredParam = stream.ReadInt32();
            si.ItemData = stream.Read<InventoryItemData>();
            si.Available = stream.ReadBoolean();
            return si;
        }
    }
}
