﻿using System;
using UtilsLib.DegSerializers;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    [Serializable]
    public class GameSettings
    {
        public bool isCheatsAllowed;
        public bool isNeedShowCheatPanel;
        public bool isNeedShowVersion;
        public bool isMusicEnabled;
        public bool isSoundEffectsEnabled;
        public bool isSoundEnvironmentEnabled;
        public float soundValue;
        public float musicValue;
        public float envValue;
        public int currentLanguage;
        public bool show_enemy_damage;
        public bool show_enemy_bottles;
        public bool show_player_bottles;
        public bool show_joystick;
        public bool enable_autoattack;
        public bool show_guild_name;
        public bool showPerkHUDDescription;
        public bool show_player_name;
        public bool show_fps;
        public bool fps_lock_30;
        public float SkillJoystickSensivity;
        public byte camera_setting;
        public bool receive_push_notifications;
        public bool testPurchase;
        public bool isDefualtSettings;
        public bool showAllyHeal;
        public bool showEnemyHeal;
        public bool show_chat_button;
        public bool show_hp_mana_panel;
		public bool allowMiniTutorials;

		public GameSettings()
        {
            soundValue = 1f;
            musicValue = 1f;
            envValue = 1f;
            show_player_bottles = true;
            show_joystick = true;
            show_guild_name = true;
            show_player_name = true;
            enable_autoattack = true;
            fps_lock_30 = true;
            SkillJoystickSensivity = 380f;
            camera_setting = 2;
            receive_push_notifications = true;

            isMusicEnabled = true;
            isSoundEffectsEnabled = true;
            isSoundEnvironmentEnabled = true;

            isDefualtSettings = true;
            showPerkHUDDescription = true;

            showAllyHeal = true;
            showEnemyHeal = true;
			allowMiniTutorials = true;

		}

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            GameSettings si = (GameSettings)value;
            stream.WriteBoolean(si.isCheatsAllowed);
            stream.WriteBoolean(si.isNeedShowCheatPanel);
            stream.WriteBoolean(si.isNeedShowVersion);
            stream.WriteBoolean(si.isMusicEnabled);
            stream.WriteBoolean(si.isSoundEffectsEnabled);
            stream.WriteBoolean(si.isSoundEnvironmentEnabled);
            stream.WriteSingle(si.soundValue);
            stream.WriteSingle(si.musicValue);
            stream.WriteSingle(si.envValue);
            stream.WriteInt32(si.currentLanguage);
            stream.WriteBoolean(si.show_enemy_damage);
            stream.WriteBoolean(si.show_enemy_bottles);
            stream.WriteBoolean(si.show_player_bottles);
            stream.WriteBoolean(si.show_joystick);
            stream.WriteBoolean(si.enable_autoattack);
            stream.WriteBoolean(si.show_guild_name);
            stream.WriteBoolean(si.showPerkHUDDescription);
            stream.WriteBoolean(si.show_player_name);
            stream.WriteBoolean(si.show_fps);
            stream.WriteBoolean(si.fps_lock_30);
            stream.WriteSingle(si.SkillJoystickSensivity);
            stream.WriteByte(si.camera_setting);
            stream.WriteBoolean(si.receive_push_notifications);
            stream.WriteBoolean(si.testPurchase);
            stream.WriteBoolean(si.isDefualtSettings);
            stream.WriteBoolean(si.showAllyHeal);
            stream.WriteBoolean(si.showEnemyHeal);
            stream.WriteBoolean(si.show_chat_button);
            stream.WriteBoolean(si.show_hp_mana_panel);
			stream.WriteBoolean(si.allowMiniTutorials);
		}

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            GameSettings si = new GameSettings();
            si.isCheatsAllowed = stream.ReadBoolean();
            si.isNeedShowCheatPanel = stream.ReadBoolean();
            si.isNeedShowVersion = stream.ReadBoolean();
            si.isMusicEnabled = stream.ReadBoolean();
            si.isSoundEffectsEnabled = stream.ReadBoolean();
            si.isSoundEnvironmentEnabled = stream.ReadBoolean();
            si.soundValue = stream.ReadSingle();
            si.musicValue = stream.ReadSingle();
            si.envValue = stream.ReadSingle();
            si.currentLanguage = stream.ReadInt32();
            si.show_enemy_damage = stream.ReadBoolean();
            si.show_enemy_bottles = stream.ReadBoolean();
            si.show_player_bottles = stream.ReadBoolean();
            si.show_joystick = stream.ReadBoolean();
            si.enable_autoattack = stream.ReadBoolean();
            si.show_guild_name = stream.ReadBoolean();
            si.showPerkHUDDescription = stream.ReadBoolean();
            si.show_player_name = stream.ReadBoolean();
            si.show_fps = stream.ReadBoolean();
            si.fps_lock_30 = stream.ReadBoolean();
            si.SkillJoystickSensivity = stream.ReadSingle();
            si.camera_setting = stream.ReadByte();
            si.receive_push_notifications = stream.ReadBoolean();
            si.testPurchase = stream.ReadBoolean();
            si.isDefualtSettings = stream.ReadBoolean();
            si.showAllyHeal = stream.ReadBoolean();
            si.showEnemyHeal = stream.ReadBoolean();
            si.show_chat_button = stream.ReadBoolean();
            si.show_hp_mana_panel = stream.ReadBoolean();
			si.allowMiniTutorials = stream.ReadBoolean();

			return si;
        }
    }
}
