﻿using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public static class EntityPartInfoBit
	{
		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			EntityPartInfo si = (EntityPartInfo)value;
			stream.WriteSByte((sbyte)si.Slot);
			stream.Write<int>(0); //si.Id);
			stream.WriteSByte((sbyte)si.Rarity);
			stream.WriteSByte((sbyte)si.Color);
            stream.WriteStringFromId(si.Model);
        }

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			EntityPartInfo si = new EntityPartInfo();
			si.Slot = (Slots)stream.ReadSByte();
			int Id = stream.Read<int>();
			si.Rarity = (ItemRarity)stream.ReadSByte();
			si.Color = (ItemColor)stream.ReadSByte();
			return si;
		}
	}
}
