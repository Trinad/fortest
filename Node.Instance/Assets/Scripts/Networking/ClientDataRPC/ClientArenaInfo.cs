﻿using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.Networking.ClientDataRPC.Arenas;
using System;
using System.Collections.Generic;
using UtilsLib.Logic.Enums;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class WavesEntityData
    {
        public int Id { get; set; }
        public PersonType Type { get; set; }
    }

	//public class WavesMatchData
	//{
	//    public int BattleId;
	//    public bool IsActive { get; set; } //TODO fil надо убрать это поле оно мне не нравится из за него много костылей
	//    public float TimeSec { get; set; }
	//    public bool IsRoundStarted { get; set; }
	//    public int RoundId { get; set; }

	//    public List<WavesEntityData> Entities { get; set; }
	//    public List<ArenaPlayerInfo> player { get; set; }
	//    public int MonstersLeft { get; set; }
	//    public bool LastWaveCompleted { get; set; }
	//    public int MaxWaves { get; set; }

	//    public bool NeedKillPreviousWave { get; set; }
	//    public bool IsAlive { get; set; }
	//    //TODO_maximpr из-за этой переменной много костылей
	//    public int MyId { get; set; }

	//    public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
	//    {
	//        WavesMatchData si = (WavesMatchData)value;
	//        stream.Write<bool>(si.IsActive);
	//        stream.Write<float>(si.TimeSec);
	//        stream.Write<bool>(si.IsRoundStarted);
	//        stream.Write<byte>((byte)si.RoundId);
	//        stream.Write<List<WavesEntityData>>(si.Entities);
	//        stream.Write<List<ArenaPlayerInfo>>(si.player);
	//        stream.Write<ushort>((ushort)si.MonstersLeft);
	//        stream.Write<bool>(si.LastWaveCompleted);
	//        stream.Write<byte>((byte)si.MaxWaves);
	//        stream.Write<bool>(si.NeedKillPreviousWave);
	//        stream.Write<bool>(si.IsAlive);
	//        stream.Write<int>(si.MyId);
	//    }
	//}

	public class TeamDeathmatchData
    {
        public GameResult GameResult;
        public int BattleId;
        public bool IsActive { get; set; }
        public float TimeSec { get; set; }
        public bool IsRoundStarted { get; set; }
        public int RoundId { get; set; }

        public int RoundRedScore { get; set; }
        public int RoundBlueScore { get; set; }
        public int MatchRedScore { get; set; }
        public int MatchBlueScore { get; set; }

        public bool FullInfo { get; set; }
        public List<int> RedScores { get; set; }
        public List<int> BlueScores { get; set; }

        public List<ArenaPlayerInfo> Players { get; set; }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            TeamDeathmatchData si = (TeamDeathmatchData)value;
            stream.Write<bool>(si.IsActive);
            stream.Write<float>(si.TimeSec);
            stream.Write<bool>(si.IsRoundStarted);
            stream.Write<byte>((byte)si.RoundId);
            stream.Write<short>((short)si.RoundRedScore);
            stream.Write<short>((short)si.RoundBlueScore);
            stream.Write<byte>((byte)si.MatchRedScore);
            stream.Write<byte>((byte)si.MatchBlueScore);
            stream.Write<List<ArenaPlayerInfo>>(si.Players);
            stream.Write<bool>(si.FullInfo);
            if (si.FullInfo)
            {
                stream.Write<List<int>>(si.RedScores);
                stream.Write<List<int>>(si.BlueScores);
                stream.Write(si.GameResult);
            }
        }
    }

    public class ArenaMatchData
    {
        public int BattleId;
        public bool IsActive { get; set; }
        public float TimeSec { get; set; }
        public bool IsRoundStarted { get; set; }
        public int RoundId { get; set; }
        public ArenaMatchType ArenaType;

		public bool FullInfo{ get; set; }

		public List<ArenaPlayerInfo> Players { get; set; }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            ArenaMatchData si = (ArenaMatchData)value;
            stream.Write<bool>(si.IsActive);
            stream.Write<float>(si.TimeSec);
            stream.Write<bool>(si.IsRoundStarted);
            stream.Write<byte>((byte)si.RoundId);
            stream.Write<ArenaMatchType>(si.ArenaType);
            stream.Write<bool>(si.FullInfo);
            stream.Write<List<ArenaPlayerInfo>>(si.Players);
        }
    }

    public class BrawlMatchData
    {
        public int BattleId;
        public GameResult GameResult;
        public bool IsActive { get; set; }
        public float TimeSec { get; set; }
        public bool IsRoundStarted { get; set; }
        public int RoundId { get; set; }

        public int RedScore;
        public int BlueScore;
        public int RedTowerEntityId;
        public List<int> RedTourelEntityId;
        public int BlueTowerEntityId;
        public List<int> BlueTourelEntityId;
        public List<ArenaPlayerInfo> Players;

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            BrawlMatchData si = (BrawlMatchData)value;
            stream.Write(si.IsActive);
            stream.Write(si.TimeSec);
            stream.Write(si.IsRoundStarted);
            stream.Write((byte)si.RoundId);
            stream.Write(si.RedScore);
            stream.Write(si.BlueScore);
            stream.Write(si.RedTowerEntityId);
            stream.Write(si.RedTourelEntityId);
            stream.Write(si.BlueTowerEntityId);
            stream.Write(si.BlueTourelEntityId);
            stream.Write(si.Players);
            stream.Write(si.GameResult);
        }
    }

	public class ArenaPlayerInfo
    {
        //Нужны везде
        public int PersonId;
        public int EntityId;

        public int Kills;
        public int KillScore;
        public void AddKill()
        {
            Kills++;
            KillScore++;
        }
        public void AddKill(int score)
        {
            Kills++;
            KillScore += score;
        }
        public int Deaths;
        public void AddDeath()
        {
            Deaths++;
        }
        public int Assists; 
        public int AssistsScore; //Нужно перепроверить подсчет этих очков
        public void AddAssist(int score)
        {
            Assists++;
            AssistsScore += score;
        }

        public int HealCount; //Неработает как нужно
        public int HealAmount;
        public int HealScore; 
        public void AddHeal(int value)
        {
            HealCount++;
            HealAmount += value;
            HealScore = HealCount;
        }

        public int DamageDealed;
        public int DamageReseaved;

        public string Name;
        public string AvatarName = string.Empty;
        public bool Disconnected;
        public PlayerTeam playerTeam;
        public PersonType PersonType;
        public int GearScore;
        public int Level;
        public bool IsMVP;

        public int FullScore { get { return KillScore +
                                            AssistsScore +
                                            HealScore +
                                            FlagsScore +
                                            WaveScore +
                                            TurretsKillsScore + TowersKillsScore +
                                            TeamScore; } }
        public int Place;
        public int startRating;

        //Только CTF
        public byte Flags;
        public int FlagsScore;
        public void AddFlag(int score)
        {
            Flags++;
            FlagsScore += score;
        }


        //Только волны
        public sbyte LastComplitedWaveId;
        public int WaveScore;
        public void AddWaveScore(sbyte value, int score)
        {
            LastComplitedWaveId = value;
            WaveScore += score;
        }

        //Только бравл
        public int TowersKills;
        public int TowersKillsScore;
        public void AddTowerKill(int score)
        {
            TowersKills++;
            TowersKillsScore += score;
        }
        public int TurretsKills;
        public int TurretsKillsScore;
        public void AddTurretKill(int score)
        {
            TurretsKills++;
            TurretsKillsScore += score;
        }
        public float TurretTeamScore;
        public float TowerTeamScore;

        //Только режимы с командами
        public int Wins;
        public int TeamScore;
        public void AddTeamScore(bool win, int value)
        {
            if (win)
                Wins++;
            TeamScore += value;
        }

        //неотправляемые данные
        public bool bDead;
        public int deathNumber;
        public int gameCount;
        public bool isPerson;
        public bool ArenaGameFinish; //Игрок завершил арену. Если арена завершилась, а эта булка false, значит игрок ушел по-английски

        

        public override string ToString()
        {
            return $"{Name} {playerTeam}";
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            ArenaPlayerInfo si = (ArenaPlayerInfo)value;
            stream.Write<int>(si.PersonId);
            stream.Write<int>(si.EntityId);
            stream.Write<byte>((byte)si.Kills);
            stream.Write<byte>((byte)si.Deaths);
            stream.Write<byte>((byte)si.Assists);
            stream.Write<byte>((byte)si.Flags);
            stream.Write<byte>((byte)si.TowersKillsScore);
            stream.Write<byte>((byte)si.TurretsKillsScore);
            stream.Write<short>((short)si.FullScore);
            stream.Write<byte>((byte)si.Place);
            stream.Write<string>(si.Name);
            stream.Write<bool>(si.Disconnected);
            stream.Write<sbyte>((sbyte)si.playerTeam);
            stream.Write<sbyte>(si.LastComplitedWaveId);
            stream.Write<uint>((uint)si.DamageDealed);
            stream.Write<uint>((uint)si.DamageReseaved);
            stream.Write<sbyte>((sbyte)si.PersonType);
            stream.Write<ushort>((ushort)si.GearScore);
            stream.Write<byte>((byte)(si.Level));
            stream.Write<ushort>((ushort)si.startRating);//RatingScore
            stream.Write<bool>(si.IsMVP);
            stream.Write<uint>((uint)si.HealAmount);
            stream.Write<string>(si.AvatarName);
            stream.Write<ushort>((ushort)si.KillScore);
            stream.Write<ushort>((ushort)si.AssistsScore);
            stream.Write<ushort>((ushort)si.HealCount);
            stream.Write<ushort>((ushort)si.HealScore);
            stream.Write<ushort>((ushort)si.FlagsScore);
            stream.Write<ushort>((ushort)si.WaveScore);
            stream.Write<byte>((byte)si.TowersKills);
            stream.Write<byte>((byte)si.TurretsKills);
            stream.Write<byte>((byte)si.Wins);
            stream.Write<ushort>((ushort)si.TeamScore);//WinScores
			stream.Write<byte>((byte)si.TowerTeamScore);
			stream.Write<byte>((byte)si.TurretTeamScore);
        }
    }

    //Пока закомменчу, я ещё работаю над этой частью
    /*
    public class ArenaPlayerInfoStandart
    {
        public int PersonId;
        public int EntityId;

        public int Kills;
        public int KillScore;
        public void AddKill()
        {
            Kills++;
            KillScore++;
        }

        public int Deaths;
        public void AddDeath()
        {
            Deaths++;
        }
        public int Assists;
        public int AssistsScore; //Нужно перепроверить подсчет этих очков
        public void AddAssist(int score)
        {
            Assists++;
            AssistsScore += score;
        }

        public int HealCount; //Неработает как нужно
        public int HealAmount;
        public int HealScore;
        public void AddHeal(int value)
        {
            HealCount++;
            HealAmount += value;
            HealScore = HealCount;
        }

        public int DamageDealed;//Очки не нужны
        public int DamageReseaved;//Очки не нужны

        public string Name;
        public string AvatarName = string.Empty;
        public PlayerTeam playerTeam;
        public PersonType PersonType;
        public int GearScore;
        public int Level;
        public bool IsMVP;

        public bool Disconnected;

        public int score;
        public virtual int FullScore
        {
            get
            {
                return score +
                       KillScore +
                       AssistsScore +
                       HealScore;
            }
        }
        public int Place;
        public int startRating;

        //неотправляемые данные
        public bool bDead;
        public int deathNumber;
        public int gameCount;
        public bool isPerson;
        public bool ArenaGameFinish; //Игрок завершил арену. Если арена завершилась, а эта булка false, значит игрок ушел по-английски



        public override string ToString()
        {
            return $"{Name} {playerTeam}";
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            ArenaPlayerInfoStandart si = (ArenaPlayerInfoStandart)value;
            stream.Write<int>(si.PersonId);
            stream.Write<int>(si.EntityId);

            stream.Write<byte>((byte)si.Kills);
            stream.Write<ushort>((ushort)si.KillScore);

            stream.Write<byte>((byte)si.Deaths);

            stream.Write<byte>((byte)si.Assists);
            stream.Write<ushort>((ushort)si.AssistsScore);

            stream.Write<ushort>((ushort)si.HealCount);
            stream.Write<ushort>((ushort)si.HealAmount);
            stream.Write<ushort>((ushort)si.HealScore);

            stream.Write<uint>((uint)si.DamageDealed);
            stream.Write<uint>((uint)si.DamageReseaved);

            stream.Write<string>(si.Name);

            stream.Write<string>(si.AvatarName);

            stream.Write<sbyte>((sbyte)si.playerTeam);

            stream.Write<sbyte>((sbyte)si.PersonType);

            stream.Write<ushort>((ushort)si.GearScore);

            stream.Write<byte>((byte)(si.Level));

            stream.Write<bool>(si.IsMVP);

            stream.Write<bool>(si.Disconnected);

            stream.Write<short>((short)si.FullScore);

            stream.Write<byte>((byte)si.Place);
                   
            stream.Write<ushort>((ushort)si.startRating);//RatingScore
        }
    }

    public class ArenaPlayerInfoCTF : ArenaPlayerInfoTeam
    {
        public byte Flags;
        public int FlagsScore;
        public void AddFlag(int score)
        {
            Flags++;
            FlagsScore += score;
        }

        public override int FullScore
        {
            get
            {
                return score +
                      KillScore +
                      AssistsScore +
                      HealScore +
                      FlagsScore +
                      TeamScore;
            }
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            ArenaPlayerInfoCTF si = (ArenaPlayerInfoCTF)value;
            stream.Write<int>(si.PersonId);
            stream.Write<int>(si.EntityId);

            stream.Write<byte>((byte)si.Kills);
            stream.Write<ushort>((ushort)si.KillScore);

            stream.Write<byte>((byte)si.Deaths);

            stream.Write<byte>((byte)si.Assists);
            stream.Write<ushort>((ushort)si.AssistsScore);

            stream.Write<ushort>((ushort)si.HealCount);
            stream.Write<ushort>((ushort)si.HealAmount);
            stream.Write<ushort>((ushort)si.HealScore);

            stream.Write<uint>((uint)si.DamageDealed);
            stream.Write<uint>((uint)si.DamageReseaved);

            stream.Write<string>(si.Name);

            stream.Write<string>(si.AvatarName);

            stream.Write<sbyte>((sbyte)si.playerTeam);

            stream.Write<sbyte>((sbyte)si.PersonType);

            stream.Write<ushort>((ushort)si.GearScore);

            stream.Write<byte>((byte)(si.Level));

            stream.Write<bool>(si.IsMVP);

            stream.Write<bool>(si.Disconnected);

            stream.Write<short>((short)si.FullScore);

            stream.Write<byte>((byte)si.Place);

            stream.Write<ushort>((ushort)si.startRating);//RatingScore

            stream.Write<byte>((byte)si.Flags);
            stream.Write<ushort>((ushort)si.FlagsScore);
        }

    }

    public class ArenaPlayerInfoTeam : ArenaPlayerInfoStandart
    {
        //Только режимы с командами
        public int TeamScore;
        public void AddTeamScore(int value)
        {
            TeamScore += value;
        }

        public override int FullScore
        {
            get
            {
                return score +
                      KillScore +
                      AssistsScore +
                      HealScore +
                      TeamScore;
            }
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            ArenaPlayerInfoTeam si = (ArenaPlayerInfoTeam)value;
            stream.Write<int>(si.PersonId);
            stream.Write<int>(si.EntityId);

            stream.Write<byte>((byte)si.Kills);
            stream.Write<ushort>((ushort)si.KillScore);

            stream.Write<byte>((byte)si.Deaths);

            stream.Write<byte>((byte)si.Assists);
            stream.Write<ushort>((ushort)si.AssistsScore);

            stream.Write<ushort>((ushort)si.HealCount);
            stream.Write<ushort>((ushort)si.HealAmount);
            stream.Write<ushort>((ushort)si.HealScore);

            stream.Write<uint>((uint)si.DamageDealed);
            stream.Write<uint>((uint)si.DamageReseaved);

            stream.Write<string>(si.Name);

            stream.Write<string>(si.AvatarName);

            stream.Write<sbyte>((sbyte)si.playerTeam);

            stream.Write<sbyte>((sbyte)si.PersonType);

            stream.Write<ushort>((ushort)si.GearScore);

            stream.Write<byte>((byte)(si.Level));

            stream.Write<bool>(si.IsMVP);

            stream.Write<bool>(si.Disconnected);

            stream.Write<short>((short)si.FullScore);

            stream.Write<byte>((byte)si.Place);

            stream.Write<ushort>((ushort)si.startRating);//RatingScore

            
        }

    }

    public class ArenaPlayerInfoBrawl : ArenaPlayerInfoTeam
    {
        public float TowersKills;
        public void AddTowerKill()
        {
            TowersKills++;
        }
        public float TurretsKills;
        public void AddTurretKill()
        {
            TurretsKills++;
        }
        public float TurretTeamScore;
        public float TowerTeamScore;

        public override int FullScore
        {
            get
            {
                return score +
                      KillScore +
                      AssistsScore +
                      HealScore +
                      TeamScore;
            }
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            ArenaPlayerInfoBrawl si = (ArenaPlayerInfoBrawl)value;
            stream.Write<int>(si.PersonId);
            stream.Write<int>(si.EntityId);

            stream.Write<byte>((byte)si.Kills);
            stream.Write<ushort>((ushort)si.KillScore);

            stream.Write<byte>((byte)si.Deaths);

            stream.Write<byte>((byte)si.Assists);
            stream.Write<ushort>((ushort)si.AssistsScore);

            stream.Write<ushort>((ushort)si.HealCount);
            stream.Write<ushort>((ushort)si.HealAmount);
            stream.Write<ushort>((ushort)si.HealScore);

            stream.Write<uint>((uint)si.DamageDealed);
            stream.Write<uint>((uint)si.DamageReseaved);

            stream.Write<string>(si.Name);

            stream.Write<string>(si.AvatarName);

            stream.Write<sbyte>((sbyte)si.playerTeam);

            stream.Write<sbyte>((sbyte)si.PersonType);

            stream.Write<ushort>((ushort)si.GearScore);

            stream.Write<byte>((byte)(si.Level));

            stream.Write<bool>(si.IsMVP);

            stream.Write<bool>(si.Disconnected);

            stream.Write<short>((short)si.FullScore);

            stream.Write<byte>((byte)si.Place);

            stream.Write<ushort>((ushort)si.startRating);//RatingScore

            stream.Write<byte>((byte)si.TowersKills);
            stream.Write<byte>((byte)si.TurretsKills);
            //Возможно это слать ненужно
            stream.Write<byte>((byte)si.TowerTeamScore);
            stream.Write<byte>((byte)si.TurretTeamScore);
        }

    }

    public class ArenaPlayerInfoWaves : ArenaPlayerInfoTeam
    {
        public sbyte LastComplitedWaveId;
        public int WaveScore;
        public void AddWaveScore(int value, int score)
        {
            LastComplitedWaveId = (sbyte)value;
            WaveScore += score;
        }
        public override int FullScore
        {
            get
            {
                return score +
                      KillScore +
                      AssistsScore +
                      HealScore +
                      WaveScore +
                      TeamScore;
            }
        }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            ArenaPlayerInfoWaves si = (ArenaPlayerInfoWaves)value;
            stream.Write<int>(si.PersonId);
            stream.Write<int>(si.EntityId);

            stream.Write<byte>((byte)si.Kills);
            stream.Write<ushort>((ushort)si.KillScore);

            stream.Write<byte>((byte)si.Deaths);

            stream.Write<byte>((byte)si.Assists);
            stream.Write<ushort>((ushort)si.AssistsScore);

            stream.Write<ushort>((ushort)si.HealCount);
            stream.Write<ushort>((ushort)si.HealAmount);
            stream.Write<ushort>((ushort)si.HealScore);

            stream.Write<uint>((uint)si.DamageDealed);
            stream.Write<uint>((uint)si.DamageReseaved);

            stream.Write<string>(si.Name);

            stream.Write<string>(si.AvatarName);

            stream.Write<sbyte>((sbyte)si.playerTeam);

            stream.Write<sbyte>((sbyte)si.PersonType);

            stream.Write<ushort>((ushort)si.GearScore);

            stream.Write<byte>((byte)(si.Level));

            stream.Write<bool>(si.IsMVP);

            stream.Write<bool>(si.Disconnected);

            stream.Write<short>((short)si.FullScore);

            stream.Write<byte>((byte)si.Place);

            stream.Write<ushort>((ushort)si.startRating);//RatingScore

            stream.Write<sbyte>(si.LastComplitedWaveId);
            stream.Write<ushort>((ushort)si.WaveScore);
        }
    }*/
}
