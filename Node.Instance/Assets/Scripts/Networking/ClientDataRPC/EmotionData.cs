﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.XmlData;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class EmotionData
    {
        public int Id { get; set; }
        public int SlotId { get; set; }
        public int Price { get; set; }
        public int RestoreTime { get; set; }
        public bool IsAvailable { get; set; }
        public bool ShowOnMinimap { get; set; }


        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            EmotionData si = (EmotionData)value;
            stream.Write<int>(si.Id);
            stream.Write<int>(si.SlotId);
            stream.Write<int>(si.Price);
            stream.Write<int>(si.RestoreTime);
            stream.Write<bool>(si.IsAvailable);
            stream.Write<bool>(si.ShowOnMinimap);
        }

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            EmotionData si = new EmotionData();
            si.Id = stream.Read<int>();
            si.SlotId = stream.Read<int>();
            si.Price = stream.Read<int>();
            si.RestoreTime = stream.Read<int>();
            si.IsAvailable = stream.Read<bool>();
            si.ShowOnMinimap = stream.Read<bool>();

            return si;
        }

        public static explicit operator EmotionData(Emotion emotion)
        {
            var totalSec = Convert.ToInt32((emotion.RestoreTime - DateTime.UtcNow).TotalSeconds);
            var newEmotionData = new EmotionData()
            {
                Id = emotion.Id,
                IsAvailable = emotion.IsAvailable,
                Price = emotion.Price,
                RestoreTime = totalSec == 0 ? -1 : totalSec,
                SlotId = emotion.SlotId

            };
            return newEmotionData;
        }
    }
}
