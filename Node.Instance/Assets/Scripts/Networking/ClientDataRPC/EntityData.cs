﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.InstanceServer.Enums;
using UtilsLib.Logic.Enums;
using System.Text;
using LobbyInstanceLib.Networking;
using UtilsLib.NetworkingData;
using UtilsLib;
using UtilsLib.Logic;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class EntityData
	{
		public int Id { get; private set; }
		public EntityType EntityType { get; private set; }
		public PersonType PersonType { get; private set; }
		public string PrefabName { get; private set; }
        public string BundleName { get; private set; }
        public Vector3 Position { get; private set; }
		public Vector2 Direction { get; private set; }
		public uLink.NetworkViewID ViewId { get; private set; }
		public CharacterType CharacterType { get; private set; }
		public FractionType Fraction { get; private set; }
		public string NickName { get; private set; }
		public float Radius { get; private set; }
		public float Height { get; private set; }
		public float ScaleMult { get; private set; }
		public List<EntityPartInfo> Parts { get; set; }
        public int Level { get; set; }
        public MonsterType MonsterType { get; set; }
		public float Transparency { get; set; }
		public float InfoPointHeightDelta { get; set; }
        public int GuildId { get; private set; }
		public ShortClanData GuildData { get; set; }
        public PlayerTeam colorName { get; set; }
        public bool IsDead;
		public float ResurrectTime;
		public float CurrentResurrectTime;
        public Elements Element = Elements.Neutral;
        public string TechnicalName = string.Empty;
        public float SendRate = 0.2f;
        public bool isNeedHideHelmet;
        public HealthTypeVisual healthTypeVisual;
        public float maxLifeTime;
        public float remainingLifeTime;
        public int health;
        public int maxHealth;
        public bool isTargetable;

        public EntityData()
		{
		}

        public EntityData(MapTargetObject target)
        {
            Id = target.Id;
            EntityType = target.eType;
            PersonType = target.personType;
            PrefabName = target.TargetName;
            BundleName = target.BundleName;
            Position = target.position;
            Direction = target.direction;
            ViewId = target.view.viewID;
            CharacterType = target.characterType;
            Fraction = target.fraction;

            Height = target.Height;
            ScaleMult = target.ScaleMult;

            Transparency = 1f;

            GuildId = -1;
            GuildData = null;

            colorName = PlayerTeam.None;
            healthTypeVisual = target.healthTypeVisual;
            isTargetable = target.isTargetable;

            Parts = target.EquippedWears;
			if(Parts == null)
                Parts = new List<EntityPartInfo>();

            SendRate = 0.2f;

            if (target is PersonBehaviour person)
            {
	            InstancePerson ip = person.owner;

                NickName = ip.personName;
                Level = ip.HeroPower;
                Transparency = person.bOnMap || person.bReconnect ? 1f : 0.5f;
				
                GuildId = ip.GuildId;
                if (GuildId > 0)
                {
                    GuildData = new ShortClanData(Guild.GetGuildDataById(GuildId), ip.GuildRole);
                }

                IsDead = person.IsDead;
				ResurrectTime = ip.ResurrectSec;
				CurrentResurrectTime = ip.GetResurrectTime();
                SendRate = PersonBehaviour.SEND_RATE;

                isNeedHideHelmet = ip.isNeedHideHelmet;
            }
            else if (target is MonsterBehaviour monster)
            {
	            if (Cheats.ShowMonsterName)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(monster.Data.Name + " ");
                    var statContainer = monster.GetStatContainer();
                    sb.Append("AttackPower=" + statContainer.GetStatValue("AttackPower") + ";");
                    sb.Append("HP=" + statContainer.GetStatValue("HP") + ";");
                    sb.Append("Level=" + monster.Level + ";");

                    NickName = sb.ToString();
                }
                Level = monster.Level;
				NickName = monster.NickName;
                TechnicalName = monster.TechnicalName;
                MonsterType = monster.Data.MonsterType;
                Element = monster.Data.Element;
                IsDead = monster.IsDead;
	            ResurrectTime = monster.ResurrectSec;
	            CurrentResurrectTime = monster.GetResurrectTime();
                SendRate = MonsterBehaviour.SEND_RATE;
                isNeedHideHelmet = FRRandom.CheckChance(0.5);
            }
			else if (target is NPCBehaviour npc)
			{
				NickName = npc.NickName;
				TechnicalName = npc.TechnicalName;
			}
			else
			{
				NickName = target.NickName;
			}

			colorName = target.TeamColor;
            Radius = target.Radius;
            InfoPointHeightDelta = 0;

            maxLifeTime = target.maxLifeTime;
            remainingLifeTime = target.RemainingLifeTime;

            health = target.Health;
            maxHealth = target.MaxHealth;
    }

	    public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			EntityData si = (EntityData) value;
			stream.Write<int>(si.Id);
			stream.Write(si.EntityType);
			stream.Write(si.PersonType);
			stream.Write<string>(si.PrefabName);
			stream.Write<string>(si.BundleName);
			stream.WriteVec3Short(si.Position);
			stream.WriteDirectionByteAngle(si.Direction);
			stream.WriteNetworkViewID(si.ViewId);
			stream.Write(si.CharacterType);
			stream.Write(si.Fraction);
			stream.Write<string>(si.NickName);
			stream.WriteFloatUnsignedPacked(si.Radius);
			stream.WriteFloatUnsignedPacked(si.Height);
			stream.WriteFloatUnsignedPacked(si.ScaleMult);
			stream.Write<List<EntityPartInfo>>(si.Parts);
            stream.WriteUInt16((ushort)si.Level);
            stream.Write(si.MonsterType);
			stream.WriteFloatUnsignedPacked(si.Transparency);
			stream.WriteFloatUnsignedPacked(si.InfoPointHeightDelta);
            stream.Write(si.colorName);
			stream.Write<int>(si.GuildId);
            if (si.GuildId > 0)
                stream.Write(si.GuildData);
            stream.Write<bool>(si.IsDead);
			if (si.IsDead)
			{
				stream.Write(si.ResurrectTime);
				stream.Write(si.CurrentResurrectTime);
			}
            stream.Write(si.Element);
            stream.WriteStringFromId(si.TechnicalName);
            stream.WriteFloatUnsignedPacked(si.SendRate);
            stream.Write(si.isNeedHideHelmet);
            stream.Write(si.healthTypeVisual);

            stream.Write(si.maxLifeTime);
            stream.Write(si.remainingLifeTime);

            stream.Write(si.health);
            stream.Write(si.maxHealth);
            stream.Write(si.isTargetable);
        }

		public override string ToString()
		{
			return string.Format("Error = {0}", Id);
		}
	}
}
