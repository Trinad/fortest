﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Networking.ClientDataRPC
{
    public class EmotionPackData
    {
        public int PackPrice { get; set; }
        public int BonusEmotionId { get; set; }
        public bool InApp { get; set; }
        public string Sku { get; set; }
        public int PointId { get; set; }
        public List<EmotionData> EmotionList { get; set; }

        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            EmotionPackData si = (EmotionPackData)value;
            stream.Write<int>(si.PackPrice);
            stream.Write<int>(si.BonusEmotionId);
            stream.Write<bool>(si.InApp);
            stream.Write<string>(si.Sku);
            stream.Write<int>(si.PointId);

            stream.Write<List<EmotionData>>(si.EmotionList);
        }

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            EmotionPackData si = new EmotionPackData();
            si.PackPrice = stream.Read<int>();
            si.BonusEmotionId = stream.Read<int>();
            si.InApp = stream.Read<bool>();
            si.Sku = stream.Read<string>();
            si.PointId = stream.Read<int>();

            si.EmotionList = stream.Read<List<EmotionData>>();

            return si;
        }
    }
}
