
using System.Numerics;
using LobbyInstanceLib.Networking;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public sealed class CageStoneData
	{
		public int Id { get; set; }
		public Vector3 Pos { get; set; }

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			CageStoneData si = (CageStoneData)value;
			stream.Write<int>(si.Id);
			stream.WriteVec3(si.Pos);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			CageStoneData si = new CageStoneData();
			si.Id = stream.Read<int>();
			si.Pos = stream.ReadVec3();
			return si;
		}
	}

	public sealed class CageWallData
	{
		public int Id { get; set; }
		public Vector3 Pos { get; set; }
		public Vector3 Dir { get; set; }

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			CageWallData si = (CageWallData)value;
			stream.Write<int>(si.Id);
			stream.WriteVec3(si.Pos);
			stream.WriteVec3(si.Dir);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			CageWallData si = new CageWallData();
			si.Id = stream.Read<int>();
			si.Pos = stream.ReadVec3();
			si.Dir = stream.ReadVec3();
			return si;
		}
	}
}
