﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Utils.Common;
using uLink;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class MapInfoData
	{
		public Vector3 min;
		public Vector3 max;
		public List<int> destroyedObj;
		public int terrainType;
		public List<LocParamData> MapTitleParams;
		public LocalizationLevelName mapId;
		public LocationType locationType;
		public string levelLocalocationlName;
		public bool PVP;

		public MapInfoData()
		{
		}

		public MapInfoData(Map map, InstancePerson person)
		{
			min = new Vector3(0, 0, 0);
			max = new Vector3(map.heightX * Helper.VoxelSize, 1000, map.heightZ * Helper.VoxelSize);
			destroyedObj = map.objects.Where(x => x.IsDead || !x.VisibleFor(person))
				.Select(x => x.Id).ToList();
			terrainType = Map.level.type;
			MapTitleParams = map.GetTitleParams(person);
			mapId = map.GetTitle(person);
            locationType = Map.Edge.Type;
			levelLocalocationlName = Map.Edge.TechnicalName;
			PVP = Map.Edge.PVP;
		}

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			MapInfoData si = (MapInfoData) value;
			stream.WriteVector3(si.min);
			stream.WriteVector3(si.max);
			stream.Write<List<int>>(si.destroyedObj);
			stream.Write((int) si.terrainType);
			stream.Write<List<LocParamData>>(si.MapTitleParams);
			stream.Write((int) si.mapId);
			stream.Write((int) si.locationType);
			stream.WriteStringFromId(si.levelLocalocationlName);
			stream.Write(si.PVP);
		}

		public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			MapInfoData si = new MapInfoData();
			si.min = stream.ReadVector3();
			si.max = stream.ReadVector3();
			si.destroyedObj = stream.Read<List<int>>();
			si.terrainType = stream.ReadInt32();
			si.MapTitleParams = stream.Read<List<LocParamData>>();
			si.mapId = (LocalizationLevelName) stream.ReadInt32();
			si.locationType = (LocationType) stream.ReadInt32();
			si.levelLocalocationlName = stream.ReadString();
			si.PVP = stream.ReadBoolean();
			return si;
		}
	}
}
