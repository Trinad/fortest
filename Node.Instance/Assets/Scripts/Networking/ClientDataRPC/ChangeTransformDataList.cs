﻿using System.Collections.Generic;

namespace Assets.Scripts.Networking.ClientDataRPC
{
	public class ChangeTransformDataList<T>
	{
		public List<T> list = new List<T>();

		public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
		{
			var si = (ChangeTransformDataList<T>)value;
			stream.WriteByte((byte)si.list.Count);
			foreach (var obj in si.list)
				stream.Write(obj);
		}
	}
}
