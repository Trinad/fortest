﻿using System;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.Utils.Db;
using System.Collections.Generic;
using uLink;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using UtilsLib.Protocol;
using Zenject;
using UtilsLib.NetworkingData.Chat;

namespace Assets.Scripts.Networking
{
	public sealed class GuildController : BaseInstanceRpcListener, IInstanceSysGuild_Clan<NetworkP2PMessageInfo>
	{
		private ClanSettingsData guildSettings;
		private IPersonsOnServer personsOnServer;
		private InstanceSqlAdapter sqlAdapter;
		private ChatController chatController;

		public GuildController(NetworkP2P networkP2P, uLink.Network network, INetworkView view) : base(networkP2P, network, view)
		{
		}

		[Inject]
		public void Construct(IPersonsOnServer _personsOnServer, ClanSettingsData _guildSettings, InstanceSqlAdapter _sqlAdapter, GlobalEvents globalEvents, ChatController _chatController)
		{
			personsOnServer = _personsOnServer;
			guildSettings = _guildSettings;
			sqlAdapter = _sqlAdapter;
			chatController = _chatController;

			globalEvents.MapCreate += map => map.PlayerConnected += OnMapOnPlayerConnected;
		}

		private void OnMapOnPlayerConnected(InstancePerson person, bool reconnect, bool changePerson)
		{
			if (changePerson)
				return;

			SendRpc(guild.OnRatingScoreChange, person.loginId, person.login.SummBestPoints());

			SendGuildDataResponse(person);
		}

		//Получить список или найти клан
		[RPC]
		public void ClanListRequest(InstancePerson person, string name)
		{
			SendRpc(guild.OnGuildList, person.loginId, name);
		}

		[RPC]
		public void ClanListAnswer(int loginId, List<GuildData> guilds, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"ClanListAnswer person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.ClanListResponse, guilds);
		}

		//Проверить имя клана
		[RPC]
		public void ClanCheckNameRequest(InstancePerson person, string clanName)
		{
			SendRpc(guild.OnGuildCheckName, person.loginId, clanName);
		}

		[RPC]
		public void CheckGuildNameAnswer(int loginId, ClientErrors error, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"CheckGuildNameAnswer person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			var clan = Guild.GetGuildDataById(person.GuildId);

			byte dayCooldown = 0;
			if (clan != null && clan.ClanNameCooldown > TimeProvider.UTCNow)
			{
				error = ClientErrors.CLAN_NAME_CHANGE_COOLDOWN;
				dayCooldown = (byte)Math.Ceiling((clan.ClanNameCooldown - TimeProvider.UTCNow).TotalDays);
			}

			SendRpc(person.ClientRpc.ClanCheckNameResponse, error, dayCooldown);
		}

		//Создать клан
		[RPC]
		public void ClanCreateRequest(InstancePerson person, GuildData clanData)
		{
			ILogger.Instance.Send($"ClanCreateRequest {clanData.ClanName}");

			//проверить что персонаж не в гильдии
			if (person.InGuild())
			{
				ILogger.Instance.Send($"ClanCreateRequest {clanData.ClanName} InGuild() = true");
				SendRpc(person.ClientRpc.ClanCreateResponse, ClientErrors.ALREADY_IN_GUILD_ERROR);
				return;
			}

			//проверить что есть нужное количество денег.
			if (!person.TryAddGold(-guildSettings.CreateClanPrice))
			{
				var resData = person.GetNeedForResource();
				resData.NeedResource(ResourceType.Gold, guildSettings.CreateClanPrice - person.Gold);
				SendRpc(person.ClientRpc.ErrorDataResponse, resData.GetError(person));

				SendRpc(person.ClientRpc.ClanCreateResponse, ClientErrors.NOT_ENOUGH_MONEY);
				ILogger.Instance.Send($"ClanCreateRequest {clanData.ClanName} not enought gold");
				return;
			}

			//отправить запрос о создании гильдии.
			SendRpc(guild.OnGuildCreate, person.loginId, person.personName, person.login.AvatarName, person.login.SummBestPoints(), clanData);
		}

		[RPC]
		public void GuildCreateAnswer(int loginId, ClientErrors error, GuildData gd, NetworkP2PMessageInfo info)
		{
			ILogger.Instance.Send("receive GuildCreateAnswer " + error);

			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"GuildCreateAnswer person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			ILogger.Instance.Send("send GuildCreateAnswer " + error);
			if (error == ClientErrors.SUCCESS)
			{
				SetGuild(loginId, gd, ClanRole.Commander, info);
			}
			else
			{
				person.TryAddGold(guildSettings.CreateClanPrice); // вернем персу деньги за неуспешную попытку
			}

			SendRpc(person.ClientRpc.ClanCreateResponse, error);
		}

		//Изменить настройки клана
		[RPC]
		public void ClanSaveChangesRequest(InstancePerson person, GuildData clanData)
		{
			ILogger.Instance.Send($"ClanSaveChangesRequest {clanData.ClanName}");
			var grs = InstanceGameManager.Instance.GuildSettings.GetRoleSettings(person.GuildRole);
			if (grs == null || !grs.CanEditClan)
			{
				SendRpc(person.ClientRpc.ClanSaveChangesResponse, ClientErrors.LOGIC_ERROR);
				return;
			}

			//отправить запрос на диспетчер о изменении гильдии.
			SendRpc(guild.OnGuildSaveChanges, person.loginId, clanData);
		}

		[RPC]
		public void GuildSaveChangeAnswer(int loginId, ClientErrors error, GuildData gd, NetworkP2PMessageInfo info)
		{
			ILogger.Instance.Send("GuildSaveChangeAnswer: " + error);

			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"GuildSaveChangeAnswer: person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			if (error == ClientErrors.SUCCESS)
			{
				Guild.SetGuild(gd);
				SendGuildDataResponse(person);

				if (person.syn != null)
					person.syn.SendRpc(person.syn.map.ClientRpc.UpdateLocationClanData, new ShortClanData(gd, person.GuildRole));
			}

			SendRpc(person.ClientRpc.ClanSaveChangesResponse, error);
		}

		//Посмотреть состав клана
		[RPC]
		public void ClanMemberListRequest(InstancePerson person, int clanId)
		{
			SendRpc(guild.OnClanMemberList, person.loginId, clanId);
		}

		[RPC]
		public void ClanMemberListAnswer(int loginId, int clanId, List<GuildPersonInfo> clanMemberList, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"OnClanMemberListAnswer: person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.ClanMemberListResponse, clanId, clanMemberList);
		}

		//Присоединиться к открытому клану/подать заявку в клан
		[RPC]
		public void ClanJoinRequest(InstancePerson person, int clanId, string joinDescription)
		{
			ILogger.Instance.Send("receive GuildJoinRequest " + clanId);

			if (person.login.ClanEnter > TimeProvider.UTCNow)
			{
				SendRpc(person.ClientRpc.ClanJoinResponse, ClientErrors.CLAN_LEAVE_WEEK_COOLDOWN);
				return;
			}

			SendRpc(guild.OnJoinGuild, person.loginId, person.personName, person.login.AvatarName, person.login.SummBestPoints(), joinDescription, clanId, guildSettings.MaxClanSize);
		}

		[RPC]
		public void JoinGuildAnswer(int loginId, ClientErrors error, GuildData gd, ClanRole roleId, NetworkP2PMessageInfo info)
		{
			ILogger.Instance.Send("receive JoinGuildAnswer " + error);

			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"JoinGuildAnswer person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			ILogger.Instance.Send("send JoinGuildAnswerJoinGuildAnswer " + error);
			if (error == ClientErrors.SUCCESS && gd.ClanId > 0)
			{
				//Сам перешел в другой клан
				if (person.InGuild())
					person.login.ClanLeave = TimeProvider.UTCNow.AddDays(7); //TODO_Deg вынести

				if (person.login.ClanLeave != DateTime.MinValue && person.login.ClanLeave > TimeProvider.UTCNow)
					person.login.ClanEnter = person.login.ClanLeave;

				SetGuild(loginId, gd, roleId, info);
			}

			SendRpc(person.ClientRpc.ClanJoinResponse, error);
		}

		//Поменять роль
		[RPC]
		public void ClanMemberRoleChangeRequest(InstancePerson person, int loginId, ClanRole roleId)
		{
			ILogger.Instance.Send($"ClanMemberRoleChangeRequest {loginId} {roleId}");

			SendRpc(guild.OnClanMemberRoleChange, person.loginId, loginId, roleId);
		}

		[RPC]
		public void ClanMemberRoleChangeAnswer(int loginId, int targetLoginId, ClientErrors error, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"ClanMemberRoleChangeAnswer person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			if (error == ClientErrors.SUCCESS)
			{
				SendRpc(guild.OnClanMemberList, person.loginId, person.GuildId); //TODO_Deg Обновить весь список
			}

			SendRpc(person.ClientRpc.ClanMemberRoleChangeResponse, error);
		}

		//Передать роль владельца
		[RPC]
		public void ClanMemberMakeOwnerRequest(InstancePerson person, int loginId)
		{
			ILogger.Instance.Send($"ClanMemberMakeOwnerRequest {loginId}");

			SendRpc(guild.OnClanMemberMakeOwner, person.loginId, loginId);
		}

		[RPC]
		public void ClanMemberMakeOwnerAnswer(int loginId, int targetLoginId, ClientErrors error, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"ClanMemberMakeOwnerAnswer person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			if (error == ClientErrors.SUCCESS)
			{
				SendRpc(guild.OnClanMemberList, person.loginId, person.GuildId); //TODO_Deg Обновить весь список
			}

			SendRpc(person.ClientRpc.ClanMemberMakeOwnerResponse, error);
		}

		//Выкинуть из клана себя или другого
		[RPC]
		public void ClanMemberKickRequest(InstancePerson person, int loginId)
		{
			ILogger.Instance.Send($"ClanMemberKickRequest {loginId}");

			if (person.loginId == loginId && person.login.ClanLeave > TimeProvider.UTCNow)
			{
				SendRpc(person.ClientRpc.ClanMemberKickResponse, ClientErrors.CLAN_LEAVE_WEEK_COOLDOWN);
				return;
			}

			SendRpc(guild.OnClanMemberKick, person.loginId, loginId, false);
		}

		[RPC]
		public void ClanMemberKickAnswer(int loginId, int targetLoginId, ClientErrors error, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"ClanMemberKickAnswer person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			//Сам вышел
			if (error == ClientErrors.SUCCESS && loginId == targetLoginId)
				person.login.ClanLeave = TimeProvider.UTCNow.AddDays(7); //TODO_Deg вынести

			SendRpc(person.ClientRpc.ClanMemberKickResponse, error);
		}

		//Данные о клане
		public void SendGuildDataResponse(InstancePerson person)
		{
			var login = person.login;

			var clanData = Guild.GetGuildDataById(login.GuildId) ?? new GuildData();

			ILogger.Instance.Send($"SendGuildDataResponse {login.loginId} {clanData.ClanId}");

			byte dayCooldown = 0;
			if (login.ClanLeave > TimeProvider.UTCNow)
				dayCooldown = (byte)Math.Ceiling((login.ClanLeave - TimeProvider.UTCNow).TotalDays);

			SendRpc(login.ClientRpc.ClanDataResponse, clanData, login.GuildRole, login.SummBestPoints(), dayCooldown);
		}

		//---//

		[RPC]
		public void GuildJoinListAnswer(int loginId, List<GuildJoin> joins, NetworkP2PMessageInfo info)
		{
			ILogger.Instance.Send("receive GuildJoinListResponse");

			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"GuildJoinListResponse person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.GuildJoinListResponse, ClientErrors.SUCCESS, joins);
		}

		[RPC]
		public void GuildJoinListRequest(InstancePerson person)
		{
			var grs = InstanceGameManager.Instance.GuildSettings.GetRoleSettings(person.GuildRole);
			if (grs == null || !grs.CanAcceptInvite)
			{
				SendRpc(person.ClientRpc.GuildJoinListResponse, ClientErrors.LOGIC_ERROR, new List<GuildJoin>());
				return;
			}

			ILogger.Instance.Send("receive GuildJoinListRequest ");

			SendRpc(guild.OnGuildJoinList, person.loginId);
		}

		[RPC]
		public void GuildCurrentJoinRequest(InstancePerson person)
		{
			ILogger.Instance.Send("receive GuildCurrentJoinRequest");

			//отправить запрос на диспетчер о изменении гильдии.
			SendRpc(guild.OnGuildCurrentJoinRequest, person.loginId);
		}

		[RPC]
		public void GuildCurrentJoinAnswer(int loginId, List<int> joins, NetworkP2PMessageInfo info)
		{
			ILogger.Instance.Send("receive GuildCurrentJoinResponse");

			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"GuildCurrentJoinResponse person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.GuildCurrentJoinResponse, joins);
		}

		[RPC]
		public void GuildSetJoinStatusRequest(InstancePerson person, int loginId, int personId, bool status)
		{
			ILogger.Instance.Send("receive GuildSetJoinStatusRequest");

			var grs = InstanceGameManager.Instance.GuildSettings.GetRoleSettings(person.GuildRole);
			if (grs == null || !grs.CanAcceptInvite)
			{
				SendRpc(person.ClientRpc.GuildSetJoinStatusResponse, ClientErrors.LOGIC_ERROR);
				return;
			}
			//if (person.personId == personId && person.loginId == loginId)
			//    return;

			//отправить запрос на диспетчер о изменении гильдии.
			SendRpc(guild.OnGuildSetJoinStatus, person.loginId, loginId, status);
		}

		[RPC]
		public void GuildSetJoinStatusAnswer(int loginId, int targetLoginId, ClientErrors error, NetworkP2PMessageInfo info)
		{
			ILogger.Instance.Send("receive GuildSetJoinStatusResponse");

			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"GuildSetJoinStatusResponse person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.GuildSetJoinStatusResponse, error);
		}

		[RPC]
		public void GuildInviteRequest(InstancePerson person, string loginName, string inviteText)
		{
			ILogger.Instance.Send("receive GuildInviteRequest");

			var grs = InstanceGameManager.Instance.GuildSettings.GetRoleSettings(person.GuildRole);
			if (grs == null || !grs.CanSendInvite)
			{
				SendRpc(person.ClientRpc.GuildInviteResponse, ClientErrors.LOGIC_ERROR);
				return;
			}

			if (person.login.loginName == loginName)
			{
				SendRpc(person.ClientRpc.GuildInviteResponse, ClientErrors.LOGIC_ERROR);
				return;
			}

			sqlAdapter.FindLogin(loginName,
				targetLoginId => SendRpc(guild.OnGuildInviteRequest, person.loginId, targetLoginId, inviteText),
				() => SendRpc(person.ClientRpc.GuildInviteResponse, ClientErrors.PERSON_NOT_FOUND)
			);
		}

		[RPC]
		public void GuildInviteAnswer(int loginId, ClientErrors error, NetworkP2PMessageInfo info)
		{
			ILogger.Instance.Send("receive GuildInviteResponse");

			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"GuildInviteResponse person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.GuildInviteResponse, error);
		}

		//---//

		[RPC]
		public void SetGuild(int loginId, GuildData gd, ClanRole roleId, NetworkP2PMessageInfo info)
		{
			ILogger.Instance.Send("receive SetGuild loginId = " + loginId + " guildId = " + gd.ClanId);

			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"SetGuild person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			ILogger.Instance.Send("SetGuild loginId = " + loginId + " guildId = " + gd.ClanId + " name = " + person.personName);
			Guild.SetGuild(gd);
			person.GuildId = gd.ClanId;
			person.GuildRole = roleId;

			SendGuildDataResponse(person);

			if (person.syn != null)
			{
				person.syn.SendRpc(person.syn.map.ClientRpc.UpdateLocationClanData, new ShortClanData(gd, person.GuildRole));
				person.syn.map.OnPersonChangeGuild(person);
			}
		}

		[RPC]
		public void ResetGuild(int loginId, NetworkP2PMessageInfo info)
		{
			ILogger.Instance.Send("receive ResetGuild loginId = " + loginId);

			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"SetGuild person == null loginId: {loginId}", ErrorLevel.error);
				return;
			}

			ILogger.Instance.Send("ResetGuild loginId = " + loginId + " name = " + person.personName);
			person.GuildRole = ClanRole.None;
			person.GuildId = -1;

			SendGuildDataResponse(person);

			if (person.syn != null)
			{
				person.syn.SendRpc(person.syn.map.ClientRpc.ResetLocationClanData);
				person.syn.map.OnPersonChangeGuild(person);
			}
		}

		public void RemoveLogin(int loginId)
		{
			SendRpc(guild.OnClanMemberKick, loginId, loginId, true);
		}

		//Чит для сброса кулдауна
		public void SendClanResetCooldown(InstancePerson person)
		{
			SendRpc(guild.OnClanResetCooldown, person.loginId);
		}
	}
}
