﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils.Common;
using Assets.Scripts.Utils.Logger;
using uLink;
using Vector2 = System.Numerics.Vector2;
using Vector3 = System.Numerics.Vector3;

namespace Assets.Scripts.Networking
{
    public static class BitStreamInstanceExtensions
    {
		/// <summary>
		/// Передать направление (нормализованный вектор2) как угол 0..360, упаковав его в байт.
		/// Будет небольшая потеря точности, которой мы можем пренебречь - диапазон углов 0..360 ужимается до 0..255 - ~ 2 градуса
		/// </summary>
	    public static void WriteDirectionByteAngle(this BitStream stream, Vector2 dir)
	    {
		    // приводим градусы поворорта из диапазона 0..360 к 0..255
		    float degreesClamped = dir.DirectionNormalizedToAngleDegree() / 360f * byte.MaxValue;
		    stream.WriteByte((byte)degreesClamped);
	    }

		public static Vector2 ReadDirectionByte(this BitStream stream)
		{
			// градусы придут в диапазоне 0..255, надо их привести к 0..360
			byte angleDegreesClamped = stream.ReadByte();
			float angleRad = (float)angleDegreesClamped / byte.MaxValue * 360f * Helper.Deg2Rad;
			var direction = new Vector2((float)Math.Cos(angleRad), (float)Math.Sin(angleRad));
			return direction;
		}

		public static Vector2DirectonPacked ToPackedDirection(this Vector2 dir)
		{
			return new Vector2DirectonPacked(dir);
		}

		public static Vector3PositionPacked ToPackedPosition(this Vector3 pos)
		{
			return new Vector3PositionPacked(pos);
		}

		public static Vector2PositionPacked ToPackedPosition(this Vector2 pos)
		{
			return new Vector2PositionPacked(pos);
		}

		public static FloatPacked ToFloatPackedPacked(this float value)
		{
			return new FloatPacked(value);
		}

		public static FloatUnsignedPacked ToFloatUnsignedPacked(this float value)
		{
			return new FloatUnsignedPacked(value);
		}

        public static void SetProtocolStrings(string[] strings)
        {
            protocolStrings = new Dictionary<string, int>();
            for (int i = 0; i < strings.Length; i++)
                protocolStrings[strings[i]] = i;
        }

        //private static Dictionary<int, string> ProtocolStrings = null;
        private static Dictionary<string, int> protocolStrings = new Dictionary<string, int>();

        public static void WriteStringFromId(this BitStream stream, string value)
        {
            if (value == null)
            {
                stream.WriteBoolean(true);
                stream.WriteInt32(-1);
                return;
            }

            if (protocolStrings.TryGetValue(value, out int id))
            {
                stream.WriteBoolean(true);
                stream.WriteInt32(id);
            }
            else
            {
                stream.WriteBoolean(false);
                stream.WriteString(value);

				ILogger.Instance.Send($"WriteStringFromId: {value}", ErrorLevel.warning);
            }
        }

        public static string ReadStringFromId(this BitStream stream)
        {
            //TODO_maximpr в эту сторону пока не работает
            //int id;
            //bool isBufferized = stream.ReadBoolean();
            //if (isBufferized)
            //    id = stream.ReadInt32();
            //else
            //    return stream.ReadString();

            //if (id == -1)
            //    return null;

            //return ProtocolStrings[id];
            throw new NotImplementedException();
        }
    }

    public class BuffString
    {
        public BuffString(string val)
        {
            value = val;
        }

        private string value;
        public static void Serialize(uLink.BitStream stream, object value, params object[] codecOptions)
        {
            BuffString si = (BuffString)value;
            stream.WriteStringFromId(si.value);
        }

        public static object DeSerialize(uLink.BitStream stream, params object[] codecOptions)
        {
            string val = stream.ReadStringFromId();
            BuffString si = new BuffString(val);
            return si;
        }

        public static implicit operator BuffString(string val)
        {
            return new BuffString(val);
        }

        public static implicit operator string(BuffString val)
        {
            return val.value;
        }
    }
}
