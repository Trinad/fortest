﻿using System.Collections.Generic;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using Zenject;
using uLink;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.Login.DataRPC;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Utils.Db;
using UtilsLib.XmlData;
using static Assets.Scripts.InstanceServer.Entries.Login.DataRPC.LobbyInfo;
using Assets.Scripts.Utils.Common;
using System;
using System.Linq;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Assets.Scripts.InstanceServer.Analytic;
using UtilsLib.NetworkingData.Party;

namespace Assets.Scripts.Networking
{
    public interface ILoginController
    {
        void SendCreatePersonTypes(InstancePerson person);
        void SendGetLobbyInfoResponse(InstancePerson person);
        void SendGetSelectMapResponse(InstancePerson instancePerson);
        void GetLobbyBuildAllDataRequest(InstancePerson person);
        void ArenaMatchTypeSelectRequest(InstancePerson person, int buttonId);
        void DeleteLogin(InstancePerson person);
    }


    public sealed class LoginController : BaseInstanceRpcListener, ILoginController
    {
	    private ArenaAlgorithm m_arenaAlgorithm;
	    private InstanceSqlAdapter sqlAdapter;
		private GameXmlData gameXmlData;
	    private ShopManager shopManager;

        public LoginController(NetworkP2P networkP2P, uLink.Network network, uLink.INetworkView view)
			: base(networkP2P, network, view)
		{
		}

		[Inject]
		public void Construct(ArenaAlgorithm arenaAlgorithm, InstanceSqlAdapter _sqlAdapter,
			GameXmlData _gameXmlData, ShopManager _shopManager, GlobalEvents globalEvents)
		{
		    m_arenaAlgorithm = arenaAlgorithm;
		    sqlAdapter = _sqlAdapter;
			gameXmlData = _gameXmlData;
		    shopManager = _shopManager;

			globalEvents.GiveBuildingKit += (login, building) => GetLobbyBuildAllDataRequest(login.Person); //обновить здания в наборе
        }

		bool bSale = false;

        [RPC]
        public void GetAccountInfoRequest(InstancePerson person)
        {
            var login = person.login;
            PlayerLoginType type = PlayerLoginType.Normal;
            ILogger.Instance.Send($"Login GetAccountInfoRequest rewrited = {login.rewrited}");
            if (login.rewrited)
            {
                type = PlayerLoginType.ReplaceDeviceID;
            }
            SendRpc(person.ClientRpc.GetAccountInfoResponse, type);
        }

        [RPC]
        public void SelectAccountRequest(InstancePerson person, int loginId /*, NetworkMessageInfo info*/)
        {
            ILogger.Instance.Send("Depricated request: SelectAccountRequest", ErrorLevel.warning);
            return;
        }

        [RPC]
		public void GetLobbyInfoRequest(InstancePerson person)
        {
            ILogger.Instance.Send($"GetLobbyInfoRequest, LoginId = {person.loginId}");

            SendGetLobbyInfoResponse(person);
            SendGetSelectMapResponse(person);
	        SendCreatePersonTypes(person);
	        ServerRpcListener.Instance.SendNotifications(person.login);
            InitInfluence(person);
        }

        void InitInfluence(InstancePerson instancePerson)
        {
            if (instancePerson.InfluenceDatas != null)
                instancePerson.StatContainer.SetInfluenceDatas(instancePerson.InfluenceDatas);
            instancePerson.InfluenceDatas = null;
        }

	    [RPC]
	    public void ArenaMatchTypeSelectRequest(InstancePerson person, int buttonId)
	    {
	        person.login.LastArenaButtonId = buttonId;
	        SendGetSelectMapResponse(person);
	    }

        public void SendGetLobbyInfoResponse(InstancePerson person)
		{
			var loginInfo = new LobbyInfo(person.login, bSale, gameXmlData.PlayerCityData, gameXmlData.ItemDataRepository, gameXmlData.ExpData);

			var map = InstanceGameManager.Instance.GetMapByGameId(person.GameId);
			map.OnGetLobbyInfoRequest(person, loginInfo);

            ServerRpcListener.Instance.SendCurrencyResponse(person);

            SendRpc(person.ClientRpc.GetLobbyInfoResponse, loginInfo);
		}

	    public void SendGetSelectMapResponse(InstancePerson instancePerson)
	    {
			int actualButtonId = m_arenaAlgorithm.GetActualButtonId(instancePerson, instancePerson.login.LastArenaButtonId);
			instancePerson.login.LastArenaButtonId = actualButtonId;

			SelectMapData result = new SelectMapData(instancePerson, m_arenaAlgorithm);

			SendRpc(instancePerson.ClientRpc.GetSelectMapResponse, result);
		}

		public void SendCreatePersonTypes(InstancePerson person)
		{
			var result = new Dictionary<PersonType, bool>();
			foreach (var personData in gameXmlData.PersonAttacksDatas.PersonDatas)
			{
				if (personData.PersonType == PersonType.None)
					continue;

				result[personData.PersonType] = personData.CheckUnlockRule(person.StatContainer);
			}

			SendRpc(person.ClientRpc.CreatePersonTypesResponse, result);
		}

		[RPC]
		public void TutorialRequest(InstancePerson person, bool answer, string tutorialNickName, string tutorialEnemyNickName)
		{
			ILogger.Instance.Send($"TutorialRequest loginId={person.loginId} answer={answer}");

			person.Dialogs.Add(answer ? "TutorialRequestYes" : "TutorialRequestNo");
			person.ScenarioStrings.Add("TutorialNickName", tutorialNickName);
			person.ScenarioStrings.Add("TutorialEnemyNickName", tutorialEnemyNickName);
		}

		[RPC]
		public void BreakTutorialRequest(InstancePerson person)
		{
			ILogger.Instance.Send($"BreakTutorialRequest loginId={person.loginId} state={person.TutorialState}");

			person.ScenarioStrings.Add("SkipTutorial", "true");
		}

		[RPC]
		public void CreatePersonRequest(PersonType type, FractionType fraction, InstancePerson instPerson, int slotId)
		{
			var login = instPerson.login;

			if (login.CheckSlotIsBusy(slotId))
			{
				SendRpc(login.ClientRpc.CreatePersonResponse, false, new LobbyPersonData());
				ILogger.Instance.Send($"CreatePersonRequest: SlotId = {slotId} is present or out of range", ErrorLevel.warning);
				return;
			}

			if (type <= PersonType.None || type >= PersonType.Count || fraction <= FractionType.None ||
			    fraction >= FractionType.Count)
			{
				SendRpc(login.ClientRpc.CreatePersonResponse, false, new LobbyPersonData());
				ILogger.Instance.Send("CreatePersonRequest: type = " + type + " fraction = " + fraction, ErrorLevel.warning);
				return;
			}

			var personData = gameXmlData.PersonAttacksDatas.GetPersonData(type);
			if (!personData.CheckUnlockRule(instPerson.StatContainer))
			{
				SendRpc(login.ClientRpc.CreatePersonResponse, false, new LobbyPersonData());
				ILogger.Instance.Send("CreatePersonRequest: !CheckUnlockRule type = " + type, ErrorLevel.warning);
				return;
			}

			var newPerson = new InstancePersonSqlData(type, fraction, slotId);

			login.CreatePerson(newPerson, true, null);
		}

		[RPC]
		public void ChangePersonRequest(int personId, bool SendPersonInfo, InstancePerson instPerson)
		{
			var login = instPerson.login;

			var person = login.InstancePersons.Find(x => x.personId == personId);
			if (person == null || person.personType == PersonType.None)
			{
				ILogger.Instance.Send("ChangePersonRequest person == null or None", ErrorLevel.error);
				return;
			}

			if (Map.Edge.Type != LocationType.Camp)
			{
				ILogger.Instance.Send("ChangePersonRequest Попытка поменять персонажа не в лобби", ErrorLevel.warning);
				return;
			}

			InstanceGameManager.Instance.UpdatePerson(person);
			SendRpc(login.ClientRpc.ChangePersonResponse, personId);

			if (instPerson.personId != personId)
			{
                login.SelectPerson(personId);
                ServerRpcListener.Instance.onPlayerConnected(person, login.player, true);
            }
        }

		[RPC]
		public void RemovePersonRequest(int personId, InstancePerson instPerson)
		{
			var person = instPerson.login.InstancePersons.Find(x => x.personId == personId);
			if (person == null)
				return;

			instPerson.login.RemovePerson(person);
		}

		[RPC]
		public void GetLobbyBuildAllDataRequest(InstancePerson person)
		{
			var lobbyBuildingStorageData = new LobbyBuildingStorageData(person, shopManager, gameXmlData.PlayerCityData);
			SendRpc(person.ClientRpc.GetLobbyBuildAllDataResponse, lobbyBuildingStorageData);
		}

		[RPC]
		public void BuyBuildingRequest(InstancePerson person, int buildingId)
		{
			var login = person.login;

			var buildingXmlData = person.gameXmlData.PlayerCityData.Buildings.Find(x => x.Id == buildingId);
			if (buildingXmlData == null)
			{
				ILogger.Instance.Send("BuyBuildingRequest: building == null", ErrorLevel.error);
				return;
			}

			var loginBuilding = login.Buildings.Find(x => x.Name == buildingXmlData.Name);
			if (loginBuilding == null || loginBuilding.State != BuildingState.None)
			{
				ILogger.Instance.Send("BuyBuildingRequest: loginBuilding == null or not None", ErrorLevel.error);
				return;
			}

			if (!buildingXmlData.UnlockRules.CanUnlock(person))
			{
				ILogger.Instance.Send("BuyBuildingRequest: !UnlockRules", ErrorLevel.error);
				return;
			}

			var tradeInfo = buildingXmlData.TradeInfos.GetCurrentTradeOffer(TimeProvider.UTCNow, person);
			if (!string.IsNullOrEmpty(tradeInfo.Sku))
			{
				ILogger.Instance.Send("BuyBuildingRequest: Нельзя купить, только через InApp", ErrorLevel.error);
				return;
			}

            var analyticInfo = new AnalyticCurrencyInfo()
            {
                needSendInWallet = false,
                sendGain = false,
                Amount = tradeInfo.Cost,
                Destination = SinkType.Building.ToString(),
                DestinationType = buildingXmlData.GetName(),
                Source = SourceGain.Craft,
                SourceType = buildingXmlData.GetName(),
                Currency = tradeInfo.BuyCostType,
                USDCost = 0f,
                PersonLevel = person.login.GetMaxPersonLevel(),
				TestPurchase = person.login.settings.testPurchase
			};


            shopManager.Buy(person, tradeInfo.Cost, tradeInfo.BuyCostType, AccountChangeReason.BuyShopItem, analyticInfo,
                () => {
                    var item = gameXmlData.ItemDataRepository.GetCopyInventoryItemData(buildingXmlData.Name, 1);
                    person.AddItem(item, analyticInfo);
                    AnalyticsManager.Instance.SendData(person, analyticInfo);
                }
			);
		}

		[RPC]
		public void SetBuildingRequest(InstancePerson person, int buildingId)
		{
			var login = person.login;

			var building = person.gameXmlData.PlayerCityData.Buildings.Find(x => x.Id == buildingId);
			if (building == null)
			{
				ILogger.Instance.Send("SetBuildingSetRequest: building == null", ErrorLevel.error);
				return;
			}

			var loginBuilding = login.Buildings.Find(x => x.Name == building.Name);
			if (loginBuilding == null || loginBuilding.State == BuildingState.None)
			{
				ILogger.Instance.Send("SetBuildingSetRequest: loginBuilding == null or None", ErrorLevel.error);
				return;
			}

			login.SelectBuilding(building, out _);

			var data = new LobbyBuildingAccountData(login, building);
			SendRpc(login.ClientRpc.SetBuildingResponse, data);
		}

		[RPC]
		public void RemoveBuildingRequest(InstancePerson person, int buildingId)
		{
			var login = person.login;

			var playerCityData = person.gameXmlData.PlayerCityData;
			var building = playerCityData.Buildings.Find(x => x.Id == buildingId);
			if (building == null)
			{
				ILogger.Instance.Send("RemoveBuildingRequest: building == null", ErrorLevel.error);
				return;
			}

			if (!login.Buildings.Exists(x => x.Name == building.Name))
			{
				ILogger.Instance.Send("RemoveBuildingRequest: !login.Buildings.Exists", ErrorLevel.error);
				return;
			}

			login.UnSelectBuilding(building, out var selected);
			SendRpc(login.ClientRpc.RemoveBuildingResponse, new LobbyBuildingAccountData(login, building));

			if (selected != null)
				SendRpc(login.ClientRpc.SetBuildingResponse, new LobbyBuildingAccountData(login, selected));
		}

        [RPC]
		public void BuyBuildingKitRequest(InstancePerson person, int buildingKitId)
		{
			var login = person.login;

			var buildingKit = person.gameXmlData.PlayerCityData.BuildingKits.Find(x => x.Id == buildingKitId);
			if (buildingKit == null)
			{
				ILogger.Instance.Send("BuyBuildingKitRequest: buildingKit == null", ErrorLevel.error);
				return;
			}

			if (login.BuildingKits.Contains(buildingKit.Name))
			{
				ILogger.Instance.Send("BuyBuildingKitRequest: login.BuildingKits.Contains", ErrorLevel.error);
				return;
			}

			//Можно ли купить здание из набора
			bool canBuyBuilding = buildingKit.Buildings.Exists(building =>
			{
				var loginBuilding = login.Buildings.Find(x => x.Name == building.Name);
				return loginBuilding != null && loginBuilding.State == BuildingState.None;
			});
			if (!canBuyBuilding)
			{
				ILogger.Instance.Send("BuyBuildingKitRequest: !canBuyBuilding", ErrorLevel.error);
				return;
			}

			if (!buildingKit.UnlockRules.CanUnlock(person))
			{
				ILogger.Instance.Send("BuyBuildingKitRequest: !UnlockRules", ErrorLevel.error);
				return;
			}

			var tradeInfo = buildingKit.TradeInfos.GetCurrentTradeOffer(TimeProvider.UTCNow, person);
			if (!string.IsNullOrEmpty(tradeInfo.Sku))
			{
				ILogger.Instance.Send("BuyBuildingKitRequest: Нельзя купить, только через InApp", ErrorLevel.error);
				return;
			}

            var analyticInfo = new AnalyticCurrencyInfo()
            {
                needSendInWallet = false,
                sendGain = false,
                Amount = tradeInfo.Cost,
                Destination = SinkType.BuildingKit.ToString(),
                DestinationType = buildingKit.GetName(),
                Source = SourceGain.Craft,
                SourceType = buildingKit.GetName(),
                Currency =  tradeInfo.BuyCostType,
                USDCost = 0f,
				PersonLevel = person.login.GetMaxPersonLevel(),
				TestPurchase = person.login.settings.testPurchase
			};

			shopManager.Buy(person, tradeInfo.Cost, tradeInfo.BuyCostType, AccountChangeReason.BuyShopItem, analyticInfo,
                () => {
					var item = gameXmlData.ItemDataRepository.GetCopyInventoryItemData(buildingKit.Name, 1);
                    person.AddItem(item, analyticInfo);
                    AnalyticsManager.Instance.SendData(person, analyticInfo);
                }
			);
		}

		[RPC]
		public void SetBuildingKitRequest(InstancePerson person, int buildingKitId)
		{
			var login = person.login;

			var buildingKit = person.gameXmlData.PlayerCityData.BuildingKits.Find(x => x.Id == buildingKitId);
			if (buildingKit == null)
			{
				ILogger.Instance.Send("SetBuildingKitRequest: buildingKit == null", ErrorLevel.error);
				return;
			}

			var data = new List<LobbyBuildingAccountData>();
			foreach (var building in buildingKit.Buildings)
			{
				var loginBuilding = login.Buildings.Find(x => x.Name == building.Name);
				if (loginBuilding == null || loginBuilding.State == BuildingState.None)
					continue;

				login.SelectBuilding(building, out _);

				data.Add(new LobbyBuildingAccountData(login, building));
			}

			SendRpc(login.ClientRpc.SetBuildingKitResponse, data);
		}

        [RPC]
        public void RemoveBuildingKitRequest(InstancePerson person, int buildingKitId)
        {
            var login = person.login;

            var buildingKit = person.gameXmlData.PlayerCityData.BuildingKits.Find(x => x.Id == buildingKitId);
            if (buildingKit == null)
            {
                ILogger.Instance.Send("BuyBuildingKitRequest: buildingKit == null", ErrorLevel.error);
                return;
            }


            var data = new List<LobbyBuildingAccountData>();
            foreach (var building in buildingKit.Buildings)
            {
                var loginBuilding = login.Buildings.Find(x => x.Name == building.Name);
                if (loginBuilding == null)
                    continue;

                login.UnSelectBuilding(building, out var selected);
                data.Add(new LobbyBuildingAccountData(login, building));

                if (selected != null)
                    data.Add(new LobbyBuildingAccountData(login, selected));
            }

            SendRpc(login.ClientRpc.RemoveBuildingKitResponse, data);
        }

		[RPC]
		public void ChangeAvatarNameRequest(InstancePerson person, string avatarName)
		{
			var login = person.login;

			bool success = login.SetAvatar(avatarName);
            ILogger.Instance.Send($"ChangeAvatarNameRequest()... avatarName = {avatarName}, success = {success}, loginId = {person.loginId}");
			SendRpc(login.ClientRpc.ChangeAvatarNameResponse, success, login.AvatarName);

			if (success)
			{
				ServerRpcListener.Instance.SendLoginDataToRatingServer(person);
				GlobalEvents.Instance.OnLoginAvatar(login);
			}
		}

	    public void DeleteLogin(InstancePerson person)
	    {
	        var id = person.loginId;
            SendRpc(dispatcher.OnLoginRemove, id);
        }
    }
}
