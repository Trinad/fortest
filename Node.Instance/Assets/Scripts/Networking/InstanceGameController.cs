﻿using System.Collections.Generic;
using System.Threading;
using Assets.Scripts.InstanceServer;
using Fragoria.Common.Utils;
using uLink;
using UtilsLib.low_engine_sharp.transport;
using UtilsLib.NetworkingData;
using UtilsLib.Protocol;
using Zenject;

namespace Assets.Scripts.Networking
{
    public class InstanceGameController : BaseRpcListener,
		IInstanceSysDispatcher_GameController<NetworkP2PMessageInfo>,
		IInstanceSysLobby_InstanceGameController<NetworkP2PMessageInfo>
    {
        InstanceGameManager m_instanceGameManager;

		public InstanceGameController(NetworkP2P networkP2P, uLink.Network network, uLink.INetworkView view) : base(networkP2P, network, view)
		{
		}

		[Inject]
        public void Constructor(InstanceGameManager instanceGameManager)
        {
            m_instanceGameManager = instanceGameManager;
        }

        //public void uLink_OnPlayerConnected(NetworkPlayer player)
        //{
        //    m_instanceGameManager.uLink_OnPlayerConnected(player);
        //}

        public void uLink_OnPlayerApproval(NetworkPlayerApproval approval)
        {
            m_instanceGameManager.uLink_OnPlayerApproval(approval);
        }

		[RPC]
		public void Translate(TranslateData trData, NetworkP2PMessageInfo info)
		{
			var buffer = ByteArray.StrToByteArray(trData.data);
			var command = new CommandIn {buffer = buffer, size = buffer.Length, position = -1};
			m_networkP2P.OnRPC(trData.sSysCommand, command, info);
		}

        [RPC]
        public void InitLoginList(int gameId, string levelParam, List<GuildData> gdList, string instData, NetworkP2PMessageInfo info)
        {
            ILogger.Instance.Send("InitLoginList gameId = " + gameId + " LevelName = " + ServerStartGameManager.levelName + 
                " LevelParam = " + levelParam + " instData = " + (string.IsNullOrEmpty(instData) ? "empty" : instData));

            m_instanceGameManager.InitMap(gameId, levelParam, instData);

            if (gdList != null)
            {
                for (int i = 0; i < gdList.Count; i++)
                {
                    Guild.SetGuild(gdList[i]);
                    ILogger.Instance.Send("InitLoginList guild = " + gdList[i].ClanName + " GuildId = " + gdList[i].ClanId, ErrorLevel.trace);
                }
            }
        }

        [RPC]
        public void AddPersonToGame(int gameId, InstancePersonData personData, List<GuildData> gdList, bool rewrited, NetworkP2PMessageInfo info)
        {
            m_instanceGameManager.AddPersonToGame(gameId, personData, gdList, rewrited);
        }

        [RPC]
        public void UpdateLogin(int gameId, InstancePersonData personData, List<GuildData> gdList, bool rewrited, NetworkP2PMessageInfo info)
        {
            m_instanceGameManager.UpdateLogin(gameId, personData, gdList, rewrited);
        }

        [RPC]
        public void StopPersonalMapResponse(int gameId, bool stop, NetworkP2PMessageInfo info)
        {
            m_instanceGameManager.StopMap(gameId, stop);
            ILogger.Instance.Send($"StopPersonalMapResponse gameId={gameId} stop={stop}");
        }

        [RPC]
        public void InstanceStopping(NetworkP2PMessageInfo info)
        {
            ILogger.Instance.Send("InstanceStopping");

            m_instanceGameManager.StoppingGame();
        }
    }
}
