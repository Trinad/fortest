﻿using Assets.Scripts.InstanceServer;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib;
using Zenject;
using System.Threading;

namespace Assets.Scripts.Networking
{
	public sealed class DataHashController : BaseInstanceRpcListener
	{
		private string itemsHash;
		private List<InventoryItemData> itemsData;
		private readonly List<InventoryItemData> itemsEmpty = new List<InventoryItemData>();
		private readonly Dictionary<DataHashType, string> Data = new Dictionary<DataHashType, string>();
		private readonly Dictionary<DataHashType, string> Hash = new Dictionary<DataHashType, string>();

		public enum DataHashType
		{
			persons = 0,
			passiveAbility = 1,
			perks = 2,
			creditRating = 3,
			progressLineXMLData = 4,
			bitStreamStringList = 5,
			channels = 6,
			social = 7,
			battles = 8,
		}

		public DataHashController(uLink.NetworkP2P networkP2P, uLink.Network network, uLink.INetworkView view)
			: base(networkP2P, network, view)
		{
		}

		BitStreamStringList bitStreamStringList;
		[Inject]
		public void Construct(PathBuilder pathBuilder, BitStreamStringList bitStreamStringList, ItemDataRepository itemDataRepository)
		{
			this.bitStreamStringList = bitStreamStringList;

			//Захешировать файлы для передачи на клиент
			LoadFile(DataHashType.persons, pathBuilder.PersonXml);
			LoadFile(DataHashType.passiveAbility, pathBuilder.PassiveAbilitysXml);
			LoadFile(DataHashType.perks, pathBuilder.PerksXml);
			LoadFile(DataHashType.creditRating, pathBuilder.CreditRatingXml);
			LoadFile(DataHashType.progressLineXMLData, pathBuilder.ProgressLineXMLData);
            LoadFile(DataHashType.channels, pathBuilder.Chat);
			LoadFile(DataHashType.social, pathBuilder.SocialXml);

			bitStreamStringList.OnBitStreamStringListCreated += OnBitStreamStringListCreated;// так как загружается позже bitStreamStringList.stringListXmlAsString = null

			//Захешировать items для передачи на клиент. Хеш по двум файлам
			var itemDataRepositoryXml = File.ReadAllText(pathBuilder.ItemDataRepositoryDataXml);
			var playerCityXml = File.ReadAllText(pathBuilder.PlayerCityXml);
			itemsData = itemDataRepository.Prototypes.Where(x => x.Final).ToList();
			itemsHash = (itemDataRepositoryXml + playerCityXml).MD5(System.Text.Encoding.UTF8);
		}

		private void LoadFile(DataHashType type, string name)
		{
			LoadString(type, File.ReadAllText(name));
		}

		void OnBitStreamStringListCreated()
		{
			//Захешировать строки в bitStream для передачи на клиент
			LoadString(DataHashType.bitStreamStringList, bitStreamStringList.stringListXmlAsString);
		}

		public void LoadString(DataHashType type, string data)
		{
			Data[type] = data;
			Hash[type] = data.MD5(System.Text.Encoding.UTF8);
		}

		private bool CheckHash(DataHashType type, string clientHash, out string fileData)
		{
			if (!Hash.TryGetValue(type, out var serverHash))
				ILogger.Instance.Send($"GameXmlRpcListener.CheckHash: hash not found {type}", ErrorLevel.error);

			fileData = clientHash == serverHash ? string.Empty : Data[type];
			return clientHash == serverHash;
		}

		[RPC]
		public void GetXmlDataRequest(InstancePerson person, DataHashType type, string clientHash)
		{
			bool isHashEqual = CheckHash(type, clientHash, out var data);
			SendRpc(person.ClientRpc.XmlDataResponse, type, isHashEqual, data);
		}

		#region TODO_deg заменить одной RPC GetXmlDataRequest, смотри выше

		[RPC]
		public void GetAbilityXmlDataRequest(InstancePerson person, string clientHash)
		{
			bool isHashEqual = CheckHash(DataHashType.persons, clientHash, out var data);
			SendRpc(person.ClientRpc.AbilityXmlDataResponse, isHashEqual, data);
		}

		[RPC]
		public void GetTalentsXmlDataRequest(InstancePerson person, string clientHash)
		{
			bool isHashEqual = CheckHash(DataHashType.passiveAbility, clientHash, out var data);
			SendRpc(person.ClientRpc.TalentsXmlDataResponse, isHashEqual, data);
		}

		[RPC]
		public void GetPerksXmlDataRequest(InstancePerson person, string clientHash)
		{
			bool isHashEqual = CheckHash(DataHashType.perks, clientHash, out var data);
			SendRpc(person.ClientRpc.PerksXmlDataResponse, isHashEqual, data);
		}

		[RPC]
		public void GetCreditRatingXmlDataRequest(InstancePerson person, string clientHash)
		{
			bool isHashEqual = CheckHash(DataHashType.creditRating, clientHash, out var data);
			SendRpc(person.ClientRpc.CreditRatingXmlDataResponse, isHashEqual, data);
		}

		[RPC]
		public void ProgressLineXmlDataRequest(InstancePerson person, string clientHash)
		{
			bool isHashEqual = CheckHash(DataHashType.progressLineXMLData, clientHash, out var data);
			SendRpc(person.ClientRpc.ProgressLineXmlDataResponse, isHashEqual, data);
		}

		[RPC]
		public void SocialXmlDataRequest(InstancePerson person, string clientHash)
		{
			bool isHashEqual = CheckHash(DataHashType.social, clientHash, out var data);
			SendRpc(person.ClientRpc.SocialXmlDataResponse, isHashEqual, data);
		}

		[RPC]
		public void GetStringListXmlDataRequest(InstancePerson person, string clientHash)
		{
			bool isHashEqual = CheckHash(DataHashType.bitStreamStringList, clientHash, out var data);
			SendRpc(person.ClientRpc.StringListXmlDataResponse, isHashEqual, data);
		}

		#endregion

		//Список всех возможных предметов
		[RPC]
		public void ItemsDataStorageRequest(InstancePerson person, string clientHash)
		{
			var items = clientHash == itemsHash ? itemsEmpty : itemsData;

			foreach (var item in items)
				item.TempTradeInfoData = item.GetTradeInfoData(person); //TODO_deg а это ннадо?

			SendRpc(person.ClientRpc.ItemsDataStorageResponse, itemsHash, items);
		}

		[RPC]
		public void GetClientConstantsRequest(InstancePerson person)
		{
			ILogger.Instance.Send($"ClientConstantsResponse player.id = {person.player.id}, loginId = {person.loginId}");
			SendRpc(person.ClientRpc.ClientConstantsResponse, person.gameXmlData.ClientConstants);
		}
	}
}
