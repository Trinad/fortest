﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Influences;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils.Logger;
using Fragoria.Common.Utils;
using Newtonsoft.Json;
using UtilsLib;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using UtilsLib.xml;

namespace Assets.Scripts.Networking
{
    public class BitStreamStringList
    {
		HashSet<string> allXmlStrings;
		AddinManager addinManager;
		GameXmlData gameXmlData;

		public Action OnBitStreamStringListCreated;

		public BitStreamStringList(GameXmlData gameXmlData, AddinManager addinManager)
        {
            allXmlStrings = new HashSet<string>();
            allXmlStrings.Add(string.Empty);

			this.gameXmlData = gameXmlData;
			this.addinManager = addinManager;

			allObjects = ReflectionUtils.GetReferences(this.gameXmlData);// надо тут получать ссылки иначе stackOwerflow
			this.addinManager.OnLoaded += OnLoaded;
		}

		List<object> allObjects;
		private void OnLoaded(List<Scenario> scenarios)
		{
			ForAll<StatInfluence>(allObjects, (x) =>
			{
				allXmlStrings.Add(x.TechnicalName);
			});

			ForAll<LocalizationAttribute>(allObjects, (x) =>
			{
				allXmlStrings.Add(x.Key);
			});

			ForAll<InventoryItemData>(allObjects, (x) =>
			{
				allXmlStrings.Add(x.Name);
				allXmlStrings.Add(x.TechnicalName);
				allXmlStrings.Add(x.Icon);
				allXmlStrings.Add(x.Model);
			});

			ForAll<Formula>(allObjects, (x) =>
			{
				allXmlStrings.Add(x.Name);
			});

			ForAll<Building>(allObjects, (x) =>
			{
				allXmlStrings.Add(x.Name);
				allXmlStrings.Add(x.Title);
				allXmlStrings.Add(x.Desc);
				allXmlStrings.Add(x.Prefab);
				allXmlStrings.Add(x.Icon);
			});

			ForAll<Category>(allObjects, (x) =>
			{
				allXmlStrings.Add(x.Title);
				allXmlStrings.Add(x.Icon);
			});


			ForAll<BuildingKit>(allObjects, (x) =>
			{
				allXmlStrings.Add(x.Name);
				allXmlStrings.Add(x.Title);
				allXmlStrings.Add(x.Icon);
			});

			ForAll<BuildingStat>(allObjects, (x) =>
			{
				allXmlStrings.Add(x.Format);
			});

			ForAll<SpecialOfferGui>(allObjects, (x) =>
			{
				allXmlStrings.Add(x.PreviewIcon);
				allXmlStrings.Add(x.BackgroundImage);
				allXmlStrings.Add(x.ContentImage);
			});

			ForAll<EntityPartInfo>(allObjects, (x) =>
			{
				allXmlStrings.Add(x.Model);
			});

			ForAll<EdgeLocationXmlData>(allObjects, (x) =>
			{
				allXmlStrings.Add(x.TechnicalName);
				allXmlStrings.Add(x.GetJsonName());
			});

			ForAll<MonsterData>(allObjects, (x) =>
			{
				allXmlStrings.Add(x.TechnicalName);
			});

			var allScenarioObjects = ReflectionUtils.GetReferences(scenarios);

			ForAll<SpecialEventScenarioData>(allScenarioObjects, (x) =>
			{
				allXmlStrings.Add(x.Text);
				allXmlStrings.Add(x.FormBackgroundImage);
				allXmlStrings.Add(x.SpecialEventBackgroundImage);
				allXmlStrings.Add(x.SpecialEventOverlayImage);
				allXmlStrings.Add(x.ProgressBarImage);
				allXmlStrings.Add(x.ProgressBarGradientImage);
				allXmlStrings.Add(x.ProgressBarHeadImage);
				allXmlStrings.Add(x.CollectedItemImage);
				allXmlStrings.Add(x.CollectedItemsBgImage);

				allXmlStrings.Add(x.LobbyIconImage);
				allXmlStrings.Add(x.ShowItemsButtonImage);

				foreach (var im in x.DecorImages)
				{
					allXmlStrings.Add(im);
				}

				allXmlStrings.Add(x.ShowItemsButtonText);
				allXmlStrings.Add(x.FirstStepHint);
				allXmlStrings.Add(x.SecondStepHint);
				allXmlStrings.Add(x.ThirdStepHint);

				allXmlStrings.Add(x.TopColor);
				allXmlStrings.Add(x.BottomColor);

				allXmlStrings.Add(x.BackButtonColor);
				allXmlStrings.Add(x.ProgressNextColor);
				allXmlStrings.Add(x.ProgressCurrentColor);
				allXmlStrings.Add(x.ProgressPrevColor);

				allXmlStrings.Add(x.ResourceIcon);

				allXmlStrings.Add(x.CompensationHint);

				allXmlStrings.Add(x.NotEnoughTokenHint);
			});

			foreach (var pt in Enum.GetNames(typeof(PersonType)))
			{
				allXmlStrings.Add(pt.ToLower());
			}

			allXmlStrings.RemoveWhere(s => s == null);
			string[] allStrings = new List<string>(allXmlStrings).ToArray();
			stringListXmlAsString = XmlHelper.SerializeToSimpleXml(allStrings);

			//ILogger.Instance.Send("BitStreamStringList \n" + JsonConvert.SerializeObject(allXmlStrings, Formatting.Indented), ErrorLevel.error);

			//stringListXmlAsHash = stringListXmlAsString.MD5(System.Text.Encoding.UTF8);
			BitStreamInstanceExtensions.SetProtocolStrings(allStrings);

			OnBitStreamStringListCreated?.Invoke();
		}

		public void ForAll<T>(List<object> allObjects, Action<T> action)
        {
            var list = allObjects.OfType<T>();

            foreach (var data in list)
            {
                action(data);
            }
        }

        // public readonly string[] allStrings;
        public string stringListXmlAsString = null;
		//public string stringListXmlAsHash = null;// в DataHashController хэш получаем
	}
}
