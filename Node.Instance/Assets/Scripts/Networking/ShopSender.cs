﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using Newtonsoft.Json;
using uLink;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.Networking
{
    public class ShopSender : BaseInstanceRpcListener, IShopSender
    {
		public ShopSender(NetworkP2P networkP2P, uLink.Network network, uLink.INetworkView view) : base(networkP2P, network, view)
		{
		}

		public void GetShopItemsResponse(InstancePerson instancePerson, bool sale, List<ShopItemData> shopItemDatas)
        {
            SendRpc(instancePerson.ClientRpc.GetShopItemsResponse, sale, shopItemDatas);
        }

        public void GetProductItemsResponse(InstancePerson instancePerson, ShopData data)
        {
            SendRpc(instancePerson.ClientRpc.GetProductItemsResponse, data);
        }

        public void SendBuyBuildingResponse(InstancePerson instancePerson, LobbyBuildingAccountData data)
        {
            SendRpc(instancePerson.ClientRpc.BuyBuildingResponse, data);
        }

        public void ErrorDataResponse(InstancePerson instancePerson, ErrorData getError)
        {
            SendRpc(instancePerson.ClientRpc.ErrorDataResponse, getError);
        }

        public void LoginAccountChangeRequest(AccountChangeParams data)
        {
            SendRpc(dispatcher.OnLoginAccountChange, data);
        }

        public void SendBuyItemsResponse(InstancePerson instancePerson, BuyItemResponseType responceType, List<InventoryItemData> rewardItems, 
	        int actionType = -1, int vipPoints = 0, int level = 0, string titleId = "")
        {
            SendRpc(instancePerson.ClientRpc.BuyItemsResponse, responceType, rewardItems, actionType, vipPoints, level, titleId);
        }

        public void ProductListResponse(InstancePerson instancePerson, Dictionary<string, int> skuData)
        {
            SendRpc(instancePerson.ClientRpc.ProductListResponse, skuData);
        }
    }

    public interface IShopSender
    {
        void GetShopItemsResponse(InstancePerson instancePerson, bool sale, List<ShopItemData> shopItemDatas);
        void GetProductItemsResponse(InstancePerson instancePerson, ShopData data);
        void SendBuyBuildingResponse(InstancePerson instancePerson, LobbyBuildingAccountData data);
        void ErrorDataResponse(InstancePerson instancePerson, ErrorData getError);
        void LoginAccountChangeRequest(AccountChangeParams data);
        void SendBuyItemsResponse(InstancePerson instancePerson, BuyItemResponseType responceType, List<InventoryItemData> rewardItems, int actionType = -1, int vipPoints = 0, int level = 0, string titleId = "");
        void ProductListResponse(InstancePerson instancePerson, Dictionary<string, int> skuData);
    }
}
