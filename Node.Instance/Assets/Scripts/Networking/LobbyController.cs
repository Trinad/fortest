﻿using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.Networking.ClientDataRPC;
using uLink;
using UtilsLib.Logic.Enums;
using UtilsLib.Protocol;
using Zenject;

namespace Assets.Scripts.Networking
{
	public sealed class LobbyController : BaseInstanceRpcListener,
		IInstanceSysDispatcher_LobbyController<NetworkP2PMessageInfo>,
		IInstanceSysLobby_LobbyController<NetworkP2PMessageInfo>
	{
		private IPersonsOnServer m_personsOnServer;
		private ServerRpcListener m_serverRpcs;
		private uLinkManager uLinkManager;

        public LobbyController(NetworkP2P networkP2P, uLink.Network network, uLink.INetworkView view) 
			: base(networkP2P, network, view)
		{
		}

		[Inject]
		public void Construct(IPersonsOnServer personsOnServer, ServerRpcListener serverRpcListener, uLinkManager _uLinkManager)
		{
            m_personsOnServer = personsOnServer;
			m_serverRpcs = serverRpcListener;
			uLinkManager = _uLinkManager;

			uLinkManager.p2pFromNewLobby.RegisterListener(this);
		}

		[RPC]
		public void TestPeerConnectRPC(string text, NetworkP2PMessageInfo info)
		{
			ILogger.Instance.Send("TestPeerConnectRPC " + text + " from " + info.sender);
			SetLobby(info.sender);

			SendRpc(lobby.PeerAuth, ServerStartGameManager.ticket, ServerStartGameManager.levelName, Map.Edge.GetJsonName(), uLinkManager.ListenPort, true);
		}

		private void uLink_OnPeerConnected(NetworkPeer peer)
		{
			int port = uLinkManager.ListenPort + AppConfig.Instance.ShiftPortForConnectFromLobby;
			//ILogger.Instance.Send("uLink_OnPeerConnected found port = " + port + " peer.ipAddress = " + peer.ipAddress + " peer.port = " + peer.port + " lobbyIp = " + lobby.ipAddress + " lobby port = " + lobby.port);

			if (lobbyPeer.Equals(NetworkPeer.unassigned) || peer.port == port) //local can't connect to yourself
				return;

			SetLobby(peer);

			uLinkManager.p2pConn = null;//Забудем родительский лобби

			if (m_networkP2P.GetStatus(peer) == NetworkStatus.Disconnected)
				m_networkP2P.Connect(peer, AppConfig.Instance.PasswordForP2P);

			SendRpc(lobby.PeerAuth, ServerStartGameManager.ticket, ServerStartGameManager.levelName, Map.Edge.GetJsonName(), uLinkManager.ListenPort, false);
		}

		// приходит с лобби, если инстанс, на который хотел перейти указанный логин, находится в очереди на запуск
		[RPC]
		public void InstanceInfoResponseQueue(int loginId, int currentQueuePosition, int totalQueueLength, NetworkP2PMessageInfo info)
		{
			ILogger.Instance.Send($"Receive InstanceInfoResponseQueue: loginId {loginId}, pos {currentQueuePosition}/" +
								  $"{totalQueueLength}");

			var instancePerson = m_personsOnServer.getPersonByLoginId(loginId);
			if (instancePerson == null)
			{
				ILogger.Instance.Send("InstanceInfoResponseQueue instancePerson == null LoginId: " + loginId, ErrorLevel.error);
				return;
			}

			SendLoadLevelResponseError(instancePerson, ClientErrors.INSTANCE_IN_QUEUE);
		}

		public void SendLoadLevelResponseError(InstancePerson instancePerson, ClientErrors error)
		{
			SendRpc(instancePerson.ClientRpc.LoadLevelResponse, error, string.Empty, string.Empty, 0, string.Empty, PlayerServerStatus.IDLE);
		}
	}
}
