﻿using System;
using LobbyInstanceLib.Networking;
using NSubstitute;
using uLink;
using UtilsLib.low_engine_sharp.transport;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using UtilsLib.Protocol;

namespace Assets.Scripts.Networking
{
	public class BaseInstanceRpcListener : BaseRpcListener
	{
		protected static NetworkPeer lobbyPeer;
		protected static ILobbySysInstance<Connection, Connection> lobby = Substitute.For<ILobbySysInstance<Connection, Connection>>();
		protected static IAuthSysInstance auth = Substitute.For<IAuthSysInstance>();
		protected static IDispatcherSysInstance dispatcher = Substitute.For<IDispatcherSysInstance>();
		protected static IGuildSysInstance guild = Substitute.For<IGuildSysInstance>();
		protected static IRatingSysInstance rating = Substitute.For<IRatingSysInstance>();
        protected static IChatSysInstance chat = Substitute.For<IChatSysInstance>();

        public BaseInstanceRpcListener(NetworkP2P networkP2P, uLink.Network network, INetworkView view) 
			: base(networkP2P, network, view)
		{
		
		}

		public void SetLobby(NetworkPeer sender)
		{
			lobbyPeer = sender;
			lobby.FillRPCByMethod((view, rpc, param) => SendRpc(rpc.sSysCommand, sender, param)); //Отправляем сообщение напрямую
			auth.FillRPCByMethod(Translate(NodeType.Auth));
			dispatcher.FillRPCByMethod(Translate(NodeType.Dispatcher));
			guild.FillRPCByMethod(Translate(NodeType.Guild));
			rating.FillRPCByMethod(Translate(NodeType.Rating));
            chat.FillRPCByMethod(Translate(NodeType.Chat));
        }

		Action<uLink.INetworkView, NetworkTranslateRPC, object[]> Translate(NodeType nodeType)
		{
			return (view, rpc, param) =>
			{
				//Отправляем сообщение через транслейт
				var data = TranslateData.PackData(param);
				if (rpc != null)
					SendRpc(lobby.Translate, new TranslateData
					{
						nodeType = nodeType,
						sSysCommand = rpc.sSysCommand,
						data = data
					});
			};
		}
	}
}
