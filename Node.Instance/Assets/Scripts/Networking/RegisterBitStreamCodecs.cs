﻿using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.Login.DataRPC;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.Perks;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.InstanceServer.Entries.XmlData.Trade;
using Assets.Scripts.InstanceServer.Groups;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Networking.ClientDataRPC.Arenas;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Common;
using Assets.Scripts.Utils.Localization;
using LobbyInstanceLib.Networking.Common;
using LobbyInstanceLib.Networking.DataRPC;
using Node.Instance.Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.DegSerializers;
using UtilsLib.Logic;
using UtilsLib.NetworkingData;
using UtilsLib.NetworkingData.Chat;
using UtilsLib.NetworkingData.Party;

namespace Assets.Scripts.Networking
{
	public class RegisterBitStreamCodecs
	{
		private readonly PersonsOnServer personsOnServer;

		public RegisterBitStreamCodecs(PersonsOnServer _personsOnServer)
		{
			personsOnServer = _personsOnServer;

            uLink.BitStreamCodec.Add<InstancePerson>(InstancePersonDeSerialize, null);
			DegSerializer_CommandIn.SetGetterParam<InstancePerson>(null);

			#region BitStreamCodec
			uLink.BitStreamCodec.Add<LobbyInfo>(LobbyInfo.DeSerialize, LobbyInfo.Serialize);
			uLink.BitStreamCodec.Add<LobbyPersonData>(null, LobbyPersonData.Serialize);
			uLink.BitStreamCodec.Add<AbilityStatus>(null, AbilityStatus.Serialize);

			uLink.BitStreamCodec.Add<LobbyBuildingStorageData>(LobbyBuildingStorageData.DeSerialize, LobbyBuildingStorageData.Serialize);
			uLink.BitStreamCodec.Add<LobbyBuildingAccountData>(LobbyBuildingAccountData.DeSerialize, LobbyBuildingAccountData.Serialize);
			uLink.BitStreamCodec.Add<LobbyBuildingData>(LobbyBuildingData.DeSerialize, LobbyBuildingData.Serialize);
			uLink.BitStreamCodec.Add<LobbyBuildingCategoryData>(LobbyBuildingCategoryData.DeSerialize, LobbyBuildingCategoryData.Serialize);
			uLink.BitStreamCodec.Add<LobbyBuildingSubCategoryData>(LobbyBuildingSubCategoryData.DeSerialize, LobbyBuildingSubCategoryData.Serialize);
			uLink.BitStreamCodec.Add<LobbyBuildingSetData>(LobbyBuildingSetData.DeSerialize, LobbyBuildingSetData.Serialize);
			uLink.BitStreamCodec.Add<BuildingStatData>(null, BuildingStatData.Serialize);

			uLink.BitStreamCodec.Add<TalentItem>(null, TalentItem.Serialize);

			BitStreamHelper.AddDegSerializer<InstancePersonData>();
			BitStreamHelper.AddDegSerializer<MailData>();
			BitStreamHelper.AddDegSerializer<RatingData>();
			BitStreamHelper.AddDegSerializer<RewardRpcData>();
			BitStreamHelper.AddDegSerializer<ProgressData>();
			uLink.BitStreamCodec.Add<CageStoneData>(CageStoneData.DeSerialize, CageStoneData.Serialize);
			uLink.BitStreamCodec.Add<CageWallData>(CageWallData.DeSerialize, CageWallData.Serialize);

            uLink.BitStreamCodec.Add<MessageContainer>(MessageContainerSerialize.DeSerialize, MessageContainerSerialize.Serialize);
            uLink.BitStreamCodec.Add<ChannelInfo>(ChannelInfoSerialize.DeSerialize, ChannelInfoSerialize.Serialize);
            uLink.BitStreamCodec.Add<ChannelsData>(ChannelsDataSerialize.DeSerialize, ChannelsDataSerialize.Serialize);

            uLink.BitStreamCodec.Add<EntityData>(null, EntityData.Serialize);
			uLink.BitStreamCodec.Add<EntityPartInfo>(EntityPartInfoBit.DeSerialize, EntityPartInfoBit.Serialize);
			uLink.BitStreamCodec.Add<ErrorData>(ErrorData.DeSerialize, ErrorData.Serialize);
			uLink.BitStreamCodec.Add<FriendData>(FriendDataSerialize.DeSerialize, FriendDataSerialize.Serialize);
			uLink.BitStreamCodec.Add<FriendItemData>(FriendItemData.DeSerialize, FriendItemData.Serialize);
			uLink.BitStreamCodec.Add<InventoryItemData>(InventoryItemData.DeSerialize, InventoryItemData.Serialize);
			uLink.BitStreamCodec.Add<TradeInfoData>(null, TradeInfoData.Serialize);
			uLink.BitStreamCodec.Add<LocParamData>(null, LocParamData.Serialize);
			//uLink.BitStreamCodec.Add<MailItemData>(null, MailItemData.Serialize);
			uLink.BitStreamCodec.Add<MapInfoData>(MapInfoData.DeSerialize, MapInfoData.Serialize);
			uLink.BitStreamCodec.Add<MapItemData>(null, MapItemData.Serialize);
			uLink.BitStreamCodec.Add<MinimapData>(null, MinimapData.Serialize);
			uLink.BitStreamCodec.Add<StatusData>(null, StatusData.Serialize);
			uLink.BitStreamCodec.Add<LocParameter>(LocParameter.DeSerialize, LocParameter.Serialize);
			uLink.BitStreamCodec.Add<AttributeFrameData>(AttributeFrameData.DeSerialize, AttributeFrameData.Serialize);

			uLink.BitStreamCodec.Add<PlayerData>(null, PlayerData.Serialize);
			uLink.BitStreamCodec.Add<PlayerServerStatusData>(null, PlayerServerStatusData.Serialize);
			uLink.BitStreamCodec.Add<ProductItemData>(ProductItemData.DeSerialize, ProductItemData.Serialize);
			uLink.BitStreamCodec.Add<QuestItemData>(QuestItemData.DeSerialize, QuestItemData.Serialize);
			uLink.BitStreamCodec.Add<QuestTaskData>(QuestTaskData.DeSerialize, QuestTaskData.Serialize);
			uLink.BitStreamCodec.Add<DailyQuestCommonData>(null, DailyQuestCommonData.Serialize);
			uLink.BitStreamCodec.Add<DailyQuestRewardData>(null, DailyQuestRewardData.Serialize);
			uLink.BitStreamCodec.Add<RatingItemData>(RatingItemData.DeSerialize, RatingItemData.Serialize);
			uLink.BitStreamCodec.Add<ResourcesToUpdateItemData>(ResourcesToUpdateItemData.DeSerialize, ResourcesToUpdateItemData.Serialize);
			uLink.BitStreamCodec.Add<ShopItemData>(ShopItemData.DeSerialize, ShopItemData.Serialize);
			uLink.BitStreamCodec.Add<TeleportData>(TeleportData.DeSerialize, TeleportData.Serialize);
			uLink.BitStreamCodec.Add<UpgradeItemData>(UpgradeItemData.DeSerialize, UpgradeItemData.Serialize);

			uLink.BitStreamCodec.Add<GroupPersonData>(GroupPersonData.DeSerialize, GroupPersonData.Serialize);
			BitStreamHelper.AddDegSerializer<Prices>();
			uLink.BitStreamCodec.Add<CollectionData>(CollectionData.DeSerialize, CollectionData.Serialize);
			uLink.BitStreamCodec.Add<DungeonRpcData>(DungeonRpcData.DeSerialize, DungeonRpcData.Serialize);
			uLink.BitStreamCodec.Add<FloorData>(FloorData.DeSerialize, FloorData.Serialize);
			uLink.BitStreamCodec.Add<GuildData>(GuildSerialize.DeSerialize, GuildSerialize.Serialize);
			uLink.BitStreamCodec.Add<GuildPersonInfo>(GuildPersonSerialize.DeSerialize, GuildPersonSerialize.Serialize);
			uLink.BitStreamCodec.Add<GuildJoin>(GuildSerialize.GuildJoinDeserealize, GuildSerialize.GuildJoinSerealize);
			uLink.BitStreamCodec.Add<ClientConstants>(ClientConstants.DeSerialize, ClientConstants.Serialize);
			uLink.BitStreamCodec.Add<ClanEmblemData>(GuildSerialize.ClanEmblemData_DeSerialize, GuildSerialize.ClanEmblemData_Serialize);
			uLink.BitStreamCodec.Add<ShortClanData>(ShortClanDataSerialize.DeSerialize, ShortClanDataSerialize.Serialize);
			uLink.BitStreamCodec.Add<MatchData>(MatchData.DeSerialize, MatchData.Serialize);
			uLink.BitStreamCodec.Add<UnlockData>(UnlockData.DeSerialize, UnlockData.Serialize);
			uLink.BitStreamCodec.Add<CastAbilityData>(null, CastAbilityData.Serialize);
			uLink.BitStreamCodec.Add<CastFxData>(null, CastFxData.Serialize);
			uLink.BitStreamCodec.Add<SelectMapItemData>(null, SelectMapItemData.Serialize);
			uLink.BitStreamCodec.Add<SelectArenaMatchData>(null, SelectArenaMatchData.Serialize);
			uLink.BitStreamCodec.Add<SelectMapData>(null, SelectMapData.Serialize);

			//uLink.BitStreamCodec.Add<SpecialActionSection>(SpecialActionSection.DeSerialize, SpecialActionSection.Serialize);
			uLink.BitStreamCodec.Add<SpecialRpcData>(SpecialRpcData.DeSerialize, SpecialRpcData.Serialize);
			uLink.BitStreamCodec.Add<SpecialRpcItemData>(SpecialRpcItemData.DeSerialize, SpecialRpcItemData.Serialize);
			uLink.BitStreamCodec.Add<SpecialOfferGui>(SpecialOfferGui.DeSerialize, SpecialOfferGui.Serialize);

			uLink.BitStreamCodec.Add<EmotionData>(EmotionData.DeSerialize, EmotionData.Serialize);
			uLink.BitStreamCodec.Add<EmotionPackData>(EmotionPackData.DeSerialize, EmotionPackData.Serialize);
			uLink.BitStreamCodec.Add<ArenaMatchData>(null, ArenaMatchData.Serialize);
			uLink.BitStreamCodec.Add<TeamDeathmatchData>(null, TeamDeathmatchData.Serialize);
			uLink.BitStreamCodec.Add<BrawlMatchData>(null, BrawlMatchData.Serialize);
			uLink.BitStreamCodec.Add<ArenaPlayerInfo>(null, ArenaPlayerInfo.Serialize);
			uLink.BitStreamCodec.Add<WavesResultPlayerInfo>(null, WavesResultPlayerInfo.Serialize);
			uLink.BitStreamCodec.Add<WavesPlayerInfo>(null, WavesPlayerInfo.Serialize);
			uLink.BitStreamCodec.Add<WavesMatchData>(null, WavesMatchData.Serialize);
			uLink.BitStreamCodec.Add<WavesResultMatchData>(null, WavesResultMatchData.Serialize);


			uLink.BitStreamCodec.Add<Vector2i>(Vector2i.DeSerialize, Vector2i.Serialize);
			uLink.BitStreamCodec.Add<ArenaRewardData>(ArenaRewardData.DeSerialize, ArenaRewardData.Serialize);
			uLink.BitStreamCodec.Add<ShopData>(null, ShopData.Serialize);
			uLink.BitStreamCodec.Add<ChangeTransformData>(null, ChangeTransformData.Serialize);
			uLink.BitStreamCodec.Add<ChangeTransformForceData>(null, ChangeTransformForceData.Serialize);
			uLink.BitStreamCodec.Add<ChangeTransformDataList<ChangeTransformData>>(null, ChangeTransformDataList<ChangeTransformData>.Serialize);
			uLink.BitStreamCodec.Add<ChangeTransformDataList<ChangeTransformForceData>>(null, ChangeTransformDataList<ChangeTransformForceData>.Serialize);
			uLink.BitStreamCodec.Add<LargeMapMonsterData>(null, LargeMapMonsterData.Serialize);
			uLink.BitStreamCodec.Add<ResourceCount>(ResourceCount.DeSerialize, ResourceCount.Serialize);
			uLink.BitStreamCodec.Add<UpgradeShipData>(null, UpgradeShipData.Serialize);
			uLink.BitStreamCodec.Add<ShiningPathPoint>(null, ShiningPathPoint.Serialize);
			uLink.BitStreamCodec.Add<Vector2DirectonPacked>(Vector2DirectonPacked.DeSerialize, Vector2DirectonPacked.Serialize);
			uLink.BitStreamCodec.Add<Vector2PositionPacked>(Vector2PositionPacked.DeSerialize, Vector2PositionPacked.Serialize);
			uLink.BitStreamCodec.Add<Vector3PositionPacked>(Vector3PositionPacked.DeSerialize, Vector3PositionPacked.Serialize);
			uLink.BitStreamCodec.Add<FloatPacked>(FloatPacked.DeSerialize, FloatPacked.Serialize);
			uLink.BitStreamCodec.Add<FloatUnsignedPacked>(FloatUnsignedPacked.DeSerialize, FloatUnsignedPacked.Serialize);
			uLink.BitStreamCodec.Add<GameSettings>(GameSettings.DeSerialize, GameSettings.Serialize);
			uLink.BitStreamCodec.Add<SpecialEventData>(null, SpecialEventData.Serialize);
			uLink.BitStreamCodec.Add<ProgressLineState>(null, ProgressLineState.Serialize);
			uLink.BitStreamCodec.Add<ProgressLineRewardInfo>(null, ProgressLineRewardInfo.Serialize);
			uLink.BitStreamCodec.Add<BonusSourceData>(null, BonusSourceData.Serialize);
			uLink.BitStreamCodec.Add<ChestEntityDescription>(null, ChestEntityDescription.Serialize);

			uLink.BitStreamCodec.Add<UsedPerksData>(null, UsedPerksData.Serialize);
			uLink.BitStreamCodec.Add<PerkKitData>(null, PerkKitData.Serialize);
			uLink.BitStreamCodec.Add<PersonExperienceData>(null, PersonExperienceData.Serialize);
            uLink.BitStreamCodec.Add<BuffString>(BuffString.DeSerialize, BuffString.Serialize);

            uLink.BitStreamCodec.Add<PlayerDeathData>(null, PlayerDeathData.Serialize);

			uLink.BitStreamCodec.Add<MiniTutorial>(null, MiniTutorial.Serialize);

			uLink.BitStreamCodec.Add<PersonCard>(PersonCardSerialize.DeSerialize, PersonCardSerialize.Serialize);
			uLink.BitStreamCodec.Add<PartyInviteData>(PartyInviteDataSerialize.DeSerialize, PartyInviteDataSerialize.Serialize);
			uLink.BitStreamCodec.Add<PartyRequestData>(PartyRequestDataSerialize.DeSerialize, PartyRequestDataSerialize.Serialize);
			uLink.BitStreamCodec.Add<PlayModeData>(PlayModeDataSerializer.DeSerialize, PlayModeDataSerializer.Serialize);
			uLink.BitStreamCodec.Add<PartyData>(PartyDataSerializer.DeSerialize, PartyDataSerializer.Serialize);
			uLink.BitStreamCodec.Add<SelectedPersonData>(SelectedPersonDataSerialize.DeSerialize, SelectedPersonDataSerialize.Serialize);
			uLink.BitStreamCodec.Add<PartyMemberData>(PartyMemberDataSerializer.DeSerialize, PartyMemberDataSerializer.Serialize);


			#endregion
		}
		object InstancePersonDeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			var info = (uLink.NetworkMessageInfo)codecOptions[0];
			var instancePerson = personsOnServer.GetPerson(info.sender);
			if (instancePerson == null)
			{
				var result = (uLink.ReadObjectResult)codecOptions[1];
				result.Success = false;
				result.ErrorMessage = "instancePerson == null " + Helper.InstancePersonFail(info.sender);
			}

			return instancePerson;
		}
	}
}
