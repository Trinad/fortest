﻿using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.ActiveObjects;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Actions;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Games;
using uLink;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using UtilsLib.NetworkingData.Chat;
using UtilsLib.NetworkingData.Party;
using UtilsLib.Protocol;
using UtilsLib.XmlData;
using Zenject;

namespace Assets.Scripts.Networking
{
	public class MatchmakingController : BaseInstanceRpcListener,
		IInstanceSysDispatcher_Matchmaking<NetworkP2PMessageInfo>,
		IInstanceSysGuild_Matchmaking<NetworkP2PMessageInfo>
	{
		public MatchmakingController(NetworkP2P networkP2P, uLink.Network network, INetworkView view) : base(networkP2P, network, view)
		{
		}

		private PersonsOnServer personsOnServer;
		private InstanceGameManager instanceGameManager;
		private DungeonManager dungeonManager;
		private PathBuilder pathBuilder;
		private ArenaAlgorithm arenaAlgorithm;
		private INotifyController notifyController;

		[Inject]
		public void Constructor(PersonsOnServer _personsOnServer, InstanceGameManager _instanceGameManager,
			DungeonManager _dungeonManager, PathBuilder _pathBuilder, ArenaAlgorithm _arenaAlgorithm,
			GlobalEvents globalEvents, INotifyController _notifyController)
		{
			personsOnServer = _personsOnServer;
			instanceGameManager = _instanceGameManager;
			dungeonManager = _dungeonManager;
			pathBuilder = _pathBuilder;
			arenaAlgorithm = _arenaAlgorithm;
			notifyController = _notifyController;

			globalEvents.PlayerConnected += SendArenaMatchRequest;
			globalEvents.PlayerDisconnected += person => ChangeReadyStatusRequest(person, false); //Игрок не готов
			globalEvents.LoginRename += OnUpdatePersonCard;
			globalEvents.LoginAvatar += OnUpdatePersonCard;

			globalEvents.PlayerConnected += SendArenaMatchRequest;

			globalEvents.MapCreate += map =>
			{
				map.PlayerConnected += (person, reconnect, changePerson) =>
				{
					if (changePerson)
						OnUpdateCharacterData(person);
				};

				map.PersonEquipItem += (person, anlyticItemInfo) => OnUpdateCharacterData(person);
			};

			Cheats.RegisterCheat("t", (p, levelName) => TeleportCheat(p, levelName, string.Empty), "телепортироваться на указанную локацию");
			Cheats.RegisterCheat("t", TeleportCheat, "Телепорт на нужную локацию. Список локаций в вики");
			Cheats.RegisterCheat("dmarenatest", (p, levelParams) =>
			{
				TeleportCheat(p, ("dm_arena_test"), levelParams == "0" ? string.Empty : levelParams);
			}, "Два участника разный гильдий пишут dmarenatest 1 и у них будет DM в лагере");
			Cheats.RegisterCheat("dmarena", (p, levelParams) =>
			{
				TeleportCheat(p, ("dm_arena_1"), levelParams == "0" ? string.Empty : levelParams);
			}, "Два участника разный гильдий пишут dmarena 1 и у них начнется DM");
			Cheats.RegisterCheat("ctftest", (p, levelParams) =>
			{
				TeleportCheat(p, ("ctf_5vs5_1"), levelParams == "0" ? string.Empty : levelParams);
			}, "Два участника разный гильдий пишут ctftest 1 и у них начнется CTF на маленькой карте");
			Cheats.RegisterCheat("ctf", (p, levelParams) =>
			{
				TeleportCheat(p, ("ctf_1"), levelParams == "0" ? string.Empty : levelParams);
			}, "Два участника разный гильдий пишут ctf 1 и у них начнется CTF на большой карте");
			Cheats.RegisterCheat("DFinish", (p) =>
			{
				p.syn.map.monsters.ForEach(behaviour => behaviour.SetHealth(p.syn, -behaviour.MaxHealth));
				var CampLevel = p.gameXmlData.Edges.GetCampLevel(p).LevelName;
				CoroutineRunner.Instance.Timer(5, () => TeleportCheat(p, CampLevel, string.Empty));
			}, "Писать в данже. Типа прошел данж и зашел в портал в конце");

			Cheats.RegisterCheat("leavearena", (p) =>
			{
				CoroutineRunner.Instance.Timer(5, () => LeaveFromMatchRequest(p));
			}, "Убежать с арены через 5 сек");
		}

		private void TeleportCheat(InstancePerson p, string levelName, string levelParam)
		{
			var edge = InstanceGameManager.Instance.GetEdgeLocation(levelName);
			var url = pathBuilder.GetZoneUrl(edge.GetJsonName());
			if (!System.IO.File.Exists(url))
			{
				Cheats.SendCheatChatMessage.Invoke("Карта " + levelName + " не найдена!!!", p);
				return;
			}

			var match = InstanceGameManager.Instance.ArenaAlgorithm.GetMatchInfoData().Find(x => x.ArenaGameInfo.Exists(y => y.Map == levelName));

			var action = new LoadLevel
			{
				Name = levelName,
				ArenaMatchType = match?.MatchType ?? ArenaMatchType.TeamDeathmatch,
				IsDeathmatchTeam = true,
				LevelParam = levelParam,
				matchTime = 0.1f,

			};
			action.Construct(this);
			action.Execute(new ContextIAction(null) { actorOwner = p });
		}

		#region ArenaMatch

		[RPC]
		public void ArenaMatchRequest(InstancePerson person)
		{
			SendArenaMatchRequest(person.login.LastArenaButtonId, person.login.currentPlayModeType, person);
		}

		[RPC]
		public void RestartLastArenaRequest(InstancePerson person)
		{
			SendArenaMatchRequest(person.login.LastArenaButtonId, person.login.currentPlayModeType, person);
		}

		private void SendArenaMatchRequest(int matchButtonId, PlayModeType currentPlayModeType, InstancePerson person)
		{
			matchButtonId = arenaAlgorithm.GetActualButtonId(person, matchButtonId);
			person.login.LastArenaButtonId = matchButtonId;

			if (person.bRunTutorial /*&& person.TutorialState == 12*/)
			{
				person.Dialogs.Add("ArenaMatchRequest");
				//return;
			}

			var matchData = InstanceGameManager.Instance.ArenaAlgorithm.GetMatchData(matchButtonId);

			if (!matchData.CheckRule(person.StatContainer))
				return;

			var rule = matchData.GetRule(person.StatContainer, currentPlayModeType);
			if (rule.Type != ArenaUnlockType.Success)
				return;

			var eloTeam = (int)person.StatContainer.GetStatValue(Stats.EloRatingTeam);
			SendRpc(guild.OnArenaMatchRequest,
				person.loginId, person.personId,
				eloTeam, person.rating_elo_coop,
				person.HeroPower, person.Level, person.personType,
				matchButtonId, rule.OnlyBot);
		}

		private void SendArenaMatchRequest(InstancePerson person)
		{
			var eloTeam = (int)person.StatContainer.GetStatValue(Stats.EloRatingTeam);
			SendRpc(guild.OnArenaMatchRequest,
				person.loginId, person.personId,
				eloTeam, person.rating_elo_coop,
				person.HeroPower, person.Level, person.personType, -1, false);
			ILogger.Instance.Send($"SendArenaMatchRequest person.loginId: {person.loginId}, person.personId: {person.personId}");
		}

		[RPC]
		public void ArenaDataResponse(int loginId, int personId, List<int> ArenaMatchButtonIds, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.GetPersonByPersonId(personId);
			ILogger.Instance.Send($"ArenaDataResponse loginId = {loginId}", ErrorLevel.info);
			if (person == null)
			{
				ILogger.Instance.Send($"ArenaDataResponse() Can't find person, loginId = {loginId}", ErrorLevel.info);
				return;
			}

			var matchType = ArenaMatchType.None;
			if (ArenaMatchButtonIds != null && ArenaMatchButtonIds.Count > 0)
			{
				var matchData = InstanceGameManager.Instance.ArenaAlgorithm.GetMatchData(ArenaMatchButtonIds[0]);
				matchType = matchData.MatchType;
			}

			if (matchType != ArenaMatchType.None)
				SendRpc(person.ClientRpc.LoadScreenResponse, matchType);
		}

		[RPC]
		public void ArenaMatchReadyResponse(int loginId, int personId, int matchButtonId, long timeSec, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.GetPersonByPersonId(personId);
			if (person == null)
				return;

			ILogger.Instance.Send($"ArenaMatchReadyResponse loginId: {loginId} personId {personId} matchButtonId: {matchButtonId}");

			var matchData = InstanceGameManager.Instance.ArenaAlgorithm.GetMatchData(matchButtonId);
			var matchType = matchData.MatchType;
			SendRpc(person.ClientRpc.ArenaMatchReadyResponse, matchType, (float)timeSec);
		}

		[RPC]
		public void ArenaMatchTeleportRequest(InstancePerson person)
		{
			SendRpc(guild.OnArenaMatchTeleportRequest, person.loginId, person.personId);
		}

		[RPC]
		public void TeleportToArena(int loginId, int personId, int matchType, string mapName, string mapGuid, NetworkP2PMessageInfo info)
		{
			var instancePerson = personsOnServer.getPersonByLoginId(loginId);
			if (instancePerson == null)
			{
				ILogger.Instance.Send("TeleportToArena instancePerson == null", $"loginId: {loginId}, personId: {personId}, uniqId: {mapGuid}", ErrorLevel.error);
				return;
			}
			if (instancePerson.personId != personId)
			{
				ILogger.Instance.Send("TeleportToArena поменял персонажа! instancePerson.personId != personId, loginId =" + loginId, ErrorLevel.warning);
				return;
			}

			var levelParams = mapGuid;
			ILogger.Instance.Send("TeleportToArena levelParams = " + levelParams, ErrorLevel.trace);

			var map = instancePerson.syn?.map;
			if (map != null && map.LevelName == mapName && map.LevelParam == levelParams)
			{
				ILogger.Instance.Send("TeleportToArena персонаж уже на указанной карте! loginId =" + loginId, ErrorLevel.warning);
				return;
			}

			Teleport(instancePerson, mapName, levelParams, string.Empty); //break;
		}

		[RPC]
		public void LeaveFromMatchRequest(InstancePerson person)
		{
			person.syn.map.OnLeaveArena(person);

			foreach (var scenario in person.syn.map.Scenarios.OfType<MatchBase>())
				scenario.LeaveArena(person);
		}

		public void ArenaGameEndFor(InstancePerson person)
		{
			SendRpc(guild.ArenaGameEndFor, person.personId);
		}

		public void RemoveArenaGroup(string levelParam)
		{
			SendRpc(guild.RemoveArenaGroup, levelParam);
		}

		#endregion

		//Телепорт с псевдо-матчингом и экраном загрузки
		public void TeleportPW3(InstancePerson person, string levelName, string levelParam, string instData, ArenaMatchType matchType, float matchTime)
		{
			SendRpc(person.ClientRpc.LoadScreenResponse, matchType);

			CoroutineRunner.Instance.Timer(matchTime, () =>
			{
				SendRpc(person.ClientRpc.ArenaMatchReadyResponse, matchType, 10f);
				Teleport(person, levelName, levelParam, instData);
			});
		}

		public void Teleport(InstancePerson instancePerson, string levelName, string levelParam, string instData = null)
		{
			if (instancePerson == null)
			{
				ILogger.Instance.Send("Teleport instancePerson == null levelName: " + levelName, ErrorLevel.error);
				return;
			}

			if (instancePerson.bRunTutorial && (instancePerson.TutorialStateFinish || instancePerson.TutorialNextStateInLevelName == levelName))
				instancePerson.NextTutorialState();

			if (instancePerson.dtLoadLevel > TimeProvider.UTCNow)
			{
				ILogger.Instance.Send($"Teleport {instancePerson.loginId} instancePerson.dtLoadLevel > TimeProvider.UTCNow", ErrorLevel.warning);
				return;
			}

			//Сохраним и загрузим игрока на другом инстансе
			instancePerson.dtLoadLevel = TimeProvider.UTCNow.AddSeconds(20);

			instanceGameManager.SyncPersons(instancePerson, true, SyncReason.Teleport,
				() => SendRpc(dispatcher.OnInstanceInfo, instancePerson.loginId, instancePerson.personId, levelName, levelParam, instData ?? string.Empty)
			);
		}

		[RPC]
		public void InstanceInfoResponse(int LoginId, string jsonName, string sIP, int port, string ticket, NetworkP2PMessageInfo info)
		{
			ILogger.Instance.Send($"Receive InstanceInfoResponse: loginId {LoginId}, jsonName {jsonName}, " +
			                      $"ip {sIP}:{port}, ticket {ticket}", ErrorLevel.info);

			var instancePerson = personsOnServer.getPersonByLoginId(LoginId);
			if (instancePerson == null)
			{
				ILogger.Instance.Send("InstanceInfoResponse instancePerson == null LoginId: " + LoginId, ErrorLevel.error);
				return;
			}

			//Игрок загружен на другом инстансе, не сохранять пока
			instancePerson.dtLoadLevel = TimeProvider.UTCNow.AddSeconds(20);

			if (instancePerson.bRunTutorial)
				notifyController.SendShowNotificationResponse(instancePerson, NotifyState.ClearTutorialTask);

			var location = instancePerson.gameXmlData.Edges.GetAllLocationXmlDatas().FirstOrDefault(x => x.GetJsonName() == jsonName);
			var playerServerStatus = location?.Type == LocationType.Camp ? PlayerServerStatus.IDLE : PlayerServerStatus.IN_BATTLE;

			SendRpc(instancePerson.ClientRpc.LoadLevelResponse, ClientErrors.SUCCESS, jsonName, sIP, port, ticket, playerServerStatus);

			if (instancePerson.login.ticket != ticket)
				instancePerson.login.ticket = string.Empty;
		}

		[RPC]
		public void GoHomeRequest(InstancePerson instancePerson)
		{
			GoHome(instancePerson);
		}

		public void GoHome(InstancePerson instancePerson)
		{
			if (instancePerson.dtLoadLevel > TimeProvider.UTCNow)
			{
				ILogger.Instance.Send("LoadLevelRequest instancePerson.bLoadLevel");
				return;
			}

			var levelName = instancePerson.gameXmlData.Edges.GetCampLevel(instancePerson).LevelName;
			if (levelName == ServerStartGameManager.levelName)
			{
				ILogger.Instance.Send("LoadLevelRequest levelName is same", ErrorLevel.warning);
				return;
			}

			if (instancePerson.syn != null && instancePerson.syn.map != null)
				instancePerson.syn.map.OnTeleport(instancePerson);

			Teleport(instancePerson, levelName, string.Empty, string.Empty);
		}

		[RPC]
		public void TeleportOpenRequest(InstancePerson instancePerson)
		{
			teleportOpenRequest(instancePerson);
		}

		public void teleportOpenRequest(InstancePerson instancePerson, bool useForce = false)
		{
			if (instancePerson.dtLoadLevel > TimeProvider.UTCNow)
			{
				ILogger.Instance.Send("LoadLevelRequest instancePerson.bLoadLevel");
				SendLoadLevelResponseError(instancePerson, ClientErrors.PORTAL_LEVEL_LOADING);
				return;
			}

			var map = instancePerson.syn.map;
			var portal = map.GetNearestActiveMapObject<MapPortal>(instancePerson.syn);
			if (portal == null)
			{
				ILogger.Instance.Send("LoadLevelRequest portal == null");
				SendLoadLevelResponseError(instancePerson, ClientErrors.PORTAL_NOT_FOUND);
				return;
			}

			var error = portal.TryEnter(instancePerson, useForce);
			if (error != ClientErrors.SUCCESS)
			{
				ILogger.Instance.Send("LoadLevelRequest TryEnterError " + error);

				if (error == ClientErrors.PORTAL_FORT_INVIDE)
					return;

				SendLoadLevelResponseError(instancePerson, error);
				return;
			}

			var levelName = portal.GetLevelName(instancePerson);
			if (string.IsNullOrEmpty(levelName))
			{
				ILogger.Instance.Send("LoadLevelRequest levelName is null", ErrorLevel.error);
				SendLoadLevelResponseError(instancePerson, ClientErrors.PORTAL_LEVEL_NAME_IS_NULL);
				return;
			}

			var levelParam = portal.GetLevelParam(instancePerson);
			if (levelName == map.LevelName && levelParam == map.LevelParam)
			{
				ILogger.Instance.Send("LoadLevelRequest levelName is same");
				SendLoadLevelResponseError(instancePerson, ClientErrors.PORTAL_LEVEL_NAME_IS_SAME);
				return;
			}

			if (instancePerson.bRunTutorial)
				instancePerson.Dialogs.Add("teleportOpenRequest");

			portal.Enter(instancePerson);


			Teleport(instancePerson, levelName, levelParam, string.Empty);
		}

		[RPC]
		public void LoadLevelRequest(InstancePerson instancePerson, bool useForce)
		{
			teleportOpenRequest(instancePerson, useForce);
		}

		public void SendLoadLevelResponseError(InstancePerson instancePerson, ClientErrors error)
		{
			SendRpc(instancePerson.ClientRpc.LoadLevelResponse, error, string.Empty, string.Empty, 0, string.Empty, PlayerServerStatus.IDLE);
		}

		[RPC]
		public void ActivateTeleportRequest(InstancePerson instancePerson)
		{
			var portal = instancePerson.syn.map.GetNearestActiveMapObject<AInteraction_Activate>(instancePerson.syn);
			if (portal == null)
			{
				ILogger.Instance.Send("ActivateTeleportRequest portal == null", ErrorLevel.warning);
				SendLoadLevelResponseError(instancePerson, ClientErrors.PORTAL_NOT_FOUND);
				return;
			}

			portal.TryActivate(instancePerson);
		}

		// нажали на шестеренку ждем ответа
		[RPC]
		public void GetEnterToTeleportRequest(InstancePerson instancePerson)
		{
			var portal = instancePerson.syn.map.GetNearestActiveMapObject<AInteraction_Activate>(instancePerson.syn);
			if (portal == null)
			{
				ILogger.Instance.Send("LoadLevelRequest portal == null");
				SendLoadLevelResponseError(instancePerson, ClientErrors.PORTAL_NOT_FOUND);
				return;
			}

			portal.TryActivate(instancePerson);
		}

		//Разлокировать или переместиться в данж
		[RPC]
		public void DungeonOpenRequest(InstancePerson instancePerson, int dungeonId, bool isBuy)
		{
			var dungeons = instancePerson.gameXmlData.Dungeons.Find(loc => loc.DungeonLocations.Exists(dun => dun.LocationId == dungeonId));
			var dungeon = dungeons?.DungeonLocations.Find(edg => edg.LocationId == dungeonId);
			if (dungeon == null)
				return;

			if (isBuy && !dungeon.UnlockRules.CanUnlock(instancePerson))
				return;

			var resData = instancePerson.GetNeedForResource();
			if (!dungeonManager.OpenDungeonLocationRequest(instancePerson, dungeonId, resData, isBuy))
				return;

			teleportOpenRequest(instancePerson, dungeon.location);

			SendRpc(instancePerson.ClientRpc.DungeonOpenResponse, resData.IsSuccess);
		}

		public void teleportOpenRequest(InstancePerson instancePerson, EdgeLocationXmlData data)
		{
			if (instancePerson.dtLoadLevel > TimeProvider.UTCNow)
			{
				ILogger.Instance.Send("LoadLevelRequest instancePerson.bLoadLevel");
				SendLoadLevelResponseError(instancePerson, ClientErrors.PORTAL_LEVEL_LOADING);
				return;
			}

			var levelName = data.LevelName;
			if (string.IsNullOrEmpty(levelName))
			{
				ILogger.Instance.Send("LoadLevelRequest levelName is null");
				SendLoadLevelResponseError(instancePerson, ClientErrors.PORTAL_LEVEL_NAME_IS_NULL);
				return;
			}

			var levelParam = data.Type == LocationType.Dungeon ? instancePerson.PersonalParam : string.Empty;

			var map = instancePerson.syn.map;
			if (levelName == map.LevelName && levelParam == map.LevelParam)
			{
				ILogger.Instance.Send("LoadLevelRequest levelName is same");
				SendLoadLevelResponseError(instancePerson, ClientErrors.PORTAL_LEVEL_NAME_IS_SAME);
				return;
			}

			Teleport(instancePerson, levelName, levelParam, string.Empty);
		}

		#region Дружеские и командные режимы

		/// <summary>
		/// Клиент запрашивает данные по группам для всех режимов при каждом подключении инстансу
		/// </summary>
		[RPC]
		public void GetAllPlayModeDatasRequest(InstancePerson person)
		{
			var personCard = new PersonCard
			{
				loginId = person.loginId,
				name = person.login.loginName,
				avatarIcon = person.login.AvatarName,
				clanData = new ShortClanData(Guild.GetGuildDataById(person.GuildId), person.GuildRole)
			};
			var selectedPersonData = new SelectedPersonData
			{
				personId = person.personId,
				personType = person.personType,
				level = person.Level,
				gearScore = person.HeroPower,
				rating = (int)person.StatContainer.GetStatValue(Stats.EloRatingTeam),
				itemsId = person.EquippedNames,
			};

			int actualButtonId = arenaAlgorithm.GetActualButtonId(person, person.login.LastArenaButtonId);
			person.login.LastArenaButtonId = actualButtonId;

			SendRpc(guild.AllPlayModeDatasRequest, person.loginId, personCard, selectedPersonData, person.login.LastArenaButtonId);
		}

		/// <summary>
		/// Отправляем на клиент данные по всем режимам игры
		/// </summary>
		[RPC]
		public void AllPlayModeDatasResponse(int loginId, Dictionary<PlayModeType, PlayModeData> playModeDatas, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"AllPlayModeDatasResponse person == null login={loginId}", ErrorLevel.error);
				return;
			}

			//Добавим одиночную игру
			playModeDatas[PlayModeType.SinglePlayer] = new PlayModeData
			{
				ArenaButtonId = person.login.LastArenaButtonId,
				selectedMapName = string.Empty,
				partyData = new PartyData()
			};
			
			SendRpc(person.ClientRpc.AllPlayModeDatasResponse, person.login.currentPlayModeType, playModeDatas);
		}

		/// <summary>
		/// Отправляем на клиент данные игрового режима, когда игрок присоединяется к новой группе
		/// </summary>
		[RPC]
		public void UpdatePlayModeDataResponse(int loginId, PlayModeType playModeType, PlayModeData playModeData, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"UpdatePlayModeDataResponse person == null login={loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.UpdatePlayModeDataResponse, playModeType, playModeData);
		}

		[RPC]
		public void ChangePlayModeRequest(InstancePerson person, PlayModeType newPlayModeType)
		{
			ChangeReadyStatusRequest(person, false);

			//TODO: Смена режима вызывает изменения в интерфейсе поиска группы для присоединения.
			//На данный момент такого интерфейса у нас нет, но когда появится, то нужно будет доплнить логику
			person.login.ChangePlayModeType(newPlayModeType);
		}

		[RPC]
		public void ChangeArenaTypeRequest(InstancePerson person, int newArenaButtonId)
		{
			if (person.login.currentPlayModeType == PlayModeType.SinglePlayer)
			{
				//Для одиночной игры изменить локально
				person.login.LastArenaButtonId = newArenaButtonId;
			}
			else
			{
				//Для командной игры отправить на guild
				SendRpc(guild.ChangeArenaTypeRequest, person.loginId, person.login.currentPlayModeType, newArenaButtonId);
			}
		}

		[RPC]
		public void UpdateArenaTypeResponse(int loginId, int partyId, int arenaButtonId, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"UpdateArenaTypeResponse person == null login={loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.UpdateArenaTypeResponse, partyId, arenaButtonId);
		}

		[RPC]
		public void ChangeReadyStatusRequest(InstancePerson person, bool readyStatus)
		{
			if (person.login.currentPlayModeType == PlayModeType.SinglePlayer)
			{
				//Игрок нажал на кнопку в бой
				if (readyStatus)
					ArenaMatchRequest(person);
			}
			else
			{
				SendRpc(guild.ChangeReadyStatusRequest, person.loginId, person.login.currentPlayModeType, readyStatus);

				//Для группы логка будет более сложной
				//Если лидер говорит, что он готов в бой, то должен запуститься бой.
				//P.S. на макетах присутствует таймер обратного отсчёта до старта битвы, его, наверное, реализуем позже
				//P.P.S. Не понятно когда сбрасывать статус готовности. По завершению матча?
			}
		}

		[RPC]
		public void ReadyStatusResponse(int loginId, int partyId, int memberId, bool readyStatus, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"SendReadyStatusResponse person == null login={loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.ReadyStatusResponse, partyId, memberId, readyStatus);
		}

		/// <summary>
		/// Приходит, когда один игрок приглашает в группу другого игрока
		/// </summary>
		[RPC]
		public void PartyInviteRequest(InstancePerson person, int partyId, int inviteLoginId)
		{
			SendRpc(guild.PartyInviteRequest, person.loginId, partyId, inviteLoginId);
		}

		[RPC]
		public void PartyInviteResponse(int loginId, PartyInviteData partyInviteData, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"PartyInviteResponse person == null login={loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.PartyInviteResponse, partyInviteData);
		}

		/// <summary>
		/// Приходит, когда игрок положительно отвечает на приглашение в группу
		/// </summary>
		/// <param name="partyId">Идентификатор группы в которую приглашается игрок</param>
		[RPC]
		public void PartyInviteAcceptRequest(InstancePerson person, int partyId)
		{
			SendRpc(guild.PartyInviteAcceptRequest, person.loginId, partyId);
		}

		[RPC]
		public void PartyInviteAcceptResponse(int loginId, bool inviteResult, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"AddPlayerToPartyResponse person == null login={loginId}", ErrorLevel.error);
				return;
			}

			//Ответим клиенту о результате операции добавления в группу
			SendRpc(person.ClientRpc.PartyInviteAcceptResponse, inviteResult);
		}

		/// <summary>
		/// Отправляем на клиент данные о добавленном в группу игроке
		/// </summary>
		/// <param name="loginId">Игрок, которму будут отправлены данные</param>
		/// <param name="partyId">Идентификтор группы, в которую добавился персонаж</param>
		/// <param name="partyMemberData">Данные нового игрока в группе</param>
		[RPC]
		public void AddPlayerToPartyResponse(int loginId, int partyId, PartyMemberData partyMemberData, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"AddPlayerToPartyResponse person == null login={loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.AddPlayerToPartyResponse, partyId, partyMemberData);
		}

		/// <summary>
		/// Клиент запрашивает удаление участиника группы
		/// </summary>
		/// <param name="partyId">Группа из которой необходимо удалить игрока</param>
		/// <param name="removeLoginId">Идентификатор грока, которого необходимо удалить</param>
		[RPC]
		public void RemovePlayerFromPartyRequest(InstancePerson person, int partyId, int removeLoginId)
		{
			SendRpc(guild.RemovePlayerFromPartyRequest, person.loginId, partyId, removeLoginId);
		}

		/// <summary>
		/// Отправляем клиенту данные о том, что из группы был удалён игрок
		/// </summary>
		/// <param name="loginId">Игрок, которому будт отправлены данные</param>
		/// <param name="partyId">Группа из которой был удалён игрок</param>
		/// <param name="memberId">Идентификтор игрока, который был удалён</param>
		[RPC]
		public void RemovePlayerFromPartyResponse(int loginId, int partyId, int memberId, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"RemovePlayerFromParty person == null login={loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.RemovePlayerFromPartyResponse, partyId, memberId);
		}

		private void OnUpdatePersonCard(LobbyLogin login)
		{
			SendRpc(guild.UpdatePersonCardRequest, login.loginId, login.loginName, login.AvatarName);
		}

		/// <summary>
		/// Отправляем на клиент данные об изменении параметров игрока
		/// </summary>
		/// <param name="loginId">Игрок, которому будт отправлены данные</param>
		/// <param name="partyId">Идентификтор группы, в которой произошли изменения</param>
		/// <param name="personCard">Изменённые параметры игрока. В personCard есть поле loginId - по нему клиент определит какой игрок изменился</param>
		[RPC]
		public void UpdatePersonCardResponse(int loginId, int partyId, PersonCard personCard, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"UpdatePersonCardResponse person == null login={loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.UpdatePersonCardResponse, partyId, personCard);
		}

		private void OnUpdateCharacterData(InstancePerson person)
		{
			var selectedPersonData = new SelectedPersonData
			{
				personId = person.personId,
				personType = person.personType,
				level = person.Level,
				gearScore = person.HeroPower,
				rating = (int)person.StatContainer.GetStatValue(Stats.EloRatingTeam),
				itemsId = person.EquippedNames,
			};

			SendRpc(guild.UpdateCharacterDataRequest, person.loginId, selectedPersonData);
		}

		/// <summary>
		/// Отправляем на клиент данные об изменении выбранного персонажа
		/// </summary>
		/// <param name="loginId">Игрок, которому будт отправлены данные</param>
		/// <param name="partyId">Идентификтор группы, в которой произошли изменения</param>
		/// <param name="memberId">loginId нужен клиенту для того, чтобы он понимал чей персонаж изменился</param>
		/// <param name="personData">Новые данные о персонаже</param>
		[RPC]
		public void UpdateCharacterDataResponse(int loginId, int partyId, int memberId, SelectedPersonData personData, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"UpdatePersonCardResponse person == null login={loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.UpdateCharacterDataResponse, partyId, memberId, personData);
		}

		/// <summary>
		/// Клиент инициирует попытку вступить в группу по коду группы
		/// Вступление по коду группы на данный момент не требует подтверждения лидером группы
		/// </summary>
		/// <param name="partyCode"></param>
		[RPC]
		public void ApplyPartyCodeRequest(InstancePerson person, string partyCode)
		{
			SendRpc(guild.ApplyPartyCodeRequest, person.loginId, partyCode);
		}

		/// <summary>
		/// Отсылаем на клиенте данные о том, что если игрок захочет присоедениться к группе, то он должен будет покинуть текущую группу
		/// </summary>
		/// <param name="loginId">Игрок, которому отправляются данные</param>
		/// <param name="playModeType">Режим группы для которого происходят изменения</param>
		/// <param name="newParty">Данные о новой группе, чтобы клиент мог отобразить из какой группы в какую происходит переход</param>
		[RPC]
		public void ApplyPartyCodeResponse(int loginId, PlayModeType playModeType, PlayModeData newParty, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"PartyTransferResponse person == null login={loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.PartyTransferResponse, playModeType, newParty);
		}

		/// <summary>
		/// Игрок согласился перейти из одной группы в другую
		/// </summary>
		/// <param name="partyId">Идентификатор группы, в котороую желает перейти игрок </param>
		[RPC]
		public void AcceptPartyTransfer(InstancePerson person, int partyId)
		{
			SendRpc(guild.AcceptPartyTransferRequest, person.loginId, partyId);
		}

		[RPC]
		public void AcceptPartyTransferResponse(int loginId, JoinPartyResult joinPartyResult, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"ApplyPartyCodeResponse person == null login={loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.JoinPartyResultResponse, joinPartyResult);
		}

		/// <summary>
		/// Игрок создаёт запрос на вступление в чью-либо группу
		/// </summary>
		[RPC]
		public void PartyRequest(InstancePerson person, int partyId)
		{
			SendRpc(guild.PartyRequest, person.loginId, partyId);
		}

		/// <summary>
		/// Отправляем на клиент запрос на вступление в группу
		/// </summary>
		/// <param name="loginId">Игрок, которому отправляется запрос (лидер группы)</param>
		/// <param name="partyRequestData">Данные запроса</param>
		[RPC]
		public void PartyRequestResponse(int loginId, PartyRequestData partyRequestData, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"ApplyPartyCodeResponse person == null login={loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.PartyRequestResponse, partyRequestData);
		}

		/// <summary>
		/// Лидер группы одобряет запрос на вступление в группу
		/// </summary>
		/// <param name="partyRequestData">Данные запроса</param>
		[RPC]
		public void PartyRequestAcceptRequest(InstancePerson person, PartyRequestData partyRequestData)
		{
			SendRpc(guild.PartyRequestResultRequest, person.loginId, partyRequestData.requestId, PartyRequestResult.Accepted);
		}

		/// <summary>
		/// Лидер группы отклоняет запрос на добавление нового участника группы
		/// </summary>
		[RPC]
		public void PartyRequestDeclineRequest(InstancePerson person, PartyRequestData partyRequestData)
		{
			SendRpc(guild.PartyRequestResultRequest, person.loginId, partyRequestData.requestId, PartyRequestResult.Declined);
		}

		[RPC]
		public void PartyRequestResultResponse(int loginId, PartyRequestData partyRequestData, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"ApplyPartyCodeResponse person == null login={loginId}", ErrorLevel.error);
				return;
			}

			SendRpc(person.ClientRpc.PartyRequestResultResponse, partyRequestData);
		}

		#endregion
	}
}
