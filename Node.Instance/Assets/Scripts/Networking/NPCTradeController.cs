﻿using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Analytic;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using LobbyInstanceLib.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using uLink;
using UtilsLib.DegSerializers;
using UtilsLib.ExternalLogs.Classes;
using UtilsLib.ExternalLogs.Enums;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using Zenject;

namespace Assets.Scripts.Networking
{
	public sealed class NPCTradeController : BaseRpcListener
	{
		private NPCTradeAlgorithm m_tradeAlgorithm;
		private ShopManager m_shopManager;
		private ShopSender m_shopSender;

		[Inject]
		public void Construct(NPCTradeAlgorithm tradeAlgorithm, ShopManager shopManager, ShopSender shopSender)
		{
			m_tradeAlgorithm = tradeAlgorithm ?? throw new ArgumentNullException(nameof(tradeAlgorithm));
			m_shopManager = shopManager;
			m_shopSender = shopSender;
		}

		public NPCTradeController(NetworkP2P networkP2P, uLink.Network network, INetworkView view) : base(networkP2P, network, view)
		{
		}

		//Продать вещи
		[RPC]
		public void SellItemsRequest(InstancePerson instancePerson, List<int> itemsId, InventorySlotMode mode, bool bunchOfItems)
		{
			bool success = true;
			foreach (var itemId in itemsId)
			{
				if (instancePerson.bRunTutorial)
				{
					instancePerson.Dialogs.Add("SellItemsRequest");

					//Нельзя продавать туторные вещи
					//var item = instancePerson.GetItemById(itemId) ?? instancePerson.GetStoreItemById(itemId);
					//if (item != null && instancePerson.bTutorialCanNotSell && item.IsTutorial())
					//{
					//	ServerRpcListener.Instance.SendShowNotificationTehNameResponse(instancePerson, NotifyState.TutorialUnavailableNotify, m_serverConstants.TutorialUnavailableMessage);
					//	success = false;
					//	continue;
					//}
				}

				if (!m_tradeAlgorithm.SellItem(instancePerson, itemId, bunchOfItems))
					success = false;
			}

			SendRpc(instancePerson.ClientRpc.SellItemsResponse, success);
		}

		//Купить вещь
		[RPC]
		public void BuyItemsRequest(InstancePerson instancePerson, int personId, int itemId, int count)
		{
			var currentPerson = instancePerson.login.InstancePersons.FirstOrDefault(x => x.personId == personId);
			if (currentPerson == null)
			{
				ILogger.Instance.Send("UseInventoryItemRequest instancePerson == null", ErrorLevel.error);
				return;
			}

			if (count <= 0)
				return;

			if (currentPerson.bRunTutorial)
				currentPerson.Dialogs.Add("BuyItemsRequest");

			buyItemFromNPC(currentPerson, itemId, count);
		}

		private void buyItemFromNPC(InstancePerson instancePerson, int itemId, int count)
		{
			int ownCount = 0;
			var item = instancePerson.GetTradeItems().Find(itemData => itemData.ItemId == itemId); //Посмотрим в сумке торговца
			if (item == null)
			{
				item = instancePerson.GetItem(itemData => itemData.ItemId == itemId) ?? //Посмотрим в сумке своей
					instancePerson.potions.GetItemById(itemId); //Посмотрим в поясе

				if (item == null || item.ItemType != ItemType.Potion) //только зелья можно докупать
					return;

				ownCount = item.Count;
				item = item.Copy();
			}

			//Нельзя покупать вещь, которую персонаж не сможет надеть
			if (item.CanEquip && !instancePerson.CanEquip(item))
				return;

			if (item.ItemType == ItemType.Potion)
				item.Count = Math.Min(count, item.StackCount - ownCount); //зелий можно купить больше одного
			else if (instancePerson.HaveTradeItem(item))
				return; //Нельзя покупать вещь, которая уже есть у персонажа

			var tradeInfo = item.TradeInfos.GetCurrentTradeOffer(TimeProvider.UTCNow, instancePerson);
			if (!string.IsNullOrEmpty(tradeInfo.Sku))
			{
				ILogger.Instance.Send("NPCTradeController.buyItemFromNPC: Нельзя купить, только через InApp", ErrorLevel.error);
				return;
			}

			var cost = tradeInfo.Cost * item.Count;
			if (cost <= 0)
				return;

            var analyticInfo = new AnalyticCurrencyInfo()
            {
                needSendInWallet = false,
                sendGain = false,
                Amount = cost,
                Destination = item.ItemType.ToString(),
                DestinationType = item.GetName(),
                Source = SourceGain.Armory,
                SourceType = tradeInfo.BuyCostType.ToString(),
                Currency = tradeInfo.BuyCostType,
                USDCost = 0f,
				PersonLevel = instancePerson.Level,
				TestPurchase = instancePerson.login.settings.testPurchase
			};

			m_shopManager.Buy(instancePerson, cost, tradeInfo.BuyCostType, AccountChangeReason.BuyItemFromNPC, analyticInfo,
                () =>
				{
					var itemsList = new List<InventoryItemData> { item };
					m_shopSender.SendBuyItemsResponse(instancePerson, BuyItemResponseType.ShopItem, itemsList);
					m_tradeAlgorithm.BuyItem(instancePerson, item, analyticInfo);
                    AnalyticsManager.Instance.SendData(instancePerson, analyticInfo);
				}
			);
		}
	}
}
