﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Analytic;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.XmlData.LootData.ConcreteLoot;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Common;
using uLink;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using UtilsLib.Protocol;
using Zenject;

namespace Assets.Scripts.Networking
{
    public class ShopController : BaseInstanceRpcListener,
		IInstanceSysDispatcher_ShopController<NetworkP2PMessageInfo>,
		IInstanceSysLobby_ShopController<NetworkP2PMessageInfo>
    {
        ShopManager shopManager;
        ServerConstants serverConstants;
		ISpecialActionController specialActionController;

		IPersonsOnServer personsOnServer;

		public ShopController(NetworkP2P networkP2P, uLink.Network network, uLink.INetworkView view) : base(networkP2P, network, view)
		{
		}

		[Inject]
        public void Constructor(IPersonsOnServer personsOnServer, ShopManager shopManager, ServerConstants serverConstants, 
            ISpecialActionController specialActionController)
        {
            this.shopManager = shopManager;
            this.personsOnServer = personsOnServer;
            this.serverConstants = serverConstants;
            this.specialActionController = specialActionController;
        }

        [RPC]
        public void PaymentXml(List<Prices> pricePoints, NetworkP2PMessageInfo info)
        {
            shopManager.SetPricePoints(pricePoints);
        }

        #region GoldShop

        [RPC]
        public void ProductListRequest(InstancePerson instancePerson, string paymentPlatform, string paymentCurrency)
        {
            
            instancePerson.login.PaymentPlatform = paymentPlatform;
            instancePerson.login.PaymentCurrency = paymentCurrency;

            ILogger.Instance.Send($"ProductListRequest().. loginId:{instancePerson.login.loginId} paymentPlatform: {instancePerson.login.PaymentPlatform} paymentCurrency: {instancePerson.login.PaymentCurrency}, instancePerson.login: {instancePerson.login.GetHashCode()} instancePerson: {instancePerson.GetHashCode()}");

            shopManager.ProductListRequest(instancePerson);

            specialActionController.SpecialActionRequest(instancePerson);
		}

        [RPC]
        public void GetShopItemsRequest(InstancePerson instancePerson, string pointKey, string pointCurrency)
        {
            throw new NotImplementedException("GetShopItemsRequest");
        }

        [RPC]
        public void GetProductItemsRequest(InstancePerson instancePerson, int filterId /*тут всегда 0*/)
        {
            ILogger.Instance.Send($"GetProductItemsRequest paymentPlatform: {instancePerson.login.PaymentPlatform} paymentCurrency: {instancePerson.login.PaymentCurrency} instancePerson.login: {instancePerson.login.GetHashCode()} instancePerson: {instancePerson.GetHashCode()}");
            shopManager.GetProductItemsRequest(instancePerson);
        }

        [RPC]
        //TODO_maximpr хорошо бы чтобы тут нам прислали фильтр
        public void BuyShopItemRequest(InstancePerson instancePerson, int itemId)
        {
            shopManager.BuyShopItemRequest(instancePerson, itemId);
        }

        [RPC]
        public void LoginAccountChangeResponse(AccountChangeParams data, NetworkP2PMessageInfo info)
        {
			var instancePerson = personsOnServer.getPersonByLoginId(data.loginId);

			var RealPaymentInfo = new AnalyticCurrencyInfo()
            {
                needSendInWallet = false,
                sendGain = false,
                Destination = SinkType.ShopBundle.ToString(),
                Currency = ResourceType.USD,                
                PersonLevel = instancePerson.login.GetMaxPersonLevel(),
                RealMoneyPurchase = true,
                transactionId = AnalyticsManager.GetSHA256(DateTime.UtcNow.ToString()+ data.loginId.ToString()),
				TestPurchase = instancePerson.login.settings.testPurchase
			};

            AnalyticCurrencyInfo analyticInfo = null;
            if (data == null)
            {
                ILogger.Instance.Send("LoginAccountChangeResponse data = null", ErrorLevel.error);
                return;
            }

            ILogger.Instance.Send($"LoginAccountChangeResponse().. loginId: {data.loginId} reason: {data.reason.ToString()} summ: {data.summ} tranId: {data.tranId}", ErrorLevel.debug);

            
            if (instancePerson == null)
            {
                ILogger.Instance.Send("LoginAccountChangeResponse: не найден персонаж! instancePerson = null, data=" + data.loginId, ErrorLevel.error);
                return;
            }

            //если купили за InApp $
            if (data.reason == AccountChangeReason.SpecialOffer)
            {
				if (shopManager.ShopOff)
				{
					return;
				}
				//аналитика внутри
				var offerId = data.summ;
				specialActionController.BuySpecialOffer(instancePerson, offerId);
                //var pricePoint = shopManager.GetSpecialOfferPricePoint(instancePerson, data.summ, out _);
                //if (pricePoint != null)
                //    instancePerson.syn?.map.OnPricePointBuy(instancePerson, pricePoint);

                return; //тк lAfterUpdate == 0
			}

			if (data.reason == AccountChangeReason.BuildingKit)
			{
				var pricePoint = shopManager.GetPricePoint(instancePerson, x => x.Item == PriceItem.BuildingKit && x.TotalSumm == data.summ);
				if (pricePoint == null)
				{
					ILogger.Instance.Send($"LoginAccountChangeResponse: pricePoint == null summ = {data.summ}", ErrorLevel.error);
					return;
				}

				var buildingKit = instancePerson.gameXmlData.PlayerCityData.BuildingKits.Find(x => x.TradeInfos.list.Exists(y => y.Sku == pricePoint.ExternPoint));
				if (buildingKit == null)
				{
					ILogger.Instance.Send($"LoginAccountChangeResponse: buildingKit == null Sku = {pricePoint.ExternPoint}", ErrorLevel.error);
					return;
				}

                RealPaymentInfo.Amount = AnalyticsManager.Instance.GetCost(pricePoint.Price);
                RealPaymentInfo.DestinationType = buildingKit.GetName();
                RealPaymentInfo.Sku = pricePoint.ExternPoint;
                RealPaymentInfo.Source = SourceGain.PurchaseShop;
                RealPaymentInfo.SourceType = buildingKit.GetName();
                RealPaymentInfo.USDCost = AnalyticsManager.Instance.GetCost(pricePoint.Price);

				var item = instancePerson.gameXmlData.ItemDataRepository.GetCopyInventoryItemData(buildingKit.Name, 1);
                instancePerson.AddItem(item, RealPaymentInfo);

                AnalyticsManager.Instance.SendData(instancePerson, RealPaymentInfo);

                return; //тк lAfterUpdate == 0
			}

            //купили за реал (InApp) изюм (Emeralds)
            if (data.reason == AccountChangeReason.RealPayment)
            {
				var pricePoint = shopManager.GetEmeraldPricePoint(instancePerson, data.summ);
                //if (pricePoint != null)
                //	instancePerson.syn.map.OnPricePointBuy(instancePerson, pricePoint);

				if (pricePoint != null)
				{
					var emeraldItem = instancePerson.gameXmlData.ItemDataRepository.GetCopyInventoryItemData(ResourceType.Emerald, data.summ);
					var rewardItems = new List<InventoryItemData> {emeraldItem};
					shopManager.SendBuyItemsResponse(instancePerson, BuyItemResponseType.ShopItem, rewardItems, vipPoints: pricePoint.VipPoint);

                    var parts = pricePoint.ExternPoint.Split('.');
                    string name = AnalyticСurrency.emerald.ToString();
                    if (parts!= null && parts.Length>0)
                    {
                        name = parts[parts.Length-1];
                    }

                    analyticInfo = new AnalyticCurrencyInfo()
                    {
                        needSendInWallet = true,
                        sendGain = true,
                        Amount = data.summ,
                        Currency = ResourceType.Emerald,
                        USDCost = AnalyticsManager.Instance.GetCost(pricePoint.Price),
                        Source = SourceGain.PurchaseShop,
                        SourceType = name,
                        PersonLevel = instancePerson.login.GetMaxPersonLevel(),
                        RealMoneyPurchase = true,
                        Sku = pricePoint.ExternPoint,
                        transactionId = RealPaymentInfo.transactionId,
						TestPurchase = instancePerson.login.settings.testPurchase
					};

                    RealPaymentInfo.Amount = AnalyticsManager.Instance.GetCost(pricePoint.Price);
                    RealPaymentInfo.DestinationType = AnalyticСurrency.emerald.ToString();
                    RealPaymentInfo.Sku = pricePoint.ExternPoint;
                    AnalyticsManager.Instance.SendData(instancePerson, RealPaymentInfo);

                    GlobalEvents.Instance.OnPersonDonat(instancePerson, data.summ, name);
                }
            }

			//купили золото за реал
			if (data.reason == AccountChangeReason.Gold)
			{
				var pricePoint = shopManager.GetGoldPricePoint(instancePerson, data.summ);

				if (pricePoint != null)
				{
					var goldItem = instancePerson.gameXmlData.ItemDataRepository.GetCopyInventoryItemData(ResourceType.Gold, data.summ);
					var rewardItems = new List<InventoryItemData> { goldItem };
					shopManager.SendBuyItemsResponse(instancePerson, BuyItemResponseType.ShopItem, rewardItems, vipPoints: pricePoint.VipPoint);

                    var parts = pricePoint.ExternPoint.Split('.');
                    string name = AnalyticСurrency.emerald.ToString();
                    if (parts != null && parts.Length > 0)
                    {
                        name = parts[parts.Length - 1];
                    }

                    analyticInfo = new AnalyticCurrencyInfo()
                    {
                        needSendInWallet = true,
                        sendGain = true,
                        Amount = data.summ,
                        Currency = ResourceType.Gold,
                        USDCost = AnalyticsManager.Instance.GetCost(pricePoint.Price),
                        Source = SourceGain.PurchaseShop,
                        SourceType = name,
                        PersonLevel = instancePerson.Level,
                        RealMoneyPurchase = true,
                        Sku = pricePoint.ExternPoint,
                        transactionId = RealPaymentInfo.transactionId,
						TestPurchase = instancePerson.login.settings.testPurchase
					};

                    RealPaymentInfo.Amount = AnalyticsManager.Instance.GetCost(pricePoint.Price);
                    RealPaymentInfo.DestinationType = AnalyticСurrency.gold.ToString();
                    RealPaymentInfo.Sku = pricePoint.ExternPoint;
                    AnalyticsManager.Instance.SendData(instancePerson, RealPaymentInfo);

                    instancePerson.AddItem(goldItem, analyticInfo); //Выдать золото
                    GlobalEvents.Instance.OnPersonDonat(instancePerson, data.summ, name);
                }

				return; //тк lAfterUpdate == 0
			}

			//бонус за покупку изюма
			if (data.reason == AccountChangeReason.EmeraldVipBonus)
			{
				int realPaymentSumm = int.Parse(data.buyParams);
				var pricePoint = shopManager.GetEmeraldPricePoint(instancePerson, realPaymentSumm);
				if (pricePoint != null)
				{
					var emeraldItem = instancePerson.gameXmlData.ItemDataRepository.GetCopyInventoryItemData(ResourceType.Emerald, realPaymentSumm - data.summ);
					var rewardItems = new List<InventoryItemData> { emeraldItem };
					shopManager.SendBuyItemsResponse(instancePerson, BuyItemResponseType.ShopItem, rewardItems, vipPoints: pricePoint.VipPoint);
				}
			}

            shopManager.LoginAccountChangeResponse(instancePerson, data, analyticInfo);
        }

        [RPC]
		public void TempEmeraldRequest(InstancePerson instancePerson, string sku)
		{
			//Читеры могут покупать не платя реальные деньги
			if (RemoteXmlConfig.Instance.IsUserCanApplyCheats(instancePerson.loginId))
			{
				var pricePoint = shopManager.GetPricePoint(instancePerson, x => x.ExternPoint == sku);
			    if (pricePoint != null)
			    {
			        switch (pricePoint.Item)
			        {
                        case "emerald": shopManager.EmeraldBuyCheat(instancePerson, pricePoint.TotalSumm); break;
                        case "gold": shopManager.GoldBuyCheat(instancePerson, pricePoint.TotalSumm); break;
                        case "slot": break;                        
                        case "specialoffer": shopManager.BuySpecialOfferCheat(instancePerson, AccountChangeReason.SpecialOffer, pricePoint.TotalSumm); break;
                        case "buildingkit": shopManager.BuySpecialOfferCheat(instancePerson, AccountChangeReason.BuildingKit, pricePoint.TotalSumm); break;
					}
                }
			}
		}

		[RPC]
		public void DonateShowResponse(int loginId, string iframeUrl, string pageUrl, string orderToken, NetworkP2PMessageInfo info)
		{
			var person = personsOnServer.getPersonByLoginId(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"DonateShowResponce: не найден персонаж! instancePerson = null, loginId={loginId}", ErrorLevel.error);
				return;
			}

			ILogger.Instance.Send($"DonateShowResponce: loginId={loginId} iframeUrl={iframeUrl} pageUrl={pageUrl} orderToken={orderToken}", ErrorLevel.debug);

			SendRpc(person.ClientRpc.DonateShowResponse, iframeUrl);

			SendRpc(auth.OnDonateShowResponse, loginId); //Подтвердим, что сообщение показано
		}

        #endregion
    }
}
