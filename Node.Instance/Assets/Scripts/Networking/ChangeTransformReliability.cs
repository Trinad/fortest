﻿namespace Assets.Scripts.Networking
{
	public enum ChangeTransformReliability
	{
		NotSet,
		Reliable,
		ReliableForced,
		Unreliable,
		UnreliableForced,
	}
}
