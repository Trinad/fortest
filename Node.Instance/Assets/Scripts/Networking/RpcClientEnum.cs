﻿using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries.Login.DataRPC;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.Perks;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Networking.ClientDataRPC.Arenas;
using Assets.Scripts.Utils;
using Node.Instance.Assets.Scripts.InstanceServer.Enums;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Actions;
using uLink;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using UtilsLib.NetworkingData.Chat;
using UtilsLib.NetworkingData.Party;
using UtilsLib.Protocol;
using Vector2 = System.Numerics.Vector2;
using Vector3 = System.Numerics.Vector3;

namespace Assets.Scripts.Networking
{
	//Инстанс-Клиент
	[RpcSystem]
	public interface RpcClientEnum
	{
		[RpcCommand]
		void GetPlayerStatusOnServerResponse(PlayerServerStatusData data);

		[RpcCommand]
		void CreatePlayerResponse(PlayerData data);

		[RpcCommand("PRS")]
		void PingResponse(byte pingId, float time, float realTime);

		[RpcCommand]
		void StopServerResponse();

		[RpcCommand]
		void AddSynchroToEntity(int entityId, NetworkViewID viewId);
		[RpcCommand]
		void CreateEntity(EntityData data);
		[RpcCommand]
		void CreateEntities(List<EntityData> entities);
		[RpcCommand]
		void RemoveEntity(EntityType eType, int id);
		[RpcCommand("LOOT")]
		void CreateLootResponse(int lootId, string lootName, Vector2PositionPacked pos, Vector2PositionPacked dest);
		[RpcCommand]
		void CollectLootResponse(int lootId, List<InventoryItemData> itemList);
		[RpcCommand]
		void OverflowLoot(int lootId);
		[RpcCommand]
		void UpdateEquipmentResponse(GameModeType gameMode, List<EntityPartInfo> equippedWeapons);
		[RpcCommand("SMR")]
		void SelectMonsterResponse(int personId, EntityType eType, int targetId, TargetIcon targetType, bool show);

		[RpcCommand("CFXR")]
		void CreateFxResponse_Regular(int effectId, int castorId, EntityType eType, EffectType fxId, Vector3PositionPacked start, sbyte upgradeId, FloatUnsignedPacked scale);
		[RpcCommand("CFXM")]
		void CreateFxResponse_Missile(int effectId, int castorId, EffectType fxId, Vector3PositionPacked start, Vector3PositionPacked dest, FloatUnsignedPacked speed, sbyte upgradeId, FloatUnsignedPacked scale);

		[RpcCommand("CFXSM")]
		void CreateFxResponse_SearchingMissile(int instId, int castorId, EffectType fxId, FloatUnsignedPacked speed, Vector3PositionPacked start, Vector3PositionPacked dest, int targetId, sbyte upgradeId, FloatUnsignedPacked scale, float angle, float angleScope);
		[RpcCommand]
		void CreateFxResponse_ShowSelector(bool show, int angle, float distMin, float distMax);
		[RpcCommand("DFX")]
		void DestroyFxResponse(int effectId, Vector3PositionPacked pos, bool explosion);

		[RpcCommand]
		void ElementStrikeFx_HitResponse(int strikeid, Vector3 pos, int nextTargetId);

		[RpcCommand]
		void CreateObjectResponse(int Id);
		[RpcCommand]
		void DestroyObjectResponse(int id, bool isLiveObject);
		[RpcCommand]
		void ActivateObjectResponse(EntityType eType, int targetId, BuildingStatusType enable);
		[RpcCommand]
		void PlayAnimationResponse(string animationName);

		[RpcCommand]
		void ActivateFireFxResponse(int mapObjectId, int FireCount);
		[RpcCommand("SCR")]
		void StatContainerResponse(int personId, Dictionary<ushort, double> statsForClientInt, Dictionary<BuffString, double> statsForClientString, bool full);
		[RpcCommand("SCR")]
		void StatContainerResponse_Beh(Dictionary<ushort, double> stats, Dictionary<BuffString, double> stringsStats, bool full);
		[RpcCommand]
		void XmlDataResponse(DataHashController.DataHashType type, bool isHashEqual, string personXmlAsString);
		[RpcCommand]
		void AbilityXmlDataResponse(bool isHashEqual, string personXmlAsString);
		[RpcCommand]
		void TalentsXmlDataResponse(bool isHashEqual, string talentsXmlAsString);

		[RpcCommand]
		void PerksXmlDataResponse(bool isHashEqual, string talentsXmlAsString);

		[RpcCommand]
		void CreditRatingXmlDataResponse(bool isHashEqual, string xmlAsString);
		[RpcCommand]
		void SocialXmlDataResponse(bool isHashEqual, string xmlAsString);

		[RpcCommand]
		void StringListXmlDataResponse(bool isHashEqual, string xmlAsString);

		#region Инвентарь
		[RpcCommand]
		void ItemsDataStorageResponse(string currentHash, List<InventoryItemData> items);
		[RpcCommand]
		void UseInventoryItemResponse(bool success, int itemId, int oldItemId, Slots place, Slots oldSlot);
		[RpcCommand]
		void UpdateEntityPartResponse(int personId, List<EntityPartInfo> parts);
		[RpcCommand]
		void UpdateWeaponResponse(List<EntityPartInfo> parts);
		#endregion

		#region Новый Инвентарь
		[RpcCommand]
		void ApplyItemResponse(EntityType eType, int iId, string elixireTechName, uint elixireColor);
		[RpcCommand]
		void LocationPotionDataResponse(List<InventoryItemData> equippedPotionDataList, bool havePotionToEquip);
		[RpcCommand]
		void GetBackpackItemsResponse(List<InventoryItemData> equippedItems, List<InventoryItemData> backpackItems);
		[RpcCommand]
		void ChangeItem(int personId, List<InventoryItemData> items);
		#endregion

		[RpcCommand]
		void GetCurrencyResponse(int gold, int emeralds, int eventCurrency);
		[RpcCommand]
		void UpdateResourcesResponse(List<ResourceCount> resourcesCount);
		[RpcCommand]
		void GloryPointResponse(int gloryPoint);

		#region Абилки
		[RpcCommand("ABSR")]
		void AbilityButtonStateResponse(Dictionary<byte, AbilityErrors> buttonsAvalability);
		[RpcCommand("SAPR")]
		void ShowAbilityButtonProgressResponse(byte slotId, FloatUnsignedPacked cooldown);
		[RpcCommand]
		void AbilitiesStateCheatOnlyResponse(int slotId, AbilityErrors buttonState);
		[RpcCommand]
		void ChangeEquippedAbilityResponse(Dictionary<int, AbilityType> equippedSkillsId);
		#endregion

		#region Улучшение предметов
		[RpcCommand]
		void ItemUpgradeInfoResponse(InventoryItemData item, int upgradeItemCost);
		[RpcCommand]
		void ItemUpgradeResponse(InventoryItemData item);
		#endregion

		#region Кристалл Passive Ability
		[RpcCommand]
		void SetSkillPointResponse(List<TalentItem> resultList);
		[RpcCommand]
		void ChangeTalentsBuildResponse(int activeTalentBuild);
		[RpcCommand]
		void ResetSkillPointsResponse(bool success);
		#endregion

		#region Маяк
		[RpcCommand]
		void ActivateTeleportResponse(bool success, float timeSec);
		[RpcCommand]
		void FortPortalCloseResponse();
		#endregion

		#region Портал
		[RpcCommand]
		void LoadLevelResponse(ClientErrors errors, string jsonName, string sIP, int port, string ticket, PlayerServerStatus playerServerStatus);
		[RpcCommand]
		void StartTeleportation();
		[RpcCommand]
		void ApplyTeleportation(int Id, Vector3 position);
		[RpcCommand]
		void EndTeleportation();
		#endregion

		#region Чат


		[RpcCommand]
		void SendChatMessageResponse(int channelId, List<MessageContainer> messages);
		[RpcCommand]
		void SendChatLastMessageResponse(int channelId, List<MessageContainer> messages);
		[RpcCommand]
        void ReplaceChatMessageRequest(int channelId, MessageContainer message);
        [RpcCommand]
        void GetChannelsDataResponse(ChannelsData channelsData);

		[RpcCommand]
		void AddFriendInfoResponse(bool success, FriendItemData offersData, FriendFormSectionType loginId);
		[RpcCommand]
		void RemoveFriendInfoResponse(bool success, int arg2, FriendFormSectionType loginId);
		[RpcCommand]
		void JoinGroupChatResponse(bool success);
		[RpcCommand]
		void BlockPersonChatResponse(bool success);
		#endregion

		#region Группа
		[RpcCommand]
		void InviteGroupResponse();
		[RpcCommand]
		void GetInviteGroupResponse(string personName, int personId, int loginId);
		#endregion

		#region Друзья / Форма общения
		[RpcCommand]
		void GetFriendsResponse(List<FriendItemData> players, List<FriendItemData> friends, List<FriendItemData> guild);
		[RpcCommand]
		void SendGiftResponse(int giftTimeLeftSec, int loginId);
		[RpcCommand]
		void SendSocialGiftResponse(bool success, string externId);
		[RpcCommand]
		void NotifyFriendResponse(FriendSection section, bool show);
		[RpcCommand]
		void SendAllGiftsResponse(int giftSec);
		#endregion

		#region Торговый Непись
		[RpcCommand]
		void SellItemsResponse(bool success);
		[RpcCommand]
		void BuyItemsResponse(BuyItemResponseType responceType, List<InventoryItemData> rewardItems, int actionType, int vipPoints, int level, string titleId);
		[RpcCommand]
		void ErrorDataResponse(ErrorData error);
		[RpcCommand]
		void UpdateEntityTitleResponse(EntityType eType, int targetId, int count);
		#endregion

		#region Сундуки

		[RpcCommand]
		void GetChestAwardResponse(int targetId, bool isOpen, ChestColor color);
		[RpcCommand]
		void CloseChestResponse(int targetId);

		#endregion

		#region Магазин
		[RpcCommand]
		void GetShopItemsResponse(bool sale, List<ShopItemData> shopItemDatas);

		[RpcCommand]
		void GetProductItemsResponse(ShopData data);

		#endregion

		#region Квесты
		[RpcCommand]
		void GetQuestsResponse(DailyQuestCommonData data);
		[RpcCommand]
		void RefreshQuestResponse(int iQuestRefreshId, QuestItemData quest);
		[RpcCommand]
		void TakeQuestRewardResponse(QuestItemData data);
		[RpcCommand]
		void TakeDailyRewardResponse(int iQuestDailyReward);
		[RpcCommand]
		void GetQuestDescResponse(QuestItemData data);
		[RpcCommand]
		void NotifyQuestResponse(bool needNotify, bool showNotify);
		[RpcCommand]
		void ShowNotificationResponse(NotifyState notifyState, int notifyId, string notifyName, List<string> rpcParams);
		#endregion

		//Инстанс-Клиент SynchroBehaviour
		[RpcCommand("CTR")]
		void ChangeTransformResponse(ChangeTransformDataList<ChangeTransformData> data);
		[RpcCommand("FCTR")]
		void ChangeTransformResponseForce(ChangeTransformDataList<ChangeTransformForceData> dataForce);
		[RpcCommand]
		void PlayStateResponse(int animState);
		[RpcCommand]
		void StartMoveResponse(bool move, Vector3 movePos);
		[RpcCommand]
		void AttackResponse(Vector2 direction);
		[RpcCommand]
		void AutoAttackDamageResponse(int targetId);
		[RpcCommand("HP")]
		void SetHealth(int health, int maxHealth);
		[RpcCommand("MP")]
		void SetEnergy(ushort energy, ushort maxEnergy);
		[RpcCommand("ST")]
		void SetStamina(int stamina, int maxStamina);
        [RpcCommand]
        void SetTeamColor(int color);
        [RpcCommand]
		void UpdateHitTimeResponse(float period);
		[RpcCommand]
		void ColliderEnable(bool enable);
		[RpcCommand]
		void SetAnimatorParameterFloat(string parameterName, float newValue);
		[RpcCommand]
		void SetAnimatorParameterInt(string parameterName, int newValue);
		[RpcCommand("SAPB")]
		void SetAnimatorParameterBool(string parameterName, bool newValue);
		[RpcCommand("SAPT")]
		void SetAnimatorParameterTrigger(string parameterName);
		[RpcCommand]
		void JinnTeleport(Vector3 pos);
		[RpcCommand("SDA")]
		void SetDamageAnimation(bool isCritical);

		[RpcCommand]
		void DeathResponse(float resurrectSec);
		[RpcCommand]
		void ResurrectResponse(Vector3 position, Vector2 direction, byte sessionId);

		[RpcCommand]
		void ChangeSpeedResponse(float speed);
		[RpcCommand("PSR")]
		void PlaySoundResponse(EntityType targetType, int targetId, SoundType sound);

		[RpcCommand]
		void AbilityEndResponse(AbilityType ability, int instId, bool isCastComplete);

		[RpcCommand]
		void SearchTargetResponse(Vector2 direction);
		[RpcCommand]
		void SetGameModeResponse(GameModeType gameMode, ActualPlaceTitle actualPlace);

		[RpcCommand]
		void SetUpTransparencyResponse(float alpha);

		[RpcCommand]
		void CageFx_CreateResponse(int instId, List<CageStoneData> stones, List<CageWallData> walls);

		[RpcCommand]
		void SquirrelBondsFxResponse(int instanceId, int upgradeId);
		[RpcCommand]
		void SquirrelStarColorFxResponse(int instanceId, int upgradeId);

		[RpcCommand]
		void ClientConstantsResponse(ClientConstants clientConstants);

		[RpcCommand]
		void ThrowFxResponse(int bombInstanceId, Vector3 position, float speed, bool bTrue);

		#region Upgrade Building
		[RpcCommand]
		void StartBuildingResponse(bool success);
		#endregion

		#region Почта
		[RpcCommand]
		void NotifyMailResponse(int mailCount);
		[RpcCommand]
		void ItemSendToMailResponse();
		[RpcCommand]
		void TakeGiftResponse(Errors errors);
		[RpcCommand]
		void TakeMailItemResponse(bool success, int mailId);
		[RpcCommand]
		void AdministrationGiftResponse();
		#endregion

		#region Статусы
		[RpcCommand]
		void SetMoveMode(bool enable);
		[RpcCommand]
		void SetPassableMode(bool enable);
		[RpcCommand]
		void SetMotionLimits(bool stunStatus, bool rootStatus);
		[RpcCommand]
		void SetRadius(float radius);

		[RpcCommand]
		void ShowStatusResponse(List<StatusData> list);
		#endregion

		#region Map and Minimap
		[RpcCommand("MMI")]
		void GetMinimapItemsResponse(List<MinimapData> mmData, PlayerTeam teamColor);
		[RpcCommand("MMIA")]
		void MinimapItemAdd(MinimapData mmData);
		[RpcCommand("MMIR")]
		void MinimapItemRemove(MinimapData mmData);
		#endregion

		#region Аналитика
		[RpcCommand]
		void DevToDevEventResponse(string eventName, AnalyticsDestination analyticsDestination, Dictionary<string, string> param, Dictionary<string, string> paramAdd);
		[RpcCommand]
		void AppsFlyerEventResponse(string arg1, Dictionary<string, string> arg2);
		[RpcCommand]
		void DevToDevCustomPropertyResponse(string propName, string propValue);
		#endregion

		#region tutorial
		[RpcCommand]
		void StartTutorialResponse(TutorialStage stage);
		[RpcCommand]
		void TutorialEventResponse(TutorialEvents tutorEvent);
		[RpcCommand]
		void TutorialAddFrameResponse(EntityType eType, int synId, TutorialFrameType type, int id);
		[RpcCommand]
		void TutorialRemoveFrameResponse(int targetId);
		[RpcCommand]
		void TutorialApproachToGolemResponse();
		[RpcCommand]
		void AddEntityBehaviourResponse(EntityType targetType, int targetId, BehaviourType bType);
		[RpcCommand]
		void TutorialGizmoShowBagResponse(int targetId, bool bShow);
		[RpcCommand]
		void TutorialCameraTargetResponse(List<Vector3> position);
		[RpcCommand]
		void TutorialAnalyticsStepResponse(int stepId);
		[RpcCommand]
		void TutorialStateResponse(List<MiniTutorial> states);
		#endregion

		[RpcCommand]
		void UpdateMapTransparencyResponse(int xMin, int zMin, int Width, int Height, bool transparent);
		[RpcCommand]
		void UpdateMapTransparencyPointsResponse(List<Vector2i> points, bool transparent);

		#region Dungeon
		[RpcCommand]
		void DungeonOpenResponse(bool isSuccess);
		#endregion

		[RpcCommand("SAS")]
		void StatusApplyStatistics(bool isActivateComplete, bool isChanceComplete);
		[RpcCommand("SASP")]
		void StatusApplyStatisticsOnPlayer(bool isActivateComplete, bool isChanceComplete);
		[RpcCommand("DDI")]
		void DPSDebugInfo(int targetId, int value, TextFrameType frameType, int attackerForEffectId, Elements elementType, DamageType damageType, float elementChange, float armorChange, float armorElementChange, float resultChange);

		[RpcCommand("ACF")]
		void AddCritFrameResponse(EntityType targetType, int targetId, int value, TextFrameType frameType, int attackerForEffectId, Elements elementType, float countingTime);
		[RpcCommand]
		void MakePlayerMortalResponse();

		[RpcCommand]
		void EnterToTeleportResultResponse(TeleportResult teleportResult, float timer);

		[RpcCommand]
		void ShowMessageBox(ServerMessage message);
		[RpcCommand]
		void ShowDebugMessageBox(string str);
		[RpcCommand]
		void PlayerDeathInfoResponse(PlayerDeathData playerDeathData);
		[RpcCommand]
		void PlayerBattlegroundDeathInfoResponse(double timeInSeconds, int attackerId, EntityType attakerType);

		[RpcCommand]
		void AddMarkerResponse(int targetId, TargetIcon targetType);
		[RpcCommand]
		void AddListMarkersResponse(List<int> targetId, TargetIcon targetType);
		[RpcCommand]
		void RemoveMarkerResponse(List<int> targetsListId);

		#region гильдии
		[RpcCommand]
		void ClanListResponse(List<GuildData> guilds);
		[RpcCommand]
		void ClanCreateResponse(ClientErrors error);
		[RpcCommand]
		void ClanDataResponse(GuildData data, ClanRole clanRole, int bestSummaryRating, byte lastLeaveDaysLeft);
		[RpcCommand]
		void ClanSaveChangesResponse(ClientErrors error);
		[RpcCommand]
		void ClanMemberListResponse(int clanId, List<GuildPersonInfo> clanMemberList);
		[RpcCommand]
		void ClanCheckNameResponse(ClientErrors error, byte dayCooldown);
		[RpcCommand]
		void ClanJoinResponse(ClientErrors error);
		[RpcCommand]
		void ClanMemberRoleChangeResponse(ClientErrors error);
		[RpcCommand]
		void ClanMemberMakeOwnerResponse(ClientErrors error);
		[RpcCommand]
		void ClanMemberKickResponse(ClientErrors error);

		[RpcCommand]
		void GuildCurrentJoinResponse(List<int> joins);
		[RpcCommand]
		void GuildJoinListResponse(ClientErrors error, List<GuildJoin> joins);
		[RpcCommand]
		void GuildSetJoinStatusResponse(ClientErrors error);
		[RpcCommand]
		void GuildInviteResponse(ClientErrors error);

		[RpcCommand]
		void UpdateLocationClanData(ShortClanData guildData);
		[RpcCommand]
		void ResetLocationClanData();
		#endregion

		[RpcCommand]
		void UpdateHintFrame(int mapTargetId);

		[RpcCommand]
		void PlayVideoClipResponse(Vector3 pos, Vector3 dir, float h);

		[RpcCommand("SAR")]
		void StartAbilityResponse(CastAbilityData castData);
		[RpcCommand("AAC")]
		void ApplyAbilityCast(int behaviourId, AbilityType type, Vector2 spellDirectionCorrected);
		[RpcCommand]
		void LikeApp(string url);

		[RpcCommand]
		void ShowActionBotTextResponse(int effectFrameInfoId, int actionBotText, int ActionBotPicture, int smileId);
		[RpcCommand]
		void SpecialActionResponse(SpecialRpcData specialRpcData);


		[RpcCommand]
		void SetRayTargetResponse(int instanceId, Vector3 target);

		#region Emotion

		// Статуя настроения (покупка и выбор смайлов)
		[RpcCommand]
		void EmotionsInfoResponse(List<EmotionData> avalibleEmotions, List<EmotionData> buyList, EmotionPackData packData);
		[RpcCommand]
		void EquipEmotionResponse(ClientErrors success, EmotionData emotionData);
		#endregion

		[RpcCommand]
		void LoginKick(KickReason reason);

		#region CheatPanel
		[RpcCommand]
		void CheatPanelDataResponse(string data);
		#endregion

		[RpcCommand("ArenaDataResponse")] //TODO_Deg
		void LoadScreenResponse(ArenaMatchType matchType);
		[RpcCommand]
		void ArenaMatchReadyResponse(ArenaMatchType matchType, float v);

		[RpcCommand]
		void SetExperienceResponse(int level, int exp, int requimentExp, bool zh, int vipLevel);
		[RpcCommand]
		void ArenaExperieneResponse(PersonExperienceData data);

		[RpcCommand]
		void GetRewardResponse(ArenaRewardChestType chestType, List<ArenaRewardData> rewardList);
		[RpcCommand]
		void GetCheatRewardResponse(ArenaRewardChestType chestType, List<ArenaRewardData> rewardList);

		[RpcCommand]
		void MultiKillResponse(int series);
		[RpcCommand]
		void MultiKillDurationResponse(int SeriesMaxTimeSec);

		[RpcCommand]
		void PickUpBuffResponse(string technicalName);

		[RpcCommand("SHPH")]
		void ShiningPathHide();
		[RpcCommand("SHPU")]
		void ShiningPathUpdate(byte[] pointsToRemove, List<ShiningPathPoint> pointsToAdd);

		#region Lobby

		[RpcCommand]
		void GetAccountInfoResponse(LobbyInfo.PlayerLoginType type);

		[RpcCommand]
		void GetLobbyInfoResponse(LobbyInfo loginInfo);

		[RpcCommand]
		void TutorialRewardResponse(List<InventoryItemData> reward, string description, Dictionary<string, string> param);
		[RpcCommand]
		void GetBonusSourceResponse(List<BonusSourceData> bonus);

		[RpcCommand]
		void SelectAccountResponse(bool success);

		[RpcCommand]
		void CreatePersonResponse(bool success, LobbyPersonData lobbyPersonData);

		[RpcCommand]
		void GiveRandomNameResponse(int slotId, string lastRandomName);

		[RpcCommand]
		void SetLoginNameResponse(ChangeNameResult res, bool state);

		[RpcCommand]
		void TimeToUnholdNameResponse(int timeSec);

		[RpcCommand]
		void RemovePersonResponse(LobbyInfo loginInfo);

		[RpcCommand]
		void ChangePersonResponse(int personId);

		[RpcCommand]
		void ProductListResponse(Dictionary<string, int> skuData);

		[RpcCommand]
		void GetLobbyBuildAllDataResponse(LobbyBuildingStorageData lobbyBuildingStorageData);

		[RpcCommand]
		void BuyBuildingResponse(LobbyBuildingAccountData data);
		[RpcCommand]
		void SetBuildingResponse(LobbyBuildingAccountData data);
		[RpcCommand]
		void RemoveBuildingResponse(LobbyBuildingAccountData data);
		[RpcCommand]
		void BuyBuildingKitResponse(List<LobbyBuildingAccountData> data);
		[RpcCommand]
		void SetBuildingKitResponse(List<LobbyBuildingAccountData> data);
		[RpcCommand]
		void RemoveBuildingKitResponse(List<LobbyBuildingAccountData> data);
		[RpcCommand]
		void ChangeAvatarNameResponse(bool success, string avatarName);

		#endregion


		[RpcCommand]
		void TDMDataResponse(TeamDeathmatchData amd);
		[RpcCommand]
		void BrawlDataResponse(BrawlMatchData amd);
		[RpcCommand]
		void CTFDataResponse(TeamDeathmatchData amd);
		[RpcCommand]
		void DMDataResponse(ArenaMatchData amd);
		[RpcCommand]
		void TDMResultDataResponse(TeamDeathmatchData amd);
		[RpcCommand]
		void BrawlResultDataResponse(BrawlMatchData amd);
		[RpcCommand]
		void CTFResultDataResponse(TeamDeathmatchData amd);
		[RpcCommand]
		void DMResultDataResponse(ArenaMatchData amd);
		[RpcCommand]
		void WavesDataResponse(WavesMatchData wcmd);
		[RpcCommand]
		void WavesResultDataResponse(WavesResultMatchData amd);

		[RpcCommand]
		void GetSelectMapResponse(SelectMapData selectMapData);
		[RpcCommand]
		void CreatePersonTypesResponse(Dictionary<PersonType, bool> personTypes);
		[RpcCommand]
		void NotificationsResponse(List<string> loginNotifications, Dictionary<int, List<string>> personNotifications);

		[RpcCommand]
		void UsePromoCodeResponse(PromoCodeState state, List<InventoryItemData> reward);

		[RpcCommand]
		void LobbySilverChestInfoResponse(int timeSec);
		[RpcCommand]
		void LobbyGoldenChestInfoResponse(int timeSec, int currentVictories, int targetVictories);
		[RpcCommand]
		void LobbyChestRewardResponse(List<InventoryItemData> itemList);
		[RpcCommand]
		void CtfFlagFxStartBlinkResponse(int fxId);
		[RpcCommand]
		void CtfFlagProgressBarResponse(bool isOn, float timer);

		#region SpecialEvent
		[RpcCommand]
		void SpecialEventDataResponse(SpecialEventData data);
		[RpcCommand]
		void TakeSpecialEventRewardResponse(int iReward);
		#endregion

		#region ProgressLine
		[RpcCommand]
		void ProgressLineXmlDataResponse(bool isHashEqual, string xmlAsString);
		[RpcCommand]
		void TakeProgressLineRewardResponse(bool success, string rewardId, List<InventoryItemData> itemList);
		[RpcCommand]
		void ProgressLineChestDetailsResponse(bool result, List<ChestEntityDescription> alwaysContains, List<ChestEntityDescription> possibleContains);
		#endregion

		[RpcCommand]
		void LoginBindResponse(bool changed);

		#region Perks
		[RpcCommand]
		void GetUsedPerksResponse(List<UsedPerksData> usedPerksDataList);

		[RpcCommand]
		void UsePerkKitResponse(Errors errorCode, int personId, int perkKitId);

		[RpcCommand]
		void SavePerkKitResponse(Errors errorCode, int personId, int perkKitId, List<int> perkIdList, string perkKitName);

		[RpcCommand]
		void PerkPointResponse(int perkPoint, List<int> currentPerks, List<int> newPerks);

		#endregion

		[RpcCommand]
		void TechworksResponse(bool bTechworks, int timeSec);

		#region CreditRating
		[RpcCommand]
		void CreditRatingRewardResponse(bool available);
		[RpcCommand]
		void CreditRatingTakeRewardResponse(List<InventoryItemData> itemList);
		#endregion

		[RpcCommand]
		void ClientAlertAFKResponse(bool afk);
		[RpcCommand]
		void KickFromMatch(KickFromMatchReason reason);

		[RpcCommand]
		void DonateShowResponse(string iframeUrl);

		[RpcCommand]
		void UnityAdsRewardResponse(Dictionary<string, InventoryItemData> data, int nextTime, int resetTime);

		[RpcCommand]
		void ShowRateAppFormResponse(bool isFirstTime, string eventName);

		[RpcCommand]
		void ShowRateAppButtonResponse();
		[RpcCommand]
		void UnityAdsFinishedResponse(string placementId, int ResetInSeconds);

		#region Друзья
		[RpcCommand]
		void FriendListResponse(List<FriendData> friendList, List<FriendData> possibleFriendList, List<FriendData> requestFriendList);
		[RpcCommand]
		void FriendInviteResponse(int loginId, ClientErrors error);
		[RpcCommand]
		void FriendAnswerResponse(int loginId, ClientErrors error);
		[RpcCommand]
		void FriendRemoveResponse(int loginId, ClientErrors error);
		#endregion

		#region Группы
		[RpcCommand]
		void AllPlayModeDatasResponse(PlayModeType playModeType, Dictionary<PlayModeType, PlayModeData> playModeDatas);
		[RpcCommand]
		void UpdatePlayModeDataResponse(PlayModeType playModeType, PlayModeData playModeData);

		[RpcCommand]
		void ReadyStatusResponse(int partyId, int loginId, bool readyStatus);
		[RpcCommand]
		void UpdateArenaTypeResponse(int partyId, int arenaId);

		[RpcCommand]
		void PartyInviteResponse(PartyInviteData partyInviteData);
		[RpcCommand]
		void PartyInviteAcceptResponse(bool inviteResult);

		[RpcCommand]
		void AddPlayerToPartyResponse(int partyId, PartyMemberData partyMemberData);
		[RpcCommand]
		void RemovePlayerFromPartyResponse(int partyId, int loginId);

		[RpcCommand]
		void UpdatePersonCardResponse(int partyId, PersonCard personCard);
		[RpcCommand]
		void UpdateCharacterDataResponse(int partyId, int loginId, SelectedPersonData personData);

		[RpcCommand]
		void JoinPartyResultResponse(JoinPartyResult joinPartyResult);
		[RpcCommand]
		void PartyTransferResponse(PlayModeType playModeType, PlayModeData newParty);

		[RpcCommand]
		void PartyRequestResponse(PartyRequestData partyRequestData);
		[RpcCommand]
		void PartyRequestResultResponse(PartyRequestData partyRequestData);
		#endregion
	}
}
