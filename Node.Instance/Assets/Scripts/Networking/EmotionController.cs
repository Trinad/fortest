﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Analytic;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Enums;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils.Common;
using uLink;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using Zenject;

namespace Assets.Scripts.Networking
{
    public class EmotionController : BaseRpcListener, IEmotionController
    {
        IPersonsOnServer personsOnServer;
        EmotionManager emotionManager;
		ShopManager shopManager;

		public EmotionController(IPersonsOnServer personsOnServer, EmotionManager emotionManager, ShopManager shopManager, NetworkP2P networkP2P, uLink.Network network, uLink.INetworkView view) : base(networkP2P, network, view)
		{
			this.personsOnServer = personsOnServer;
			this.emotionManager = emotionManager;
			this.shopManager = shopManager;

			emotionManager.SendRemoveSmileFrameResponse += SendRemoveSmileFrameResponse;
		}

		public void CreateEmotionPerson(InstancePerson instancePerson, List<Emotion> availableEmotions, List<Emotion> availableEmotionsForBuy)
		{
			emotionManager.CreatEmotionPerson(instancePerson, availableEmotions, availableEmotionsForBuy);
		}

		[RPC]
        public void EmotionsInfoRequest(InstancePerson instancePerson)
        {
			SendEmotionsInfoResponse(instancePerson);
		}

		void SendEmotionsInfoResponse(InstancePerson instancePerson)
		{
			var avalibleEmotions = emotionManager.GetRPCAvailableEmotions(instancePerson);
			var buyList = emotionManager.GetRpcEmotionsForBuy(instancePerson).Select(e => (EmotionData)e).ToList();

			var packData = new EmotionPackData()
			{
				EmotionList = avalibleEmotions,
			};

			SendRpc(instancePerson.ClientRpc.EmotionsInfoResponse, avalibleEmotions, buyList, packData);
		}

		[RPC]
        public void UseEmotionRequest(InstancePerson instancePerson, int emotionId)
        {
			if(emotionManager.UseEmotion(instancePerson, emotionId))
				SendAddSmileFrameResponse(instancePerson);
		}

        [RPC]
        public void EquipEmotionRequest(InstancePerson instancePerson, int emotionId, int slotEmotionId)
        {
			if(instancePerson.EmotionPerson.PutInSlot(emotionId, slotEmotionId))
				SendEmotionsInfoResponse(instancePerson);

			var emotionData = instancePerson.EmotionPerson.GetEmotionData(emotionId);
			SendRpc(instancePerson.ClientRpc.EquipEmotionResponse, ClientErrors.SUCCESS, emotionData);
		}
        
        [RPC]
        public void BuyEmotionRequest(InstancePerson instancePerson, int emotionId)
        {
            BuyEmotion(instancePerson, emotionId);
        }

		#region Cheats
		public void CheatsRefreshEmotions(InstancePerson instancePerson)
		{
			emotionManager.RefreshEmotions(instancePerson);
		}

		public void CheatDeleteAllEmitions(InstancePerson instancePerson)
		{
			emotionManager.CheatDeleteAllEmitions(instancePerson);
			SendEmotionsInfoResponse(instancePerson);
		}
		#endregion

		public void EquipEmotionResponse(InstancePerson instancePerson, ClientErrors success, EmotionData emotionData)
		{
			SendRpc(instancePerson.ClientRpc.EquipEmotionResponse, success, emotionData);
		}

		public void TutorialAddFrameResponse(InstancePerson instancePerson, TutorialFrameType smiley, int id)
		{
			if (instancePerson.syn == null)
				return;

			SendRpc(instancePerson.syn.map.ClientRpc.TutorialAddFrameResponse, instancePerson.syn.eType, instancePerson.syn.Id, TutorialFrameType.Smiley, id);
		}

		public void ErrorDataResponse(InstancePerson instancePerson, ErrorData getError)
		{
			SendRpc(instancePerson.ClientRpc.ErrorDataResponse, getError);
		}



		public void BuyEmotion(InstancePerson instancePerson, int emotionId)
		{
			var product = GetProductItemData(instancePerson, emotionId);
			if (product.InApp)
				return;

			if (instancePerson.EmotionPerson.InAvailableEmotionsList(product.BaseId))
			{
				ILogger.Instance.Send("BuyEmotionRequest InAvailableEmotionsList id: " + product.BaseId, ErrorLevel.error);
				return;
			}

			shopManager.BuyForEmeralds(instancePerson, (int)product.Price, AccountChangeReason.BuyShopItem, data =>
			{
				if (data.success)
				{
					emotionManager.AddNewEmotion(instancePerson, product.BaseId);
					SendEmotionsInfoResponse(instancePerson);

					var analyticInfo = new AnalyticCurrencyInfo()
					{
						needSendInWallet = false,
						sendGain = false,
						Amount = data.summ,
						Destination = SinkType.Unknown.ToString(),
						DestinationType = "Emotions",
						Currency = ResourceType.Emerald,
						USDCost = 0f,
						PersonLevel = instancePerson.login.GetMaxPersonLevel(),
						TestPurchase = instancePerson.login.settings.testPurchase
					};

					AnalyticsManager.Instance.SendData(instancePerson, analyticInfo);

					ILogger.Instance.Send("BuyEmotionRequest success AddNewEmotion id: " + product.BaseId, ErrorLevel.info);
				}
				else
				{
					var resData = instancePerson.GetNeedForResource();
					resData.NeedResource(ResourceType.Emerald, (int)product.Price - instancePerson.Emeralds);

					ErrorDataResponse(instancePerson, resData.GetError(instancePerson));

					ILogger.Instance.Send("BuyEmotionRequest fail NeedResource id: " + product.BaseId, ErrorLevel.info);
				}
			});
		}

		ProductItemData GetProductItemData(InstancePerson instancePerson, int itemId)
		{
			var items = shopManager.GetShopItems(instancePerson.login, 6);
			return items.Find(x => x.BaseId == itemId);
		}

		public void SendAddSmileFrameResponse(InstancePerson instancePerson)
		{
			if (InstancePersonFail(instancePerson))
			{
				ILogger.Instance.Send("SendAddSmileFrameResponse InstancePersonFail", ErrorLevel.error);
				return;
			}

			var data = instancePerson.EmotionPerson.InUse;
			if (data != null)
			{
				TutorialAddFrameResponse(instancePerson, TutorialFrameType.Smiley, data.Id);
			}
			else
			{
				ILogger.Instance.Send("UseEmotionRequest GetEmotionData == null",
					string.Format("instancePerson loginId: {0}, personId: {1}, name: {2}", instancePerson.loginId, instancePerson.personId, instancePerson.personName), ErrorLevel.error);
			}
		}

		public void SendRemoveSmileFrameResponse(InstancePerson instancePerson)
		{
			if (InstancePersonFail(instancePerson))
			{
				ILogger.Instance.Send("SendRemoveSmileFrameResponse InstancePersonFail", ErrorLevel.error);
				return;
			}
			var data = instancePerson.EmotionPerson.InUse;
			if (data != null)
			{
				if (instancePerson.syn != null)
					SendRpc(instancePerson.syn.map.ClientRpc.TutorialRemoveFrameResponse, instancePerson.syn.Id);
			}
			else
			{
				ILogger.Instance.Send("UseEmotionRequest GetEmotionData == null",
					string.Format("instancePerson loginId: {0}, personId: {1}, name: {2}", instancePerson.loginId, instancePerson.personId, instancePerson.personName), ErrorLevel.error);
			}
		}

		bool InstancePersonFail(InstancePerson instancePerson)
		{
			if (instancePerson == null || instancePerson.player.Equals(uLink.NetworkPlayer.unassigned))
			{
				ILogger.Instance.Send("InstancePersonFail: ", string.Format("personId: {0} ", instancePerson != null ? instancePerson.personId : 0), ErrorLevel.error);
				return true;
			}

			return false;
		}
	}

	public interface IEmotionController
    {
		void CreateEmotionPerson(InstancePerson instancePerson, List<Emotion> availableEmotions, List<Emotion> availableEmotionsForBuy);
		void EmotionsInfoRequest(InstancePerson instancePerson);
		void UseEmotionRequest(InstancePerson instancePerson, int emotionId);
		void BuyEmotionRequest(InstancePerson instancePerson, int emotionId);
		void CheatsRefreshEmotions(InstancePerson instancePerson);
		void CheatDeleteAllEmitions(InstancePerson instancePerson);


		void EquipEmotionResponse(InstancePerson instancePerson, ClientErrors success, EmotionData emotionData);
		void TutorialAddFrameResponse(InstancePerson instancePerson, TutorialFrameType smiley, int id);
		void ErrorDataResponse(InstancePerson instancePerson, ErrorData getError);
		void SendAddSmileFrameResponse(InstancePerson instancePerson);
		void SendRemoveSmileFrameResponse(InstancePerson instancePerson);
	}
}
