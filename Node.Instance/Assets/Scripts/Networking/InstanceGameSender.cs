﻿using uLink;
using UtilsLib.Protocol;

namespace Assets.Scripts.Networking
{
    public interface IInstanceGameSender
    {
        void InitLoginListAnswer(int gameId);
        void AddPersonToGameConfirm(int gameId, int loginId, string loginName);
        void SendStopPersonalMap(int gameId);
        void NotifyLobbyAboutInstanceClear();
        void SendStopGameInfo();
        void PingDispatcher();
    }

    public class InstanceGameSender : BaseInstanceRpcListener, IInstanceGameSender
    {
		public InstanceGameSender(NetworkP2P networkP2P, uLink.Network network, uLink.INetworkView view) : base(networkP2P, network, view)
		{
		}

		public void InitLoginListAnswer(int gameId)
        {
			SendRpc(dispatcher.OnInitLoginListAnswer, gameId);
        }

        public void AddPersonToGameConfirm(int gameId, int loginId, string loginName)
        {
            SendRpc(dispatcher.OnConfirmAddPersonToGame, gameId, loginId, loginName);
        }

        public void SendStopPersonalMap(int gameId)
        {
            SendRpc(dispatcher.OnStopPersonalMap, gameId);
            ILogger.Instance.Send($"StopPersonalMapRequest gameId {gameId}");
        }

        public void NotifyLobbyAboutInstanceClear()
        {
            SendRpc(dispatcher.OnInstanceClear, ServerStartGameManager.ticket);
        }

        public void SendStopGameInfo()
        {
            SendRpc(dispatcher.OnInstanceStopping, ServerStartGameManager.ticket);
        }

        public void PingDispatcher()
        {
            SendRpc(dispatcher.OnInstancePingDispathcer, ServerStartGameManager.ticket);
        }
    }
}
