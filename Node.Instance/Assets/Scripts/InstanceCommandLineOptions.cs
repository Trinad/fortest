﻿using CommandLine;

namespace Node.Instance.Assets.Scripts
{
    public sealed class InstanceCommandLineOptions
    {
		[Option]
		public string Ticket { get; set; }

		[Option("level")]
		public string LevelName { get; set; }

		[Option("ver")]
		public string LobbyVersion { get; set; }

		[Option("host")]
		public string p2pLobbyConnect_host { get; set; }

		[Option("port")]
		public int p2pLobbyConnect_port { get; set; }

		[Option("start_port")]
		public int ListenPortStartRange { get; set; }

		[Option("end_port")]
		public int ListenPortEndRange { get; set; }

		public override string ToString()
		{
			return $"Ticket: {Ticket ?? ""}; level: {LevelName ?? ""}; ver: {LobbyVersion ?? ""}; " +
				   $"host: {p2pLobbyConnect_host ?? ""}; port: {p2pLobbyConnect_port}; " +
				   $"start_port: {ListenPortStartRange}; end_port: {ListenPortEndRange}";
		}
	}
}
