﻿using System;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Login;

namespace Assets.Scripts.Utils.Db
{
	public interface IInstanceSqlAdapter
	{
		void CreatePerson(LobbyLogin login, InstancePersonSqlData person, InstanceSqlAdapter.PersonCreateOnSuccess onSuccess, Action<LobbyLogin, Exception> onError);
		void FindLogin(string loginName, Action<int> onSuccess, Action onError);
		void GetLikeLoginName(string name, Action<List<string>> onSuccess, Action<Exception> onError);
		void LoadLogin(int loginId, Action<LobbyLogin, Dictionary<int, int>> onSuccess, Action<int, MsSqlLoadResult, Exception> onError);
		void LoadPersons(List<int> personIds, Action<List<InstancePersonSqlData>> onSuccess, Action<MsSqlLoadResult, Exception> onError);
		void RemovePerson(int personId, Action<int> onSuccess, Action<int, MsSqlSaveResult, Exception> onError);
		void SaveBugReport(int loginId, int slotId, string message, Action onSuccess, Action<Exception> onError);
		void SaveLogin(LobbyLogin login, Action<int> onSuccess, Action<int, MsSqlSaveResult, Exception> onError);
		void SavePerson(InstancePersonSqlData personSqlData, Action<InstancePersonSqlData> onSuccess, Action<InstancePersonSqlData, MsSqlSaveResult, Exception> onError);
		void SetLoginName(string name, int loginId, Action onSuccess, Action<Exception> onError);
	}
}
