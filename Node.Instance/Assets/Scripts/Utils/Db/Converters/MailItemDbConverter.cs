﻿using System.Collections.Generic;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.DegSerializers;

namespace Assets.Scripts.Utils.Db.Converters
{
	internal sealed class MailItemDbConverter : IDbConverter<MailItemData>
	{
		private readonly MailDbConverter mailDbConverter;

		public MailItemDbConverter()
		{
			mailDbConverter = new MailDbConverter();
		}

		public Dictionary<string, object> ToSqlDictionary(MailItemData source)
		{
			return mailDbConverter.ToSqlDictionary(source);
		}

		public MailItemData FromDictionary(Dictionary<string, object> newItems)
		{
			var mailData = mailDbConverter.FromDictionary(newItems);
			var mailItemData = mailData.CopyTo<MailItemData>();
			return mailItemData;
		}
	}
}
