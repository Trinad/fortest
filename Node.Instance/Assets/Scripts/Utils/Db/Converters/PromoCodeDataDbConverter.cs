﻿using Assets.Scripts.Networking.ClientDataRPC;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using UtilsLib.DegSerializers;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.Utils.Db
{
    public class PromoCodeDataDbConverter : IDbConverter<PromoCodeDBClass>
    {
        public PromoCodeDBClass FromDictionary(Dictionary<string, object> source)
        {
            var ansver = new PromoCodeDBClass();

            var result = -1;
            Int32.TryParse(source["result"].ToString(), out result);
            ansver.result = result;

            return ansver;
        }

        public Dictionary<string, object> ToSqlDictionary(PromoCodeDBClass source)
        {
            var result = new Dictionary<string, object>();
            result.Add("@LoginId", source.loginId);
            result.Add("@PromoCode", source.promoCode);
            return result;
        }
    }

}
