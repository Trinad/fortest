﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Utils.Db
{
    public class FindPersonDbConverter : IDbConverter<FindPersonDbConverter.FindPersonData>
    {
	    public class FindPersonData
	    {
		    public int LoginId { get; private set; }
		    public int PersonId { get; private set; }

		    public FindPersonData(int loginId, int personId)
		    {
			    LoginId = loginId;
			    PersonId = personId;
		    }
	    }

	    public Dictionary<string, object> ToSqlDictionary(FindPersonData source)
        {
            throw new NotImplementedException();
        }

        public FindPersonData FromDictionary(Dictionary<string, object> source)
        {
            return new FindPersonData(Convert.ToInt32(source["LoginId"]), Convert.ToInt32(source["PersonId"]));
        }
    }
}
