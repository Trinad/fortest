﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Login;
using Batyi.TinyServer;
using UtilsLib;
using UtilsLib.corelite;
using UtilsLib.DegSerializers;
using UtilsLib.Logic;
using UtilsLib.NetworkingData;

namespace Assets.Scripts.Utils.Db
{
	public sealed class InstanceSqlAdapter : QueueThreadsMSSQLAdapter, IInstanceSqlAdapter
	{
		public InstanceSqlAdapter(string sConnectString, string name, bool logStateInfo = false) 
			: base(sConnectString, name, logStateInfo)
		{
		}

		#region Person
		public void LoadPersons(List<int> personIds, Action<List<InstancePersonSqlData>> onSuccess, Action<MsSqlLoadResult, Exception> onError)
		{
			ILogger.Instance.Send("MSSQLAdapter LoadPersons", ErrorLevel.trace);
			foreach (var personId in personIds)
				ILogger.Instance.Send($"LoadPersons() personId {personId}");
			
			enqueueAction(new FuncAction
			{
				name = "load_person",
				action = () => loadPerson(personIds, onSuccess, onError)
			});
		}

		private Action loadPerson(List<int> personIds, Action<List<InstancePersonSqlData>> onSuccess, Action<MsSqlLoadResult, Exception> onError)
		{
			const string sql = "InstancePersonLoad";
			List<InstancePersonSqlData> persons = new List<InstancePersonSqlData>();
			try
			{
				for (int i = 0; i < personIds.Count; i++)
				{
					using (var connection = GetOpenConnection())
					{
						try
						{
							using (SqlCommand command = connection.CreateCommand())
							{
								command.CommandType = CommandType.StoredProcedure;
								command.CommandText = sql;

								command.AddParameter("@personId", personIds[i]);

								using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
								{
									if (reader.Read())
									{
										var personData = new DBRecordCollection(reader);
                                        var personSqlData = JSON.JsonDecode(personData.GetString("data")).JsonDecode<InstancePersonSqlData>();
                                        if (personSqlData != null)
										{
                                            personSqlData.PersonId = personIds[i];
											personSqlData.SessionId = personData.GetInt("SessionId");
											personSqlData.Items2 = JSON.JsonDecode(personData.GetString("Items")).JsonDecode<List<ItemSaveData>>();
											personSqlData.ItemsPotions2 = JSON.JsonDecode(personData.GetString("ItemsPotions")).JsonDecode<List<ItemSaveData>>();
											personSqlData.ItemsEquiped2 = JSON.JsonDecode(personData.GetString("ItemsEquiped")).JsonDecode<List<ItemSaveData>>();
											personSqlData.TradeItems2 = JSON.JsonDecode(personData.GetString("TradeItems")).JsonDecode<List<string>>();
											persons.Add(personSqlData);
										}
									}
								}
							}
						}
						catch (Exception ex)
						{
							return () => onError(MsSqlLoadResult.Fail, ex);
						}
					}
				}
			}
			catch (Exception ex)
			{
				return () => onError(MsSqlLoadResult.Fail, ex);
			}
			return () => { if (onSuccess != null) onSuccess(persons); };
		}

		public void SavePerson(InstancePersonSqlData personSqlData, Action<InstancePersonSqlData> onSuccess, Action<InstancePersonSqlData, MsSqlSaveResult, Exception> onError)
		{
			ILogger.Instance.Send("MSSQLAdapter SavePerson " + personSqlData.PersonId, ErrorLevel.trace);
			enqueueAction(new FuncAction
			{
				name = "save_person",
				action = () => savePerson(personSqlData, onSuccess, onError)
			});
		}

		private Action savePerson(InstancePersonSqlData personSqlData, Action<InstancePersonSqlData> onSuccess, Action<InstancePersonSqlData, MsSqlSaveResult, Exception> onError)
		{
			const string sql = "InstancePersonSave";
			bool success = false;

			using (var connection = GetOpenConnection())
			{
				string personDataJson = null;
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@personId", personSqlData.PersonId);
						command.AddParameter("@data", personDataJson = JSON.JsonEncode(personSqlData.JsonEncode()));
						command.AddParameter("@authLastTime", TimeProvider.UTCNow);
					    command.AddParameter("@guildId", personSqlData.GuildId);
                        command.AddParameter("@Items", JSON.JsonEncode(personSqlData.Items2.JsonEncode()));
                        command.AddParameter("@ItemsPotions", JSON.JsonEncode(personSqlData.ItemsPotions2.JsonEncode()));
                        command.AddParameter("@ItemsEquiped", JSON.JsonEncode(personSqlData.ItemsEquiped2.JsonEncode()));
                        command.AddParameter("@TradeItems", JSON.JsonEncode(personSqlData.TradeItems2.JsonEncode()));
						command.AddParameter("@sessionId", personSqlData.SessionId);
						command.ExecuteNonQuery();

						using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
						{
							if (reader.Read())
							{
								var data = new DBRecordCollection(reader);
								var getLogin = data.GetInt("PersonId");
								var errorCount = data.GetInt("ErrorCount");
								var msg = data.GetString("msg");

								if (errorCount > 0)
									ILogger.Instance.Send($"InstancePersonSave():personId = {personSqlData.PersonId} trys sawes with old SessionId = {personSqlData.SessionId} msg = {msg}",  ErrorLevel.exception);

								success = true;
							}

							reader.Read(); // can throw
						}
					}
				}
				catch (Exception ex)
				{
					return () =>
					{
						ILogger.Instance.Send("InstancePersonSave: json = " + (personDataJson ?? "null"));
						onError(personSqlData, MsSqlSaveResult.Fail, ex);
					};
				}
			}

			if (!success)
				return () => onError(personSqlData, MsSqlSaveResult.Fail, null);

			return () => { if (onSuccess != null) onSuccess(personSqlData); };
		}

		#endregion

		#region Login

		public void LoadLogin(int loginId, Action<LobbyLogin, Dictionary<int, int>> onSuccess, Action<int, MsSqlLoadResult, Exception> onError)
		{
			ILogger.Instance.Send("MSSQLAdapter LoadLogin " + loginId, ErrorLevel.trace);            
			enqueueAction(new FuncAction
			{
				name = "load_login",
				action = () => loadLogin(loginId, onSuccess, onError)
			});
		}

		private Action loadLogin(int loginId, Action<LobbyLogin, Dictionary<int, int>> onSuccess, Action<int, MsSqlLoadResult, Exception> onError)
		{
			const string sql = "LoginInfoGet";

			string loginName = null;
            string loginIdent = null;
			string loginInfo = null;
			int sessionId = 0;
            DateTime authLastTime = TimeProvider.UTCNow;
            DateTime authRegTime = TimeProvider.UTCNow; // TODO fil зашквар! на авториазции создаем новый логин по Now а тут по Utc, срочно надо переделывать
			var friendListId = new List<int>();
			var inviteFriendListId = new List<int>();
			var requestFriendListId = new List<int>();

            Dictionary<int, int> persons1;

            using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@loginId", loginId);

                        using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
						{
                            while (reader.Read())
                            {
                                var data = new DBRecordCollection(reader);
                                var getLogin = data.GetInt("LoginId");
                                loginName = data.GetString("LoginName");
                                loginInfo = data.GetString("LoginInfo");
                                loginIdent = data.GetString("Identifier");
                                authLastTime = data.GetDateTime("authLastTime");
                                authRegTime = data.GetDateTime("authRegTime");
								sessionId = data.GetInt("SessionId");

							}

							if (!reader.NextResult()) // can throw
								throw new Exception("No data about persons");

							persons1 = new Dictionary<int, int>();
                            while (reader.Read())
							{
                                var personData = new DBRecordCollection(reader);
								int slotId = personData.GetInt("slotId");
								int personId = personData.GetInt("personId");
                                persons1.Add(personId, slotId);
                            }

							if (!reader.NextResult()) // can throw
								throw new Exception("No data about friends");

							while (reader.Read())
							{
								var data = new DBRecordCollection(reader);
								int firstLoginId = data.GetInt("FirstLoginId");
								int secondLoginId = data.GetInt("SecondLoginId");
								int status = data.GetInt("Status");
								if (loginId == firstLoginId)
								{
									switch (status)
									{
										case 1: inviteFriendListId.Add(secondLoginId); break;
										case 2: requestFriendListId.Add(secondLoginId); break;
										case 3: friendListId.Add(secondLoginId); break;
									}
								}
								else if (loginId == secondLoginId)
								{
									switch (status)
									{
										case 1: requestFriendListId.Add(firstLoginId); break;
										case 2: inviteFriendListId.Add(firstLoginId); break;
										case 3: friendListId.Add(firstLoginId); break;
									}
								}
							}
						}
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send("loadLogin(): " + ex, ErrorLevel.exception);
					return () => onError(loginId, MsSqlLoadResult.Fail, ex);
				}
			}

			if (loginInfo == null)
				return () =>
				{
					//Если нету, то создать новый
					var login = new LobbyLogin(loginId, authRegTime);
					SaveLogin(login);

					onSuccess?.Invoke(login, persons1);
				};

			return () =>
			{
				var login = new LobbyLogin(loginId, loginName, loginIdent, authLastTime, authRegTime, JSON.JsonDecode(loginInfo).JsonDecode<LobbyLoginSqlData>(),
					friendListId, inviteFriendListId, requestFriendListId, sessionId);

                onSuccess?.Invoke(login, persons1);
			};
		}

		public void SaveLogin(LobbyLogin login)
		{
			SaveLogin(login, null, (i, result, arg3) => { }); //TODO_Deg
		}

		public void SaveLogin(LobbyLogin login, Action<int> onSuccess, Action<int, MsSqlSaveResult, Exception> onError)
		{
			SaveLogin(login.loginId, login.loginName, JSON.JsonEncode(login.GetSaveData().JsonEncode()), login.SessionId, onSuccess, onError);
			login.lastSave = TimeProvider.UTCNow;
		}

		public void SaveLogin(int loginId, string loginName, string loginInfo,int sessionId, Action<int> onSuccess, Action<int, MsSqlSaveResult, Exception> onError)
		{
			ILogger.Instance.Send("MSSQLAdapter SaveLogin " + loginId, ErrorLevel.trace);
			enqueueAction(new FuncAction

			{
				name = "save_login",
				action = () => saveLogin(loginId, loginName, loginInfo, sessionId, onSuccess, onError)
			});
		}

		private Action saveLogin(int loginId, string loginName, string loginInfo, int sessionId, Action<int> onSuccess, Action<int, MsSqlSaveResult, Exception> onError)
		{
			const string sql = "LoginInfoSave";
			bool success = false;

			using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@loginId", loginId);
						command.AddParameter("@loginName", loginName);
						command.AddParameter("@loginInfo", loginInfo);
						command.AddParameter("@sessionId", sessionId);

						using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
						{
							if (reader.Read())
							{
								var data = new DBRecordCollection(reader);
								var getLogin = data.GetInt("LoginId");
								var errorCount = data.GetInt("ErrorCount");
								var msg = data.GetString("msg");

								if (errorCount > 0)
									ILogger.Instance.Send($"saveLogin(): loginId = {loginId} sessionId = {sessionId}" + msg, ErrorLevel.exception);

								success = true;
							}

							reader.Read(); // can throw
						}
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send("saveLogin(): " + ex, ErrorLevel.exception);
					return () => onError(loginId, MsSqlSaveResult.Fail, ex);
				}
			}

			if (!success)
				return () => onError(loginId, MsSqlSaveResult.Fail, null);

			return () => { if (onSuccess != null) onSuccess(loginId); };
		}

		public void RemoveLogin(int loginId)
		{
			ILogger.Instance.Send("MSSQLAdapter RemoveLogin " + loginId, ErrorLevel.trace);
			enqueueAction(new FuncAction

			{
				name = "remove_login",
				action = () => removeLogin(loginId)
			});
		}

		private Action removeLogin(int loginId)
		{
			const string sql = "LoginInfoRemove";

			using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@loginId", loginId);

						command.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send("MSSQLAdapter.RemoveLogin: loginId = " + loginId, ex.ToString(), ErrorLevel.exception);
				}
			}

			return () => { };
		}

		//Удалить персов второго логина. Скопировать персов с первого логина во второй. Удалить первый логин
		public void CopyPersonAndRemoveLogin(int fromLoginId, int toLoginId)
		{
			ILogger.Instance.Send("MSSQLAdapter CopyPersonAndRemoveLogin " + fromLoginId + " " + toLoginId, ErrorLevel.trace);
			enqueueAction(new FuncAction
			{
				name = "copy_and_remove_login",
				action = () => copyPersonAndRemoveLogin(fromLoginId, toLoginId)
			});
		}

		private Action copyPersonAndRemoveLogin(int fromLoginId, int toLoginId)
		{
			const string sql = "LoginInfoCopyAndRemove";

			using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@fromLoginId", fromLoginId);
						command.AddParameter("@toLoginId", toLoginId);

						command.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send("MSSQLAdapter.CopyPersonAndRemoveLogin: fromLoginId = " + fromLoginId + " toLoginId = " + toLoginId, ex.ToString(), ErrorLevel.exception);
				}
			}

			return () => { };
		}

		public void FindLogin(string loginName, Action<int> onSuccess, Action onError)
		{
			ILogger.Instance.Send("MSSQLAdapter FindLogin " + loginName, ErrorLevel.trace);
			enqueueAction(new FuncAction
			{
				name = "find_login",
				action = () => findLogin(loginName, onSuccess, onError)
			});
		}

		public Action findLogin(string loginName, Action<int> onSuccess, Action onError)
		{
			const string sql = "FindLogin";

			int loginId = -1;
			bool success = false;

			using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@name", loginName);

						using (SqlDataReader reader = command.ExecuteReader())
						{
							if (reader.HasRows)
							{
								reader.Read();
								var data = new DBRecordCollection(reader);
								loginId = data.GetInt("LoginId");
								success = true;
								//if (success)
								//    onSuccess(login, type, name, fraction, slotId);
								//else
								//    onError(login);
							}

							reader.Read(); // can throw
						}
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send("findLogin(): " + ex, ErrorLevel.exception);
					return () => onError();
				}
			}

			if (!success)
				return () => onError();

			return () => { if (onSuccess != null) onSuccess(loginId); };

		}

		#endregion

		#region Person2

	    public delegate void PersonCreateOnSuccess(LobbyLogin login, InstancePersonSqlData person);

		public void CreatePerson(LobbyLogin login, InstancePersonSqlData person, PersonCreateOnSuccess onSuccess, Action<LobbyLogin, Exception> onError)
		{
			ILogger.Instance.Send("MSSQLAdapter CreatePerson " + login.loginId + " : " + person.SlotId, ErrorLevel.trace);
			enqueueAction(new FuncAction

			{
				name = "create_person",
				action = () => createPerson(login, person, onSuccess, onError)
			});
		}

		private Action createPerson(LobbyLogin login, InstancePersonSqlData person, PersonCreateOnSuccess onSuccess, Action<LobbyLogin, Exception> onError)
		{
			const string sql = "CreatePerson";

			bool success = false;

			int personId = 0;

			using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@LoginId", login.loginId);
						command.AddParameter("@SlotId", person.SlotId);
						command.AddParameter("@type", (int)person.Type);
						command.AddParameter("@fraction", (int)person.Fraction);
						command.AddParameter("@data", JSON.JsonEncode(person.JsonEncode()));
						command.AddParameter("@analyticId", person.AnalyticId);
                        command.AddParameter("@Items", JSON.JsonEncode(person.Items2.JsonEncode()));
                        command.AddParameter("@ItemsPotions", JSON.JsonEncode(person.ItemsPotions2.JsonEncode()));
                        command.AddParameter("@ItemsEquiped", JSON.JsonEncode(person.ItemsEquiped2.JsonEncode()));
                        command.AddParameter("@TradeItems", JSON.JsonEncode(person.TradeItems2.JsonEncode()));

                        using (SqlDataReader reader = command.ExecuteReader())
						{
							if (reader.HasRows)
							{
								reader.Read();
								var data = new DBRecordCollection(reader);
								personId = data.GetInt("success");
								success = personId != 0;
							}

							reader.Read(); // can throw
						}
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send("createPerson(): " + ex, ErrorLevel.exception);
					return () => onError(login, ex);
				}
			}

			if (!success)
				return () => onError(login, new Exception(" personId == 0 "));

			return () =>
			{
				person.PersonId = personId;
				if (onSuccess != null)
					onSuccess(login, person);
			};
		}

		public void RemovePerson(int personId, Action<int> onSuccess, Action<int, MsSqlSaveResult, Exception> onError)
		{
			ILogger.Instance.Send("MSSQLAdapter RemovePerson " + personId, ErrorLevel.trace);
			enqueueAction(new FuncAction

			{
				name = "remove_person",
				action = () => removePerson(personId, onSuccess, onError)
			});
		}

		private Action removePerson(int personId, Action<int> onSuccess, Action<int, MsSqlSaveResult, Exception> onError)
		{
			const string sql = "PersonInfoRemove";

			using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@personId", personId);

						command.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send("removePerson(): " + ex, ErrorLevel.exception);
					return () => onError(personId, MsSqlSaveResult.Fail, ex);
				}
			}

			return () => { if (onSuccess != null) onSuccess(personId); };
		}

		public void FindPerson(string personName, Action<int, int> onSuccess, Action onError)
		{
			ILogger.Instance.Send("MSSQLAdapter FindPerson " + personName, ErrorLevel.trace);
			enqueueAction(new FuncAction

			{
				name = "find_person",
				action = () => findPerson(personName, onSuccess, onError)
			});
		}

		public Action findPerson(string personName, Action<int, int> onSuccess, Action onError)
		{
			const string sql = "FindPerson";

			int loginId = -1;
			int personId = -1;
			bool success = false;

			using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@name", personName);

						using (SqlDataReader reader = command.ExecuteReader())
						{
							if (reader.HasRows)
							{
								reader.Read();
								var data = new DBRecordCollection(reader);
								loginId = data.GetInt("LoginId");
								personId = data.GetInt("PersonId");
								success = true;
								//if (success)
								//    onSuccess(login, type, name, fraction, slotId);
								//else
								//    onError(login);
							}

							reader.Read(); // can throw
						}
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send("findPerson(): " + ex, ErrorLevel.exception);
					return onError;
				}
			}

			if (!success)
				return onError;

			return () => { if (onSuccess != null) onSuccess(loginId, personId); };

		}

		#endregion

		#region BagReport

		public void SaveBugReport(int loginId, int slotId, string message, Action onSuccess, Action<Exception> onError)
		{
			ILogger.Instance.Send("MSSQLAdapter SaveBugReport " + loginId + ":" + slotId, ErrorLevel.trace);
			enqueueAction(new FuncAction

			{
				name = "save_bug_report",
				action = () => saveBugReport(loginId, slotId, message, onSuccess, onError)
			});
		}

		private Action saveBugReport(int loginId, int slotId, string message, Action onSuccess, Action<Exception> onError)
		{
			const string sql = "BugReportsSave";

			bool success = false;

			using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@LoginId", loginId);
						command.AddParameter("@SlotId", slotId);

						command.AddParameter("@Message", message);

						using (SqlDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
								success = true;

							reader.Read(); // can throw
						}
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send("saveBugReport(): " + ex, ErrorLevel.exception);
					return () => onError(ex);
				}
			}

			if (!success)
				return () => onError(null);

			return () => { if (onSuccess != null) onSuccess(); };
		}

		#endregion

		#region Rename

		public void GetLikeLoginName(string name, Action<List<string>> onSuccess, Action<Exception> onError)
		{
			enqueueAction(new FuncAction
			{
				name = "get_like_login_name",
				action = () => getLikeLoginName(name, onSuccess, onError)
			});
		}

		private Action getLikeLoginName(string name, Action<List<string>> onSuccess, Action<Exception> onError)
		{
			const string sql = "GetLikeLoginName";

			var list = new List<string>();

			using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@LoginName", name);

						using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
						{
							while (reader.Read())
							{
								var record = new DBRecordCollection(reader);
								list.Add(record.GetString("LoginName"));
							}
						}
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send($"getLikeLoginName(): name={name} ex={ex}", ErrorLevel.exception);
					return () => onError(ex);
				}
			}

			return () => onSuccess?.Invoke(list);
		}

		public void SetLoginName(string name, int loginId, Action onSuccess, Action<Exception> onError)
		{
			enqueueAction(new FuncAction
			{
				name = "set_login_name",
				action = () => setLoginName(name, loginId, onSuccess, onError)
			});
		}

		private Action setLoginName(string name, int loginId, Action onSuccess, Action<Exception> onError)
		{
			const string sql = "SetLoginName";

			bool exists = false;

			using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@Name", name);
						command.AddParameter("@LoginId", loginId);

						exists = (Int32)command.ExecuteScalar() != 0;
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send("setLoginName(): " + ex, ErrorLevel.exception);
					return () => onError(ex);
				}
			}

			if (exists)
				return () => onError(null);

			return () => onSuccess?.Invoke();
		}

		#endregion

		#region Friend

		public void FriendInvite(int loginId, int friendId, Action onSuccess, Action<Exception> onError = null)
		{
			enqueueAction(new FuncAction
			{
				name = "friend_invite",
				action = () => friendInvite(loginId, friendId, onSuccess, onError)
			});
		}

		private Action friendInvite(int loginId, int friendId, Action onSuccess, Action<Exception> onError)
		{
			const string sql = "FriendInvite";

			using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@LoginId", loginId);
						command.AddParameter("@FriendId", friendId);

						command.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send("friendInvite(): " + ex, ErrorLevel.exception);
					return () => onError(ex);
				}
			}

			return () => onSuccess?.Invoke();
		}

		public void FriendAccept(int loginId, int friendId, int maxFriendCount, Action<ClientErrors> onSuccess, Action<Exception> onError = null)
		{
			enqueueAction(new FuncAction
			{
				name = "friend_accept",
				action = () => friendAccept(loginId, friendId, maxFriendCount, onSuccess, onError)
			});
		}

		private Action friendAccept(int loginId, int friendId, int maxFriendCount, Action<ClientErrors> onSuccess, Action<Exception> onError)
		{
			const string sql = "FriendAccept";

			var error = ClientErrors.SUCCESS;

			using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@LoginId", loginId);
						command.AddParameter("@FriendId", friendId);
						command.AddParameter("@MaxFriendCount", maxFriendCount);

						using (var reader = command.ExecuteReader(CommandBehavior.CloseConnection))
						{
							if (reader.Read())
							{
								var data = new DBRecordCollection(reader);
								int result = data.GetInt("result");
								if (result == 1)
									error = ClientErrors.FRIEND_MAX_COUNT;
								if (result == 2)
									error = ClientErrors.OTHER_PLAYER_FRIEND_MAX_COUNT;
							}
						}
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send("friendAccept(): " + ex, ErrorLevel.exception);
					return () => onError(ex);
				}
			}

			return () => onSuccess?.Invoke(error);
		}

		public void FriendRemove(int loginId, int friendId, Action onSuccess, Action<Exception> onError = null)
		{
			enqueueAction(new FuncAction
			{
				name = "friend_remove",
				action = () => friendRemove(loginId, friendId, onSuccess, onError)
			});
		}

		private Action friendRemove(int loginId, int friendId, Action onSuccess, Action<Exception> onError)
		{
			const string sql = "FriendRemove";

			using (var connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						command.AddParameter("@LoginId", loginId);
						command.AddParameter("@FriendId", friendId);

						command.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send("friendRemove(): " + ex, ErrorLevel.exception);
					return () => onError(ex);
				}
			}

			return () => onSuccess?.Invoke();
		}

		#endregion
	}
}
