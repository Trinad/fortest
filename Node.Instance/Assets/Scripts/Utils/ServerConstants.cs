﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Actions;
using Batyi.TinyServer;
using UtilsLib.BatiyScript;

namespace Assets.Scripts.Utils
{
	public class ServerConstants
	{
		public static ServerConstants Instance;

		public ServerConstants()
		{
			Instance = this;
		}

		public float LootPickupRadius { get; set; } // радиус подбора лута
		public float LootOverflowTimeSec { get; set; }
		public int ResourceLootLifeTimeoutSec { get; set; }
		public int ItemLootLifeTimeoutSec { get; set; }
		public int LootUnavailAfterCreateSec { get; set; }

		public List<MapObjectConstant> MapObjectConstants = new List<MapObjectConstant>();

		public class MapObjectConstant
		{
			[XmlAttribute]
			public string name;

			[XmlAttribute]
			public int Health;

            [XmlAttribute]
            public float HitRadius;
        }

		public float ActiveMapObjectsHighlightRadius;
		public float ActiveMapObjectsNearRadius;
		public float ActiveMapObjectsMaxRadius;
		public int ActiveMapObjectsHistoryCount = 5;
	    public int ActionBotWaitTimeSec { get; set; }

	    public int MaxMailCount = 50; //Максимальное колличество писем, которое сохранится в базу
		public int MaxGiftSenders = 7; //Максимальное отправителей подарков, которые будут объединины в одно письмо
		public int MonsterDefaultResurrectTime = 5; //время через которое воскресает монстр
        public int MonsterHealPercent = 20; //процент отлечивания монстра в секунду.
        public int BuyForEmeraldTimeout = 10; //время когда можно отправить повторный запрос на некоторые покупки...
		public float FavorOfGodsTimeSec = 3f; //сколько секунд после воскрешения не разрешать каставать абилки и получать урон
        public int BattleStateOnSpellCastTimeout = 10;

        public int DefaultObjectsRespawnCooldownSec = 120;
		
        public int MaxGroupSize = 5;
		public float GroupInviteValidTime = 30f;

        public int ActiveAbilityMaxLevel = 25;

		public IActionList StartPack; //Стартовые шмотки

        public int WorldMessagePrice = 10;

        public int ToOpenFloorPrice = 10;
        public int ToEnterFloorPrice = 2;
        public int ToResetCoolDown = 25; // за минуту

        public int TimeOpenFloor = 60; // минут
        public int TimeCooldownFloor = 720; // минут

        public int RessurectCost = 5;//цена в изумрудах за воскрешение

		public int TapSqrRadius = 9 * 9; //Квадрат радиуса тапа по цели
		public int TapPriorityMultiply = 200; //Множитель приоритета при тапе

		//Время для сохранить и выйти
		public int SaveAndExitTimeSec = 30; //Если перс в бою и закрывает игру, то его тушка столько сек будет стоять на локации
		public int SaveAndExitTimeSecAfterDead = 3; //А если его убъют в это время, то столько сек для анимации смерти

        public int LevelGsMaxOffset = +150;// на сколько превышать должен гирскор игрока чтоб сработыл ПК
        public int LevelGsMinOffset = -10;

        public int MiniMapQuestTarget = 3; //Сколько квестовых целей показывать на миникарте

        public int ChanceItemDropForFort = 50;

		public int SaveZonePortal = 5; //Безопасная зона по умолчанию 1м

	    public int RemoveRatingRevardMailDays = 15;// через какое время удалять писма с наградой рейтинга
	    public int DefaultRemoveMailDays = 7;

	    [XmlArrayItem("XmlPair", typeof(XmlPair<string, string>))]
        public List<XmlPair<string, string>> LikeAppUrls;

        public int CountForBuy = 3; // количество эмотиконов для покупки
        public int RestoreTimeSec = 30; // время когда откроется новый эмотикон для покупки в секундах
        public int RefreshTimeHours = 24; // время обновления всех эмотиконов для покупки в часах
        public float TimeInUseSec = 3.5f; // время обновления всех эмотиконов для покупки в часах

		public int ActionBotRefreshTimeMin = 1; //время обновления акций у бота

        public class ClientLocalizationKey
        {
            [XmlAttribute]
            public string key; //Ключ локализации для клиента
            [XmlAttribute]
            public string tags //Указывать теги через ";" без подчеркиваний типа "{_tag1_}". Например tags="tag1;tag2;tag3"
            {
                get { return string.Join(";", Tags); }
                set { Tags = value.Split(';'); }
            }

            [XmlIgnore]
            public string[] Tags;
	    }

		public string WallInSaveZoneMessage; //Сообщение "Вы поставили стену в нестабильном месте. Часть стены исчезла."
		public string ElixirNotAvailableMessage; //Сообщение "Нельзя использовать в данной локации"
		public string NoSparksMessage; //Сообщение "У вас недостаточно Искр"
		public string TutorialUnavailableMessage; //Сообщение "Недоступно в туториале"
		public string CollectionCollectedMessage; //Сообщение "Коллекция собрана! Возвращайтесь в лагерь"
		public string LocationPvPMessage; //Сообщение "Осторожно! На этой локации другие игроки могут убить вас!"
		public string BeltRarityMessage; //Сообщение "Чтобы использовать больше разных зелий вам нужен пояс с большим числом карманов."
		public string BeltUnavailableMessage; //Сообщение "У вас нет пояса, поэтому вы не можете использовать быстрый доступ к зельям."
        public string ElixirNotAvailableFreezeMessage; //Сообщение "Нельзя использовать зелье до начала матча"
        public string ElixirNotAvailableFlagMessage; //Сообщение "Нельзя использовать зелье когда несешь флаг"
        public ClientLocalizationKey ArenaDeathmatchAssist; //Сообщение "Победные очки +{_score_diff_} за помощь в убийстве"

        [XmlIgnore]
		public List<string> NeedKeyMessages; //Сообщение "Нужен ключ" для трех видов ключей
		public string NeedKeyMessage
		{
			get { return ConvertUtils.ConvertToString(NeedKeyMessages); }
			set { NeedKeyMessages = ConvertUtils.ConvertToListStr(value); }
		}

		public FormulaX PvPChestCooldown;

        public FormulaX BlessingTime;

        public int ChangeNameEmeraldsCost = 1;

		public class PotionUnlock
		{
			[XmlAttribute]
			public byte Level;
			[XmlAttribute]
			public string Notification;
		}

		public List<PotionUnlock> PotionLevelUnlock;

        public int Dev2DevLimit = 50000;
    }

    public class XmlPair<K,V>
    {
        [XmlAttribute]
        public K key;

        [XmlAttribute]
        public V value;
    }
}
