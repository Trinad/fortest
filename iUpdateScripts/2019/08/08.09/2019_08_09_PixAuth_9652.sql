﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT 1 FROM sys.databases WHERE [NAME] = DB_NAME() AND compatibility_level <130)
	BEGIN
		ALTER DATABASE CURRENT SET COMPATIBILITY_LEVEL = 130
	END
GO

BEGIN TRAN

DECLARE @personType int;
DECLARE @List2 nvarchar(max);
DECLARE @List3 nvarchar(max);

	SET @personType = 0;
	SET @List2 = '[27, 29, 30, 32, 34, 36]';
	SET @List3 = '[28, 2, 8, 33, 20, 9]';

UPDATE dbo.Persons 
SET
	data = JSON_MODIFY(convert(nvarchar(max),data),'$.PerksCollection.UsedPerkList[1].PerkList', JSON_QUERY(@List2))
	from dbo.Persons WHERE ISJSON(convert(nvarchar(max),data)) > 0 and PersonId in 
		(SELECT [PersonId] FROM [dbo].[Persons] as p CROSS APPLY OPENJSON(JSON_QUERY(convert(nvarchar(max),data), '$.PerksCollection.UsedPerkList[1]')) AS x 	
		WHERE (x.[value] = '[-1, -1, -1, -1, -1, -1]') and p.[type] = @personType)
UPDATE dbo.Persons 
SET
	data = JSON_MODIFY(convert(nvarchar(max),data),'$.PerksCollection.UsedPerkList[2].PerkList', JSON_QUERY(@List3))
	from dbo.Persons WHERE ISJSON(convert(nvarchar(max),data)) > 0 and PersonId in 
		(SELECT [PersonId] FROM [dbo].[Persons] as p CROSS APPLY OPENJSON(JSON_QUERY(convert(nvarchar(max),data), '$.PerksCollection.UsedPerkList[2]')) AS y
		WHERE (y.[value] = '[-1, -1, -1, -1, -1, -1]') and p.[type] = @personType)

/****** next personType = 1  ******/
	SET @personType = 1;
	SET @List2 = '[50, 53, 56, 58, 59, 61]';
	SET @List3 = '[52, 11, 55, 17, 21, 14]';

UPDATE dbo.Persons 
SET
	data = JSON_MODIFY(convert(nvarchar(max),data),'$.PerksCollection.UsedPerkList[1].PerkList', JSON_QUERY(@List2))
	from dbo.Persons WHERE ISJSON(convert(nvarchar(max),data)) > 0 and PersonId in 
		(SELECT [PersonId] FROM [dbo].[Persons] as p CROSS APPLY OPENJSON(JSON_QUERY(convert(nvarchar(max),data), '$.PerksCollection.UsedPerkList[1]')) AS x
		WHERE (x.[value] = '[-1, -1, -1, -1, -1, -1]') and p.[type] = @personType)
UPDATE dbo.Persons 
SET
	data = JSON_MODIFY(convert(nvarchar(max),data),'$.PerksCollection.UsedPerkList[2].PerkList', JSON_QUERY(@List3))
	from dbo.Persons WHERE ISJSON(convert(nvarchar(max),data)) > 0 and PersonId in
		(SELECT [PersonId] FROM [dbo].[Persons] as p CROSS APPLY OPENJSON(JSON_QUERY(convert(nvarchar(max),data), '$.PerksCollection.UsedPerkList[2]')) AS y
		WHERE (y.[value] = '[-1, -1, -1, -1, -1, -1]') and p.[type] = @personType)

/****** next @personType = 2  ******/
	SET @personType = 2;
	SET @List2 = '[63, 65, 67, 69, 70, 72]';
	SET @List3 = '[64, 66, 8, 69, 23, 72]';

UPDATE dbo.Persons 
SET
	data = JSON_MODIFY(convert(nvarchar(max),data),'$.PerksCollection.UsedPerkList[1].PerkList', JSON_QUERY(@List2))
	from dbo.Persons WHERE ISJSON(convert(nvarchar(max),data)) > 0 and PersonId in 
		(SELECT [PersonId] FROM [dbo].[Persons] as p CROSS APPLY OPENJSON(JSON_QUERY(convert(nvarchar(max),data), '$.PerksCollection.UsedPerkList[1]')) AS x 	
		WHERE (x.[value] = '[-1, -1, -1, -1, -1, -1]') and p.[type] = @personType)
UPDATE dbo.Persons 
SET
	data = JSON_MODIFY(convert(nvarchar(max),data),'$.PerksCollection.UsedPerkList[2].PerkList', JSON_QUERY(@List3))
	from dbo.Persons WHERE ISJSON(convert(nvarchar(max),data)) > 0 and PersonId in 
		(SELECT [PersonId] FROM [dbo].[Persons] as p CROSS APPLY OPENJSON(JSON_QUERY(convert(nvarchar(max),data), '$.PerksCollection.UsedPerkList[2]')) AS y
		WHERE (y.[value] = '[-1, -1, -1, -1, -1, -1]') and p.[type] = @personType)

/****** next personType = 3  ******/
	SET @personType = 3;
	SET @List2 = '[38, 40, 43, 45, 46, 48]';
	SET @List3 = '[39, 11, 42, 44, 24, 49]';

UPDATE dbo.Persons 
SET
	data = JSON_MODIFY(convert(nvarchar(max),data),'$.PerksCollection.UsedPerkList[1].PerkList', JSON_QUERY(@List2))
	from dbo.Persons WHERE ISJSON(convert(nvarchar(max),data)) > 0 and PersonId in 
		(SELECT [PersonId] FROM [dbo].[Persons] as p CROSS APPLY OPENJSON(JSON_QUERY(convert(nvarchar(max),data), '$.PerksCollection.UsedPerkList[1]')) AS x 	
		WHERE (x.[value] = '[-1, -1, -1, -1, -1, -1]') and p.[type] = @personType)
UPDATE dbo.Persons 
SET
	data = JSON_MODIFY(convert(nvarchar(max),data),'$.PerksCollection.UsedPerkList[2].PerkList', JSON_QUERY(@List3))
	from dbo.Persons WHERE ISJSON(convert(nvarchar(max),data)) > 0 and PersonId in 
		(SELECT [PersonId] FROM [dbo].[Persons] as p CROSS APPLY OPENJSON(JSON_QUERY(convert(nvarchar(max),data), '$.PerksCollection.UsedPerkList[2]')) AS y
		WHERE (y.[value] = '[-1, -1, -1, -1, -1, -1]') and p.[type] = @personType)

/****** next personType = 4  ******/
	SET @personType = 4;
	SET @List2 = '[73, 75, 78, 80, 82, 84]';
	SET @List3 = '[1, 11, 77, 79, 81, 83]';

UPDATE dbo.Persons 
SET
	data = JSON_MODIFY(convert(nvarchar(max),data),'$.PerksCollection.UsedPerkList[1].PerkList', JSON_QUERY(@List2))
	from dbo.Persons WHERE ISJSON(convert(nvarchar(max),data)) > 0 and PersonId in 
		(SELECT [PersonId] FROM [dbo].[Persons] as p CROSS APPLY OPENJSON(JSON_QUERY(convert(nvarchar(max),data), '$.PerksCollection.UsedPerkList[1]')) AS x 	
		WHERE (x.[value] = '[-1, -1, -1, -1, -1, -1]') and p.[type] = @personType)
UPDATE dbo.Persons SET
	data = JSON_MODIFY(convert(nvarchar(max),data),'$.PerksCollection.UsedPerkList[2].PerkList', JSON_QUERY(@List3))
	from dbo.Persons WHERE ISJSON(convert(nvarchar(max),data)) > 0 and PersonId in 
		(SELECT [PersonId] FROM [dbo].[Persons] as p CROSS APPLY OPENJSON(JSON_QUERY(convert(nvarchar(max),data), '$.PerksCollection.UsedPerkList[2]')) AS y
		WHERE (y.[value] = '[-1, -1, -1, -1, -1, -1]') and p.[type] = @personType)

COMMIT TRAN
