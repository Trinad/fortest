﻿IF(OBJECT_ID('PK_Clans_CreatorId', 'F') IS NOT NULL)
	alter table dbo.clans drop constraint PK_Clans_CreatorId

IF(OBJECT_ID('PK_Clans_LeaderId', 'F') IS NOT NULL)
	alter table dbo.clans drop constraint PK_Clans_LeaderId
GO

ALTER PROCEDURE [dbo].[ClanCreate] 
 @Name nvarchar(100),
 @Description nvarchar(max),
 @Type int,
 @MinRatingScore int,
 @Time int,
 @EmblemType int,
 @EmblemId int,
 @PatternId int,
 @EmblemColor nvarchar(20),
 @PatternColor nvarchar(20),
 @FlagColor nvarchar(20), 
 @LeaderId int,
 @Persons nvarchar(max),
 @Invites nvarchar(max),
 @Joins nvarchar(max),
 @RatingScore int
AS
BEGIN
	SET NOCOUNT ON;
 
	INSERT INTO [Clans]
		   ([Name], [Description], [Type], [MinRatingScore], [Time], [EmblemType], [EmblemId], [PatternId], [EmblemColor], [PatternColor], [FlagColor], [LeaderId], [Persons], [Invites], [Joins], [RatingScore])
	VALUES (@Name,  @Description,  @Type,  @MinRatingScore,  @Time,  @EmblemType,  @EmblemId,  @PatternId,  @EmblemColor,  @PatternColor,  @FlagColor, @LeaderId,  @Persons,  @Invites,  @Joins,  @RatingScore);

	SELECT SCOPE_IDENTITY() as clanid

END
GO