﻿IF((SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME=N'BestPlace' AND TABLE_NAME=N'Clans') is NUll)	
	ALTER TABLE dbo.Clans ADD [BestPlace] INT NOT NULL CONSTRAINT [DF_BestPlace] DEFAULT(-1);
GO

ALTER PROCEDURE [dbo].[ClanCreate] 
 @Name nvarchar(100),
 @Description nvarchar(max),
 @Type int,
 @MinRatingScore int,
 @Time int,
 @EmblemType int,
 @EmblemId int,
 @PatternId int,
 @EmblemColor nvarchar(20),
 @PatternColor nvarchar(20),
 @FlagColor nvarchar(20), 
 @LeaderId int,
 @Persons nvarchar(max),
 @Invites nvarchar(max),
 @Joins nvarchar(max),
 @RatingScore int,
 @BestPlace int
AS
BEGIN
	SET NOCOUNT ON;
 
	INSERT INTO [Clans]
		   ([Name], [Description], [Type], [MinRatingScore], [Time], [EmblemType], [EmblemId], [PatternId], [EmblemColor], [PatternColor], [FlagColor], [LeaderId], [Persons], [Invites], [Joins], [RatingScore], [BestPlace])
	VALUES (@Name,  @Description,  @Type,  @MinRatingScore,  @Time,  @EmblemType,  @EmblemId,  @PatternId,  @EmblemColor,  @PatternColor,  @FlagColor, @LeaderId,  @Persons,  @Invites,  @Joins,  @RatingScore, @BestPlace);

	SELECT SCOPE_IDENTITY() as clanid

END
GO

ALTER PROCEDURE [dbo].[ClanCommit] 
 @Id int,
 @Name nvarchar(100),
 @Description nvarchar(max),
 @Type int,
 @MinRatingScore int,
 @Time int,
 @EmblemType int,
 @EmblemId int,
 @PatternId int,
 @EmblemColor nvarchar(20),
 @PatternColor nvarchar(20),
 @FlagColor nvarchar(20), 
 @LeaderId int,
 @Persons nvarchar(max),
 @Invites nvarchar(max),
 @Joins nvarchar(max),
 @RatingScore int,
 @BestPlace int
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE Clans 
	set
		Name = @Name,
		Description = @Description,
		Type = @Type,
		MinRatingScore = @MinRatingScore,
		Time = @Time,
		EmblemType = @EmblemType,
		EmblemId = @EmblemId,
		PatternId = @PatternId,
		EmblemColor = @EmblemColor,
		PatternColor = @PatternColor,
		FlagColor = @FlagColor,		
		LeaderId = @LeaderId,
		Persons = @Persons,
		Invites = @Invites,
		Joins = @Joins,
		RatingScore = @RatingScore,
		BestPlace = @BestPlace
	WHERE Id = @Id;
END
GO

