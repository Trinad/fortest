﻿ALTER PROCEDURE [dbo].[LoginCheatDelete] 
 @LoginId INT
AS
BEGIN
	SET NOCOUNT ON; 
	disable trigger LoginsDeleteTrigger on [dbo].[Logins];
	disable trigger LoginInfosDeleteTrigger on [dbo].[LoginInfos];
	disable trigger PersonDeleteTrigger on [dbo].[Persons];
	
	DELETE [dbo].[Mails] WHERE [LoginId] = @LoginId;
	DELETE [dbo].[LoginInfos] WHERE [LoginId] = @LoginId;
	DELETE [dbo].[Friends] WHERE [FirstLoginId] = @LoginId OR [SecondLoginId] = @LoginId;
	DELETE [dbo].[Persons] WHERE [LoginId] = @LoginId;
	DELETE [dbo].[Logins] WHERE [LoginId] = @LoginId;

	enable trigger LoginsDeleteTrigger on [dbo].[Logins];
	enable trigger LoginInfosDeleteTrigger on [dbo].[LoginInfos];
	enable trigger PersonDeleteTrigger on [dbo].[Persons];
END
GO