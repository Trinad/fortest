﻿IF(EXISTS(select * from sysindexes where name = 'IX_Clans_CreatorId'))
begin
	drop index IX_Clans_CreatorId ON [dbo].[Clans]  
	ALTER TABLE [dbo].[Clans] DROP COLUMN CreatorId
end
GO

ALTER PROCEDURE [dbo].[ClanCommit] 
 @Id int,
 @Name nvarchar(100),
 @Description nvarchar(max),
 @Type int,
 @MinRatingScore int,
 @Time int,
 @EmblemType int,
 @EmblemId int,
 @PatternId int,
 @EmblemColor nvarchar(20),
 @PatternColor nvarchar(20),
 @FlagColor nvarchar(20), 
 @LeaderId int,
 @Persons nvarchar(max),
 @Invites nvarchar(max),
 @Joins nvarchar(max),
 @RatingScore int
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE Clans 
	set
		Name = @Name,
		Description = @Description,
		Type = @Type,
		MinRatingScore = @MinRatingScore,
		Time = @Time,
		EmblemType = @EmblemType,
		EmblemId = @EmblemId,
		PatternId = @PatternId,
		EmblemColor = @EmblemColor,
		PatternColor = @PatternColor,
		FlagColor = @FlagColor,		
		LeaderId = @LeaderId,
		Persons = @Persons,
		Invites = @Invites,
		Joins = @Joins,
		RatingScore = @RatingScore
	WHERE Id = @Id;
END
GO
