﻿

Create PROCEDURE [dbo].[weekGuildRatingCreate]
    @Id int,
	@GuildId int,
	@GuildName nvarchar(max),
	@GuildType int, 

	@GuildSymbolId int,
	@GuildSymbolColor nchar(20),
	@GuildBgColor nchar(20),

	@GuildRank int,
	@KillCount int,
	@Points int,
	@DeathCount int,
	@Wins int,
	@Looses int,
	@Draw int,
    @GameCount int
AS
BEGIN
    set nocount on;

insert into dbo.weekGuildRating 
(Id, GuildId, GuildName, GuildType, GuildSymbolId, GuildSymbolColor, GuildBgColor, GuildRank,  KillCount, Points, DeathCount, Wins, Looses, Draw, GameCount )
values
(@Id, @GuildId, @GuildName, @GuildType, @GuildSymbolId, @GuildSymbolColor, @GuildBgColor, @GuildRank, @KillCount, @Points, @DeathCount, @Wins, @Looses, @Draw, @GameCount )
END

