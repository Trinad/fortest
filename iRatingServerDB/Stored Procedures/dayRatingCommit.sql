﻿
CREATE PROCEDURE[dbo].[dayRatingCommit]
@Id int,
	@LoginId int,
	@PersonType int,
	@Name nvarchar(max), 
	@KillCount int,
	@Points int,
	@Series int,
    @CurrentSeries int,
    @Rank int,
    @DeathCount int,
	@GameCount int
AS
BEGIN
    set nocount on;

update dbo.dayRating

set LoginId = @LoginId, PersonType = @PersonType, Name = @Name, KillCount = @KillCount,
Points = @Points, Series = @Series, CurrentSeries = @CurrentSeries, Rank = @Rank,
DeathCount = @DeathCount, GameCount = @GameCount

where Id = @Id
END


