﻿CREATE PROCEDURE [dbo].[LoginRatingSave]
	@LoginId int,
	@Data nvarchar(max)
AS
BEGIN
	set nocount on;

	IF(not exists(SELECT 1 FROM [LoginRating] WHERE [LoginId] = @LoginId))
		BEGIN
			INSERT INTO [LoginRating] ([LoginId],[Data]) VALUES (@LoginId,@Data)
		END
	ELSE
		BEGIN
			UPDATE [LoginRating] SET [Data] = @Data	WHERE [LoginId] = @LoginId
		END
END