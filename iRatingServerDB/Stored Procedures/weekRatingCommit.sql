﻿
CREATE PROCEDURE[dbo].[weekRatingCommit]
@Id int,
	@LoginId int,
	@SlotId int,
	@PersonType int,
	@Name nvarchar(max), 
	@KillCount int,
	@PvpPoints int,
	@Series int,
    @CurrentSeries int,
    @Rank int,
    @DeathCount int,
	@GearScore int,
	@RatingPlace int,
	@PayFineCount int,
	@GiveReward bit,
	@elo_coop_rating int,
	@elo_solo_rating int,
	@elo_team_rating int,

	@GameStatistics nvarchar(max)
AS
BEGIN
    SET NOCOUNT ON;

UPDATE dbo.weekRating

SET [LoginId] = @LoginId, [SlotId] = @SlotId, [PersonType] = @PersonType, [Name] = @Name, [KillCount] = @KillCount,
	[PvpPoints] = @PvpPoints, [Series] = @Series, [CurrentSeries] = @CurrentSeries, [Rank] = @Rank,
	[DeathCount] = @DeathCount, [GearScore] = @GearScore, [RatingPlace] = @RatingPlace, [PayFineCount] = @PayFineCount,
	[GiveReward] = @GiveReward, [elo_coop_rating] = @elo_coop_rating, [elo_solo_rating] = @elo_solo_rating, [elo_team_rating] = @elo_team_rating
WHERE [Id] = @Id

DECLARE @TempTable TABLE( [PersonId]	int,
			[ArenaTypeId] int,
			[Place1Count] int,
			[Place2Count] int,
			[Place3Count] int,
			[Place4Count] int,
			[WinCount] int,
			[TotalGamesCount] int); 

	INSERT INTO @TempTable SELECT [PersonId], 0 AS [ArenaTypeId],[Place1Count],[Place2Count],[Place3Count],[Place4Count],[WinCount],[TotalGamesCount]  FROM  OPENJSON(@GameStatistics)
			WITH (	PersonId	int '$.PersonId',
					Place1Count	int '$.SoloPlaces[0]',
					Place2Count	int '$.SoloPlaces[1]',
					Place3Count	int '$.SoloPlaces[2]', 
					Place4Count	int '$.SoloPlaces[3]',
					WinCount	int '$.SoloWinCount',
					TotalGamesCount	int '$.SoloGameCount');
	INSERT INTO @TempTable SELECT [PersonId], 100 AS [ArenaTypeId],[Place1Count],[Place2Count],[Place3Count],[Place4Count],[WinCount],[TotalGamesCount]  FROM  OPENJSON(@GameStatistics)
			WITH (	PersonId	int '$.PersonId',
					Place1Count	int '$.TdmPlaces[0]',
					Place2Count	int '$.TdmPlaces[1]',
					Place3Count	int '$.TdmPlaces[2]', 
					Place4Count	int '$.TdmPlaces[3]',
					WinCount	int '$.TdmWinCount',
					TotalGamesCount	int '$.TdmGameCount');
	INSERT INTO @TempTable SELECT [PersonId], 101 AS [ArenaTypeId],[Place1Count],[Place2Count],[Place3Count],[Place4Count],[WinCount],[TotalGamesCount]  FROM  OPENJSON(@GameStatistics)
			WITH (	PersonId	int '$.PersonId',
					Place1Count	int '$.CtfPlaces[0]',
					Place2Count	int '$.CtfPlaces[1]',
					Place3Count	int '$.CtfPlaces[2]', 
					Place4Count	int '$.CtfPlaces[3]',
					WinCount	int '$.CtfWinCount',
					TotalGamesCount	int '$.CtfGameCount');
	INSERT INTO @TempTable SELECT [PersonId], 201 AS [ArenaTypeId],[Place1Count],[Place2Count],[Place3Count],[Place4Count],[WinCount],[TotalGamesCount]  FROM  OPENJSON(@GameStatistics)
			WITH (	PersonId	int '$.PersonId',
					Place1Count	int '$.WavesPlaces[0]',
					Place2Count	int '$.WavesPlaces[1]',
					Place3Count	int '$.WavesPlaces[2]', 
					Place4Count	int '$.WavesPlaces[3]',
					WinCount	int '$.WaveWinCount',
					TotalGamesCount	int '$.WaveGameCount');
	INSERT INTO @TempTable SELECT [PersonId], 103 AS [ArenaTypeId],[Place1Count],[Place2Count],[Place3Count],[Place4Count],[WinCount],[TotalGamesCount]  FROM  OPENJSON(@GameStatistics)
			WITH (	PersonId	int '$.PersonId',
					Place1Count	int '$.BrawlPlaces[0]',
					Place2Count	int '$.BrawlPlaces[1]',
					Place3Count	int '$.BrawlPlaces[2]', 
					Place4Count	int '$.BrawlPlaces[3]',
					WinCount	int '$.BrawlWinCount',
					TotalGamesCount	int '$.BrawlGameCount');

UPDATE weekPlayersMatchesCounts
	SET
		[Place1Count] = B.[Place1Count],
		[Place2Count] = B.[Place2Count],
		[Place3Count] = B.[Place3Count],
		[Place4Count] = B.[Place4Count],
		[WinCount] = B.[WinCount],
		[TotalGamesCount] = B.[TotalGamesCount]
		FROM
			weekPlayersMatchesCounts AS A
			INNER JOIN  @TempTable AS B ON A.[PersonId] = B.[PersonId] AND A.[ArenaTypeId] = B.[ArenaTypeId]
	WHERE A.[PersonId] =  @Id;
END

