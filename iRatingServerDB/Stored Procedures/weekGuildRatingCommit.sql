﻿
Create PROCEDURE[dbo].[weekGuildRatingCommit]
	@Id int,
	@GuildId int,
	@GuildName nvarchar(max),
	@GuildType int, 

	@GuildSymbolId int,
	@GuildSymbolColor nchar(20),
	@GuildBgColor nchar(20),

	@GuildRank int,
	@KillCount int,
	@Points int,
	@DeathCount int,
	@Wins int,
	@Looses int,
	@Draw int,
    @GameCount int
AS
BEGIN
    set nocount on;

update dbo.weekGuildRating

set GuildId = @GuildId, GuildName= @GuildName, GuildType = @GuildType, GuildSymbolId = @GuildSymbolId, 
GuildSymbolColor = @GuildSymbolColor, GuildBgColor = @GuildBgColor, GuildRank= @GuildRank, 
KillCount= @KillCount, Points= @Points, DeathCount=@DeathCount , Wins= @Wins, Looses= @Looses, Draw=@Draw , GameCount= @GameCount

where Id = @Id
END

