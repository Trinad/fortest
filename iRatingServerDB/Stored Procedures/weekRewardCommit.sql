﻿
Create PROCEDURE[dbo].[weekRewardCommit]
	@Id int,
	@LoginId int,
	@RewardId int
AS
BEGIN
    set nocount on;

update dbo.weekReward

set LoginId = @LoginId, RewardId = @RewardId

where Id = @Id
END

