﻿

CREATE PROCEDURE [dbo].[weekRatingCreate]
    @Id int,
	@LoginId int,
	@SlotId int,
	@PersonType int,
	@Name nvarchar(max), 
	@KillCount int,
	@PvpPoints int,
	@Series int,
    @CurrentSeries int,
    @Rank int,
    @DeathCount int,
	@GearScore int,
	@RatingPlace int,
	@PayFineCount int,
	@GiveReward bit,
	@elo_coop_rating int,
	@elo_solo_rating int,
	@elo_team_rating int,
	@GameStatistics  nvarchar(max)
AS
BEGIN
    SET NOCOUNT ON;

	IF(not exists(SELECT 1 FROM [weekRating] WHERE [Id] = @Id))
	BEGIN
		INSERT INTO dbo.weekRating ( [Id], [LoginId], [SlotId], [PersonType], [Name], [KillCount], [PvpPoints], [Series], [CurrentSeries], [Rank], [DeathCount], 
			[GearScore], [RatingPlace], [PayFineCount], [GiveReward], [elo_coop_rating], [elo_solo_rating], [elo_team_rating])
		VALUES (@Id, @LoginId, @SlotId, @PersonType, @Name, @KillCount, @PvpPoints, @Series, @CurrentSeries, @Rank, @DeathCount,
			@GearScore, @RatingPlace, @PayFineCount, @GiveReward, @elo_coop_rating, @elo_solo_rating, @elo_team_rating)

		INSERT INTO weekPlayersMatchesCounts SELECT @Id, 0   AS ArenaTypeId, 0, 0, 0, 0, 0, 0
		INSERT INTO weekPlayersMatchesCounts SELECT @Id, 100 AS ArenaTypeId, 0, 0, 0, 0, 0, 0
		INSERT INTO weekPlayersMatchesCounts SELECT @Id, 101 AS ArenaTypeId, 0, 0, 0, 0, 0, 0
		INSERT INTO weekPlayersMatchesCounts SELECT @Id, 201 AS ArenaTypeId, 0, 0, 0, 0, 0, 0
		INSERT INTO weekPlayersMatchesCounts SELECT @Id, 103 AS ArenaTypeId, 0, 0, 0, 0, 0, 0
	END
END

