﻿CREATE procedure ReplacePersonNames
	@mask nvarchar(20) -- N'%Пизд%'
	AS
 BEGIN
	 declare @number int;
	 declare @repeat int;
	 declare @ID int;
	
	SELECT @number = (ISNULL ((max(convert(int,SUBSTRING([name], 10, len([name])- 9)))),0) + 1) FROM 
		(SELECT [LoginId]
		, JSON_VALUE([Data],N'$.Name') as [name]
		FROM [dbo].[LoginRating]) as sel
		WHERE [name] Like(N'без_имени%')

	 SELECT @repeat = count(1) 
	 FROM (
			SELECT [LoginId]
				, JSON_VALUE([Data],N'$.Name') AS [name]
			  FROM [dbo].[LoginRating] 
			  ) AS sel 
	WHERE sel.[name] Like(@mask)  
	SELECT @repeat as [count]

	 WHILE(@repeat > 0)
	  BEGIN 

		UPDATE [dbo].[LoginRating]
		SET [Data] = JSON_MODIFY([Data], N'$.Name',N'без_имени'+ REPLICATE('0', 4 - LEN(@number)) + RTRIM(@number))
		WHERE [LoginId] in (SELECT TOP (1) [LoginId] FROM (
			SELECT [LoginId]
				, JSON_VALUE([Data],N'$.Name') AS [name]
			  FROM [dbo].[LoginRating] 
			  ) AS sel 
			WHERE sel.[name] Like(@mask) )

		SET @repeat = @repeat - 1;
		SET @number = @number + 1;
	  END

  END