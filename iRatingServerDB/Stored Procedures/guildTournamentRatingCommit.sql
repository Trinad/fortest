﻿
CREATE PROCEDURE[dbo].[guildTournamentRatingCommit]
@Id int,
	@GuildId int,
	@GuildRank int,
	@GuildName nvarchar(max), 
	@KillCount int,
	@Points int,
	@DeathCount int,
	@Wins int,
	@Looses int,
	@Draw int,
    @GameCount int
AS
BEGIN
    set nocount on;

update dbo.tournamentGuildRating

set GuildId = @GuildId, GuildRank = @GuildRank, GuildName = @GuildName, KillCount = @KillCount,
Points = @Points, DeathCount = @DeathCount, Wins = @Wins, Looses = @Looses, Draw = @Draw, GameCount = @GameCount

where Id = @Id
END


