﻿

CREATE PROCEDURE [dbo].[guildDayRatingCreate]
    @Id int,
@GuildId int,
@GuildRank int,
@GuildName nvarchar(max), 
	@KillCount int,
	@Points int,
	@DeathCount int,
	@Wins int,
	@Looses int,
	@Draw int,
    @GameCount int
AS
BEGIN
    set nocount on;

insert into dbo.dayGuildRating ( Id, GuildId, GuildRank, GuildName, KillCount, Points, DeathCount, Wins, Looses, Draw, GameCount)

    values(@Id, @GuildId, @GuildRank, @GuildName, @KillCount, @Points, @DeathCount, @Wins, @Looses, @Draw, @GameCount )
END


