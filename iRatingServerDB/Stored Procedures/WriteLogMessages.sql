﻿
CREATE PROCEDURE[dbo].[WriteLogMessages]
@Title NVARCHAR(MAX),
	@Message NVARCHAR(MAX)
AS
BEGIN

    set nocount on;

	insert into LogMessages([Title], [Message]) values(@Title, @Message)
END


