﻿
CREATE PROCEDURE[dbo].[dayRatingCreate]
@Id int,
	@LoginId int,
	@PersonType int,
	@Name nvarchar(max), 
	@KillCount int,
	@Points int,
	@Series int,
    @CurrentSeries int,
    @Rank int,
    @DeathCount int,
	@GameCount int
AS
BEGIN
    set nocount on;

insert into dbo.dayRating ( Id, LoginId, PersonType, Name, KillCount, Points, Series, CurrentSeries, Rank, DeathCount, GameCount )

    values(@Id, @LoginId, @PersonType, @Name, @KillCount, @Points, @Series, @CurrentSeries, @Rank, @DeathCount, @GameCount )
END


