﻿
CREATE PROCEDURE [dbo].[weekRatingLoad]
AS
BEGIN
	set nocount on;

	select wr.*,wp1.Place1Count as SoloPlaces1
	,wp1.Place2Count as SoloPlaces2
	,wp1.Place3Count as SoloPlaces3
	,wp1.Place4Count as SoloPlaces4
	,wp1.WinCount as SoloWinCount
	,wp1.TotalGamesCount as SoloGameCount

	,wp2.Place1Count as TdmPlaces1
	,wp2.Place2Count as TdmPlaces2
	,wp2.Place3Count as TdmPlaces3
	,wp2.Place4Count as TdmPlaces4
	,wp2.WinCount as TdmWinCount
	,wp2.TotalGamesCount as TdmGameCount

	,wp3.Place1Count as CtfPlaces1
	,wp3.Place2Count as CtfPlaces2
	,wp3.Place3Count as CtfPlaces3
	,wp3.Place4Count as CtfPlaces4
	,wp3.WinCount as CtfWinCount
	,wp3.TotalGamesCount as CtfGameCount

	,wp4.Place1Count as WavesPlaces1
	,wp4.Place2Count as WavesPlaces2
	,wp4.Place3Count as WavesPlaces3
	,wp4.Place4Count as WavesPlaces4
	,wp4.WinCount as WavesWinCount
	,wp4.TotalGamesCount as WavesGameCount

	,wp5.Place1Count as BrawlPlaces1
	,wp5.Place2Count as BrawlPlaces2
	,wp5.Place3Count as BrawlPlaces3
	,wp5.Place4Count as BrawlPlaces4
	,wp5.WinCount as BrawlWinCount
	,wp5.TotalGamesCount as BrawlGameCount

	 from dbo.weekRating as wr
	left join WeekPlayersMatchesCounts as wp1 on wp1.[PersonId] = wr.SlotId and wp1.ArenaTypeId =0
	left join WeekPlayersMatchesCounts as wp2 on wp2.[PersonId] = wr.SlotId and wp2.ArenaTypeId =100
	left join WeekPlayersMatchesCounts as wp3 on wp3.[PersonId] = wr.SlotId and wp3.ArenaTypeId =101
	left join WeekPlayersMatchesCounts as wp4 on wp4.[PersonId] = wr.SlotId and wp4.ArenaTypeId =201
	left join WeekPlayersMatchesCounts as wp5 on wp5.[PersonId] = wr.SlotId and wp5.ArenaTypeId =103
END


