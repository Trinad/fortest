﻿CREATE TABLE [dbo].[LogMessages] (
    [Id]      INT            IDENTITY (1, 1) NOT NULL,
    [Date]    DATETIME       DEFAULT (GETUTCDATE()) NOT NULL,
    [Title]   NVARCHAR (MAX) NULL,
    [Message] NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

