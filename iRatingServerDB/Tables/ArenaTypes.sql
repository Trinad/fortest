﻿CREATE TABLE [dbo].[ArenaTypes](
	[ArenaTypeId] [int] NOT NULL,
	[ArenaTypeName] [nvarchar](50) NOT NULL,
	[Avalible] [int]
) ON [PRIMARY]
GO
ALTER TABLE [ArenaTypes]
ADD CONSTRAINT PK_ArenaTypes PRIMARY KEY CLUSTERED (ArenaTypeId)
