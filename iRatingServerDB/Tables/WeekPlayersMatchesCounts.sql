﻿CREATE TABLE [dbo].[WeekPlayersMatchesCounts](
	[PersonId] [int] NOT NULL,
	[ArenaTypeId] [int] NOT NULL,
	[Place1Count] [int] NOT NULL,
	[Place2Count] [int] NOT NULL,
	[Place3Count] [int] NOT NULL,
	[Place4Count] [int] NOT NULL,
	[WinCount] [int] NOT NULL,
	[TotalGamesCount] [int] NOT NULL
) ON [PRIMARY]
Go
ALTER TABLE WeekPlayersMatchesCounts
ADD CONSTRAINT PK_PlayersMatchesCounts PRIMARY KEY CLUSTERED (PersonId, ArenaTypeId)

GO  
ALTER TABLE WeekPlayersMatchesCounts     
ADD CONSTRAINT FK_WeekPlayersMatchesCounts_ArenaType FOREIGN KEY (ArenaTypeId)     
    REFERENCES ArenaTypes (ArenaTypeId)
