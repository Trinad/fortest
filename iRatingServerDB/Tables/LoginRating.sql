﻿CREATE TABLE [dbo].[LoginRating](
	[LoginId] [int] NOT NULL,
	[Data] [nvarchar](max) NOT NULL);

GO

ALTER TABLE [LoginRating]
ADD CONSTRAINT PK_LoginRating PRIMARY KEY CLUSTERED (LoginId);