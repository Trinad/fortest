﻿CREATE TABLE [dbo].[tournamentGuildRating] (
    [Id]         INT            NOT NULL,
    [GuildId]    INT            NOT NULL,
    [GuildRank]  INT            NOT NULL,
    [GuildName]  NVARCHAR (MAX) NOT NULL,
    [KillCount]  INT            NOT NULL,
    [Points]     INT            NOT NULL,
    [DeathCount] INT            NOT NULL,
    [Wins]       INT            DEFAULT ((0)) NOT NULL,
    [Looses]     INT            DEFAULT ((0)) NOT NULL,
    [Draw]       INT            DEFAULT ((0)) NOT NULL,
    [GameCount]  INT            DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

