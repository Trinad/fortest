﻿CREATE TABLE [dbo].[weekRating] (
    [Id]            INT            NOT NULL,
    [LoginId]       INT            NOT NULL,
    [SlotId]        INT            NOT NULL,
    [PersonType]    INT            NOT NULL,
    [Name]          NVARCHAR (MAX) NOT NULL,
    [KillCount]     INT            NOT NULL,
    [PvpPoints]     INT            NOT NULL,
    [Series]        INT            NOT NULL,
    [CurrentSeries] INT            NOT NULL,
    [Rank]          INT            NOT NULL,
    [DeathCount]    INT            NOT NULL,
    [GearScore]     INT            CONSTRAINT [DF_globalRating_GearScore] DEFAULT ((0)) NOT NULL,
    [RatingPlace]   INT            CONSTRAINT [DF_globalRating_RatingPlace] DEFAULT ((0)) NOT NULL,
    [PayFineCount]  INT            CONSTRAINT [DF_globalRating_PayFineCount] DEFAULT ((0)) NOT NULL,
    [GiveReward]    BIT            CONSTRAINT [DF_globalRating_GiveReward] DEFAULT ((0)) NOT NULL,
    [elo_solo_rating] INT NOT NULL DEFAULT ((0)), 
    [elo_coop_rating] INT NOT NULL DEFAULT ((0)), 
    [elo_team_rating] INT NOT NULL DEFAULT ((0)), 
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

