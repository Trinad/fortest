﻿CREATE TABLE [dbo].[weekReward] (
    [Id]         INT      CONSTRAINT [DF_weekReward_Id] DEFAULT ((0)) NOT NULL,
    [LoginId]    INT      CONSTRAINT [DF_weekReward_LoginId] DEFAULT ((0)) NOT NULL,
    [RewardId]   INT      NOT NULL,
    [RemoveDate] DATETIME CONSTRAINT [DF_weekReward_RemoveDate] DEFAULT ((0)) NOT NULL
);

