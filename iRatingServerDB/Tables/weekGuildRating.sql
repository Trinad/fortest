﻿CREATE TABLE [dbo].[weekGuildRating] (
    [Id]               BIGINT         NOT NULL,
    [GuildId]          INT            NOT NULL,
    [GuildName]        NVARCHAR (MAX) NOT NULL,
    [GuildType]        INT            NOT NULL,
    [GuildSymbolId]    INT            CONSTRAINT [DF_weekGuildRating_GuildSymbolId] DEFAULT ((0)) NOT NULL,
    [GuildSymbolColor] NCHAR (20)     CONSTRAINT [DF_weekGuildRating_GuildSymbolColor] DEFAULT (N'#ffffff') NOT NULL,
    [GuildBgColor]     NCHAR (20)     CONSTRAINT [DF_weekGuildRating_GuildBgColor] DEFAULT (N'#ffffff') NOT NULL,
    [GuildRank]        INT            CONSTRAINT [DF_weekGuildRating_GuildRank] DEFAULT ((0)) NOT NULL,
    [KillCount]        INT            NOT NULL,
    [Points]           INT            NOT NULL,
    [DeathCount]       INT            NOT NULL,
    [Wins]             INT            CONSTRAINT [DF__globalGuil__Wins__44FF419A] DEFAULT ((0)) NOT NULL,
    [Looses]           INT            CONSTRAINT [DF__globalGui__Loose__45F365D3] DEFAULT ((0)) NOT NULL,
    [Draw]             INT            CONSTRAINT [DF__globalGuil__Draw__46E78A0C] DEFAULT ((0)) NOT NULL,
    [GameCount]        INT            CONSTRAINT [DF__globalGui__GameC__52593CB8] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK__globalGu__3214EC07BB259311] PRIMARY KEY CLUSTERED ([Id] ASC)
);

