﻿CREATE TABLE [dbo].[tournamentRating] (
    [Id]            INT            NOT NULL,
    [LoginId]       INT            NOT NULL,
    [PersonType]    INT            NOT NULL,
    [Name]          NVARCHAR (MAX) NOT NULL,
    [KillCount]     INT            NOT NULL,
    [Points]        INT            NOT NULL,
    [Series]        INT            NOT NULL,
    [CurrentSeries] INT            DEFAULT ((0)) NULL,
    [Rank]          INT            DEFAULT ((0)) NULL,
    [DeathCount]    INT            DEFAULT ((0)) NOT NULL,
    [GameCount]     INT            DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

