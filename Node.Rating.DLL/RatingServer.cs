﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using Assets.Scripts.Utils.Logger;
using CoreLite;
using Fragoria.Common.Utils.Pool;
using Fragoria.Common.Utils.Threads;
using Node.Rating.Db;
using Node.Rating.Logic;
using Node.Rating.Network;
using Node.Rating.Protocols;
using Node.Rating.Utils;
using UtilsLib;
using UtilsLib.Logic.Enums;
using UtilsLib.low_engine_sharp.transport;
using UtilsLib.Logic.ServerStart;

namespace Node.Rating
{
    public class RatingServer : IStartGameManager
    {
        public PerformanceStatisticsManager PerformanceManager { get; private set; }

        public static TimeManager TimeManager = new TimeManager();
        
        private static readonly string _versionInfo = System.Reflection.Assembly.GetExecutingAssembly()
            .GetCustomAttributes(inherit: false)
            .OfType<System.Reflection.AssemblyInformationalVersionAttribute>()
            .Single().InformationalVersion;
        public static string VersionInfo { get { return _versionInfo; } }

        public void Stop()
        {
            try
            {
                Log("Попытка останова сервера ...");
                //if (StatisticManager != null)
                //    StatisticManager.Stop();

                //if (UnloadManager != null)
                //    UnloadManager.Stop();
                ConnectionManager?.Stop();
                RatingManager?.Stop();
                //RewardManager?.Stop();
                DBStoreManager?.Stop();

				TimeManager.Stop();
                ILogger.Instance.Send("TimeManager stop.");

                PoolManager.Instance.Stop();
                ILogger.Instance.Send("PoolManager stop.");

                PerformanceManager.Stop();
                ILogger.Instance.Send("m_PerformanceManager stop.");

                //Cache.Stop();
                Log("Кэш остановлен ...");

                transport.Stop();
                Log("Транспорт остановлен ...");
				
				FRRandom.Stop();

				//Ждем всех минуту, кто не успел, тот опоздал
				for (int i = 0; i < 60 && (transport.ThreadCount > 0 || OneThreadTimer.ThreadCount > 0); i++)
				{
					//сказать серверу что нужно подождать секунды 1
					ILogger.Instance.Send("Wait 1 seconds...");
					Thread.Sleep(1000);
				}

                //Status = ServerStatuses.Stopped;

                Sos.Debug("RatingServer остановлен ...");
            }
            catch (Exception exception)
            {
                //Status = ServerStatuses.StopError;
				ILogger.Instance.Send("Остановка сервера завершилась неудачей. Ошибка " + exception, ErrorLevel.error);
				throw;
			}
			finally
			{
				ILogger.Instance.Shutdown();
			}
		}

        public void Start()
        {
            try
            {
                SysExternalApi.SetServer(this);

                OneThreadTimer.logMessageFunc = LogMessageFromOneThreadTimer;
                OneThreadTimer.logExceptionFunc = LogExceptionFromOneThreadTimer;

                PerformanceManager = new PerformanceStatisticsManager(
                    SystemConfig.bPerformanceStatisticsEnabled,
                    SystemConfig.sStasdHost,
                    SystemConfig.sStatsdPort,
                    SystemConfig.sStatsdServerName("Shooter.Rating", "."),
                    200);
                BaseCmdSystem.SetPerformanceStatisticsManager(PerformanceManager);
                OneThreadTimer.SetPerformanceStatisticsManager(PerformanceManager);
                //m_PerformanceManager.RegisterAddinEvents(Enum.GetNames(typeof(UtilsLib.AddinEvents.Enums.AddInsEventTypes)));
                PerformanceManager.Start(ThreadPriority.Normal);
                ILogger.Instance.Send("PerformanceManager started!");
                
                TimeManager.Start(ThreadPriority.Normal);
                ILogger.Instance.Send("TimeManager started!");

                ILogger.Instance.Send($"Version: {VersionInfo}");

                PoolManager.Instance.Start(ThreadPriority.Normal);
                ILogger.Instance.Send("PoolManager started!");

                Entry.GetAdapterCallback = GetAdapter;
                //Entry.EntryCreateCallback = EntryCreate;
                RegisterEntryTypes();
                ILogger.Instance.Send("EntryTypes registered!");
                //AuthManager = new AuthManager();
                //NodeManager = new NodeManager();

                InitTransport();
                ILogger.Instance.Send("Transport inited!");
                RegisterTransportSystem();
                ILogger.Instance.Send("Transport registered!");
                StartManagers();
                ILogger.Instance.Send("Managers started!");

                var dbAdapter = GetAdapter();
				dbAdapter.LoadAllLoginRating(RatingManager.LoginStarage);
                ILogger.Instance.Send("LoginRating Loaded!");
                //dbAdapter.LoadReward("week", RewardManager.WeekReward);

                dbAdapter.LoadGuildRating("week", RatingManager.WeekGuildRating);
                ILogger.Instance.Send("GuildRating Loaded!");

                ILogger.Instance.Send("LoginRating setted!");

                //GetAdapter(null).LoadRating("day", RatingManager.DayRating);
                //GetAdapter(null).LoadRating("tournament", RatingManager.TournamentRating);
                //GetAdapter(null).LoadGuildRating("day", RatingManager.DayRating._ratingGuild);
                //GetAdapter(null).LoadGuildRating("tournament", RatingManager.TournamentRating._ratingGuild);

                //if (SystemConfig.LoadAllLogins)
                //    GetAdapter(null).LoadLogins();
                //else
                //    GetAdapter(null).LoginIdsGet();

                StartTransport();
                ILogger.Instance.Send("Transport started!");

                bStarted = true;

                ILogger.Instance.Send("RatingServer: старт успешен");
            }
            catch (Exception exx)
            {
                ILogger.Instance.Send(exx.ToString());
				Stop();
				throw;
			}
        }

        private void StartTransport()
        {
            Sos.Debug("transport.Start(): стартует...");
            transport.Start();
            Sos.Debug("RatingServer:","transport.Start() успешно");
            Dispatcher.Start();

            Sos.Debug("StartTransport");
        }

        public bool bStarted = false;

        public FlashPolicyFile SysFlashPolicyFile { get; private set; }
        public SysDispatcher Dispatcher { get; private set; }
        public ConnectionManager ConnectionManager { get; private set; }
        public RatingManager RatingManager { get; private set; }
        //public RewardManager RewardManager { get; private set; }
        //public StatisticManager StatisticManager { get; private set; }
        //public UnloadManager UnloadManager { get; private set; }
        public DBStoreManager DBStoreManager { get; private set; }

        private void RegisterTransportSystem()
        {
            ILogger.Instance.Send("Register SysSysDispatcher");
            Sos.Debug("RegisterTransportSystem: старт успешен");
            transport.RegisterSystem(Dispatcher = new SysDispatcher(this));
			
            transport.AddSpecCommand("crossdomain.xml", FlashPolicyFile.CommandGetCrossDomain);
            transport.AddSpecCommand(SystemConfig.ExternalApiUrl, FlashPolicyFile.CommandExternalAPI);

            ILogger.Instance.Send("Register SysFlashPolicyFile");
            transport.RegisterSystem(SysFlashPolicyFile = new FlashPolicyFile());
            //SysFlashPolicyFile.Start(); //нечего там стартовать...

            transport.OnDisconnect += OnDisconnect;
        }

        private void OnDisconnect(long connectionId, string ticket, string sIP, DisconnectReason reason)
        {
            if (SysDispatcher.DsConnection != null && SysDispatcher.DsConnection.ConnectionId == connectionId)
            {
                ILogger.Instance.Send("Lost connect to dispatcher", ErrorLevel.warning);
                SysDispatcher.DsConnection = null;
            }
        }

        public void StartManagers()
        {
            //UnloadManager = new UnloadManager();
            //UnloadManager.Start(ThreadPriority.Normal);
            //ILogger.Instance.Send(MType.ThreadNames, SType.Warning, "UnloadManager: " + UnloadManager.iThreadId.ToString(CultureInfo.InvariantCulture));

            DBStoreManager = new DBStoreManager();
            DBStoreManager.Start(ThreadPriority.Normal);
            ILogger.Instance.Send("DBStoreManager: " + DBStoreManager.iThreadId.ToString(CultureInfo.InvariantCulture), ErrorLevel.warning);

            ConnectionManager = new ConnectionManager(this, 2000);
            ConnectionManager.Start(ThreadPriority.Normal);
            ILogger.Instance.Send("ConnectionManager: " + ConnectionManager.iThreadId.ToString(CultureInfo.InvariantCulture), ErrorLevel.warning);

            RatingManager = new RatingManager(this, SystemConfig.iRatingManagerTimeout);
            RatingManager.Start(ThreadPriority.Normal);
            ILogger.Instance.Send("RatingManager: " + RatingManager.iThreadId.ToString(CultureInfo.InvariantCulture), ErrorLevel.warning);

            //RewardManager = new RewardManager(2000);
            //RewardManager.Start(ThreadPriority.Normal);
            //ILogger.Instance.Send(MType.ThreadNames, SType.Warning, "RewardManager: " + RewardManager.iThreadId.ToString(CultureInfo.InvariantCulture));

            ILogger.Instance.Send("RatingServer: StartManagers");
            //StatisticManager = new StatisticManager();
            //StatisticManager.Start(ThreadPriority.Normal);
            //ILogger.Instance.Send(MType.ThreadNames, SType.Warning, "StatisticManager: " + StatisticManager.iThreadId.ToString(CultureInfo.InvariantCulture));
        }

        private static void InitTransport()
        {
            transport.IsTotalLoggingEnabled = SystemConfig.IsTotalTransportLoggingEnabled;
            transport.default_dos_border = SystemConfig.iDOSAttackMessageCount;
            transport.ZipPacketMinSize = SystemConfig.ZipPacketMinSize;

            ILogger.Instance.Send("Init transport!");
            transport.Init(SystemConfig.MaxOnline,
                           SystemConfig.CtrlPortFrom,
                           SystemConfig.sServerName,
                           SystemConfig.sServerPort,
                           SystemConfig.sAdditionalIPs,
                           SystemConfig.sAdditionalPorts,
                           SystemConfig.TransportType,
                           TimeManager);

            transport.SetSendTimeout(SystemConfig.TransportSendTimeoutMS);

            Sos.Debug("InitTransport");
        }


        private static void LogMessageFromOneThreadTimer(string msg)
        {
            ILogger.Instance.Send(msg);
        }

        private static void LogExceptionFromOneThreadTimer(Exception ex)
        {
            ILogger.Instance.Send(ex.ToString(), ErrorLevel.error);
        }

        static void RegisterEntryTypes()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int eCount = 0;
            for (var i = 1; i < (int)EntryTypes.Count; i++)
            {
                Entry.RegisterType(i, ((EntryTypes)i).ToString());
                stringBuilder.AppendFormat("EntryType: {0} \n", ((EntryTypes) i).ToString());
                eCount++;
            }

            Sos.Debug("RegisterEntryTypes: eCount: " + eCount, stringBuilder.ToString());
        }
        public void Log(string s)
        {
            ILogger.Instance.Send(s);
            Sos.Debug(s);
        }

        public static MSSQLAdapter GetAdapter()
        {
            return Db.DBStoreManager.GetAdapter();
        }
    }
}
