﻿using System.IO;
using System.Reflection;

namespace Node.Rating.Utils
{
    public class PathBuilder
    {
        public string RewardXml(string filename)
        {
            return Path.Combine(GetPath(), filename);
        }

        public string GetXmlPath(string filename)
        {
            return Path.Combine(GetPath(), filename);
        }

        private string GetPath()
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Xml");
            return path;
        }
    }
}