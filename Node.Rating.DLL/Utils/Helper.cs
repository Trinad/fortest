﻿using System;
using System.Collections.Generic;

namespace Node.Rating.Utils
{
    public static class Helper
    {
        // пока оставлю пригодится

        public static TItem MaxByKey<TItem, TKey>(this IEnumerable<TItem> items, Func<TItem, TKey> keySelector)
        {
            var comparer = Comparer<TKey>.Default;

            var enumerator = items.GetEnumerator();
            if (!enumerator.MoveNext())
                throw new InvalidOperationException("Collection is empty.");

            TItem maxItem = enumerator.Current;
            TKey maxKey = keySelector(maxItem);

            while (enumerator.MoveNext())
            {
                TKey key = keySelector(enumerator.Current);
                if (comparer.Compare(key, maxKey) > 0)
                {
                    maxItem = enumerator.Current;
                    maxKey = key;
                }
            }

            return maxItem;
        }

        public static TItem MinByKey<TItem, TKey>(this IEnumerable<TItem> items, Func<TItem, TKey> keySelector)
        {
            var comparer = Comparer<TKey>.Default;

            var enumerator = items.GetEnumerator();
            if (!enumerator.MoveNext())
                throw new InvalidOperationException("Collection is empty.");

            TItem minItem = enumerator.Current;
            TKey minKey = keySelector(minItem);

            while (enumerator.MoveNext())
            {
                TKey key = keySelector(enumerator.Current);
                if (comparer.Compare(key, minKey) < 0)
                {
                    minItem = enumerator.Current;
                    minKey = key;
                }
            }

            return minItem;
        }
    }
}
