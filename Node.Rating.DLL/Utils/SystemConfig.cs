﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Assets.Scripts.Utils.Logger;

namespace Node.Rating.Utils
{
    public class SystemConfig : ILogConfig
    {
        //static readonly NameValueCollection Nc = ConfigurationManager.AppSettings;

        private static NameValueCollection Nc;// = ConfigurationManager.AppSettings;

        public SystemConfig(NameValueCollection nc)
        {
            Nc = nc;
        }

        #region methods
        private static string GetParamFromConfig(string name, string defaultValue)
        {
            var sRes = Nc.Get(name);
            if (String.IsNullOrEmpty(sRes))
                sRes = defaultValue;
            return sRes;
        }

        private static bool GetParamFromConfig(string name, bool defaultValue)
        {
            string sRes = Nc.Get(name);
            return String.IsNullOrEmpty(sRes) ? defaultValue : bool.Parse(sRes);
        }

        private static int GetParamFromConfig(string name, int defaultValue)
        {
            string sRes = Nc.Get(name);
            return String.IsNullOrEmpty(sRes) ? defaultValue : int.Parse(sRes);
        }
        private static int NcParse(string name, int value)
        {
            string res = Nc.Get(name);
            int iRes;
            if (res != null && int.TryParse(res, out iRes))
            {
                return iRes;
            }
            return value;
        }
        public static string[] sStringArr(string sPrefix)
        {
            bool bFound = true;
            int i = 1;
            List<string> alRes = new List<string>();

            while (bFound)
            {
                string sRes = Nc.Get(sPrefix + i.ToString());
                if (sRes != null)
                {
                    alRes.Add(sRes);
                    i++;
                }
                else
                    bFound = false;
            }

            return alRes.ToArray();
        }
        #endregion

        public static string sConnectString { get { return Nc.Get("SQLConnectString"); } }

        public static int iRecursiveOptexSpinCount
        {
            get
            {
                return 10;
            }
        }
        public static int iMaxQueueLogMessage
        {
            get { return 10000; }
        }
        static int m_MaxOnline = -1;
        /// <summary>
        /// Максимальное количество человек в онлайне
        /// </summary>
        public static int MaxOnline
        {
            get
            {
                if (m_MaxOnline == -1)
                {
                    m_MaxOnline = 100;
                    if (Nc.Get("MaxOnline") != null)
                    {
                        try
                        {
                            m_MaxOnline = int.Parse(Nc.Get("MaxOnline"));
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                return m_MaxOnline;
            }
        }
        #region Complaints
        public static int iComplaintSavePeriodMS
        {
            get
            {
                return 1000;
            }
        }
        public static int iComplaintCountPerTick
        {
            get
            {
                return 10000;
            }
        }
        #endregion

        #region transport
        private static bool? m_IsTotalTransportLoggingEnabled;
        public static bool IsTotalTransportLoggingEnabled
        {
            get
            {
                if (!m_IsTotalTransportLoggingEnabled.HasValue)
                {
                    var paramFromConfig = GetParamFromConfig("IsTotalTransportLoggingEnabled", bool.FalseString);
                    bool temp;
                    if (!bool.TryParse(paramFromConfig, out temp))
                    {
                        ILogger.Instance.Send(string.Format("Какая-то фигня в конфиге: IsTotalTransportLoggingEnabled = {0}", paramFromConfig), ErrorLevel.error);
                        temp = true;
                    }
                    m_IsTotalTransportLoggingEnabled = temp;
                }
                return m_IsTotalTransportLoggingEnabled.Value;
            }
        }
        private static int m_iZipPacketMinSize = -1;
        public static int ZipPacketMinSize
        {
            get
            {
                if (m_iZipPacketMinSize == -1)
                {
                    m_iZipPacketMinSize = Math.Max(0, NcParse("ZipPacketMinSize", 0));
                }
                return m_iZipPacketMinSize;
            }
        }

        static int m_CtrlPortFrom = -1;
        /// <summary>
        /// Максимальное количество человек в онлайне
        /// </summary>
        public static int CtrlPortFrom
        {
            get
            {
                if (m_CtrlPortFrom == -1)
                {
                    m_CtrlPortFrom = 30000;
                    if (Nc.Get("ControlPortFrom") != null)
                    {
                        try
                        {
                            m_CtrlPortFrom = int.Parse(Nc.Get("ControlPortFrom"));
                        }
                        catch (Exception)
                        {
                            throw new Exception("no control port");
                        }
                    }
                }
                return m_CtrlPortFrom;
            }
        }
        public static string[] sAdditionalIPs
        {
            get
            {
                return sStringArr("ServerName");
            }
        }

        public static string[] sAdditionalPorts
        {
            get
            {
                return sStringArr("ServerPort");
            }
        }

        public static UtilsLib.low_engine_sharp.transport.TransportType TransportType
        {
            get
            {
                string sRes = Nc.Get("TransportType");
                if (sRes == null || sRes == "")
                    return UtilsLib.low_engine_sharp.transport.TransportType.LowEngine;
                return (sRes.ToLower() == "iocp") ? UtilsLib.low_engine_sharp.transport.TransportType.IOCPTransport :
                    UtilsLib.low_engine_sharp.transport.TransportType.LowEngine;
            }
        }
        public static string sServerName
        {
            get
            {
                string sRes = Nc.Get("ServerName");
                if (string.IsNullOrEmpty(sRes))
                    sRes = "127.0.0.1";
                return sRes;
            }
        }


        public static string sServerPort
        {
            get
            {
                string sRes = Nc.Get("ServerPort");
                if (string.IsNullOrEmpty(sRes))
                    sRes = "10055";
                return sRes;
            }
        }
        private static int? m_iDOSAttackMessageCount;
        public static int iDOSAttackMessageCount
        {
            get
            {
                if (!m_iDOSAttackMessageCount.HasValue)
                {
                    var sRes = Nc.Get("DOSAttackMessageCount");
                    if (string.IsNullOrEmpty(sRes))
                        sRes = "100";
                    m_iDOSAttackMessageCount = int.Parse(sRes);
                }
                return m_iDOSAttackMessageCount.Value;
            }
        }
        private static int m_iTransportSendTimeoutMS = -1;
        public static int TransportSendTimeoutMS
        {
            get
            {
                if (m_iTransportSendTimeoutMS == -1)
                {
                    m_iTransportSendTimeoutMS = NcParse("TransportSendTimeoutMS", 50);
                }
                return m_iTransportSendTimeoutMS;
            }
        }
        #endregion

        private static bool? m_bPerformanceStatisticsEnabled;
        /// <summary>
        /// включен ли менеджер сбора и отправки статистики параметров сервера
        /// </summary>
        public static bool bPerformanceStatisticsEnabled
        {
            get
            {
                if (!m_bPerformanceStatisticsEnabled.HasValue)
                {
                    string paramFromConfig = GetParamFromConfig("PerformanceStatisticsEnabled", bool.FalseString);

                    bool temp;
                    if (!bool.TryParse(paramFromConfig, out temp))
                    {
                        ILogger.Instance.Send(string.Format("Какая-то фигня в конфиге: PerformanceStatisticsEnabled = {0}", paramFromConfig), ErrorLevel.error);
                        temp = false;
                    }

                    m_bPerformanceStatisticsEnabled = temp;
                }

                return m_bPerformanceStatisticsEnabled.Value;
            }
        }

        public static string DefaultLanguage { get { return GetParamFromConfig("DefaultLanguage", "en"); } }

        public static string sStasdHost { get { return GetParamFromConfig("StatsD_Host", "127.0.0.1"); } }
        public static string sStatsdPort { get { return GetParamFromConfig("StatsD_Port", "8080"); } }
        public static string sStatsdSuffix { get { return GetParamFromConfig("StatsD_Suffix", string.Empty); } }
        public static string sStatsdServerName(string x, string y)
        {
            if (string.IsNullOrEmpty(sStatsdSuffix))
                return x + y;
            else
                return x + sStatsdSuffix + y;
        }

        public static string ClientAuthApiAllowedHosts
        {
            get
            {
                string sRes = Nc.Get("ClientAuthApiAllowedHosts");
                if (string.IsNullOrEmpty(sRes))
                    sRes = "127.0.0.1";

                return sRes;
            }
        }

        public static string sFlashDomainPolicy
        {
            get
            {
                string sRes = Nc.Get("FlashDomainPolicy");
                return sRes ?? "{?xml version='1.0\'?}{cross-domain-policy}{allow-access-from domain='*'/}{/cross-domain-policy}";
            }
        }

        public static string DispatcherServer
        {
            get { return Nc.Get("DispatcherServer"); }
        }

        public static string DispatcherServerPort
        {
            get { return Nc.Get("DispatcherServerPort"); }
        }

        public static int iMillisecondsRecieveTimeout { get { return 3000; } }

        public static int iRatingManagerTimeout 
        { 
            get 
            {
                string ms = GetParamFromConfig("RatingManagerTimeout", "100");
                int res = 100;
                if (!int.TryParse(ms, out res))
                    res = 100;
                return res;
            } 
        }

        public static string ExternalApiKey { get { return Nc.Get("ExternalApiKey"); } }

        public static string ExternalApiUrl { get { return "/rating.php?"; } }

        public static string sServerSecretKey
        {
            get
            {
                string sRes = Nc.Get("LoginHashKey");
                return sRes;
            }
        }

        private static string m_DispatcherOfflineReason = null;
        public static string DispatcherOfflineReason 
        { 
            get 
            {
                if (m_DispatcherOfflineReason == null)
                    m_DispatcherOfflineReason = GetParamFromConfig("DispatcherOfflineReason", string.Empty);
                return m_DispatcherOfflineReason;
            } 
        }

        private static string m_WLSOfflineReason = null;
        public static string WLSOfflineReason 
        { 
            get 
            {
                if (m_WLSOfflineReason == null)
                    m_WLSOfflineReason = GetParamFromConfig("WLSOfflineReason", string.Empty);
                return m_WLSOfflineReason;
            } 
        }

        public static string LoginAccountAllowedHosts
        {
            get
            {
                string sRes = Nc.Get("LoginAccountAllowedHosts");
                if (string.IsNullOrEmpty(sRes))
                    sRes = "*";

                return sRes;
            }
        }
        public static string LoginInfoAllowedHosts
        {
            get
            {
                string sRes = Nc.Get("LoginInfoAllowedHosts");
                if (string.IsNullOrEmpty(sRes))
                    sRes = "*";

                return sRes;
            }
        }
        public static string LoginChangeAllowedHosts
        {
            get
            {
                string sRes = Nc.Get("LoginChangeAllowedHosts");
                if (string.IsNullOrEmpty(sRes))
                    sRes = "*";

                return sRes;
            }
        }
        private static bool? _sentryEnabled;
        public static bool SentryEnabled
        {
            get
            {
                if (!_sentryEnabled.HasValue)
                {
                    string paramFromConfig = GetParamFromConfig("SentryEnabled", bool.FalseString);

                    bool temp;
                    if (!bool.TryParse(paramFromConfig, out temp))
                    {
                        ILogger.Instance.Send(string.Format("Какая-то фигня в конфиге: SentryEnabled = {0}", paramFromConfig), ErrorLevel.error);
                        temp = false;
                    }

                    _sentryEnabled = temp;
                }
                return _sentryEnabled.Value;
            }
        }

        private static string _sentryUrl;
        public static string SentryUrl
        {
            get
            {
                if (_sentryUrl == null)
                    _sentryUrl = GetParamFromConfig("SentryUrl", "");

                return _sentryUrl;
            }
        }

        public static int SavePeriodSec
        {
            get
            {
                var result = Convert.ToInt32(Nc.Get("SavePeriod"));
                if (result == 0)
                {
                    result = 300;
                }
                return result;
            }
        }

        private static DateTime _buildTime = System.IO.File.GetLastWriteTime(System.Reflection.Assembly.GetExecutingAssembly().Location);
        public static DateTime BuildTime { get { return _buildTime; } }

        public static string sAuthServersCode { get { return "AS"; } }
        public static string sDispatcherCode { get { return "RT"; } }
        public static string sTranslateCode { get { return "TR"; } }

        private static bool? m_SosEnable = null;
        public static bool SosEnable
        {
            get
            {
                if (m_SosEnable == null)
                    m_SosEnable = GetParamFromConfig("SosEnable", false);
                return m_SosEnable.Value;
            }
        }

        private static int m_SosPort = -1;
        public static int SosPort
        {
            get
            {
                if (m_SosPort == -1)
                    m_SosPort = GetParamFromConfig("SosPort", -1);
                return m_SosPort;
            }
        }

        private static string m_SosIp;
        public static string SosIp
        {
            get
            {
                if (m_SosIp == null)
                    m_SosIp = GetParamFromConfig("SosIp", string.Empty);
                return m_SosIp;
            }
        }

		public ErrorLevel MinLogLevel { get; } = ErrorLevel.debug;
		public bool EnableUlink { get; } = false;
		public SosConfig Sos => new SosConfig { enabled = SosEnable, ip = SosIp, port = SosPort, logPrefix = "Rating" };

#if DEBUG
		public bool bDebugLogger { get; } = true;
		public bool ConsoleLog { get; } = true;
#else
		public bool bDebugLogger { get; } = false;
		public bool ConsoleLog { get; } = false;
#endif

		public NLogConfig nlog => new NLogConfig { enable = true, ConfigurationFileName = "NLog.config" };
	}
}
