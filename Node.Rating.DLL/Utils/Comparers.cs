﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Node.Rating.Logic.Records;

namespace Node.Rating.Utils
{
    class Comparers
    {
        public class GuildKillCountComparer : IComparer<OneGuildRatingRecord>
        {
            public int Compare(OneGuildRatingRecord r1, OneGuildRatingRecord r2)
            {
                if (r1 == null)
                    return -1;
                if (r2 == null)
                    return 1;

                if (r1.killCounter != r2.killCounter)
                    return r2.killCounter - r1.killCounter;

                if (r1.guildRank != r2.guildRank)
                    return r2.guildRank - r1.guildRank;

                return r2.Data.ClanId - r1.Data.ClanId;
            }
        }

        public class GuildDeathCountComparer : IComparer<OneGuildRatingRecord>
        {
            public int Compare(OneGuildRatingRecord r1, OneGuildRatingRecord r2)
            {
                if (r1 == null)
                    return -1;
                if (r2 == null)
                    return 1;

                if (r1.deathCounter != r2.deathCounter)
                    return r2.deathCounter - r1.deathCounter;

                if (r1.guildRank != r2.guildRank)
                    return r2.guildRank - r1.guildRank;

                return r2.Data.ClanId - r1.Data.ClanId;
            }
        }

        public class GuildXPCountComparer : IComparer<OneGuildRatingRecord>
        {
            public int Compare(OneGuildRatingRecord r1, OneGuildRatingRecord r2)
            {
                if (r1 == null)
                    return -1;
                if (r2 == null)
                    return 1;

                if (r1.pvpPoints != r2.pvpPoints)
                    return r2.pvpPoints - r1.pvpPoints;

                return r2.Data.ClanId - r1.Data.ClanId;
            }
        }

        public class LoginGlobalComparer : IComparer<OneLoginRatingRecord>
        {
            public int Compare(OneLoginRatingRecord r1, OneLoginRatingRecord r2)
            {
                if (r1 == null)
                    return -1;
                if (r2 == null)
                    return 1;

                if (r1.SummBestPoints != r2.SummBestPoints)
                    return r2.SummBestPoints - r1.SummBestPoints;

                return r2.loginId - r1.loginId;
            }

            //TODO_maximpr этот метод пригодитлся бы тут
            //public int GetPoints(OneLoginRatingRecord r)
            //{
            //    return r.SummBestPoints;
            //}
        }

        public class LoginWarriorComparer : IComparer<OneLoginRatingRecord>
        {
            public int Compare(OneLoginRatingRecord r1, OneLoginRatingRecord r2)
            {
                if (r1 == null)
                    return -1;
                if (r2 == null)
                    return 1;

                if (r1.BestWarriorPoints != r2.BestWarriorPoints)
                    return r2.BestWarriorPoints - r1.BestWarriorPoints;

                return r2.loginId - r1.loginId;
            }
        }

        public class LoginArcherComparer : IComparer<OneLoginRatingRecord>
        {
            public int Compare(OneLoginRatingRecord r1, OneLoginRatingRecord r2)
            {
                if (r1 == null)
                    return -1;
                if (r2 == null)
                    return 1;

                if (r1.BestArcherPoints != r2.BestArcherPoints)
                    return r2.BestArcherPoints - r1.BestArcherPoints;

                return r2.loginId - r1.loginId;
            }
        }

        public class LoginGunnerComparer : IComparer<OneLoginRatingRecord>
        {
            public int Compare(OneLoginRatingRecord r1, OneLoginRatingRecord r2)
            {
                if (r1 == null)
                    return -1;
                if (r2 == null)
                    return 1;

                if (r1.BestGunnerPoints != r2.BestGunnerPoints)
                    return r2.BestGunnerPoints - r1.BestGunnerPoints;

                return r2.loginId - r1.loginId;
            }
        }

        public class LoginWizardComparer : IComparer<OneLoginRatingRecord>
        {
            public int Compare(OneLoginRatingRecord r1, OneLoginRatingRecord r2)
            {
                if (r1 == null)
                    return -1;
                if (r2 == null)
                    return 1;

                if (r1.BestWizardPoints != r2.BestWizardPoints)
                    return r2.BestWizardPoints - r1.BestWizardPoints;

                return r2.loginId - r1.loginId;
            }
        }

        public class LoginShamanComparer : IComparer<OneLoginRatingRecord>
        {
            public int Compare(OneLoginRatingRecord r1, OneLoginRatingRecord r2)
            {
                if (r1 == null)
                    return -1;
                if (r2 == null)
                    return 1;

                if (r1.BestShamanPoints != r2.BestShamanPoints)
                    return r2.BestShamanPoints - r1.BestShamanPoints;

                return r2.loginId - r1.loginId;
            }
        }

        public class GuildWarEfficiencyCountComparer : IComparer<OneGuildRatingRecord>
        {
            public int Compare(OneGuildRatingRecord r1, OneGuildRatingRecord r2)
            {
                if (r1 == null)
                    return -1;
                if (r2 == null)
                    return 1;

                if (r1.guildwar_efficiency != r2.guildwar_efficiency)
                    return r2.guildwar_efficiency - r1.guildwar_efficiency;

                if (r1.wins != r2.wins)
                    return r2.wins - r1.wins;

                if (r1.looses != r2.looses)
                    return r1.looses - r2.looses;

                if (r1.draw != r2.draw)
                    return r2.draw - r1.draw;

                if (r1.efficiency != r2.efficiency)
                    return r2.efficiency - r1.efficiency;

                if (r1.killCounter != r2.killCounter)
                    return r2.killCounter - r1.killCounter;

                if (r1.deathCounter != r2.deathCounter)
                    return r1.deathCounter - r2.deathCounter;

                if (r1.guildRank != r2.guildRank)
                    return r2.guildRank - r1.guildRank;

                return r2.Data.ClanId - r1.Data.ClanId;
            }
        }

        public class GuildEfficiencyCountComparer : IComparer<OneGuildRatingRecord>
        {
            public int Compare(OneGuildRatingRecord r1, OneGuildRatingRecord r2)
            {
                if (r1 == null)
                    return -1;
                if (r2 == null)
                    return 1;

                if (r1.efficiency != r2.efficiency)
                    return r2.efficiency - r1.efficiency;

                if (r1.killCounter != r2.killCounter)
                    return r2.killCounter - r1.killCounter;

                if (r1.deathCounter != r2.deathCounter)
                    return r1.deathCounter - r2.deathCounter;

                if (r1.guildRank != r2.guildRank)
                    return r2.guildRank - r1.guildRank;

                return r2.Data.ClanId - r1.Data.ClanId;
            }
        }

        //public class SeriesCountComparer : IComparer<OneRatingRecord>
        //{
        //    public int Compare(OneRatingRecord r1, OneRatingRecord r2)
        //    {
        //        if (r1 == null)
        //            return -1;
        //        if (r2 == null)
        //            return 1;

        //        if (r1.series != r2.series)
        //            return r2.series - r1.series;

        //        if (r1.loginId != r2.loginId)
        //            return r2.loginId - r1.loginId;

        //        return r2.personType - r1.personType;
        //    }
        //}

        //public class KillCountComparer : IComparer<OneRatingRecord>
        //{
        //    public int Compare(OneRatingRecord r1, OneRatingRecord r2)
        //    {
        //        if (r1 == null)
        //            return -1;
        //        if (r2 == null)
        //            return 1;

        //        if (r1.killCounter != r2.killCounter)
        //            return r2.killCounter - r1.killCounter;

        //        if (r1.loginId != r2.loginId)
        //            return r2.loginId - r1.loginId;

        //        return r2.personType - r1.personType;
        //    }
        //}

        //public class DeathCountComparer : IComparer<OneRatingRecord>
        //{
        //    public int Compare(OneRatingRecord r1, OneRatingRecord r2)
        //    {
        //        if (r1 == null)
        //            return -1;
        //        if (r2 == null)
        //            return 1;

        //        if (r1.deathCounter != r2.deathCounter)
        //            return r2.deathCounter - r1.deathCounter;

        //        if (r1.loginId != r2.loginId)
        //            return r2.loginId - r1.loginId;

        //        return r2.personType - r1.personType;
        //    }
        //}

        //public class EfficiencyCountComparer : IComparer<OneRatingRecord>
        //{
        //    public int Compare(OneRatingRecord r1, OneRatingRecord r2)
        //    {
        //        if (r1 == null)
        //            return -1;
        //        if (r2 == null)
        //            return 1;

        //        //if (r1.efficiency != r2.efficiency)
        //        //    return r2.efficiency - r1.efficiency;

        //        if (r1.killCounter != r2.killCounter)
        //            return r2.killCounter - r1.killCounter;

        //        if (r1.deathCounter != r2.deathCounter)
        //            return r1.deathCounter - r2.deathCounter;

        //        if (r1.loginId != r2.loginId)
        //            return r2.loginId - r1.loginId;

        //        return r2.personType - r1.personType;
        //    }
        //}

        //public class EloCoopComparer : IComparer<OneRatingRecord>
        //{
        //    public int Compare(OneRatingRecord r1, OneRatingRecord r2)
        //    {
        //        if (r1 == null)
        //            return -1;
        //        if (r2 == null)
        //            return 1;

        //        if (r1.rating_elo_coop != r2.rating_elo_coop)
        //            return r2.rating_elo_coop - r1.rating_elo_coop;

        //        if (r1.gearScore != r2.gearScore)
        //            return r2.gearScore - r1.gearScore;

        //        if (r1.loginId != r2.loginId)
        //            return r2.loginId - r1.loginId;

        //        return r2.personType - r1.personType;
        //    }
        //}

        //public class EloSoloComparer : IComparer<OneRatingRecord>
        //{
        //    public int Compare(OneRatingRecord r1, OneRatingRecord r2)
        //    {
        //        if (r1 == null)
        //            return -1;
        //        if (r2 == null)
        //            return 1;

        //        if (r1.rating_elo_solo != r2.rating_elo_solo)
        //            return r2.rating_elo_solo - r1.rating_elo_solo;

        //        if (r1.gearScore != r2.gearScore)
        //            return r2.gearScore - r1.gearScore;

        //        if (r1.loginId != r2.loginId)
        //            return r2.loginId - r1.loginId;

        //        return r2.personType - r1.personType;
        //    }
        //}

        //public class EloTeamComparer : IComparer<OneRatingRecord>
        //{
        //    public int Compare(OneRatingRecord r1, OneRatingRecord r2)
        //    {
        //        if (r1 == null)
        //            return -1;
        //        if (r2 == null)
        //            return 1;

        //        if (r1.rating_elo_team != r2.rating_elo_team)
        //            return r2.rating_elo_team - r1.rating_elo_team;

        //        if (r1.gearScore != r2.gearScore)
        //            return r2.gearScore - r1.gearScore;

        //        if (r1.loginId != r2.loginId)
        //            return r2.loginId - r1.loginId;

        //        return r2.personType - r1.personType;
        //    }
        //}

        //public class PvpPointsComparer : IComparer<OneRatingRecord>
        //{
        //    public int Compare(OneRatingRecord r1, OneRatingRecord r2)
        //    {
        //        if (r1 == null)
        //            return -1;
        //        if (r2 == null)
        //            return 1;

        //        if (r1.ratingPlace != r2.ratingPlace)
        //            return r2.ratingPlace - r1.ratingPlace;

        //        if (r1.pvpPoints != r2.pvpPoints)
        //            return r2.pvpPoints - r1.pvpPoints;

        //        if (r1.loginId != r2.loginId)
        //            return r2.loginId - r1.loginId;

        //        if (r1.gearScore != r2.gearScore)
        //            return r2.gearScore - r1.gearScore;

        //        return r2.personType - r1.personType;
        //    }
        //}

        public class RewardComparer : IComparer<OneRewardRecord>
        {
            public int Compare(OneRewardRecord r1, OneRewardRecord r2)
            {
                if (r1 == null)
                    return -1;
                if (r2 == null)
                    return 1;

                if (r1.loginId != r2.loginId)
                    return r2.loginId - r1.loginId;

                return 0;
            }
        }
    }
}
