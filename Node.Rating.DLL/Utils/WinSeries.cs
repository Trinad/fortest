﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilsLib.Logic.Enums;

namespace Node.Rating.Utils
{
    public class WinSeries
    {
        public ArenaMatchType arenaMatchType;
        public int winSeries;
        public int winSeriesMax;

        public void CalculateWinSeries(bool isWin)
        {
            if (isWin)
                winSeries++;
            else
                winSeries = 0;

            if (winSeries > winSeriesMax)
                winSeriesMax = winSeries;
        }

        public void Reset()
        {
            winSeries = 0;
            winSeriesMax = 0;
        }
    }
}
