﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UtilsLib.Logic.Enums;

namespace Node.Rating.Logic.XmlData
{
    public class RewardXmlData
    {
        [XmlAttribute]
        public int Id = 1;
        [XmlAttribute]
        public Rank PersonRank = Rank.Newbie;
        [XmlAttribute]
        public int PvpMin = 0;
        [XmlAttribute]
        public int PvpMax = int.MaxValue;
        [XmlAttribute]
        public int RatingPlaceFrom = int.MaxValue;
        [XmlAttribute]
        public int RatingPlaceTo = 0;
        [XmlAttribute]
        public int ItemId = -1;
        [XmlAttribute]
        public int ItemCount = 0;
        [XmlAttribute]
        public CurrencyType CurrencyType = 0;
        [XmlAttribute]
        public int CurrencyCount = 0;
        [XmlAttribute]
        public int League = 0;
    }

    //public enum Rank
    //{
    //    None,
    //    Hero,
    //    Guardian,
    //    Conqueror,
    //    Fighter,
    //    Brave,
    //    Solder,
    //    Newbie,
    //}

    public enum CurrencyType
    {
        Gold = 0,
        Emeralds = 1,
    }
}
