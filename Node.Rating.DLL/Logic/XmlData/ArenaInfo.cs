﻿using Node.Rating.Logic.Records;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using UtilsLib.Logic.Enums;

namespace Node.Rating.Logic.XmlData
{

    public class StatisticsXMLdata
    {
        [XmlElement("ArenaInfo", typeof(ArenaInfo))]
        public List<ArenaInfo> arenaInfo;

        //[XmlArrayItem(typeof(RateInfo))]
        //public List<RateInfo> RatePointsLokalization;

    }

    public class ArenaInfo
    {
        [XmlAttribute]
        public ArenaMatchType ArenaType;
        [XmlAttribute]
        public string LoсalizationKey;
        [XmlAttribute]
        public string Color;

        public List<List<BattleStatType>> VisibleData;
        public List<BattleStatType> StoredData;
        
    }
}
