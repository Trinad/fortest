﻿namespace Node.Rating.Logic.Datas
{
    public class GuildWarData
    {
        public int guild1;
        public int guild2;
        public int result;

        public GuildWarData(int guild1, int guild2, int result)
        {
            this.guild1 = guild1;
            this.guild2 = guild2;
            this.result = result;
        }
    }
}
