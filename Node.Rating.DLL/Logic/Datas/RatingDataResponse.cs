﻿using System.Collections.Generic;

namespace Node.Rating.Logic.Datas
{
    public class RatingDataRecord
    {
        public int LoginId;
        public string Name;
        public string Avatar;
        public string AvatarBorder;
        public int RatingPlace;
        public int RatingValue; 
        public int CountryId;
        public int BestGlobalPlace;
        public int BestLocalPlace;
    }

    public class RatingDataResponse
    {
        public List<RatingDataRecord> OtherPlayers = new List<RatingDataRecord>();
        public RatingDataRecord PlayerRating;
    }
}
