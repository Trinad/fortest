﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilsLib.Logic.Enums;

namespace Node.Rating.Logic.Datas
{
    public class ArenaStatisticsDataShort
    {
        public ArenaMatchType arenaType;
        public int totalGamesCount;
        public int winCount;
        public List<int> Places = new List<int>();
    }
}
