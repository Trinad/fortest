﻿using UtilsLib.NetworkingData;

namespace Node.Rating.Logic.Datas
{
    public class UpdateGuildRatingInfo : InfoData
    {
        public GuildData Data { get; set; }
    }
}
