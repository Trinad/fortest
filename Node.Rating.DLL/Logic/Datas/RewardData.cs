﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Node.Rating.Logic.Datas
{
    public class RewardData
    {
        public long Id { get; set; }
        public int LoginId { get; set; }
        public int RewardId { get; set; }
        public DateTime RemoveDateTime { get; set; }
    }
}
