﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Node.Rating.Logic.Datas
{
    class AllPersonsBattleStatisticsData
    {
        public int totalGoldCount = 0;
        public List<PersonBattleStatisticsData> personBattleStatisticsDatas = new List<PersonBattleStatisticsData>();
    }
}
