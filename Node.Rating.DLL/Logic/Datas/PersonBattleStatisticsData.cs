﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Node.Rating.Logic.Datas
{
    public class PersonBattleStatisticsData
    {
        public int PersonId = 0;
        public int GameCount = 0;
        public List<ArenaStatisticsDataShort> arenaData = new List<ArenaStatisticsDataShort>();
    }
}
