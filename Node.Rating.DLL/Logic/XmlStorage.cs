﻿using Node.Rating.Logic.XmlData;
using Node.Rating.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilsLib;
using UtilsLib.Logic;

namespace Node.Rating.Logic
{
    public class XmlStorage
    {
        public static XmlStorage Instance { get; private set; }

        public StatisticsXMLdata statisticsXMLdata;

        string LoginStatisticsXmlAsString = string.Empty;
        string LoginStatisticsXmlAsHash = string.Empty;

        private XmlStorage() { }
        public static void Create()
        {
            if (Instance == null)
                Instance = new XmlStorage();
        }

        public void LoadXML()
        {
            try
            {
                PathBuilder pathBuilder = new PathBuilder();
                var path = pathBuilder.GetXmlPath("LoginStatistics.xml");
                LoginStatisticsXmlAsString = System.IO.File.ReadAllText(path);
                LoginStatisticsXmlAsHash = LoginStatisticsXmlAsString.MD5(System.Text.Encoding.UTF8);

                statisticsXMLdata = path.LoadFromFileXml<StatisticsXMLdata>();
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"Faild to load LoginStatistics.xml and get MD5 error:{ex.Message}", ErrorLevel.error);
            }
        }

        //TODO: дописать работу с xml  по имени
        public bool ValidateXML(string hash, string Name)
        {
            return hash.CompareTo(LoginStatisticsXmlAsHash) == 0;
        }

        //TODO: дописать работу с xml  по имени
        public string GetXMLasString(string Name)
        {
            return LoginStatisticsXmlAsString;
        }
    }
}
