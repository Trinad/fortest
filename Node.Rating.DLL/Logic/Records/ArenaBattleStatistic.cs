﻿using Node.Rating.Logic.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilsLib.DegSerializers;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Node.Rating.Logic.Records
{
    public class ArenaBattleStatistic : ICanCopy
    {
        public ArenaMatchType arenaType;
        public int totalGamesCount;
        public int winCount;
        public List<int> Places = new List<int>();
        public int WinSeries;
        public Dictionary<BattleStatType, float> statisticParams = new Dictionary<BattleStatType, float>();

        public ArenaBattleStatistic()
        {
        }

        public ArenaBattleStatistic(ArenaMatchType arenaMatchType)
        {
            arenaType = arenaMatchType;
            Places.Add(0);
            Places.Add(0);
            Places.Add(0);
            Places.Add(0);
        }
        public void UpdateWith(ArenaBattleStatistic arenaStat, bool getAgregated = false)
        {
            if (!getAgregated && arenaType != arenaStat.arenaType)
                return;
            totalGamesCount += arenaStat.totalGamesCount;
            winCount += arenaStat.winCount;
            Places[0] += arenaStat.Places[0];
            Places[1] += arenaStat.Places[1];
            Places[2] += arenaStat.Places[2];
            Places[3] += arenaStat.Places[3];

            SetMaxParam(BattleStatType.maxInDamag, arenaStat);
            SetMaxParam(BattleStatType.maxOutDamag, arenaStat);
            AddValue(BattleStatType.firstBlood, arenaStat);
            AddValue(BattleStatType.assists, arenaStat);
            AddValue(BattleStatType.totalKills, arenaStat);
            AddValue(BattleStatType.AddedGold, arenaStat);
            AddValue(BattleStatType.MVPwin, arenaStat);
            AddValue(BattleStatType.MVPlose, arenaStat);
            AddValue(BattleStatType.MVPcoop, arenaStat);
            AddValue(BattleStatType.kills2, arenaStat);
            AddValue(BattleStatType.kills3, arenaStat);
            AddValue(BattleStatType.kills4, arenaStat);
            AddValue(BattleStatType.kills5, arenaStat);
            AddValue(BattleStatType.kills6, arenaStat);
            AddValue(BattleStatType.kills7, arenaStat);
            SetMaxParam(BattleStatType.WinMax, arenaStat);
        }

        public float GetParam(BattleStatType type)
        {
            if (statisticParams.TryGetValue(type, out float curVal))
                return curVal;

            return 0f;
        }

        public void AddValue(BattleStatType type, ArenaBattleStatistic arenaStat)
        {
            if (statisticParams.TryGetValue(type, out float curVal))
            {
                if (arenaStat.statisticParams.TryGetValue(type, out float otherVal))
                {
                    statisticParams[type] = curVal + otherVal;
                }
            }
        }

        public void AddValue(BattleStatType type, float value)
        {
            if (statisticParams.TryGetValue(type, out float curVal))
                statisticParams[type] = curVal + value;
        }

        public void SetValue(BattleStatType type, float value)
        {
            if (statisticParams.ContainsKey(type))
                statisticParams[type] = value;
        }

        public void IncrementParam(BattleStatType type)
        {
            if (statisticParams.TryGetValue(type, out float curVal))
                statisticParams[type] = curVal + 1;
        }

        public void SetMaxParam(BattleStatType type, ArenaBattleStatistic arenaStat)
        {
            if (statisticParams.TryGetValue(type, out float curVal))
            {
                if (arenaStat.statisticParams.TryGetValue(type, out float otherVal))
                {
                    statisticParams[type] = (otherVal > curVal) ? otherVal : curVal;
                }
            }
        }

        public void SetMaxParam(BattleStatType type, float value)
        {
            if (statisticParams.TryGetValue(type, out float curVal))
                statisticParams[type] = (value > curVal) ? value : curVal;
        }

        public void Update(BattleStatType type, int value,bool adding)
        {
            if(adding)
                AddValue(type, value);
            else
                SetMaxParam(type, value);
        }

        public void Update(PersonMaxDamage pmd)
        {
            SetMaxParam(BattleStatType.maxInDamag, pmd.maxInDamag);
            SetMaxParam(BattleStatType.maxOutDamag, pmd.maxOutDamag);

            AddValue(BattleStatType.MVPwin, pmd.MVPwin);
            AddValue(BattleStatType.MVPlose, pmd.MVPlose);
            AddValue(BattleStatType.MVPcoop, pmd.MVPcoop);
        }

        public void Update(PersonSeries ps)
        {
            AddValue(BattleStatType.kills2, ps.kills2);
            AddValue(BattleStatType.kills3, ps.kills3);
            AddValue(BattleStatType.kills4, ps.kills4);
            AddValue(BattleStatType.kills5, ps.kills5);
            AddValue(BattleStatType.kills6, ps.kills6);
            AddValue(BattleStatType.kills7, ps.kills7);

            AddValue(BattleStatType.firstBlood, ps.firstBlood);
            AddValue(BattleStatType.assists, ps.assists);
            AddValue(BattleStatType.totalKills, ps.totalKills);
        }

        public ArenaStatisticsDataShort GetDataForRating()
        {
            var ratingData = new ArenaStatisticsDataShort();
            ratingData.arenaType = arenaType;
            ratingData.totalGamesCount = totalGamesCount;
            ratingData.winCount = winCount;
            ratingData.Places = new List<int>(Places);
            return ratingData;
        }
    }


}
