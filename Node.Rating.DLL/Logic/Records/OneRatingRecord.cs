﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Utils.Logger;
using Newtonsoft.Json;
using Node.Rating.Logic.Datas;
using Node.Rating.Logic.XmlData;
using UtilsLib.corelite;
using UtilsLib.DegSerializers;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Node.Rating.Logic.Records
{
    public enum BattleStatType
    {
        maxInDamag,
        maxOutDamag,
        firstBlood,
        assists,
        totalKills,
        AddedGold,
        MVPwin,
        MVPlose,
        kills2,
        kills3,
        kills4,
        kills5,
        kills6,
        kills7,
        WinMax,
        MVPcoop,
    }

    public class BattleStatRecord
    {
        public BattleStatType key;
        public float value;

        public BattleStatRecord(BattleStatType first, float second)
        {
            key = first;
            value = second;
        }
    }

    public class OneRatingRecord//TODO_maximpr устаревшая штука. можно удалять
    {

    }

   
    public class LoginStatisticsData
    {
        public List<ArenaStatisticsData> arenaStatisticsDatas = new List<ArenaStatisticsData>();

        void Add(ArenaStatisticsData statData)
        {
            arenaStatisticsDatas.Add(statData);
        }
    }

    public class ArenaStatisticsData
    {
        public ArenaMatchType arenaType;
        public int totalGamesCount;
        public int winCount;
        public List<int> Places = new List<int>();

        public List<BattleStatRecord> firstColumn = new List<BattleStatRecord>();
        public List<BattleStatRecord> secondColumn = new List<BattleStatRecord>();

        public ArenaStatisticsData(ArenaMatchType ArenaType)
        {
            arenaType = ArenaType;
        }
        public ArenaStatisticsData(ArenaBattleStatistic arenaStat)
        {
            arenaType = arenaStat.arenaType;
            totalGamesCount = arenaStat.totalGamesCount;
            winCount = arenaStat.winCount;
            Places = arenaStat.Places;
        }
    }

    public class BattleStatistics
    {
        public const int maxPlaces = 4;

        public int PersonId = 0;

        public int GameCount = 0;
        public int WinSeries = 0;
        public int WinSeriesMax = 0;

        public List<ArenaBattleStatistic> arenaBattleStatistics = new List<ArenaBattleStatistic>();
        public BattleStatistics() { }


        void SetEmptyArenasBattleStatistics(StatisticsXMLdata statisticsXMLdata)
        {
            foreach (ArenaMatchType arena in statisticsXMLdata.arenaInfo.Select(x => x.ArenaType).ToList())
            {
                // исключаем тип None, так-как общие данные создаются из существующих и их не надо сохранять в БД
                if (arena != ArenaMatchType.None && !arenaBattleStatistics.Exists(x=> x.arenaType == arena))
                    arenaBattleStatistics.Add(new ArenaBattleStatistic(arena));
            }
        }
        
        public void ValidateArenasFields()
        {
            var statisticsXMLdata = XmlStorage.Instance.statisticsXMLdata;
            SetEmptyArenasBattleStatistics(statisticsXMLdata);

            foreach (var arena in arenaBattleStatistics)
            {
                var arenaInfo = statisticsXMLdata.arenaInfo.Find(x => x.ArenaType == arena.arenaType);

                if (arenaInfo == null)
                    continue;

                //добавляем новое поле если его раньше не было 
                foreach (var el in arenaInfo.StoredData)
                {
                    if (!arena.statisticParams.ContainsKey(el))
                        arena.statisticParams.Add(el, 0);
                }

                List<BattleStatType> removeItems = new List<BattleStatType>();
                //ищем поля на удаление из хранимых данных
                foreach (var el in arena.statisticParams)
                {
                    if (!arenaInfo.StoredData.Contains(el.Key))
                        removeItems.Add(el.Key);
                }
                //удаляем уже не нужные поля из данных для хранения
                foreach (var el in removeItems)
                {
                    arena.statisticParams.Remove(el);
                }
            }
        }
        public void InitDictionaryKeys()
        {
            var statisticsXMLdata = XmlStorage.Instance.statisticsXMLdata;
            SetEmptyArenasBattleStatistics(statisticsXMLdata);

            for (int i = 0; i < arenaBattleStatistics.Count; i++)
            {
                var arenaInfo = statisticsXMLdata.arenaInfo.Find(x => x.ArenaType == arenaBattleStatistics[i].arenaType);

                if (arenaInfo == null)
                    continue;

                foreach (var el in arenaInfo.StoredData)
                {
                    arenaBattleStatistics[i].statisticParams.Add(el, 0);
                }
            }
        }

        public BattleStatistics Copy()//TODO_maximpr этот метод надо покрыть тестом, чтобы добавление полей не сломало ничего
        {
            return this.JsonEncode().JsonDecode<BattleStatistics>(); //TODO нужено адекватное расширение для списков для глубокого копирования
        }

        public void Apply(int addGold, ArenaMatchType arenaType)
        {
			var arenaBattleStatistic = arenaBattleStatistics.Find(x => x.arenaType == arenaType);
			arenaBattleStatistic?.Update(BattleStatType.AddedGold, addGold, true); //Добыть золото можно и вне боя, ничего страшного
        }

        public void Apply(PersonMaxDamage pmd)
        {
            arenaBattleStatistics.First(x => x.arenaType == pmd.arenaType).Update(pmd);
        }

        public void Apply(PersonSeries ps)
        {
            arenaBattleStatistics.First(x => x.arenaType == ps.arenaType).Update(ps);
        }

        internal BattleStatistics Apply(RatingData uri)
        {
            var copy = Copy();

			var curentBattleSeries = copy.arenaBattleStatistics.Find(x => x.arenaType == uri.GameType);
            if (curentBattleSeries != null)//TODO: maximpr это плохое условие. скрывает факт плохой команды
            {
                copy.GameCount++;

                if (uri.IsWin)
                {
                    copy.WinSeries++;
                    curentBattleSeries.WinSeries++;
                    curentBattleSeries.winCount++;
                    curentBattleSeries.Places[Math.Min(maxPlaces, uri.Place) - 1]++;
                }
                else
                {
                    copy.WinSeries = 0;
                    curentBattleSeries.WinSeries = 0;
                }

                if (copy.WinSeries > copy.WinSeriesMax)
                    copy.WinSeriesMax = copy.WinSeries;

                curentBattleSeries.SetMaxParam(BattleStatType.WinMax, curentBattleSeries.WinSeries);

                curentBattleSeries.totalGamesCount++;
            }
            else
            {
                //cheat
            }

            return copy;
        }

        public void UpdateWith(BattleStatistics bs)
        {
            WinSeriesMax = Math.Max(WinSeriesMax, bs.WinSeriesMax);
            for(int i=0;i< arenaBattleStatistics.Count;i++)
                arenaBattleStatistics[i].UpdateWith(bs.arenaBattleStatistics[i]);
        }

        public ArenaBattleStatistic GetTotal()
        {
            ArenaBattleStatistic total = new ArenaBattleStatistic(ArenaMatchType.None);

            var arenaInfo = XmlStorage.Instance.statisticsXMLdata.arenaInfo.Find(x => x.ArenaType == ArenaMatchType.None);

            if (arenaInfo == null)
                return null;

            foreach (var el in arenaInfo.StoredData)
               total.statisticParams.Add(el, 0);

            foreach (var arenaStat in arenaBattleStatistics)
            {
                total.UpdateWith(arenaStat,true);
            }
            return total;
        }

        public PersonBattleStatisticsData GetBattleRatingData()
        {
            var battleStatisticsRatingData = new PersonBattleStatisticsData();
            battleStatisticsRatingData.PersonId = PersonId;
            battleStatisticsRatingData.GameCount = GameCount;

            foreach (var arenaStat in arenaBattleStatistics)
            {
                battleStatisticsRatingData.arenaData.Add(arenaStat.GetDataForRating());
            }

            return battleStatisticsRatingData;
        }
    }
}
