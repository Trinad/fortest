﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using Node.Rating.Utils;
using UtilsLib.corelite;
using UtilsLib.Logic;

namespace Node.Rating.Logic.Records
{
    public abstract class OneRecord
    {
        protected const int PersonsCount = 8;
        protected long key;
        public abstract long Key();
        public int loginId { get; set; }

        private bool removeFromList = false;
        public bool RemoveFromList { get { return removeFromList; } }

        private bool removeInDB = false;
        public bool RemoveInDB { get { return removeInDB; } }

        protected bool m_InSaveQueue = false;
        public bool InSaveQueue
        {
            get { return m_InSaveQueue; }
            set
            {
                m_InSaveQueue = value;
                if (m_InSaveQueue)
                    SaveInitDate = TimeProvider.UTCNow;
                else
                    SaveInitDate = DateTime.MinValue;
            }
        }

        protected bool m_bGetfromDB = false;
        public bool bGetFromDB { get { return m_bGetfromDB; } set { m_bGetfromDB = value; } }
        //другие поля...
        protected bool m_bChange = false;
        public bool bChange { get { return m_bChange; } set { m_bChange = value; } }

        protected DateTime SaveInitDate = DateTime.MinValue;
        public virtual void MakeClear()
        {
            removeFromList = true;
        }

        public virtual void RemovedInDb()
        {
            removeInDB = true;
        }
        public bool needSave
        {
            get { return SaveInitDate.AddSeconds(SystemConfig.SavePeriodSec) < TimeProvider.UTCNow; }
        }

        public abstract void UpdatePlaceData(int place, bool isInit = false);
        public abstract void Fill(DBRecordCollection source);
        public abstract void Reset();
    }
}
