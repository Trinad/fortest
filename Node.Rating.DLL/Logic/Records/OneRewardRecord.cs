﻿using Node.Rating.Logic.Datas;
using UtilsLib.corelite;

namespace Node.Rating.Logic.Records
{
    public class OneRewardRecord : OneRecord
    {
        //public override long Key { get { return key; } }

        public override long Key()
        {
            return key;
        }

        public RewardData Reward { get; set; }

        public bool WasSavedToDb { get; set; }
        public OneRewardRecord(bool fromDB)
        {
            m_bGetfromDB = fromDB;
        }

        public void SetKey(long key)
        {
            this.key = key;
        }

        public override void UpdatePlaceData(int place, bool isInit = false)
        {
            // пока ничего не надо
        }

        public override void Fill(DBRecordCollection source)
        {
            var reward = new RewardData();
            reward.Id = key = source.GetInt("Id");
            reward.LoginId = loginId = source.GetInt("LoginId");
            reward.RewardId = source.GetInt("RewardId");
            reward.RemoveDateTime = source.GetDateTime("RemoveDate");
            
            Reward = reward;
        }

        public override void Reset()
        {
            // пока ничего не надо
        }

		public override string ToString()
		{
			return $"LoginId {loginId} RewardId {Reward.RewardId} RemoveDateTime {Reward.RemoveDateTime}";
		}
	}
}
