﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Node.Rating.Logic.Datas;
using Node.Rating.Logic.RatingInstances;
using UtilsLib.corelite;
using UtilsLib.NetworkingData;

namespace Node.Rating.Logic.Records
{
    public class OneLoginRatingRecord : OneRecord
    {
        //public int LoginId;
        public string Name;
        public string Avatar;
        public string AvatarBorder = "AvatarBorder";
        
        public int maxWarriorLevel;
        public int maxWizardLevel;
        public int maxArcherLevel;
        public int maxGunnerLevel;
        public int maxShamanLevel;

        public int BestWarriorPoints;
        public int BestWizardPoints;
        public int BestArcherPoints;
        public int BestGunnerPoints;
        public int BestShamanPoints;
        public int SummBestPoints;
        public int place { get; set; }

        public override long Key()
        {
            return loginId;
        }

        public OneLoginRatingRecord()
        {
        }

        public override void UpdatePlaceData(int place, bool isInit = false)
        {
            this.place = place;
        }

        public override void Fill(DBRecordCollection source)
        {

        }

        public override void Reset()
        {
            // что то будем тут обнулять
            m_bChange = true;
        }

        internal void ClearRating()
        {
            BestWarriorPoints = 0;
            BestWizardPoints = 0;
            BestArcherPoints = 0;
            BestGunnerPoints = 0;
            BestShamanPoints = 0;

            maxWarriorLevel = -1;
            maxWizardLevel = -1;
            maxArcherLevel = -1;
            maxGunnerLevel = -1;
            maxShamanLevel = -1;
            SummBestPoints = 0;
        }
    }
}