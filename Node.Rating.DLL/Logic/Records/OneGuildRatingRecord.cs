﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Node.Rating.Logic.Datas;
using Node.Rating.Logic.RatingInstances;
using UtilsLib.corelite;
using UtilsLib.NetworkingData;

namespace Node.Rating.Logic.Records
{
    public class OneGuildRatingRecord : OneRecord
    {
        private bool reset = false;
        public bool IsResetRecord { get { return reset; } }
        public GuildData Data { get; private set; }
        public int guildRank { get; set; }
        public int killCounter { get; set; }
        public int deathCounter { get; set; }
        public int games { get; set; }
        public int gearScore { get; set; }
        public int pvpPoints { get; set; }
        public int efficiency { get; set; }
        public int wins { get; set; }
        public int looses { get; set; }
        public int draw { get; set; }
        public int guildwar_efficiency { get; set; }
        public int guildPlace { get; set; }

        public void MakeReset()
        {
            reset = true;
        }

        public override long Key()
        {
            return Data.ClanId;
        }
        public bool forDelete = false;
        public static int GetGuildwarEfficiency(int wins, int looses, int draw)
        {
            if (wins == 0 || (wins + looses + draw == 0))
                return 0;
            return 10000 * wins / (wins + looses + draw);
        }

        private static int GetEfficiency(int kill, int death, int games)
        {
            return 10000 * kill / Math.Max(1, (death < 10 ? 10 : death) + kill) * games / (games + 1);
        }

        public OneGuildRatingRecord(bool fromDB)
        {
            m_bGetfromDB = fromDB;
        }

        public OneGuildRatingRecord(UpdateGuildRatingInfo uri)
        {
            Data = uri.Data;
            //pvpPoints = uri.pvpPoints;
            efficiency = GetEfficiency(killCounter, deathCounter, games);
            wins = 0;
            looses = 0;
            draw = 0;
            guildwar_efficiency = 0;

            m_bChange = true;
        }

        public void Update(UpdateGuildRatingInfo uri, bool canAddAchXp)
        {
            if (uri.Data.ClanType != Data.ClanType || 
                uri.Data.ClanName != Data.ClanName ||
                uri.Data.EmblemData.ClanEmblemType != Data.EmblemData.ClanEmblemType ||
                uri.Data.EmblemData.ClanEmblemIconId != Data.EmblemData.ClanEmblemIconId ||
				uri.Data.EmblemData.ClanPatternIconId != Data.EmblemData.ClanPatternIconId ||
				uri.Data.EmblemData.ClanEmblemColorId != Data.EmblemData.ClanEmblemColorId ||
				uri.Data.EmblemData.ClanPatternColorId != Data.EmblemData.ClanPatternColorId ||
				uri.Data.EmblemData.ClanFlagColorId != Data.EmblemData.ClanFlagColorId)
            {
                Data = uri.Data;
                m_bChange = true;
            }

            //if (uri.pvpPoints!= 0 && uri.xp_source != UtilsLib.Logic.Enums.XPSource.Achievement || canAddAchXp)
            //{
            //    pvpPoints = uri.pvpPoints;
            //    m_bChange = true;
            //}
        }

        public override void UpdatePlaceData(int place, bool isInit = false)
        {
            guildPlace = place;
        }

        public override void Fill(DBRecordCollection source)
        {
            var data = new GuildData()
            {
                ClanId = source.GetInt("GuildId"),
                ClanName = source.GetString("GuildName"),
                ClanType = (ClanType)source.GetInt("GuildType"),

                //GuildSymbolId = source.GetInt("GuildSymbolId"), //TODO_Deg Clan
                //GuildSymbolColor = source.GetString("GuildSymbolColor"),
                //GuildBgColor = source.GetString("GuildBgColor"),
            };

            Data = data;

            guildRank = source.GetInt("GuildRank");
            killCounter = source.GetInt("KillCount");
            pvpPoints = source.GetInt("Points");
            deathCounter = source.GetInt("DeathCount");
            games = source.GetInt("GameCount");

            efficiency = GetEfficiency(killCounter, deathCounter, games);

            wins = source.GetInt("Wins");
            looses = source.GetInt("Looses");
            draw = source.GetInt("Draw");

            guildwar_efficiency = GetGuildwarEfficiency(wins, looses, draw);
        }

        public override void Reset()
        {
            // что то будем тут обнулять
            m_bChange = true;
        }

		public override string ToString()
		{
			return $"GuildId {Data.ClanId} GuildName {Data.ClanName}";
		}
	}
}
