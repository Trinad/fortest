﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilsLib.Logic;

namespace Node.Rating.DLL.Logic
{
    public class SmallCache<T>
    {
        private class CacheData<K>
        {
            public K data;
            public long key;
            public long changeTime;
        }

        private int maxSize = 1000;
        private int recommendedSize = 500;
        private Func<long, T> rememberFunction;
        private Action<long, T> forgetFunction;

        public SmallCache( int recommendedSize, int maxSize, Func<long, T> rememberFunction, Action<long, T> forgetFunction)
        {
            this.maxSize = maxSize;
            this.recommendedSize = recommendedSize;
            this.rememberFunction = rememberFunction;
            this.forgetFunction = forgetFunction;
        }

        object cache_lock = new object();

        private Dictionary<long, CacheData<T>> cache = new Dictionary<long, CacheData<T>>();

        public void Forget()
        {
            ILogger.Instance.Send("SmallCache.Forget()");
            List<CacheData<T>> list;
            lock (cache_lock)
            {
                list = cache.Values.ToList();
            }
            list.Sort((x, y) => (int)(y.changeTime - x.changeTime));
            for (int i = recommendedSize; i < list.Count; i++)
            {
                var cacheData = list[i];
                lock (cache_lock)
                {
                    cache.Remove(cacheData.key);
                }
                try
                {
                    forgetFunction(cacheData.key, cacheData.data);
                }
                catch (Exception ex)
                {
                    ILogger.Instance.Send($"SmallCache.Forget() key={cacheData.key} exception: {ex}", ErrorLevel.exception);
                }
            }
        }

        public int Count
        {
            get
            {
                lock (cache_lock)
                {
                    return cache.Count;
                }
            }
        }

        public bool ExistInCache(long key)
        {
            return cache.ContainsKey(key);
        }

        public IEnumerable<T> AllCachedValues()
        {
            lock (cache_lock)
            {
                return cache.Values.Select(x => x.data);
            }
        }

        public T Remember(long key)
        {
            T data = default(T);
            try
            {
                 data = rememberFunction(key);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"SmallCache.Remember() key = {key}: {ex}", ErrorLevel.exception);
            }
            Add(key, data);
            return data;
        }

        public void Add(long key, T data)
        {
            long current = XmlUtils.GetEpochTimeSec();
            var cacheData = new CacheData<T>() { key = key, data = data, changeTime = current };
            lock (cache_lock)
            {
                cache[key] = cacheData;
            }

            if (Count > maxSize)
                Forget();
        }

		public void RemoveFromCache(long key)
		{
			lock (cache_lock)
			{
				cache.Remove(key);
			}
		}

		public T Get(long key)
        {
            T data;
            CacheData<T> cacheData;
            lock (cache_lock)
            {
                cache.TryGetValue(key, out cacheData);
            }
            if (cacheData == null)
                data = Remember(key);
            else
            {
                cacheData.changeTime = XmlUtils.GetEpochTimeSec();
                data = cacheData.data;
            }

            return data;
        }
    }
}
