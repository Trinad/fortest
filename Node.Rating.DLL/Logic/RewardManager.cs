﻿using System;
using System.Collections.Generic;
using System.Threading;
using Fragoria.Common.Utils.Threads;
using Node.Rating.Logic.RatingInstances;
using System.Collections.Concurrent;
using Assets.Scripts.Utils.Logger;

namespace Node.Rating.Logic
{
    public class RewardManager : OneThreadTimer
    {
        readonly RewardInstance weekReward;
        private readonly RatingServer ratingServer;

        public RewardInstance WeekReward { get { return weekReward; } }

        public int MaxRatingCount = 50000;
        public int MaxGuldRatingCount = 5000;
        public RewardManager(RatingServer ratingServer, int period) : base("RewardManager", period)
        {
            weekReward = new RewardInstance(ratingServer,MaxRatingCount, DateTime.MaxValue);
            this.ratingServer = ratingServer;
        }

        public override void Start(ThreadPriority priority)
        {
            base.Start(priority);
            Sos.Debug("RewardManager: Started, PvpReward.Count" + weekReward.PvpReward.Count);
        }

        public override void Stop()
        {
            weekReward.Stop();
            base.Stop();
            Sos.Debug("RewardManager: Stoped success");
        }
        private ConcurrentQueue<RewardManagerRequest> requestsQueue = new ConcurrentQueue<RewardManagerRequest>();
        public void AddRequest(RewardManagerRequest req)
        {
            requestsQueue.Enqueue(req);
        }

        private Queue<RewardManagerRequest> internalQueue = new Queue<RewardManagerRequest>();

        public override void OnTimer()
        {
            RewardManagerRequest rmr = null;
            internalQueue.Clear();
            while (requestsQueue.TryDequeue(out rmr))
            {
                internalQueue.Enqueue(rmr);
                ApplyRequest(rmr);
            }
            if (internalQueue.Count > 0)
            {
                while (internalQueue.Count > 0)
                {
                    RewardManagerRequest req = internalQueue.Dequeue();
                    SendRequestData(req); // ответы
                }
            }


            base.OnTimer();
//            weekReward.Update(null);
        }

        public RewardInstance GetRewardInstance(RewardInstanceType instanceType)
        {
            switch (instanceType)
            {
                case RewardInstanceType.Week:
                    return weekReward;
            }
            return null;
        }

        private void ApplyRequest(RewardManagerRequest ratingRequest)
        {
            if (ratingRequest == null)
                return;

            switch (ratingRequest.rewardManagerRequestType)
            {
                case RewardManagerRequestType.UpgradeReward:
                    break;
                case RewardManagerRequestType.TournamentState:
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.RequestTournamentResult:
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.DispatcherRewardListRequest:
                    Sos.Debug("Reward ApplyRequest RatingManagerRequestType.DispatcherRatingListRequest");
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.DispatcherRewardRequest:
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.WebRewardRequest:
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.WebGuildRewardRequest:
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.GetLoginRating:
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.DisbandGuild:
                    break;
                case RewardManagerRequestType.GuildWarResult:
                    break;
                case RewardManagerRequestType.LoginBlock:
                    break;
                case RewardManagerRequestType.GuildRewardRequest:
                    //нечего тут обрабатывать.
                    break;
            }
        }

        private void SendRequestData(RewardManagerRequest ratingRequest)
        {
            if (ratingRequest == null)
                return;
            Sos.Debug("RewardManager: SendRequestData: ratingManagerRequestType: " +
                      ratingRequest.rewardManagerRequestType);
            switch (ratingRequest.rewardManagerRequestType)
            {
                case RewardManagerRequestType.UpgradeReward:
                    break;
                case RewardManagerRequestType.TournamentState:
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.RequestTournamentResult:
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.DispatcherRewardListRequest:
                    Sos.Debug("Rating ApplyRequest RatingManagerRequestType.DispatcherRatingListRequest");
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.DispatcherRewardRequest:
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.WebRewardRequest:
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.WebGuildRewardRequest:
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.GetLoginRating:
                    //нечего тут обрабатывать.
                    break;
                case RewardManagerRequestType.DisbandGuild:
                    break;
                case RewardManagerRequestType.GuildWarResult:
                    break;
                case RewardManagerRequestType.LoginBlock:
                    break;
                case RewardManagerRequestType.GuildRewardRequest:
                    //нечего тут обрабатывать.
                    break;
            }
        }
    }

    public enum RewardInstanceType
    {
        Day,
        Week,
        GuildDay,
        GuilWeek
    }
}
