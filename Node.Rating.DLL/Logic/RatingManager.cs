﻿using System;
using System.Collections;
using System.Collections.Generic;
using Fragoria.Common.Utils.Threads;
using Node.Rating.Logic.Datas;
using Node.Rating.Logic.RatingInstances;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using System.Collections.Concurrent;
using UtilsLib.DegSerializers;

namespace Node.Rating.Logic
{
    public enum RatingPeriodType
    {
        Week = 0,
        Today = 1,
        Tournament = 2,
    }
    public class RatingManager : OneThreadTimer
    {
        //тут может быть несколько рейтингов текущих... глобальный. суточный и турнирный... (тоже что суточный. только даты и время другое)
        readonly LoginStorage loginStarage;

        readonly RatingLoginInstance loginRating;

        readonly RatingGuildInstance weekGuildRating;
        private readonly RatingServer ratingServer;

        public LoginStorage LoginStarage { get { return loginStarage; } }
        public RatingGuildInstance WeekGuildRating { get { return weekGuildRating; } }

        public RatingLoginInstance LoginRating { get { return loginRating; } }

        public int MaxRatingCount = 50000;
        public int MaxGuldRatingCount = 5000;

        public RatingManager(RatingServer ratingServer, int timeOut)
            : base("RatingManager", timeOut)
        {
            XmlStorage.Create();
            XmlStorage.Instance.LoadXML();
            //неделя начинается в воскресение см enum DayOfWeek
            int enDay = (int)DateTime.UtcNow.DayOfWeek;
            //переведем английские номера недели в русские
            int rusDay = (enDay + 6) % 7;
            int days = 7 - rusDay;
            loginStarage = new LoginStorage(ratingServer);

            weekGuildRating = new RatingGuildInstance(ratingServer, MaxGuldRatingCount, DateTime.UtcNow.Date.AddHours(24 * days));
            weekGuildRating.OnResetRating += GlobalGuldRating_OnResetRating;

            loginRating = new RatingLoginInstance(MaxRatingCount);
            this.ratingServer = ratingServer;
        }

        public void SendXML(string hash, int connectId)
        {
            if (XmlStorage.Instance.ValidateXML(hash,null))
            {
                ratingServer.SysFlashPolicyFile.SendText(string.Empty, connectId);
            }
            else
            {
                ratingServer.SysFlashPolicyFile.SendText(XmlStorage.Instance.GetXMLasString(null), connectId);
            }
        }

        private ConcurrentQueue<RatingManagerRequest> requestsQueue = new ConcurrentQueue<RatingManagerRequest>();
        public void AddRequest(RatingManagerRequest req)
        {
            requestsQueue.Enqueue(req);
        }

        private Queue<RatingManagerRequest> internalQueue = new Queue<RatingManagerRequest>();

        public override void OnTimer()
        {
            internalQueue.Clear();
            while (requestsQueue.TryDequeue(out RatingManagerRequest rmr))
            {
                internalQueue.Enqueue(rmr);
            }

            while (internalQueue.Count > 0)
            {
                RatingManagerRequest req = internalQueue.Dequeue();
                ApplyRequest(req);
            }

            base.OnTimer();
        }

        private void ApplyRequest(RatingManagerRequest ratingRequest)
        {
            try
            {
                ratingRequest.actionSetData?.Invoke(loginStarage);
            }
            catch (Exception ex)
            {
                ratingServer.SysFlashPolicyFile.SendError(ratingRequest.ConnectId);
                ILogger.Instance.Send(ex.ToString(), ErrorLevel.exception);
            }
        }

        private void GlobalGuldRating_OnResetRating(Instance rating)
        {
            rating.Reset();

            //неделя начинается в воскресение см enum DayOfWeek
            int enDay = (int)DateTime.UtcNow.DayOfWeek;
            //переведем английские номера недели в русские
            int rusDay = (enDay + 6) % 7;
            int days = 7 - rusDay;
            rating.ResetRatingTimer(DateTime.UtcNow.Date.AddHours(24 * days));
        }

        private Hashtable WebGuildRatingRequest(int guildId)
        {
            RatingGuildInstance inst = weekGuildRating;

            return inst.WebRequest(guildId, 0, 0, null);
        }

        public void WebLoginRatingRequest(int loginId, RatingType ratingId, PersonType classId, RatingMode ratingMode, int connectId)
        {
            RatingLoginInstance inst = loginRating;
            //if (classId == -1)
            //    inst = LoginRating;
            //    inst = dayRating;
            //else if (ratingId == (int)RatingPeriodType.Tournament)
            //    inst = tournamentRating;

            var ratingDataResponse = inst.WebRequest(loginId, ratingId, classId, ratingMode);
            var JSONstr = JSON.JsonEncode(ratingDataResponse.JsonEncode());

            ratingServer.SysFlashPolicyFile.SendText(JSONstr, connectId);
        }

        public override void Stop()
        {
            loginStarage.Stop();
            weekGuildRating.Stop();

            base.Stop();
        }
    }
}
