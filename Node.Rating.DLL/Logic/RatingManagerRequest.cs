﻿using Node.Rating.Logic.RatingInstances;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UtilsLib.low_engine_sharp.transport;

namespace Node.Rating.Logic
{
    public class RatingManagerRequest
    {
        public RatingManagerRequest( Action<LoginStorage> action, int connectId)
        {
            actionSetData = action;

            ConnectId = connectId;
        }

        public Action<LoginStorage> actionSetData;
        public int ConnectId;
    }
}
