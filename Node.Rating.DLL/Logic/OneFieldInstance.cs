﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Node.Rating.Logic.Records;

namespace Node.Rating.Logic.RatingInstances
{
    public abstract class OneFieldInstance<T> where T : Instance
    {
        public bool bChange { get; set; }
        protected int RecordCount { get; set; }
        protected T _instance { get; set; }
        protected Dictionary<long, int> PositionByLoginId { get; set; }
        public abstract void Add(long key, OneRecord oneRecord);
        public abstract void RemoveRecords(long key);
        public abstract void Sort();
        protected abstract void CheckSize();
        public abstract void ResetData();
        public abstract void SaveData();
        public virtual void Clear()
        {
            RecordCount = 0;
            PositionByLoginId.Clear();
        }       
    }
}
