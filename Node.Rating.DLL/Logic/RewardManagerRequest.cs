﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Node.Rating.Logic
{
    public enum RewardManagerRequestType
    {
        UpgradeReward = 1,
        DispatcherRewardRequest = 2,
        TournamentState = 3,
        RequestTournamentResult = 4,
        WebRewardRequest = 5,
        WebGuildRewardRequest = 6,
        DisbandGuild = 7,
        GetLoginRating = 8,
        GuildWarResult = 9,
        LoginBlock = 10,
        GuildRewardRequest = 11,
        DispatcherRewardListRequest = 12
    }
    public class RewardManagerRequest
    {
        public RewardManagerRequest(RewardManagerRequestType requestType, object data, int connectId)
        {
            rewardManagerRequestType = requestType;
            requestData = data;
            ConnectId = connectId;
        }
        public RewardManagerRequestType rewardManagerRequestType;
        public object requestData;
        public int ConnectId;
    }
}
