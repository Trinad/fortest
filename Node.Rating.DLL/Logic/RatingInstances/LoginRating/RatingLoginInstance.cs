﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Assets.Scripts.Utils.Logger;
using Node.Rating.Logic.Datas;
using Node.Rating.Logic.RatingInstances.GameStatistic;
using Node.Rating.Logic.Records;
using Node.Rating.Utils;
using UtilsLib.Logic.Enums;
using UtilsLib.low_engine_sharp.transport;

namespace Node.Rating.Logic.RatingInstances
{
    public class RatingLoginInstance
    {
        Dictionary<int, OneLoginRatingRecord> logins;

        OneFieldLoginRatingInstance loginGlobalRating;
        OneFieldLoginRatingInstance loginWarriorRating;
        OneFieldLoginRatingInstance loginArcherRating;
        OneFieldLoginRatingInstance loginGunnerRating;
        OneFieldLoginRatingInstance loginWizardRating;
        OneFieldLoginRatingInstance loginShamanRating;

        public RatingLoginInstance(int count)
        {
            logins = new Dictionary<int, OneLoginRatingRecord>(count);
            loginGlobalRating = new OneFieldLoginRatingInstance(count, new Comparers.LoginGlobalComparer(), x => x.SummBestPoints, x => x.maxWarriorLevel != -1 ||
                                                                                                                                        x.maxArcherLevel != -1 || 
                                                                                                                                        x.maxGunnerLevel != -1 || 
                                                                                                                                        x.maxWizardLevel != -1 || 
                                                                                                                                        x.maxShamanLevel != -1);
            loginWarriorRating = new OneFieldLoginRatingInstance(count, new Comparers.LoginWarriorComparer(), x => x.BestWarriorPoints, x => x.maxWarriorLevel != -1);
            loginArcherRating = new OneFieldLoginRatingInstance(count, new Comparers.LoginArcherComparer(), x => x.BestArcherPoints, x => x.maxArcherLevel != -1);
            loginGunnerRating = new OneFieldLoginRatingInstance(count, new Comparers.LoginGunnerComparer(), x => x.BestGunnerPoints, x => x.maxGunnerLevel != -1);
            loginWizardRating = new OneFieldLoginRatingInstance(count, new Comparers.LoginWizardComparer(), x => x.BestWizardPoints, x => x.maxWizardLevel != -1);
            loginShamanRating = new OneFieldLoginRatingInstance(count, new Comparers.LoginShamanComparer(), x => x.BestShamanPoints, x => x.maxShamanLevel != -1);
        }

        private OneLoginRatingRecord GetGlobalLoginRatingRecord(int key)
        {
            if (!logins.ContainsKey(key))
            {
                OneLoginRatingRecord record = new OneLoginRatingRecord();// uri);
                record.loginId = key;

                logins[key] = record;
                loginGlobalRating.Add(key, record);
                loginWarriorRating.Add(key, record);
                loginArcherRating.Add(key, record);
                loginGunnerRating.Add(key, record);
                loginWizardRating.Add(key, record);
                loginShamanRating.Add(key, record);
            }
            return logins[key];
        }

        public void Sort()
        {
            loginGlobalRating.Sort();
            loginWarriorRating.Sort();
            loginArcherRating.Sort();
            loginGunnerRating.Sort();
            loginWizardRating.Sort();
            loginShamanRating.Sort();
        }

		public void RemoveRating(int loginId)
		{
			loginGlobalRating.RemoveRecords(loginId);
			loginWarriorRating.RemoveRecords(loginId);
			loginArcherRating.RemoveRecords(loginId);
			loginGunnerRating.RemoveRecords(loginId);
			loginWizardRating.RemoveRecords(loginId);
			loginShamanRating.RemoveRecords(loginId);
		}

        public void RecalcGameStatistic(LoginGameStatistic loginGameStatatistic)
        {
            OneLoginRatingRecord record = GetGlobalLoginRatingRecord(loginGameStatatistic.LoginId);
            record.Name = loginGameStatatistic.Name;
            record.Avatar = loginGameStatatistic.Avatar;

            record.ClearRating();
            foreach (PersonGameStatistic personGameStatistic in loginGameStatatistic.Persons)
            {
                if (personGameStatistic.Level < 5)
                    continue;

                UpdateGameStatistic(record, personGameStatistic);
            }
        }

        public void UpdateGameStatistic(OneLoginRatingRecord record, PersonGameStatistic gameStatatistic)
        {

            if (gameStatatistic.PersonType == PersonType.Archer)
            {
                if (gameStatatistic.Level > record.maxArcherLevel ||
                    (gameStatatistic.Level == record.maxArcherLevel && gameStatatistic.rating_elo_team > record.BestArcherPoints))
                {
                    record.BestArcherPoints = gameStatatistic.rating_elo_team;
                    record.maxArcherLevel = gameStatatistic.Level;
                }
            }

            if (gameStatatistic.PersonType == PersonType.Warrior)
            {
                if (gameStatatistic.Level > record.maxWarriorLevel ||
                    (gameStatatistic.Level == record.maxWarriorLevel && gameStatatistic.rating_elo_team > record.BestWarriorPoints))
                {
                    record.BestWarriorPoints = gameStatatistic.rating_elo_team;
                    record.maxWarriorLevel = gameStatatistic.Level;
                }
            }

            if (gameStatatistic.PersonType == PersonType.Wizard)
            {
                if (gameStatatistic.Level > record.maxWizardLevel ||
                    (gameStatatistic.Level == record.maxWizardLevel && gameStatatistic.rating_elo_team > record.BestWizardPoints))
                {
                    record.BestWizardPoints = gameStatatistic.rating_elo_team;
                    record.maxWizardLevel = gameStatatistic.Level;
                }
            }

            if (gameStatatistic.PersonType == PersonType.Gunner)
            {
                if (gameStatatistic.Level > record.maxGunnerLevel ||
                    (gameStatatistic.Level == record.maxGunnerLevel && gameStatatistic.rating_elo_team > record.BestGunnerPoints))
                {
                    record.BestGunnerPoints = gameStatatistic.rating_elo_team;
                    record.maxGunnerLevel = gameStatatistic.Level;
                }
            }

            if ((PersonType)gameStatatistic.PersonType == PersonType.Shaman)
            {
                if (gameStatatistic.Level > record.maxShamanLevel ||
                    (gameStatatistic.Level == record.maxShamanLevel && gameStatatistic.rating_elo_team > record.BestShamanPoints))
                {
                    record.BestShamanPoints = gameStatatistic.rating_elo_team;
                    record.maxShamanLevel = gameStatatistic.Level;
                }
            }

            record.SummBestPoints = record.BestArcherPoints + record.BestWarriorPoints + record.BestWizardPoints + record.BestGunnerPoints + record.BestShamanPoints;

            //Sos.Debug($"record SummBestPoints {record.SummBestPoints} personType {gameStatatistic.PersonType} rating_elo_team " + gameStatatistic.rating_elo_team);
        }

        public RatingDataResponse WebRequest(int loginId, RatingType ratingId, PersonType classId, RatingMode ratingMode)
        {
            OneFieldLoginRatingInstance inst = loginGlobalRating;

            if (ratingMode == RatingMode.GlobalRating)
            {
                switch(ratingId)
                {
                    case RatingType.ClassRating:
                        if (classId == PersonType.Warrior)
                            inst = loginWarriorRating;
                        else if (classId == PersonType.Archer)
                            inst = loginArcherRating;
                        else if (classId == PersonType.Gunner)
                            inst = loginGunnerRating;
                        else if (classId == PersonType.Wizard)
                            inst = loginWizardRating;
                        else if (classId == PersonType.Shaman)
                            inst = loginShamanRating;
                        break;
                    case RatingType.CommonRating:
                        inst = loginGlobalRating;
                        break;
                    default:
                        ILogger.Instance.Send($"RatingType.{ratingId} not implemented but requested by client", ErrorLevel.error);
                        break;
                }
            }
            else
            {
                ILogger.Instance.Send("ratingMode.LocalRating not implemented but requested by client", ErrorLevel.error);
            }

            return inst.WebRequestRating(loginId);
        }
    }
}
