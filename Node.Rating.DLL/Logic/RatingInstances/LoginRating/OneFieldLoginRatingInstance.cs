﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Utils.Logger;
using Node.Rating.Logic.Datas;
using Node.Rating.Logic.Records;
using UtilsLib;

namespace Node.Rating.Logic.RatingInstances
{
    public class OneFieldLoginRatingInstance
    {
        protected int RecordCount { get; set; }
        protected Dictionary<long, int> PositionByLoginId { get; set; }

        public Func<OneLoginRatingRecord, int> getPoints;

        private OneLoginRatingRecord[] records;
        IComparer<OneLoginRatingRecord> RatingComparer;
        Func<OneLoginRatingRecord, bool> ExistRecordChecker;

        public OneFieldLoginRatingInstance(int count, IComparer<OneLoginRatingRecord> comparer, Func<OneLoginRatingRecord,int> getPoints, Func<OneLoginRatingRecord, bool> existRecordChecker)
        {
            records = new OneLoginRatingRecord[count];
            PositionByLoginId = new Dictionary<long, int>(count);
            RatingComparer = comparer;
            ExistRecordChecker = existRecordChecker;
            this.getPoints = getPoints;
        }

        public void Sort()
        {
            //TODO_maximpr можно вставить сюда проверку по времени
            Array.Sort<OneLoginRatingRecord>(records, 0, RecordCount, RatingComparer);
            int place = 0;
            for (int i = 0; i < RecordCount; i++)
            {
                var record = records[i];
                if (record == null)
                    continue;

                PositionByLoginId[record.loginId] = place;
                record.UpdatePlaceData(place++);
            }
        }

        protected void TryResize()
        {
            if (RecordCount >= records.Length - 1)
            {
                OneLoginRatingRecord[] new_records = new OneLoginRatingRecord[records.Length * 2];
                for (int i = 0; i < records.Length; i++)
                {
                    new_records[i] = records[i];
                    records[i] = null;
                }
                records = new_records;
            }
        }

        public void Add(long key, OneLoginRatingRecord record)
        {
            if (record == null)
            {
                ILogger.Instance.Send("OneFieldLoginRatingInstance Add: record == null", ErrorLevel.error);
                return;
            }
            TryResize();
            records[RecordCount] = record;

            PositionByLoginId[key] = RecordCount;
            RecordCount++;
        }

        public void Clear()
        {
            RecordCount = 0;
            PositionByLoginId.Clear();
        }

        internal int getPosition(long key, bool needSort = true)
        {
            if (needSort)
                Sort();
            if (PositionByLoginId.ContainsKey(key))
                return PositionByLoginId[key];
            return RecordCount;
        }

        internal RatingDataResponse WebRequestRating(int loginId)
        {
            Sort();

            var rating = new RatingDataResponse();
            int pos = Math.Min(200, RecordCount);

            ArrayList allRatings = new ArrayList();
            for (int i = 0; i < pos; i++)
            {
                var record = records[i];
                int ratingValue = getPoints(record);
                if (ratingValue <= 0)
                    break;

                var el = new RatingDataRecord();
                el.LoginId = record.loginId;
                el.Name = record.Name;
                el.Avatar = record.Avatar;
                el.AvatarBorder = record.AvatarBorder;
                el.RatingPlace = record.place + 1;
                el.RatingValue = ratingValue;//.SummBestPoints;
                el.CountryId = 2;//record.
                el.BestGlobalPlace = -1;// record.
                el.BestLocalPlace = -1;// record.
                rating.OtherPlayers.Add(el);
            }


            if (loginId > 0)
            {
                int position = getPosition(loginId, false);

                OneLoginRatingRecord record = null;
                if (position < records.Length)
                    record = records[position];

                var selfRating = new RatingDataRecord();


                if (record != null)
                {
                    selfRating.LoginId = record.loginId;
                    selfRating.Name = record.Name;
                    selfRating.Avatar = record.Avatar;
                    selfRating.AvatarBorder = record.AvatarBorder;
                    selfRating.RatingPlace = ExistRecordChecker(record) ? record.place + 1 : -1;
                    selfRating.RatingValue = getPoints(record);
                }
                else
                {
                    selfRating.LoginId = loginId;
                    selfRating.Name = "unknown";
                    selfRating.Avatar = "avatar";
                    selfRating.AvatarBorder = "AvatarBorder";
                    selfRating.RatingPlace = -1;
                    selfRating.RatingValue = 0;
                }

                selfRating.CountryId = 2;
                selfRating.BestGlobalPlace = -1;
                selfRating.BestLocalPlace = -1;


                rating.PlayerRating = selfRating;
            }
            return rating;

        }

        public void RemoveRecords(long loginId)
        {
            bool bFound = false;
            for (int i = 0; i < RecordCount && !bFound; i++)
                if (records[i].loginId == loginId)
                {
                    bFound = true;
                    if (i + 1 != RecordCount)
                        records[i] = records[RecordCount - 1];
                    records[RecordCount] = null;
                    RecordCount--;
                }
            PositionByLoginId.Remove(loginId);
        }
    }
}
