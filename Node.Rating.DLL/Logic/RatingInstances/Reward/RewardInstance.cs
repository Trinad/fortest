﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Utils.Logger;
using Node.Rating.Logic.Datas;
using Node.Rating.Logic.Records;
using Node.Rating.Logic.XmlData;
using Node.Rating.Utils;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Node.Rating.Logic.RatingInstances
{
    public class RewardInstance : Instance
    {
        const int RemoveDays = 15;
        private readonly RatingServer ratingServer;

        public delegate int GetPoints(OneRewardRecord record);
        public OneFieldRewardInstance week_Rewards;
        public List<RewardXmlData> PvpReward { get; private set; }
        Dictionary<long, OneRewardRecord> personRewards;

        private RewardXmlData conqueror;
        PathBuilder pathBuilder = new PathBuilder();
        public RewardInstance(RatingServer ratingServer, int count, DateTime dtRatingResetDate) : base(dtRatingResetDate)
        {
            var path = pathBuilder.RewardXml("RatingReward.xml");
            PvpReward = path.LoadFromFileXml<List<RewardXmlData>>();

            personRewards = new Dictionary<long, OneRewardRecord>();
            week_Rewards = new OneFieldRewardInstance(this, count, new Comparers.RewardComparer(), x => x.loginId);
            conqueror = PvpReward.FirstOrDefault(r => r.PersonRank == Rank.Conqueror);
            this.ratingServer = ratingServer;
        }
        public override void AddRecord(OneRecord oneRecord)
        {
            var record = oneRecord as OneRewardRecord;
            if (record == null)
            {
                Sos.Error("RewardInstance: AddRecord: oneRatingRecord == null");
                return;
            }
            long key = record.Key();
            personRewards[key] = record;
            week_Rewards.Add(key, record);
            record.WasSavedToDb = true;
        }

        //public RewardRpcData GetPersonRewardRPC(OneRecord oneRecord)
        //{
        //    var oneRatingRecord = oneRecord as OneRatingRecord;
            
        //    if (oneRatingRecord == null)
        //    {
        //        Sos.Error("RewardInstance: GetPersonRewardRPC oneRatingRecord == null");
        //        return null;
        //    }

        //    RewardRpcData result = null;
        //    GetRewardList(oneRatingRecord, out result);
            
        //    return result;
        //}

        //private void GetRewardList(OneRatingRecord oneRatingRecord, out RewardRpcData result)
        //{
        //    var rewards = new List<Reward>();
        //    RemoveRecordsByDate(oneRatingRecord);

        //    var allRewards = week_Rewards.GetRewardDatas(oneRatingRecord.Key());
        //    foreach (var data in allRewards)
        //    {
        //        var rewardXml = PvpReward.Find(r => r.Id == data.RewardId);
        //        if (rewardXml != null)
        //        {
        //            var reward = new Reward();
        //            reward.ItemId = rewardXml.ItemId.ToString();
        //            reward.ItemCount = rewardXml.ItemCount;
        //            reward.CurrencyType = (int)rewardXml.CurrencyType;
        //            reward.CurrencyCount = rewardXml.CurrencyCount;

        //            rewards.Add(reward);
        //        }
        //    }
        //    result = new RewardRpcData();
        //    result.RewardDatas = rewards;
        //    result.LoginId = oneRatingRecord.loginId;
        //    result.PersonType = oneRatingRecord.personType;
        //    result.League = oneRatingRecord.league;

        //    if (oneRatingRecord.payFineCount > 0)
        //    {
        //        Sos.Warning("GetRewardList: oneRatingRecord.payFineCount > 0");
        //        //result.PayFineCount = oneRatingRecord.payFineCount;
        //        //result.CurrentPvpPoints = oneRatingRecord.pvpPoints;

        //        oneRatingRecord.payFineCount = 0;
        //    }
        //}

        public override void SaveRecordToDB(OneRecord oneRecord)
        {
            var record = oneRecord as OneRewardRecord;
            if (record == null)
            {
                Sos.Error("RatingInstance Save record == null");
                return;
            }

            if ((record.bChange && !record.InSaveQueue && !record.WasSavedToDb))
            {
                record.InSaveQueue = true;
                record.WasSavedToDb = true;
                ratingServer.DBStoreManager.AddWeekRewardForSave(record);

                //Sos.Warning("RewardInstance: SaveToDB", string.Format("record.Key: {0} record.loginId: {1} record.Reward.Id: {2} record.Reward.LoginId: {3} record.Reward.RewardId: {4}",
                //record.Key, record.loginId, record.Reward.Id, record.Reward.LoginId, record.Reward.RewardId));
            }
        }

        public bool CheckToReward(OneRecord oneRecord)
        {
            return week_Rewards.CheckRewardDatas(oneRecord.Key());
        }

        public ArrayList GetAllRewards()
        {
            ArrayList allRewards = new ArrayList();
            var rewardsList = PvpReward;
            foreach (var data in rewardsList)
            {
                Hashtable innerHt = new Hashtable();
                innerHt["rank"] = (int)data.PersonRank;
                innerHt["pvpPoints"] = data.PvpMin;
                innerHt["itemId"] = (int)data.ItemId;
                innerHt["itemCount"] = data.ItemCount;
                innerHt["currencyType"] = (int)data.CurrencyType;
                innerHt["currencyCount"] = data.CurrencyCount;
                innerHt["league"] = data.League;
                if (data.PersonRank <= Rank.Conqueror)
                {
                    innerHt["placeFrom"] = data.RatingPlaceFrom;
                    innerHt["placeTo"] = data.RatingPlaceTo + 1;
                }
                else
                {
                    innerHt["placeFrom"] = -1;
                    innerHt["placeTo"] = -1;
                }
                

                allRewards.Add(innerHt);
            }
            return allRewards;
        }

        //public RewardXmlData GetRewardForRating(OneRatingRecord oneRecord)
        //{
        //    RewardXmlData reward = GetReward(oneRecord);
        //    if (reward != null)
        //    {
        //        return reward;
        //    }
        //    else
        //    {
        //        Sos.Error(string.Format("OneRatingRecord reward == null, loginId:{0}, pvpPoints:{1}, ratingPlace: {2}", oneRecord.loginId, oneRecord.pvpPoints, oneRecord.ratingPlace));

        //        return new RewardXmlData();
        //    }
        //}

        //private RewardXmlData GetReward(OneRatingRecord oneRecord)
        //{
        //    RewardXmlData reward = null;
            
        //    if (oneRecord.ratingPlace <= conqueror.RatingPlaceFrom && oneRecord.pvpPoints >= conqueror.PvpMin)
        //    {
        //        var allAwalableRewards = PvpReward.FindAll(r => (oneRecord.pvpPoints >= r.PvpMin && oneRecord.pvpPoints < r.PvpMax));
        //        reward = allAwalableRewards.Last();

        //        if (!(oneRecord.ratingPlace <= reward.RatingPlaceFrom && oneRecord.ratingPlace > reward.RatingPlaceTo))
        //        {
        //            var heroPvp = PvpReward.Last().PvpMin;
        //            if (oneRecord.pvpPoints >= heroPvp)
        //                reward = allAwalableRewards.Find(r => oneRecord.ratingPlace <= r.RatingPlaceFrom && oneRecord.ratingPlace > r.RatingPlaceTo);
        //            else
        //                for (int i = allAwalableRewards.Count - 1; i >= 0; i--)
        //                {
        //                    var avalableReward = allAwalableRewards[i];
        //                    if (oneRecord.ratingPlace <= avalableReward.RatingPlaceFrom &&
        //                        oneRecord.ratingPlace > avalableReward.RatingPlaceTo)
        //                    {
        //                        reward = avalableReward;
        //                        break;
        //                    }
        //                }

        //        }
        //    }
        //    else
        //    {
        //        reward = PvpReward.Find(r => oneRecord.pvpPoints >= r.PvpMin && oneRecord.pvpPoints < r.PvpMax);
        //    }
        //    return reward;
        //}

        //public void AddRewardRecord(OneRatingRecord oneRatingRecord)
        //{
        //    var rewardXml = GetReward(oneRatingRecord);
        //    if (rewardXml == null)
        //    {
        //        Sos.Error("RewardInstance AddRatingRecord rewardXml == null");
        //        return;
        //    }

        //    var rewardRecord = new OneRewardRecord(false);
        //    rewardRecord.loginId = oneRatingRecord.loginId;
        //    rewardRecord.Reward = GetRewardData(rewardXml, oneRatingRecord);

        //    week_Rewards.Add(oneRatingRecord.Key(), rewardRecord);
        //    rewardRecord.bChange = true;

        //    SaveRecordToDB(rewardRecord);
        //}

        //private RewardData GetRewardData(RewardXmlData reward, OneRatingRecord oneRatingRecord)
        //{
        //    var result = new RewardData();
        //    result.Id = oneRatingRecord.Key();
        //    result.LoginId = oneRatingRecord.loginId;
        //    result.RewardId = reward.Id;
        //    result.RemoveDateTime = DateTime.UtcNow.AddDays(RemoveDays);
        //    return result;
        //}

        public override void Update(InfoData infoData)
        {
            if (week_Rewards.Records.Any(r => r.RemoveInDB))
            {
                var count = week_Rewards.Records.RemoveAll(r => r.RemoveInDB);
                ILogger.Instance.Send("RemoveOldRecords removeCount: " + count, ErrorLevel.trace);
            }
        }

        public override void LoadFromDbComplite()
        {
            StringBuilder stringBuilder = new StringBuilder("\n");
            foreach (var record in personRewards.Values)
            {
                stringBuilder.AppendFormat("loginId: {0}, RewardId: {1}, rewardEndTime: {2}\n", record.loginId, record.Reward.RewardId, record.Reward.RemoveDateTime);
            }
            Sos.Trace("Reward.LoadFromDb: count: " + personRewards.Count + " logTime UtcNow: " + DateTime.UtcNow, stringBuilder.ToString());
        }


        public override void RemoveRecord(OneRecord record)
        {
            var key = record.Key();
            week_Rewards.RemoveRecords(key);
            RemoveRewardFromDb(key);
        }

        public void RemoveRecordsByDate(OneRecord record)
        {
            var key = record.Key();
            week_Rewards.RemoveByDateTime(key);
            Sos.Trace(string.Format("RemoveRecordsByDate: key: {0}", key));
        }

        private void RemoveRewardFromDb(long key)
        {
            //удалим из базы
            var adapter = RatingServer.GetAdapter();
            adapter.ExecStoredProc("RemoveReward", new Dictionary<string, object>() { { "Id", key } });
        }

        private void RemoveRewardByDateTimeFromDb()
        {
            //удалим из базы по дате
            var adapter = RatingServer.GetAdapter();
            adapter.ExecStoredProc("RemoveRewardDateTime", new Dictionary<string, object>() { { "DateTimeNow", DateTime.UtcNow } });
        }

        void ClearDb(OneRecord rewardRecord)
        {
            rewardRecord.MakeClear();
            ratingServer.DBStoreManager.AddWeekRewardForSave(rewardRecord);
        }


        public override int GetPosition(int id, int slotId = 0)
        {
            throw new NotImplementedException();
        }

        public override void SendInfoResponse(OneRecord oneRecord)
        {
            throw new NotImplementedException();
        }

        public override void SendInfoRequest(OneRecord oneRecord)
        {
            throw new NotImplementedException();
        }

        public override void Reset()
        {
            Sos.Trace("RewardInstance: ************* Reset**************");
            
            ratingServer.DBStoreManager.NeedRewardResetData();
            week_Rewards.ResetData();
        }

        public override void Stop()
        {
            week_Rewards.SaveData();
        }
    }
}
