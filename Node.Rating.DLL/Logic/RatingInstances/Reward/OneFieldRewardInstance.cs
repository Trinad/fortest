﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Utils.Logger;
using Node.Rating.Logic.Datas;
using Node.Rating.Logic.Records;

namespace Node.Rating.Logic.RatingInstances
{
    public class OneFieldRewardInstance : OneFieldInstance<RewardInstance>
    {
        public delegate int GetPoints(OneRewardRecord record);
        
        private List<OneRewardRecord> records;
        public List<OneRewardRecord> Records { get { return records; } }

        public OneFieldRewardInstance(RewardInstance rewardInstance, int count, IComparer<OneRewardRecord> comparer, GetPoints getPointsDelegate)
        {
            records = new List<OneRewardRecord>(count);
            PositionByLoginId = new Dictionary<long, int>(count);
            _instance = rewardInstance;
        }

        public override void Add(long key, OneRecord oneRecord)
        {
            var record = oneRecord as OneRewardRecord;
            if (record == null)
            {
                Sos.Error("OneFieldRatingInstance: Add record == null");
                return;
            }
            record.SetKey(key);
            record.loginId = oneRecord.loginId;
            if (!records.Contains(record))
            {
                records.Add(record);
            }

            bChange = true;

            //Sos.Warning("OneFieldRewardInstance: Add", string.Format("record.Key: {0} record.loginId: {1} record.Reward.Id: {2} record.Reward.LoginId: {3} record.Reward.RewardId: {4}", 
            //    record.Key, record.loginId, record.Reward.Id, record.Reward.LoginId, record.Reward.RewardId));
        }

        public override void Sort()
        {
            // награды сортированы
        }

        protected override void CheckSize()
        {

        }

        public override void ResetData()
        {
            if (records.Any(r => r.Reward.RemoveDateTime < DateTime.UtcNow))
            {
                var toRemove = records.FindAll(r => r.Reward.RemoveDateTime < DateTime.UtcNow);

                foreach (var rewardRecord in toRemove)
                {
                    rewardRecord.MakeClear();
                }
                ILogger.Instance.Send("RemoveOldRecords removeCount: " + toRemove.Count, ErrorLevel.trace);
            }
        }

        public override void SaveData()
        {

            try
            {
                var recordList = records.ToList().FindAll(r => r != null);

                for (int i = 0; i < recordList.Count; i++)
                {
                    var currentRecord = recordList[i];

                    if (currentRecord == null)
                        continue;
                    currentRecord.bChange = true;
                    _instance.SaveRecordToDB(currentRecord);
                }
            }
            catch (Exception e)
            {
                Sos.Error("OneFieldRatingInstance ResetData: ex: " + e.ToString());
                throw;
            }
        }

        public void RemoveByDateTime(long key)
        {
            var allKeys = records.FindAll(r => r != null && r.Key() == key && r.Reward.RemoveDateTime < DateTime.UtcNow);
            StringBuilder stringBuilder = new StringBuilder("\n");
            foreach (var record in allKeys)
            {
                stringBuilder.AppendFormat("record: id: {0}, RemoveDateTime: {1}, DateTime.UtcNow: {2}", record.Reward.RewardId, record.Reward.RemoveDateTime, DateTime.UtcNow);
                records.Remove(record);
            }
            Sos.Trace("RemoveByDateTime from Reward records keyCount: " + allKeys.Count, stringBuilder.ToString());
        }

        public override void RemoveRecords(long key)
        {
            var allKeys = records.FindAll(r => r != null && r.Key() == key);
            StringBuilder stringBuilder = new StringBuilder("\n");
            foreach (var record in allKeys)
            {
                stringBuilder.AppendFormat("record: id: {0}, RemoveDateTime: {1}, DateTime.UtcNow: {2}", record.Reward.RewardId, record.Reward.RemoveDateTime, DateTime.UtcNow);
                records.Remove(record);
            }
            Sos.Trace("Remove from Reward records keyCount: " + allKeys.Count, stringBuilder.ToString());
        }

        public bool CheckRewardDatas(long key)
        {
            return records.Any(r => r != null && r.Key() == key);
        }

        public List<RewardData> GetRewardDatas(long key)
        {
            var findedRecords = records.FindAll(r => r != null && r.Key() == key);
            if (findedRecords.Count == 0)
            {
                ILogger.Instance.Send("OneFieldRewardInstance: GetPersonRewardRpc findedRecords.Count == 0 ", ErrorLevel.warning);
                return new List<RewardData>();
            }
            var result = new List<RewardData>();
            foreach (var record in findedRecords)
            {
                var data = new RewardData()
                {
                    Id = record.Reward.Id,
                    LoginId = record.Reward.LoginId,
                    RewardId = record.Reward.RewardId,
                    RemoveDateTime = record.Reward.RemoveDateTime
                };
                result.Add(data);
            }

            return result;
        }
    }
}
