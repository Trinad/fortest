﻿using Node.Rating.Logic.Datas;
using Node.Rating.Logic.Records;
using Node.Rating.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace Node.Rating.Logic.RatingInstances.GameStatistic
{
    public class DBrecord<T>
    {
        public bool m_bChange;
        public T Data;
        public int Key;

        public DBrecord(int key)
        {
            Key = key;
        }
    }

    public class LoginGameStatistic
    {
        public int LoginId;
        public string Name;
        public string Avatar;
        public string AvatarBorder;
        public int AddedGold;
        public List<PersonGameStatistic> Persons = new List<PersonGameStatistic>();
        public Dictionary<ArenaMatchType, WinSeries> winSeries = new Dictionary<ArenaMatchType, WinSeries>();

        public LoginGameStatistic() { }


        public void AddRecord(int key)
        {
            dbRecord = new DBrecord<LoginGameStatistic>(key);
            dbRecord.Data = this;
        }
        public void SetDBrecordNew(int key)
        {
            AddRecord(key);
            //dbRecord.Key = LoginId;
            dbRecord.m_bChange = true;
        }

		public void RemovePersons()
		{			
			dbRecord.m_bChange = true;

			Persons.Clear();
			AddedGold = 0;

			foreach (var el in winSeries.Values)
				el.Reset();

		}

		internal PersonGameStatistic GetPerson(int personId)
        {
            var person = Persons.Find(x => x.PersonId == personId);
            return person;
        }

        public DBrecord<LoginGameStatistic> GetDBRecord()
        {
            return dbRecord;
        }

        private DBrecord<LoginGameStatistic> dbRecord;

        public void Init(RatingData uri)
        {
            dbRecord.m_bChange = true;
            LoginId = uri.LoginId;
            var person = Persons.Find(x => x.PersonId == uri.PersonId);
            if (person != null)
            {
                person.Init(uri);
            }
            else
            {
                PersonGameStatistic p = new PersonGameStatistic(uri);
                Persons.Add(p);
            }
        }

        public void Update(PersonSeries psr)
        {
            dbRecord.m_bChange = true;
            LoginId = psr.loginId;
            //AvatarBorder = uri.avatarBorder;
            var person = Persons.Find(x => x.PersonId == psr.personId);
            if (person != null)
            {
                person.Update(psr);
            }
            else
            {
                PersonGameStatistic p = new PersonGameStatistic(psr);
                Persons.Add(p);
            }
        }

        public void Update(PersonMaxDamage psr)
        {
            dbRecord.m_bChange = true;
            LoginId = psr.loginId;
            //AvatarBorder = uri.avatarBorder;
            var person = Persons.Find(x => x.PersonId == psr.personId);
            if (person != null)
            {
                person.Update(psr);
            }
            else
            {
                PersonGameStatistic p = new PersonGameStatistic(psr);
                Persons.Add(p);
            }
        }

        void CalculateWinSeries(bool isWin, ArenaMatchType arenaMatchType)
        {
            winSeries[ArenaMatchType.None].CalculateWinSeries(isWin);
            winSeries[arenaMatchType].CalculateWinSeries(isWin);
            }


        public void Update(RatingData uri)
        {
            CalculateWinSeries(uri.IsWin, uri.GameType);

            dbRecord.m_bChange = true;
            LoginId = uri.LoginId;
            var person = Persons.Find(x => x.PersonId == uri.PersonId);
            if (person != null)
            {
                person.Update(uri);
            }
            else
            {
                PersonGameStatistic p = new PersonGameStatistic(uri);
                Persons.Add(p);
            }
        }

        public void UpdateGold(int personId, int addGold, ArenaMatchType arenaType)
        {
            dbRecord.m_bChange = true;
            AddedGold += addGold;
            PersonGameStatistic person = Persons.Find(x => x.PersonId == personId);
            if (person == null)
            {
                person = new PersonGameStatistic(personId);
                Persons.Add(person);
            }

            person.battleStatistics.Apply(addGold, arenaType);
        }

        public void Apply(int personId)
        {
            dbRecord.m_bChange = true;
            var person = Persons.Find(x => x.PersonId == personId);
            if (person != null)
            {
                person.ApplyBattleStatistics();
            }
            //Sos.Debug($"LoginGameStatistic Apply Name {Name}");
        }

        public void ValidateBattleStat()
        {
            foreach (var person in Persons)
            {
                person.battleStatistics.PersonId = person.PersonId;
                person.battleStatistics.ValidateArenasFields();
            }

            foreach (ArenaMatchType arena in XmlStorage.Instance.statisticsXMLdata.arenaInfo.Select(x => x.ArenaType).ToList())
            {
                if (!winSeries.ContainsKey(arena))
                    winSeries[arena] = new WinSeries { arenaMatchType = arena };
        }
    }
    }

    public class PersonGameStatistic
    {
        public int PersonId;
        public PersonType PersonType = PersonType.None;
        public BattleStatistics battleStatistics = new BattleStatistics();
        public int Level = 0;
        public int rating_elo_coop = 0;
        public int rating_elo_team = 0;
        
        private RatingData lastUri;

        public PersonGameStatistic()
        {
            //TODO_maximpr пустой конструктор нужен для JsonConvert.DeserializeObject<...>
        }

        public PersonGameStatistic(int personId)
        {
            PersonId = personId;
            battleStatistics.PersonId = PersonId;
            battleStatistics.InitDictionaryKeys();
        }

        public PersonGameStatistic(RatingData uri)
        {
            rating_elo_coop = uri.rating_elo_coop;
            rating_elo_team = uri.rating_elo_team;

            PersonType = uri.PersonType;
            PersonId = uri.PersonId;
            Level = uri.Level;

            battleStatistics.PersonId = PersonId;
            battleStatistics.InitDictionaryKeys();
        }

        public PersonGameStatistic(PersonSeries ps)
        {
            PersonType = PersonType.None;
            PersonId = ps.personId;

            battleStatistics.PersonId = PersonId;
            battleStatistics.Apply(ps);
        }

        public PersonGameStatistic(PersonMaxDamage pmd)
        {
            PersonType = PersonType.None;
            PersonId = pmd.personId;

            battleStatistics.PersonId = PersonId;
            battleStatistics.Apply(pmd);
        }

        public void Update(RatingData uri)
        {
            Init(uri);

            if (lastUri != null && lastUri.LevelParam != uri.LevelParam)
            {
                battleStatistics = battleStatistics.Apply(lastUri);
            }
            lastUri = uri;
        }

        public void Update(PersonSeries ps)
        {
            battleStatistics.Apply(ps);
        }

        public void Update(PersonMaxDamage pmd)
        {
            battleStatistics.Apply(pmd);
        }

        public void Init(RatingData uri)
        {
            PersonType = uri.PersonType;
            PersonId = uri.PersonId;
            rating_elo_coop = uri.rating_elo_coop;
            rating_elo_team = uri.rating_elo_team;
            Level = uri.Level;
        }

        public void ApplyBattleStatistics()
        {
            if (lastUri != null)
            {
                battleStatistics = battleStatistics.Apply(lastUri);
                lastUri = null;
            }
        }

        public PersonBattleStatisticsData BattleStatisticForClient()
        {
            if (lastUri != null)
            {
                var battleStatistics = this.battleStatistics.Apply(lastUri);
                return battleStatistics.GetBattleRatingData();
            }
            else
                return battleStatistics.GetBattleRatingData();
        }

        public BattleStatistics BattleStatisticForClientNewFormat()
        {
            return battleStatistics.Copy();
        }
    }
}
