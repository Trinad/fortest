﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Node.Rating.Logic.Datas;
using Node.Rating.Logic.Records;

namespace Node.Rating.Logic.RatingInstances
{
    public interface IInstance
    {
        void Update(InfoData uri);
        void AddRecord(OneRecord record);
        void LoadFromDbComplite();
        int GetPosition(int id, int slotId = 0);
        void SendInfoResponse(OneRecord oneRecord);
        void Reset();
    }
}