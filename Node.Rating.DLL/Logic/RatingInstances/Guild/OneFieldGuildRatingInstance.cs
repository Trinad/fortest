﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Utils.Logger;
using Node.Rating.Logic.Records;
using UtilsLib;
using UtilsLib.low_engine_sharp.transport;

namespace Node.Rating.Logic.RatingInstances
{
    public class OneFieldGuildRatingInstance : OneFieldInstance<RatingGuildInstance>
    {
        public delegate int GetPoints(OneGuildRatingRecord record);
        //public GetPoints getPoints;

        private OneGuildRatingRecord[] records;
        IComparer<OneGuildRatingRecord> RatingComparer;

        public OneFieldGuildRatingInstance(RatingGuildInstance ratingInstance, int count, IComparer<OneGuildRatingRecord> comparer)
        {
            records = new OneGuildRatingRecord[count];
            PositionByLoginId = new Dictionary<long, int>(count);
            RatingComparer = comparer;
            _instance = ratingInstance;
            //getPoints = getPointsDelegate;
        }

        public List<int> GetTOPGuilds(int count, out string message)
        {
            List<int> res = new List<int>(count);
            ILogger.Instance.Send("Победители в рейтинге");
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < count && i < RecordCount; i++)
            {
                res.Add(records[i].Data.ClanId);
                string line = "№" + i + " " + records[i].Data.ClanName + "; GuildId " + records[i].Data.ClanId;
                ILogger.Instance.Send(line);
                sb.AppendLine(line);
            }
            message = sb.ToString();
            return res;
        }

        public override void Sort()
        {
            if (bChange)
            {
                //отсортировать все...
                bChange = false;
                Array.Sort<OneGuildRatingRecord>(records, 0, RecordCount, RatingComparer);
                for (int i = 0; i < RecordCount; i++)
                    PositionByLoginId[records[i].Data.ClanId] = i;
            }

        }

        protected override void CheckSize()
        {
            if (records.Length <= RecordCount)
            {
                OneGuildRatingRecord[] new_records = new OneGuildRatingRecord[records.Length * 2];
                for (int i = 0; i < records.Length; i++)
                {
                    new_records[i] = records[i];
                    records[i] = null;
                }
                records = new_records;
            }
        }

        public override void ResetData()
        {
            try
            {
                var recordList = records.ToList().FindAll(r => r != null);
                //AddAllRewards(recordList);
                ResetAllRecords(recordList);
                Sort();
                UpdatePlaceData(recordList);

            }
            catch (Exception e)
            {
                ILogger.Instance.Send("OneFieldRatingInstance ResetData: ex: " + e.ToString(), ErrorLevel.error);
                throw;
            }
        }

        void ResetAllRecords(List<OneGuildRatingRecord> recordList)
        {
            for (int i = 0; i < recordList.Count; i++)
            {
                var currentRecord = recordList[i];
                if (currentRecord == null)
                    continue;
                currentRecord.Reset();
            }
        }

        void UpdatePlaceData(List<OneGuildRatingRecord> recordList)
        {
            for (int i = 0; i < recordList.Count; i++)
            {
                var currentRecord = recordList[i];
                if (currentRecord == null)
                    continue;
                currentRecord.UpdatePlaceData(getPosition(currentRecord.Key()));
                _instance.SaveRecordToDB(currentRecord);
            }
        }

        public override void SaveData()
        {
            // сохранение в БД

            try
            {
                var recordList = records.ToList().FindAll(r => r != null);

                for (int i = 0; i < recordList.Count; i++)
                {
                    var currentRecord = recordList[i];

                    if (currentRecord == null)
                        continue;
                    currentRecord.bChange = true;
                    _instance.SaveRecordToDB(currentRecord);
                }
            }
            catch (Exception e)
            {
                Sos.Error("OneFieldRatingInstance ResetData: ex: " + e.ToString());
                throw;
            }
        }

        public override void Add(long key, OneRecord oneRecord)
        {
            OneGuildRatingRecord record = oneRecord as OneGuildRatingRecord;
            if (record == null)
            {
                Sos.Error("OneFieldGuildRatingInstance Add: record == null");
                return;
            }
            CheckSize();
            records[RecordCount] = record;
            PositionByLoginId[key] = RecordCount;
            RecordCount++;
            bChange = true;
        }

        public override void Clear()
        {
            RecordCount = 0;
            PositionByLoginId.Clear();
        }

        internal int getPosition(long key)
        {
            Sort();
            if (PositionByLoginId.ContainsKey(key))
                return PositionByLoginId[key] + 1;
            return RecordCount + 1;
        }

        internal Hashtable WebRequestRating(int guildId)
        {
            Sort();

            int pos = Math.Min(20, RecordCount);
            Hashtable ht = new Hashtable();
            ht["result"] = "success";
            ArrayList allRatings = new ArrayList();
            for (int i = 0; i < pos; i++)
            {
                Hashtable innerHt = new Hashtable();
                innerHt["name"] = records[i].Data.ClanId;
                innerHt["place"] = getPosition(records[i].Data.ClanId);
                innerHt["pvpPoints"] = 0;// records[i].pvpPoints;
                // GuildData
                innerHt["guildId"] = records[i].Data.ClanId;
                innerHt["guildName"] = records[i].Data.ClanName;
                innerHt["guildType"] = records[i].Data.ClanType;

				innerHt["guildSymbolId"] = 0; //cords[i].Data.GuildSymbolId; //TODO_deg Clan
				innerHt["guildSymbolColor"] = ""; // records[i].Data.GuildSymbolColor.Trim();
	            innerHt["guildBgColor"] = ""; //records[i].Data.GuildBgColor.Trim();

                //// recorData
                //innerHt["death"] = records[i].deathCounter;
                //innerHt["efficiency"] = records[i].efficiency;
                //innerHt["pvpPoints"] = records[i].pvpPoints; //getPoints(records[i]);
                //innerHt["gearScore"] = records[i].gearScore; //getPoints(records[i]);
                //innerHt["wins"] = records[i].wins;
                //innerHt["looses"] = records[i].looses;
                //innerHt["draw"] = records[i].draw;
                //innerHt["gw_efficiency"] = records[i].guildwar_efficiency;
                allRatings.Add(innerHt);
            }
            ht["rating"] = allRatings;

            if (guildId > 0)
            {
                int key = guildId;
                int position = getPosition(key);
                //int points = 0;
                Hashtable selfRating = new Hashtable();
                if (RecordCount > position)
                {
                    OneGuildRatingRecord oggrr = records[position - 1];

                    selfRating["name"] = oggrr.Data.ClanId;
                    selfRating["place"] = position;
                    selfRating["pvpPoints"] = 0;// oggrr.pvpPoints;
                    // GuildData
                    selfRating["guildId"] = oggrr.Data.ClanId;
                    selfRating["guildName"] = oggrr.Data.ClanName;
                    selfRating["guildType"] = oggrr.Data.ClanType;

	                selfRating["guildSymbolId"] = 0; //oggrr.Data.GuildSymbolId; //TODO_Deg Clan
	                selfRating["guildSymbolColor"] = ""; //oggrr.Data.GuildSymbolColor.Trim();
	                selfRating["guildBgColor"] = ""; //oggrr.Data.GuildBgColor.Trim();

                }
                else
                {
                    selfRating["name"] = "";
                    selfRating["place"] = position;
                    selfRating["pvpPoints"] = 0;
                    // GuildData
                    selfRating["guildId"] = 0;
                    selfRating["guildName"] = "";
                    selfRating["guildType"] = 0;

                    selfRating["guildSymbolId"] = 0;
                    selfRating["guildSymbolColor"] = "#ffffff";
                    selfRating["guildBgColor"] = "#ffffff";
                }

                ht["selfRating"] = selfRating;
            }
            return ht;

        }

        public override void RemoveRecords(long guildId)
        {
            bool bFound = false;
            for (int i = 0; i < RecordCount && !bFound; i++)
                if (records[i].Data.ClanId == guildId)
                {
                    bFound = true;
                    if (i + 1 != RecordCount)
                        records[i] = records[RecordCount - 1];
                    records[RecordCount] = null;
                    RecordCount--;
                }
            PositionByLoginId.Remove(guildId);
            bChange = true;
        }
    }
}
