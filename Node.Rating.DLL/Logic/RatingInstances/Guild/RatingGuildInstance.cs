﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Assets.Scripts.Utils.Logger;
using Node.Rating.Logic.Datas;
using Node.Rating.Logic.Records;
using Node.Rating.Utils;
using UtilsLib.low_engine_sharp.transport;

namespace Node.Rating.Logic.RatingInstances
{
    public class RatingGuildInstance : Instance
    {
        public event Action<Instance> OnResetRating = delegate { };

        Dictionary<int, OneGuildRatingRecord> guilds;

        //OneFieldGuildRatingInstance kill_rating;
        //OneFieldGuildRatingInstance death_rating;
        //OneFieldGuildRatingInstance efficiency_rating;
        //OneFieldGuildRatingInstance guildwars_efficiency_rating;
        OneFieldGuildRatingInstance pvp_rating;
        RatingServer ratingServer;

        public RatingGuildInstance(RatingServer ratingServer, int count, DateTime dtRatingResetDate) : base(dtRatingResetDate)
        {
            this.dtRatingResetDate = dtRatingResetDate;
            this.ratingServer = ratingServer;
            guilds = new Dictionary<int, OneGuildRatingRecord>(count);
            //kill_rating = new OneFieldGuildRatingInstance(count, new Comparers.GuildKillCountComparer());
            //death_rating = new OneFieldGuildRatingInstance(count, new Comparers.GuildDeathCountComparer());
            //efficiency_rating = new OneFieldGuildRatingInstance(count, new Comparers.GuildEfficiencyCountComparer());
            //guildwars_efficiency_rating = new OneFieldGuildRatingInstance(count, new Comparers.GuildWarEfficiencyCountComparer());
            pvp_rating = new OneFieldGuildRatingInstance(this, count, new Comparers.GuildXPCountComparer());

            //this.saveMethod = saveMethod;
        }

        private OneGuildRatingRecord GetGlobalGuildRatingRecord(int key, UpdateGuildRatingInfo uri)
        {
            if (!guilds.ContainsKey(key))
            {
                OneGuildRatingRecord record = new OneGuildRatingRecord(uri);
                guilds[key] = record;
                //kill_rating.Add(key, record);
                //death_rating.Add(key, record);
                //efficiency_rating.Add(key, record);
                //guildwars_efficiency_rating.Add(key, record);
                pvp_rating.Add(key, record);
            }
            return guilds[key];
        }

        public override void Update(InfoData infoData)
        {
            var data = infoData as UpdateGuildRatingInfo;
            if (data == null)
            {
                Sos.Error("RatingGuildInstance: Update data == null");
                return;
            }
                
            int key = data.Data.ClanId;
            if (key == -1)
            {
                Sos.Error("RatingGuildInstance: Update key == -1");
                return;
            }

            OneGuildRatingRecord record = GetGlobalGuildRatingRecord(key, data);
            if (record != null)
            {
                record.Update(data, false);
                pvp_rating.bChange = record.bChange;
                pvp_rating.Sort();

                if (record.bChange && !record.InSaveQueue)
                {
                    record.InSaveQueue = true;
                    ratingServer.DBStoreManager.AddWeekGuildRatingForSave(record);
                }
            }
        }

        private bool CanAddAchievementXP = true;
        internal void Set_CanAddAchievementXP(bool canAdd)
        {
            CanAddAchievementXP = canAdd;
        }

        public override void AddRecord(OneRecord oneRecord)
        {
            var record = oneRecord as OneGuildRatingRecord;

            if (record == null)
            {
                Sos.Error("RatingGuildInstance: AddRecord: record == null");
                return;
            }

            int key = record.Data.ClanId;
            guilds[key] = record;
            //kill_rating.Add(key, record);
            //death_rating.Add(key, record);
            //efficiency_rating.Add(key, record);
            //guildwars_efficiency_rating.Add(key, record);
            pvp_rating.Add(key, record);
        }

        public override void LoadFromDbComplite()
        {
            pvp_rating.Sort();

            StringBuilder stringBuilder = new StringBuilder("\n");
            foreach (var record in guilds.Values)
            {
                stringBuilder.AppendFormat("loginId: {0}, name: {1}, guildType: {2}\n", record.loginId, record.Data.ClanName, record.Data.ClanType);
            }
            Sos.Trace("Guild Rating.LoadFromDb: count: " + guilds.Count + " logTime UtcNow: " + DateTime.UtcNow, stringBuilder.ToString());

        }

        public override void RemoveRecord(OneRecord record)
        {
            
        }

        public override void SaveRecordToDB(OneRecord oneRecord)
        {
            var record = oneRecord as OneGuildRatingRecord;
            if (record == null)
            {
                Sos.Error("RatingInstance Save record == null");
                return;
            }

            if ((record.bChange && !record.InSaveQueue))
            {
                record.InSaveQueue = true;
                ratingServer.DBStoreManager.AddWeekGuildRatingForSave(record);
            }
        }

        public override int GetPosition(int guildId, int slotId = 0)
        {
            if (guildId == -1)
                return -1;
            int key = guildId;
            //int pos = efficiency_rating.getPosition(key);
            int pos = pvp_rating.getPosition(key);
            //ratingServer.SysDispatcher.SendRatingPosition(loginId, personType, pos);
            return pos;
        }

        //public override Hashtable WebGuildRequest(int guildId)
        //{
        //    return pvp_rating.WebRequestRating(guildId);
        //}

        public override void SendInfoResponse(OneRecord oneRecord)
        {
            return;
        }

        public override void SendInfoRequest(OneRecord oneRecord)
        {
            return;
        }

        public Hashtable WebRequest(int fieldId, int guildId, int guildRank, string guildName)
        {
            CheckResetRating();

            OneFieldGuildRatingInstance instance = pvp_rating;

            return instance.WebRequestRating(guildId);
        }

        //public DateTime dtRatingResetDate = DateTime.MinValue;
        private bool CheckResetRating()
        {
            //if (!CanResetRating)
            //    return;

            if (DateTime.UtcNow > dtRatingResetDate)
            {
                dtRatingResetDate = DateTime.MaxValue;
                ResetRating();

                Sos.Debug("OneFieldRatingInstance: Reset Week");
                return true;
            }
            return false;
        }
        private void ResetRating()
        {
            OnResetRating(this);
        }

        internal void OnGuildWarResult(GuildWarData data)
        {
            OneGuildRatingRecord record1 = null;
            OneGuildRatingRecord record2 = null;
            if (guilds.ContainsKey(data.guild1))
                record1 = guilds[data.guild1];
            if (guilds.ContainsKey(data.guild2))
                record2 = guilds[data.guild2];
            if (data.result > 0)
            {
                if (record1 != null)
                {
                    record1.wins++;
                    record1.bChange = true;
                }
                if (record2 != null)
                {
                    record2.looses++;
                    record2.bChange = true;
                }
            }
            else if (data.result < 0)
            {
                if (record2 != null)
                {
                    record2.wins++;
                    record2.bChange = true;
                }
                if (record1 != null)
                {
                    record1.looses++;
                    record1.bChange = true;
                }
            }
            else //data.data == 0
            {
                if (record2 != null)
                {
                    record2.draw++;
                    record2.bChange = true;
                }
                if (record1 != null)
                {
                    record1.draw++;
                    record1.bChange = true;
                }
            }

            if (record1 != null)
            {
                record1.guildwar_efficiency = OneGuildRatingRecord.GetGuildwarEfficiency(record1.wins, record1.looses, record1.draw);
                record1.bChange = true;
            }
            if (record2 != null)
            {
                record2.guildwar_efficiency = OneGuildRatingRecord.GetGuildwarEfficiency(record2.wins, record2.looses, record2.draw);
                record2.bChange = true;
            }

            //if (saveMethod != null)
            {
                if (record1 != null && record1.bChange && !record1.InSaveQueue)
                {
                    record1.InSaveQueue = true;
                    ratingServer.DBStoreManager.AddWeekGuildRatingForSave(record1);
                }
                if (record2 != null && record2.bChange && !record2.InSaveQueue)
                {
                    record2.InSaveQueue = true;
                    ratingServer.DBStoreManager.AddWeekGuildRatingForSave(record2);
                }
            }
        }

        internal void OnDisbandGuild(int guildId)
        {
            OneGuildRatingRecord record = null;
            if (guilds.ContainsKey(guildId))
            {
                record = guilds[guildId];
                guilds.Remove(guildId);
                //kill_rating.RemoveRecords(guildId);
                //death_rating.RemoveRecords(guildId);
                //efficiency_rating.RemoveRecords(guildId);
                //guildwars_efficiency_rating.RemoveRecords(guildId);

                pvp_rating.RemoveRecords(guildId);

                record.forDelete = true;
                //if (saveMethod != null)
                ratingServer.DBStoreManager.AddWeekGuildRatingForSave(record);
            }
        }

        public override void Reset()
        {
            Sos.Trace("Guild RatingInstance: ************* Reset**************");
            pvp_rating.ResetData();
        }

        public override void Stop()
        {
            pvp_rating.SaveData();
        }

        public void ClearDb()
        {
            //OneGuildRatingRecord resetRecord = new OneGuildRatingRecord(false);
            //resetRecord.MakeReset();
            //saveMethod(resetRecord);
        }
    }
}
