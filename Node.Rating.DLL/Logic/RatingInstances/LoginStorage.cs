﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Utils.Logger;
using UtilsLib.NetworkingData;
using Node.Rating.Logic.RatingInstances.GameStatistic;
using Newtonsoft.Json;
using Node.Rating.Logic.Records;
using UtilsLib.DegSerializers;
using Node.Rating.Logic.Datas;
using UtilsLib.Logic.Enums;
using Node.Rating.DLL.Logic;

namespace Node.Rating.Logic.RatingInstances
{
    public class LoginStorage
    {
        SmallCache<LoginGameStatistic> logins;
        private readonly RatingServer ratingServer;

        public LoginStorage(RatingServer ratingServer)
        {
            logins = new SmallCache<LoginGameStatistic>(1000, 1500,
                (loginId) =>
                {
                    var loginGameStatistic = RatingServer.GetAdapter().LoadLoginRating(loginId);
                    if (loginGameStatistic != null)
                        ratingServer.RatingManager.LoginRating.RecalcGameStatistic(loginGameStatistic);
                    return loginGameStatistic;
                },
                (loginId, loginGameStat) => {
                    if (loginGameStat != null)
                        SaveRecordToDB(loginGameStat.GetDBRecord());
                });
        
            this.ratingServer = ratingServer;
        }

        public void AddLoginGameStatistic(LoginGameStatistic loginGameStatistic)
        {
            logins.Add(loginGameStatistic.LoginId, loginGameStatistic);

            //TODO_maximpr нужнн какое-то событие на обновление данных
            ratingServer.RatingManager.LoginRating.RecalcGameStatistic(loginGameStatistic);
        }

        private LoginGameStatistic GetOrCreateGlobalRatingRecord(int loginId)
        {
            LoginGameStatistic record = logins.Get(loginId);

            if (record == null)
            {
                record = new LoginGameStatistic();
                record.SetDBrecordNew(loginId);
                record.ValidateBattleStat();
                logins.Add(loginId, record);
            }

            return record;
        }

        private LoginGameStatistic GetGlobalRatingRecord(long loginId)
        {
            LoginGameStatistic record = logins.Get(loginId);

            return record;
        }

        public void Update(PersonSeries psr)
        {
            long key = psr.loginId;
            var record = GetGlobalRatingRecord(key);

            if (record == null)
            {
                ILogger.Instance.Send("Update(PersonSeries psr) record == null", ErrorLevel.error);
                return;
            }
            record.Update(psr);
            SaveRecordToDB(record.GetDBRecord());
        }

        public void Update(PersonMaxDamage pmd)
        {
            long key = pmd.loginId;
            var record = GetGlobalRatingRecord(key);

            if (record == null)
            {
                ILogger.Instance.Send("Update(PersonMaxDamage psr) record == null", ErrorLevel.error);
                return;
            }
            record.Update(pmd);
            SaveRecordToDB(record.GetDBRecord());
        }

        public void UpdateGold(int loginId, int personId, int goldCount, ArenaMatchType arenaType)
        {
            var record = GetGlobalRatingRecord(loginId);

            if (record == null)
            {
                ILogger.Instance.Send("UpdateGold(...) record == null", ErrorLevel.error);
                return;
            }
            record.UpdateGold(personId, goldCount, arenaType);
            SaveRecordToDB(record.GetDBRecord());
        }

        public void RemovePersons(int loginId)
        {
            var record = GetGlobalRatingRecord(loginId);
			
			if (record == null)
                return;

			record.RemovePersons();

            ratingServer.RatingManager.LoginRating.RecalcGameStatistic(record);
			SaveRecordToDB(record.GetDBRecord());
        }

		public void RemoveLogin(int loginId)
		{
			var record = GetGlobalRatingRecord(loginId);
			if (record == null)
				return;

			logins.RemoveFromCache(loginId);
			ratingServer.RatingManager.LoginRating.RemoveRating(loginId);

			ratingServer.DBStoreManager.RemoveRecordFromDb(record.GetDBRecord());

			ILogger.Instance.Send($"RemoveLogin from rating and DB loginId: {loginId} ");
		}

		public void Apply(int loginId, int personId)
        {
            var record = GetGlobalRatingRecord(loginId);
            if (record == null)
            {
                ILogger.Instance.Send($"RatingInstance: Apply record == null loginId: {loginId} personId: {personId}", ErrorLevel.error);
                return;
            }

            ILogger.Instance.Send($"RatingInstance  Apply: LoginId{loginId}, PersonId{personId}");

            record.Apply(personId);
            ratingServer.RatingManager.LoginRating.RecalcGameStatistic(record);
            SaveRecordToDB(record.GetDBRecord());
        }

        public void UpdateLoginData(LoginData data)
        {
            var loginData = GetOrCreateGlobalRatingRecord(data.LoginId);

            loginData.Avatar = data.Avatar;
            loginData.Name = data.Name;
            ratingServer.RatingManager.LoginRating.RecalcGameStatistic(loginData);
        }

        public void Update(RatingData data)
        {
            var key = data.LoginId;
            var record = GetOrCreateGlobalRatingRecord(key);

            RatingRequestType reqType = (RatingRequestType)data.RequestType;
            ILogger.Instance.Send($"RatingInstance data = success: LoginId{data.LoginId}, PersonId{ data.PersonId}, Update reqType {reqType}");

            switch (reqType)
            {
                case RatingRequestType.Init:
                    record.Init(data);
                    break;
                case RatingRequestType.Update:
                    record.Update(data);
                    break;
            }
            ratingServer.RatingManager.LoginRating.RecalcGameStatistic(record);

            SaveRecordToDB(record.GetDBRecord());
        }

        public void SaveRecordToDB(DBrecord<LoginGameStatistic> record)
        {
            if (record == null)
            {
                ILogger.Instance.Send("RatingInstance Save record == null");
                return;
            }

            ratingServer.DBStoreManager.AddLoginGameStatisticForSave(record);
        }

        public void WebRequest(int loginId, int personId, int fieldId, string name, int ConnectId)
        {
            Hashtable htResponse = new Hashtable();
            ArrayList heroRating = new ArrayList();
            AllPersonsBattleStatisticsData personRatings = new AllPersonsBattleStatisticsData();

            var record = GetGlobalRatingRecord(loginId);

            if (record != null)
            {
                personRatings.totalGoldCount = record.AddedGold;
                foreach (var heroPerson in record.Persons)
                {
                    personRatings.personBattleStatisticsDatas.Add(heroPerson.BattleStatisticForClient());
                }
            }

            string JSONstr = JSON.JsonEncode(personRatings.JsonEncode());

            ratingServer.SysFlashPolicyFile.SendText(JSONstr, ConnectId);
        }

        ArenaStatisticsData CreateArenaStatisticsData( ArenaBattleStatistic arenaBattleStatistic)
        {
            var arenaData = new ArenaStatisticsData(arenaBattleStatistic);
            var arenaInfo = XmlStorage.Instance.statisticsXMLdata.arenaInfo.Find(x => x.ArenaType == arenaBattleStatistic.arenaType);
            if (arenaInfo != null)
            {
                Hashtable ht = (Hashtable)arenaBattleStatistic.JsonEncode();

                foreach (var el in arenaInfo.VisibleData[0])
                {
                    arenaData.firstColumn.Add(new Records.BattleStatRecord(el, arenaBattleStatistic.GetParam(el)));
                }
                foreach (var el in arenaInfo.VisibleData[1])
                {
                    arenaData.secondColumn.Add(new Records.BattleStatRecord(el, arenaBattleStatistic.GetParam(el)));
                }
            }
            return arenaData;
        }

        LoginStatisticsData GetDataForClient(LoginGameStatistic loginGameStatistic, BattleStatistics battleStatistics)
        {
            var loginStat = new LoginStatisticsData();

            List<ArenaStatisticsData> arenadata = new List<ArenaStatisticsData>();

            var total = battleStatistics.GetTotal();
            //устанавливаем винсерю по логину
            var win = loginGameStatistic?.winSeries[ArenaMatchType.None].winSeriesMax ?? 0;
            total.SetValue(BattleStatType.WinMax, win);

            arenadata.Add(CreateArenaStatisticsData(total));

            foreach (var battleStat in battleStatistics.arenaBattleStatistics)
            {
                var winSeries = loginGameStatistic?.winSeries[battleStat.arenaType].winSeriesMax ?? 0;
                battleStat.SetValue(BattleStatType.WinMax, winSeries);
                var arenaStatData = CreateArenaStatisticsData( battleStat);
                arenadata.Add(arenaStatData);
            }

            loginStat.arenaStatisticsDatas = arenadata;
            return loginStat;
        }

        public void WebRequestStatistic(int loginId, int connectId)
        {
            var record = GetGlobalRatingRecord(loginId);
            BattleStatistics summary = new BattleStatistics();
            summary.InitDictionaryKeys();

            if (record != null)
            {
                foreach (var heroPerson in record.Persons)
                {
                    var personBtlstat = heroPerson.BattleStatisticForClientNewFormat();
                    summary.UpdateWith(personBtlstat);
                }
            }
            LoginStatisticsData loginStatisticsData = GetDataForClient(record, summary);
            string JSONstr = JSON.JsonEncode(loginStatisticsData.JsonEncode());

            ratingServer.SysFlashPolicyFile.SendText(JSONstr, connectId);
        }

        public void Stop()
        {
            foreach (var loignGameStat in logins.AllCachedValues())
            {
                try
                {
                    if (loignGameStat == null)
                    {
                        ILogger.Instance.Send("LoginStorage.Stop() loignGameStat == null", ErrorLevel.error);
                        continue;
                    }

                    if (loignGameStat.GetDBRecord().m_bChange)
                        SaveRecordToDB(loignGameStat.GetDBRecord());
                }
                catch (Exception e)
                {
                    ILogger.Instance.Send("LoginStorage.Stop() ex: " + e.ToString(), ErrorLevel.exception);
                }
            }
        }

        public void Reset()
        {
            Sos.Trace("RatingInstance: ************* Reset**************");
        }
    }
}
