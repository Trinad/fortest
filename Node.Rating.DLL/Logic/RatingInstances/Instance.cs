﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Node.Rating.Logic.Datas;
using Node.Rating.Logic.RatingInstances.GameStatistic;
using Node.Rating.Logic.Records;

namespace Node.Rating.Logic.RatingInstances
{
    public abstract class Instance : IInstance
    {
        protected  DateTime dtRatingResetDate = DateTime.MinValue;
        public DateTime DtRatingResetDate { get { return dtRatingResetDate; } }

        private const int PersonMaxCount = 8;

        public static long GetKey(int loginId, int personId)
        {
            return personId;
        }

        public Instance(DateTime dtRatingResetDate)
        {
            this.dtRatingResetDate = dtRatingResetDate;
        }

        public void ResetRatingTimer(DateTime newtime)
        {
            dtRatingResetDate = newtime;
        }
        public abstract void Update(InfoData uri);
        public abstract void AddRecord(OneRecord record);
        public abstract void LoadFromDbComplite();
        public abstract void RemoveRecord(OneRecord record);
        public abstract void SaveRecordToDB(OneRecord record);
        public abstract int GetPosition(int id, int slotId = 0);
        public abstract void SendInfoResponse(OneRecord oneRecord);
        public abstract void SendInfoRequest(OneRecord oneRecord);
        public abstract void Reset();
        public abstract void Stop();
    }
}
