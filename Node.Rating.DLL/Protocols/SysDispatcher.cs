﻿using System.Threading;
using Fragoria.Common.Utils;
using Node.Rating.Logic;
using Node.Rating.Utils;
using UtilsLib;
using UtilsLib.low_engine_sharp.transport;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using System;
using NSubstitute;
using UtilsLib.Protocol;
using Node.Rating.Logic.RatingInstances;

namespace Node.Rating.Protocols
{
    public class SysDispatcher : BaseCmdSystem, IRatingSysDispatcher, IRatingSysInstance
	{
        public SysDispatcher(RatingServer ratingServer)
            : base(SystemConfig.sDispatcherCode)
        {
            this.ratingServer = ratingServer;
        }

        public override void MakeCommandsTable()
        {
			FillCommand<IRatingSysDispatcher>();
			FillCommand<IRatingSysInstance>();
        }

		private readonly IDispatcherSysRating dispatcher = Substitute.For<IDispatcherSysRating>();
        private readonly RatingServer ratingServer;
        public static Connection DsConnection;

        public static void UpdateConnection(RatingServer server)
        {
            DsConnection = server.Dispatcher.GetConnectToDispatcher();
        }

        private static int pingNumber;
        public void DoPing()
        {
            if (DsConnection != null)
            {
				SendRpc(dispatcher.OnPing, pingNumber);
				pingNumber = (pingNumber + 1) % 100;
            }
        }

        public void OnPing(int ping)
        {
            //приняли пинг. он нам нафиг не нужен, но примем)))
        }

        public override bool Start()
        {
            DsConnection = GetConnectToDispatcher();
            return base.Start();
        }

        private ManualResetEvent connectEvent = new ManualResetEvent(false);
        private string ticket = string.Empty;
        Connection tempConnection;

        public Connection GetConnectToDispatcher()
        {
            if (DsConnection != null)
            {
                transport.Disconnect(DsConnection, DisconnectReason.UserRequest);
            }

            tempConnection = Connection.Connect(SystemConfig.DispatcherServer, SystemConfig.DispatcherServerPort);
            if (tempConnection == null)
                return null;

            ticket = string.Empty;

            tempConnection.SetCanBufferSendOperation(false);
            tempConnection.dos_border = -1;
            tempConnection.DestinationType = DestinationType.Server;
            tempConnection.SetCanBufferSendOperation(false);

			FillRPCByMethod(dispatcher, tempConnection);

            connectEvent.Reset();
			//отправить туда запрос на тикет. получить тикет.
			//отправить запрос на авторизацию. получить авторизацию.

			SendRpc(dispatcher.OnGetTicket);
            if (!connectEvent.WaitOne(SystemConfig.iMillisecondsRecieveTimeout, false))
            {
                ILogger.Instance.Send("Rating.Start: SysDispatcher don't respond for GET TICKET command, ip = " + SystemConfig.DispatcherServer + " port = " + SystemConfig.DispatcherServerPort, ErrorLevel.error);
                return null;
            }

            //отсылаем имя и закрытый ключ сервера + порт на котором принимаем игроков
            connectEvent.Reset();

	        SendRpc(dispatcher.OnGetAuth, ByteArray.Encode(SystemConfig.sServerSecretKey, ticket));
            if (!connectEvent.WaitOne(SystemConfig.iMillisecondsRecieveTimeout, false))
            {
                ILogger.Instance.Send("AuthServer.Start: SysDispatcher don't respond for GET AUTH command, ip = " + SystemConfig.DispatcherServer + " port = " + SystemConfig.DispatcherServerPort, ErrorLevel.error);
                return null;
            }

            ILogger.Instance.Send("Rating.Start: complete connect to dispatcher");

            return tempConnection;
        }

        public void OnGetTicket(string newTicket, Connection sender)
        {
            if (sender == tempConnection)
            {
                ticket = newTicket;
                connectEvent.Set();
            }
        }

        public void OnGetAuth(bool bAuth, Connection sender)
        {
            if (sender == tempConnection)
            {
                connectEvent.Set();
            }
        }

        public void OnApplyRating(int loginId, int personId)
        {
            ILogger.Instance.Send($"OnApplyRating loginId: {loginId} personId: {personId}");
            ratingServer.RatingManager.AddRequest(new RatingManagerRequest((loginStorage) => loginStorage.Apply(loginId, personId), -1));
        }

		public void OnRemoveRating(int loginId, bool isPersons)
        {
            ILogger.Instance.Send($"OnRemoveRating loginId: {loginId}");

			Action<LoginStorage> action = null;
			if (isPersons)
				action = (loginStorage) => { loginStorage.RemovePersons(loginId); };
			else
				action = (loginStorage) => { loginStorage.RemoveLogin(loginId); };

			ratingServer.RatingManager.AddRequest(new RatingManagerRequest(action, -1));
		}

		public void OnUpgradeRating(RatingData ratingData)
        {
            ILogger.Instance.Send($"OnUpgradeRating loginId: {ratingData.LoginId} personId: {ratingData.PersonId}, ratingData.Place: {ratingData.Place}, ratingData.GameType: {ratingData.GameType} ");
            ratingServer.RatingManager.AddRequest(new RatingManagerRequest((loginStorage) => loginStorage.Update(ratingData), -1));
        }

	    public void OnUpdateLoginData(LoginData data)
	    {
	        ratingServer.RatingManager.AddRequest(new RatingManagerRequest((loginStorage) => loginStorage.UpdateLoginData(data), -1));
        }

        public void OnUpdateGold(int loginId, int personId, int goldCount, ArenaMatchType arenaType)
        {
            ILogger.Instance.Send($"OnUpdateGold loginId: {loginId} personId: {personId}, goldCount = {goldCount}, arenaType = {arenaType}");
            ratingServer.RatingManager.AddRequest(new RatingManagerRequest((loginStorage) => loginStorage.UpdateGold(loginId, personId, goldCount, arenaType), -1));
        }

        public void OnUpgradeKillSeriesRating(PersonSeries psr)
        {
            ILogger.Instance.Send($"OnUpgradeKillSeriesRating loginId: {psr.loginId} personId: {psr.personId}, totalKills = {psr.totalKills}");
            ratingServer.RatingManager.AddRequest(new RatingManagerRequest((loginStorage) => loginStorage.Update(psr), -1));
        }

		public void OnUpgradeMaxDamageRating(PersonMaxDamage pmd)
        {
            ILogger.Instance.Send($"OnUpgradeMaxDamageRating loginId: {pmd.loginId} personId: {pmd.personId}, maxInDamag = {pmd.maxInDamag}, maxOutDamag = {pmd.maxOutDamag}");
            ratingServer.RatingManager.AddRequest(new RatingManagerRequest((loginStorage) => loginStorage.Update(pmd), -1));
        }

        internal void SendPersonRequest(int loginId)
        {
	        SendRpc(dispatcher.OnPersonRequest, loginId);
        }

        public void OnXmlChanged(string fileName, string data)
        {
            switch (fileName)
            {
                case "arenaData.xml":
                    OnArenaXmlChanged(data);
                    break;
            }
        }

        private void OnArenaXmlChanged(string data)
        {
            try
            {
                string decodedata = ZipUtils.GetUnZipBase64DecodeXorString(data, string.Empty);
                //m_arenaAlgorithm.SetXmlData(decodedata.LoadFromStringXml<ArenaAlgorithmData>());
                //ratingServer.RatingManager.LoginGameStatistic.arenaAlgorithmData = decodedata.LoadFromStringXml<ArenaAlgorithmData>();
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("Error in OnXmlChanged (arenaData.xml):" + ex, ErrorLevel.exception);
            }
        }
    }
}
