using System;
using System.Globalization;
using Fragoria.Common.Utils;
using Node.Rating.Logic;
using Node.Rating.Utils;
using UtilsLib;
using UtilsLib.Logic;
using UtilsLib.low_engine_sharp.transport;
using UtilsLib.Protocol;

namespace Node.Rating.Protocols
{
    /// <summary>
    /// ��� �������� ����� � ���������
    /// </summary>
    public class FlashPolicyFile : BaseCmdSystem, IRatingSysFlashPolicyFile
    {
        public const string CommandGetCrossDomain = "GC"; // ������ ������ ���� � ���������� ������������ - ����������� GF, ������ ������ �������� �� <policy-file-request>, � crossdomain.xml
        public const string CommandExternalAPI = "EA";

        public FlashPolicyFile()
            : base(transport.scSpecCommandSystem)
        {
        }

        public override void MakeCommandsTable()
        {
			FillCommand<IRatingSysFlashPolicyFile>();
        }

        private static string MakeHttpRedirect(string url, string data = "")
        {
            return "HTTP/1.1 307 Temporary Redirect\r\nLocation: " + url + "\r\n\r\n" + (string.IsNullOrEmpty(data) ? "\0" : data);
        }

        private static string MakeHttpError()
        {
            return "HTTP/1.1 400 Bad Request\r\n\r\n\0";
        }

        public static bool IsValidHost(string ip)
        {
            return true; // AuthManager.ValidHostController.IsCorrectMask(ip);
        }

		public void OnExternalApi(string sRequest, Connection sender)
        {
            string result;
            try
            {
                try
                {
                    if (!IsValidHost(sender.sIP))
                        return;
                }
                catch (Exception ex)
                {
                    ILogger.Instance.Send(ex.ToString(), ErrorLevel.error);
                    return;
                }

                ILogger.Instance.Send("OnExternalApi - REQUEST: " + sRequest);

                //string sign;
                string data;
                string[] stringSeparators = { SystemConfig.ExternalApiUrl, "HTTP/" };
                string[] stringSepNL = new string[] { "\n" };

                if (sRequest.StartsWith("POST "))
                {
                    string[] lines = sRequest.Split(stringSepNL, StringSplitOptions.RemoveEmptyEntries);
                    data = sRequest.Split(stringSeparators, StringSplitOptions.None)[1].Trim();
                    //sign = HttpHelperCommon.GetParamByName(data, "sign");
                    data = lines[lines.Length - 1].Trim();
                }
                else
                {
                    data = sRequest.Split(stringSeparators, StringSplitOptions.None)[1].Trim();
                    //sign = HttpHelperCommon.GetParamByName(data, "sign");
                }
                result = SysExternalApi.Process(data, /*sign,*/ sender);
                ILogger.Instance.Send("OnExternalApi - ANSWER: " + result);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send(ex.ToString(), ErrorLevel.error);
                result = SysExternalApi.MakeSysError("Exception");
            }

            if (result != null)
            {
                SendRpc(sSystemCode, CommandExternalAPI, TimeProvider.UTCNow.AddSeconds(15), sender, result.MakeHttpOK());
            }
        }

		public void OnGetCrossDomain(string sRequest, Connection sender)
        {
            if (sRequest.IndexOf("crossdomain.xml", StringComparison.Ordinal) > -1)
            {
                string sMsg = SystemConfig.sFlashDomainPolicy.Replace('{', '<').Replace('}', '>').Replace('\'', '\"');
                string sResponce = HttpHelper.sResponceSuccessXml + "Content-Length: " + ByteArray.UTF8Length(sMsg) + "\r\n\r\n" + sMsg;
                SendRpc(sSystemCode, CommandGetCrossDomain, sender, sResponce);
                // �� ���� ��� ���������� - �.�. �� ���� ���� ����� �������� ����� �������� ��� ��� ����...
                //Send(cmd.Sender, c, true, TimeProvider.UTCNow.AddSeconds(5));
            }
        }

        internal void WebRatingRequestData(System.Collections.Hashtable ht, int connectId)
        {
            string result = SysExternalApi.MakeGetRatingResult(ht);
            SendText(result, connectId);
        }
        
        internal void WebGuildRatingRequestData(System.Collections.Hashtable ht, int connectId)
        {
            string result = SysExternalApi.MakeGetGuildRatingResult(ht);
            SendText(result, connectId);
        }

        internal void SendText(string text, int connectId)
        {
	        if (transport.TryGetConnectionById(connectId, out var connect))
                SendRpc(sSystemCode, CommandExternalAPI, TimeProvider.UTCNow.AddSeconds(3), connect, text.MakeHttpOK());
        }

        internal void SendError(int connectId)
        {
	        if (transport.TryGetConnectionById(connectId, out var connect))
                SendRpc(sSystemCode, CommandExternalAPI, TimeProvider.UTCNow.AddSeconds(3), connect, string.Empty.MakeHttpERROR());
        }
    }
}