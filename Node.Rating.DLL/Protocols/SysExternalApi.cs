﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Assets.Scripts.Utils.Logger;
using Fragoria.Common.Utils.ObjectPool;
using Node.Rating.Logic;
using Node.Rating.Logic.Datas;
using Node.Rating.Utils;
using UtilsLib;
using UtilsLib.low_engine_sharp.transport;
using System;
using UtilsLib.Logic.Enums;

namespace Node.Rating.Protocols
{
    public class SysExternalApi
    {
        private delegate string CommandProcessor(NameValueCollection dic, Connection connect);
        private readonly static Dictionary<string, CommandProcessor> Handlers = new Dictionary<string, CommandProcessor>();
        private static readonly Encoding Encoding = new ASCIIEncoding();

        //private readonly static byte[] secretKeyByteArray;

        static SysExternalApi()
        {
            Handlers.Add("GetRating", GetRating);
            Handlers.Add("GetBattleStatistic", GetBattleStatistic);
            Handlers.Add("GetGuildRating", GetGuildRating);
            Handlers.Add("GetStatistic", GetStatistic);
            Handlers.Add("GetLoginStatisticsXML", GetXML);
            //Handlers.Add("LoginChange", LoginChange);
            //Handlers.Add("LoginHash", LoginHash);
            //Handlers.Add("RequestHash", LoginHash);
            //Handlers.Add("LoginsInfo", LoginsInfo);
            //Handlers.Add("LoginsOnline", LoginsOnline);

            //secretKeyByteArray = Encoding.UTF8.GetBytes(SystemConfig.ExternalApiKey);
        }

        static RatingServer ratingServer;
        public static void SetServer(RatingServer server)
        {
            ratingServer = server;
        }

        public static string Process(string req, /*string sign,*/ Connection connect)
        {
            //string xReq = getRealRequest(req);
            try
            {
                //if (!CheckSign(sign, req))
                //{
                //    ILogger.Instance.Send(MType.ExternalApi, SType.Warning, "Process: Invalid Md5 sign");
                //    //return MakeSysError("Invalid sign");
                //}

                //if (string.IsNullOrEmpty(sign))
                //{
                //    return MakeSysError("sign is IsNullOrEmpty ");
                //}

                ILogger.Instance.Send("Process: req = " + req);

                NameValueCollection dic = HttpUtility.ParseQueryString(req);
                string action = dic["action"];
                Sos.Trace("SysExternalApi: Process: action: " + action);
                return Handlers.ContainsKey(action) ? Handlers[action](dic, connect) : MakeSysError("unknown command");
            }
            catch (FormatException ex)
            {
                return MakeError("PARAMS_ERROR", ex.Message);
            }
            catch (Exception exc)
            {
                return MakeSysError(exc.ToString());
            }
        }

        private static int GetInt(NameValueCollection dic, string name)
        {
            string svalue = dic.Get(name);
            if (string.IsNullOrEmpty(svalue))
            {
                throw new FormatException($"{name} is null");
            }

            int res = -1;
            if (!int.TryParse(svalue, out res))
            {
                throw new FormatException($"invalid {name}");
            }

            return res;
        }

        private static string GetString(NameValueCollection dic, string name)
        {
            string svalue = dic.Get(name);
            if (string.IsNullOrEmpty(svalue))
            {
                throw new FormatException($"{name} is null");
            }

            return svalue;
        }

        private static string GetXML(NameValueCollection dic, Connection connect)
        {
            var hash = GetString(dic, "hash");
            
            ILogger.Instance.Send($"SysExternalApi: GetXML: start: hash: {hash}");

            ratingServer.RatingManager.SendXML(hash, connect.id);

            return null;
        }

        private static string GetRating(NameValueCollection dic, Connection connect)
        {
            int loginId = GetInt(dic, "loginId");
            PersonType classId = (PersonType)GetInt(dic, "classId");
            RatingType ratingId = (RatingType)GetInt(dic, "ratingId");
            RatingMode modeId = (RatingMode)GetInt(dic, "modeId");

            ILogger.Instance.Send($"SysExternalApi: GetRating: start: loginId: {loginId}, classId: {classId}, ratingId: {ratingId}, modeId: {modeId}");

            //LoginRatingData lrr = new LoginRatingData(loginId, ratingId, classId, modeId == 0);
            ratingServer.RatingManager.AddRequest(new RatingManagerRequest((loginStorage) =>
            ratingServer.RatingManager.WebLoginRatingRequest(loginId, ratingId, classId, modeId, connect.id) , connect.id));
            
            return null;
        }

        private static string GetStatistic(NameValueCollection dic, Connection connect)
        {
            int loginId = GetInt(dic, "loginId");

            ILogger.Instance.Send($"SysExternalApi: GetRating: start: loginId: {loginId}");
            ratingServer.RatingManager.AddRequest(new RatingManagerRequest((loginStorage) => loginStorage.WebRequestStatistic(loginId,  connect.id), connect.id));

            return null;
        }


        private static string GetBattleStatistic(NameValueCollection dic, Connection connect)
        {
            int loginId = GetInt(dic, "loginId");
            int personId = GetInt(dic, "personId");
            int ratingId = GetInt(dic, "ratingId");
            int fieldId = GetInt(dic, "fieldId");
            //string name = dic.Get("name");

            Sos.Trace("SysExternalApi: GetRating: start: ",
                string.Format("sloginId: {0}, sratingId: {1}, sfieldId: {2},", loginId, ratingId, fieldId));

            ratingServer.RatingManager.AddRequest(new RatingManagerRequest((loginStorage) => loginStorage.WebRequest(loginId, personId, fieldId, null, connect.id), connect.id));

            return null;

        }

        private static string GetGuildRating(NameValueCollection dic, Connection connect)
        {
            int guildId = GetInt(dic, "guildId");

            LoginGuildRatingData lgrr = new LoginGuildRatingData(guildId);
            throw new NotSupportedException("гильдии будем переделывать");
            //ratingServer.RatingManager.AddRequest(new RatingManagerRequest(RatingManagerRequestType.WebGuildRatingRequest, lgrr, connect.id));

            return null;
        }

        //private static string getRealRequest(string req)
        //{
        //    //byte[] x = Convert.FromBase64String(req);
        //    return req;
        //}

        private static string Md5(string s)
        {
            StringBuilder ret = null;
            try
            {
                ret = ObjectPoolSB.GetObject();
                MD5 md5Prov = new MD5CryptoServiceProvider();
                byte[] b = md5Prov.ComputeHash(Encoding.GetBytes(s));

                for (int i = 0; i < 16; i++)
                {
                    ret.Append(ArrayLib.hexArray[b[i]]); //(String.Format("{0:x02}", b[i]);
                }
                return ret.ToString();
            }
            finally
            {
                ObjectPoolSB.Back(ret);
            }
        }

        public static string MakeServerError(string err, string info)
        {
            Hashtable ht = new Hashtable();
            ht.Add("result", "error");
            ht.Add("error", err);
            if (!string.IsNullOrEmpty(info))
                ht.Add("info", info);
            return JSON.JsonEncode(ht);
        }

        public static string MakeSysError(string info)
        {
            Hashtable ht = new Hashtable();
            ht.Add("result", "error");
            ht.Add("error", "SYS_ERROR");
            ht.Add("info", info);
            return JSON.JsonEncode(ht);
            //return string.Format("result=ERROR;error=SYS_ERROR;info={0};", err);
        }

        public static string MakeError(string err, string info)
        {
            Hashtable ht = new Hashtable();
            ht.Add("result", "error");
            ht.Add("error", err);
            ht.Add("info", info);
            return JSON.JsonEncode(ht);
            //return string.Format("result=ERROR;error={0};info={1};", err, info);
        }

        private static bool CheckSign(string sign, string req)
        {
            //return true;
            Sos.Debug(string.Format("CheckSign: req: {0}, SystemConfig.ExternalApiKey: {1}, sign: {2}", req, SystemConfig.ExternalApiKey, sign));
            var needSing = Md5(req + SystemConfig.ExternalApiKey);

            return sign == needSing;
        }

        private static string dateformat = "yyyy.MM.dd HH:mm:ss";

        public static string MakeOnlineResult(int loginsLobby, int loginsLocation)
        {
            Hashtable ht = new Hashtable();
            ht.Add("result", "success");
            ht.Add("loginsLobby", loginsLobby);
            ht.Add("loginsLocation", loginsLocation);
            string result = JSON.JsonEncode(ht);

            ILogger.Instance.Send("MakeOnlineResult = " + result);

            return result;
        }

        public static string MakeHashResult(string hash)
        {
            Hashtable ht = new Hashtable();
            ht.Add("result", "success");
            ht.Add("actionHash", hash);
            string result = JSON.JsonEncode(ht);

            ILogger.Instance.Send("MakeHashResult = " + result);

            return result;
        }

        internal static string MakeGetGuildRatingResult(Hashtable ht)
        {
            string result = JSON.JsonEncode(ht);
#if DEBUG
            ILogger.Instance.Send("MakeGetGuildRatingResult = " + result);
#endif
            return result;
        }

        internal static string MakeGetRatingResult(Hashtable ht)
        {
            string result = JSON.JsonEncode(ht);
#if DEBUG
            ILogger.Instance.Send("MakeGetRatingResult = " + result);
#endif
            return result;
        }
    }
}
