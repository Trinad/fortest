﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Assets.Scripts.Utils.Logger;
using Newtonsoft.Json;
using Node.Rating.Logic.RatingInstances;
using Node.Rating.Logic.RatingInstances.GameStatistic;
using Node.Rating.Logic.Records;
using UtilsLib;
using UtilsLib.corelite;
using UtilsLib.DegSerializers;

namespace Node.Rating.Db
{
    public class MSSQLAdapter : CoreLite.Adapters.MsSqlAdapter
    {
        internal MSSQLAdapter(string sConnect)
            : base(sConnect)
        {

        }

        internal void LoadGuildRating(string ratingType, RatingGuildInstance instance)
        {
            string sql = ratingType + "GuildRatingLoad";
            ILogger.Instance.Send( $"{sql}");

            using (SqlConnection connection = GetOpenConnection())
            {
                try
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = sql;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            /*считывание данных из таблицы weekGuilRating*/
                            while (reader.Read())
                            {
                                OneGuildRatingRecord rec = new OneGuildRatingRecord(true);
                                rec.Fill(new DBRecordCollection(reader));
                                instance.AddRecord(rec);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ILogger.Instance.Send($"MSSQLAdapter.{sql} {ex}", ErrorLevel.error);
                    throw;
                }
            }
            instance.LoadFromDbComplite();
        }

        internal void LoadAllLoginRating(LoginStorage instance)
        {
            string sql = "LoginRatingLoad";
            ILogger.Instance.Send( $"{sql}");

            using (SqlConnection connection = GetOpenConnection())
            {
                try
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = sql;
                        ILogger.Instance.Send(string.Format("LoadRating; sql: " + sql));
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            /*считывание данных из таблицы weekRating*/
                            while (reader.Read())
                            {
                                var keyValueRader = new DBRecordCollection(reader);
                                int loginId = keyValueRader.GetInt("LoginId");
                                string jsonData = keyValueRader.GetString("Data");

                                //Sos.Debug($"LoginRatingLoad LoginId: {loginId} Data: {jsonData}");                        
                                    
                                LoginGameStatistic loginGameStatistic = JSON.JsonDecode(jsonData).JsonDecode<LoginGameStatistic>();
                                loginGameStatistic.ValidateBattleStat();
                                loginGameStatistic.AddRecord(loginGameStatistic.LoginId);
                                instance.AddLoginGameStatistic(loginGameStatistic);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ILogger.Instance.Send($"MSSQLAdapter.{sql} {ex}", ErrorLevel.error);
                    throw;
                }
            }

            GC.Collect();
            //instance.LoadFromDbComplite();
        }

        internal LoginGameStatistic LoadLoginRating(long loginId)
        {
            string sql = $"select [Data] from [LoginRating]  WHERE LoginId = {loginId}";
            ILogger.Instance.Send( $"{sql}");

            LoginGameStatistic loginGameStatistic = null;
            using (SqlConnection connection = GetOpenConnection())
            {
                try
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = sql;
                        ILogger.Instance.Send(string.Format("LoadLoginRating; sql: " + sql));
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var keyValueRader = new DBRecordCollection(reader);
                                string jsonData = keyValueRader.GetString("Data");

                                loginGameStatistic = JSON.JsonDecode(jsonData).JsonDecode<LoginGameStatistic>();
                                loginGameStatistic.ValidateBattleStat();
                                loginGameStatistic.AddRecord(loginGameStatistic.LoginId);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ILogger.Instance.Send($"MSSQLAdapter.{sql} {ex}", ErrorLevel.error);
                    throw;
                }
            }

            return loginGameStatistic;
        }

        internal void LoadReward(string ratingType, RewardInstance instance)
        {
            string sql = ratingType + "RewardLoad";
            ILogger.Instance.Send( $"{sql}");

            using (SqlConnection connection = GetOpenConnection())
            {
                try
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = sql;
                        Sos.Debug(string.Format("LoadReward; sql: " + sql));
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            /*считывание данных из таблицы weekReward*/
                            while (reader.Read())
                            {
                                OneRewardRecord rec = new OneRewardRecord(true);
                                rec.Fill(new DBRecordCollection(reader));
                                instance.AddRecord(rec);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ILogger.Instance.Send($"MSSQLAdapter.{sql} {ex}", ErrorLevel.error);
                    throw;
                }
            }

            instance.LoadFromDbComplite();
        }

        internal void SaveRewardRecord(string actionType, string ratingType, OneRewardRecord record)
        {
            string sql = ratingType + "Reward" + actionType;
            //ILogger.Instance.Send(MType.QueryExecution, SType.Info, $"{sql} {record}");

			using (var connection = GetOpenConnection())
            {
                using (var Command = connection.CreateCommand())
                {
                    Command.CommandText = sql;
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = CiCommandTimeout;

                    Command.AddParameter("@Id", record.Key());
                    Command.AddParameter("@LoginId", record.loginId);
                    Command.AddParameter("@RewardId", record.Reward.RewardId);
                    Command.AddParameter("@RemoveDate", record.Reward.RemoveDateTime);

                    try
                    {
                        Command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        ILogger.Instance.Send($"MSSQLAdapter.{sql} {ex}", ErrorLevel.error);
                        throw;
                    }
                }
            }
        }

        internal void SaveRecord<T>(DBrecord<T> record)
        {
            string sql = "LoginRatingSave";
            //ILogger.Instance.Send(MType.QueryExecution, SType.Info, $"{sql} {record}");

            using (var connection = GetOpenConnection())
            {
                using (var Command = connection.CreateCommand())
                {
                    Command.CommandText = sql;
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = CiCommandTimeout;

                    Command.AddParameter("@LoginId", record.Key);
                    string dataStr = JSON.JsonEncode(record.Data.JsonEncode());
                    Command.AddParameter("@Data", dataStr);
                    ILogger.Instance.Send($"{sql} LoginId {record.Key}");
                    try
                    {
                        Command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        ILogger.Instance.Send($"MSSQLAdapter.{sql} {ex}", ErrorLevel.error);
                        throw;
                    }
                }
            }
        }

		internal void RemoveRating<T>(DBrecord<T> record)
		{
			string sql = "RemoveRating";
			//ILogger.Instance.Send(MType.QueryExecution, SType.Info, $"{sql} {record}");

			using (var connection = GetOpenConnection())
			{
				using (var Command = connection.CreateCommand())
				{
					Command.CommandText = sql;
					Command.CommandType = CommandType.StoredProcedure;
					Command.CommandTimeout = CiCommandTimeout;

					Command.AddParameter("@LoginId", record.Key);
					try
					{
						Command.ExecuteNonQuery();
					}
					catch (Exception ex)
					{
						ILogger.Instance.Send($"MSSQLAdapter.{sql} {ex}", ErrorLevel.error);
						throw;
					}
				}
			}
		}

		internal void TruncateTable(string ratingType)
        {
            string sql = "Truncate table " + ratingType + "Rating";
            ILogger.Instance.Send( $"{sql}");

            try
            {
                using (var connection = GetOpenConnection())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = sql;
                        command.CommandType = CommandType.Text;
                        //SqlParameter par = new SqlParameter("@list", str);
                        //par.DbType = DbType.String;

                        //command.AddParameter("@list", str);
                        //command.Parameters.Add(par);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"MSSQLAdapter.{sql} {ex}", ErrorLevel.error);
                throw;
            }
        }

        internal void SaveGuildRatingRecord(string actionType, string ratingType, OneGuildRatingRecord record)
        {
            string sql = ratingType + "Rating" + actionType;
            //ILogger.Instance.Send(MType.QueryExecution, SType.Info, $"{sql} {record}");

            using (var connection = GetOpenConnection())
            {
                using (var Command = connection.CreateCommand())
                {
                    Command.CommandText = sql;
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = CiCommandTimeout;

                    Command.AddParameter("@Id", record.Data.ClanId);

                    Command.AddParameter("@GuildId", record.Data.ClanId);
                    Command.AddParameter("@GuildName", record.Data.ClanName);
                    Command.AddParameter("@GuildType", record.Data.ClanType);

	                Command.AddParameter("@GuildSymbolId", 0); //record.Data.GuildSymbolId); //TODO_Deg Clan
	                Command.AddParameter("@GuildSymbolColor", ""); //record.Data.GuildSymbolColor );
	                Command.AddParameter("@GuildBgColor", ""); //record.Data.GuildBgColor);

                    Command.AddParameter("@GuildRank", record.guildRank);
                    Command.AddParameter("@KillCount", record.killCounter);
                    Command.AddParameter("@Points", 0);//record.pvpPoints);
                    Command.AddParameter("@DeathCount", record.deathCounter);
                    Command.AddParameter("@Wins", record.wins);
                    Command.AddParameter("@Looses", record.looses);
                    Command.AddParameter("@Draw", record.draw);
                    Command.AddParameter("@GameCount", record.games);

                    try
                    {
                        Command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        ILogger.Instance.Send($"MSSQLAdapter.{sql} {ex}", ErrorLevel.error);
                        throw;
                    }
                }
            }
        }

        internal void RemoveGuildRecord(string ratingType, OneGuildRatingRecord record)
        {
            string sql = ratingType + "RatingDelete";
            //ILogger.Instance.Send(MType.QueryExecution, SType.Info, $"{sql} {record}");

            using (var connection = GetOpenConnection())
            {
                using (var Command = connection.CreateCommand())
                {
                    Command.CommandText = sql;
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = CiCommandTimeout;

                    Command.AddParameter("@Id", record.Data.ClanId);
                    try
                    {
                        Command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        ILogger.Instance.Send($"MSSQLAdapter.{sql} {ex}", ErrorLevel.error);
                        throw;
                    }
                }
            }
        }

        internal object ExecStoredProc(string name, Dictionary<string, object> param, Func<SqlDataReader, object> onSuccess)
        {
            object res = null;
            string sql = name;
            ILogger.Instance.Send( sql);
            try
            {
                using (SqlConnection connection = GetOpenConnection())
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = sql;

                        if (param != null)
                            foreach (var pair in param)
                                command.AddParameter(pair.Key, pair.Value);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            res = onSuccess(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"MSSQLAdapter.{sql} {ex}", ErrorLevel.error);
                throw;
            }
            return res;
        }

        internal void ExecStoredProc(string name, Dictionary<string, object> param)
        {
            string sql = name;
            ILogger.Instance.Send( sql);
            try
            {
                using (SqlConnection connection = GetOpenConnection())
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = sql;

                        if (param != null)
                            foreach (var pair in param)
                                command.AddParameter(pair.Key, pair.Value);

                        int rows = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send($"MSSQLAdapter.{sql} {ex}", ErrorLevel.error);
                throw;
            }
        }
    }
}
