﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
using Node.Rating.Logic.Records;
using Node.Rating.Utils;
using UtilsLib;
using UtilsLib.db;
using System.Collections.Concurrent;
using Assets.Scripts.Utils.Logger;
using Node.Rating.Logic.RatingInstances.GameStatistic;

namespace Node.Rating.Db
{
    /// <summary>
    /// Менеджер, сохраняющий в базу сущности
    /// </summary>
	public class DBStoreManager : DBStoreManagerBase
    {
        public DBStoreManager()
            : base(1000)
        {
            
        }

        private static readonly Dictionary<string, MSSQLAdapter> m_htAdapters = new Dictionary<string, MSSQLAdapter>(); //адаптеры -- по одному на один поток
        public static MSSQLAdapter GetAdapter()
        {
            int iThreadHash = Thread.CurrentThread.GetHashCode();
            MSSQLAdapter res = null;

            if (m_htAdapters != null)
            {
                string key = iThreadHash.ToString(CultureInfo.InvariantCulture);

                if (!m_htAdapters.TryGetValue(key, out res))
                {
                    res = new MSSQLAdapter(SystemConfig.sConnectString);
                    //res.OnLooseConnection += res_OnLooseConnection;
                    //res.OnRestoreConnection += res_OnRestoreConnection;
                    //res.OnConnectSuccess += res_OnConnectSuccess;
                    m_htAdapters[key] = res;
                }
            }
            return res;
        }

        public override void OnTimer()
        {
            try
            {
                base.OnTimer();
                ClearRewardDatas();
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("DBStoreManager: OnTimer: " + ex.Message, ErrorLevel.exception);
            }
        }

        bool stopping = false;

        public override void Stop()
        {
            base.Stop();
            try
            {
                Sos.Debug("RatingServer stopping = true;");
                stopping = true;
                SaveData();
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("DBStoreManager: Stop: " + ex.Message, ErrorLevel.exception);
            }
        }

        public ConcurrentQueue<DBrecord<LoginGameStatistic>> LoginGameStatisticsForSave = new ConcurrentQueue<DBrecord<LoginGameStatistic>>();
        private List<DBrecord<LoginGameStatistic>> loginGameStatisticListForSave = new List<DBrecord<LoginGameStatistic>>();

        public ConcurrentQueue<OneRewardRecord> weekRewardsForSave = new ConcurrentQueue<OneRewardRecord>();
        private List<OneRewardRecord> weekRewardListForSave = new List<OneRewardRecord>();


        public ConcurrentQueue<OneRatingRecord> dayRatingsForSave = new ConcurrentQueue<OneRatingRecord>();
        private List<OneRatingRecord> dayRatingListForSave = new List<OneRatingRecord>();
        public ConcurrentQueue<OneRatingRecord> tournamentRatingsForSave = new ConcurrentQueue<OneRatingRecord>();
        private List<OneRatingRecord> tournamentRatingListForSave = new List<OneRatingRecord>();
        public void AddLoginGameStatisticForSave(DBrecord<LoginGameStatistic> record)
        {
            //var record = oneRecord as OneRatingRecord;
            if (record == null)
            {
                ILogger.Instance.Send("DBStoreManager: AddWeekRatingForSave record == null", ErrorLevel.error);
                return;
            }
            LoginGameStatisticsForSave.Enqueue(record);
        }

		public void RemoveRecordFromDb(DBrecord<LoginGameStatistic> record)
		{
			var adapter = RatingServer.GetAdapter();
			adapter.RemoveRating(record);
		}

		public void AddWeekRewardForSave(OneRecord oneRecord)
        {
            var reward = oneRecord as OneRewardRecord;
            
            if (reward == null)
            {
                ILogger.Instance.Send("DBStoreManager: AddWeekRewardForSave reward == null", ErrorLevel.error);
                return;
            }
            weekRewardsForSave.Enqueue(reward);
        }
        //public void AddDayRatingForSave(OneRecord oneRecord)
        //{
        //    var record = oneRecord as OneRatingRecord;
        //    if (record == null)
        //    {
        //        ILogger.Instance.Send("DBStoreManager: AddDayRatingForSave record == null", ErrorLevel.error);
        //        return;
        //    }
        //    dayRatingsForSave.Enqueue(record);
        //}
        //public void AddTournamentRatingForSave(OneRecord oneRecord)
        //{
        //    var record = oneRecord as OneRatingRecord;
        //    if (record == null)
        //    {
        //        ILogger.Instance.Send("DBStoreManager: AddTournamentRatingForSave record == null", ErrorLevel.error);
        //        return;
        //    }
        //    tournamentRatingsForSave.Enqueue(record);
        //}

        public ConcurrentQueue<OneGuildRatingRecord> weekGuildRatingsForSave = new ConcurrentQueue<OneGuildRatingRecord>();
        private List<OneGuildRatingRecord> weekGuildRatingListForSave = new List<OneGuildRatingRecord>();

        public ConcurrentQueue<OneGuildRatingRecord> dayGuildRatingsForSave = new ConcurrentQueue<OneGuildRatingRecord>();
        private List<OneGuildRatingRecord> dayGuildRatingListForSave = new List<OneGuildRatingRecord>();
        public ConcurrentQueue<OneGuildRatingRecord> tournamentGuildRatingsForSave = new ConcurrentQueue<OneGuildRatingRecord>();
        private List<OneGuildRatingRecord> tournamentGuildRatingListForSave = new List<OneGuildRatingRecord>();
        public void AddWeekGuildRatingForSave(OneRecord oneRecord)
        {
            var record = oneRecord as OneGuildRatingRecord;
            if (record == null)
            {
                ILogger.Instance.Send("DBStoreManager: AddWeekGuildRatingForSave record == null", ErrorLevel.error);
                return;
            }
            weekGuildRatingsForSave.Enqueue(record);
        }
        public void AddDayGuildRatingForSave(OneRecord oneRecord)
        {
            var record = oneRecord as OneGuildRatingRecord;
            if (record == null)
            {
                ILogger.Instance.Send("DBStoreManager: AddDayGuildRatingForSave record == null", ErrorLevel.error);
                return;
            }
            dayGuildRatingsForSave.Enqueue(record);
        }
        public void AddTournamentGuildRatingForSave(OneRecord oneRecord)
        {
            var record = oneRecord as OneGuildRatingRecord;
            if (record == null)
            {
                ILogger.Instance.Send("DBStoreManager: AddTournamentGuildRatingForSave record == null", ErrorLevel.error);
                return;
            }
            tournamentGuildRatingsForSave.Enqueue(record);
        }

        private void ProcessSaveReward(ConcurrentQueue<OneRewardRecord> rewardsForSave, List<OneRewardRecord> rewardListForSave, string name)
        {
            var adapter = RatingServer.GetAdapter();
            OneRewardRecord record;
            while (rewardsForSave.TryDequeue(out record))
            {
                if (record.RemoveFromList)
                {
                    
                }
                else
                {
                    rewardListForSave.Add(record);
                }
            }

            int recordsCount = rewardListForSave.Count;
            //StringBuilder stringBuilder = null;
            //if (recordsCount > 0)
                //stringBuilder = new StringBuilder("\n");
            while (rewardListForSave.Count > 0)
            {
                record = rewardListForSave[0];
                if (record.needSave || stopping)
                {
                    record.InSaveQueue = false;
                    rewardListForSave.RemoveAt(0);

                    if (record.RemoveInDB)
                        continue;

                    try
                    {
                        //string process;
                        if (record.bGetFromDB)
                        {
                            //process = " Commit: ";
                            record.bChange = false;
                            adapter.SaveRewardRecord("Commit", name, record);
                        }
                        else
                        {
                            //process = " Create: ";
                            record.bChange = false;
                            adapter.SaveRewardRecord("Create", name, record);
                            record.bGetFromDB = true;
                        }
                        //if (stringBuilder != null)
                            //stringBuilder.Append(process + string.Format("name: {0}, recordId: {1}, RewardId: {2}\n", name, record.Key(), record.Reward.RewardId));
                    }
                    catch (Exception ex)
                    {
                        ILogger.Instance.Send("ProcessSaveReward: " + ex.Message, ErrorLevel.exception);
                    }
                }
                else
                {
                    return;
                }
            }
            //if (stringBuilder != null)
            ILogger.Instance.Send("ProcessSaveReward: count: " + recordsCount, ErrorLevel.trace);
        }

        private void ProcessSaveRating<T>(ConcurrentQueue<DBrecord<T>> ratingsForSave, List<DBrecord<T>> ratingListForSave)
        {
            var adapter = RatingServer.GetAdapter();
            DBrecord<T> record;
            while (ratingsForSave.TryDequeue(out record))
            {
                //if (record.RemoveFromList)
                //{
                //    ratingListForSave.Clear();
                //    adapter.TruncateTable(name);
                //}
                //else
                {
                    ratingListForSave.Add(record);
                }
            }
            int recordsCount = ratingListForSave.Count;
            //StringBuilder stringBuilder = null;
            //if (recordsCount > 0)
                //stringBuilder = new StringBuilder("\n");
            
            while (ratingListForSave.Count > 0)
            {
                record = ratingListForSave[0];
                ratingListForSave.RemoveAt(0);

                if (record.m_bChange /*|| stopping*/)
                {
                    //record.InSaveQueue = false;
                   
                    //if (record.RemoveInDB)
                    //    continue;

                    try
                    {
                        //string process;
                        //process = " Create: ";
                        record.m_bChange = false;
                        adapter.SaveRecord(record);
                        //if(stringBuilder != null)
                        //  stringBuilder.Append(process + string.Format("name: {0}, loginId: {1}, personId: {2}, personType: {3}, pvpPoints: {4}, gearScore: {5}, place: {6}\n",
                        //    name, record.loginId, record.personId, record.personType, record.pvpPoints, record.gearScore, record.ratingPlace));
                    }
                    catch (Exception ex)
                    {
                        ILogger.Instance.Send("ProcessSaveRating: " + ex, ErrorLevel.exception);
                    }
                }
                else
                {
                    //ILogger.Instance.Send($"ProcessSaveRating: !record.m_bChange && !stopping   LoginId(Key) : {record.Key}");
                }
            }

            if(recordsCount > 0)
                ILogger.Instance.Send("ProcessSaveRating: count: " + recordsCount);
        }

        private void ProcessSaveGuildRating(ConcurrentQueue<OneGuildRatingRecord> RatingsForSave, List<OneGuildRatingRecord> RatingListForSave, string name, string truncate_name)
        {
            var adapter = RatingServer.GetAdapter();
            OneGuildRatingRecord record;

            while (RatingsForSave.TryDequeue(out record))
            {
                if (record.IsResetRecord)
                {
                    RatingListForSave.Clear();
                    adapter.TruncateTable(truncate_name);
                }
                else if (record.forDelete)
                {
                    adapter.RemoveGuildRecord(name, record);
                }
                else
                {
                    RatingListForSave.Add(record);
                }
            }

            int recordsCount = RatingListForSave.Count;
            //StringBuilder stringBuilder = null;
            //if (recordsCount > 0)
               // stringBuilder = new StringBuilder("\n");

            while (RatingListForSave.Count > 0)
            {
                record = RatingListForSave[0];
                if (record.needSave || stopping)
                {
                    record.InSaveQueue = false;
                    RatingListForSave.RemoveAt(0);
                    try
                    {
                        if (record.forDelete)
                            continue;
                        //string process;
                        if (record.bGetFromDB)
                        {
                            //process = " Commit: ";
                            record.bChange = false;
                            adapter.SaveGuildRatingRecord("Commit", name, record);
                            //Sos.Trace("ProcessSaveGuildRating данные сохранены Commit");
                        }
                        else
                        {
                            //process = " Create: ";
                            record.bChange = false;
                            adapter.SaveGuildRatingRecord("Create", name, record);
                            record.bGetFromDB = true;
                            //Sos.Trace("ProcessSaveGuildRating данные сохранены Create");
                        }
                        //if (stringBuilder != null)
                          //  stringBuilder.Append(process + string.Format("name: {0}, id: {1}, guildName: {2}, guildtype: {3}, symbolId: {4}, symbolColor: {5}, points: {6},  place: {7}\n",
                            //    name, record.Data.GuildId, record.Data.GuildName, record.Data.GuildType, record.Data.GuildSymbolId, record.Data.GuildSymbolColor, record.pvpPoints, record.guildPlace));
                    }
                    catch (Exception ex)
                    {
                        ILogger.Instance.Send($"ProcessSaveGuildRating: {ex.ToString()}");
                    }
                }
                else
                {
                    return;
                }
            }
            //if (stringBuilder != null)
            ILogger.Instance.Send("ProcessSaveGuildRating: count: " + recordsCount, ErrorLevel.trace);
        }

        public override void SaveData()
        {
            ProcessSaveRating(LoginGameStatisticsForSave, loginGameStatisticListForSave);
            ProcessSaveReward(weekRewardsForSave, weekRewardListForSave, "week");

            ProcessSaveGuildRating(weekGuildRatingsForSave, weekGuildRatingListForSave, "weekGuild", "globalGuild");
            //ProcessSaveRating(tournamentRatingsForSave, tournamentRatingListForSave, "tournament");
            //ProcessSaveRating(dayRatingsForSave, dayRatingListForSave, "day");


            //ProcessSaveGuildRating(dayGuildRatingsForSave, dayGuildRatingListForSave, "guildDay", "dayGuild");
            //ProcessSaveGuildRating(tournamentGuildRatingsForSave, tournamentGuildRatingListForSave, "guildTournament", "tournamentGuild");

        }

        #region Clear

        bool needRewardReset = false;

        public void NeedRewardResetData()
        {
            needRewardReset = true;
        }
        
        private void ClearRewardDatas()
        {
            if(!needRewardReset)
                return;
            var adapter = RatingServer.GetAdapter();
            adapter.ExecStoredProc("RemoveRewardDateTime", new Dictionary<string, object>() { { "DateTimeNow", DateTime.UtcNow } });
            needRewardReset = false;
        }


        #endregion
    }
}

