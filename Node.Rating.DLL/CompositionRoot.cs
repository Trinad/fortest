﻿using System;
using System.Collections.Specialized;
using Assets.Scripts.Utils.Logger;
using Node.Rating.DLL;
using UtilsLib;

namespace Node.Rating.DLL
{
    public class CompositionRoot
    {
        public static void Init(NameValueCollection appSettings)
        {
            FRRandom.Init();

            var config = new Utils.SystemConfig(appSettings);
            ILogger.Instance = new AsyncLogger(config, new CommonLogger(config));
            ILogger.Instance.Init();
            ILogger.Instance.AddTag("srvType", "Rating");

            ILogger.Instance.Send("Rating StartingServer!");            
        }
    }
}
