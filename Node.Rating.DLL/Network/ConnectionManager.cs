﻿using System;
using Fragoria.Common.Utils.Threads;
using Node.Rating.Protocols;
using UtilsLib;

namespace Node.Rating.Network
{
    public class ConnectionManager : OneThreadTimer
    {
        private readonly RatingServer ratingServer;

        public ConnectionManager(RatingServer ratingServer, int iPeriodMilliseconds)
            : base("ConnectionManager", iPeriodMilliseconds)
        {
            this.ratingServer = ratingServer;
        }

        public override void OnTimer()
        {
            if (ratingServer.bStarted)
                ConnectToDispatcher();
        }

        private void ConnectToDispatcher()
        {
            try
            {
                ratingServer.Dispatcher.DoPing();
                if (SysDispatcher.DsConnection == null)
                {
                    ILogger.Instance.Send("Попытка подключиться к диспетчеру узлов");
                    SysDispatcher.UpdateConnection(ratingServer);
                }
            }
            catch (Exception exception)
            {
                ILogger.Instance.Send($"ConnectionManager.ConnectToDispatcher: {exception}", ErrorLevel.error);
            }

        }

    }
}
