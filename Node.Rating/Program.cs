﻿using Node.Rating.DLL;
using UtilsLib.Logic.ServerStart;

namespace Node.Rating
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			CompositionRoot.Init(System.Configuration.ConfigurationManager.AppSettings);

			var server = new RatingServer();

			StartGameService.Run(server, "PixelRating");
		}
	}
}
