﻿using LobbyInstanceLib;
using Node.Lobby.Assets.Scripts.Lobby.DI;

namespace Node.Lobby
{
	class Program
	{
		static void Main(string[] args)
		{
			var server = new DotnetDIServerStart<PixelLobbyInstaller>();
			server.Run();
		}
	}
}
