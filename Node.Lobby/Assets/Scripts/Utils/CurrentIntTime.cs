﻿using System;
using Fragoria.Common.Utils.Threads;
using UtilsLib.Logic;

namespace Node.Lobby.Assets.Scripts.Utils
{
    internal class CurrentIntTime : ICurrentIntTime
    {
		public int GetCurrentServerTime()
		{
			return (int) Time.time;
		}

		public int GetServerTime(DateTime date)
		{
			throw new NotImplementedException();
		}

		public bool ExceedSeconds(int begin_time, int seconds)
		{
			throw new NotImplementedException();
		}
	}
}
