﻿using LobbyInstanceLib;
using Node.Lobby.Assets.Scripts.Lobby;
using Node.Lobby.Assets.Scripts.Networking;

namespace Node.Lobby.Assets.Scripts
{
	/// <summary>
	/// все менеджеры регистрируются тут, 
	/// чтобы не увеличивать лишний раз ServerStartGameManager и InstanceGameManager
	/// </summary>
	public class DIListManagers : BaseDIListManagers
	{
		public DIListManagers(TransportManager transportManager, uLinkManager uLinkManager, NodeManager nodeManager, LobbyServerStatistics lobbyServerStatManager)
		{
			AddManager(transportManager);
			AddManager(uLinkManager);
			AddManager(nodeManager);
			AddManager(lobbyServerStatManager);
		}
	}
}