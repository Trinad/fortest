﻿using System.Collections.Generic;
using Assets.Scripts.Networking;
using LobbyInstanceLib.Networking;
using Node.Lobby.Assets.Scripts.Lobby;
using Node.Lobby.Assets.Scripts.Lobby.Entries;
using System.Linq;
using uLink;
using UtilsLib.NetworkingData;
using UtilsLib.Protocol;

namespace Node.Lobby.Assets.Scripts.Networking
{
	public sealed class InstanceController : BaseRpcListener, ILobbySysInstance<NetworkP2PMessageInfo, OneInstanceInfo>
	{
		private readonly AppConfig cfg;
		private readonly DIListManagers listManagers;
		private readonly SysDispatcher sysDispatcher;
		private readonly InstanceCollection instanceCollection;
		private readonly ServerDataCollections serverDataCollections;

		public InstanceController(AppConfig _cfg, DIListManagers _listManagers, SysDispatcher _sysDispatcher,
			InstanceCollection _instanceCollection, ServerDataCollections _serverDataCollections,
			NetworkP2P networkP2P, uLink.Network network, INetworkView view)
			: base(networkP2P, network, view)
		{
			cfg = _cfg;
			listManagers = _listManagers;
			sysDispatcher = _sysDispatcher;
			instanceCollection = _instanceCollection;
			serverDataCollections = _serverDataCollections;

			sysDispatcher.instanceController = this;
			serverDataCollections.SendXmlToAllInstances = SendXmlToAllInstances;
		}

		public void uLink_OnPreBufferedRPCs(NetworkBufferedRPC[] bufferedArray)
		{
			foreach (NetworkBufferedRPC rpc in bufferedArray)
			{
				rpc.DontExecuteOnConnected();

				ILogger.Instance.Send("uLink_OnPreBufferedRPCs rpc (name = " + rpc.rpcName + ") on Lobby from " + rpc.sender.id, ErrorLevel.error);
			}
		}

		public void uLink_OnPeerConnected(NetworkPeer peer)
		{
			var connectors = listManagers.GetManagers<uLinkP2PConnector>();
			foreach (var uLinkP2PConnector in connectors)
			{
				//это пришел коннект от потерявшегося инстанса или от текущего лобби, ждем от потерявшегося инстанса команду PeerAuth
				//uLinkP2PConnector.port == peer.port - это локальный коннект лобби на само себя
				//uLinkP2PConnector.port == peer.port - AppConfig.Instance.ShiftPort - а это коннект уже существующего инстанса на наш коннект, и отвечать на этот коннект нам ненадо, 
				if (uLinkP2PConnector.host == peer.ipAddress &&
					(uLinkP2PConnector.port == peer.port ||
					uLinkP2PConnector.port == peer.port - cfg.ShiftPort))
				{
					ILogger.Instance.Send("uLink_OnPeerConnected skip connect from peer.ipAddress = " + peer.ipAddress + "peer.port = " + peer.port, ErrorLevel.warning);
					return;
				}
			}

			SendRpc("TestPeerConnectRPC", peer, "qwerty");// 1 - test string
		}

		public void uLink_OnPeerDisconnected(NetworkPeer peer)
		{
			ILogger.Instance.Send("uLink_OnPeerDisconnected " + peer.ipAddress + "peer.port = " + peer.port);

			var instInfo = instanceCollection.GetInstanceByNetworkPeer(peer);
			if (instInfo != null)
			{
				instInfo.ServerRpcListner.RemoveRPCByMethod();

				sysDispatcher.InstanceClear(instInfo.ticket);
			}
		}

		// Авторизация инстанса
		[RPC]
		public void PeerAuth(string ticket, string levelName, string jsonName, int port, bool NewInstance, NetworkP2PMessageInfo info)
		{
			ILogger.Instance.Send($"PeerAuth ticket = {ticket} levelName = {levelName} port = {port} NewInstance = {NewInstance} info.sender.ipAddress = {info.sender.ipAddress} info.sender.port = {info.sender.port}");

			string instIpAddr = string.IsNullOrEmpty(cfg.InstanceHostName) ? info.sender.ipAddress : cfg.InstanceHostName;
			//shiftport - смещение порта для обмена данными инстанса с лобби, ShiftPortForAttachInstance смещение для обмена данными инстанса с новым лобби
			int instPort = info.sender.port - (NewInstance ? cfg.ShiftPort : cfg.ShiftPortForAttachInstance);

			var instInfo = instanceCollection.GetInstanceByTicket(ticket);

			if (!NewInstance || instInfo == null)
			{
				sysDispatcher.SendCommandOnRegisterExistingInstance(ticket, levelName);
				instInfo = instanceCollection.AddInstanceInfo(ticket, levelName);
			}

			if (instInfo == null)
				return;

			instInfo.ServerRpcListner.FillRPCByMethod((view, rpc, param) => SendRpc(rpc.sSysCommand, info.sender, param)); //Отправляем сообщение напрямую

			instInfo.SetPeer(info.sender);
			instInfo.SetInstance(instIpAddr, instPort);
			instInfo.JsonName = jsonName;

			if (instInfo.Ready())
				sysDispatcher.SendStartInstanceSuccess(instInfo, NewInstance);

			if (NewInstance)
			{
				SendRpc(instInfo.ServerRpcListner.RemoteXml, serverDataCollections.Xmls);
				SendRpc(instInfo.ServerRpcListner.PaymentXml, serverDataCollections.PricePoints);
			}
			else
			{
				var connectors = listManagers.GetManagers<uLinkP2PConnector>().ToList();
				foreach (var listener in connectors)
				{
					if (listener.host == instIpAddr && listener.port == instPort - cfg.ShiftPort + cfg.ShiftPortForAttachInstance)//вычитаем сначала смещение который было при самом старте инстанса, а потом добавляем наше
					{
						listener.Dispose();
						listManagers.RemoveManager(listener);
						break;
					}
				}
			}
		}

		public void AddExistsInstance(string instanceHost, int instancePort, string passwordP2P, float interval)
		{
			int instanceConnectPort = instancePort + cfg.ShiftPortForAttachInstance;

			var connectors = listManagers.GetManagers<uLinkP2PConnector>();
			uLinkP2PConnector connector = null;
			foreach (var existConnector in connectors)
			{
				var exCon = existConnector;
				if (exCon.host == instanceHost && exCon.port == instanceConnectPort)
				{
					connector = exCon;
					break;
				}
			}
			if (connector == null)
			{
				var p2pConn = new uLinkP2PConnector(m_networkP2P);

				p2pConn.host = instanceHost;
				p2pConn.port = instanceConnectPort;

				p2pConn.incomingPassword = passwordP2P;
				p2pConn.interval = interval;
				p2pConn.connectingTimeout = 1;
				p2pConn.tryConnectCountMax = 30;

				ILogger.Instance.Send($"AddExistsInstance p2pConn host={p2pConn.host} port={p2pConn.port}",
					$"incomingPassword={p2pConn.incomingPassword} interval={p2pConn.interval} connectingTimeout={p2pConn.connectingTimeout}", ErrorLevel.debug);

				listManagers.AddManager(p2pConn);
			}
		}

		public void SendTranslate(string ticket, TranslateData trData)
		{
			var inst = instanceCollection.GetInstanceByTicket(ticket);
			if (inst == null || inst.peer.Equals(NetworkPeer.unassigned))
				return;

			SendRpc(inst.ServerRpcListner.Translate, trData);
		}

		[RPC]
		public void Translate(TranslateData trData, OneInstanceInfo info)
		{
			sysDispatcher.SendTranslate(trData);
		}

		[RPC]
		public void PingLoginFromInstance(int loginId, int personId, OneInstanceInfo instInfo)
		{
			sysDispatcher.LoginPing(loginId, personId, instInfo.ticket);
		}

		public void SendXmlToAllInstances(Dictionary<string, string> xmls)
		{
			foreach (var instance in instanceCollection.GetAllInstances())
				SendRpc(instance.ServerRpcListner.RemoteXml, xmls);
		}

		public void SendPaymentXmlToAllInstances()
		{
			foreach (var instance in instanceCollection.GetAllInstances())
				SendRpc(instance.ServerRpcListner.PaymentXml, serverDataCollections.PricePoints);
		}
	}
}
