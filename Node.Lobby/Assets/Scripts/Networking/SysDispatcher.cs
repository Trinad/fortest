﻿using System.Collections.Generic;
using System.Threading;
using Fragoria.Common.Utils;
using Node.Lobby.Assets.Scripts.Lobby;
using Node.Lobby.Assets.Scripts.Lobby.Entries;
using NSubstitute;
using UtilsLib.low_engine_sharp.transport;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using UtilsLib.Protocol;

namespace Node.Lobby.Assets.Scripts.Networking
{
	/// <summary>
	/// отправляет данные на диспетчер SysDispatcher сервер код AS
	/// </summary>
	public class SysDispatcher : BaseCmdSystem, ILobbySysDispatcher
	{
		public Connection DsConnection;

		public static string SysCode = "AS";

		private readonly AppConfig cfg;
		private readonly InstanceCollection instanceCollection;
		private readonly NodeManager nodeManager;
		private readonly ServerDataCollections serverDataCollections;

		public InstanceController instanceController;

		public SysDispatcher(AppConfig _cfg, InstanceCollection _instanceCollection, NodeManager _nodeManager,
			ServerDataCollections _serverDataCollections) : base(SysCode)
		{
			cfg = _cfg;
			instanceCollection = _instanceCollection;
			nodeManager = _nodeManager;
			serverDataCollections = _serverDataCollections;

			nodeManager.sysDispatcher = this;
		}

		public override void MakeCommandsTable()
		{
			FillCommand<ILobbySysDispatcher>();
        }

		public void ReconnectToDispatcher()
		{
			DsConnection = ConnectToDispatcher();
			if (DsConnection != null)
			{
				OnConnectedToDispatcher();
			}
		}

		protected static IDispatcherSysLobby dispatcher = Substitute.For<IDispatcherSysLobby>();
		protected static IDispatcherSysTranslate translate = Substitute.For<IDispatcherSysTranslate>();

		private readonly AutoResetEvent connectEvent = new AutoResetEvent(false);
		private string DispatcherTicket = string.Empty;
		Connection tempConnection;

		public Connection ConnectToDispatcher()
		{
			if (tempConnection != null)
			{
				transport.Disconnect(tempConnection, DisconnectReason.UserRequest);
				tempConnection = null;
			}

			string ip = cfg.dispatcher.ip;
			int port = cfg.dispatcher.port;
			tempConnection = Connection.Connect(ip, port.ToString());
			if (tempConnection == null)
				return null;

			DispatcherTicket = string.Empty;

			tempConnection.dos_border = -1;
			tempConnection.DestinationType = DestinationType.Server;
			tempConnection.SetCanBufferSendOperation(false);

			FillRPCByMethod(dispatcher, tempConnection);
			FillRPCByMethod(translate, tempConnection);

			//отправить туда запрос на тикет. получить тикет.
			//отправить запрос на авторизацию. получить авторизацию.
			GetTicket();
			if (!connectEvent.WaitOne(cfg.iMillisecondsRecieveTimeout, false))
			{
				ILogger.Instance.Send("Dispatcher don't respond for GET TICKET command, ip = " +
									  ip + " port = " + port, ErrorLevel.error);
				return null;
			}

			//отсылаем имя и закрытый ключ сервер
			string encodedTicket = ByteArray.Encode(cfg.dispatcher.key, DispatcherTicket);
			GetAuth(encodedTicket, NodeType.Lobby, $" ip = {cfg.lobby_udp_info.ip}, port = {cfg.lobby_udp_info.port}");
			if (!connectEvent.WaitOne(cfg.iMillisecondsRecieveTimeout, false))
			{
				ILogger.Instance.Send("Dispatcher don't respond for GET AUTH command, ip = " + ip + " port = " + port,
					ErrorLevel.error);
				return null;
			}

			ILogger.Instance.Send("Connected to dispatcher", ErrorLevel.info);

			return tempConnection;
		}

		private void OnConnectedToDispatcher()
		{
			// если лобби реконнектится к диспетчеру после его падения, например,
			// диспетчер должен узнать об инстансах этого лобби
			var instances = instanceCollection.GetAllInstances();
			foreach (var instance in instances)
			{
				SendCommandOnRegisterExistingInstance(instance.ticket, instance.sLevelName);
				if (instance.Ready())
					SendStartInstanceSuccess(instance, false);
			}

			SendRpc(dispatcher.OnGetNodeParameters, cfg.lobby_udp_info.MaxConnections, cfg.Version);
		}

		#region TranslateToInstance

		private class InstanceWithLogin
		{
			public bool Register;
			public int LoginId;
			public readonly IInstanceSysLobby<Connection> ServerRpcLisner = Substitute.For<IInstanceSysLobby<Connection>>();
			public static readonly CommandRingBuffer<InstanceWithLogin> Buffer = new CommandRingBuffer<InstanceWithLogin>(50);
		}

		//Это одноразовый инстанс, две команды не отправлять!
		private IInstanceSysLobby<Connection> GetInstanceByLogin(int loginId)
		{
			var result = InstanceWithLogin.Buffer.Get();
			result.LoginId = loginId;

			if (!result.Register)
				FillRPCByMethod(result.ServerRpcLisner, (rpc, param) =>
				{
					//отправим на инстанс через диспетчер
					SendRpc(translate.OnTranslate, new TranslateData
					{
						nodeType = NodeType.InstanceServer,
						sSysCommand = rpc.sSysCommand,
						loginId = result.LoginId,
						data = TranslateData.PackData(rpc, param)
					});
					InstanceWithLogin.Buffer.Put(result);
				});

			result.Register = true;
			return result.ServerRpcLisner;
		}

		#endregion

		#region TranslateToLobby

		private class LobbyWithLogin
		{
			public bool Register;
			public int LoginId;
			public readonly ILobbySysDispatcher SysDispatcher = Substitute.For<ILobbySysDispatcher>();
			public static readonly CommandRingBuffer<LobbyWithLogin> Buffer = new CommandRingBuffer<LobbyWithLogin>(50);
		}

		//Это одноразовый инстанс, две команды не отправлять!
		private ILobbySysDispatcher GetLobbyByLogin(int loginId)
		{
			var result = LobbyWithLogin.Buffer.Get();
			result.LoginId = loginId;

			if (!result.Register)
				FillRPCByMethod(result.SysDispatcher, (rpc, param) =>
				{
					//отправим на инстанс через диспетчер
					SendRpc(translate.OnTranslate, new TranslateData
					{
						nodeType = NodeType.Lobby,
						sSysCommand = rpc.sSysCommand,

						loginId = result.LoginId,
						data = TranslateData.PackData(rpc, param)
					});
					LobbyWithLogin.Buffer.Put(result);
				});

			result.Register = true;
			return result.SysDispatcher;
		}

		#endregion

		public void GetTicket()
		{
			SendRpc(dispatcher.OnGetLobbyTicket);
		}

		public void OnGetTicketReceived(string dispatcherTicket, Connection sender)
		{
			if (sender != tempConnection)
			{
				ILogger.Instance.Send(
					$"OnGetTicketReceived() expected conn {tempConnection.ConnectionId} but received from {sender.ConnectionId}",
					ErrorLevel.warning);
				return;
			}

			DispatcherTicket = dispatcherTicket;
			connectEvent.Set();
		}

		private int LobbyId = -1;

        public void GetAuth(string ticket, NodeType nodeType, string lobbyUdpInfo)
		{
			SendRpc(dispatcher.OnGetAuthLobby, ticket, nodeType, LobbyId, lobbyUdpInfo);
		}

		public void OnGetAuthReceived(Errors result, int lobbyId)
		{
			LobbyId = lobbyId;
			connectEvent.Set();
		}

		//пинг диспетчера.
		private void PingCommand(int id)
		{
			SendRpc(dispatcher.OnPing, id);
		}

		public void OnPingCommand(int id)
		{
			PingCommand(id);
		}

		public void OnNodeStop()
		{
			foreach (var instance in instanceCollection.GetAllInstances())
				instanceController.SendRpc(instance.ServerRpcListner.InstanceStopping);
		}

		public void OnStartInstance(string ticket, string levelName)
		{
			nodeManager.StartInstance(ticket, levelName);
		}

		public void OnStopInstance(string ticket)
		{
			instanceCollection.OnInstanceClear(ticket);
		}

		public void SendStartInstanceSuccess(OneInstanceInfo instInfo, bool firstTime)
		{
			SendRpc(dispatcher.OnStartInstance,
				instInfo.ticket,
				instInfo.IpForClient,
				instInfo.PortForClient,
				instInfo.JsonName,
				firstTime);
		}

		public void OnTranslateInstance(string ticket, TranslateData trData)
		{
			instanceController.SendTranslate(ticket, trData);
		}

		internal void SendTranslate(TranslateData trData)
		{
			if (trData.nodeType == NodeType.Dispatcher)
				SendRpc(DsConnection, trData); //Отправим напрямую
			else
				SendRpc(translate.OnTranslate, trData); //Отправим через транслейт диспетчера
		}

		public void OnXmlChanged(string xml, string name)
		{
			serverDataCollections.OnXmlChangedReceived(xml, name);
		}

		public void OnPriceInfo(List<Prices> prices)
		{
			serverDataCollections.PricePoints = prices;

			instanceController.SendPaymentXmlToAllInstances();
		}

		internal void LoginPing(int loginId, int personId, string instanceTicket)
		{
			SendRpc(dispatcher.OnLoginPing, loginId, personId, instanceTicket);
		}

		internal void SendCommandOnRegisterExistingInstance(string ticket, string levelName)
		{
			SendRpc(dispatcher.OnRegisterExistingInstance, ticket, levelName);
		}

		internal void InstanceClear(string ticket)
		{
			SendRpc(dispatcher.OnInstanceClear, ticket);
		}


		public void OnAddInstance(string instanceHost, int instancePort)
		{
			instanceController.AddExistsInstance(instanceHost, instancePort, cfg.p2pLobbyConnect.pwd, 0.2f);
		}
    }
}