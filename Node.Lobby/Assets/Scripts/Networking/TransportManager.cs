﻿using System;
using System.Threading;
using Fragoria.Common.Utils.Threads;
using Node.Lobby.Assets.Scripts.Utils;
using UtilsLib.low_engine_sharp.transport;

namespace Node.Lobby.Assets.Scripts.Networking
{
	//Инитит и запускает transport
	public sealed class TransportManager : BaseGameManager
	{
		private readonly AppConfig cfg;
		private readonly ConnectToDispatcher connectToDispatcher;

		public TransportManager(AppConfig _cfg, SysDispatcher _sysDispatcher)
		{
			cfg = _cfg;
			connectToDispatcher = new ConnectToDispatcher(_sysDispatcher);
		}

		public override void Start()
		{
			//InitTransport
			BaseCmdSystem.SetPerformanceStatisticsManager(new NullPerformanceStatisticsManager());

			transport.IsTotalLoggingEnabled = cfg.IsTotalTransportLoggingEnabled;
			transport.default_dos_border = cfg.DOSAttackMessageCount;
			transport.ZipPacketMinSize = cfg.ZipPacketMinSize;

			ILogger.Instance.Send("Init transport!");
			transport.Init(10000,
				0,
				"127.0.0.1", "0",
				null, null,
				cfg.GetTransportType(),
				new CurrentIntTime());

			transport.SetSendTimeout(cfg.TransportSendTimeoutMS);

			//RegisterTransportSystem
			connectToDispatcher.Init();

			//StartTransport
			int ret = transport.Start();
			if (ret != 0)
				throw new Exception($"transport.Start() returned {ret}, fatal error");

			connectToDispatcher.Start(ThreadPriority.Normal);
		}

		public override void Update()
		{
			transport.ProcessIncomingCommands();
		}

		public override void Dispose()
		{
			connectToDispatcher.Stop();

			transport.Stop();
			ILogger.Instance.Send("Транспорт остановлен ...");
		}
	}
}
