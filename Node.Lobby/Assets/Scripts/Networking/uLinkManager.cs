﻿using System;
using uLink;

namespace Node.Lobby.Assets.Scripts.Networking
{
	public sealed class uLinkManager : BaseGameManager
	{
		private readonly AppConfig cfg;
		private readonly uLink.Network m_network;
		private readonly uLink.NetworkP2P m_networkP2P;

		public uLinkManager(AppConfig _cfg, uLink.Network _network, uLink.NetworkP2P _networkP2P)
		{
			cfg = _cfg;
			m_network = _network;
			m_networkP2P = _networkP2P;

			uLink.NetworkLog.SetLevel(uLink.NetworkLogFlags.All, uLink.NetworkLogLevel.Info);
		}

		public override void Start()
		{
			AddP2PNetworkComponent();
		}

		private void AddP2PNetworkComponent()
		{
			m_networkP2P.peerType = cfg.p2pLobbyConnect.type;
			m_networkP2P.peerName = cfg.p2pLobbyConnect.type;
			m_networkP2P.comment = cfg.p2pLobbyConnect.type;
			m_networkP2P.incomingPassword = cfg.p2pLobbyConnect.pwd;
			m_networkP2P.Open(cfg.p2pLobbyConnect.port, cfg.p2pLobbyConnect.maxConnections);
			if (!m_networkP2P.isListening)
			{
				throw new Exception($"uLink.NetworkP2P.Open() failed to bind to port {cfg.p2pLobbyConnect.port}");
			}

			var result = uLink.Network.Instance.InitializeServer(cfg.lobby_udp_info.MaxConnections, cfg.lobby_udp_info.port, cfg.lobby_udp_info.use_nat);
			if (result != NetworkConnectionError.NoError)
			{
				throw new Exception($"uLink.Network.Instance.InitializeServer() failed with reason {result}");
			}
		}

		public override void Update()
		{
			// read input
			try
			{
				m_network.Update();
				m_networkP2P.Update();
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send(ex.ToString(), ErrorLevel.exception);
			}
		}

		public override void LateUpdate()
		{
			// send rpc's
			try
			{
				m_network.Update();
				m_networkP2P.Update();
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send(ex.ToString(), ErrorLevel.exception);
			}
		}
	}
}
