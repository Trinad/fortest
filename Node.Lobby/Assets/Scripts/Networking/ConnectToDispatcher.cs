﻿using System;
using System.Threading;
using Fragoria.Common.Utils.Threads;
using UtilsLib.low_engine_sharp.transport;

namespace Node.Lobby.Assets.Scripts.Networking
{
	//Создает и поддерживает коннект с диспетчером
	public class ConnectToDispatcher : OneThreadTimer //TODO_deg попробовать переиспользовать в Auth Guild Rating
	{
		private readonly SysDispatcher sysDispatcher;

		public ConnectToDispatcher(SysDispatcher _sysDispatcher) : base("ConnectToDispatcher", 2000)
		{
			sysDispatcher = _sysDispatcher;
		}

		public void Init()
		{
			transport.RegisterSystem(sysDispatcher, noProcessingThread: true);
			transport.OnDisconnect += OnDisconnect;

			sysDispatcher.Start();
		}

		public override void OnTimer()
		{
			try
			{
				if (sysDispatcher.DsConnection == null)
				{
					ILogger.Instance.Send("Попытка подключиться к диспетчеру узлов");
					sysDispatcher.ReconnectToDispatcher();
				}
			}
			catch (Exception exception)
			{
				ILogger.Instance.Send($"DispatcherConnectionManager.ConnectToDispatcher: {exception}", ErrorLevel.error);
			}
		}

		private void OnDisconnect(long connectionId, string ticket, string sIP, DisconnectReason reason)
		{
			if (sysDispatcher.DsConnection != null && sysDispatcher.DsConnection.ConnectionId == connectionId)
			{
				ILogger.Instance.Send("Lost connect to dispatcher", ErrorLevel.warning);
				sysDispatcher.DsConnection = null;
			}
		}
	}
}
