﻿using LobbyInstanceLib.Networking.Common;
using LobbyInstanceLib.Networking.DataRPC;
using Node.Lobby.Assets.Scripts.Lobby;
using Node.Lobby.Assets.Scripts.Lobby.Entries;
using UtilsLib.DegSerializers;
using UtilsLib.NetworkingData;

namespace Node.Lobby.Assets.Scripts.Networking
{
	public class RegisterBitStreamCodecs
	{
		private readonly InstanceCollection instanceCollection;

		public RegisterBitStreamCodecs(InstanceCollection _instanceCollection)
		{
			instanceCollection = _instanceCollection;

			uLink.BitStreamCodec.Add<OneInstanceInfo>(InstanceInfoDeSerialize, null);
			DegSerializer_CommandIn.SetGetterParam<OneInstanceInfo>(null);

			BitStreamHelper.AddDegSerializer<MailData>();
			BitStreamHelper.AddDegSerializer<InstancePersonData>();
			BitStreamHelper.AddDegSerializer<RatingData>();
			BitStreamHelper.AddDegSerializer<RewardRpcData>();
			BitStreamHelper.AddDegSerializer<ProgressData>();
			BitStreamHelper.AddDegSerializer<Prices>();
			uLink.BitStreamCodec.Add<GuildData>(GuildSerialize.DeSerialize, GuildSerialize.Serialize);
			uLink.BitStreamCodec.Add<GuildPersonInfo>(GuildPersonSerialize.DeSerialize, GuildPersonSerialize.Serialize);
			uLink.BitStreamCodec.Add<GuildJoin>(GuildSerialize.GuildJoinDeserealize, GuildSerialize.GuildJoinSerealize);
		}

		object InstanceInfoDeSerialize(uLink.BitStream stream, params object[] codecOptions)
		{
			var info = (uLink.NetworkP2PMessageInfo)codecOptions[0];
			var gameInstance = instanceCollection.GetInstanceByNetworkPeer(info.sender);
			if (gameInstance == null)
			{
				var result = (uLink.ReadObjectResult)codecOptions[1];
				result.Success = false;
				result.ErrorMessage = "oneInstanceInfo == null " + BitStreamHelper.NetworkP2PMessageInfoDetails(info);
			}

			return gameInstance;
		}
	}
}
