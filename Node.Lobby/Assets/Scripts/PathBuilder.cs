﻿using System.IO;

namespace Node.Lobby.Assets.Scripts
{
	internal class PathBuilder
	{
		private readonly string streamingAssetsPath;

		public PathBuilder(string streamingAssetsPath)
		{
			this.streamingAssetsPath = streamingAssetsPath;
		}

		public string LobbyXml
		{
			get { return Path.Combine(streamingAssetsPath, "cfg/lobby.xml"); }
		}
	}
}

