using System;
using System.Xml.Serialization;
using Assets.Scripts.Utils.Logger;

namespace Node.Lobby.Assets.Scripts
{
	public class AppConfig : ILogConfig
	{
		public int ConnectionTimeout = 5000;
		public int MaxPersonsInLogin = 8;
        public int MaxSlotsInLogin = 8;
		public string Version = "TEST";
		
		public p2pLobbyConnectConfig p2pLobbyConnect = new p2pLobbyConnectConfig();
		public DispatcherConfig dispatcher = new DispatcherConfig();
		public LobbyConfig lobby_udp_info = new LobbyConfig();
		public MasterServerConfig master_server = new MasterServerConfig();
		public string SqlConnectString;
		public InstanceServerConfig instance_server = new InstanceServerConfig();
		public int StartTutorialState = 0;
		public bool UseLocalServer = false;
		public string LocalServerHost = "192.168.1.13";
		public string FleetId = "fleet-c2a66ee0-4820-4916-84a3-cf5f3c16dc90";
		public int ShiftPortForAttachInstance = 2000;
		public int ShiftPort = 1000;
		public bool EnableExternalLogs = false;
		public string ConnectionStringExternalLogs = "";
		public string InstanceWorkingDir;
		public string InstanceHostName;

		public bool IsTotalTransportLoggingEnabled = false;
		public int DOSAttackMessageCount = 0;
		public int ZipPacketMinSize = 0;
		public int iMillisecondsRecieveTimeout = 3000;
		public int TransportSendTimeoutMS = 20;

		public string TransportType = "IOCP";
		public UtilsLib.low_engine_sharp.transport.TransportType GetTransportType()
		{
			if (string.IsNullOrEmpty(TransportType) || TransportType.ToLower() == "iocp")
				return UtilsLib.low_engine_sharp.transport.TransportType.IOCPTransport;
			if (TransportType.ToLower() == "asio")
				return UtilsLib.low_engine_sharp.transport.TransportType.BoostAsioTransport;
			throw new ArgumentException("Unknown transport type in .config: " + TransportType);
		}

#if DEBUG
		public bool ConsoleLog { get; set; } = true;
#else
		public bool ConsoleLog { get; set; } = false;
#endif

		public ErrorLevel MinLogLevel { get; set; } = ErrorLevel.debug;
		public bool EnableUlink { get; set; }
		public SosConfig Sos { get; set; } = new SosConfig();
		public NLogConfig nlog { get; set; } = new NLogConfig();
		public bool bDebugLogger { get; set; } = true;

		public class DispatcherConfig
		{
			[XmlAttribute]
			public string ip = string.Empty;

			[XmlAttribute]
			public int port;

			[XmlAttribute]
			public string key = string.Empty;
		}

		public class p2pLobbyConnectConfig
		{
			[XmlAttribute]
			public string host = string.Empty;

			[XmlAttribute]
			public int port;

			[XmlAttribute]
			public string pwd = string.Empty;

			[XmlAttribute]
			public int interval;

			[XmlAttribute]
			public int maxConnections;

			[XmlAttribute]
			public string type = string.Empty;
		}

		public class LobbyConfig
		{
			[XmlAttribute]
			public int MaxConnections;

			[XmlAttribute]
			public bool use_nat;

			[XmlAttribute]
			public string ip = string.Empty;

			[XmlAttribute]
			public int port;
		}

		public class MasterServerConfig
		{
			[XmlAttribute]
			public string ip;

			[XmlAttribute]
			public int port;
		}

		public class InstanceServerConfig
		{
			[XmlAttribute]
			public string group = "shooter";

			[XmlAttribute]
			public string logs;
		}
	}
}
