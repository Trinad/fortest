﻿using System;
using System.Collections.Generic;
using System.Xml;
using UtilsLib;
using UtilsLib.Logic;
using UtilsLib.NetworkingData;

namespace Node.Lobby.Assets.Scripts.Lobby
{
	public class ServerDataCollections
	{
		public Dictionary<string, string> Xmls = new Dictionary<string, string>();
		public List<Prices> PricePoints = new List<Prices>();

		public Action<Dictionary<string, string>> SendXmlToAllInstances;

		public void OnXmlChangedReceived(string data, string fileName)
		{
			ILogger.Instance.Send("OnXmlChangedReceived(): filename " + fileName);

			try
			{
				//провери на валидность хмл
				string decodedata = ZipUtils.GetUnZipBase64DecodeXorString(data, string.Empty);
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(decodedata);

				if (fileName == "remoteconfig.xml")
					RemoteXmlConfig.Instance = decodedata.LoadFromStringXml<RemoteXmlConfig>();

				Xmls[fileName] = data;
				SendXmlToAllInstances?.Invoke(new Dictionary<string, string> { { fileName, data } });
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send("Error in ServerDataCollections.OnXmlChangedReceived:" + ex, ErrorLevel.exception);
			}
		}
	}
}
