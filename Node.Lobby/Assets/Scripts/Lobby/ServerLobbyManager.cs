﻿using System;
using System.Net.NetworkInformation;
using System.Threading;
using Fragoria.Common.Utils.Threads;
using UtilsLib;
using UtilsLib.ExternalLogs;
using UtilsLib.Logic;
using UtilsLib.Logic.ServerStart;

namespace Node.Lobby.Assets.Scripts.Lobby
{
	/// <summary>
	/// Класс, который производит старт лобби сервера
	/// </summary>
	public class ServerLobbyManager : IUpdateGameManager
	{
		public int FPS_MAX { get; } = 120;

		private readonly AppConfig cfg;
		private readonly DIListManagers managers;

		private TimeProviderGameManager timeManager;
		private ExternalLogsManager ExternalLogs;

		public ServerLobbyManager(AppConfig _cfg, DIListManagers _managers)
		{
			cfg = _cfg;
			managers = _managers;
		}

		public void Start()
		{
			FRRandom.Init();

			OneThreadTimer.logMessageFunc = ILogger.Instance.Send;
			OneThreadTimer.logExceptionFunc = ex => ILogger.Instance.Send(ex.ToString(), ErrorLevel.exception);
			OneThreadTimer.SetPerformanceStatisticsManager(new NullPerformanceStatisticsManager());

			timeManager = new TimeProviderGameManager();
			TimeProvider.Instance = timeManager;

			managers.Start();

			ILogger.Instance.Send($"Lobby server v{cfg.Version}");
			ILogger.Instance.Send($"dispatcher ip = {cfg.dispatcher.ip}, port = {cfg.dispatcher.port}, key = {cfg.dispatcher.key}");
			ILogger.Instance.Send($"lobby_udp_info ip = {cfg.lobby_udp_info.ip}, port = {cfg.lobby_udp_info.port}");
			ILogger.Instance.Send($"master_server ip = {cfg.master_server.ip}, port = {cfg.master_server.port}");

			ExternalLogs = new ExternalLogsManager(cfg.EnableExternalLogs, cfg.ConnectionStringExternalLogs);

			try
			{
				var pingStatus = new Ping().Send(cfg.p2pLobbyConnect.host)?.Status;
				if (pingStatus == IPStatus.Success || pingStatus == IPStatus.TimedOut)
				{
					ILogger.Instance.Send($"ServerLobbyManager.Start: p2pLobbyConnect.host='{cfg.p2pLobbyConnect.host}'", $"pingStatus={pingStatus}", ErrorLevel.info);
				}
				else
				{
					ILogger.Instance.Send($"ServerLobbyManager.Start: p2pLobbyConnect.host='{cfg.p2pLobbyConnect.host}'", $"pingStatus={pingStatus}", ErrorLevel.exception);
					Quit();
				}
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send($"ServerLobbyManager.Start: p2pLobbyConnect.host='{cfg.p2pLobbyConnect.host}'", ex.ToString(), ErrorLevel.exception);
				Quit();
			}
		}

		public void Update()
		{
			try
			{
				timeManager.Update();

				managers.Update();
				managers.LateUpdate();
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send(ex.ToString(), ErrorLevel.exception);
			}
		}

		public bool bQuit { get; private set; }

		public void Quit()
		{
			ILogger.Instance.Send("Quit()", ErrorLevel.debug);
			bQuit = true;
		}

		public void Stop()
		{
			managers.UnloadAll();

			FRRandom.Stop();

			ExternalLogs?.Stop();
			ILogger.Instance.Shutdown();
		}

		public override string ToString()
		{
			return "Lobby";
		}
	}
}
