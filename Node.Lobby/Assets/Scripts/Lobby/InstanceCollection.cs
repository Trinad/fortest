﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Node.Lobby.Assets.Scripts.Lobby.Entries;

namespace Node.Lobby.Assets.Scripts.Lobby
{
	public class InstanceCollection
	{
	    private Dictionary<string, OneInstanceInfo> instanceList = new Dictionary<string, OneInstanceInfo>();

        public int GetRunningInstanceCount()
		{
			int runningInstances = 0;
			foreach (var inst_info in instanceList.Values)
			{
				if (!inst_info.peer.Equals(uLink.NetworkPeer.unassigned))
				{
					runningInstances++;
				}
			}

			return runningInstances;
		}

		internal int Count()
		{
			return instanceList.Count;
		}

		public OneInstanceInfo AddInstanceInfo(string ticket, string levelName)
		{
			return instanceList[ticket] = new OneInstanceInfo(ticket, levelName);
		}

		public void OnInstanceClear(string ticket)
		{
			OneInstanceInfo oii = GetInstanceByTicket(ticket);
			if (oii != null)
			{
				instanceList.Remove(ticket);
			}
		}

		public OneInstanceInfo GetInstanceByTicket(string ticket)
		{
			OneInstanceInfo instInfo;
			instanceList.TryGetValue(ticket, out instInfo);
			if (instInfo == null)
			{
				ILogger.Instance.Send("GetInstanceByTicket() : not found by ticket <" + ticket + ">", 
					new StackTrace().ToString(), string.IsNullOrEmpty(ticket) ? ErrorLevel.trace : ErrorLevel.warning);
			}
			return instInfo;
		}

		public OneInstanceInfo GetInstanceByNetworkPeer(uLink.NetworkPeer peer)
		{
			foreach (var instInfo in instanceList)
				if (instInfo.Value.peer == peer)
					return instInfo.Value;

			return null;
		}

        public List<OneInstanceInfo> GetAllInstances()
        {
            return (from oneInstanceInfo in instanceList where oneInstanceInfo.Value != null select oneInstanceInfo.Value).ToList();
        }
    }
}
