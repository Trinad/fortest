﻿using NSubstitute;
using uLink;
using UtilsLib.low_engine_sharp.transport;
using UtilsLib.Protocol;

namespace Node.Lobby.Assets.Scripts.Lobby.Entries
{
	public class OneInstanceInfo
	{
		public OneInstanceInfo(string ticket, string levelName)
		{
			this.ticket = ticket;
			sLevelName = levelName;
		}
		
		public NetworkPeer peer;
		public string ticket;
		public string sLevelName;
		public string IpForClient;
		public int PortForClient;
		public string JsonName;

		public IInstanceSysLobby<Connection> ServerRpcListner = Substitute.For<IInstanceSysLobby<Connection>>();

		public void SetInstance(string ip, int port)
		{
            IpForClient = ip;// gameSession.IpAddress;
            PortForClient = port;// gameSession.Port;
		}

		public void SetPeer(uLink.NetworkPeer peer)
		{
			this.peer = peer;
		}

		public bool Ready()
		{
			return !string.IsNullOrEmpty(IpForClient) && !peer.Equals(NetworkPeer.unassigned);
		}
	}
}
