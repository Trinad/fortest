﻿using System;
using System.Diagnostics;
using System.Text;
using UtilsLib.Logic;

namespace Node.Lobby.Assets.Scripts.Lobby
{
	public sealed class LobbyServerStatistics : ActivatedByTimer
	{
		private readonly InstanceCollection instanceCollection;
		private readonly CPUUsage CPUUsage = new CPUUsage();

		public LobbyServerStatistics(InstanceCollection _instanceCollection)
			: base(60)
		{
			instanceCollection = _instanceCollection;
		}

		private int nodemanager_instancecount_prev;

		private void AppendNodeManagerInfo(StringBuilder sb)
		{
			int instanceCount = instanceCollection.Count();

			sb.Append($"NodeManager (instance count: {instanceCount}, previous {nodemanager_instancecount_prev} ){Environment.NewLine}");
			nodemanager_instancecount_prev = instanceCount;
		}

		private void AppendPerformanceStatsInfo(StringBuilder sb)
		{
			const int bytesInMb = 1048576;
			long totalGcMemory = GC.GetTotalMemory(false) / bytesInMb;
			var process = Process.GetCurrentProcess();
			long processPrivateMemory = process.PrivateMemorySize64 / bytesInMb;
			long processWorkingMemory = process.PeakWorkingSet64 / bytesInMb;
			var cpuUsage = CPUUsage.Read();

			sb.Append($"Lobby CPU usage: {cpuUsage:0.00}%, GC memory {totalGcMemory} MB, processPrivateMemory {processPrivateMemory} MB, " +
			          $"processWorkingMemory {processWorkingMemory} MB");
		}

		public override void OnTimer()
		{
			StringBuilder sb = new StringBuilder();

			AppendNodeManagerInfo(sb);
			AppendPerformanceStatsInfo(sb);

			ILogger.Instance.Send(sb.ToString());
		}
	}
}

