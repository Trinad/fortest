﻿using System;
using System.IO;
using System.Reflection;
using Assets.Scripts.Utils.Logger;
using Node.Lobby.Assets.Scripts.Networking;
using UtilsLib.ExternalLogs;
using UtilsLib.Logic;
using Zenject;

namespace Node.Lobby.Assets.Scripts.Lobby.DI
{
	public class PixelLobbyInstaller : Installer
	{
		public override void InstallBindings()
		{
			// регистрируем зависимости
			InstallULink();
			InstallConfigsAndSettings();

			Container.Bind<LobbyServerStatistics>().AsSingle();
			Container.Bind<ExternalLogsManager>().AsSingle();
			Container.Bind<SysDispatcher>().AsSingle();
			Container.Bind<TransportManager>().AsSingle();
			Container.Bind<InstanceCollection>().AsSingle();
			Container.Bind<NodeManager>().AsSingle();
			Container.Bind<ServerDataCollections>().AsSingle();

			InstallControllers();

			Container.Bind<DIListManagers>().AsSingle();
			Container.BindInterfacesAndSelfTo<ServerLobbyManager>().AsSingle().NonLazy();
		}

		private void InstallULink()
		{
			const int networkViewId = 2;
			var network = uLink.Network.Instance;
			var p2p = new uLink.NetworkP2P(network);
			var view = new uLink.NetworkView(network);
			view.RegisterListener(this);
			view.stateSynchronization = uLink.NetworkStateSynchronization.Off;
			view.SetManualViewID(networkViewId);
			view.OnEnable();

			network.statsCalculationInterval = 1.0f;

			Container.Bind<uLink.Network>().FromInstance(network).AsSingle();
			Container.Bind<uLink.NetworkP2P>().FromInstance(p2p).AsSingle();
			Container.Bind<uLink.INetworkView>().FromInstance(view).AsSingle();

			Container.Bind<uLinkManager>().AsSingle();
			Container.Bind<RegisterBitStreamCodecs>().AsSingle().NonLazy(); //AsTransient что-то не сработало
		}

		private void InstallConfigsAndSettings()
		{
			//Container.Bind<InstanceCommandLineOptions>().FromInstance(m_options).AsSingle();

			var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			var pathBuilder = new PathBuilder(Path.Combine(path, "Assets/StreamingAssets"));
			Container.Bind<PathBuilder>().FromInstance(pathBuilder).AsSingle();

			var appConfig = pathBuilder.LobbyXml.LoadFromFileXml<AppConfig>();
			appConfig.nlog.ConfigurationFileName = "Assets/StreamingAssets/cfg/NLog.xml";
			Container.Bind<AppConfig>().FromInstance(appConfig).AsSingle();

			var logger = new AsyncLogger(appConfig, new LobbyInstanceLogger(appConfig));
			logger.Init();
			logger.AddTag("srvType", "Lobby");
			ILogger.Instance = logger;
			Container.Bind<ILogger>().FromInstance(logger).AsSingle();

			//Container.BindInterfacesAndSelfTo<Clock>().AsSingle();

			//InstallLoadAllXmlsInParallel();

			//Container.Bind(typeof(InstanceSqlAdapter), typeof(MSSQLRenameAdapter), typeof(IRenameAdapter))
			//	.To<InstanceSqlAdapter>()
			//	.FromResolveGetter<AppConfig, InstanceSqlAdapter>(config => new InstanceSqlAdapter(config.SqlConnectString, "Instance", logStateInfo: false))
			//	.AsSingle();

			Container.Bind<Func<Type, object>>().FromInstance((type) => Container.Resolve(type)).AsSingle();
		}

		private void InstallControllers()
		{
			InstallController<InstanceController>();
		}

		private void InstallController<T>()
		{
			Container.BindInterfacesAndSelfTo<T>().AsSingle().NonLazy();
		}
	}
}
