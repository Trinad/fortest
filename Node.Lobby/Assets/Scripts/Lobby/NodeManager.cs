﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using Node.Lobby.Assets.Scripts.Networking;
using UtilsLib.Logic;

namespace Node.Lobby.Assets.Scripts.Lobby
{
	public class NodeManager : BaseGameManager
	{
		private readonly AppConfig cfg;
		private readonly InstanceCollection instanceCollection;

		public SysDispatcher sysDispatcher;

		public NodeManager(AppConfig _cfg, InstanceCollection _instanceCollection)
		{
			cfg = _cfg;
			instanceCollection = _instanceCollection;
		}

		private class StartInstanceData
		{
			public StartInstanceData(string ticket, string levelName)
			{
				Ticket = ticket;
				LevelName = levelName;
				CreateTime = TimeProvider.UTCNow;
			}

			public string Ticket { get; }
			public string LevelName { get; }
			public DateTime CreateTime { get; }
		}

		private readonly float instanceStartDelay = 1f;
		private DateTime lastInstanceStartTime;
		private readonly ConcurrentQueue<StartInstanceData> startInstanceQueue = new ConcurrentQueue<StartInstanceData>();

		public override void Update()
		{
			if (lastInstanceStartTime < TimeProvider.UTCNow && startInstanceQueue.TryDequeue(out var sid))
			{
				lastInstanceStartTime = TimeProvider.UTCNow.AddSeconds(instanceStartDelay);

				ILogger.Instance.Send($"Try to start instance, Ticket = {sid.Ticket}, LevelParam = {sid.LevelName}, DelayTime = {TimeProvider.UTCNow - sid.CreateTime} ", ErrorLevel.info);

				instanceCollection.AddInstanceInfo(sid.Ticket, sid.LevelName);

				string commandLine = $"{cfg.InstanceWorkingDir}/Node.Instance.dll --ticket {sid.Ticket} --level {sid.LevelName} " +
					                    $"--ver {cfg.Version} --host {cfg.p2pLobbyConnect.host} --port {cfg.p2pLobbyConnect.port}";
				var processStartInfo = new ProcessStartInfo("dotnet", commandLine);
				processStartInfo.WorkingDirectory = cfg.InstanceWorkingDir;
				var p = new Process {StartInfo = processStartInfo, EnableRaisingEvents = true};
				p.Start();
			}
		}

		public void StartInstance(string ticket, string levelName)
		{
			var sid = new StartInstanceData(ticket, levelName);
			startInstanceQueue.Enqueue(sid);

			ILogger.Instance.Send(string.Format("Enqueue instance, Ticket = {0}, LevelName = {1}, EnqueueTime = {2}", sid.Ticket, 
				sid.LevelName, TimeProvider.UTCNow), ErrorLevel.info);
		}
	}
}