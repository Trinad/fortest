﻿using Assets.Scripts.Utils.Db;
using NodeGuild.Logic.Entries;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using UtilsLib;
using UtilsLib.corelite;
using UtilsLib.Logic;
using UtilsLib.NetworkingData;

namespace NodeGuild.DB
{
	public class MSSQLAdapter : CoreLite.Adapters.MsSqlAdapter
	{
		private readonly MailDbConverter m_mailConverter;

		internal MSSQLAdapter(string sConnect)
			: base(sConnect)
		{
			m_mailConverter = new MailDbConverter();
		}

		internal List<Guild> ClansLoad()
		{
			const string sql = "ClansLoad";
			ILogger.Instance.Send($"{sql}");

			List<Guild> result = new List<Guild>();

			using (SqlConnection connection = GetOpenConnection())
			{
				try
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = sql;

						using (SqlDataReader reader = command.ExecuteReader())
						{
							while (reader.Read())
							{
								Guild guild = new Guild();
								guild.FillFromDataRow(new DBRecordCollection(reader));
								result.Add(guild);
							}
						}
					}
				}
				catch (Exception ex)
				{
					ILogger.Instance.Send($"MSSQLAdapter.{sql} {ex}", ErrorLevel.error);
					throw;
				}
			}
			return result;
		}

		internal int ClanCreate(Guild guild)
		{
			int clanid = -1;
			string sql = "ClanCreate";
			ILogger.Instance.Send(sql);
			using (var connection = GetOpenConnection())
			{
				using (var Command = connection.CreateCommand())
				{
					Command.CommandText = sql;
					Command.CommandType = CommandType.StoredProcedure;
					Command.CommandTimeout = CiCommandTimeout;

					guild.FillCommandCreate(Command);

					try
					{
						using (var reader = Command.ExecuteReader(CommandBehavior.CloseConnection))
						{
							while (reader.Read())
							{
								clanid = (int)(decimal)reader["clanid"];
							}
						}
					}
					catch (Exception ex)
					{
						ILogger.Instance.Send(ex.ToString(), ErrorLevel.error);
						throw;
					}
				}
			}

			return clanid;
		}

		internal void ClanCommit(Guild guild)
		{
			string sql = "ClanCommit";
			ILogger.Instance.Send(sql);
			using (var connection = GetOpenConnection())
			{
				using (var Command = connection.CreateCommand())
				{
					Command.CommandText = sql;
					Command.CommandType = CommandType.StoredProcedure;
					Command.CommandTimeout = CiCommandTimeout;

					guild.FillCommandUpdate(Command);

					try
					{
						Command.ExecuteNonQuery();
					}
					catch (Exception ex)
					{
						ILogger.Instance.Send(ex.ToString(), ErrorLevel.error);
						throw;
					}
				}
			}
		}

		internal void ClanDelete(Guild guild)
		{
			string sql = "ClanDelete";
			ILogger.Instance.Send($"{sql}");
			using (var connection = GetOpenConnection())
			{
				using (var Command = connection.CreateCommand())
				{
					Command.CommandText = sql;
					Command.CommandType = CommandType.StoredProcedure;
					Command.CommandTimeout = CiCommandTimeout;

					Command.AddParameter("@Id", guild.guildData.ClanId);

					try
					{
						Command.ExecuteNonQuery();
					}
					catch (Exception ex)
					{
						ILogger.Instance.Send(ex.ToString(), ErrorLevel.error);
						throw;
					}
				}
			}
		}

		internal void CreateMail(MailData mail, Action<MailData> onSuccess, Action<MsSqlLoadResult, Exception> onError)
		{
			var parameters = m_mailConverter.ToSqlDictionary(mail);
			var action = _ExecuteWithOneResult(parameters, m_mailConverter, onSuccess, onError, Procedures.CreateMail);
			action();
		}

		public GuildPersonInfo GetLoginTinyInfo(int loginId)
		{
			string sql = "LoginInfoTinyGet";
			ILogger.Instance.Send(sql);

			using (var connection = GetOpenConnection())
			{
				using (var Command = connection.CreateCommand())
				{
					Command.CommandText = sql;
					Command.CommandType = CommandType.StoredProcedure;
					Command.CommandTimeout = CiCommandTimeout;

					Command.AddParameter("@LoginId", loginId);

					try
					{
						using (var reader = Command.ExecuteReader(CommandBehavior.CloseConnection))
						{
							if (reader.Read())
							{
								var name = (string)reader["LoginName"];
								var data = (string)reader["LoginInfo"];

								var ht = (Hashtable) JSON.JsonDecode(data); //TODO_deg это LobbyLoginSqlData

								return new GuildPersonInfo
								{
									LoginId = loginId,
									PersonName = name,
									AvatarName = ht.TryGet("AvatarName", string.Empty),
									RatingScore = ht.TryGet("RatingScore", 500),
								};
							}
						}
					}
					catch (Exception ex)
					{
						ILogger.Instance.Send(ex.ToString(), ErrorLevel.error);
						throw;
					}
				}
			}

			return null;
		}
	}
}
