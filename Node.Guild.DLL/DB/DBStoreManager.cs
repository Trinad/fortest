﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using Assets.Scripts.Utils.Db;
using Assets.Scripts.Utils.Logger;
using Fragoria.Common.Utils.Threads;
using NodeGuild.Logic.Entries;
using NodeGuild.Utils;
using UtilsLib;
using UtilsLib.db;
using UtilsLib.Logic;
using UtilsLib.NetworkingData;

namespace NodeGuild.DB
{
    /// <summary>
    /// Менеджер, сохраняющий в базу сущности
    /// </summary>
	public class DBStoreManager : DBStoreManagerBase
    {
        public DBStoreManager()
            : base(1000)
        {
        }

		private const int SaveGuildsEveryMinutes = 5;
		
		private static readonly ConcurrentDictionary<string, MSSQLAdapter> m_htAdapters = 
			new ConcurrentDictionary<string, MSSQLAdapter>(); //адаптеры -- по одному на один поток

	    public static MSSQLAdapter GetAdapter()
	    {
		    int iThreadHash = Thread.CurrentThread.GetHashCode();
		    MSSQLAdapter res;
		    string key = iThreadHash.ToString(CultureInfo.InvariantCulture);

		    if (!m_htAdapters.TryGetValue(key, out res))
		    {
			    res = new MSSQLAdapter(SystemConfig.sDBConnectString);
			    m_htAdapters[key] = res;
		    }

		    return res;
	    }

		public override void OnTimer()
        {
            try
            {
                base.OnTimer();
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send(ex.ToString(), ErrorLevel.error);
                Sos.Fatal(ex.Message);
            }
        }
		
        public override void SaveData()
        {
	        base.SaveData();

	        try
	        {
		        ProcessSaveGuild();
			}
	        catch (Exception ex)
	        {
				ILogger.Instance.Send(ex.ToString(), ErrorLevel.error);
			}

	        try
	        {
		        ProcessSaveMail();
			}
	        catch (Exception ex)
	        {
				ILogger.Instance.Send(ex.ToString(), ErrorLevel.error);
			}
		}

		private readonly ConcurrentQueue<Guild> m_guildsToDelete = new ConcurrentQueue<Guild>();
		private DateTime m_lastGuildSave = DateTime.MinValue;

	    public void EnqueueForDelete(Guild guild)
	    {
		    m_guildsToDelete.Enqueue(guild);
	    }

	    void ProcessSaveGuild()
	    {
		    var adapter = GetAdapter();

			Guild guildToRemove;
		    while (m_guildsToDelete.TryDequeue(out guildToRemove))
		    {
				ILogger.Instance.Send($"ProcessSaveGuild() RemoveGuild() guildId = {guildToRemove.guildData.ClanId}");
			    adapter.ClanDelete(guildToRemove);
			}

			// во время останова сохранить обязательно. а пока сервер работает, сохраняем раз в N минут.
		    if (bRunning)
		    {
			    if (m_lastGuildSave.AddMinutes(SaveGuildsEveryMinutes) > TimeProvider.UTCNow)
				    return;

				m_lastGuildSave = TimeProvider.UTCNow;
		    }

		    var guildList = GuildServer.Instance.GuildManager.GetGuilds();
		    foreach (Guild guild in guildList)
		    {
			    if (!guild.bChanged)
				    continue;
			    guild.bChanged = false;

			    ILogger.Instance.Send($"ProcessSaveGuild() saving guildId = {guild.guildData.ClanId}");

			    try
			    {
				    if (guild.bGetFromDB)
				    {
					    adapter.ClanCommit(guild);
				    }
				    else
				    {
				        int guildid = adapter.ClanCreate(guild);
				        guild.guildData.ClanId = guildid;
                        guild.bGetFromDB = true;
				    }
			    }
			    catch (Exception ex)
			    {
				    ILogger.Instance.Send(ex.ToString(), ErrorLevel.exception);
			    }
		    }
	    }

	    private class MailSaveData
	    {
		    public MailData Mail;
		    public Action<MailData> OnSuccess;
		    public Action<MsSqlLoadResult, Exception> OnError;
	    }

	    private readonly ConcurrentQueue<MailSaveData> qMail = new ConcurrentQueue<MailSaveData>();

	    public void EnqueueForSave(MailData mail, Action<MailData> onSuccess, Action<MsSqlLoadResult, Exception> onError)
	    {
		    qMail.Enqueue(new MailSaveData
		    {
			    Mail = mail,
				OnSuccess = onSuccess,
				OnError = onError
		    });
	    }

	    private void ProcessSaveMail()
	    {
		    var adapter = GetAdapter();
		    MailSaveData mailSaveData;
		    while (qMail.TryDequeue(out mailSaveData))
		    {
			    adapter.CreateMail(mailSaveData.Mail, mailSaveData.OnSuccess, mailSaveData.OnError);
		    }
	    }
	}
}

