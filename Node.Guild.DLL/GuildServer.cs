﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using Assets.Scripts.Utils.Logger;
using CoreLite;
using Fragoria.Common.Utils.Pool;
using Fragoria.Common.Utils.Threads;
using NodeGuild.DB;
using NodeGuild.Logic;
using NodeGuild.Network;
using NodeGuild.Protocols;
using NodeGuild.Utils;
using Utils.Events;
using UtilsLib;
using UtilsLib.low_engine_sharp.transport;
using UtilsLib.Logic.Enums;
using UtilsLib.Logic;
using Node.Guild.Logic;
using UtilsLib.Logic.ServerStart;
using UtilsLib.NetworkingData;

namespace NodeGuild
{
	public sealed class GuildServer : IStartGameManager
	{
		public PerformanceStatisticsManager PerformanceManager { get; private set; }
		public SysDispatcher Dispatcher { get; private set; }
		public DispatcherConnectionManager ConnectionManager { get; private set; }
		public DBStoreManager DBStoreManager { get; private set; }
		public GuildManager GuildManager { get; private set; }
		public FriendManager FriendManager { get; private set; }
		public static TimeManager TimeManager = new TimeManager();
        public RemoteXmlConfig RemoteXmlConfig;
        //public SubscribeManagerBase<GroupList, PreGroup, int> SubscribeManagerBase;
        public SubscribeManager SubscribeManagerBase;
        public ESportDataSender ESportDataSender;

        public Events Events = Events.Instance;

		public bool bStarted;
		
		private static readonly string _versionInfo = System.Reflection.Assembly.GetExecutingAssembly()
			.GetCustomAttributes(inherit: false)
			.OfType<System.Reflection.AssemblyInformationalVersionAttribute>()
			.Single().InformationalVersion;
		public static string VersionInfo { get { return _versionInfo; } }

		public void Start()
		{
			try
			{
				Sos.Debug("StartServer!");

				OneThreadTimer.logMessageFunc = LogMessageFromOneThreadTimer;
				OneThreadTimer.logExceptionFunc = LogExceptionFromOneThreadTimer;

				PerformanceManager = new PerformanceStatisticsManager(
					SystemConfig.bPerformanceStatisticsEnabled,
					SystemConfig.sStasdHost,
					SystemConfig.sStatsdPort,
					SystemConfig.sStatsdServerName("Pixel.Guild", "."),
					200);
				BaseCmdSystem.SetPerformanceStatisticsManager(PerformanceManager);
				OneThreadTimer.SetPerformanceStatisticsManager(PerformanceManager);
				//m_PerformanceManager.RegisterAddinEvents(Enum.GetNames(typeof(UtilsLib.AddinEvents.Enums.AddInsEventTypes)));
				PerformanceManager.Start(ThreadPriority.Normal);
				
				TimeManager.Start(ThreadPriority.Normal);

				ILogger.Instance.Send($"Version: {VersionInfo}");

				PoolManager.Instance.Start(ThreadPriority.Normal);

				Entry.GetAdapterCallback = DBStoreManager.GetAdapter;
				Entry.EntryCreateCallback = EntryCreate;
				RegisterEntryTypes();

                //AuthManager = new AuthManager();
                //NodeManager = new NodeManager();

                GuildManager = new GuildManager();
				FriendManager = new FriendManager();
				SubscribeManagerBase = new SubscribeManager(GuildManager);

                InitTransport();
				RegisterTransportSystem();
                StartManagers(SubscribeManagerBase, GuildManager);

				//GetAdapter(null).LoadRating("week", RatingManager.WeekRating);

				StartTransport();

				bStarted = true;

				Sos.Debug("GuildServer: старт успешен");
			}
			catch (Exception ex)
			{
				ILogger.Instance.Send(ex.ToString(), ErrorLevel.error);
				Stop();
				throw;
			}
		}

		public void Stop()
		{
			try
			{
                if (ESportDataSender != null)
                {
                    ESportDataSender.Stop();
                    ILogger.Instance.Send("ESportDataSender stop");
                }

                if (SubscribeManagerBase != null)
                {
                    SubscribeManagerBase.Stop();
                    ILogger.Instance.Send("SubscribeManagerBase stop.");
                }

                ILogger.Instance.Send("Попытка останова сервера ...");
				if (ConnectionManager != null)
				{
					ConnectionManager.Stop();
					ILogger.Instance.Send("ConnectionManager stop.");
				}

				if (DBStoreManager != null)
				{
					DBStoreManager.Stop();
					ILogger.Instance.Send("DBStoreManager stop.");
				}

				TimeManager.Stop();
				ILogger.Instance.Send("TimeManager stop.");

				PoolManager.Instance.Stop();
				ILogger.Instance.Send("PoolManager stop.");

				PerformanceManager.Stop();
				ILogger.Instance.Send("m_PerformanceManager stop.");

				transport.Stop();
				ILogger.Instance.Send("Транспорт остановлен ...");

				Events.Stop();
				FRRandom.Stop();

				//Ждем всех минуту, кто не успел, тот опоздал
				for (int i = 0; i < 60 && (transport.ThreadCount > 0 || OneThreadTimer.ThreadCount > 0); i++)
				{
					//сказать серверу что нужно подождать секунды 1
					ILogger.Instance.Send("Wait 1 seconds...");
					Thread.Sleep(1000);
				}

				Sos.Debug("GuildServer остановлен ...");
			}
			catch (Exception exception)
			{
				ILogger.Instance.Send("Остановка сервера завершилась неудачей. Ошибка " + exception, ErrorLevel.error);
				throw;
			}
			finally
			{
				ILogger.Instance.Shutdown();
			}
		}

		private static void InitTransport()
		{
			transport.IsTotalLoggingEnabled = SystemConfig.IsTotalTransportLoggingEnabled;
			transport.default_dos_border = SystemConfig.iDOSAttackMessageCount;
			transport.ZipPacketMinSize = SystemConfig.ZipPacketMinSize;

			ILogger.Instance.Send("Init transport!");
			transport.Init(10000,
				0,
				SystemConfig.sServerName,
				SystemConfig.sServerPort,
				SystemConfig.sAdditionalIPs,
				SystemConfig.sAdditionalPorts,
				SystemConfig.TransportType,
				TimeManager);

			transport.SetSendTimeout(SystemConfig.TransportSendTimeoutMS);

			Sos.Debug("InitTransport");
		}

		private void StartTransport()
		{
			Sos.Debug("transport.Start(): стартует...");
			transport.Start();
			Sos.Debug("GuildServer:", "transport.Start() успешно");
			Dispatcher.Start();

			Sos.Debug("StartTransport");
		}

		private void RegisterTransportSystem()
		{
			ILogger.Instance.Send("Register SysDispatcher");
			Sos.Debug("RegisterTransportSystem: старт успешен");
            Dispatcher = new SysDispatcher(SubscribeManagerBase, GuildManager, FriendManager);
            SubscribeManagerBase.SetSysDyspatcher(Dispatcher);

            transport.RegisterSystem(Dispatcher);
			
			//transport.AddSpecCommand("crossdomain.xml", FlashPolicyFile.CommandGetCrossDomain);
			//transport.AddSpecCommand(SystemConfig.ExternalApiUrl, FlashPolicyFile.CommandExternalAPI);

			//ILogger.Instance.Send("Register SysFlashPolicyFile");
			//transport.RegisterSystem(SysFlashPolicyFile = new FlashPolicyFile());
			//SysFlashPolicyFile.Start(); //нечего там стартовать...

			transport.OnDisconnect += OnDisconnect;
		}

		private void OnDisconnect(long connectionId, string ticket, string sIP, DisconnectReason reason)
		{
			if (Dispatcher.DsConnection != null && Dispatcher.DsConnection.ConnectionId == connectionId)
			{
				ILogger.Instance.Send("Lost connect to dispatcher", ErrorLevel.warning);
				Dispatcher.DsConnection = null;
			}
		}

		public void StartManagers(SubscribeManager subscribeManagerBase, GuildManager guildManager)
		{
			DBStoreManager = new DBStoreManager();
			
			ConnectionManager = new DispatcherConnectionManager(2000);
			ConnectionManager.Start(ThreadPriority.Normal);
			ILogger.Instance.Send("ConnectionManager: " + ConnectionManager.iThreadId.ToString(CultureInfo.InvariantCulture), ErrorLevel.warning);

            guildManager.Init();

			DBStoreManager.Start(ThreadPriority.Normal);
			ILogger.Instance.Send("DBStoreManager: " + DBStoreManager.iThreadId.ToString(CultureInfo.InvariantCulture), ErrorLevel.warning);
			
            //SubscribeManagerBase = new SubscribeManager(GuildManager);
            subscribeManagerBase.Init();
            subscribeManagerBase.Start(ThreadPriority.Normal);

            ESportDataSender = new ESportDataSender();
            ESportDataSender.Start(ThreadPriority.Normal);

            Events.Start(ThreadPriority.Normal);

			Sos.Debug("GuildServer: ", "StartManagers");
		}

		private static void LogMessageFromOneThreadTimer(string msg)
		{
			ILogger.Instance.Send(msg);
		}

		private static void LogExceptionFromOneThreadTimer(Exception ex)
		{
			ILogger.Instance.Send(ex.ToString(), ErrorLevel.error);
		}

        private static Entry EntryCreate(object sender, int oEntryType)
        {
            throw new ApplicationException(string.Format("EntryCreate: Unknown type {0}.", oEntryType.ToString(CultureInfo.InvariantCulture)));
        }

		private static void RegisterEntryTypes()
		{
			for (int i = 1; i < (int)EntryTypes.Count; i++)
			{
				Entry.RegisterType(i, ((EntryTypes)i).ToString());
			}
		}

        public static GuildServer Instance;
	}
}
