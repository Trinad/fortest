using NodeGuild.Logic.Entries;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilsLib.Logic;
using UtilsLib.NetworkingData;
using UtilsLib.NetworkingData.Chat;

namespace NodeGuild.Protocols
{
	public sealed partial class SysDispatcher
	{
        private Dictionary<int, Action<int, ChannelTypes>> chatCreateTransactions = new Dictionary<int, Action<int, ChannelTypes>> ();
        private int transactionIdGenerator;

        public void OnGuildList(int loginId, string name)
		{
			const int SearchCount = 100;

			var guilds = guildManager.SearchGuilds(name).Select(x =>
			{
				guildManager.UpdateMemberData(x);
				return x.guildData;
			}).ToList();

			guilds.Sort((x, y) => y.RatingScore - x.RatingScore);
			guilds = guilds.Take(SearchCount).ToList();

			SendRpc(GetInstanceByLogin(loginId).ClanListAnswer, loginId, guilds);
		}

		internal void NotifyAboutRoleChange(GuildPersonInfo person)
		{
			var guild = guildManager.GetGuildByLoginId(person.LoginId);
			if (guild == null)
				return;

			SendRpc(GetInstanceByLogin(person.LoginId).SetGuild, person.LoginId, guild.guildData, person.RoleId);
		}

		public void OnUpdateLoginData(LoginData loginData)
		{
			guildManager.OnRenameLogin(loginData.LoginId, loginData.Name, loginData.Avatar);
			friendManager.OnUpdateLoginData(loginData);
		}

		public void OnRatingScoreChange(int loginId, int ratingScore)
		{
			guildManager.OnRatingScoreChange(loginId, ratingScore, out var update);
			friendManager.OnRatingScoreChange(loginId, ratingScore);

			if (update)
			{
				var guild = guildManager.GetGuildByLoginId(loginId);
				if (guild != null)
				{
					guildManager.UpdateMemberData(guild);

					foreach (var person in guild.Persons)
						if (person.LoginId != loginId)
							SendRpc(GetInstanceByLogin(person.LoginId).SetGuild, person.LoginId, guild.guildData, person.RoleId);

					SendRpc(rating.UpdateGuildData, guild.guildData, loginId);
				}
			}
		}

		public void OnGuildSaveChanges(int loginId, GuildData guildData)
		{
            Guild currentSettings = guildManager.GetGuildByLoginId (loginId);

            bool isClanNameChanged = guildData.ClanName != currentSettings.guildData.ClanName;
            string adminName = currentSettings.GetPerson (loginId).PersonName;
            string oldClanName = currentSettings.guildData.ClanName;
            string newClanName = guildData.ClanName;


            var error = guildManager.ChangeGuild(loginId, guildData, out var guild);

			SendRpc(GetInstanceByLogin(loginId).GuildSaveChangeAnswer, loginId, error, guild.guildData);

			if (error == ClientErrors.SUCCESS && guild.Persons != null)
			{
				foreach (var person in guild.Persons)
				{
					if (person.LoginId != loginId)
						SendRpc(GetInstanceByLogin(person.LoginId).SetGuild, person.LoginId, guild.guildData, person.RoleId);

					UpdatePersonCardGuild(person, guild);
				}

                if (isClanNameChanged)
                {
                    ClanNameChangedActivityChatMessage message = new ClanNameChangedActivityChatMessage ()
                    {
                        adminNickname = adminName,
                        oldClanName = oldClanName,
                        newClanName = newClanName
                    };
                    SendMessage (guild, message.ToJson ());
                }
            }
		}

		public void OnClanResetCooldown(int loginId)
		{
			var clan = guildManager.GetGuildByLoginId(loginId);
			if (clan != null)
			{
				clan.guildData.ClanNameCooldown = DateTime.MinValue;

				foreach (var person in clan.Persons)
					SendRpc(GetInstanceByLogin(person.LoginId).SetGuild, person.LoginId, clan.guildData, person.RoleId);
			}
		}

		public void OnClanMemberList(int loginId, int clanId)
		{
			var guild = guildManager.GetGuildById(clanId);
			if (guild != null)
				guildManager.UpdateMemberData(guild);

			var persons = guild?.Persons ?? new List<GuildPersonInfo>();
			persons.Sort((x, y) => y.RatingScore - x.RatingScore);

			SendRpc(GetInstanceByLogin(loginId).ClanMemberListAnswer, loginId, clanId, persons);
		}

		public void OnGuildCurrentJoinRequest(int loginId)
		{
			var result = guildManager.GetJoinGuild(loginId);

			SendRpc(GetInstanceByLogin(loginId).GuildCurrentJoinAnswer, loginId, result);
		}

		public void OnGuildJoinList(int loginId)
		{
			var result = guildManager.GetGuildJoinList(loginId);

			SendRpc(GetInstanceByLogin(loginId).GuildJoinListAnswer, loginId, result);
		}

		public void OnGuildInviteRequest(int loginId, int targetLoginId, string inviteText)
		{
			//если таргета приняли то тут его гильдия. чтобы его уведомить об этом. проверять вместе с результатом. если только SUCCESS и только если принимали а не отклоняли заявку
			var error = guildManager.GuildInviteRequest(loginId, targetLoginId, inviteText, out _);

			SendRpc(GetInstanceByLogin(loginId).GuildInviteAnswer, loginId, error);
		}

		public void OnGuildCheckName(int loginId, string guildName)
		{
			var error = guildManager.CheckGuildName(guildName);

			SendRpc(GetInstanceByLogin(loginId).CheckGuildNameAnswer, loginId, error);
		}

        private void GuildManager_OnMemberJoin(Guild guild, GuildPersonInfo target)
        {
            CreateChatChannelIfNotExist (guild);

            //Отсылаем сообщение в чат клана о том, что игрок вступил в клан
            UserJoinActivityChatMessage userJoinActivityChatMessage = new UserJoinActivityChatMessage ()
            {
                joinedUserNickname = target.PersonName
            };

            SendMessage (guild, userJoinActivityChatMessage.ToJson ());
        }

        private void SendMessage(Guild guild, string message)
        {
            if (guild.guildData.ChannelId == -1)
            {
                guild.notSendedMessages.Add (message);
            }
            else
            {
                SendRpc (chat.OnClientSendMessageRequest, 0, guild.guildData.ChannelId, ChannelTypes.Guild, message);
            }
        }

        public void OnJoinGuild(int loginId, string personName, string avatarName, int ratingScore, string joinDescription, int guildId, int maxClanSize)
        {
	        guildManager.ClanSize = maxClanSize; //TODO_Deg пока так

			var error = guildManager.JoinGuild(loginId, personName, avatarName, ratingScore, joinDescription, guildId, out var guild);

			var roleId = guild?.GetRole(loginId) ?? ClanRole.None;

            if (guild != null && error == ClientErrors.SUCCESS)
            {
                guildManager.UpdateMemberData(guild);
                SendRpc(chat.AddToChatChannel, loginId, guild.guildData.ChannelId);
                SendRpc(rating.AddLoginToGuild, guild.guildData, loginId);
			}

			SendRpc(GetInstanceByLogin(loginId).JoinGuildAnswer, loginId, error, guild?.guildData, roleId);

			//Обновить список у всех
			if (guild != null && error == ClientErrors.SUCCESS)
				foreach (var person in guild.Persons)
				{
					SendRpc(GetInstanceByLogin(person.LoginId).SetGuild, person.LoginId, guild.guildData, person.RoleId);
					SendRpc(GetInstanceByLogin(person.LoginId).ClanMemberListAnswer, person.LoginId, guild.guildData.ClanId, guild.Persons);
				}
		}

        private void GuildManager_OnMemberRoleChanged(Guild guild, GuildPersonInfo admin, GuildPersonInfo target, ClanRole oldRole)
        {
            //Отправляем в чат клана сообщение об изменении роли
            UserRoleChangedActivityChatMessage userRoleChangedActivityChatMessage = new UserRoleChangedActivityChatMessage ()
            {
                adminNickname = admin.PersonName,
                targetNickname = target.PersonName,
                oldClanRole = oldRole,
                newClanRole = target.RoleId
            };

            SendMessage (guild, userRoleChangedActivityChatMessage.ToJson ());
        }

        public void OnClanMemberRoleChange(int loginId, int targetLoginId, ClanRole roleId)
		{
            var guild = guildManager.GetGuildByLoginId (loginId);
            
            var error = guildManager.GuildMemberRoleChange(loginId, targetLoginId, roleId) ? ClientErrors.SUCCESS : ClientErrors.LOGIC_ERROR;

			SendRpc(GetInstanceByLogin(loginId).ClanMemberRoleChangeAnswer, loginId, targetLoginId, error);

			//Обновить список у всех
			if (error == ClientErrors.SUCCESS)
			{
				guildManager.UpdateMemberData(guild);
				foreach (var person in guild.Persons)
				{
					//SendRpc(GetInstanceByLogin(person.LoginId).SetGuild, person.LoginId, guild.guildData, person.RoleId);
					SendRpc(GetInstanceByLogin(person.LoginId).ClanMemberListAnswer, person.LoginId, guild.guildData.ClanId, guild.Persons);
				}
            }
		}

		public void OnClanMemberMakeOwner(int loginId, int targetLoginId)
		{
			var error = guildManager.GuildMemberMakeOwner(loginId, targetLoginId) ? ClientErrors.SUCCESS : ClientErrors.LOGIC_ERROR;

			SendRpc(GetInstanceByLogin(loginId).ClanMemberMakeOwnerAnswer, loginId, targetLoginId, error);

			//Обновить список у всех
			if (error == ClientErrors.SUCCESS)
			{
				var guild = guildManager.GetGuildByLoginId(loginId);
				guildManager.UpdateMemberData(guild);
				foreach (var person in guild.Persons)
				{
					//SendRpc(GetInstanceByLogin(person.LoginId).SetGuild, person.LoginId, guild.guildData, person.RoleId);
					SendRpc(GetInstanceByLogin(person.LoginId).ClanMemberListAnswer, person.LoginId, guild.guildData.ClanId, guild.Persons);
				}
			}
		}

        private void GuildManager_OnClanMemberLeave(Guild guild, GuildPersonInfo admin, GuildPersonInfo target)
        {
            BaseChatMessage userJoinActivityChatMessage;
            if (admin.LoginId == target.LoginId)
            {
                //Отправить сообщение в чата клана о выходе игрока из клана
                userJoinActivityChatMessage = new UserLeaveActivityChatMessage ()
                {
                    leaveUserNickname = target.PersonName
                };
            }
            else
            {
                //Отправить сообщение в чата клана об исключении игрока из клана
                userJoinActivityChatMessage = new UserRemovedActivityChatMessage ()
                {
                    adminNickname = admin.PersonName,
                    targetNickname = target.PersonName
                };
            }

            SendMessage (guild, userJoinActivityChatMessage.ToJson ());
        }

		public void OnClanMemberKick(int loginId, int targetLoginId, bool force)
		{
			var guild = guildManager.GetGuildByLoginId(loginId);

            var error = guildManager.GuildMemberKick(loginId, targetLoginId, force) ? ClientErrors.SUCCESS : ClientErrors.LOGIC_ERROR;

			if (error == ClientErrors.SUCCESS)
			{
                SendRpc(chat.RemoveFromChatChannel, targetLoginId, guild.guildData.ChannelId);

                SendRpc(GetInstanceByLogin(targetLoginId).ResetGuild, targetLoginId);

                SendRpc(rating.RemoveLoginFromClan, guild.guildData, loginId);
			}

            SendRpc(GetInstanceByLogin(loginId).ClanMemberKickAnswer, loginId, targetLoginId, error);

			//Обновить список у всех
			if (error == ClientErrors.SUCCESS)
			{
				guildManager.UpdateMemberData(guild);

				SendRpc(GetInstanceByLogin(targetLoginId).ClanMemberListAnswer, targetLoginId, guild.guildData.ClanId, guild.Persons);

				foreach (var person in guild.Persons)
				{
					SendRpc(GetInstanceByLogin(person.LoginId).SetGuild, person.LoginId, guild.guildData, person.RoleId);
					SendRpc(GetInstanceByLogin(person.LoginId).ClanMemberListAnswer, person.LoginId, guild.guildData.ClanId, guild.Persons);
				}
			}
		}

		public void OnGuildCreate(int loginId, string personName, string avatarName, int ratingScore, GuildData guildData)
		{
			var error = guildManager.CreateGuild(loginId, personName, avatarName, ratingScore, guildData, out var guildId);

			var guild = guildManager.GetGuildById(guildId);
			guildManager.UpdateMemberData(guild);

            
            int createId = transactionIdGenerator++;
            chatCreateTransactions[createId] = (ChannelId, channelType) =>
            {
                guild.guildData.ChannelId = ChannelId;

                //Добавляем в чат клана сообщение об успешном создании
                ClanCreatedActivityChatMessage clanCreatedActivityChatMessage = new ClanCreatedActivityChatMessage () { creatorNickname = personName };
                SendRpc (chat.OnClientSendMessageRequest, 0, ChannelId, channelType, clanCreatedActivityChatMessage.ToJson ());
            };

            if (error == ClientErrors.SUCCESS)
	            SendRpc (chat.CreateGuildChatChannel, createId, ChannelTypes.Guild, loginId);

            SendRpc (GetInstanceByLogin(loginId).GuildCreateAnswer, loginId, error, guild?.guildData);

            if (error == ClientErrors.SUCCESS)
            {
	            var members = guildManager.GetGuildMembers(loginId);
	            SendRpc(rating.OnClanCreate, guildData, members);
            }
		}

        public void OnChatCreated(int transactionId, int channelId, ChannelTypes channelType)
        {
            if (chatCreateTransactions.ContainsKey (transactionId))
            {
                chatCreateTransactions[transactionId].Invoke (channelId, channelType);
                chatCreateTransactions.Remove (transactionId);
            }
            else
            {
                ILogger.Instance.Send ("OnChatCreated. TransactionId " + transactionId + " not exist", ErrorLevel.error);
            }
        }

        public void OnGuildSetJoinStatus(int loginId, int targetLoginId, bool status)
		{
			//если таргета приняли то тут его гильдия. чтобы его уведомить об этом. проверять вместе с результатом. если только SUCCESS и только если принимали а не отклоняли заявку
			var error = guildManager.GuildSetJoinStatus(loginId, targetLoginId, status, out var guild);

			var result = error == ClientErrors.SUCCESS ? guildManager.GetGuildJoinList(loginId) : null;

			if (error == ClientErrors.SUCCESS && status)
				SendRpc(GetInstanceByLogin(targetLoginId).SetGuild, targetLoginId, guild?.guildData, ClanRole.Squire);

			if (error == ClientErrors.SUCCESS)
				SendRpc(GetInstanceByLogin(loginId).GuildJoinListAnswer, loginId, result);

			SendRpc(GetInstanceByLogin(loginId).GuildSetJoinStatusAnswer, loginId, targetLoginId, error);

			if(error == ClientErrors.SUCCESS)
				SendRpc(rating.UpdateGuildData, guild?.guildData, -1);
		}
	}
}
