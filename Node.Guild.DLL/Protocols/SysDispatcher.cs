﻿using System;
using System.Collections.Generic;
using System.Threading;
using Fragoria.Common.Utils;
using NodeGuild.Logic;
using NodeGuild.Utils;
using NSubstitute;
using UtilsLib;
using UtilsLib.low_engine_sharp.transport;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using UtilsLib.Protocol;
using UtilsLib.XmlData;
using Node.Guild.Logic;
using System.Linq;
using Newtonsoft.Json;
using NodeGuild.Logic.Entries;
using UtilsLib.NetworkingData.Chat;
using UtilsLib.NetworkingData.Party;

namespace NodeGuild.Protocols
{
	public sealed partial class SysDispatcher : BaseCmdSystem, IGuildSysDispatcher, IGuildSysInstance, IGuildSysChat, IGuildSysRating
	{
        public SubscribeManager subscribeManager;
        public GuildManager guildManager;
	    public FriendManager friendManager;

		public SysDispatcher(SubscribeManager subscribeManager, GuildManager guildManager, FriendManager friendManager) : base(SystemConfig.sDispatcherCode)
		{
            this.subscribeManager = subscribeManager;
            this.guildManager = guildManager;
            this.friendManager = friendManager;

            this.guildManager.OnMemberLeave += GuildManager_OnClanMemberLeave;
            this.guildManager.OnMemberRoleChanged += GuildManager_OnMemberRoleChanged;
            this.guildManager.OnMemberJoin += GuildManager_OnMemberJoin;

			this.guildManager.OnMemberLeave += (guild, admin, target) => UpdatePersonCardGuild(target, null);
			this.guildManager.OnMemberRoleChanged += (guild, admin, target, oldRole) => UpdatePersonCardGuild(target, guild);
			this.guildManager.OnMemberJoin += (guild, target) => UpdatePersonCardGuild(target, guild);
        }

		private void GuildManager_OnInit()
		{
			ILogger.Instance.Send($"GuildManager_OnInit", ErrorLevel.error);
			var guilds = GuildServer.Instance.GuildManager.GetGuilds();

			foreach (var guild in guilds)
			{
				var emblem = JsonConvert.SerializeObject(guild.guildData.EmblemData);
				ILogger.Instance.Send($"GuildManager_OnInit guild: {guild.guildData.ClanId} emblem: {emblem}", ErrorLevel.error);
				SendRpc(rating.OnClanCreate, guild.guildData, guild.Persons);
			}
		}

		public Connection DsConnection { get; set; }

		public override void MakeCommandsTable()
		{
			FillCommand<IGuildSysDispatcher>();
			FillCommand<IGuildSysInstance>();
            FillCommand<IGuildSysChat>();
            FillCommand<IGuildSysRating>();
		}

		public void ReconnectToDispatcher()
		{
			DsConnection = ConnectToDispatcher();
		}

		private static int pingNumber;
		public void DoPing()
		{
			if (DsConnection != null)
			{
				SendRpc(dispatcher.OnPing, pingNumber);
				pingNumber = (pingNumber + 1) % 100;
			}
		}

		public void OnPing(int ping)
		{
			//приняли пинг. он нам нафиг не нужен, но примем)))
		}

		public readonly IDispatcherSysGuild dispatcher = Substitute.For<IDispatcherSysGuild>();
        public readonly IChatSysGuild chat = Substitute.For<IChatSysGuild>();
        public readonly IRatingSysGuild rating = Substitute.For<IRatingSysGuild>();
        public readonly IDispatcherSysTranslate translate = Substitute.For<IDispatcherSysTranslate>();

		private readonly AutoResetEvent connectEvent = new AutoResetEvent(false);
		private string dispatcherTicket = string.Empty;
		Connection tempConnection;

		public Connection ConnectToDispatcher()
		{
			if (DsConnection != null)
			{
				transport.Disconnect(DsConnection, DisconnectReason.UserRequest);
			}

			tempConnection = Connection.Connect(SystemConfig.DispatcherServer, SystemConfig.DispatcherServerPort);
			if (tempConnection == null)
				return null;

			dispatcherTicket = string.Empty;

			tempConnection.dos_border = -1;
			tempConnection.DestinationType = DestinationType.Server;
			tempConnection.SetCanBufferSendOperation(false);

			FillRPCByMethod(dispatcher, tempConnection);
			FillRPCByMethod(translate, tempConnection);
            FillRPCByMethod(chat, (rpc, param) =>
            {
                //отправим на чат через диспетчер
                SendRpc(translate.OnTranslate, new TranslateData
                {
                    nodeType = NodeType.Chat,
                    sSysCommand = rpc.sSysCommand,
                    data = TranslateData.PackData(rpc, param)
                });
            });

            FillRPCByMethod(rating, (rpc, param) =>
            {
	            //отправим на рейтинг через диспетчер
	            SendRpc(translate.OnTranslate, new TranslateData
	            {
		            nodeType = NodeType.Rating,
		            sSysCommand = rpc.sSysCommand,
		            data = TranslateData.PackData(rpc, param)
	            });
            });

			//отправить туда запрос на тикет. получить тикет.
			//отправить запрос на авторизацию. получить авторизацию.
			SendRpc(dispatcher.OnGetTicket);
			if (!connectEvent.WaitOne(SystemConfig.iMillisecondsRecieveTimeout, false))
			{
                ILogger.Instance.Send("SysDispatcher don't respond for GET TICKET command, ip = " + SystemConfig.DispatcherServer + " port = " + SystemConfig.DispatcherServerPort, ErrorLevel.error);
				return null;
			}

			//отсылаем имя и закрытый ключ сервер
			SendRpc(dispatcher.OnGetAuth, ByteArray.Encode(SystemConfig.sServerSecretKey, dispatcherTicket));
			if (!connectEvent.WaitOne(SystemConfig.iMillisecondsRecieveTimeout, false))
			{
                ILogger.Instance.Send("SysDispatcher don't respond for GET AUTH command, ip = " + SystemConfig.DispatcherServer + " port = " + SystemConfig.DispatcherServerPort, ErrorLevel.error);
				return null;
			}

			ILogger.Instance.Send("SysDispatcher complete connect to dispatcher");

			GuildManager_OnInit();

			return tempConnection;
		}

		public void OnGetTicket(string ticket, Connection sender)
		{
			if (sender == tempConnection)
			{
				dispatcherTicket = ticket;
				connectEvent.Set();
			}
		}

		public void OnGetAuth(bool auth, Connection sender)
		{
			if (sender == tempConnection)
			{
				connectEvent.Set();
			}
		}

		#region TranslateToInstance

		private class InstanceWithLogin
		{
			public bool Register;
			public int LoginId;
			public readonly IInstanceSysGuild<Connection> ServerRpcLisner = Substitute.For<IInstanceSysGuild<Connection>>();
			public static readonly CommandRingBuffer<InstanceWithLogin> Buffer = new CommandRingBuffer<InstanceWithLogin>(50);
		}

		//Это одноразовый инстанс, две команды не отправлять!
		private IInstanceSysGuild<Connection> GetInstanceByLogin(int loginId)
		{
			var result = InstanceWithLogin.Buffer.Get();
			result.LoginId = loginId;

			if (!result.Register)
				FillRPCByMethod(result.ServerRpcLisner, (rpc, param) =>
				{
					//отправим на инстанс через диспетчер
					SendRpc(translate.OnTranslate, new TranslateData
					{
						nodeType = NodeType.InstanceServer,
						sSysCommand = rpc.sSysCommand,
						loginId = result.LoginId,
						data = TranslateData.PackData(rpc, param)
					});
					InstanceWithLogin.Buffer.Put(result);
				});

			result.Register = true;
			return result.ServerRpcLisner;
		}

		#endregion

		public void OnSetFriendGroupReadyStatus(InstancePersonData person, string mapName, string mapParam)
		{
			ILogger.Instance.Send("call OnSetFriendGroupReadyStatus");

            var onArena = subscribeManager.ArenaPersonCheck(person, out var arenaGroup);

			SendRpc(dispatcher.OnSetFriendGroupReadyStatus, person, mapName, mapParam, onArena, arenaGroup);
		}

		public void OnXmlChanged(string fileName, string data)
        {
            switch (fileName)
            {
                case "arenaData.xml":
                    OnArenaXmlChanged(data);
                    break;
                case "remoteconfig.xml":
                    OnRemoteXmlChanged(data);
                    break;
            }
        }

        private void OnArenaXmlChanged(string data)
        {
            try
            {
                string decodedata = ZipUtils.GetUnZipBase64DecodeXorString(data, string.Empty);
                //m_arenaAlgorithm.SetXmlData(decodedata.LoadFromStringXml<ArenaAlgorithmData>());
                subscribeManager.SetArenaAlgorithmData(decodedata.LoadFromStringXml<ArenaAlgorithmData>());
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("Error in OnXmlChanged (arenaData.xml):" + ex, ErrorLevel.exception);
            }
        }

        private void OnRemoteXmlChanged(string data)
        {
            try
            {
                string decodedata = ZipUtils.GetUnZipBase64DecodeXorString(data, string.Empty);
                GuildServer.Instance.RemoteXmlConfig = decodedata.LoadFromStringXml<RemoteXmlConfig>();
                GuildServer.Instance.ESportDataSender.Init(GuildServer.Instance.RemoteXmlConfig);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("Error in OnXmlChanged (remoteconfig.xml):" + ex, ErrorLevel.exception);
            }
        }

        public void OnLoginListToPeerRequest(string ticket, int gameId, string levelName, string levelParam, List<InstancePersonData> persons)
		{
			string instanceData;
			var guilds = new List<GuildData>();
            if (subscribeManager.CheckExistArena(levelParam, out instanceData))
            {
                for (int i = 0; i < persons.Count; i++)
                {
                    int loginId = persons[i].loginId;
                    var guild = guildManager.GetGuildByLoginId(loginId);
	                guildManager.UpdateMemberData(guild);

					persons[i].GuildId = guild == null ? -1 : guild.guildData.ClanId;
					persons[i].GuildRole = guild == null ? ClanRole.None : guild.GetRole(loginId);

                    //if (i == 0)
                    //{
                    //    instanceData = getInstanceData(levelName, guild, ticket, gameId);
                    //}

                    if (guild != null && !guilds.Contains(guild.guildData))
                        guilds.Add(guild.guildData);
                }
            }
            else
            {
                for (int i = 0; i < persons.Count; i++)
                {
                    int loginId = persons[i].loginId;
                    var guild = guildManager.GetGuildByLoginId(loginId);
	                guildManager.UpdateMemberData(guild);

					persons[i].GuildId = guild == null ? -1 : guild.guildData.ClanId;
					persons[i].GuildRole = guild == null ? ClanRole.None : guild.GetRole(loginId);

                    if (i == 0)
                    {
                        instanceData = string.Empty;
                    }

                    if (guild != null && !guilds.Contains(guild.guildData))
                        guilds.Add(guild.guildData);
                }
            }

            ILogger.Instance.Send($"Send OnLoginListToPeer levelName = {levelName} gameId = {gameId} personsCount = {persons.Count} instanceData = {instanceData}");

            SendRpc(dispatcher.OnLoginListToPeer, ticket, gameId, levelName, levelParam, persons, guilds, instanceData);
		}

		public void OnUpdateLoginRequest(string ticket, int loginId)
		{
			var guild = guildManager.GetGuildByLoginId(loginId);
			guildManager.UpdateMemberData(guild);

			CreateChatChannelIfNotExist(guild);

			SendRpc(dispatcher.OnUpdateLogin, ticket, loginId, guild != null, guild?.guildData, guild?.GetRole(loginId) ?? ClanRole.None);
		}

		public void OnAddPersonToGameRequest(string ticket, int loginId)
        {
			var guild = guildManager.GetGuildByLoginId(loginId);
			guildManager.UpdateMemberData(guild);

			CreateChatChannelIfNotExist(guild);

			SendRpc(dispatcher.OnAddPersonToGame, ticket, loginId, guild != null, guild?.guildData, guild?.GetRole (loginId) ?? ClanRole.None);
        }

        private void CreateChatChannelIfNotExist(Logic.Entries.Guild guild)
        {
			if (guild == null)
				return;

            if (guild.guildData.IsNeedCreateChannel)
            {
                guild.guildData.OnChatChannelCreateStarted ();
                List<int> loginIds = guild.Persons.Select (x => x.LoginId).ToList ();

                int createId = transactionIdGenerator++;
                chatCreateTransactions[createId] = (ChannelId, channelType) =>
                {
                    guild.guildData.ChannelId = ChannelId;
                };
                SendRpc (chat.LoadGuildChatChannel, createId, ChannelTypes.Guild, loginIds);
            }
        }

        public void SendNewMailRequest(int loginId, int personId)
		{
			SendRpc(dispatcher.OnSendNewMailRequest, loginId, personId);
		}

		public void OnLoginPing(int loginId, int personId)
        {
            subscribeManager.LoginPing(loginId, personId);
            guildManager.LoginPing(loginId);
		}

        public void OnArenaMatchTeleportRequest(int loginId, int personId)
        {
            subscribeManager.Teleport(loginId, personId);
        }

        public void OnArenaMatchRequest(int loginId, int personId, int ratingTeam, int ratingCoop, int heroPower, int level, PersonType personType, int matchButtonId, bool onlyBot)
        {
            ILogger.Instance.Send($"OnArenaMatchRequest loginId = {loginId} personId = {personId} ratingTeam = {ratingTeam} ratingCoop = {ratingCoop} heroPower = {heroPower} level = {level} personType = {personType} matchButtonId = {matchButtonId} onlyBot={onlyBot}");
            long current = XmlUtils.GetEpochTimeSec();

            int rating = ratingTeam;
            var matchData = subscribeManager.arenaAlgorithmData.GetMatchData(matchButtonId);
            if (matchData != null)
            {
                //ILogger.Instance.Send($"MatcdData not found matchButtonId = {matchButtonId}");
                var groupType = matchData.ArenaGroupType;
                if (groupType == ArenaGroupType.Coop)
                    rating = ratingCoop;
            }

            ArenaPersonSubscribeData apsd = new ArenaPersonSubscribeData() { lastOnline = current, LoginId = loginId, PersonId = personId, Rating = rating, ArenaMatchButtonId = matchButtonId, HeroPower = heroPower, Level = level, PersonType = personType, OnlyBot = onlyBot };

//#warning если есть группа - (поискать) - то не надо подписывать никуда.. сразу вернуть инфу о текущей группе.
            ArenaGroup g = subscribeManager.GetArenaGroup(apsd);
            if (g != null)
            {
                ArenaPersonSubscribeData p = g.getPerson(apsd.PersonId);
                if (!p.bTeleport)
                    SendGroupInfo(g, p);

                if (g.started)
                    SendTeleportToArenaBattle(g, new List<ArenaPersonSubscribeData>() { p });
                return;
            }

            //ArenaGroup arenaGroup = null;
            List<int> subscribedToMatchTypes = subscribeManager.Subscribe(apsd, matchButtonId, current);

			var instance = GetInstanceByLogin(loginId);
            //На самом деле список тут не нужен, но пока оставил
			SendRpc(instance.ArenaDataResponse, loginId, personId, subscribedToMatchTypes);

            //if (arenaGroup != null)
            //    SendGroupInfoToAll(arenaGroup);
        }

        internal void SendGroupInfo(ArenaGroup arenaGroup, ArenaPersonSubscribeData person)
        {
            long timeToStart = Math.Max(0, arenaGroup.startTime - XmlUtils.GetEpochTimeSec());

			var instance = GetInstanceByLogin(person.LoginId);
			SendRpc(instance.ArenaMatchReadyResponse, person.LoginId, person.PersonId, arenaGroup.matchButtonId, timeToStart);
        }

        internal void SendGroupInfoToAll(ArenaGroup arenaGroup)
        {
	        long timeToStart = Math.Max(0, arenaGroup.startTime - XmlUtils.GetEpochTimeSec());
			foreach (var person in arenaGroup.persons)
			{
				var instance = GetInstanceByLogin(person.LoginId);
				SendRpc(instance.ArenaMatchReadyResponse, person.LoginId, person.PersonId, arenaGroup.matchButtonId, timeToStart);
			}
        }

        internal void StartInstanceForGroup(ArenaGroup arenaGroup)
        {
	        SendRpc(dispatcher.OnStartInstanceForArena, arenaGroup.MapName, arenaGroup.uniq, arenaGroup.uniq);
        }

		public void OnStartInstanceForArena(string groupUniq)
		{
			var group = subscribeManager.GetArenaGroup(groupUniq);
			if (group == null)
			{
				ILogger.Instance.Send($"SysDispatcher.OnStartInstanceForArena(): not found group by uniq {groupUniq}", ErrorLevel.error);
				return;
			}

			long currentTime = XmlUtils.GetEpochTimeSec();
			group.InitStartEndTime(currentTime, group.GameTimeSec);

			ILogger.Instance.Send($"SysDispatcher.OnStartInstanceForArena(): group uniq {groupUniq}", ErrorLevel.info);
			SendGroupInfoToAll(group);
		}

		internal void SendTeleportToArenaBattle(ArenaGroup arenaGroup, List<ArenaPersonSubscribeData> persons)
		{
			foreach (var person in persons)
			{
				var instance = GetInstanceByLogin(person.LoginId);
				SendRpc(instance.TeleportToArena, person.LoginId, person.PersonId, arenaGroup.matchButtonId, arenaGroup.MapName, arenaGroup.uniq);
			}
		}

        public void ArenaGameEndFor(int personId)
        {
            subscribeManager.ArenaGameEndFor(personId);
		}

		public void EsportData(ESportGameStatus gameStatus, string postData)
        {
            GuildServer.Instance.ESportDataSender.Send(gameStatus, postData);
        }

		public void RemoveArenaGroup(string groupUniq)
	    {
            subscribeManager.RemoveArenaGroup(groupUniq);
        }

		public void OnChatStarted()
		{
			guildManager.ResetGuildListChatInfo();
		}

		private int MaxPartyId = 1;

		private Dictionary<int, PlayModeData> PlayModeDatas = new Dictionary<int, PlayModeData>();
		private Dictionary<string, PlayModeData> PlayModeDatasByCode = new Dictionary<string, PlayModeData>();

		private PlayModeData CreatePartyData(int аrenaButtonId, PlayModeType playModeType, PersonCard personCard, SelectedPersonData selectedPersonData)
		{
			var partyMemberData = new PartyMemberData();
			partyMemberData.isReady = false;
			partyMemberData.isLeader = true;
			partyMemberData.slotIndex = 0;
			partyMemberData.personCard = personCard;
			partyMemberData.selectedCharacterData = selectedPersonData;

			var partyId = MaxPartyId++;
			var party = new PartyData
			{
				Type = playModeType,
				partyId = partyId,
				partyCode = partyId.ToString(),
				partyMemberDatas = new List<PartyMemberData> { partyMemberData }
			};

			//Создаём канал чата для группы
			int createId = transactionIdGenerator++;
            chatCreateTransactions[createId] = (ChannelId, channelType) =>
            {
                party.channelId = ChannelId;

                //Добавляем в чат группы сообщение об успешном создании
                var teamCreatedActivityChatMessage = new TeamCreatedActivityChatMessage { creatorNickname = personCard.name };
                SendRpc (chat.OnClientSendMessageRequest, 0, ChannelId, channelType, teamCreatedActivityChatMessage.ToJson ());
            };
            SendRpc (chat.CreateGuildChatChannel, createId, party.GetRequiredChannelType(), personCard.loginId);

			var partyModeData = new PlayModeData {ArenaButtonId = аrenaButtonId, partyData = party};
			PlayModeDatas[partyId] = partyModeData;
			PlayModeDatasByCode[party.partyCode] = partyModeData;

			return partyModeData;
		}

		private Dictionary<int, LoginPlayData> LoginPlayDatas = new Dictionary<int, LoginPlayData>();

		public class LoginPlayData
		{
			public PersonCard PersonCard;
			public SelectedPersonData SelectedPersonData;
			public Dictionary<PlayModeType, PlayModeData> PlayModeDatas = new Dictionary<PlayModeType, PlayModeData>();
		}

		public void AllPlayModeDatasRequest(int loginId, PersonCard personCard, SelectedPersonData selectedPersonData, int аrenaButtonId)
		{
			if (!LoginPlayDatas.TryGetValue(loginId, out var loginPlayData))
			{
				LoginPlayDatas[loginId] = loginPlayData = new LoginPlayData();

				loginPlayData.PlayModeDatas[PlayModeType.RatingParty] = CreatePartyData(аrenaButtonId, PlayModeType.RatingParty, personCard, selectedPersonData);
				loginPlayData.PlayModeDatas[PlayModeType.CustomParty] = CreatePartyData(аrenaButtonId, PlayModeType.CustomParty, personCard, selectedPersonData);
				loginPlayData.PlayModeDatas[PlayModeType.Squad] = CreatePartyData(аrenaButtonId, PlayModeType.Squad, personCard, selectedPersonData);
			}

			loginPlayData.PersonCard = personCard;
			loginPlayData.SelectedPersonData = selectedPersonData;

			var instance = GetInstanceByLogin(loginId);
			SendRpc(instance.AllPlayModeDatasResponse, loginId, loginPlayData.PlayModeDatas);
		}

		public void ChangeArenaTypeRequest(int loginId, PlayModeType playModeType, int newArenaButtonId)
		{
			if (!LoginPlayDatas.TryGetValue(loginId, out var loginPlayData))
				return;

			if (!loginPlayData.PlayModeDatas.TryGetValue(playModeType, out var playModeData))
				return;

			var memberData = playModeData.partyData.partyMemberDatas.FirstOrDefault(x => x.personCard.loginId == loginId);
			if (memberData == null)
				return;

			if (!memberData.isLeader)
			{
				ILogger.Instance.Send("ChangeArenaTypeRequest: только лидер может менять арену!", ErrorLevel.warning);
				return;
			}

			//Поменять
			playModeData.ArenaButtonId = newArenaButtonId;

			//Разослать всем
			foreach (var member in playModeData.partyData.partyMemberDatas)
			{
				if (loginId == member.personCard.loginId)
					continue;

				var instance = GetInstanceByLogin(member.personCard.loginId);
				SendRpc(instance.UpdateArenaTypeResponse, member.personCard.loginId, playModeData.partyData.partyId, newArenaButtonId);
			}
		}

		public void ChangeReadyStatusRequest(int loginId, PlayModeType playModeType, bool readyStatus)
		{
			if (!LoginPlayDatas.TryGetValue(loginId, out var loginPlayData))
				return;

			if (!loginPlayData.PlayModeDatas.TryGetValue(playModeType, out var playModeData))
				return;

			var loginMemberData = playModeData.partyData.partyMemberDatas.FirstOrDefault(x => x.personCard.loginId == loginId);
			if (loginMemberData == null)
				return;

			loginMemberData.isReady = readyStatus;

			//Обновим у всех остальных
			foreach (var memberData in playModeData.partyData.partyMemberDatas)
				//if (loginId != memberData.personCard.loginId)
				{
					var instance2 = GetInstanceByLogin(memberData.personCard.loginId);
					SendRpc(instance2.ReadyStatusResponse, memberData.personCard.loginId, playModeData.partyData.partyId, loginId, readyStatus);
				}

			//Запуск игры
			if (playModeData.partyData.partyMemberDatas.All(x => x.isReady))
			{
				var persons = new List<ArenaPersonSubscribeData>();
				foreach (var memberData in playModeData.partyData.partyMemberDatas)
				{
					memberData.isReady = false;

					persons.Add(new ArenaPersonSubscribeData
					{
						//lastOnline = current,
						LoginId = memberData.personCard.loginId,
						PersonId = memberData.selectedCharacterData.personId,
						//Rating = rating,
						//ArenaMatchButtonId = matchButtonId,
						//HeroPower = heroPower,
						//Level = level,
						//PersonType = personType,
						//OnlyBot = onlyBot
					});
				}

				var gameInfo = subscribeManager.arenaAlgorithmData.GetArenaGameInfo(playModeData.ArenaButtonId).Random();

				long currentEpochTimeSec = XmlUtils.GetEpochTimeSec();
				var newGroup = subscribeManager.MakeGroup(persons, gameInfo, playModeData.ArenaButtonId, currentEpochTimeSec);
				subscribeManager.AddGroup(newGroup);
				StartInstanceForGroup(newGroup);
			}
		}

		private int MaxInviteId = 1;

		public void PartyInviteRequest(int loginId, int partyId, int inviteLoginId)
		{
			if (!LoginPlayDatas.TryGetValue(loginId, out var loginPlayData))
				return;

			var playModeData = loginPlayData.PlayModeDatas.FirstOrDefault(x => x.Value.partyData.partyId == partyId).Value;
			if (playModeData == null)
				return;

			var partyData = playModeData.partyData;

			var memberData = partyData.partyMemberDatas.FirstOrDefault(x => x.personCard.loginId == loginId);
			if (memberData == null)
				return;

			if (!memberData.isLeader)
			{
				ILogger.Instance.Send("PartyInviteRequest: только лидер может приглашать!", ErrorLevel.warning);
				return;
			}

			int inviteId = MaxInviteId++;
			var partyInviteData = new PartyInviteData
			{
				inviteId = inviteId,
				targetPartyId = partyId,
				targetLoginId = loginId,
				inviterElo = memberData.selectedCharacterData.rating,
				inviterPersonCard = memberData.personCard,
				playModeType = partyData.Type
			};

			var instance = GetInstanceByLogin(inviteLoginId);
			SendRpc(instance.PartyInviteResponse, inviteLoginId, partyInviteData);
		}

		public void PartyInviteAcceptRequest(int loginId, int partyId)
		{
			var inviteResult = PlayModeDatas.TryGetValue(partyId, out var playModeData) && JoinParty(playModeData, loginId);

			var instance = GetInstanceByLogin(loginId);
			SendRpc(instance.PartyInviteAcceptResponse, loginId, inviteResult);
		}

		private bool JoinParty(PlayModeData playModeData, int loginId)
		{
			var party = playModeData.partyData;

			var slot = 0;
			while (party.partyMemberDatas.Exists(x => x.slotIndex == slot))
				slot++;

			if (!LoginPlayDatas.TryGetValue(loginId, out var loginPlayData))
				return false;

			var partyMemberData = new PartyMemberData();
			partyMemberData.isReady = false;
			partyMemberData.isLeader = false;
			partyMemberData.slotIndex = slot;
			partyMemberData.personCard = loginPlayData.PersonCard;
			partyMemberData.selectedCharacterData = loginPlayData.SelectedPersonData;
			party.partyMemberDatas.Add(partyMemberData);

			//Обновим у чувака
			{
				//Удалим из старой группы
				var oldParty = loginPlayData.PlayModeDatas[party.Type].partyData;

				oldParty.partyMemberDatas.RemoveAll(x => x.personCard.loginId == loginId);
				if (oldParty.partyMemberDatas.Count == 0)
				{
					PlayModeDatas.Remove(oldParty.partyId);
					PlayModeDatasByCode.Remove(oldParty.partyCode);
				}

                //Удалим из чата текущей группы
                SendRpc (chat.RemoveFromChatChannel, loginId, party.channelId);

                //Отправить сообщение в чат группы о выходе игрока
                var userLeaveFromTeamActivityChatMessage = new UserLeaveFromTeamActivityChatMessage()
				{
					leaveUserNickname = partyMemberData.personCard.name
				};
				SendRpc(chat.OnClientSendMessageRequest, 0, party.channelId, party.GetRequiredChannelType(), userLeaveFromTeamActivityChatMessage.ToJson());

				//Обновим у всех остальных
				foreach (var memberData in oldParty.partyMemberDatas)
				{
					var instance3 = GetInstanceByLogin(memberData.personCard.loginId);
					SendRpc(instance3.RemovePlayerFromPartyResponse, memberData.personCard.loginId, oldParty.partyId, loginId);
				}

				//Добавим в новую группу
				loginPlayData.PlayModeDatas[party.Type] = playModeData;

				var instance2 = GetInstanceByLogin(loginId);
				SendRpc(instance2.UpdatePlayModeDataResponse, loginId, party.Type, playModeData);
			}

			//Обновим у всех остальных
			foreach (var memberData in party.partyMemberDatas)
				if (loginId != memberData.personCard.loginId)
				{
					var instance2 = GetInstanceByLogin(memberData.personCard.loginId);
					SendRpc(instance2.AddPlayerToPartyResponse, memberData.personCard.loginId, party.partyId, partyMemberData);
				}

			//Добавляем присоеденившегося игрока в чат группы
			SendRpc(chat.AddToChatChannel, loginId, party.channelId);

			//Отсылаем сообщение в чат группы о том, что игрок присоеденился
			var userJoinToTeamActivityChatMessage = new UserJoinToTeamActivityChatMessage
			{
				joinedUserNickname = partyMemberData.personCard.name
			};
			SendRpc(chat.OnClientSendMessageRequest, 0, party.channelId, party.GetRequiredChannelType(), userJoinToTeamActivityChatMessage.ToJson());

			return true;
		}

		public void RemovePlayerFromPartyRequest(int loginId, int partyId, int removeLoginId)
		{
			if (!PlayModeDatas.TryGetValue(partyId, out var playModeData))
				return;

			var party = playModeData.partyData;

			var adminMember = party.partyMemberDatas.FirstOrDefault(x => x.personCard.loginId == loginId);
			if (adminMember == null)
				return;

			if (loginId != removeLoginId && !adminMember.isLeader)
			{
				ILogger.Instance.Send("RemovePlayerFromPartyRequest: только лидер может удалять других!", ErrorLevel.warning);
				return;
			}

			var removeMember = party.partyMemberDatas.FirstOrDefault(x => x.personCard.loginId == removeLoginId);
			if (removeMember == null)
				return;

			party.partyMemberDatas.Remove(removeMember);
			if (party.partyMemberDatas.Count == 0)
			{
				PlayModeDatas.Remove(party.partyId);
				PlayModeDatasByCode.Remove(party.partyCode);
			}

			//Удалим из чата группы
			SendRpc (chat.RemoveFromChatChannel, removeLoginId, party.channelId);

            if (loginId == removeLoginId)
            {
                //Отправить сообщение в чат группы о выходе игрока
                var userLeaveFromTeamActivityChatMessage = new UserLeaveFromTeamActivityChatMessage ()
                {
                    leaveUserNickname = removeMember.personCard.name
                };
                SendRpc (chat.OnClientSendMessageRequest, 0, party.channelId, party.GetRequiredChannelType (), userLeaveFromTeamActivityChatMessage.ToJson ());
            }
            else
            {
                //Отправить сообщение в чат группы об исключении игрока
                var userRemovedFromTeamActivityChatMessage = new UserRemovedFromTeamActivityChatMessage ()
                {
                    adminNickname = adminMember.personCard.name,
                    targetNickname = removeMember.personCard.name
                };
                SendRpc (chat.OnClientSendMessageRequest, 0, party.channelId, party.GetRequiredChannelType (), userRemovedFromTeamActivityChatMessage.ToJson ());
            }

            //Обновим у чувака
            if (LoginPlayDatas.TryGetValue(removeLoginId, out var loginPlayData))
			{
				var newPlayModeData = CreatePartyData(playModeData.ArenaButtonId, party.Type, removeMember.personCard, removeMember.selectedCharacterData);
				loginPlayData.PlayModeDatas[party.Type] = newPlayModeData;

				var instance2 = GetInstanceByLogin(removeLoginId);
				SendRpc(instance2.UpdatePlayModeDataResponse, removeLoginId, party.Type, newPlayModeData);
			}

			//Обновим у всех остальных
			foreach (var memberData in party.partyMemberDatas)
			{
				var instance2 = GetInstanceByLogin(memberData.personCard.loginId);
				SendRpc(instance2.RemovePlayerFromPartyResponse, memberData.personCard.loginId, partyId, removeLoginId);
			}
		}

		public void UpdatePersonCardRequest(int loginId, string name, string avatar)
		{
			if (!LoginPlayDatas.TryGetValue(loginId, out var loginPlayData))
				return;

			loginPlayData.PersonCard.name = name;
			loginPlayData.PersonCard.avatarIcon = avatar;

			foreach (var playModeData in loginPlayData.PlayModeDatas.Values)
				foreach (var memberData in playModeData.partyData.partyMemberDatas)
				{
					var instance2 = GetInstanceByLogin(memberData.personCard.loginId);
					SendRpc(instance2.UpdatePersonCardResponse, memberData.personCard.loginId, playModeData.partyData.partyId, loginPlayData.PersonCard);
				}
		}

		public void UpdatePersonCardGuild(GuildPersonInfo login, Guild guild)
		{
			int loginId = login.LoginId;
			if (!LoginPlayDatas.TryGetValue(loginId, out var loginPlayData))
				return;

			loginPlayData.PersonCard.clanData = new ShortClanData(guild?.guildData, login.RoleId);

			foreach (var playModeData in loginPlayData.PlayModeDatas.Values)
				foreach (var memberData in playModeData.partyData.partyMemberDatas)
				{
					var instance2 = GetInstanceByLogin(memberData.personCard.loginId);
					SendRpc(instance2.UpdatePersonCardResponse, memberData.personCard.loginId, playModeData.partyData.partyId, loginPlayData.PersonCard);
					SendRpc(rating.UpdateGuildData, guild?.guildData, memberData.personCard.loginId);
				}
		}

		public void UpdateCharacterDataRequest(int loginId, SelectedPersonData selectedPersonData)
		{
			if (!LoginPlayDatas.TryGetValue(loginId, out var loginPlayData))
				return;

			loginPlayData.SelectedPersonData = selectedPersonData;

			foreach (var playModeData in loginPlayData.PlayModeDatas.Values)
			{
				var loginData = playModeData.partyData.partyMemberDatas.FirstOrDefault(x => x.personCard.loginId == loginId);
				if (loginData == null)
					continue;

				loginData.selectedCharacterData = selectedPersonData;

				foreach (var memberData in playModeData.partyData.partyMemberDatas)
				{
					var instance2 = GetInstanceByLogin(memberData.personCard.loginId);
					SendRpc(instance2.UpdateCharacterDataResponse, memberData.personCard.loginId, playModeData.partyData.partyId, loginId, selectedPersonData);
				}
			}
		}

		public void ApplyPartyCodeRequest(int loginId, string partyCode)
		{
			if (!PlayModeDatasByCode.TryGetValue(partyCode, out var playModeData))
				return;

			var instance = GetInstanceByLogin(loginId);
			SendRpc(instance.ApplyPartyCodeResponse, loginId, playModeData.partyData.Type, playModeData);
		}

		public void AcceptPartyTransferRequest(int loginId, int partyId)
		{
			var result = PlayModeDatas.TryGetValue(partyId, out var playModeData) && JoinParty(playModeData, loginId);

			var joinPartyResult = result ? JoinPartyResult.Success : JoinPartyResult.Failed;

			var instance = GetInstanceByLogin(loginId);
			SendRpc(instance.AcceptPartyTransferResponse, loginId, joinPartyResult);
		}

		private int MaxRequestId = 1;
		private Dictionary<int, PartyRequestData> PartyRequestDatas = new Dictionary<int, PartyRequestData>();

		public void PartyRequest(int loginId, int partyId)
		{
			if (!PlayModeDatas.TryGetValue(partyId, out var playModeData))
				return;

			var leader = playModeData.partyData.partyMemberDatas.FirstOrDefault(x => x.isLeader);
			if (leader == null)
				return;

			if (!LoginPlayDatas.TryGetValue(loginId, out var loginPlayData))
				return;

			var partyRequestData = new PartyRequestData
			{
				requestId = MaxRequestId++,
				targetPartyId = playModeData.partyData.partyId,
				partyLeaderLoginId = leader.personCard.loginId,
				candidateLoginId = loginId,
				candidateElo = loginPlayData.SelectedPersonData.rating,
				candidatePersonCard = loginPlayData.PersonCard,
				playModeType = playModeData.partyData.Type,
				partyRequestResult = PartyRequestResult.Created
			};

			PartyRequestDatas[partyRequestData.requestId] = partyRequestData;

			var instance = GetInstanceByLogin(leader.personCard.loginId);
			SendRpc(instance.PartyRequestResponse, leader.personCard.loginId, partyRequestData);
		}

		public void PartyRequestResultRequest(int loginId, int requestId, PartyRequestResult result)
		{
			if (!PartyRequestDatas.TryGetValue(requestId, out var partyRequestData))
				return;

			if (partyRequestData.partyLeaderLoginId != loginId)
				return;

			partyRequestData.partyRequestResult = result;

			if (result == PartyRequestResult.Accepted)
			{
				//Добавим в группу
				var joinResult = PlayModeDatas.TryGetValue(partyRequestData.targetPartyId, out var playModeData) && JoinParty(playModeData, loginId);
			}

			PartyRequestDatas.Remove(requestId);

			var instance = GetInstanceByLogin(partyRequestData.candidateLoginId);
			SendRpc(instance.PartyRequestResultResponse, partyRequestData.candidateLoginId, partyRequestData);
		}

		public void RatingStarted(int id)
		{
			GuildManager_OnInit();
		}

		public void SendValue(Dictionary<long, int> places)
		{
			guildManager.UpdateGuildsData(places);
		}
	}
}
