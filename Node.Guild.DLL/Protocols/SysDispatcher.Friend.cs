using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Networking.ClientDataRPC;
using UtilsLib.NetworkingData;

namespace NodeGuild.Protocols
{
	public sealed partial class SysDispatcher
	{
		private FriendData GetFriendData(GuildPersonInfo info, GuildData clanData, FriendshipReason friendshipReason, FriendshipStatus friendshipStatus, int gameCount = 0)
		{
			return new FriendData
			{
				LoginId = info.LoginId,
				PersonName = info.PersonName,
				AvatarName = info.AvatarName,
				RatingScore = info.RatingScore,
				WasOnline = guildManager.GetLoginPing(info.LoginId), //info.WasOnline,
				FlagIconName = info.FlagIconName,
				Gender = PlayerGender.Male, //TODO_Deg ???
				ClanData = clanData ?? new GuildData(),
				ClanRole = info.RoleId,
				FriendshipReason = friendshipReason,
				PlayedGamesTogether = gameCount,
				FriendshipStatus = friendshipStatus
			};
		}

		public void OnFriendList(int loginId, List<int> friendListId, Dictionary<int, int> playTogether, List<int> inviteListId, List<int> requestListId,
			int maxFriendCount, int maxIncomeRequestCount)
		{
			var friendList = new List<FriendData>();
			foreach (var friendId in friendListId)
			{
				var friendClan = guildManager.GetGuildByLoginId(friendId);
				var friendInfo = friendClan?.GetPerson(friendId) ?? friendManager.GetPerson(friendId);
				if (friendInfo != null)
					friendList.Add(GetFriendData(friendInfo, friendClan?.guildData, FriendshipReason.None, FriendshipStatus.Friend));
			}

			//Возможные друзья, cоклановцы
			var possibleFriendList = new List<FriendData>();
			var clan = guildManager.GetGuildByLoginId(loginId);
			if (clan != null)
				foreach (var clanPerson in clan.Persons)
				{
					if (clanPerson.LoginId == loginId || friendListId.Contains(clanPerson.LoginId))
						continue;

					if (inviteListId.Contains(clanPerson.LoginId) || requestListId.Contains(clanPerson.LoginId))
						continue;

					possibleFriendList.Add(GetFriendData(clanPerson, clan.guildData, FriendshipReason.TheSameClan, FriendshipStatus.PossibleFriend));
				}

			//Играли вместе
			foreach (var kvp in playTogether)
			{
				var friendId = kvp.Key;
				var gameCount = kvp.Value;
				if (friendListId.Contains(friendId) || requestListId.Contains(friendId))
					continue;

				var friendClan = guildManager.GetGuildByLoginId(friendId);
				if (friendClan == clan)
					continue;

				var friendInfo = friendClan?.GetPerson(friendId) ?? friendManager.GetPerson(friendId);
				possibleFriendList.Add(GetFriendData(friendInfo, friendClan?.guildData, FriendshipReason.PlayTogether, FriendshipStatus.PossibleFriend, gameCount));
			}

			var requestFriendList = new List<FriendData>();
			foreach (var friendId in requestListId)
			{
				if (friendListId.Contains(friendId))
					continue;

				var friendClan = guildManager.GetGuildByLoginId(friendId);
				var friendInfo = friendClan?.GetPerson(friendId) ?? friendManager.GetPerson(friendId);
				if (friendInfo != null)
				{
					int gameCount = 0;
					var reason = FriendshipReason.None;
					if (friendClan == clan)
						reason = FriendshipReason.TheSameClan;
					else if(playTogether.TryGetValue(friendId, out gameCount))
						reason = FriendshipReason.PlayTogether;

					requestFriendList.Add(GetFriendData(friendInfo, friendClan?.guildData, reason, FriendshipStatus.RequestFriend, gameCount));
				}
			}

			friendList.Sort((x, y) => y.RatingScore - x.RatingScore);
			possibleFriendList.Sort((x, y) => y.RatingScore - x.RatingScore);
			requestFriendList.Sort((x, y) => y.RatingScore - x.RatingScore);

			//if (possibleFriendList.Count > maxFriendCount)
			//	possibleFriendList.RemoveRange(maxFriendCount, possibleFriendList.Count - maxFriendCount);

			if (requestFriendList.Count > maxIncomeRequestCount)
				requestFriendList.RemoveRange(maxIncomeRequestCount, requestFriendList.Count - maxIncomeRequestCount);

			SendRpc(GetInstanceByLogin(loginId).FriendListAnswer, loginId, friendList, possibleFriendList, requestFriendList);
		}

		public void OnFriendInvite(int loginId, int loginInviteId)
		{
			SendRpc(GetInstanceByLogin(loginId).FriendInviteAnswer, loginId, loginInviteId);
		}

		public void OnFriendAnswer(int loginId, int loginAnswerId, bool accept)
		{
			SendRpc(GetInstanceByLogin(loginId).FriendAnswerAnswer, loginId, loginAnswerId, accept);
		}

		public void OnFriendRemove(int loginId, int loginRemoveId)
		{
			SendRpc(GetInstanceByLogin(loginId).FriendRemoveAnswer, loginId, loginRemoveId);
		}

		public void OnFriendDeepLinkInvite(int loginId, int loginDeepLinkId)
		{
			SendRpc(GetInstanceByLogin(loginId).FriendDeepLinkInviteAnswer, loginId, loginDeepLinkId);
		}
	}
}
