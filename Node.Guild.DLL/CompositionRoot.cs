﻿using System;
using System.Collections.Specialized;
using Assets.Scripts.Utils.Logger;
using NodeGuild;
using NodeGuild.Utils;
using UtilsLib;

namespace Node.Guild.DLL
{
    public class CompositionRoot
    {
        public static void Init(NameValueCollection appSettings, out GuildServer server)
        {
            FRRandom.Init();

            var config = new SystemConfig(appSettings);
            ILogger.Instance = new AsyncLogger(config, new CommonLogger(config));
            ILogger.Instance.Init();
            ILogger.Instance.AddTag("srvType", "Guild");

            server = new GuildServer();
            GuildServer.Instance = server;
        }
    }
}
