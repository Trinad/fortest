﻿using NodeGuild.DB;
using NodeGuild.Logic.Entries;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;

namespace NodeGuild.Logic
{
	public class GuildManager
	{
#warning вынести эту цифру в настройки. или получить в команде...
		public int ClanSize = 100;
		public bool PermanentDelete = false;

        public event Action<Guild, GuildPersonInfo> OnMemberJoin;
        public event Action<Guild, GuildPersonInfo, GuildPersonInfo> OnMemberLeave;
        public event Action<Guild, GuildPersonInfo, GuildPersonInfo, ClanRole> OnMemberRoleChanged;

		public void Init()
		{
			GuildList = DBStoreManager.GetAdapter().ClansLoad();
			for (var i = 0; i < GuildList.Count; i++)
			{
				var g = GuildList[i];
				GuildNames.Add(g.guildData.ClanName);
				GuildById[g.guildData.ClanId] = g;

				foreach (var p in g.Persons)
				{
					LastPings[p.LoginId] = TimeProvider.UTCNow.AddSeconds(-p.WasOnline);
					GuildIdByLoginId[p.LoginId] = g.guildData.ClanId;
				}

				for (var j = 0; j < g.Invites.Count; j++)
				{
					var guildInvite = g.Invites[j];
					if (!InviteByKey.ContainsKey(guildInvite.loginId))
						InviteByKey[guildInvite.loginId] = new List<GuildInvite>();
					InviteByKey[guildInvite.loginId].Add(guildInvite);
				}

				for (var j = 0; j < g.Joins.Count; j++)
				{
					var guildJoin = g.Joins[j];
					if (!JoinByKey.ContainsKey(guildJoin.loginId))
						JoinByKey[guildJoin.loginId] = new List<GuildJoin>();
					JoinByKey[guildJoin.loginId].Add(guildJoin);
				}
			}
		}

		private readonly object m_guildListLock = new object();
		private readonly Dictionary<int, int> GuildIdByLoginId = new Dictionary<int, int>();
		private readonly Dictionary<int, Guild> GuildById = new Dictionary<int, Guild>();
		private readonly HashSet<string> GuildNames = new HashSet<string>();
		private List<Guild> GuildList;
		private readonly Dictionary<int, List<GuildJoin>> JoinByKey = new Dictionary<int, List<GuildJoin>>();
		private readonly Dictionary<int, List<GuildInvite>> InviteByKey = new Dictionary<int, List<GuildInvite>>();

		public void ResetGuildListChatInfo()
		{
			lock (m_guildListLock)
			{
				foreach (var g in GuildList)
				{	
					g.guildData.ChannelId = -1;
				}
			}
		}

		// возвращает копию списка, творите что хотите
		public List<Guild> GetGuilds()
		{
			lock (m_guildListLock)
			{
				return new List<Guild>(GuildList);
			}
		}

		public List<Guild> SearchGuilds(string name)
		{
			name = name.ToLower();
			lock (m_guildListLock)
			{
				return GuildList.Where(x => x.guildData.ClanName.ToLower().Contains(name)).ToList();
			}
		}

		internal ClientErrors CreateGuild(int loginId, string personName, string avatarName, int ratingScore, GuildData gd, out int guildId)
		{
			guildId = -1;

			if (GuildNames.Contains(gd.ClanName))
			{
				ILogger.Instance.Send($"GuildManager.CreateGuild(): guild name {gd.ClanName} already exists", ErrorLevel.error);
				return ClientErrors.GUILD_NAME_EXIST_ERROR;
			}

			if (GuildIdByLoginId.ContainsKey(loginId))
			{
				ILogger.Instance.Send($"GuildManager.CreateGuild(): person {personName} ({loginId}) already in guild", ErrorLevel.error);
				return ClientErrors.ALREADY_IN_GUILD_ERROR;
			}

			RemoveJoins(loginId);
			RemoveInvite(loginId);

			var guild = new Guild {guildData = gd, LeaderId = loginId};
			guild.AddPerson(loginId, personName, avatarName, ClanRole.Commander, ratingScore);

			var clanId = DBStoreManager.GetAdapter().ClanCreate(guild);
			guild.bGetFromDB = true;

			guild.guildData.ClanId = clanId;
			GuildById[guild.guildData.ClanId] = guild;
			GuildNames.Add(gd.ClanName);
			GuildIdByLoginId[loginId] = gd.ClanId;
			lock (m_guildListLock)
				GuildList.Add(guild);

			guildId = gd.ClanId;

			ILogger.Instance.Send($"GuildManager.CreateGuild(): person {personName} ({loginId}) created guild {gd}");

			return ClientErrors.SUCCESS;
		}

		private void removeGuild(Guild g)
		{
			GuildById.Remove(g.guildData.ClanId);
			GuildNames.Remove(g.guildData.ClanName);
			lock (m_guildListLock)
				GuildList.Remove(g);

			removeJoinAndInvites(g);
		}

		private void removeJoinAndInvites(Guild g)
		{
			//удалить все инвайты в эту гильдию и все приглашения.
			for (var i = 0; i < g.Joins.Count; i++)
				if (JoinByKey.ContainsKey(g.Joins[i].loginId))
					JoinByKey[g.Joins[i].loginId].Remove(g.Joins[i]);

			for (var i = 0; i < g.Invites.Count; i++)
				if (InviteByKey.ContainsKey(g.Invites[i].loginId))
					InviteByKey[g.Invites[i].loginId].Remove(g.Invites[i]);
		}

		private GuildInvite GetInvite(int loginId, Guild guild)
		{
			if (!InviteByKey.TryGetValue(loginId, out var invites))
				return null;

			if (invites == null || invites.Count == 0)
			{
				InviteByKey.Remove(loginId);
				return null;
			}

			for (var i = 0; i < invites.Count; i++)
				if (invites[i].guildId == guild.guildData.ClanId)
					return invites[i];

			return null;
		}

		private GuildJoin GetJoin(int loginId, Guild guild)
		{
			if (!JoinByKey.TryGetValue(loginId, out var joins))
				return null;

			if (joins == null || joins.Count == 0)
			{
				JoinByKey.Remove(loginId);
				return null;
			}

			for (var i = 0; i < joins.Count; i++)
				if (joins[i].guildId == guild.guildData.ClanId)
					return joins[i];

			return null;
		}

		private void RemoveInvite(int loginId)
		{
			if (!InviteByKey.TryGetValue(loginId, out var giList))
				return;

			for (var i = 0; i < giList.Count; i++)
			{
				var g = GetGuildById(giList[i].guildId);
				if (g != null)
					g.RemoveInvite(loginId);
			}

			InviteByKey.Remove(loginId);
		}

		private void AddInviteToGuild(int loginId, GuildInvite invite, Guild guild)
		{
			if (!InviteByKey.ContainsKey(loginId))
				InviteByKey[loginId] = new List<GuildInvite>();

			InviteByKey[loginId].Add(invite);
			guild.AddInvite(invite);
		}

		internal ClientErrors AddJoinToGuild(int loginId, string personName, int ratingScore, string joinDescription, Guild guild)
		{
			var guildJoin = GetJoin(loginId, guild);

			ILogger.Instance.Send($"GuildManager.AddJoinToGuild(): person {personName} ({loginId}) sent join message '{joinDescription}'" +
											$"to guild {guild.guildData}");

			//проверить наличие инвайтов от этого персонажа в эту гильдию.
			//если есть - отредактировать. если нет - добавить. все)
			if (guildJoin != null)
			{
				guildJoin.personName = personName;
				guildJoin.ratingScore = ratingScore;
				guildJoin.joinDescription = joinDescription;
				guild.bChanged = true;
				return ClientErrors.GUILD_REQUEST_SUCCESS;
			}

			guildJoin = new GuildJoin();
			//запомнить в 2 коллекции. инвайты в гильдию. инвайты от данного персонажа
			guildJoin.loginId = loginId;
			guildJoin.personName = personName;
			guildJoin.ratingScore = ratingScore;
			guildJoin.joinDescription = joinDescription;
			guildJoin.guildId = guild.guildData.ClanId;

			if (!JoinByKey.ContainsKey(loginId))
				JoinByKey[loginId] = new List<GuildJoin>();

			JoinByKey[loginId].Add(guildJoin);
			guild.AddJoin(guildJoin);

			return ClientErrors.GUILD_REQUEST_SUCCESS;
		}

		internal ClientErrors JoinGuild(int loginId, string personName, string avatarName, int ratingScore, string joinDescription, int guildId, out Guild guild)
		{
			guild = null;
			if (GuildIdByLoginId.ContainsKey(loginId))
			{
				if (!GuildMemberKick(loginId, loginId))
				{
					ILogger.Instance.Send($"GuildManager.JoinGuild(): person {personName} ({loginId}) already in guild", ErrorLevel.error);
					return ClientErrors.ALREADY_IN_GUILD_ERROR;
				}
			}

			guild = GetGuildById(guildId);
			if (guild == null)
			{
				ILogger.Instance.Send($"GuildManager.JoinGuild(): person {personName} ({loginId}) not found guild {guildId}", ErrorLevel.error);
				return ClientErrors.GUILD_NOT_FOUND;
			}

			if (ratingScore < guild.guildData.MinRatingScore)
			{
				ILogger.Instance.Send($"GuildManager.JoinGuild(): person {personName} ({loginId}) not enough gs {ratingScore} < {guild.guildData.MinRatingScore}", ErrorLevel.error);
				return ClientErrors.LOW_RATING_FOR_GUILD;
			}

			if (guild.guildData.ClanType == ClanType.Close)
			{
				ILogger.Instance.Send($"GuildManager.JoinGuild(): person {personName} ({loginId}) tried to join closed guild {guild.guildData}", ErrorLevel.error);
				return ClientErrors.GUILD_INVITE_TYPE_ERROR;
			}

			if (guild.guildData.ClanType == ClanType.Invite)
			{
				return AddJoinToGuild(loginId, personName, ratingScore, joinDescription, guild);
			}

			if (guild.PersonsCount >= ClanSize)
			{
				ILogger.Instance.Send($"GuildManager.JoinGuild(): person {personName} ({loginId}) {guild.guildData} is full", ErrorLevel.error);
				return ClientErrors.MAX_SIZE_REACHED_ERROR;
			}

			RemoveJoins(loginId);
			RemoveInvite(loginId);

			var login = guild.AddPerson(loginId, personName, avatarName, ClanRole.Squire, ratingScore);
			GuildIdByLoginId[loginId] = guild.guildData.ClanId;

            OnMemberJoin?.Invoke (guild, login);

			ILogger.Instance.Send($"GuildManager.AddJoinToGuild(): person {personName} ({loginId}) joined guild {guild.guildData}");
			return ClientErrors.SUCCESS;
		}

		internal Guild GetGuildById(int guildId)
		{
			return GuildById.TryGetValue(guildId, out var g) ? g : null;
		}

		internal Guild GetGuildByLoginId(int loginId)
		{
			return GuildIdByLoginId.TryGetValue(loginId, out var gid) ? GetGuildById(gid) : null;
		}

		internal void OnRenameLogin(int loginId, string name, string avatarName)
		{
			var g = GetGuildByLoginId(loginId);
			if (g == null)
				return;

			g.RenamePerson(loginId, name, avatarName);
			ILogger.Instance.Send($"GuildManager.OnRenamePerson(): person {loginId} new name {name} in {g.guildData}");
		}

		internal ClientErrors ChangeGuild(int loginId, GuildData gd, out Guild guild)
		{
#warning проверить роль того кто это делает.
			guild = GetGuildByLoginId(loginId);

			if (guild == null)
			{
				ILogger.Instance.Send($"GuildManager.ChangeGuild(): person {loginId} {gd} guild not found", ErrorLevel.error);
				return ClientErrors.GUILD_NOT_FOUND;
			}

			if (guild.guildData.ClanName != gd.ClanName && GuildNames.Contains(gd.ClanName))
			{
				ILogger.Instance.Send($"GuildManager.ChangeGuild(): person {loginId} {gd} guild name exists", ErrorLevel.error);
				return ClientErrors.GUILD_NAME_EXIST_ERROR;
			}

			if (guild.guildData.ClanName != gd.ClanName)
			{
				if (guild.guildData.ClanNameCooldown > DateTime.UtcNow)
					gd.ClanName = guild.guildData.ClanName; //Время не прошло, имя менять нельзя
				else
					guild.guildData.ClanNameCooldown = DateTime.UtcNow.AddDays(7);
			}

#warning узнать что из этого можно редактировать.
			guild.guildData.ClanDescription = gd.ClanDescription;
			guild.guildData.ClanName = gd.ClanName;
			guild.guildData.EmblemData = gd.EmblemData;
			guild.guildData.ClanType = gd.ClanType;
			guild.guildData.GuildUTCLocation = gd.GuildUTCLocation;
			guild.guildData.MinRatingScore = gd.MinRatingScore;
			guild.bChanged = true;

			ILogger.Instance.Send($"GuildManager.ChangeGuild(): person {loginId} {gd}");

			return ClientErrors.SUCCESS;
		}

		internal ClientErrors CheckGuildName(string guildName)
		{
			if (GuildNames.Contains(guildName))
				return ClientErrors.GUILD_NAME_EXIST_ERROR;

			return ClientErrors.SUCCESS;
		}

		internal void OnRatingScoreChange(int loginId, int ratingScore, out bool update)
		{
			update = false;

			//поискать гильдию человека. если есть - ок. и выход.
			var guild = GetGuildByLoginId(loginId);
			var gpi = guild?.GetPerson(loginId);
			if (gpi != null && gpi.RatingScore != ratingScore)
			{
				update = true;
				gpi.RatingScore = ratingScore;
				ILogger.Instance.Send($"GuildManager.OnGSChange(): person {loginId} ratingScore {ratingScore}");
				guild.bChanged = true;
			}

			//поискать его приглашения. если есть - поправить их.
			var invites = getInvites(loginId);
			if (invites != null && invites.Count > 0)
			{
				for (var i = 0; i < invites.Count; i++)
				{
					var gi = invites[i];
					if (gi != null)
					{
						gi.ratingScore = ratingScore;
						var g = GetGuildById(gi.guildId);
						if (g != null)
							g.bChanged = true;
					}
				}
			}
		}

		private List<GuildJoin> getInvites(int loginId)
		{
			return JoinByKey.TryGetValue(loginId, out var result) ? result : null;
		}

		internal List<int> GetJoinGuild(int loginId)
		{
			var lst = new List<int>();

			var invites = getInvites(loginId);
			if (invites == null || invites.Count == 0)
				return lst;

			for (var i = 0; i < invites.Count; i++)
				lst.Add(invites[i].guildId);

			return lst;
		}

		internal List<GuildPersonInfo> GetGuildMembers(int loginId)
		{
			var guild = GetGuildByLoginId(loginId);

			if (guild == null)
				return new List<GuildPersonInfo>();

			return guild.Persons;
		}

		internal bool GuildMemberRoleChange(int loginId, int targetLoginId, ClanRole RoleId)
		{
			ILogger.Instance.Send($"GuildManager.GuildMemberRoleChange(): person {loginId} target {targetLoginId} role {RoleId}");

			var guild = GetGuildByLoginId(loginId);

			if (guild == null)
				return false;

			var person = guild.GetPerson(loginId);
			var target = guild.GetPerson(targetLoginId);
            
            if (target == null || person == null)
				return false;

			if (RoleId < ClanRole.Squire || RoleId > ClanRole.Commander)
				return false;

			//проверим... повышаем или понижаем роль
			if (target.RoleId == RoleId)
				return true;

            ClanRole oldRole = target.RoleId;
            //понижаем.
            if (RoleId < target.RoleId)
			{
				var self = target.LoginId == person.LoginId;
				if (self && person.RoleId == ClanRole.Commander)
					return false;

				if (target.RoleId > person.RoleId)
					return false;

				target.RoleId = RoleId;
				if (!self)
					GuildServer.Instance.Dispatcher.NotifyAboutRoleChange(target);

                OnMemberRoleChanged?.Invoke(guild, person, target, oldRole);

                guild.bChanged = true;
				return true;
			}

			//повышаем
			if (RoleId > target.RoleId)
			{
				if (RoleId > person.RoleId || RoleId == ClanRole.Commander)
					return false;

				target.RoleId = RoleId;
				GuildServer.Instance.Dispatcher.NotifyAboutRoleChange(target);

                OnMemberRoleChanged?.Invoke (guild, person, target, oldRole);

                guild.bChanged = true;
				return true;
			}

			return false; //хз что произошло)
		}

		internal bool GuildMemberMakeOwner(int loginId, int targetLoginId)
		{
			var guild = GetGuildByLoginId(loginId);
			var targetGuild = GetGuildByLoginId(loginId);

			if (guild == null || guild != targetGuild)
				return false;

			var person = guild.GetPerson(loginId);
            var target = guild.GetPerson(targetLoginId);
            ClanRole targetOldRole = target.RoleId;

            if (target == null || person == null)
				return false;
			if (person.RoleId != ClanRole.Commander)
				return false;

			person.RoleId = ClanRole.Advisor;
            GuildServer.Instance.Dispatcher.NotifyAboutRoleChange(person);

			makeOwner(guild, target);

            //Сообщаем только о повышении, клиент сам поймёт, что идёт повышение до главы клана и выдаст соотвествующее сообщение
            OnMemberRoleChanged?.Invoke (guild, person, target, targetOldRole);

            return true;
		}

		private void makeOwner(Guild guild, GuildPersonInfo target)
		{
			target.RoleId = ClanRole.Commander;
			guild.LeaderId = target.LoginId;

			ILogger.Instance.Send($"GuildManager.makeOwner(): guild {guild.guildData} new owner {target.PersonName}");

			GuildServer.Instance.Dispatcher.NotifyAboutRoleChange(target);
		}

		internal bool GuildMemberKick(int loginId, int targetLoginId, bool force = false)
		{
			var guild = GetGuildByLoginId(loginId);
			if (guild == null)
				return false;

			var person = guild.GetPerson(loginId);
			var target = guild.GetPerson(targetLoginId);

			if (target == null || person == null)
				return false;

			if (target.RoleId > person.RoleId)
				return false;

			//Командир должен назначить нового. Но если он последний, то гильдия распустится
			if (!force && target.RoleId == ClanRole.Commander && guild.PersonsCount > 1)
				return false;

			guild.RemoveLogin(targetLoginId);
			GuildIdByLoginId.Remove(targetLoginId);
			ILogger.Instance.Send($"GuildManager.GuildMemberKick(): guild {guild.guildData} kick {target.PersonName} ({target.LoginId}) by person {loginId}");

			if (guild.PersonsCount == 0)
			{
				guildDisband(guild);
			}
			else if (target.RoleId == ClanRole.Commander)
			{
				var newOwner = guild.getNewOwner();
                ClanRole oldMemberRole = newOwner.RoleId;
                makeOwner (guild, newOwner);
                //сообщить причастным, что овнер поменялся. и овнеру. и описание гильдии..

                OnMemberRoleChanged?.Invoke(guild, person, newOwner, oldMemberRole);
			}

            OnMemberLeave?.Invoke(guild, person, target);

			return true;
		}

		internal ClientErrors GuildSetJoinStatus(int loginId, int targetLoginId, bool status, out Guild guild)
		{
			guild = GetGuildByLoginId(loginId);

			if (guild == null)
			{
				ILogger.Instance.Send($"GuildManager.GuildSetJoinStatus(): not found guild for person {loginId}", ErrorLevel.error);
				return ClientErrors.GUILD_NOT_FOUND;
			}

			var person = guild.GetPerson(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"GuildManager.GuildSetJoinStatus(): not found person {loginId} in guild {guild.guildData}", ErrorLevel.error);
				return ClientErrors.GUILD_NOT_FOUND;
			}

			var targetGuild = GetGuildByLoginId(targetLoginId);
			if (targetGuild != null)
			{
				ILogger.Instance.Send($"GuildManager.GuildSetJoinStatus(): target {targetLoginId} already in guild {targetGuild.guildData}", ErrorLevel.error);
				return ClientErrors.ALREADY_IN_GUILD_ERROR;
			}

			var join = GetJoin(targetLoginId, guild);
			if (join == null)
			{
				ILogger.Instance.Send($"GuildManager.GuildSetJoinStatus(): not found join for target {targetLoginId} in guild {guild.guildData}", ErrorLevel.error);
				return ClientErrors.GUILD_REQUEST_NOT_FOUND;
			}

			var guildId = guild.guildData.ClanId;
			if (status)
			{
				//принять в гильдию и удалить заявку. если есть места.
				if (guild.PersonsCount >= ClanSize)
				{
					ILogger.Instance.Send($"GuildManager.GuildSetJoinStatus(): can't join person {targetLoginId}:" +
													$" guild {guild.guildData} is full", ErrorLevel.error);
					return ClientErrors.MAX_SIZE_REACHED_ERROR;
				}

				guild.AddPerson(targetLoginId, join.personName, join.avatarName, ClanRole.Squire, join.ratingScore);
				GuildIdByLoginId[targetLoginId] = guildId;
				guild.bChanged = true;
				ILogger.Instance.Send($"GuildManager.GuildSetJoinStatus(): added person {targetLoginId}" +
												 $" to guild {guild.guildData}", ErrorLevel.info);


				var mail = GetGuildRequestSuccessMail(guild, targetLoginId);
				GuildServer.Instance.DBStoreManager.EnqueueForSave(mail,
					m => GuildServer.Instance.Dispatcher.SendNewMailRequest(m.LoginId, m.PersonId),
					(res, ex) => ILogger.Instance.Send($"Failed to create mail guildId={guildId} loginId={loginId}", ErrorLevel.error));

				RemoveJoins(targetLoginId);
				RemoveInvite(targetLoginId);

				return ClientErrors.SUCCESS;
			}
			else
			{
				ILogger.Instance.Send($"GuildManager.GuildSetJoinStatus(): reject join request from {targetLoginId}" +
												$" to guild {guild.guildData} by {loginId}");
				//отклонить заявку
				RemoveJoin(targetLoginId, guild);
				return ClientErrors.SUCCESS;
			}
		}

		private MailData GetGuildInviteRecievedMail(Guild guild, int targetLoginId, int targetPersonId, string text)
		{
			var mail = new MailData
			{
				LoginId = targetLoginId,
				PersonId = targetPersonId,
				AskSender = null,
				AskTitle = 0,
				CanThank = false,
				BodyKey = string.Empty,
				BodyParams = null,
				GiftCount = 0,
				GiftSenders = null,
				GiftType = ResourceType.None,
				LostReward = null,
				MailType = MailType.GuildInviteRecieved,
				RemoveTime = TimeProvider.UTCNow.AddHours(24 * 7),
				TitleKey = "",
				TitleParams = null,
				GuildId = guild.guildData.ClanId,
				Text = text
			};
#warning вынести в настройки куда-нибудь это
			return mail;
		}

		private MailData GetGuildRequestSuccessMail(Guild guild, int targetLoginId)
		{
			var mail = new MailData
			{
				LoginId = targetLoginId,
				AskSender = null,
				AskTitle = 0,
				CanThank = false,
				BodyKey = "interface.guilds.notification.text_guild_request_success_body",
				BodyParams = new Dictionary<string, string>() { { "{_guild_name_}", guild.guildData.ClanName } },
				GiftCount = 0,
				GiftSenders = null,
				GiftType = ResourceType.None,
				LostReward = null,
				MailType = MailType.GuildRequestSuccess,
				RemoveTime = TimeProvider.UTCNow.AddHours(36),
				TitleKey = "",
				TitleParams = new Dictionary<string, string>() { { "title", guild.LeaderName } },
				GuildId = guild.guildData.ClanId
			};
#warning вынести в настройки куда-нибудь это
			return mail;
		}

		private void RemoveJoins(int loginId)
		{
			List<GuildJoin> joins;
			if (!JoinByKey.TryGetValue(loginId, out joins))
				return;

			for (var i = 0; i < joins.Count; i++)
			{
				var g = GetGuildById(joins[i].guildId);
				if (g != null)
					g.RemoveJoin(loginId);
			}

			JoinByKey.Remove(loginId);
		}

		private void RemoveJoin(int targetLoginId, Guild guild)
		{
			List<GuildJoin> joins;
			if (!JoinByKey.TryGetValue(targetLoginId, out joins))
				return;

			for (var i = joins.Count - 1; i >= 0; i--)
			{
				if (joins[i].guildId == guild.guildData.ClanId)
				{
					joins.RemoveAt(i);

					guild.RemoveJoin(targetLoginId);

					if (joins.Count == 0)
					{
						JoinByKey.Remove(targetLoginId);
						return;
					}
				}
			}
		}

		internal List<GuildJoin> GetGuildJoinList(int loginId)
		{
			var guild = GetGuildByLoginId(loginId);

			if (guild == null)
				return new List<GuildJoin>(0);

			return guild.Joins;
		}

		internal ClientErrors GuildInvite(int loginId, string personName, string avatarName, int ratingScore, int guildId, out Guild guild)
		{
			guild = GetGuildById(guildId);
			if (guild == null)
			{
				ILogger.Instance.Send($"GuildManager.GuildInvite(): not found guild {guildId} for person {personName} ({loginId})", ErrorLevel.error);
				return ClientErrors.GUILD_NOT_FOUND;
			}

			var targetGuild = GetGuildByLoginId(loginId);
			if (targetGuild != null)
			{
				guild = null;
				ILogger.Instance.Send($"GuildManager.GuildInvite(): person {personName} ({loginId}) already in guild {targetGuild.guildData}", ErrorLevel.error);
				return ClientErrors.ALREADY_IN_GUILD_ERROR;
			}

			var invite = GetInvite(loginId, guild);
			if (invite == null)
			{
				guild = null;
				ILogger.Instance.Send($"GuildManager.GuildInvite(): not found invite in guild {guildId} for person {personName} ({loginId})", ErrorLevel.error);
				return ClientErrors.GUILD_REQUEST_NOT_FOUND;
			}

			if (guild.PersonsCount >= ClanSize)
			{
				ILogger.Instance.Send($"GuildManager.GuildInvite(): max size guild {guild.guildData} for person {personName} ({loginId})", ErrorLevel.error);
				guild = null;
				return ClientErrors.MAX_SIZE_REACHED_ERROR;
			}

			RemoveJoins(loginId);
			RemoveInvite(loginId);

			guild.AddPerson(loginId, personName, avatarName, ClanRole.Squire, ratingScore);
			GuildIdByLoginId[loginId] = guild.guildData.ClanId;
			guild.bChanged = true;
			ILogger.Instance.Send($"GuildManager.GuildInvite(): added person {personName} ({loginId}) to guild {guild.guildData}");

			return ClientErrors.SUCCESS;
		}

		internal ClientErrors GuildInviteRequest(int loginId, int targetLoginId, string inviteText, out Guild guild)
		{
			guild = GetGuildByLoginId(loginId);

			if (guild == null)
			{
				ILogger.Instance.Send($"GuildManager.GuildInviteRequest(): not found guild for person {loginId}", ErrorLevel.error);
				return ClientErrors.GUILD_NOT_FOUND;
			}

			var person = guild.GetPerson(loginId);
			if (person == null)
			{
				ILogger.Instance.Send($"GuildManager.GuildInviteRequest(): not found {loginId} in guild {guild.guildData}", ErrorLevel.error);
				return ClientErrors.GUILD_NOT_FOUND;
			}

			var targetGuild = GetGuildByLoginId(targetLoginId);
			if (targetGuild != null)
			{
				ILogger.Instance.Send($"GuildManager.GuildInviteRequest(): target person {targetLoginId} already in guild {targetGuild.guildData}", ErrorLevel.error);
				return ClientErrors.ALREADY_IN_GUILD_ERROR;
			}

			if (guild.PersonsCount >= ClanSize)
			{
				ILogger.Instance.Send($"GuildManager.GuildInviteRequest(): guild {guild.guildData} is full", ErrorLevel.error);
				return ClientErrors.MAX_SIZE_REACHED_ERROR;
			}

			var guildId = guild.guildData.ClanId;
			var invite = GetInvite(targetLoginId, guild);
			if (invite == null)
			{
				invite = new GuildInvite();
				invite.guildId = guildId;
				invite.inviteText = inviteText;
				invite.loginId = targetLoginId;
				invite.sended = false;
				AddInviteToGuild(targetLoginId, invite, guild);
			}
			else
			{
				invite.inviteText = inviteText;
				guild.bChanged = true;
			}

			ILogger.Instance.Send($"GuildManager.GuildInviteRequest(): person {loginId} invited {targetLoginId} to guild {guild.guildData} with text {inviteText}");

			var mail = GetGuildInviteRecievedMail(guild, targetLoginId, targetLoginId, inviteText);
			GuildServer.Instance.DBStoreManager.EnqueueForSave(mail,
				m => GuildServer.Instance.Dispatcher.SendNewMailRequest(m.LoginId, m.PersonId),
				(res, ex) => ILogger.Instance.Send($"Failed to create mail guildId={guildId} loginId={loginId}", ErrorLevel.error));

			return ClientErrors.SUCCESS;
		}

		internal void guildDisband(Guild guild)
		{
			ILogger.Instance.Send($"GuildManager.guildDisband(): disbanding guild {guild.guildData}");

			if (PermanentDelete)
			{
				//удалить с концами
				removeGuild(guild);
				guild.deleted = true;
				GuildServer.Instance.DBStoreManager.EnqueueForDelete(guild);
			}
			else
			{
				//Сделать клан открытым
				guild.guildData.ClanType = ClanType.Open;
				guild.guildData.MinRatingScore = 0;
				removeJoinAndInvites(guild);
			}
		}

		internal ClientErrors GuildDisband(int loginId, out int guildId)
		{
			guildId = -1;

			var guild = GetGuildByLoginId(loginId);

			if (guild == null)
			{
				ILogger.Instance.Send($"GuildManager.GuildDisband(): not found guild for person {loginId}", ErrorLevel.error);
				return ClientErrors.GUILD_NOT_FOUND;
			}

			guildId = guild.guildData.ClanId;

			var person = guild.GetPerson(loginId);

			if (person == null)
			{
				ILogger.Instance.Send($"GuildManager.GuildDisband(): not found {loginId} in guild {guild.guildData}", ErrorLevel.error);
				return ClientErrors.PERSON_NOT_FOUND;
			}

			if (person.RoleId != ClanRole.Commander)
			{
				ILogger.Instance.Send($"GuildManager.GuildDisband(): person {loginId} is not owner ({person.RoleId}) in guild {guild.guildData}", ErrorLevel.error);
				return ClientErrors.LOGIC_ERROR;
			}

			if (guild.PersonsCount > 1)
			{
				ILogger.Instance.Send($"GuildManager.GuildDisband(): guild {guild.guildData} is not empty", ErrorLevel.error);
				return ClientErrors.LOGIC_ERROR;
			}

			guild.RemoveLogin(loginId);
			GuildIdByLoginId.Remove(loginId);

			guildDisband(guild);
			return ClientErrors.SUCCESS;
		}

		private readonly Dictionary<int, DateTime> LastPings = new Dictionary<int, DateTime>();

		public void LoginPing(int loginId)
		{
			LastPings[loginId] = TimeProvider.UTCNow;
		}

		public double GetLoginPing(int loginId)
		{
			if (!LastPings.TryGetValue(loginId, out var lastPing))
				return 7*24*60*60;

			var time = (TimeProvider.UTCNow - lastPing).TotalSeconds;
			return time > 30 ? time : 0;
		}

		//Пересчитать онлайн и рейтинг
		public void UpdateMemberData(Guild guild)
		{
			if (guild == null)
				return;

			var membersOnlineCount = 0;
			var rating = 0;
			foreach (var person in guild.Persons)
			{
				if (!LastPings.TryGetValue(person.LoginId, out var lastPing))
					continue;

				var time = (TimeProvider.UTCNow - lastPing).TotalSeconds;
				if (time > 30)
				{
					person.WasOnline = time;
				}
				else
				{
					person.WasOnline = 0;
					membersOnlineCount++;
				}

				rating += person.RatingScore;
			}

			guild.guildData.MembersOnlineCount = membersOnlineCount;
			guild.guildData.RatingScore = rating;
		}

		public void UpdateGuildsData(Dictionary<long, int> places)
		{
			lock (m_guildListLock)
			{
				foreach (var place in places)
				{
					var clan = GuildList.FirstOrDefault(g => g.guildData.ClanId == place.Key);
					if (clan != null)
					{
						clan.guildData.BestPlace = place.Value;
					}
				}
			}
		}
	}
}
