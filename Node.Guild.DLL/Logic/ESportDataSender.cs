﻿using Fragoria.Common.Utils.Threads;
using NodeGuild;
using NodeUtilsLib.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Utils.Events;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace Node.Guild.Logic
{
    public class ESportDataSender : OneThreadTimer
    {
        public ESportDataSender() : base("ESportDataSender", 1000)
        {
        }

        public string eSportUrl;
        private string eSportResultUrl;
        public string eSportGameToken;
        public string eSportGameName;
        public bool eSportEnable = false;
        public int eSportRepeatMsgLimit = 20;
        public int eSportRepeatMsgTimeout = 15;

        private ConcurrentQueue<AsyncRequestData> queue = new ConcurrentQueue<AsyncRequestData>();

        public void Init(RemoteXmlConfig config)
        {
            if (config.eSportEnable)
            {
                eSportGameToken = config.eSportGameToken;
                eSportGameName = config.eSportGameName;
                eSportUrl = config.eSportUrl.Replace("{gameSlug}", config.eSportGameSlug);
                eSportResultUrl = eSportUrl + "/result";
                eSportRepeatMsgLimit = config.eSportRepeatMsgLimit;
                eSportRepeatMsgTimeout = config.eSportRepeatMsgTimeout;
            }
            eSportEnable = config.eSportEnable;
        }

        public override void OnTimer()
        {
            AsyncRequestData esportdata;

            while (queue.TryDequeue(out esportdata))
                SendData(esportdata.gameStatus, esportdata.postData, esportdata.counter);
        }

        private void SendData(ESportGameStatus gameStatus, string postData, int sendCounter = 1)
        {
            if (!eSportEnable)
                return;

            if (sendCounter > eSportRepeatMsgLimit)
            {
                ILogger.Instance.Send($"SendDataError reached the limit of trying to send data {postData}", ErrorLevel.error);
                return;
            }

            string url;
            if (gameStatus == ESportGameStatus.FINISHED || gameStatus == ESportGameStatus.PLAYING)
                url = eSportResultUrl;
            else
                url = eSportUrl;

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";

            request.Headers.Add("x-gc-token", eSportGameToken);

            byte[] requestBody = Encoding.UTF8.GetBytes(postData);
            ILogger.Instance.Send(" try send (" + sendCounter + " times) data to " + url + ", data = " + postData, ErrorLevel.warning);

            AsyncRequestData ard = new AsyncRequestData(request, postData, gameStatus, sendCounter);

            request.BeginGetRequestStream(GetRequestStreamCallback, new object[] { request, requestBody, ard });
        }

        private void Send(AsyncRequestData reqData)
        {
            queue.Enqueue(reqData);
        }

        internal void Send(ESportGameStatus gameStatus, string postData, int sendCounter = 1)
        {
            if (!eSportEnable)
                return;

            AsyncRequestData ard = new AsyncRequestData(null, postData, gameStatus, sendCounter);

            queue.Enqueue(ard);

        }

        private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                var request = (HttpWebRequest)((object[])asynchronousResult.AsyncState)[0];
                using (var postStream = request.EndGetRequestStream(asynchronousResult))
                {
                    var byteArray = (byte[])((object[])asynchronousResult.AsyncState)[1];

                    // Write to the request stream.
                    postStream.Write(byteArray, 0, byteArray.Length);

                }
                request.BeginGetResponse(GetResponseCallback, ((object[])asynchronousResult.AsyncState)[2]);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("GetRequestStreamCallback Exception error = " + ex.ToString(), ErrorLevel.error);
            }
        }

        private void GetResponseCallback(IAsyncResult asynchronousResult)
        {
            AsyncRequestData ard = (AsyncRequestData)asynchronousResult.AsyncState;
            try
            {
                var request = ard.request;
                var response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
                if (response != null)
                {
                    var reader = new StreamReader(response.GetResponseStream());
                    string responseString = reader.ReadToEnd();
                    ILogger.Instance.Send("Response = " + responseString, ErrorLevel.warning);
                }
            }
            catch (WebException we)
            {
                var reader = new StreamReader(we.Response.GetResponseStream());
                string responseString = reader.ReadToEnd();
                ILogger.Instance.Send("Response WebException error = " + responseString, ErrorLevel.error);
                ResendMessageIfNeed(ard);
            }
            catch (Exception ex)
            {
                ILogger.Instance.Send("Response Exception error = " + ex.ToString(), ErrorLevel.error);
                ResendMessageIfNeed(ard);
            }
        }

        private void ResendMessageIfNeed(AsyncRequestData ard)
        {
            if (ard != null && ard.gameStatus != ESportGameStatus.PLAYING)
            {
                ard.counter++;
                Period.AfterSec(eSportRepeatMsgTimeout).Action(() => Send(ard));
            }
        }

        class AsyncRequestData
        {
            public AsyncRequestData(HttpWebRequest httpWebRequest, string postdata, ESportGameStatus gamestatus, int count)
            {
                request = httpWebRequest;
                postData = postdata;
                gameStatus = gamestatus;
                counter = count;
            }

            public HttpWebRequest request;
            public string postData;
            public ESportGameStatus gameStatus;
            public int counter;
        }

    }
}
