﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Fragoria.Common.Utils.Threads;
using Newtonsoft.Json;
using NodeGuild;
using NodeGuild.Logic;
using NodeGuild.Protocols;
using UtilsLib;
using UtilsLib.Logic;
using UtilsLib.DegSerializers;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using UtilsLib.XmlData;
using NodeGuild.Utils;
using System.Diagnostics;

namespace Node.Guild.Logic
{
    public class SubscribeManager : OneThreadTimer
    {
        private const int timeSecForRemove = 120;
        private const int timeSecForOnLine = 60;

        public ArenaAlgorithmData arenaAlgorithmData;
        protected GuildManager guildManager;

        public Dictionary<int, ArenaPersonSubscribeData> persons = new Dictionary<int, ArenaPersonSubscribeData>();
        protected object personLock_object = new object();

        public List<ArenaGroup> groupList = new List<ArenaGroup>();
        protected object grouplistLock_object = new object();

        protected Dictionary<int, SubscribeGroup> subscribes = new Dictionary<int, SubscribeGroup>();
        protected object subscribes_object = new object();

        public SubscribeManager(GuildManager guildManager) : base("SubscribeManagerBase", iPeriodMilliseconds: 1000)
        {
            this.guildManager = guildManager;
        }

        private SysDispatcher sysDispatcher;
        public void SetArenaAlgorithmData(ArenaAlgorithmData arenaAlgorithmData)
        {
            this.arenaAlgorithmData = arenaAlgorithmData;
            arenaAlgorithmData.Init();
        }

        public void SetSysDyspatcher(SysDispatcher sysDispatcher)
        {
            this.sysDispatcher = sysDispatcher;
        }

        public void Init()
        {
            //TODO fil транспорт инитится после манагеров потому и arenaAlgorithmData=null
            //foreach (var data in arenaAlgorithmData.ArenaMatchInfo)
            //{
            //    if (!subscribes.ContainsKey(data.MatchButtonId))
            //        subscribes.Add(data.MatchButtonId, new PreGroup());
            //}            

            //guildManager.GuildEventsForAll += GuildManagerOnGuildEventsForAll;
        }

        public override void OnTimer()
        {
            Update();
        }
        #region groupList

        protected List<int> GetSubscribesList(int personId)
        {
            lock (subscribes_object)
            {
                List<int> list = new List<int>();
                foreach (var kvp in subscribes)
                {
                    if (kvp.Value.Contains(personId))
                        list.Add(kvp.Key);
                }
                return list;
            }
        }

        internal bool ArenaPersonCheck(InstancePersonData simplePersonInfo, out ArenaGroup group)
        {
            lock (grouplistLock_object)
            {
                group = groupList.Find(i => i.persons.Exists(p => p.PersonId == simplePersonInfo.personId));
                return (group != null);
            }
        }

        internal ArenaGroup GetArenaGroup(ArenaPersonSubscribeData apsd)
        {
            lock (grouplistLock_object)
            {
                return groupList.Find(i => i.persons.Exists(p => p.PersonId == apsd.PersonId));
            }
        }

        internal ArenaGroup GetArenaGroup(string uniq)
        {
            lock (grouplistLock_object)
            {
                return groupList.Find(i => i.uniq == uniq);
            }
        }

        #endregion

		public void AddGroup(ArenaGroup group)
        {
            lock (grouplistLock_object)
            {
                groupList.Add(group);
                ILogger.Instance.Send($"AddGroup group: {group.uniq}, startTime: {group.startTime}, group.endtime: {group.endtime} ", ErrorLevel.debug);
            }
        }

        protected void AddPerson(ArenaPersonSubscribeData p)
        {
            lock (personLock_object)
            {
                persons[p.PersonId] = p;
            }
        }


        protected void AddToSubscribes(int matchButtonId, ArenaPersonSubscribeData apsd)
        {
            lock (subscribes_object)
            {
                if (!subscribes.ContainsKey(matchButtonId))
                    subscribes.Add(matchButtonId, new SubscribeGroup());

                subscribes[matchButtonId].AddToSubscribe(apsd);
            }
        }

        protected void RecalcGroupList(long current)
        {
            lock (grouplistLock_object)
            {
                if (groupList == null || groupList.Count == 0)
                    return;

                //старт и останов групп. забывание про них и т.д.
                for (int i = 0; i < groupList.Count; i++)
                {
                    ArenaGroup group = groupList[i];
                    if (!group.started && group.startTime < current)
                    {
                        group.started = true;
                        List<ArenaPersonSubscribeData> lst = new List<ArenaPersonSubscribeData>();
                        for (int j = 0; j < group.persons.Count; j++)
                            if (!group.persons[j].bTeleport)
                                lst.Add(group.persons[j]);
                        sysDispatcher.SendTeleportToArenaBattle(groupList[i], lst);
                        for (int j = 0; j < group.persons.Count; j++)
                            group.persons[j].bTeleport = false;
                    }
                    else if (group.started && current > group.endtime)
                    {
                        ILogger.Instance.Send($"RecalcGroupList group: {group.uniq} current: {current}, startTime: {group.startTime}, group.endtime: {group.endtime} ", ErrorLevel.debug);
                        groupList.RemoveAt(i);
                        i--;
                    }
                }
            }
        }

        internal void unsubscribe(int personId, PersonType personType)
        {
            RemovePerson(personId);

            RemovePersonFromSubscribes(personId, personType);
        }

        protected void RemovePersonFromSubscribes(int personId, PersonType personType)
        {
            lock (subscribes_object)
            {
                bool isRemove = false;
                foreach (var kvp in subscribes)
                {
                    if(kvp.Value.Remove(personId, personType))
                    {
                        isRemove = true;
                    }                        
                }
                if (!isRemove)
                {
                    ILogger.Instance.Send($"RemovePersonFromSubscribes PreGroup Remove not Contains: {personId}", ErrorLevel.error);
                }
            }
        }

        protected void RemovePerson(int personId)
        {
            lock (personLock_object)
            {
                persons.Remove(personId);
            }
        }

        protected List<ArenaPersonSubscribeData> getPersonList()
        {
            lock (personLock_object)
            {
                return persons.Values.ToList<ArenaPersonSubscribeData>();
            }
        }
        internal bool CheckExistArena(string levelParam, out string instanceData)
        {
            lock (grouplistLock_object)
            {
                ArenaGroup ag = groupList.Find(i => i.uniq == levelParam);
                instanceData = string.Empty;

                if (ag != null)
                {
                    MatchBaseData matchBaseData = new MatchBaseData()
                    {
                        PersonsSubscribedCount = ag.persons.Count,
                        AverageRating = ag.GetAverageRating(),
                        AverageLevel = ag.GetAverageLevel(),
                        MatchingTime = ag.startTime - XmlUtils.GetEpochTimeSec()
                    };

                    if (ag.matchType == ArenaMatchType.TeamDeathmatch || ag.matchType == ArenaMatchType.CaptureFlag || ag.matchType == ArenaMatchType.Brawl)
                        matchBaseData.PlayerTeams = ag.GetTeams();
                    else
	                    matchBaseData.PlayerTeams = ag.GetPersons();

                    instanceData = JSON.JsonEncode(matchBaseData.JsonEncode());
                    return true;
                }

                return false;
            }
        }

        internal void LoginPing(int loginId, int personId)
        {
            //if (persons.ContainsKey(personId))
            {
                ArenaPersonSubscribeData p = getPerson(personId);// persons[personId];
                if (p != null)
                    p.lastOnline = XmlUtils.GetEpochTimeSec();
            }
        }

        internal void ArenaGameEndFor(int personId)
        {
            ArenaGroup group = null;
            lock (grouplistLock_object)
            {
                group = groupList.Find(i => i.persons.Exists(p => p.PersonId == personId));

                if (group != null)
                {
                    group.RemovePerson(personId);
                }
            }
        }
        internal void RemoveArenaGroup(string uniq)
        {
            lock (grouplistLock_object)
            {
                for (int i = 0; i < groupList.Count; i++)
                {
                    ArenaGroup group = groupList[i];
                    if (group == null)
                        continue;
                    if (group.uniq == uniq)
                    {
                        ILogger.Instance.Send($"RemoveArenaGroup group: {group.uniq}, startTime: {group.startTime}, group.endtime: {group.endtime} ", ErrorLevel.debug);
                        groupList.RemoveAt(i);
                        return;
                    }
                }
            }
        }

        internal void Teleport(int loginId, int personId)
        {
            ArenaPersonSubscribeData person = getPerson(personId);
            if (person == null)
                return;

            ArenaGroup g = GetArenaGroup(person);
            if (g == null)
                return;

            //вот тут накорябать код для телепортации
            sysDispatcher.SendTeleportToArenaBattle(g, new List<ArenaPersonSubscribeData>() { person });
            person.bTeleport = true;
            return;
        }
        protected ArenaPersonSubscribeData getPerson(int personId)
        {
            lock (personLock_object)
            {
                ArenaPersonSubscribeData p;
                persons.TryGetValue(personId, out p);
                return p;
            }
        }

        public void Update()
        {
            //проверить список игр. начать нужные. завершить и забыть завершенные. иногда чистить список пользователей. (от удаленных, от оффлайновых и т.д.)
            if (arenaAlgorithmData == null)
                return;

            long currentGetEpochTimeSec = XmlUtils.GetEpochTimeSec();

            //удалить тех кто давно молчит.
            List<ArenaPersonSubscribeData> personList = getPersonList();
            for (int i = 0; i < personList.Count; i++)
            {
                ArenaPersonSubscribeData p = personList[i];
                if (p.lastOnline + timeSecForRemove < currentGetEpochTimeSec)
                {
                    unsubscribe(p.PersonId, p.PersonType);
                }
            }

			int iterationCount = 0;
			for (iterationCount = 0; iterationCount < arenaAlgorithmData.MaxGroupsCountInSec; iterationCount++)
			{
				ArenaGroup newGroup = checkHaveGroups(currentGetEpochTimeSec);
				if (newGroup == null)
					break;

				AddGroup(newGroup);
				sysDispatcher.StartInstanceForGroup(newGroup);
			}
			if (iterationCount > 0)
				ILogger.Instance.Send($"====== MAKED groups: {iterationCount}, CountInSubscribe: {CountInSubscribe()}", ErrorLevel.info);

			RecalcGroupList(currentGetEpochTimeSec);
        }

		Dictionary<int, int> result = new Dictionary<int, int>();
		string CountInSubscribe()
		{
			result.Clear();
			foreach (var kvp in subscribes)
			{
				result.Add(kvp.Key, kvp.Value.persons.Count);
			}
			
			return JsonConvert.SerializeObject(result);
		}

        protected ArenaGroup checkHaveGroups(long currentEpochTimeSec)
        {
            lock (subscribes_object)
            {
                foreach (var kvp in subscribes)
                {
                    try
                    {
                        int matchButtonId = kvp.Key;
                        SubscribeGroup subscribesGroup = kvp.Value;
                        var gameInfo = arenaAlgorithmData.GetArenaGameInfo(matchButtonId).Random();
                        if (gameInfo == null)
                        {
                            ILogger.Instance.Send($"checkHaveGroups gameInfo = null", ErrorLevel.error);
                            continue;
                        }

                        List<PreGroup> pregroups = GetPregroups(currentEpochTimeSec, subscribesGroup, matchButtonId, gameInfo);

                        PreGroup firstPreGroup = pregroups.Find(preGroup =>
                        {
                            bool isTimeOut = currentEpochTimeSec - preGroup.lider.createSubscribeTime >= gameInfo.TimeOut;

                            return isTimeOut || preGroup.persons.Count >= gameInfo.MaxCount;
                        });

                        if (firstPreGroup != null)
                        {
                            PreGroup bestPreGroup = firstPreGroup;
                            var lider = bestPreGroup.lider;
                            long bestSummTime = bestPreGroup.persons.Sum(x => currentEpochTimeSec - x.createSubscribeTime);
                            foreach (var preGroup in pregroups)
                            {

                                if (!preGroup.persons.Contains(lider))
                                    continue;

                                long summTime = preGroup.persons.Sum(x => currentEpochTimeSec - x.createSubscribeTime);

                                if (preGroup.persons.Count > bestPreGroup.persons.Count)//те группы у котрых больше людей выбираются в первую очередь
                                {
                                    bestPreGroup = preGroup;
                                    bestSummTime = summTime;
                                }

                                else if (preGroup.persons.Count == bestPreGroup.persons.Count &&
                                    bestSummTime < summTime)
                                {
                                    //если людей одинаково то выбираются те группы которые дольше всех ждали
                                    bestPreGroup = preGroup;
                                    bestSummTime = summTime;
                                }
                            }

                            //запустить группу
                            ArenaGroup ag = MakeGroup(bestPreGroup.persons, gameInfo, matchButtonId, currentEpochTimeSec);
                            ILogger.Instance.Send($"SetPersonSelectionFilter matchType: {matchButtonId}, group.persons: {JsonConvert.SerializeObject(bestPreGroup.persons)}", ErrorLevel.info);
                            return ag;
                        }

                    }
                    catch (Exception ex)
                    {
                        ILogger.Instance.Send($"checkHaveGroups exception {ex}", ErrorLevel.exception);
                    }
                }

                return null;
            }
        }

        private List<PreGroup> GetPregroups(long currentEpochTimeSec, SubscribeGroup subscribesGroup, int matchButtonId, ArenaAlgorithmData.GamesInfoData gameInfo)
        {
            List<PreGroup> pregroups = new List<PreGroup>();
            for (int i = 0; i < subscribesGroup.persons.Count; i++)
            {
                int mark = 1;
                ArenaPersonSubscribeData personGetGroup = getPerson(subscribesGroup.persons[i]);
                try
                {
                    if (personGetGroup == null)
                        continue;
                    PreGroup preGroup = new PreGroup(personGetGroup);

                    mark = 9;
                    if (!personGetGroup.OnlyBot)
                    {
                        for (int j = 0; j < subscribesGroup.persons.Count; j++)
                        {
                            mark = 10;
                            ArenaPersonSubscribeData personForFilter = getPerson(subscribesGroup.persons[j]);
                            try
                            {
                                mark = 11;
                                if (personForFilter == null || personForFilter.OnlyBot)
                                    continue;

                                mark = 12;
                                if (personGetGroup == personForFilter ||
                                    personGetGroup.ArenaMatchButtonId != personForFilter.ArenaMatchButtonId)
                                    continue;
                                mark = 13;

                                var timeSpan = currentEpochTimeSec - personGetGroup.createSubscribeTime;

                                mark = 15;
                                //то что группа наполнилась тоже проверяется в CheckPersonTypesInGroup
                                bool checkPersonTypeGroup = CheckPersonTypesInGroup(gameInfo, personForFilter.PersonType, preGroup, matchButtonId);
                                bool isLevelEquals = IsPersonSuitableByLevel(personGetGroup, personForFilter, timeSpan);
                                bool isRatingEquals = IsPersonSuitableByRating(personGetGroup, personForFilter, timeSpan);
                                mark = 16;
                                if (checkPersonTypeGroup && isLevelEquals && isRatingEquals)
                                {
                                    mark = 17;
                                    preGroup.AddPerson(personForFilter);
                                }
                            }
                            catch (Exception ex)
                            {
                                ILogger.Instance.Send($"checkHaveGroups personGetGroup = {personGetGroup.PersonId} personForFilter={personForFilter?.PersonId} mark = {mark} matchButtonId=  {matchButtonId} exception {ex}", ErrorLevel.exception);
                            }
                        }
                    }

                    pregroups.Add(preGroup);

                    if (pregroups.Count > 50)//уже есть из чего выбрать. прекращаем набирать следующие группы
                        break;

                }
                catch (Exception ex)
                {
                    ILogger.Instance.Send($"checkHaveGroups  personGetGroup = {personGetGroup?.PersonId} mark = {mark}  matchButtonId = {matchButtonId}exception {ex}", ErrorLevel.exception);
                }
            }

            return pregroups;
        }

        // подбираем типы персонажей в матчинг
        bool CheckPersonTypesInGroup(ArenaAlgorithmData.GamesInfoData gameInfo, PersonType personType, PreGroup preGroup, int matchButtonId)
        {
            int typeCount = preGroup.persons.Count(x => x.PersonType == personType);

            if (preGroup.persons.Count >= gameInfo.MaxCount)
                return false;

            var minTypeCount = gameInfo.MaxCount / (int)PersonType.Count + 1;

            return typeCount < minTypeCount;
        }

        public ArenaGroup MakeGroup(List<ArenaPersonSubscribeData> groupPersons, ArenaAlgorithmData.GamesInfoData gameInfos, int matchButtonId, long current)
        {
            ArenaGroup group = new ArenaGroup();

            for (int i = 0; i < groupPersons.Count && group.persons.Count < gameInfos.MaxCount; i++)
            {
                ArenaPersonSubscribeData person = groupPersons[i];
                if (person != null)
                    group.AddPerson(person);
            }

            foreach (var person in group.persons)
                unsubscribe(person.PersonId, person.PersonType);

            group.matchButtonId = matchButtonId;
            group.matchType = gameInfos.matchType;
            group.uniq = Guid.NewGuid().ToString();
            group.InitStartEndTime(current, gameInfos.GameTimeSec);
            group.MapName = gameInfos.Map;
            //groupList.Add(group);
            ILogger.Instance.Send($"MakeGroup group.persons: {group.persons.Count} group.matchButtonId: {group.matchButtonId}", ErrorLevel.debug);
            return group;
        }

        public bool IsPersonSuitableByLevel(ArenaPersonSubscribeData personGetGroup, ArenaPersonSubscribeData personForFilter, long timeSpan)
        {
            int levelShift = 0;
            var matchInfo = arenaAlgorithmData.GetMatchData(personGetGroup.ArenaMatchButtonId);
            if(matchInfo == null)
            {
                ILogger.Instance.Send($"IsPersonSuitableByLevel personGetGroup: {personGetGroup.LoginId} personForFilter: {personForFilter.LoginId} personGetGroup.ArenaMatchButtonId: {personGetGroup.ArenaMatchButtonId}, personForFilter.ArenaMatchButtonId: {personForFilter.ArenaMatchButtonId}", ErrorLevel.exception);
                return false;
            }

            levelShift = (int)matchInfo.LevelOffset.Calc(timeSpan);

            int minLevel = personGetGroup.Level - levelShift;// levelOffset - levelShift;
            int maxLevel = personGetGroup.Level + levelShift;

            return minLevel <= personForFilter.Level && personForFilter.Level <= maxLevel;
        }

        public bool IsPersonSuitableByRating(ArenaPersonSubscribeData personGetGroup, ArenaPersonSubscribeData personForFilter, long timeSpan)
        {
            var matchInfo = arenaAlgorithmData.GetMatchData(personGetGroup.ArenaMatchButtonId);

            int ratingShift = (int)matchInfo.RatingOffset.Calc(timeSpan);

            int ratingPersonGetGroup = personGetGroup.getRating();
            int ratingPersonForFilter = personForFilter.getRating();
            int minRating = ratingPersonGetGroup - ratingShift;
            int maxRating = ratingPersonGetGroup + ratingShift;

            return minRating <= ratingPersonForFilter && ratingPersonForFilter <= maxRating;
        }

        public List<int> Subscribe(ArenaPersonSubscribeData apsd, int matchButtonId, long current)
        {
            ArenaPersonSubscribeData personSubscribeData2 = getPerson(apsd.PersonId);
            if (personSubscribeData2 != null)
            {
                //TODO fil надо подумать над этими данными стоит ли менять их во время матчинга
                //ArenaPersonSubscribeData personSubscribeData2 = this.persons[apsd.PersonId];
                personSubscribeData2.Rating = apsd.Rating;
                personSubscribeData2.HeroPower = apsd.HeroPower;
                personSubscribeData2.lastOnline = current;
                personSubscribeData2.createSubscribeTime = current;
                //personSubscribeData2.ArenaMatchButtonId = matchButtonId; не надо менять батонАйди когда игрок уже в матчинге
            }
            else
            {
                if (matchButtonId != -1)
                {
                    apsd.createSubscribeTime = current;
                    AddPerson(apsd);
                }
            }

            if (matchButtonId != -1)
            {
                AddToSubscribes(matchButtonId, apsd);
            }

            return GetSubscribesList(apsd.PersonId);
        }

    }

    public class PreGroup
    {
        public PreGroup(ArenaPersonSubscribeData lid)
        {
            lider = lid;
            persons = new List<ArenaPersonSubscribeData>();
            persons.Add(lider);
        }

        public List<ArenaPersonSubscribeData> persons;

        public ArenaPersonSubscribeData lider;

        public void AddPerson(ArenaPersonSubscribeData person)
        {
            persons.Add(person);
        }
    }

    public class SubscribeGroup
    {
        public List<int> persons;
        protected object persons_object = new object();

        public SubscribeGroup()
        {
            persons = new List<int>();
        }

        internal bool Remove(int personId, PersonType personType)
        {
            lock (persons_object)
            {
                return persons.Remove(personId);
            }
        }

        internal void AddToSubscribe(ArenaPersonSubscribeData apsd)
        {
            lock (persons_object)
            {
                if (!persons.Contains(apsd.PersonId))
                    persons.Add(apsd.PersonId);
                else
                    ILogger.Instance.Send($"PreGroup AddToSubscribe already exist apsd: {apsd.PersonId}", ErrorLevel.error);
            }                
        }

        internal bool Contains(int personId)
        {
            lock (persons_object)
            {
                return persons.Contains(personId);
            }
        }
    }
}