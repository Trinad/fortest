﻿using System.Collections.Generic;
using NodeGuild.DB;
using UtilsLib.NetworkingData;

namespace NodeGuild.Logic
{
	public class FriendManager
	{
		private readonly Dictionary<int, GuildPersonInfo> logins = new Dictionary<int, GuildPersonInfo>();

		public GuildPersonInfo GetPerson(int loginId)
		{
			if (!logins.TryGetValue(loginId, out var data))
				logins[loginId] = data = DBStoreManager.GetAdapter().GetLoginTinyInfo(loginId);

			return data;
		}

		/*public List<GuildPersonInfo> GetPlayTogether(int loginId)
		{
			var result = new List<GuildPersonInfo>();

			foreach (var login in logins.Values)
				if(login.LoginId != loginId)
					result.Add(login);

			return result;
		}*/

		public void OnUpdateLoginData(LoginData loginData)
		{
			if (logins.TryGetValue(loginData.LoginId, out var data))
			{
				data.PersonName = loginData.Name;
				data.AvatarName = loginData.Avatar;
			}
			else
			{
				logins[loginData.LoginId] = new GuildPersonInfo(loginData.LoginId, loginData.Name, loginData.Avatar, ClanRole.None, 0);
			}
		}

		public void OnRatingScoreChange(int loginId, int ratingScore)
		{
			if (logins.TryGetValue(loginId, out var data))
			{
				data.RatingScore = ratingScore;
			}
			else
			{
				logins[loginId] = new GuildPersonInfo(loginId, string.Empty, string.Empty, ClanRole.None, ratingScore);
			}
		}
	}
}
