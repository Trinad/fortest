﻿using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json;
using UtilsLib.NetworkingData;
using UtilsLib;

namespace NodeGuild.Logic.Entries
{
    public class Guild
    {
        public GuildData guildData;

	    private long m_leaderId;
		public long LeaderId
	    {
		    get { return m_leaderId; }
			set
			{
				m_leaderId = value;
				bChanged = true;
			}
	    }

	    //список персонажей
        public List<GuildPersonInfo> Persons { get; private set; }
		public int PersonsCount => Persons.Count;

	    public bool deleted = false;

        //список инвайтов, список запросов на вступление и т.д. 
        public List<GuildJoin> Joins = new List<GuildJoin>();
        public List<GuildInvite> Invites = new List<GuildInvite>();

	    public bool bGetFromDB { get; set; }

	    public volatile bool bChanged;

        /// <summary>
        /// Используется для временного хранения сообщений клана, пока не будет создан канал чата
        /// </summary>
        public List<string> notSendedMessages = new List<string> ();

        public Guild()
        {
	        Persons = new List<GuildPersonInfo>();
	        GuildRoomGameId = -1;
	        bChanged = true;
        }

        internal void AddJoin(GuildJoin guildJoin)
        {
            Joins.Add(guildJoin);
	        bChanged = true;
        }

	    internal void RemoveJoin(int targetLoginId)
	    {
		    for (int i = Joins.Count - 1; i >= 0; i--)
		    {
			    if (Joins[i].loginId == targetLoginId)
			    {
				    Joins.RemoveAt(i);
				    bChanged = true;
				}
		    }
	    }

	    internal void AddInvite(GuildInvite invite)
	    {
		    Invites.Add(invite);
		    bChanged = true;
	    }

		internal void RemoveInvite(long loginId)
        {
            for (int i = Invites.Count - 1; i >= 0; i--)
            {
                if (Invites[i].loginId == loginId)
                {
                    Invites.RemoveAt(i);
	                bChanged = true;
                }
            }
        }

        public void FillFromDataRow(UtilsLib.corelite.DBRecordCollection dr)
        {
            int GuildId = dr.GetInt("Id");
            string GuildName = dr.GetString("Name");
            string GuildDescription = dr.GetString("Description");
            
            ClanType GuildType = (ClanType)dr.GetInt("Type");
            int MinRatingScore = dr.GetInt("MinRatingScore");
            int GuildUTCLocation = dr.GetInt("Time");
            
            var EmblemType = (ClanEmblemType)dr.GetInt("EmblemType");
            int EmblemId = dr.GetInt("EmblemId");
			int PatternId = dr.GetInt("PatternId");
            string EmblemColor = dr.GetString("EmblemColor");
            string PatternColor = dr.GetString("PatternColor");
            string FlagColor = dr.GetString("FlagColor");

            guildData = new GuildData
            {
                ClanId = GuildId,
                ClanName = GuildName,
                ClanDescription = GuildDescription,
                ClanType = GuildType,
                GuildUTCLocation = GuildUTCLocation,
                MinRatingScore = MinRatingScore,
				EmblemData = new ClanEmblemData
				{
					ClanEmblemType = EmblemType,
					ClanEmblemIconId = EmblemId,
					ClanPatternIconId = PatternId,
					ClanEmblemColorId = int.TryParse(EmblemColor, out var res0) ? res0 : 0,
					ClanPatternColorId = int.TryParse(PatternColor, out var res1) ? res1 : 0,
					ClanFlagColorId = int.TryParse(FlagColor, out var res2) ? res2 : 0,
				}
            };
			
            LeaderId = dr.GetInt("LeaderId");
	        Persons = JsonConvert.DeserializeObject<List<GuildPersonInfo>>(dr.GetString("Persons")) ?? new List<GuildPersonInfo>();
            Joins = JsonConvert.DeserializeObject<List<GuildJoin>>(dr.GetString("Joins")) ?? new List<GuildJoin>();
            Invites = JsonConvert.DeserializeObject<List<GuildInvite>>(dr.GetString("Invites")) ?? new List<GuildInvite>();

			guildData.MembersCount = PersonsCount;
            guildData.RatingScore = dr.GetInt("RatingScore");
            guildData.BestPlace = dr.GetInt("BestPlace");// TODo fil времемнный костыль для хранения значения
            bGetFromDB = true;
	        bChanged = false;
        }

		public void FillCommandCreate(IDbCommand command)
		{
			command.AddParameter("@Name", guildData.ClanName);
			command.AddParameter("@Description", guildData.ClanDescription);
			command.AddParameter("@Type", (int)guildData.ClanType);
			command.AddParameter("@MinRatingScore", guildData.MinRatingScore);
			command.AddParameter("@Time", guildData.GuildUTCLocation);
			command.AddParameter("@EmblemType", (int)guildData.EmblemData.ClanEmblemType);
			command.AddParameter("@EmblemId", guildData.EmblemData.ClanEmblemIconId);
			command.AddParameter("@PatternId", guildData.EmblemData.ClanPatternIconId);
			command.AddParameter("@EmblemColor", guildData.EmblemData.ClanEmblemColorId.ToString());
			command.AddParameter("@PatternColor", guildData.EmblemData.ClanPatternColorId.ToString());
			command.AddParameter("@FlagColor", guildData.EmblemData.ClanFlagColorId.ToString());
			command.AddParameter("@LeaderId", LeaderId);
			command.AddParameter("@Persons", JsonConvert.SerializeObject(Persons));
			command.AddParameter("@Joins", JsonConvert.SerializeObject(Joins));
			command.AddParameter("@Invites", JsonConvert.SerializeObject(Invites));
			command.AddParameter("@RatingScore", guildData.RatingScore);
			command.AddParameter("@BestPlace", guildData.BestPlace);// TODo fil времемнный костыль для хранения значения
		}

		public void FillCommandUpdate(IDbCommand command)
		{
			command.AddParameter("@Id", guildData.ClanId);
			FillCommandCreate(command);
		}

        internal ClanRole GetRole(InstancePersonData instancePersonData)
        {
            return GetRole(instancePersonData.loginId);
        }

	    internal ClanRole GetRole(int loginId)
	    {
		    foreach (GuildPersonInfo guildPerson in Persons)
			    if (guildPerson.LoginId == loginId)
				    return guildPerson.RoleId;

		    return ClanRole.None;
	    }

        internal void RemoveLogin(int loginId)
        {
            for (int i = Persons.Count - 1; i >= 0; i--)
            {
                if (Persons[i].LoginId == loginId)
                {
	                Persons.RemoveAt(i);
	                bChanged = true;
                    break;
                }
            }

            guildData.MembersCount = PersonsCount;
        }

        internal GuildPersonInfo AddPerson(int loginId, string personName, string avatarName, ClanRole roleId, int ratingScore)
        {
			if (Persons.Count == 0)
				roleId = ClanRole.Commander;

            var guildPersonInfo = new GuildPersonInfo(loginId, personName, avatarName, roleId, ratingScore);
	        Persons.Add(guildPersonInfo);
	        bChanged = true;

            guildData.MembersCount = PersonsCount;

			return guildPersonInfo;
        }

        internal void RenamePerson(int loginId, string name, string avatarName)
        {
	        foreach (GuildPersonInfo guildPerson in Persons)
	        {
		        if (guildPerson.LoginId == loginId)
		        {
			        guildPerson.PersonName = name;
					guildPerson.AvatarName = avatarName;
			        bChanged = true;
			        return;
		        }
	        }
        }

        internal GuildPersonInfo GetPerson(int loginId)
        {
            foreach (GuildPersonInfo guildPerson in Persons)
            {
                if (guildPerson.LoginId == loginId)
                    return guildPerson;
            }
            return null;
        }

        public string LeaderName 
        { 
            get
            {
	            foreach (GuildPersonInfo guildPerson in Persons)
		            if (guildPerson.RoleId == ClanRole.Commander)
			            return guildPerson.PersonName;
	            return string.Empty;
            } 
        }

        public int GuildRoomGameId { get; private set; }

        internal GuildPersonInfo getNewOwner()
        {
            GuildPersonInfo res = null;
            foreach (GuildPersonInfo guildPerson in Persons)
            {
	            if (res == null)
		            res = guildPerson;
	            else
	            {
		            if ((int)guildPerson.RoleId > (int)res.RoleId)
			            res = guildPerson;
	            }
            }
            return res;
        }

	    public override string ToString()
	    {
		    return guildData == null ? "" : guildData.ToString();
	    }
    }
}
