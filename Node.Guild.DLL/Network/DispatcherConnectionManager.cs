﻿using System;
using Fragoria.Common.Utils.Threads;
using UtilsLib;

namespace NodeGuild.Network
{
	public sealed class DispatcherConnectionManager : OneThreadTimer
	{
		public DispatcherConnectionManager(int iPeriodMilliseconds) : base("ConnectionManager", iPeriodMilliseconds)
		{
		}

		public override void OnTimer()
		{
			if (GuildServer.Instance.bStarted)
				ConnectToDispatcher();
		}

		private void ConnectToDispatcher()
		{
			try
			{
                GuildServer.Instance.Dispatcher.DoPing();
				if (GuildServer.Instance.Dispatcher.DsConnection == null)
				{
					ILogger.Instance.Send("Попытка подключиться к диспетчеру узлов");
                    GuildServer.Instance.Dispatcher.ReconnectToDispatcher();
				}
			}
			catch (Exception exception)
			{
				ILogger.Instance.Send($"DispatcherConnectionManager.ConnectToDispatcher: {exception}", ErrorLevel.error);
			}
		}
	}
}
