﻿using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Common;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using Assets.Scripts;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Addin;
using NodeInstance.Tests.HelperClasses;
using UtilsLib.Logic;

namespace NodeInstance.Tests.PathTest
{
    [TestFixture]
    public class PathTest
    {
        MapPath mapPath;
        Map map;
        float moveSpeed = 3;

        [OneTimeSetUp]
        public void SetUp()
        {
			var levelName = "test_level_1";
			ServerStartGameManager.levelName = levelName;
            Map.LoadJsonByUrl(PathHelper.TestPathBuilder.GetZoneUrl(levelName));
            DeepPathFinderAlgorithm.Inited = false;

            MapObject.MapObjectConstants = ServerConstants.Instance.MapObjectConstants.ToDictionary(x => x.name);

            var notify = NSubstitute.Substitute.For<INotifyController>();
            map = new Map(null, notify) { LevelName = levelName };
            Map.Edge = new EdgeLocationXmlData { LevelName = levelName };
            map.RequestLevel(new List<Scenario>(), levelName, "");
            mapPath = new MapPath(MapPathArray.iMaxSize);

		}

		private PathFindData GetDefaultPathFindData()
		{
            float StartX = 35.49705f;
            float StartZ = 8.895807f;
            float EndX = 53.70155f;
            float EndZ = 15.99973f;
            var pathFindData = new PathFindData();
            pathFindData.StartX = StartX;
            pathFindData.StartZ = StartZ;
            pathFindData.moveCost = ServerPathFinderAlgorithm.MonsterAngryMoveCost;
            pathFindData.map = map;
            pathFindData.EndX = EndX;
            pathFindData.EndZ = EndZ;
            pathFindData.timelifePath = 0.5f;
            pathFindData.pointLimit = ServerPathFinderAlgorithm.iMaxPointSearched;
			return pathFindData;
		}

        [Test]
        public void Path_PathFindAStarTest()
        {
            var astar = new PathFinderAStar();
			var pathFindData = GetDefaultPathFindData();
            astar.MakePath(mapPath, pathFindData);

            mapPath.Start();
            CheckPathPassable(mapPath, pathFindData, moveSpeed);
        }

        [Test]
        public void Path_PathFindDeepTest()
        {
            var deep = new PathFinderDeep(map);
			var pathFindData = GetDefaultPathFindData();
            deep.MakePath(mapPath, pathFindData);

            mapPath.Start();
            CheckPathPassable(mapPath, pathFindData, moveSpeed);
        }

        [Test]
        public void Path_PathFindDeepMultuTest()
        {
            Path_PathFindMultuTest(new PathFinderDeep(map));
        }

        [Test]
        public void Path_PathFindAStarMultuTest()
        {
            Path_PathFindMultuTest(new PathFinderAStar());
        }

		//[Test]
		//public void Path_PathFinderDirectLineMultuTest()
		//{
		//	Path_PathFindMultuTest(new PathFinderDirectLine());
		//}

        public void Path_PathFindMultuTest(IPathFinder pathFinder)
        {
            Random r = new Random(1);
            for (int i = 0; i < 1000; i++)
            {
                int startX = r.Next(Map.HeightX);
                int startZ = r.Next(Map.HeightZ);
                if (!map.IsTransparent(startX, startZ))
                    continue;

				int endX = r.Next(Map.HeightX);
				int endZ = r.Next(Map.HeightZ);

                var pathFindData = new PathFindData();
                pathFindData.StartX = Helper.CellToPosition(startX);
                pathFindData.StartZ = Helper.CellToPosition(startZ);
                pathFindData.moveCost = ServerPathFinderAlgorithm.MonsterAngryMoveCost;
                pathFindData.map = map;
                pathFindData.EndX = Helper.CellToPosition(endX);
                pathFindData.EndZ = Helper.CellToPosition(endZ);

                pathFinder.MakePath(mapPath, pathFindData);

                if (mapPath.PathLength != 0)
                {
					mapPath.Smooth(map);
					mapPath.Smooth(map);

					mapPath.Start();

					CheckPathPassable(mapPath, pathFindData, moveSpeed);

					//mapPath.SavePicturePath(map, $"D:\\path\\{i}_{pathFinder.GetType().Name}.png", bitmap =>
					//{
					//	/*if (pathFinder is PathFinderDeep)
					//	{
					//		List<int> lst = DeepPathFinderAlgorithm.FindPath(map, startX, startZ, endX, endZ);
					//		var gr = Graphics.FromImage(bitmap);

					//		for (int j = 0; j < lst.Count - 2; j += 2)
					//			gr.DrawLine(new Pen(Color.Fuchsia), lst[j] * 3 + 1, lst[j + 1] * 3 + 1, lst[j + 2] * 3 + 1, lst[j + 3] * 3 + 1);
					//	}*/

					//	for (int p = 0; p < 9; p++)
					//	{
					//		bitmap.SetPixel(startX * 3 + p % 3, startZ * 3 + p / 3, Color.Lime);
					//		bitmap.SetPixel(endX * 3 + p % 3, endZ * 3 + p / 3, Color.Red);
					//	}
					//});
                }
            }
        }


        [Test]
        public void Path_PathFindDirectLineTest()
        {
            var direct = new PathFinderDirectLine();
			var pathFindData = GetDefaultPathFindData();
            direct.MakePath(mapPath, pathFindData);

            mapPath.Start();
            CheckPathPassable(mapPath, pathFindData, moveSpeed, false);
        }

        [Test]
        public void Path_AddPointTest()
        {
            var path = new MapPath();

            int count = 10;
            for (int i = 0; i < count; i++)
                path.AddPointCentered(i, i);

            Assert.AreEqual(count, path.PathLength);

            path.Start();
			CheckPathPassable(path, null, moveSpeed, false);

            Assert.AreEqual(path.PathLength, count);

            path.Clear();

            Assert.AreEqual(path.PathLength, 0);
        }

        public void CheckPathPassable(MapPath path, PathFindData pathFindData, float speed, bool checkCorrect = true)
        {
            Vector2 start = path.GetPointByIndex(0);

			if(pathFindData != null)
				if (Math.Abs(start.X - pathFindData.StartX) > 0.33 || Math.Abs(start.Y - pathFindData.StartZ) > 0.33)
					Assert.Fail("еще не начали идти, а уже оказались не в начальной точке!");

            for (int i = 1; i < path.PathLength; i++)
            {
                Vector2 next = path.GetPointByIndex(i);
                float distance = (start - next).magnitude();

                if (distance < 0.001)
                    Assert.Fail("Подозритльный путь. Слишком близко точки");

                if (distance > 1)
                    Assert.Fail("Подозритльный путь. Слишком далекие точки");

				if (checkCorrect)
				{
					var point = map.UltraCorrectYPosition(start.Vector2ToVector3());
					var forward = map.UltraCorrectYPosition(next.Vector2ToVector3());

					var pointUltra = map.UltraCorrectXZPosition(point, forward, false);
					if (Math.Abs(pointUltra.X - forward.X) > 0.2 || Math.Abs(pointUltra.Z - forward.Z) > 0.2)
						Assert.Fail("Подозритльный путь. Не смогли пройти UltraCorrectXZPosition");

					//var pointUltraMega = map.UltraMegaCorrectXZPosition(point, forward, false, false);
					//if (Math.Abs(pointUltraMega.X - forward.X) > 0.03 || Math.Abs(pointUltraMega.Z - forward.Z) > 0.03)
					//	Assert.Fail("Подозритльный путь. Не смогли пройти UltraMegaCorrectXZPosition");
				}

				start = next;
            }

			for (int i = 0; i < path.PathLength; i++)
				for (int j = i + 1; j < path.PathLength; j++)
				{
					var point_i = path.GetPointByIndex(i);
					var point_j = path.GetPointByIndex(j);
					if ((point_i - point_j).magnitude() < 0.001)
						ILogger.Instance.Send("Подозритльный путь. Одинаковые точки", ErrorLevel.error);
				}

            Vector3 movePoint = path.GetPointByIndex(0).Vector2ToVector3();
            int cooldownMoveMS = (int)(1000 / speed);
            int step = 0;
            while (!path.Ended && step < 10000)
            {
                step++;
                Vector2 Force = path.GetForce(movePoint, path.dtLastRecalc + MapTargetObject.SEND_RATE, cooldownMoveMS);
                if (Force == Vector2.Zero)
                    break;

                movePoint = movePoint + Force.Vector2ToVector3();

				//int zoom = 256;
				//int movePointX = Helper.GetCell(movePoint.X);
				//int movePointZ = Helper.GetCell(movePoint.Z);
				//mapPath.SavePicturePathLoopa(map, $"D:\\path\\{step}.png", movePointX, movePointZ, zoom,
				//	bitmap =>
				//	{
				//		var pen = new Pen(Color.Red);
				//		var gr = Graphics.FromImage(bitmap);

				//		var x1 = (int)((movePoint.X - movePointX * Helper.VoxelSize) * zoom) + bitmap.Width / 2;
				//		var y1 = (int)((movePoint.Z - movePointZ * Helper.VoxelSize) * zoom) + bitmap.Height / 2;

				//		var movePoint0 = movePoint - Force.Vector2ToVector3();

				//		var x2 = (int)((movePoint0.X - movePointX * Helper.VoxelSize) * zoom) + bitmap.Width / 2;
				//		var y2 = (int)((movePoint0.Z - movePointZ * Helper.VoxelSize) * zoom) + bitmap.Height / 2;

				//		gr.DrawLine(pen, x1, y1, x2, y2);
				//		gr.DrawEllipse(pen, x2 - 4, y2 - 4, 9, 9);
				//	}
				//);
            }

            Assert.True(path.Ended, $"долго шли, но путь так и не закончился. steps: {step}");

            float x, y;
            path.GetLastPoint(out x, out y);
            if (Math.Abs(movePoint.X - x) > 0.33 || Math.Abs(movePoint.Z - y) > 0.33)
                Assert.Fail("вроде дошли до конца пути, но оказались не в конечной точке!");
        }

        [Ignore("Тяжелый и не всегда нужен!")]
        public void Path_PathBuildingComparationTest()
        {
            int testCount = 10000;

            float StartX = 35.49705f;
            float StartZ = 8.895807f;
            float EndX = 53.70155f;
            float EndZ = 15.99973f;
            float radius = 4;//(int)(4 / 0.2f);
            long firastTime = 0;
            long secondtime = 0;

            Stopwatch stopWatch = new Stopwatch();

            DateTime oldPathLastRecalc = TimeProvider.UTCNow;
            MapPathArray mp = new MapPathArray(MapPathArray.iMaxSize);
            ServerPathFinderAlgorithm.MakePath(mp, StartX, StartZ, EndX, EndZ, map, radius, ServerPathFinderAlgorithm.MonsterAngryMoveCost);
            mp.Smooth();
            mp.Smooth();

            stopWatch.Restart();
            for (int r = 0; r < testCount; r++)
            {
                ServerPathFinderAlgorithm.MakePath(mp, StartX, StartZ, EndX, EndZ, map, radius, ServerPathFinderAlgorithm.MonsterAngryMoveCost);
                mp.Smooth();
                mp.Smooth();
            }
            stopWatch.Stop();
            secondtime = stopWatch.ElapsedMilliseconds;

            var pathFindData = new PathFindData();
            pathFindData.StartX = StartX;
            pathFindData.StartZ = StartZ;
            pathFindData.moveCost = ServerPathFinderAlgorithm.MonsterAngryMoveCost;
            pathFindData.map = map;
            pathFindData.EndX = EndX;
            pathFindData.EndZ = EndZ;
            pathFindData.timelifePath = 0.5f;
            pathFindData.radius = (int)radius / 0.2f;
            pathFindData.pointLimit = ServerPathFinderAlgorithm.iMaxPointSearched;
            pathFindData.altitude = 2;
            ServerPathFinderAlgorithm.MakePath(mapPath, pathFindData);

            mapPath.Smooth(map);
            mapPath.Smooth(map);

            stopWatch.Restart();
            for (int i = 0; i < testCount; i++)
            {
                pathFindData.StartX = StartX;
                pathFindData.StartZ = StartZ;
                pathFindData.moveCost = ServerPathFinderAlgorithm.MonsterAngryMoveCost;
                pathFindData.map = map;
                pathFindData.EndX = EndX;
                pathFindData.EndZ = EndZ;
                pathFindData.timelifePath = 0.5f;
                pathFindData.radius = (int)radius/0.2f;
                pathFindData.pointLimit = ServerPathFinderAlgorithm.iMaxPointSearched;
                pathFindData.altitude = 2;
                ServerPathFinderAlgorithm.MakePath(mapPath, pathFindData);

                mapPath.Smooth(map);
                mapPath.Smooth(map);
            }
            stopWatch.Stop();
            firastTime = stopWatch.ElapsedMilliseconds;
            ILogger.Instance.Send("Test get=" + firastTime + " was=" + secondtime);
            Assert.That(firastTime, Is.LessThan(secondtime));
        }
    }
}
