﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace NodeInstance.Tests
{
	public class TestConfig
    {
        [XmlElement("LocalizationPath", typeof(string))]
        public List<string> LocalizationPaths;

        [XmlElement("ZonesPath", typeof(string))]
        public List<string> ZonesPaths;

        public string GetCorrectLocalizationPath()
        {
            foreach (var path in LocalizationPaths)
            {
                if (Directory.Exists(path))
                    return path;
            }
            return null;
        }

        public string GetCorrectZonesPath()
        {
            foreach (var path in ZonesPaths)
            {
                if (Directory.Exists(path))
                    return path;
            }
            return null;
        }
    }
}
