﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.Networking;
using Assets.Scripts.Networking.ClientDataRPC;
using Assets.Scripts.Utils;
using NodeInstance.Tests.HelperClasses;
using NSubstitute;
using NUnit.Framework;
using UtilsLib.Logic;

namespace NodeInstance.Tests.Emotion
{
    [TestFixture]
    public class EmotionTest
    {
        PathBuilder pathBuilder;
		IEmotionController emotionController;
		EmotionManager emotionManager;
        ShopManager shopManager;
        InstancePerson instancePerson;
        ITimeProvider timeProvider;

		[OneTimeSetUp]
        public void Setup()
        {
            pathBuilder = PathHelper.TestPathBuilder;
			shopManager = ObjectMother.GetShopManager();
			var serverConstants = new ServerConstants();            

            timeProvider = Substitute.For<ITimeProvider>();
            timeProvider.TimeUTCNow.Returns(new DateTime(2017, 10, 5, 19, 10, 0));

			emotionManager = new EmotionManager(serverConstants, pathBuilder.EmotionXml.LoadFromFileXml<EmotionSettings>(), shopManager);
			emotionController = ObjectMother.GetEmotionController();

			instancePerson = ObjectMother.GetDefaultInstancePerson();

			emotionController.CreateEmotionPerson(instancePerson, new List<Assets.Scripts.InstanceServer.Entries.Person.Emotion>(), new List<Assets.Scripts.InstanceServer.Entries.Person.Emotion>());			
        }


        [Test]
        public void IsXmlMatches()
        {
            var emotionList = emotionManager.EmotionSettings.EmotionList;
            var ismatches = emotionList.Count(xmlData => emotionList.Exists(e => e.Id == xmlData.Id && e != xmlData)) > 0;
            Assert.False(ismatches);
        }

        // выбираем только группу халявных
        [Test]
        public void GetFreeGroupCount()
        {
            var freeGroup = emotionManager.EmotionSettings.EmotionList.FindAll(e => e.Group == EmotionGroup.Free);
            Assert.AreEqual(5, freeGroup.Count);
        }
        // проверка конвертации
        [Test]
        public void EmotionXmlConvertToEmotion()
        {
            var emoXml = new EmotionXmlData();
            var castedEmo = (global::Assets.Scripts.InstanceServer.Entries.Person.Emotion)emoXml;
            Assert.IsNotNull(castedEmo);
        }
        // проверяем список покупок после обновления (происходит по честнову раз в сутки при входе)
        [Test]
        public void GetEmotionForBuyPerson()
        {
            instancePerson.EmotionPerson.SetLastRefreshTime(timeProvider.TimeUTCNow);
            emotionController.CheatsRefreshEmotions(instancePerson);
            
            Assert.AreEqual(3, instancePerson.EmotionPerson.AvailableEmotionsForBuy.Count);
        }

        // проверка метода который собирает список доступных покупок на отправку клиенту
        [Test]
        public void SendEmotionsInfoResponse()
        {
            var buyList = emotionManager.GetRpcEmotionsForBuy(instancePerson).Select(e => (EmotionData)e).ToList();
            Assert.AreEqual(3, buyList.Count);
        }
        
        [Test]
        public void IsDifferent()
        {
            var list = emotionManager.GetRpcEmotionsForBuy(instancePerson);
            var result = list.Count(data => list.Exists(d => d.Id == data.Id && d != data));
            Assert.AreEqual(0, result);
        }

       
        [Test]
        public void UseEmotionRequest()
        {
			var forUse = instancePerson.EmotionPerson.AvailableEmotions.FirstOrDefault(emotion => emotion.Group == EmotionGroup.Free);
			emotionManager.UseEmotion(instancePerson, forUse.Id);
			var updateCount = emotionManager.UsingEmotionsPerson.Count;
            Assert.AreEqual(1, updateCount);
        }

        [Test]
        public void AddKillBossSmile()
        {
			emotionManager.UseEmotion(instancePerson, 0);
			var count = emotionManager.UsingEmotionsPerson.Count;
            Assert.AreEqual(1, count);
        }
    }
}