﻿using NUnit.Framework;
using Node.Rating;
using Node.Rating.Logic.RatingInstances;
using Node.Rating.Logic.RatingInstances.GameStatistic;
using Node.Rating.Logic;
using Node.Rating.Utils;

namespace NodeInstance.Tests.Rating
{
    [TestFixture]
    class RatingTest
    {
        RatingServer ratingServer;
        [OneTimeSetUp]
        public void Setup()
        {
            //var config = new ConfigurationManager();
            //Node.Rating.DLL.CompositionRoot.Init(System.Configuration.ConfigurationManager.AppSettings);
            SystemConfig config = new SystemConfig(new System.Collections.Specialized.NameValueCollection());
            ratingServer = new RatingServer();
            ratingServer.StartManagers();
            XmlStorage.Create();
            XmlStorage.Instance.LoadXML();
        }

        [Test]
        public void Init()
        {
            var storage = new LoginStorage(ratingServer);
            var statistics = new LoginGameStatistic();
            statistics.LoginId = 1;
            statistics.SetDBrecordNew(1);
            var person = new PersonGameStatistic();
            person.PersonId = 1;
            statistics.Persons.Add(person);
            storage.AddLoginGameStatistic(statistics);
            storage.Apply(1, 1);
        }
    }
}
