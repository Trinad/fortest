﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Assets.Scripts;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.PassiveAbilitys;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.Perks;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.Networking.ClientDataRPC;
using NodeInstance.Tests.HelperClasses;
using NUnit.Framework;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;

namespace NodeInstance.Tests.Localization
{
    [TestFixture]
    public class LocalizationTest
    {
        private InstancePerson instancePerson;
        private InfluenceXmlDataList influences;
        private List<Elixir> elixirs;
        private List<InventoryItemData> items;
        private PlayerCityData playerCityData;
        private PersonDataList personDatas;
        private List<GridRowLoc> clientLocRows;
        private List<PassiveAbilityBase> passiveAbilitys;
        private SpecialOffer specialOffer;
        private Edges edges;
        private QuestCollection questCollection;
        private PerkDataList PerkList;

        private TestConfig config;
        private PathBuilder m_pathBuilder;
        private Dictionary<string, string> lngRU;
        private Dictionary<string, string> lngEN;

        private PathBuilder GetPaths()
        {
            return m_pathBuilder;
        }

        [OneTimeSetUp]
        public void Setup()
        {
            m_pathBuilder = PathHelper.TestPathBuilder;
            m_pathBuilder.FormulasXml.LoadFromFileXml<Assets.Scripts.InstanceServer.Formulas>();

			var gameXmlData = ObjectMother.GetEmptyGameXmlData(personAbility: true);

            instancePerson = new InstancePerson(1234, null);
			instancePerson.personType = PersonType.Warrior;
			instancePerson.SetGameXmlData(gameXmlData);
            instancePerson.StatContainer = new PersonStatContainer(instancePerson);

            influences = m_pathBuilder.InfluenceXml.LoadFromFileXml<InfluenceXmlDataList>();
            elixirs = GetPaths().ElixireXml.LoadFromFileXml<List<Elixir>>();
            playerCityData = GetPaths().PlayerCityXml.LoadFromFileXml<PlayerCityData>();
            playerCityData.Init();
            var itemRepository = GetPaths().ItemDataRepositoryDataXml.LoadFromFileXml<ItemDataRepository>();
            itemRepository.Init(playerCityData);
            items = itemRepository.Prototypes;
            
            personDatas = GetPaths().PersonXml.LoadFromFileXml<PersonDataList>();
            edges = GetPaths().EdgesXml.LoadFromFileXml<Edges>();
            specialOffer = GetPaths().SpecialOffersXml.LoadFromFileXml<SpecialOffer>();
            questCollection = GetPaths().QuestDataXml.LoadFromFileXml<QuestCollection>();
            PerkList = GetPaths().PerksXml.LoadFromFileXml<PerkDataList>();


            PassiveAbilityXmlDataList passiveAbilityXmlDataList = GetPaths().PassiveAbilitysXml.LoadFromFileXml<PassiveAbilityXmlDataList>();
            passiveAbilitys = passiveAbilityXmlDataList.Abilitys;

            config = "TestConfig.xml".LoadFromFileXml<TestConfig>();

            string path = config.GetCorrectLocalizationPath();
            if (path == null)
            {
                Assert.Ignore("укажите корректный путь до клиентской локализации в файле 'TestConfig.xml'");
            }

            clientLocRows = new List<GridRowLoc>();

            string finalPathManifest = Path.Combine(path, $"ManifestLLSheets.xml");
            Manifest manifestLoc = finalPathManifest.LoadFromFileXml<Manifest>();

            for(int sheetId = 0; sheetId < manifestLoc.CountSheets; sheetId++)
            {
                string finalPath = Path.Combine(path, $"sheet_{sheetId}.xml");
                sheet clientLoc = finalPath.LoadFromFileXml<sheet>();
                clientLocRows.AddRange(clientLoc.RowsForGrid);
            }

            lngRU = clientLocRows.ToDictionary(x => x.key, y => y.Loc1);
            lngEN = clientLocRows.ToDictionary(x => x.key, y => y.Loc2);
        }

        [Test]
        public void LocalizationInfluences_Test()
        {
            StringBuilder sbFail = new StringBuilder();
            StringBuilder sbWarn = new StringBuilder();
            foreach (var infl in influences.Influences)
            {
                if (infl.Icon != PlayerStatus.None && string.IsNullOrEmpty(infl.TechnicalName))
                {
                    sbFail.AppendLine($"Инфлуенс {infl.Id} имеет картинку, но не имеет техимени");
                }

                if (infl.Icon == PlayerStatus.None && !string.IsNullOrEmpty(infl.TechnicalName))
                {
                    sbFail.AppendLine($"Инфлуенс {infl.Id} не имеет картинку, но имеет техимя");
                }

                if (infl.Icon != PlayerStatus.None && infl.HintType == InfluenceHintType.None)
                {
                    sbWarn.AppendLine($"Инфлуенс {infl.Id} имеет картинку, но ему не назначена подложка в аттрибуте {nameof(infl.HintType)}");
                }
            }

            string logFail = sbFail.ToString();
            LogFail(logFail);

            string logWarn = sbWarn.ToString();
            LogWarn(logWarn);
        }

        private void LogFail(string log)
        {
            if (!string.IsNullOrEmpty(log))
            {
                ILogger.Instance.Send(log, ErrorLevel.error);
                Assert.Fail(log);
            }
        }

        private void LogWarn(string log)
        {
            if (!string.IsNullOrEmpty(log))
            {
                ILogger.Instance.Send(log, ErrorLevel.warning);
                Assert.Warn(log);
            }
        }

        [Test]
        public void LocalizationInfluencesRU_Test()
        {
            InflLocTest(lngRU, out var warnText, out var failText);

            LogWarn(warnText);
            LogFail(failText);
        }

        [Test]
        public void LocalizationInfluencesEN_Test()
        {
            InflLocTest(lngEN, out var warnText, out var failText);

            LogWarn(warnText);
            LogWarn(failText);
        }

        private void InflLocTest(Dictionary<string, string> lng, out string warnText, out string failText)
        {
            StringBuilder sbFail = new StringBuilder();
            StringBuilder sbWranings = new StringBuilder();
            foreach (var infl in influences.Influences)
            {
                if (!string.IsNullOrEmpty(infl.TechnicalName))
                {
					lng.TryGetValue($"influence.{infl.TechnicalName}_desc", out string loc);
                    if (loc == null)
                    {
                        //в локализации нет ключа
                        sbFail.AppendLine($"Influence {infl.Id} localization not contains a key influence.{infl.TechnicalName}_desc");
                        continue;
                    }

                    string shortloc = loc;

                    foreach (var attr in infl.LocalizationAttributes)
                    {
                        string resultReplace = shortloc.Replace("{_" + attr.Key + "_}", "[параметр есть]");
                        if (resultReplace == shortloc)
                            //В локализации нет параметра но он указан в сервере
                            sbWranings.AppendLine($"Инфлуенс {infl.Id} '{loc}'. localization not contains a params '{attr.Key}', but it is listed on the server");

                        shortloc = resultReplace;
                    }

                    if (shortloc.Contains('{') || shortloc.Contains('}'))
                        //В локализации есть лишний параметр, который не указан в сервере
                        sbFail.AppendLine($"Инфлуенс {infl.Id} '{shortloc}'. Localization has an extra parameter, which is not listed in the server");

                }
            }

            warnText = sbWranings.ToString();
            failText = sbFail.ToString();
        }

        [Test]
        public void LocalizationElixirsRU_Test()
        {
            ElixirLocTest(lngRU, out string warnText, out string failText);

            LogWarn(warnText);
            LogFail(failText);
        }

        [Test]
        public void LocalizationElixirsEN_Test()
        {
            ElixirLocTest(lngEN, out string warnText, out string failText);

            LogWarn(warnText);
            LogWarn(failText);
        }

        private void ElixirLocTest(Dictionary<string, string> lng, out string warnText, out string failText)
        {
            StringBuilder sbWranings = new StringBuilder();
            StringBuilder sbFail = new StringBuilder();
            foreach (var elixir in elixirs)
            {
                if (elixir.TechnicalName == null)
                {
                    //Эликсир не указан ключ локализации
                    sbFail.AppendLine($"Elixir {elixir.BaseId} localization key not specified");
                    continue;
                }

                lng.TryGetValue($"potions.{elixir.TechnicalName}_name", out string loc);
                if (loc == null)
                {
                    // Эликсир в локализации нет ключа с названием
                    sbFail.AppendLine($"Elixir {elixir.BaseId} localization not contains a key potions.{elixir.TechnicalName}_name");
                    continue;
                }
            }

            warnText = sbWranings.ToString();
            failText = sbFail.ToString();
        }

        [Test]
        public void LocalizationPerkRU_Test()
        {
            PerkLocTest(lngRU, out string warnText, out string failText);

            LogWarn(warnText);
            LogFail(failText);
        }

        [Test]
        public void LocalizationPerkEN_Test()
        {
            PerkLocTest(lngEN, out string warnText, out string failText);

            LogWarn(warnText);
            LogWarn(failText);
        }

        private void PerkLocTest(Dictionary<string, string> lng, out string warnText, out string failText)
        {
            StringBuilder sbWranings = new StringBuilder();
            StringBuilder sbFail = new StringBuilder();

            foreach (var perk in PerkList.PerkList)
            {
                if (perk.TechnicalName == null)
                {
                    //Перк не указан ключ локализации
                    sbFail.AppendLine($"Perk {perk.Id} localization key not specified");
                }
                else
                {
                    lng.TryGetValue(perk.TechnicalName, out string loc);
                    if (loc == null)
                    {
                        //Перк  в локализации нет ключа
                        sbFail.AppendLine($"Perk {perk.Id} localization not contains a key '{perk.TechnicalName}'");
                    }
                }

                TestLocalizationDescription(sbFail, sbWranings, perk.HUDDescription, lng);

                TestLocalizationDescription(sbFail, sbWranings, perk.Description, lng);
            }

            warnText = sbWranings.ToString();
            failText = sbFail.ToString();
        }

        private void TestLocalizationDescription(StringBuilder sbFail, StringBuilder sbWranings, LocalizationDescription descr , Dictionary<string, string> lng)
        {
            lng.TryGetValue(descr.LocKey, out string loc);
            if (loc == null)
            {
                //Описание  В локализации нет ключа
                sbFail.AppendLine($"Description {descr.LocKey}. localization key not specified");
                return;
            }

            string shortloc = loc;

            foreach (var attr in descr.LocalizationAttributes)
            {
                string resultReplace = shortloc.Replace("{_" + attr.Key + "_}", "[params exists]");
                if (resultReplace == shortloc)
                {
                    //Описание  В локализации нет параметра  но он указан в сервере
                    sbWranings.AppendLine($"Description {descr.LocKey}. localization not contains a params '{attr.Key}', but it is listed on the server");
                    sbWranings.AppendLine($"===============================");
                }

                shortloc = resultReplace;
            }

            if (shortloc.Contains('{') || shortloc.Contains('}'))
            {
                //Описание  В локализации есть лишний параметр, который не указан в сервере
                sbFail.AppendLine($"Description {descr.LocKey}. '{shortloc}'. Localization has an extra parameter, which is not listed in the server");
                sbFail.AppendLine($"===============================");
            }
        }


        [Test]
        public void LocalizationItemsRU_Test()
        {
            ItemsLocTest(lngRU, out string failText);
                        
            LogFail(failText);
        }

        [Test]
        public void LocalizationItemsEN_Test()
        {
            ItemsLocTest(lngEN, out string failText);
                        
            LogWarn(failText);            
        }

        private void ItemsLocTest(Dictionary<string, string> lng, out string failText)
        {            
            StringBuilder sbFail = new StringBuilder();

            foreach (var item in items)
            {
                if (string.IsNullOrEmpty(item.TechnicalName))
                    continue;

				switch (item.ItemType)
				{
					case ItemType.Potion:
						CheckKey($"Предмет {item.Name}", $"potions.{item.TechnicalName}_name", lng, sbFail);
						break;
					case ItemType.Building:
					case ItemType.BuildingKit:
						CheckKey($"Предмет {item.Name}", item.TechnicalName, lng, sbFail);
						break;
					default:
						CheckKey($"Предмет {item.Name}", $"items.wearables.{item.TechnicalName}_name", lng, sbFail);
						break;
				}
            }

            failText = sbFail.ToString();
        }


        [Test]
        public void LocalizationAbilityRU_Test()
        {
            AbilityLocTest(lngRU, out string warnText, out string failText);

            LogWarn(warnText);
            LogFail(failText);
        }

        [Test]
        public void LocalizationAbilityEN_Test()
        {
            AbilityLocTest(lngEN, out string warnText, out string failText);

            LogWarn(warnText);
            LogWarn(failText);
        }

        private void AbilityLocTest(Dictionary<string, string> lng, out string warnText, out string failText)
        {
            StringBuilder sbWranings = new StringBuilder();
            StringBuilder sbFail = new StringBuilder();

            foreach (var personData in personDatas.PersonDatas)
            {
                string stupidPrefix = null;
                switch (personData.PersonType)
                {
                    case PersonType.Archer: stupidPrefix = "archer"; break;
                    case PersonType.Warrior: stupidPrefix = "warrior"; break;
                    case PersonType.Wizard: stupidPrefix = "mage"; break;
                    case PersonType.Gunner: stupidPrefix = "gunner"; break;
                    case PersonType.Shaman: stupidPrefix = "shaman"; break;
                }

                foreach (var ability in personData.Abilities)
                {
                    if (!string.IsNullOrEmpty(ability.TechnicalName))
                    {
                        if (ability.TechnicalName == "autoattack")
                            continue;

						lng.TryGetValue($"ability.{stupidPrefix}.{ability.TechnicalName}_desc", out string loc);
                        if (loc == null)
                        {
                            //Умение  в локализации нет ключа
                            sbFail.AppendLine($"Ability {ability.Type} localization key not specified 'ability.{stupidPrefix}.{ability.TechnicalName}_desc'");
                            sbFail.AppendLine($"===============================");
                            continue;
                        }

                        string shortloc = loc;

                        foreach (var attr in ability.LocalizationAttributes)
                        {
                            string resultReplace = shortloc.Replace("{_" + attr.Key + "_}", "[параметр есть]");
                            if (resultReplace == shortloc)
                            {
                                //Умение  В локализации нет параметра но он указан в сервере
                                sbWranings.AppendLine($"Ability {ability.Type} '{loc}'. localization not contains a params '{attr.Key}', but it is listed on the server");
                                sbWranings.AppendLine($"===============================");
                            }

                            shortloc = resultReplace;
                        }

                        if (shortloc.Contains('{') || shortloc.Contains('}'))
                        {
                            //Умение В локализации есть лишний параметр, который не указан в сервере
                            sbFail.AppendLine($"Ability {ability.Type} '{shortloc}'. Localization has an extra parameter, which is not listed in the server");
                            sbFail.AppendLine($"===============================");
                        }

                    }
                }
            }

            warnText = sbWranings.ToString();
            failText = sbFail.ToString();

        }

        [Test]
        public void LocalizationPassiveAbilityRU_Test()
        {
            string log = PassiveAbilityLocTest(lngRU);
            LogWarn(log);
        }

        [Test]
        public void LocalizationPassiveAbilityEN_Test()
        {
            string log = PassiveAbilityLocTest(lngEN);
            LogWarn(log);
        }

        private string PassiveAbilityLocTest(Dictionary<string, string> lng)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var pability in passiveAbilitys)
            {
                CheckKey($"Talent {pability.Id} {pability.PersonType}", pability.Name, lng, sb);
                CheckKey($"Talent {pability.Id} {pability.PersonType}", pability.Description, lng, sb);

                foreach (var param in pability.PassiveParameters)
                {
                    CheckKey($"Talent {pability.Id} {pability.PersonType}", param.Name, lng, sb);
                }
            }

            return sb.ToString();
        }

        [Test]
        public void LocalizationMapRU_Test()
        {
            string log = MapLocTest(lngRU);
            LogWarn(log);
        }

        [Test]
        public void LocalizationMapEN_Test()
        {
            string log = MapLocTest(lngEN);
            LogWarn(log);
        }

        private string MapLocTest(Dictionary<string, string> lng)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var locationXmlData in edges.GetAllLocationXmlDatas())
            {
                CheckKey($"Map {locationXmlData.LevelName}", locationXmlData.DescriptionKey, lng, sb);
                CheckKey($"Map {locationXmlData.LevelName}", locationXmlData.TechnicalName, lng, sb);
            }

            return sb.ToString();
        }

        [Test]
        public void LocalizationPlayerCityDataRU_Test()
        {
            string log = PlayerCityDataLocTest(lngRU);
            LogWarn(log);
        }

        [Test]
        public void LocalizationPlayerCityDataEN_Test()
        {
            string log = PlayerCityDataLocTest(lngEN);
            LogWarn(log);
        }

        private string PlayerCityDataLocTest(Dictionary<string, string> lng)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var building in playerCityData.Buildings)
            {
                if (building.Default)
                    continue;

                CheckKey($"Building {building.Name}", building.Title, lng, sb);
                CheckKey($"Building {building.Name}", building.Desc, building.LocalizationAttributes, lng, sb);
            }

            foreach (var category in playerCityData.Categories)
            {
                foreach (var subcategory in category.Subcategories)
                    CheckKey($"subcategory {subcategory.SortId}", subcategory.Title, lng, sb);

                CheckKey($"category {category.SortId}", category.Title, lng, sb);
            }

            return sb.ToString();
        }
        private void CheckKey(string nameObj, string key, Dictionary<string, string> lng, StringBuilder sbFail)
        {
            if (key.Contains("fix_me_please"))
                return;

            if (key == null)
            {
                //не указан ключ локализации 
                sbFail.AppendLine($"{nameObj} localization key not specified ");
                return;
            }

            lng.TryGetValue(key, out string loc2);
            if (loc2 == null)
            {
                // в локализации нет ключа
                sbFail.AppendLine($"{nameObj} localization not contains a key '{key}'");
                sbFail.AppendLine($"===============================");
            }
        }

        private void CheckKey(string nameObj, string key, List<LocalizationAttribute> LocalizationAttributes, Dictionary<string, string> lng, StringBuilder sb)
        {
            if (key.Contains("fix_me_please"))
                return;

            if (key == null)
            {
                //не указан ключ локализации
                sb.AppendLine($"{nameObj} localization key not specified");
                return;
            }

            lng.TryGetValue(key, out string loc1);
            if (loc1 == null)
            {
                //в локализации нет ключа
                sb.AppendLine($"{nameObj} localization not contains a key '{key}'");
                sb.AppendLine($"===============================");
                return;
            }

            string shortloc = loc1;

            foreach (var attr in LocalizationAttributes)
            {
                string resultReplace = shortloc.Replace("{_" + attr.Key + "_}", "[параметр есть]");
                if (resultReplace == shortloc)
                {
                    //В локализации нет параметра но он указан в сервере
                    sb.AppendLine($"{nameObj}. localization not contains a params '{key}', but it is listed on the server");
                    sb.AppendLine($"===============================");
                }

                shortloc = resultReplace;
            }

            if (shortloc.Contains('{') || shortloc.Contains('}'))
            {
                //В локализации есть лишний параметр, который не указан в сервере
                sb.AppendLine($"{nameObj}. '{shortloc}'.  Localization has an extra parameter, which is not listed in the server");
                sb.AppendLine($"===============================");
            }
        }

        [Test]
        public void LocalizationOffersRU_Test()
        {
            string log = OffersLocTest(lngRU);
            LogFail(log);
        }

        [Test]
        public void LocalizationOffersEN_Test()
        {
            string log = OffersLocTest(lngEN);
            LogWarn(log);
        }

        private string OffersLocTest(Dictionary<string, string> lng)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var offerData in specialOffer.SpecialOfferItems)
            {
                CheckKey($"Offer {offerData.OfferName}", offerData.TechnicalNameDesc, lng, sb);
                CheckKey($"Offer {offerData.OfferName}", offerData.TechnicalNameTitle, lng, sb);
            }

            return sb.ToString();
        }

        [Test]
        public void LocalizationDailyQuestsRU_Test()
        {
            string log = DailyQuestsLocTest(lngRU);
            LogWarn(log);
        }

        [Test]
        public void LocalizationDailyQuestsEN_Test()
        {
            string log = DailyQuestsLocTest(lngEN);
            LogWarn(log);
        }

        private string DailyQuestsLocTest(Dictionary<string, string> lng)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var questData in questCollection.QuestDatas)
            {
                if (questData.Type != QuestType.Daily)
                    continue;
                CheckKey($"DailyQuest {questData.Name}", questData.TechnicalName, lng, sb);
            }

            return sb.ToString();
        }
    }
}