﻿using Assets.Scripts;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Logger;
using NodeInstance.Tests.HelperClasses;
using NSubstitute;
using NUnit.Framework;
using System;
using UtilsLib;
using UtilsLib.Logic;

namespace NodeInstance.Tests
{
	[SetUpFixture]
	public sealed class TestsSetup
	{
		[OneTimeSetUp]
		public void Init()
		{
            CommonLogger сommonLogger =  new CommonLogger(new TestsLogConfig());
            сommonLogger.AddLogger(new ThrowExceptionLogger());

            ILogger.Instance = сommonLogger;
            ILogger.Instance.Init();

			Assets.Scripts.InstanceServer.Formulas.Init();
			FRRandom.Init();
			ServerConstants.Instance = new ServerConstants();

            uLink.Network network = uLink.Network.Instance;
            uLink.INetworkView view = new TestNetworkView();
            uLink.NetworkP2P p2p = new uLink.NetworkP2P(network);

            var analitycsSender = Substitute.For<IAnalyticsSender>();
            

            PathHelper.TestPathBuilder.FormulasXml.LoadFromFileXml<global::Assets.Scripts.InstanceServer.Formulas>();

            RemoteXmlConfig.Instance = PathHelper.TestAuthPathBuilder.RemoteConfigXml.LoadFromFileXml<RemoteXmlConfig>();
            TimeForTests.SetTimeMocks(TimeProvider.UTCNow);

            GameXmlData gameXmlData = ObjectMother.GetEmptyGameXmlData();

            AddinManager addinManager = new AddinManager(PathHelper.TestPathBuilder, gameXmlData, null);

            InstanceGameManager gameManager = new InstanceGameManager(null, null, addinManager,  null, null,
                    null, null, null, null, gameXmlData, null, null);

			AnalyticsManager.Instance = new AnalyticsManager(analitycsSender, gameManager);

			new RegisterBitStreamCodecs(new PersonsOnServer());

            //new BitStreamStringList(gameXmlData);

            var listener = new ServerRpcListener(p2p, network, view);
            listener.Construct(null, null, null, null, null, null, null, null, null);
        }

		public class TestsLogConfig : ILogConfig
		{
			public ErrorLevel MinLogLevel => ErrorLevel.debug;
			public bool EnableUlink => false;
			public SosConfig Sos => new SosConfig() { enabled = true };
			public bool bDebugLogger => true;
			public NLogConfig nlog => new NLogConfig { enable = false };
			public bool ConsoleLog => true;
		}
	}
}
