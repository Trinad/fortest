﻿using Assets.Scripts;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils;
using NodeInstance.Tests.HelperClasses;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using UtilsLib.Logic;
using UtilsLib.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Fragoria.Common.Utils;
using Assets.Scripts.Utils.Logger;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.PassiveAbilitys;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.Perks;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Actions;

namespace NodeInstance.Tests.MapLife
{
    [TestFixture]
    public sealed class AbilityCastTest
    {
        private GameXmlData gameXmlData;
        private InstanceGameManager gameManager;
        private ServerRpcListener listener;
        private INotifyController notifyController;
        private int gameId = 1;
		private int personId = 1;
        ILoginController loginController;

        private ArenaAlgorithmData arenaDataAll;
        private ArenaAlgorithm arenaAlgorithm;
        GlobalEvents globalEvents;

        [OneTimeSetUp]
        public void Setup()
        {
            notifyController = Substitute.For<INotifyController>();
            loginController = Substitute.For<ILoginController>();

            uLink.Network network = uLink.Network.Instance;
            uLink.INetworkView view = new TestNetworkView();
            uLink.NetworkP2P p2p = new uLink.NetworkP2P(network);

            DataHashController dataHashController = new DataHashController(p2p, network, view);

            gameXmlData = GameXmlData.LoadAllXmlsInParallel(PathHelper.TestPathBuilder);

            arenaDataAll = PathHelper.TestAuthPathBuilder.ArenaDataXml.LoadFromFileXml<ArenaAlgorithmData>();
            arenaAlgorithm = new ArenaAlgorithm(dataHashController, gameXmlData);
            arenaAlgorithm.SetXmlData(arenaDataAll);

            globalEvents = new GlobalEvents();

            
            DIInjector.Inject(gameXmlData, (type) =>
            {
                if (type == typeof(GameXmlData))
                    return gameXmlData;

               if (type == typeof(PlayerCityData))
                    return gameXmlData.PlayerCityData;

                if (type == typeof(INotifyController))
                    return notifyController;

                if (type == typeof(ShopManager))
                    return null;

                if (type == typeof(PersonsOnServer))
                    return null;

                if (type == typeof(ILoginController))
                    return loginController;

                if (type == typeof(GlobalEvents))
                    return globalEvents;

                if (type == typeof(MatchmakingController))
                    return null;

                throw new Exception($"добавь в свитч AbilityCastTest.Setup тип {type.Name}");
            });

			var levelName = "test_level_1";
			ServerStartGameManager.levelName = levelName;
            Map.LoadJsonByUrl(PathHelper.TestPathBuilder.GetZoneUrl(levelName));
            DeepPathFinderAlgorithm.Inited = false;
            PersonsOnServer personsOnServer = new PersonsOnServer();

            AddinManager addinManager = new AddinManager(PathHelper.TestPathBuilder, gameXmlData, (type) =>
            {
                if (type == typeof(GameXmlData))
                    return gameXmlData;

                if (type == typeof(uLink.INetworkView))
                    return new TestNetworkView();//Substitute.For<uLink.INetworkView>();

				if (type == typeof(PersonsOnServer))
					return personsOnServer;

				if (type == typeof(AnalyticsManager))
					return AnalyticsManager.Instance;

				if (type == typeof(EdgesMapController))
					return null;

                if (type == typeof(GlobalEvents))
                    return globalEvents;

                if (type == typeof(InstanceGameManager))
                    return gameManager;
                
                if (type == typeof(ILoginController))
                    return loginController;

                if (type == typeof(INotifyController))
                    return notifyController;

                if (type == typeof(ArenaAlgorithm))
                    return arenaAlgorithm;

                if (type == typeof(ServerRpcListener))
                    return listener;

                if (type == typeof(MatchmakingController))
                    return null;

                throw new Exception($"добавь в свитч AbilityCastTest.addinManager тип {type.Name}");
            });

            MonsterBehaviourFactory monsterBehaviourFactory = new MonsterBehaviourFactory();

            MapFactory mapFactory = new MapFactory(monsterBehaviourFactory, null);

            IInstanceGameSender gameSender = Substitute.For<IInstanceGameSender>();
            
            TutorialAlgorithm tutorialAlgorithm = new TutorialAlgorithm(new TutorialAlgorithmData(), personsOnServer);

            ShopManager shopManager = new ShopManager(Substitute.For<IShopSender>());

            gameManager = new InstanceGameManager(null, mapFactory, addinManager, null,
                tutorialAlgorithm, personsOnServer, shopManager,
                gameSender, null, gameXmlData, arenaAlgorithm, new AppConfig());

            listener = new ServerRpcListener(p2p, network, view);
            listener.Construct(gameManager, null, personsOnServer, null, shopManager, null, gameXmlData.ServerConstantsXml, null, notifyController);

            listener.SetLobby(uLink.NetworkPeer.unassigned);

            gameManager.Start();

            gameId++;
            string levelParam = string.Empty;
            string instData = string.Empty;

            Map map = gameManager.InitMap(gameId, levelParam, instData);

            TimeForTests.AddTime(1);

        }

        private void SetTalant(InstancePerson person, int id, int level)
        {
            PassiveAbilityStorage storage = person.passiveAbilityStorage;
            PassiveAbilityBase passiveAbility = storage.GetPassiveAbilityById(id);
            for (int i = 0; i < level; i++)
                passiveAbility.LevelUp();
        }

        [Test]
        [TestCase(PersonType.Warrior)]
        [TestCase(PersonType.Wizard)]
        [TestCase(PersonType.Archer)]
        [TestCase(PersonType.Gunner)]
        [TestCase(PersonType.Shaman)]
        public void AbilityCast_Test(PersonType personType)
        {
            var Data = gameXmlData.PersonAttacksDatas.PersonDatas.Find(x => x.PersonType == personType);

            foreach (var ability in Data.Abilities)
            {
                AbilityType abilityType = ability.Type;
                if (abilityType == AbilityType.Dash2)
                    continue;
                var person = ObjectMother.GetInstancePerson(new ObjectMother.GetInstancePersonParams()
                {
                    personType = personType,
                    gamexmlData = gameXmlData,
                    initItems = true,
                    globalEvents = globalEvents,
                    personId = personId++,
                });
                person.StatContainer = new PersonStatContainer(person);
                person.SetGameXmlData(gameXmlData);
                person.SetLevel(30);
                gameManager.AddPerson(person);
                person.GameId = gameId;

                InstancePersonSqlData sqlData = new InstancePersonSqlData();
                sqlData.Abilities = new List<AbilitySaveData>();

                person.InitAbility(sqlData);
                person.InitPerks(sqlData);

                listener.onPlayerConnected(person, new uLink.NetworkPlayer(1));

				person.InitStartPack();
                listener.PersonSetOnMapRequest(person);

                person.SetAbilitySlot(abilityType, 1);

                foreach (var tatant in person.passiveAbilityStorage.PassiveAbilities)
                    SetTalant(person, tatant.Id, 1);

                foreach (Perk perk in gameXmlData.PerkList.PerkList)
                {
                    perk.Apply(person.syn);
                }


                CreateMonster cm = new CreateMonster();
                cm.Construct(gameXmlData);
                cm.Name = "target";

                var target = cm.GetMonster(person.syn.map, person.syn.position, 0);
                cm.AddMonsterOnMap(target);
                target.GetStatContainer().AddStat(Stats.HpAdd, 10000);
                target.Health = target.MaxHealth;

                Map map = person.syn.map;


                PersonBehaviour behaviour = person.syn;

                behaviour.AbilityStartRequest(Vector3.Zero, abilityType);
                behaviour.SetSpellDirection(behaviour.direction, behaviour.sessionId);

                if (behaviour.SpellCastAction == null)
                    Assert.Fail("Не смогли начать каст " + abilityType);

				var applyTime = behaviour.SpellCastAction.ApplyTime;
				for (float i = 0; i < applyTime + 2; i += Time.deltaTime)
                {
                    TimeForTests.AddTime(Time.deltaTime);

                    gameManager.MonoBehavioursUpdate();
                    map.MyUpdate();
                    map.ScenariosUpdate();

                    if (behaviour.SpellCastAction != null)
                        behaviour.SpellCastAction.AbilityUpdate();
                }

                if (behaviour.SpellCastAction != null)
                    Assert.Fail("Не смогли закончить каст " + abilityType);

                if (map.persons.Count > 2)
                    map.RemovePerson(person.syn);
            }
        }
    }
}
