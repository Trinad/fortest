﻿using Assets.Scripts;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.Networking;
using NodeInstance.Tests.HelperClasses;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using UtilsLib.Logic;
using UtilsLib.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Fragoria.Common.Utils;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial.Actions;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.PassiveAbilitys;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.Utils;

namespace NodeInstance.Tests.MapLife
{
    [TestFixture]
    public sealed class TalantsInBattle
    {
        private GameXmlData gameXmlData;
        private InstanceGameManager gameManager;
        private ServerRpcListener listener;
        private int gameId = 1;

        private ArenaAlgorithmData arenaDataAll;
        private ArenaAlgorithm arenaAlgorithm;
        private Map map;
        private InstancePerson person;
        private MonsterBehaviour target;

        private const int Warrior_FuriousCharge = 1;//Яростный Рывок
        private const int Warrior_heaven_blade = 2;//Небесный Клинок
        private const int Warrior_steel_hardening = 3;//Стальная Закалка
        private const int Warrior_accelerated_reaction = 4;//Ускоренная Реакция
        private const int Warrior_sweeping_lunge = 5;//Стремительный Выпад
        private const int Warrior_forbidden_method =  6;//Запрещенный Прием
        private const int Warrior_dancing_blades = 7;//Танцующие Клинки
        private const int Warrior_cutting_tendons =    8;//Рассечение Сухожилий
        private const int Warrior_multiple_fractures = 9;//Множественные Переломы
        private const int Warrior_slowing_down_vortex =   10;//Замедляющий Вихрь
        private const int Warrior_filling_with_power =   11;//Наполнение Силой
        private const int Warrior_sword_master = 12;//Мастер Меча
        private const int Warrior_wheel_of_death = 13;//Колесо Смерти
        private const int Warrior_protective_cocoon =  14;//Защитный Кокон
        private const int Warrior_imperial_assault = 15;//Имперский Штурм
        private const int Warrior_open_wounds = 16;//Открытые Раны
        private const int Warrior_energy_balance = 17;//Энергетический Баланс
        private const int Warrior_honed_tehnique = 18;//Отточенная Техника



        [OneTimeSetUp]
        public void Setup()
        {
            uLink.Network network = uLink.Network.Instance;
            uLink.INetworkView view = new TestNetworkView();
            uLink.NetworkP2P p2p = new uLink.NetworkP2P(network);

            DataHashController dataHashController = new DataHashController(p2p, network, view);
            gameXmlData = GameXmlData.LoadAllXmlsInParallel(PathHelper.TestPathBuilder);

            arenaDataAll = PathHelper.TestAuthPathBuilder.ArenaDataXml.LoadFromFileXml<ArenaAlgorithmData>();
            arenaAlgorithm = new ArenaAlgorithm(dataHashController, gameXmlData);
            arenaAlgorithm.SetXmlData(arenaDataAll);

            var notifiController = Substitute.For<INotifyController>();
            
            DIInjector.Inject(gameXmlData, (type) =>
            {
                if (type == typeof(GameXmlData))
                    return gameXmlData;

               if (type == typeof(PlayerCityData))
                    return gameXmlData.PlayerCityData;

                if (type == typeof(INotifyController))
                    return notifiController;

                if (type == typeof(ShopManager))
                    return null;

                if (type == typeof(PersonsOnServer))
                    return null;

                if (type == typeof(ILoginController))
                    return null;

                if (type == typeof(GlobalEvents))
                    return null;

                if (type == typeof(MatchmakingController))
                    return null;

                throw new Exception($"добавь в свитч TalantsInBattle.Setup тип {type.Name}");
            });

			var levelName = "test_level_1";
			ServerStartGameManager.levelName = levelName;
            Map.LoadJsonByUrl(PathHelper.TestPathBuilder.GetZoneUrl(levelName));
            DeepPathFinderAlgorithm.Inited = false;

            Cheats.Clear();
            PersonsOnServer personsOnServer = new PersonsOnServer();
            AddinManager addinManager = new AddinManager(PathHelper.TestPathBuilder, gameXmlData, (type) =>
            {
                if (type == typeof(GameXmlData))
                    return gameXmlData;

                if (type == typeof(uLink.INetworkView))
                    return new TestNetworkView();

                if (type == typeof(PersonsOnServer))
                    return personsOnServer;

				if (type == typeof(AnalyticsManager))
					return AnalyticsManager.Instance;

                if (type == typeof(EdgesMapController))
                    return null;

                if (type == typeof(GlobalEvents))
                    return new GlobalEvents();

                if (type == typeof(InstanceGameManager))
                    return gameManager;

                if (type == typeof(ILoginController))
                    return null;

                if (type == typeof(INotifyController))
                    return notifiController;

                if (type == typeof(ArenaAlgorithm))
                    return arenaAlgorithm;

                if (type == typeof(ServerRpcListener))
                    return listener;

                if (type == typeof(MatchmakingController))
                    return null;

                throw new Exception($"добавь в свитч TalantsInBattle.addinManager тип {type.Name}");
            });

            MonsterBehaviourFactory monsterBehaviourFactory = new MonsterBehaviourFactory();

            MapFactory mapFactory = new MapFactory(monsterBehaviourFactory, null);

            IInstanceGameSender gameSender = Substitute.For<IInstanceGameSender>();
            TutorialAlgorithm tutorialAlgorithm = new TutorialAlgorithm(new TutorialAlgorithmData(), personsOnServer);

            ShopManager shopManager = new ShopManager(Substitute.For<IShopSender>());

            gameManager = new InstanceGameManager(null, mapFactory, addinManager, null,
                tutorialAlgorithm, personsOnServer, shopManager,
                gameSender, null, gameXmlData, arenaAlgorithm, new AppConfig());

            listener = new ServerRpcListener(p2p, network, view);

            listener.Construct(gameManager, null, personsOnServer, null, shopManager, null, gameXmlData.ServerConstantsXml, null, notifiController);
              

            listener.SetLobby(uLink.NetworkPeer.unassigned);

            gameManager.Start();

            gameId++;
            string levelParam = string.Empty;
            string instData = string.Empty;

            map = gameManager.InitMap(gameId, levelParam, instData);

            TimeForTests.AddTime(1);

            PersonType personType = PersonType.Warrior;
            person = ObjectMother.GetInstancePerson(new ObjectMother.GetInstancePersonParams()
            {
                personType = personType,
                gamexmlData = gameXmlData,
                globalEvents = new GlobalEvents(),
                initItems = true,
            });
            person.StatContainer = new PersonStatContainer(person);
            person.SetGameXmlData(gameXmlData);
            person.SetLevel(30);
            gameManager.AddPerson(person);
            person.GameId = gameId;

            InstancePersonSqlData sqlData = new InstancePersonSqlData();
            sqlData.Abilities = new List<AbilitySaveData>();

            person.InitAbility(sqlData);

            listener.onPlayerConnected(person, new uLink.NetworkPlayer(1));

            person.InitStartPack();
            listener.PersonSetOnMapRequest(person);


            CreateMonster cm = new CreateMonster();
            cm.Construct(gameXmlData);
            cm.Name = "target";

            target = cm.GetMonster(map, person.syn.position, 0);
            cm.AddMonsterOnMap(target);
            target.GetStatContainer().AddStat(Stats.HpAdd, 10000); //прибавим ему здоровья, а то сдохнуть может
            target.Health = target.MaxHealth;

        }

        public void Update(InstancePerson person)
        {
            PersonBehaviour behaviour = person.syn;
            if (behaviour.SpellCastAction != null)
                behaviour.SpellCastAction.AbilityUpdate();

            behaviour.UpdatePhysics();
        }

        public void CastAbility(InstancePerson person, AbilityType abilityType)
        {
            person.SetAbilitySlot(abilityType, 1);

            PersonBehaviour behaviour = person.syn;
            behaviour.AbilityStartRequest(Vector3.Zero, abilityType);
            behaviour.SetSpellDirection(behaviour.direction, behaviour.sessionId);

            var ability = person.GetAbilityById(abilityType);

            //ждем пока урон пройдет
            for (float time = 0; time < ability.ApplyTime + 2; time += Time.deltaTime)
            {
                TimeForTests.AddTime(Time.deltaTime);
                Update(person);
            }

            behaviour.ResetCooldown();
        }

        private void SetTalant(InstancePerson person, int id, int level)
        {
            PassiveAbilityStorage storage = person.passiveAbilityStorage;
            PassiveAbilityBase passiveAbility = storage.GetPassiveAbilityById(id);
            for (int i = 0; i < level; i++)
                passiveAbility.LevelUp();
        }

        private void ResetTalants(InstancePerson person)
        {
            PassiveAbilityStorage storage = person.passiveAbilityStorage;
            storage.ResetSpendedTalentPoints();
        }

        private void AreEqual(double val1, double val2, double delta)
        {
            if (Math.Abs(val1 - val2) > delta)
                Assert.Warn($"Not equal '{val1}' and '{val2}'");
        }

        //[Test]
        public void Talants_FuriousCharge_Test()
        {
            CastAbility(person, AbilityType.Charge3_0);
            int originalDamage = target.MaxHealth - target.Health;
            target.Health = target.MaxHealth;

            ResetTalants(person);
            SetTalant(person, Warrior_FuriousCharge, 5);
            CastAbility(person, AbilityType.Charge3_0);
            int newlDamage = target.MaxHealth - target.Health;
            target.Health = target.MaxHealth;

            AreEqual(newlDamage, originalDamage * 1.25, 1);
        }

        [Test]
        public void Talants_SteelHardening_Test()
        {
            int originalHP = person.syn.MaxHealth;

            ResetTalants(person);
            SetTalant(person, Warrior_steel_hardening, 5);

            int newlHP = person.syn.MaxHealth;

            AreEqual(newlHP, originalHP * 1.05, 1);
        }
    }
}
