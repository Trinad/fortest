﻿using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace NodeInstance.Tests.MapLife
{
    [TestFixture]
    public sealed class PositionSortedListTest
    {
        private void EqualsCheck(PositionSortedList<MapObject> positionList, List<MapObject> standardList)
        {
            Assert.That(positionList.Count, Is.EqualTo(standardList.Count));

            for (int i = 0; i < positionList.Count; i++)
            {
                Assert.That(positionList[i], Is.EqualTo(standardList[i]));
            }

            int j = 0;
            foreach (var mo in positionList)
            {
                Assert.That(mo, Is.EqualTo(standardList[j++]));
            }
        }

        /// <summary>
        /// проверить что операции Add, Remove, RemoveAt, Clear, AddRange ведут себя точно также как у обычного списка
        /// </summary>
        [Test]
        public void EqualsListRandomTest()
        {
            PositionSortedList<MapObject> positionList = new PositionSortedList<MapObject>();
            List<MapObject> standardList = new List<MapObject>();

            Random r = new Random(1);
            for (int i = 0; i < 500; i++)
            {
                int rnd = r.Next(8);
                switch (rnd)
                {
                    case 0:
                    case 1:
                    case 2:
                        AddElement(positionList, standardList, new MapObject() { position = new Vector3(r.Next(10000) / 100f, r.Next(50), r.Next(10000) / 100f) });
                        break;
                    case 3:
                        if (positionList.Count != 0)
                            RemoveElement(positionList, standardList, positionList[r.Next(positionList.Count)]);
                        break;
                    case 4:
                        if (positionList.Count != 0)
                            RemoveAt(positionList, standardList, r.Next(positionList.Count));
                        break;
                    case 5:
                        Clear(positionList, standardList);
                        break;
                    case 6:
                        AddRangeDouble(positionList, standardList);
                        break;
                    case 7:
                        positionList.Refresh();
                        break;
                }
                EqualsCheck(positionList, standardList);
            }
        }


        [Test]
        public void FindNearestTest()
        {
            PositionSortedList<MapObject> list = new PositionSortedList<MapObject>();

            int count = 0;
            foreach (var mo in list.GetXNearest(2.1f, 1, false))
                count++;

            Assert.That(count, Is.EqualTo(0));


            list.Add(new MapObject() { position = new Vector3(4, 0, 0), Radius = 0.5f });
            list.Add(new MapObject() { position = new Vector3(5, 0, 0), Radius = 0.5f });
            list.Add(new MapObject() { position = new Vector3(7, 0, 0), Radius = 0.5f });
            list.Add(new MapObject() { position = new Vector3(1, 0, 0), Radius = 0.5f });
            list.Add(new MapObject() { position = new Vector3(2, 0, 0), Radius = 0.5f });
            list.Add(new MapObject() { position = new Vector3(2, 0, 0), Radius = 0.5f });
            list.Add(new MapObject() { position = new Vector3(3, 0, 0), Radius = 0.5f });
            list.Add(new MapObject() { position = new Vector3(float.PositiveInfinity, 0, 0), Radius = 0.5f });
            list.Add(new MapObject() { position = new Vector3(float.NegativeInfinity, 0, 0), Radius = 0.5f });
            list.Refresh();

             count = 0;
            foreach (var mo in list.GetXNearest(2.1f, 1, false))
                count++;

            Assert.That(count, Is.EqualTo(3));

            count = 0;
            foreach (var mo in list.GetXNearest(2.1f, 1, true))
                count++;

            Assert.That(count, Is.EqualTo(4));

            count = 0;
            foreach (var mo in list.GetXNearest(2.1f, 2, true))
                count++;

            Assert.That(count, Is.EqualTo(5));
        }

        private void AddElement(PositionSortedList<MapObject> positionList, List<MapObject> standardList, MapObject mo)
        {
            positionList.Add(mo);
            standardList.Add(mo);
        }

        private void RemoveElement(PositionSortedList<MapObject> positionList, List<MapObject> standardList, MapObject mo)
        {
            positionList.Remove(mo);
            standardList.Remove(mo);
        }

        private void RemoveAt(PositionSortedList<MapObject> positionList, List<MapObject> standardList, int index)
        {
            positionList.RemoveAt(index);
            standardList.RemoveAt(index);
        }

        private void Clear(PositionSortedList<MapObject> positionList, List<MapObject> standardList)
        {
            positionList.Clear();
            standardList.Clear();
        }

        private void AddRangeDouble(PositionSortedList<MapObject> positionList, List<MapObject> standardList)
        {
            positionList.AddRange(positionList.ToList());
            standardList.AddRange(standardList.ToList());
        }
    }
}
