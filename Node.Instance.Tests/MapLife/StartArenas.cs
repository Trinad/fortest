﻿using Assets.Scripts;
using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Addin;
using Assets.Scripts.InstanceServer.Algorithms;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.TargetObjects;
using Assets.Scripts.Networking;
using NodeInstance.Tests.HelperClasses;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using UtilsLib.Logic;
using UtilsLib.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.Tutorial;
using Fragoria.Common.Utils;
using Assets.Scripts.Utils.Logger;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.Perks;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.PassiveAbilitys;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using Assets.Scripts.Utils;
using UtilsLib.DegSerializers;

namespace NodeInstance.Tests.MapLife
{
    [TestFixture]
    public sealed class StartArenas
    {
        private GameXmlData gameXmlData;
        private InstanceGameManager gameManager;
        private ServerRpcListener listener;
        private MatchmakingController matchmakingController;
        private INotifyController notifyController;
        private int gameId = 1;
        private int maxUpdates = 6000;

        private ArenaAlgorithmData arenaDataAll;
        private ArenaAlgorithm arenaAlgorithm;
		private NickNameGeneratorManager nickNameGeneratorManager;
		ShopManager shopManager;


		GlobalEvents globalEvents;

		private MatchBaseData instanceData = new MatchBaseData()
		{
			AverageHeroPower = 114,
			AverageRating = 478,
			PersonsSubscribedCount = 1,
			AverageLevel = 30,
			PlayerTeams = new List<PlayerTeamData>
			{
				new PlayerTeamData{ PersonId = 3, Team = PlayerTeam.Team1, PersonType = PersonType.Warrior},
				new PlayerTeamData{ PersonId = 4, Team = PlayerTeam.Team2, PersonType = PersonType.Archer},
			},
			MatchingTime = 1,
		};

        [OneTimeSetUp]
        public void Setup()
        {
            uLink.Network network = uLink.Network.Instance;
            uLink.INetworkView view = new TestNetworkView();
            uLink.NetworkP2P p2p = new uLink.NetworkP2P(network);

            DataHashController dataHashController = new DataHashController(p2p, network, view);
            gameXmlData = GameXmlData.LoadAllXmlsInParallel(PathHelper.TestPathBuilder);
            arenaDataAll = PathHelper.TestAuthPathBuilder.ArenaDataXml.LoadFromFileXml<ArenaAlgorithmData>();
            arenaAlgorithm = new ArenaAlgorithm(dataHashController, gameXmlData);
            arenaAlgorithm.SetXmlData(arenaDataAll);

			shopManager = new ShopManager(Substitute.For<IShopSender>());

			notifyController = Substitute.For<INotifyController>();
           
            globalEvents = new GlobalEvents();

            matchmakingController = new MatchmakingController(p2p, network, view);
            matchmakingController.Constructor(null, null, null, null, null, globalEvents, notifyController);

           
            DIInjector.Inject(gameXmlData, (type) =>
            {
                if (type == typeof(GameXmlData))
                    return gameXmlData;

               if (type == typeof(PlayerCityData))
                    return gameXmlData.PlayerCityData;

                if (type == typeof(uLink.INetworkView))
                    return new TestNetworkView(); //Substitute.For<uLink.INetworkView>();

                if (type == typeof(INotifyController))
                    return notifyController;

                if (type == typeof(ShopManager))
                    return shopManager;

                if (type == typeof(PersonsOnServer))
                    return null;

                if (type == typeof(ILoginController))
                    return null;

                if (type == typeof(GlobalEvents))
                    return globalEvents;

                if (type == typeof(MatchmakingController))
                    return null;

                throw new Exception($"добавь в свитч StartArenas.Setup тип {type.Name}");
            });

			nickNameGeneratorManager = new NickNameGeneratorManager(gameXmlData);
        }

        [Test]
        [TestCase(ArenaMatchType.Waves)]
        [TestCase(ArenaMatchType.CaptureFlag)]
        [TestCase(ArenaMatchType.Deathmatch)]
        [TestCase(ArenaMatchType.TeamDeathmatch)]
        [TestCase(ArenaMatchType.Brawl)]
        public void StartMap_Arenas_Test(ArenaMatchType matchType)
        {
            foreach (var levelName in arenaDataAll.GetLevelNames(matchType))
            {
                var location = gameXmlData.Edges.GetEdgeLocationXmlData(levelName);
                if (location is null)
                    Assert.Fail($"В edges.xml нет карты {levelName}");

                ServerStartGameManager.levelName = levelName;
                Map.LoadJsonByUrl(PathHelper.TestPathBuilder.GetZoneUrl(location.GetJsonName()));
                DeepPathFinderAlgorithm.Inited = false;

                Cheats.Clear();
                PersonsOnServer personsOnServer = new PersonsOnServer();

				AddinManager addinManager = new AddinManager(PathHelper.TestPathBuilder, gameXmlData, (type) =>
                {
                    if (type == typeof(GameXmlData))
                        return gameXmlData;

                    if (type == typeof(uLink.INetworkView))
                        return new TestNetworkView(); //Substitute.For<uLink.INetworkView>();

	                if (type == typeof(PersonsOnServer))
		                return personsOnServer;

					if (type == typeof(NickNameGeneratorManager))
						return nickNameGeneratorManager;

					if (type == typeof(AnalyticsManager))
						return AnalyticsManager.Instance;

					if (type == typeof(EdgesMapController))
						return null;

                    if (type == typeof(GlobalEvents))
                        return globalEvents;

                    if (type == typeof(InstanceGameManager))
                        return gameManager;

                    if (type == typeof(ILoginController))
                        return null;

                    if (type == typeof(INotifyController))
                        return notifyController;

                    if (type == typeof(ArenaAlgorithm))
                        return arenaAlgorithm;

                    if (type == typeof(ServerRpcListener))
                        return listener;

                    if (type == typeof(MatchmakingController))
                        return matchmakingController;

                    throw new Exception($"добавь в свитч StartArenas.addinManager тип {type.Name}");
                });

                MonsterBehaviourFactory monsterBehaviourFactory = new MonsterBehaviourFactory();

                MapFactory mapFactory = new MapFactory(monsterBehaviourFactory, null);

                IInstanceGameSender gameSender = Substitute.For<IInstanceGameSender>();
                
                TutorialAlgorithm tutorialAlgorithm = new TutorialAlgorithm(new TutorialAlgorithmData(), personsOnServer);

                gameManager = new InstanceGameManager(null, mapFactory, addinManager, null,
                    tutorialAlgorithm, personsOnServer, shopManager,
                    gameSender, null, gameXmlData, arenaAlgorithm, new AppConfig());

                uLink.Network network = uLink.Network.Instance;
                uLink.INetworkView view = new TestNetworkView(); //Substitute.For<uLink.INetworkView>();
                uLink.NetworkP2P p2p = new uLink.NetworkP2P(network);

                listener = new ServerRpcListener(p2p, network, view);

                listener.Construct(gameManager, null, personsOnServer, null, shopManager, null, gameXmlData.ServerConstantsXml, null, notifyController);
                listener.SetLobby(uLink.NetworkPeer.unassigned);

                //matchmakingController = new MatchmakingController(p2p, network, view);
                //matchmakingController.Constructor(null, null, null, null, null);

                gameManager.Start();

                StartMap_NotKillMonster_Test();
                StartMap_KillAllMonsters_Test();
                StartMap_KillPerson_Test();
            }
        }

        public void CheckScenarios(Map map)
        {
            if (map.Scenarios.OfType<StatisticBase>().Count() != 1)
            {
                Assert.Fail($"У арены нет базовой статистики");
            }
            var statistic = map.Scenarios.OfType<StatisticBase>().First();
            var matchType = statistic.GetArenaMatchType();
            if (map.Scenarios.OfType<DamageLogger>().Count() != 1)
            {
                Assert.Fail($"У арены нет DamageLogger");
            }
            var damageLoger = map.Scenarios.OfType<DamageLogger>().First();

            if (damageLoger.MatchType != matchType)
            {
                Assert.Fail($"У арены DamageLogger тип арены {damageLoger.MatchType} не совпадает с типом арены базовой статистики {matchType}");
            }

            
            if (map.Scenarios.OfType<KilSeriesLogger>().Count() != 1)
            {
                Assert.Fail($"У арены нет KilSeriesLogger");
            }
            var killSeries = map.Scenarios.OfType<KilSeriesLogger>().First();
            if (killSeries.MatchType != matchType)
            {
                Assert.Fail($"У арены KilSeriesLogger тип арены {killSeries.MatchType} не совпадает с типом арены базовой статистики {matchType}");
            }
        }

        private int loginId = 1;
        private int personId = 1;
        public InstancePerson GetPerson()
        {
            var person = ObjectMother.GetInstancePerson(new ObjectMother.GetInstancePersonParams()
            {
                personType = PersonType.Warrior,
                gamexmlData = gameXmlData,
                globalEvents = globalEvents,
				shopManager = shopManager,
				initItems = true,
                loginId = loginId++,
                personId = personId++,
            });

			person.PersonalParam = person.loginId + "_" + person.personId;

            person.StatContainer = new PersonStatContainer(person);

            person.SetGameXmlData(gameXmlData);
            InstancePersonSqlData sqlData = new InstancePersonSqlData();
            sqlData.Abilities = new List<AbilitySaveData>();
            person.InitAbility(sqlData);
            person.InitPerks(sqlData);

            gameManager.AddPerson(person);
            person.GameId = gameId;

            foreach (var tatant in person.passiveAbilityStorage.PassiveAbilities)
                SetTalant(person, tatant.Id, 1);

            person.InitStartPack();

            return person;
        }

        private int connectId = 1;
        public void ConnectPerson(InstancePerson person)
        {
            listener.onPlayerConnected(person, new uLink.NetworkPlayer(connectId++));
        }

        public void DisconnectPerson(InstancePerson person)
        {
            gameManager.OnPlayerDisconnected(person.player, out var bCurrentConnect);
        }

        public void SetOnMapPerson(InstancePerson person)
        {
            listener.PersonSetOnMapRequest(person);

            foreach (Perk perk in gameXmlData.PerkList.PerkList)
            {
                perk.Apply(person.syn);
            }
        }

        private void SetTalant(InstancePerson person, int id, int level)
        {
            PassiveAbilityStorage storage = person.passiveAbilityStorage;
            PassiveAbilityBase passiveAbility = storage.GetPassiveAbilityById(id);
            for (int i = 0; i < level; i++)
                passiveAbility.LevelUp();
        }

        public void StartMap_NotKillMonster_Test()
        {
            gameId++;
            string levelParam = string.Empty;
            string instData = JSON.JsonEncode(instanceData.JsonEncode());

            Map map = gameManager.InitMap(gameId, levelParam, instData);
            CheckScenarios(map);

            var person = GetPerson();
            var person2 = GetPerson();

            map.MyUpdate();
            TimeForTests.AddTime(1);//игрок заходит всегда чуть позже чем карта создается

            ConnectPerson(person);
            ConnectPerson(person2);

            CoroutineRunner.Instance.Timer(30, () => DisconnectPerson(person2));

            SetOnMapPerson(person);
            //SetOnMapPerson(person2); эмулируем что второй пользователь не вошел

            int i;
            for (i = 0; i < maxUpdates; i++)
            {
                gameManager.MonoBehavioursUpdate();
                map.MyUpdate();
                map.ScenariosUpdate();
                TimeForTests.AddTime(5);
                Sos.Debug($"Sec {i} life monsters: {map.monsters.Count(x => x.IsLiveObject && !x.IsDead)}");
                if (map.GetVariableBool("endMatch"))
                    break;
            }
            TimeForTests.AddTime(5);

            if (i == maxUpdates)
                Assert.Fail($"Матчь не закончился за {maxUpdates} секунд");
        }

        public void StartMap_KillAllMonsters_Test()
        {
            gameId++;
            string levelParam = string.Empty;
            string instData = JSON.JsonEncode(instanceData.JsonEncode());

            Map map = gameManager.InitMap(gameId, levelParam, instData);
            CheckScenarios(map);

            var person = GetPerson();
            var person2 = GetPerson();

            map.MyUpdate();
            TimeForTests.AddTime(1);//игрок заходит всегда чуть позже чем карта создается

            ConnectPerson(person);
            ConnectPerson(person2);

            CoroutineRunner.Instance.Timer(30, () => DisconnectPerson(person2));

            SetOnMapPerson(person);
            //SetOnMapPerson(person2); эмулируем что второй пользователь не вошел

            int i;
            for (i = 0; i < maxUpdates; i++)
            {
                gameManager.MonoBehavioursUpdate();
                map.MyUpdate();
                map.ScenariosUpdate();
                TimeForTests.AddTime(5);
                //убиваем по одному в секунду
                foreach (var monster in map.monsters)
                {
                    if (person.syn.CanAttackTarget(monster))
                    {
                        monster.SetHealth(person.syn, 0);
                        break;
                    }
                }
                Sos.Debug($"Sec {i} life monsters: {map.monsters.Count(x => x.IsLiveObject && !x.IsDead)}");
                if (map.GetVariableBool("endMatch"))
                    break;
            }

            TimeForTests.AddTime(5);

            if (i == maxUpdates)
                Assert.Fail($"Матчь не закончился за {maxUpdates} секунд");
        }

        public void StartMap_KillPerson_Test()
        {
            gameId++;
            string levelParam = string.Empty;
            string instData = JSON.JsonEncode(instanceData.JsonEncode());

            Map map = gameManager.InitMap(gameId, levelParam, instData);
            CheckScenarios(map);

            var person = GetPerson();
            var person2 = GetPerson();

            CoroutineRunner.Instance.Timer(30, () => DisconnectPerson(person2));

            map.MyUpdate();
            TimeForTests.AddTime(1);//игрок заходит всегда чуть позже чем карта создается

            ConnectPerson(person);
            ConnectPerson(person2);

            SetOnMapPerson(person);
            //SetOnMapPerson(person2); эмулируем что второй пользователь не вошел

            int i;
            for (i = 0; i < maxUpdates; i++)
            {
                gameManager.MonoBehavioursUpdate();
                map.MyUpdate();
                map.ScenariosUpdate();
                TimeForTests.AddTime(5);
                //убиваем персонажа и всех его друзей
                var friends = map.monsters.FindAll(x => x.TeamColor == person.syn.TeamColor);
                MonsterBehaviour notFriend = map.monsters.Find(m => m.IsLiveObject && m.CanAttackTarget(person.syn));
                if (notFriend != null)
                {
                    foreach (var f in friends)
                        f.SetHealth(notFriend, 0);
                    person.syn.SetHealth(notFriend, 0);
                }

                Sos.Debug($"Sec {i} life monsters: {map.monsters.Count(x => x.IsLiveObject && !x.IsDead)}");
                if (map.GetVariableBool("endMatch"))
                    break;
            }

            TimeForTests.AddTime(5);

            if (i == maxUpdates)
                Assert.Fail($"Матчь не закончился за {maxUpdates} секунд");
        }
    }
}
