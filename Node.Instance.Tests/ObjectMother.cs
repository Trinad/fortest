﻿using Assets.Scripts.InstanceServer;
using Assets.Scripts.InstanceServer.Entries;
using Assets.Scripts.InstanceServer.Entries.Login;
using Assets.Scripts.InstanceServer.Entries.Person;
using Assets.Scripts.InstanceServer.Entries.XmlData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.PassiveAbilitys;
using Assets.Scripts.InstanceServer.Entries.XmlData.AbilityData.Perks;
using Assets.Scripts.Networking;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.Logger;
using NodeInstance.Tests.HelperClasses;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Assets.Scripts.InstanceServer.Entries.XmlData.PlayerCityDatas;
using uLink;
using UtilsLib;
using UtilsLib.Logic;
using UtilsLib.Logic.Enums;
using UtilsLib.NetworkingData;
using Assets.Scripts.Utils.Db;
using Fragoria.Common.Utils;
using System.Collections;
using Assets.Scripts;
using Assets.Scripts.Networking.ClientDataRPC;

namespace NodeInstance.Tests
{
    public static class ObjectMother
	{
        public static GameXmlData GetEmptyGameXmlData(bool fillItems = false, bool personAbility = false, bool lootlist = false, bool emotion = false, bool constants = false)
        {
            var gxd = new GameXmlData();
            gxd.MonsterXmlDatas = new MonsterDataList();
            gxd.Edges = new Edges {EdgeDatas = new List<EdgeXmlData>()};
            gxd.Elixirs = new List<Elixir>();
            gxd.AddinLocations = new List<AddinLocation>();

            if (fillItems)
            {
				gxd.PlayerCityData = PathHelper.TestPathBuilder.PlayerCityXml.LoadFromFileXml<PlayerCityData>();
				gxd.PlayerCityData.Init();
                gxd.ItemDataRepository = PathHelper.TestPathBuilder.ItemDataRepositoryDataXml.LoadFromFileXml<ItemDataRepository>();
            }

            if (lootlist)
            {
				var traderDataXml = PathHelper.TestPathBuilder.TraderDataXml.LoadFromFileXml<LootLists>();
                gxd.LootListsXml = PathHelper.TestPathBuilder.LootListsXml.LoadFromFileXml<LootLists>();
				gxd.LootListsXml.loots.AddRange(traderDataXml.loots);
                gxd.LootListsXml.Init();
            }

            if (personAbility)
            {
                gxd.PersonAttacksDatas = PathHelper.TestPathBuilder.PersonXml.LoadFromFileXml<PersonDataList>();
                gxd.PassiveAbilitys = PathHelper.TestPathBuilder.PassiveAbilitysXml.LoadFromFileXml<PassiveAbilityXmlDataList>();
                gxd.PerkList = PathHelper.TestPathBuilder.PerksXml.LoadFromFileXml<PerkDataList>();
            }

            if (emotion)
            {
                gxd.EmotionSettings = PathHelper.TestPathBuilder.EmotionXml.LoadFromFileXml<EmotionSettings>();
            }

            if (constants)
            {
                gxd.ServerConstantsXml = PathHelper.TestPathBuilder.ServerConstantsXml.LoadFromFileXml<ServerConstants>();
            }

            return gxd;
        }

		public static IEmotionController GetEmotionController()
		{
			uLink.Network network = uLink.Network.Instance;
			uLink.INetworkView view = new TestNetworkView();
			uLink.NetworkP2P p2p = new uLink.NetworkP2P(network);

			var gameXmlData = GetEmptyGameXmlData(fillItems: true, personAbility: true, lootlist: true, constants: true, emotion: true);

			var shopManager = GetShopManager();

			EmotionManager emotionManager = new EmotionManager(gameXmlData.ServerConstantsXml, gameXmlData.EmotionSettings, shopManager);
			IEmotionController emotionController = new EmotionController(NSubstitute.Substitute.For<IPersonsOnServer>(), emotionManager, shopManager, p2p, network, view);

			return emotionController;
		}

		public static ShopManager GetShopManager()
		{
			var shopSender = Substitute.For<IShopSender>();
			var shopManager = new ShopManager(shopSender);
			shopManager.ShopOff = true;

			//var newPrices = DownLoadNewPrices();
			var newPrices = LoadNewPrices();
			shopManager.SetPricePoints(newPrices);
			shopManager.SetShopItems(PathHelper.TestAuthPathBuilder.ShopXml.LoadFromFileXml<List<ShopItemData>>());

			return shopManager;
		}

		public static List<Prices> LoadNewPrices()
		{
			var price = PathHelper.TestPaymentsPathBuilder.GooglePlayXml.LoadFromFileXml<Prices>();
			price.priceType = "googleplay";
			price.priceId = 12;

			var price1 = PathHelper.TestPaymentsPathBuilder.AppStoreXml.LoadFromFileXml<Prices>();
			price1.priceType = "appstore";
			price1.priceId = 13;

			var price2 = PathHelper.TestPaymentsPathBuilder.BaseXml.LoadFromFileXml<Prices>();
			price2.priceType = "base";
			price2.priceId = 15;

			var newPrices = new List<Prices>();
			newPrices.Add(price);
			newPrices.Add(price1);
			newPrices.Add(price2);

			return newPrices;
		}

		static List<Prices> DownLoadNewPrices()
		{
			var answer = HttpHelperCommon.GetPostRequest("http://s2.datcroft.org/pixels/cubomatrix/pay/server", "PricePointRequest", "goni dani", "fnh3r90qghperwgpwogbrneopw"); ;
			var priceArray = (ArrayList)JSON.JsonDecode(answer);

			var newPrices = new List<Prices>();
			foreach (Hashtable priceTable in priceArray)
			{
				var price = ((string)priceTable["xml"]).LoadFromStringXml<Prices>();
				price.priceType = (string)priceTable["priceType"];
				price.priceId = (int)(double)priceTable["priceId"];
				newPrices.Add(price);
			}

			return newPrices;
		}

		static int personId = 1;
		public static InstancePerson GetDefaultInstancePerson()
		{
			var gameXmlData  = GetEmptyGameXmlData(fillItems: true, personAbility: true, lootlist: true, constants: true, emotion: true);

			var person = ObjectMother.GetInstancePerson(new ObjectMother.GetInstancePersonParams()
			{
				personType = PersonType.Warrior,
				gamexmlData = gameXmlData,
				initItems = true,
				personId = personId++,
			});

			return person;
		}

		public static InstancePerson GetInstancePerson(GetInstancePersonParams personParams)
        {
            InstancePerson person = new InstancePerson(personParams.loginId, Substitute.For<IInstanceSqlAdapter>());
            person.CreatEmotionPerson(new EmotionPerson());
            person.personType = personParams.personType;
            person.personId = personParams.personId;
            person.SetGameXmlData(personParams.gamexmlData);
            person.login = new LobbyLogin(personParams.loginId, DateTime.MinValue);
            person.login.AddPerson(person);
            person.activeAbility = new List<AbilityXmlData>();
            person.Wallet = new Wallet(person, personParams.globalEvents, personParams.shopManager);
            person.StatContainer = new PersonStatContainer(person);
			person.Notifications = new List<string>();
			var sqlData = new InstancePersonSqlData();

            if (personParams.initItems)
            {
                sqlData.Items2 = new List<ItemSaveData>();
                person.InitItems(sqlData, new List<ItemSaveData>());
            }

            person.login.InitPersons();
            person.login.SetPlayer(NetworkPlayer.unassigned);

			return person;
        }

        public class GetInstancePersonParams
        {
            public PersonType personType;
            public GameXmlData gamexmlData;
            public GlobalEvents globalEvents = GlobalEvents.Instance;
            public ShopManager shopManager = null;
            public bool initItems = false;
            public int loginId = 1000;
            public int personId = 1000;
        }
    }
}
