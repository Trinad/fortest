﻿using System;
using Assets.Scripts.InstanceServer.DI.Installers;
using CommandLine;
using LobbyInstanceLib;
using Node.Instance.Assets.Scripts;

namespace Node.Instance
{
	class Program
	{
		static void Main(string[] args)
		{
			InstanceCommandLineOptions options = null;
			Parser.Default.ParseArguments<InstanceCommandLineOptions>(args)
				.WithParsed(parsed => options = parsed)
				.WithNotParsed(errors =>
				{
					Console.WriteLine("Bad command args");
					Environment.Exit(1);
				});

			var server = new DotnetDIServerStart<PixelInstanceInstaller>();
			server.Run(options);
		}
	}
}
