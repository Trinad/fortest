﻿using Node.Guild.DLL;
using System.Configuration;
using UtilsLib.Logic.ServerStart;

namespace NodeGuild
{
	class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			CompositionRoot.Init(ConfigurationManager.AppSettings, out GuildServer server);

			StartGameService.Run(server, "PixelGuild");
		}
	}
}
